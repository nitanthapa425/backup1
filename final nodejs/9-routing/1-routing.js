
// req.url gives the url which is sring


//routing
const http=require("http")

const server=http.createServer((req,res)=>{

    console.log(req.url);
    //it gives url
    if(req.url=="/about")

    // / means url is orginal url
    // /about means url is  orginal url/about ..........(same as exact="true") 
    // /contact means url is orginal url/contact
    // imp
    //"/about"//means path must be orginalurl/about
    //it will not enter in if , if  path is orginalurl/about/anyother...  unlike react  (it is same as exact="true" condition)
    {
       
        res.end("<h1> you are in about page</h1>")
        // just check
        console.log("hellow hero");
    }
    else if(req.url=="/contact")
    {
        res.end("<h1> you are in contact page</h1>")
    }
    else if(req.url=="/about/contact")
    // enter in the condition if path (url) is "/about/contact"
    {
        res.end("<h1>this is combination of about and contact page</h1>")
    }
    else 
    {
        //for page not found 
        // we do one thing
        //for sucessful http request status is 200
        //for clien error(ie if clien reques other than available) status is 400
        //if res.writeHead is not given then it is 200
        //thus we have to chage it to 404
        //if you get confuse forget it
        //to see status go in network in inpect
        res.writeHead(404)
        //error message will through in console due to status=404 which is good
        res.end("<h1>404 page page is not found</h1>")
    }

    


})

server.listen(8000,"127.0.0.1",()=>{
    console.log("server is listening at localhost:8000")
})