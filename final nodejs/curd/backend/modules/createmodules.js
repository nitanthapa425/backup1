const mongoose=require("mongoose")
const userSchema=mongoose.Schema({
    // here we are making name unique
    name:{
        type:String,
        required:true,
        unique:true
    },
    price:{
        type:Number,
        required:true,
    },
    quantity:{
        type:Number,
        required:true
    },
    image:{
        type:String,
        required:true
    }

},{timestamps:true})

const Product=mongoose.model("Product",userSchema)
// in databse USER wiil be users collections

module.exports={Product}