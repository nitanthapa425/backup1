
const mongoose=require("mongoose")
const database=process.env.DATABASE||8000
mongoose.connect(database,{
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false
}).then(()=>{
    console.log("connected to the database successfully")
}).catch((error)=>{
    console.log("unable to connect database")
})
