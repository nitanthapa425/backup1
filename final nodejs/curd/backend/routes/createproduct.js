const express = require("express");
const upload = require("../middlewares/upload");
const { Product } = require("../modules/createmodules");
const router = express.Router();
// you always forget what to do when req.body is use
router.post("/createproduct", upload.single("image"), async (req, res) => {
  let { name, price, quantity } = req.body;
  let image = req.file.path;
  // product_image:req.file.path,

  const document = Product({
    name,
    price,
    quantity,
    image,
  });
  try {
    const result = await document.save();
    res.send("product is post successfully");
  } catch (err) {
    res.send("Unsuccessfull");
  }
});

module.exports = router;
