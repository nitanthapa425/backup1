const express=require("express")
const { Product } = require("../modules/createmodules")
const router=express.Router()
router.put("/updateproduct",async (req,res)=>{
    let {price,quantity,name} = req.body

    try 
    {
        console.log(price)
        console.log(name)
        console.log(quantity)
        const result=await Product.updateMany({name:name},{$set:{price:price,quantity:quantity}})
        res.send("updated successfully")
    }
    catch(err)
    {
        res.send("cannot update")
    }   

})

module.exports=router