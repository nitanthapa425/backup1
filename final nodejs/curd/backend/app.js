//connecting with dotenv start
const dotenv = require("dotenv");
const cors = require("cors");
dotenv.config();
port = process.env.PORT;

//connecting with dotenv start
const express = require("express");
const createrouter = require("./routes/createproduct");
const deleterouter = require("./routes/deleteproduct");
const readrouter = require("./routes/readproduct");
const updaterouter = require("./routes/updateproduct");
const app = express();
//connecting with database start
require("./Database/conn");
//connecting with database end
// 1 way
app.use(express.static("public/uploads"));
// second way
// app.use("/public/uploads",express.static("public/uploads"))

app.listen(port, () => {
  console.log(`server is listening at ${port}`);
});

app.use(
  cors({
    credentials: true,
    origin: true,
  })
);

app.use(express.json());
app.use(createrouter);

app.use(updaterouter);

app.use(readrouter);

app.use(deleterouter);
