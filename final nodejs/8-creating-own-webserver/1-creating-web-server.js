



//to create own web server you have to require http core moduel
//the http.createServer() method includes request and response parameters 
//the request object can be used to get information about the current   http request
//eg url request header and data
//the response object can be use to send a response for a current HTTp request
const http=require("http");

const server=http.createServer((req,res)=>{
  
 
res.writeHead(200,{"Content-Type":"text/plain"})
// status 200 is given for the page which is ok
// note status code is number
//status 404 means page is non available
// Content-Type:text/plain means the  response content is simple text
// content-Type:application/json means the response content is json
// content-Type:text/html means the response content is html
//
    res.write("server has response")//write a response to the client
    // ************imp********
    // res.write(string) ie res.write method take only string 
    //it does not take obj list or variable
    res.end();//end the response
    //when any request is done then the serve gives response
// by the help of res.write responce is write on cliend
// note to end respond res.end() must be use

        // in side " "you can write html code alse
    // imp res.end must be given
    // you can not write two or more res
})
//creasterver()has on call back functino
//this call back function has two paramenter request and response8



server.listen(8000,"127.0.0.1",()=>{console.log("hello")});//the server object listens  request and give response on port 8000
// here the request is listen in
// 8000 port (note the port must be selected such that the port is unuse here we have 3000 8000 ,5000unuse port)
// 127.0.0.1 is a ip address (localhost)..is a host name
//call back functinon will execute in last

