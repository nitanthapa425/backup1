// checkin weather the token will remove from database when its expiri time reach
//answer is no
//the token will not remove from database
//but the token will not work after its expiri time

//connecting nodejs to server
const express=require("express")
const secretkey="mynameisnitanthapaandiliveingagalphedi"

const jwt=require("jsonwebtoken")

const server=express()
server.listen(8000,()=>{
    console.log("serveri is listening at port 8000")
})
//to use req.body from body
server.use(express.json())
server.use(express.urlencoded({extended:false}))

//connectint nodejs to database
const mongoose=require("mongoose")
mongoose.connect("mongodb://localhost/jwtexpirechecking",{
    useCreateIndex:true,
    useUnifiedTopology:true,
    useNewUrlParser:true
}).then(()=>{
    console.log("connected to the database")
})

//creating schema

const newSchema=mongoose.Schema({
    token:{
        type:String,
        required:true,
        
    },
    // to remove from databse

    //just make one field  whose contain date
    //and to delete the the object use expires
    d1:{
        type:Date,
        default:Date.now(),
        // 
        expires:30

    }
})

//creating model or collection
const collection = mongoose.model("collection",newSchema)

//creating document

server.post("/token",async(req,res)=>{

    const token=await jwt.sign({name:"nitan"},secretkey)
    // VVVVVVVVvv**************MMMMMMMMMMMMPPPPPPPPPPPPPP
    //note the expiresIn in token doesnote remove the token 
    //but it will only expire token


    const document=new collection({
        token:token
    })

    const result=await document.save()

    res.send(result)


})

server.get("/token",async(req,res)=>{

    const document= await collection.find()

    const token=document[0].token
    console.log(token)

    try
    {

        const payload=await jwt.verify(token,secretkey)
        res.send(payload)
    }
    catch(err)
    {
        res.send("sorry the token has already expired")
    }
    

})
server.get("/",(req,res)=>{
    res.send("home page")
})