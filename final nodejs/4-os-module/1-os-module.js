//note you can get ram informatin in task manager
//to open it press ctr+ shift +esc

const os = require("os");

// getting the bit ie 64 bit architecture or 32 bit architecture of os

const osbit = os.arch();
console.log(osbit);

//getting the total ram;
//gives the total ram in bytes
const totalram = os.totalmem();
console.log(`total ram is ${totalram / 1024 / 1024 / 1024}B ie bytes`);

//getting total ram in GB ie giga bytes
console.log(`total ram is ${totalram / 1024 / 1024 / 1024}GB`);

//getting the free ram in GB
console.log(`free ram is ${os.freemem()}B`);

//getting the frre ram in  GB
console.log(`total ram is ${os.freemem / 1024 / 1024 / 1024}GB`);

//getting the type ie mac or window or linux

console.log(`the type of os  is ${os.type} `);
