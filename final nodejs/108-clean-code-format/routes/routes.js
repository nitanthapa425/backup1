const collection1 = require("../models/models.js");
const express = require("express");
const router = express.Router();
const { call1, call2 } = require("../controller/controller.js");

router.get("/call1", call1);
router.get("/call2", call2);

router.post("/push", async (req, res) => {
  //if server.use("/abc",router) is used then  the route will be /abc/push

  const document1 = new collection1(req.body);

  const result = await document1.save();
  res.send(result);
});
router.get("/push/:id", async (req, res) => {
  const { id } = req.params;
  console.log(id);
  console.log(typeof id);
  // imp though it is string it acts as both
  const document1 = await collection1.find({ _id: id });
  res.send(document1);
});

module.exports = router;
