


//reading file and writing in server in stream format

//requiring fs module
const fs= require("fs");
//reauiring http module
const http=require("http")

//reading the file.txt in streming manner
// does not have call back function unlike readFile with asynk

//reading in stream manner
const rstream=fs.createReadStream("./file.txt")
// imp rstream is not whole data it is chunk data


//creatin server
const server=http.createServer((req,res)=>{
    //writing in stream manner
    const rstream=fs.createReadStream("./file.txt")
    rstream.pipe(res)
    // note her no need to write res.end because by defauld res.end() is present in rstream.pipe()
});
server.listen(8000,"127.0.0.1",()=>{
    console.log("server is listening at localhost 8000")
})