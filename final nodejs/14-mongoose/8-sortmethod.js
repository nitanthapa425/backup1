//requiring mongoose

const mongoose = require("mongoose");

//connecting with database4
mongoose
  .connect("mongodb://localhost:27017/database4", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connect successfuly");
  });

//defining Schema

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  amount: { type: Number, required: true },
  no: { type: Number, required: true },
});

//connecting to collection
const collection1 = new mongoose.model("collection1", schema);

//reading with locical operation

const getDocument = async () => {
  //sort() method

  // in sorting All Big letter are small than small letter
  //ie ascending order is
  // A,B,Z,a,b,
  //here Z is smaller than small a

  //note in js number sort doesnot work as expected
  //but in mondb number sort work properlyb wi sort() methode
  const result = await collection1.find().sort("no").select("no");
  //note
  //"" means no sorting
  //"no" means sort according to no and in ascending
  //"-no" means sort descending

  //note in alfabet while sorting
  //the capital letter has small order than small one
  //for if the intput are a Z b;
  // if the sorting is ascending then
  //the output will be Z,a,b

  console.log(result);
};

getDocument();
