//requiring mongoose

const mongoose = require("mongoose");

//connecting with database4
mongoose
  .connect("mongodb://localhost:27017/database4", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connect successfuly");
  });

//defining Schema

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  amount: { type: Number, required: true },
  no: { type: Number, required: true },
});

//connecting to collection
const collection1 = new mongoose.model("collection1", schema);

//reading with locical operation

const getDocument = async () => {
  const result = await collection1.find({ no: { $gt: 8 } }).select("no");
  console.log(result);
  const result1 = await collection1.find({ no: { $gte: 5 } }).select("no");
  console.log(result1);
  const result2 = await collection1.find({ no: { $lt: 2 } }).select("no");
  console.log(result2);
  const result3 = await collection1.find({ no: { $lte: 5 } }).select("no");
  console.log(result3);
  const result4 = await collection1.find({ no: { $ne: 5 } }).select("no");
  console.log(result4);
  const result5 = await collection1
    .find({ no: { $in: [1, 2, 3, 4] } })
    .select("no");
  // here 1,2,3,4 nomber is shown
  // note vvvvimp in  is use for single key it does not see for two keys
  console.log(result5);
};

getDocument();
