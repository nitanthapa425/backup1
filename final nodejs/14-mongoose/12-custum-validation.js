

// make your own validatin while inserting
//custoum validation

const mongoose=require("mongoose")

//creating or conneccting with database
mongoose.connect("mongodb://localhost:27017/databasecustumvalidation",{ useNewUrlParser: true,useUnifiedTopology: true }).then(()=>{console.log("connect successfuly")})
//mongoose.connect()is a promise object


//creating schema

const schemaobj=new mongoose.Schema(
    //new mongoose.Schema give a object
    {
  //lets apply custum validation on videos
        no:{
            type:Number,
            unique:true,
            //unique true means no should be unique if inserting document no is same then error will through
            required:true
            //required:true it meas while inserting file in document if there is NO (no) field then show error
        },
        name:{
            type:String,
            require:true,
            lowercase:true,
            trim:true,
            // for more information see in mongoosejs.com and click in schema
            //for string there are many validator
            //losercase:true...it means while inserting into database the name is converted into lowercase
            //uppercase:true
            //trim:true....it means while inserting the name trim(or remove)(trim is just like beared trim) all left and right space(but not center space)
            //minlength:2 ..it means if the length of inserting name is less than 2 then througha a error
            //maxlenght:30 ..it means if the length of inserting name is greater than 30 then throught a error
            //enum:["ram","sham","hari"]..it means if the inserting name is other than ram ,sham, hari then through a error
        },
        videos:{
            type:Number,
            validate(value)
            {
                if(value<=0)
                {
                    throw new Error("videos must be greater than 0")
                }
            }

            //for number inbuilt validator are
            //max:30 it means if the inserting number is greater than 30 then show error
            //min:5 it means if the inserting number is greater than 5 then show error
            //enum:[1,2,3] it means if the inserting number(videos) is other than 1,2,3 then show error


        },

        ctype:{
            type:"string",
            lowercase:true,
            trim:true,
            enum:["frontend","backend","database"]
        }

    }
)

//creating model//or creating a collection name as collection 1
const collection1=new mongoose.model("collection1",schemaobj)
// here "colection1" represent a name of collection
//collection1 is a function


//creating document
const document1=new collection1({
    no:55,
    name:"expressjs",
    videos:0,
    // here videos is 0 thus it will show error
    ctype:"backend"

})




const insertdocument=async()=>{

    const result=await collection1.insertMany([document1])
    console.log(result)

}
insertdocument();

const getdocument=async()=>{

  const result1= await collection1.find();
  console.log(result1);

}

// getdocument();








