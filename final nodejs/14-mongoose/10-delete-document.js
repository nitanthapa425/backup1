//requiring mongoose

const mongoose = require("mongoose");

//connecting with database4
mongoose
  .connect("mongodb://localhost:27017/database4", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connect successfuly");
  });

//defining Schema

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  amount: { type: Number, required: true },
  no: { type: Number, required: true },
});

//connecting to collection1
const collection1 = new mongoose.model("collection1", schema);

// deleting document

const deleteDocument = async () => {
  //there is DeleteOne and DeleteMany
  //DeleteOne Delete the first match
  //DeleteMany Delete all of the match

  //note await collection1.DeleteMany({no:12}}) delete the document where no:12
  //return how many (number) object are Deleteed

  const result = await collection1.deleteMany({ no: 12 });
  // const result=await collection1.deleteMany({_id:{$in:["id1","id2","id3"]}})

  //note you can alsodelete by
  // const result=await collection1.deleteMany({name:{$in:["name5","name6"]}})

  //you can also delete like this
  // const result=await collection1.deleteMany({$or:[{name:"name2"},{name:"name3"}]})

  console.log(result);
};

deleteDocument();
