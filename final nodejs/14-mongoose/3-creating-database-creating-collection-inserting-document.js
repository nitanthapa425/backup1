

//plese dont play with collection1 of database4
//becaulse we learn read operation form collection1
//requiring mongooes
const mongoose=require("mongoose")

//creating or conneccting with database
mongoose.connect("mongodb://localhost:27017/database4",{ useNewUrlParser: true,useUnifiedTopology: true }).then(()=>{console.log("connect successfuly")})
//mongoose.connect()is a promise object


//creating schema

const schemaobj=new mongoose.Schema(
    //new mongoose.Schema give a object
    {
        name:{required:true,type:String},
        // note the first letter of string is S and sam for number
        amount:{required:true,type:Number},
        no:{required:true,type:Number}
    }
)

//creating model//or creating a collection name as collection 2
const collection2=new mongoose.model("collection2",schemaobj)
// here "colection1" represent a name of collection
//collection1 is a function


//creating document
const document1=new collection2({
    //note document1 is a object
    name:"name1",
    amount:10,
    no:1
})
const document2=new collection2({
    //note document1 is a object
    name:"name2",
    amount:20,
    no:2
})

//it is recommande that
//for the promise object use async
//it is mostly recommmande that to use try and catch
//and if the promise object read or write then you must use it in async for easy purpose
const insertdocument=async()=>{

    try
    {

        
            const result=await collection1.insertMany([document1,document2])
            //await collection1.insertMany([document1,document2])
            //it will return the documents are are inserted ie show list of document1 and document2
            console.log(result)
    }
    catch(err)
    {
        console.log(err)
    }

}
insertdocument();
// note it is recommend you not to use ,then, to show result show result inside async function for simplicity


//inserting document
// collection2.insertMany([document1,document2])
//imp collection2.insertMany() is a promsie
//it is better to use it in async await


//








