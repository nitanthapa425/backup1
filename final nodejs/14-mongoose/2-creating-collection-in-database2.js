// schema
//schema define the structure of the document
//we can set default values in schema

//requiring mongoose
const mongoose = require("mongoose");
//connecting nodejs with database2
mongoose.connect("mongodb://localhost:27017/database3", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});

// creating schema
// ////////////////////////vvimp**********************************
//note
// for dame database
// you can apply two or more Schema
//for eg for documen1 and documen2 we can give one schema
//and for documen3 we can give another schema

//and note
//while reading
//just menstion schema
//but the content of schema does not matter
//but nout make the type of key proper
//note but while reading make schema such tha it is proper way
//ie can be converted easily in type
//for eg database has name:"nitan" which is string type
//now if you try to pull by using name type number then it will show error
//but if name:"2" in database
//then you can pull it by usin name type number as it can be converted in to number
//for eg
//if you push data in database by using one schema
//then you can pull it by using another schema
//********************************************************************************imip********** */
//in simple word while finding
//just use blank schema ie even dont mention name height etc
const structureDocument = new mongoose.Schema(
  {
    name: { type: String, default: "nitan" },
    // imp dont use "String"
    // default "nitan means"
    //if there is no name field in document then by default  name:"nitan " is added
    height: { type: Number, default: 5.3 },

    age: { type: Number, required: true },
    // not not require its required
    // required:true means in ducument we must have age field
    //if you dont add age field then it will show error
    weight: Number,
  },
  { timestamps: true }
);
// imp timestamps:true give the createat and updated at timint information

//creating collection
const anothername = new mongoose.model("collectionn2", structureDocument);
// "collectionn1" is the name of the Collection
//it is recommended that  anothername must be Anothername
// anothername is the name ie is use to know the collectionn2 in node express

//creating document
//while making document just thik you are making class with the name of collection

const document1 = new anothername({
  // here name key is not given tus by defauld name:"nitan" will be place in document
  height: 5.3,
  age: 27,
  weight: 60,
});
// different document have different name
const document2 = new anothername({
  name: "jackma",
  height: 5.5,
  age: 27,
  weight: 60,
});

//inserting into database

anothername.insertMany([document1, document2]);
