

//requiring mongoose

const mongoose=require("mongoose")

//connecting with database4
mongoose.connect("mongodb://localhost:27017/database4",{ useNewUrlParser: true,useUnifiedTopology: true }).then(()=>{console.log("connect successfuly")})

//defining Schema

const schema=new mongoose.Schema({
    name:{type:String,required:true},
    amount:{type:Number,required:true},
    no:{type:Number,required:true}
    
})

//connecting to collection
const collection1=new mongoose.model("collection1",schema)

//reading with locical operation

const getDocument=async()=>{
    // countDocuments() method
    // note there is s

    const result=await collection1.find().countDocuments();
    //countDocument just cont the number of object ie ie matches
    console.log(result)

  
    
}


getDocument();



