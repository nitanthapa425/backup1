//requireing mongoose
const mongoose = require("mongoose");

//making database and connecting node js with database by the help of mongoose

mongoose.connect("mongodb://localhost:27017/databaserrr", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});

//before  adding collection we have to define the structure

const schemaobj = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
  },
});

//making collection
const collection1 = new mongoose.model("collection1", schemaobj);

//making document

const document1 = new collection1({
  name: "                   1                      ",
});

const document2 = new collection1({
  name: "        2             ",
});

const pushdata = async () => {
  try {
    const result = await collection1.insertMany([document1, document2]);
    console.log(result);
  } catch (e) {
    console.log(e);
  }
};

const finddata = async () => {
  try {
    const result = await collection1.find({}).select("-_id");
    console.log(result);
  } catch (e) {
    console.log(e);
  }
};
const deletedata = async () => {
  try {
    const result = await collection1.deleteMany({});

    console.log(result);
  } catch (e) {
    console.log(e);
  }
};
const updatedata = async () => {
  try {
    const result = await collection1.updateMany(
      {},
      { $set: { name: "nitan" } }
    );

    console.log(result);
  } catch (e) {
    console.log(e);
  }
};

// pushdata()
// finddata();
// deletedata();
// updatedata()
