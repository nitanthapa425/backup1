

//requiring mongoose

const mongoose=require("mongoose")

//connecting with database4
mongoose.connect("mongodb://localhost:27017/database4",{ useNewUrlParser: true,useUnifiedTopology: true }).then(()=>{console.log("connect successfuly")})

//defining Schema

const schema=new mongoose.Schema({
    name:{type:String,required:true},
    amount:{type:Number,required:true},
    no:{type:Number,required:true}
    
})

//connecting to collection1
const collection1=new mongoose.model("collection1",schema)

// updating

const getandupdateDocument=async()=>{
//there is updateOne and updateMany
//updateOne update the first match
//updateMany update all of the match

//note await collection1.updateMany({repet:3},{$set:{repet:"2"}})
//return how many (number) object are updates

// ******************************************imp***************************************ask************************************
// what if the schema is not known
// note 
    const result=await collection1.updateMany({no:11},{$set:{no:12}})


    console.log(result)

  
    
}


getandupdateDocument();



