//requiring mongooes
const mongoose = require("mongoose");

//creating or conneccting with database
mongoose
  .connect("mongodb://localhost:27017/database4", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connect successfuly");
  });
//mongoose.connect()is a promise object

//creating schema

const schemaobj = new mongoose.Schema(
  //new mongoose.Schema give a object
  {
    name: { required: true, type: String },
    // note the first letter of string is S and sam for number
    amount: { required: true, type: Number },
    no: { required: true, type: Number },
  }
);

//creating model//or creating a collection name as collection 1
const collection1 = new mongoose.model("collection1", schemaobj);
// note to read also there must be schema
// here "colection1" represent a name of collection
//collection1 is a function
//creating document
//reading

const getdocument = async () => {
  const result = await collection1.find({}).select("no").limit(3).skip(2);
  //   find({amount:10})..means find the object whose one key and value are amonut:10
  // find({amount:10,name:"name1"})..means find object whose has amoung:10 and name:name1
  //select("name") means show name field only from document
  //select("") means show all field  from document
  //select("-name") means show all field  except name from document
  //but note id is automatically show thus you have to do select({name:1,_id:0})
  //limig(2)  means only show 2 item form match
  //skip work just before limit (ie the position does not matter)

  console.log(result);
};
getdocument();
