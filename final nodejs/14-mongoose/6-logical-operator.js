//requiring mongoose

const mongoose = require("mongoose");

//connecting with database4
mongoose
  .connect("mongodb://localhost:27017/database4", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("connect successfuly");
  });

//defining Schema

const schema = new mongoose.Schema({
  name: { type: String, required: true },
  amount: { type: Number, required: true },
  no: { type: Number, required: true },
});

//connecting to collection
const collection1 = new mongoose.model("collection1", schema);

//reading with locical operation

const getDocument = async () => {
  // logical operator

  const result = await collection1
    .find({ $or: [{ no: 2 }, { no: 3 }] })
    .select("no");
  console.log(result);
  const result1 = await collection1
    .find({ $and: [{ no: 2 }, { no: 3 }] })
    .select("no");
  console.log(result1);

  //you can also delete like this
  // const result=await collection1.deleteMany({$or:[{name:"name2"},{name:"name3"}]})
};

getDocument();
