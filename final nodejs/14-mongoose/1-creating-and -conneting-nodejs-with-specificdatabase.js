//if this error will show dont worry neglect it
// (node:7944) Warning: Accessing non-existent property 'MongoError' of module exports inside circular dependency
// (Use `node --trace-warnings ...` to show where the warning was created)

// to start mongoose
// you have to downoload npm from npm
// npm i mangoose

//requiring mangoose

const mongoose = require("mongoose");

//creating database and connecting with nodejs
//or connecting the existing database with nodejs

//syntax
//mongoose.connect("pathof database2",{if require which is through in error})
// note mongoose.connect() is an promise object
//thus it can be use in async await

//or"mongodb://locolhost/databasename"

mongoose.connect("mongodb://localhost:27017/database2", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
  // always use these thing
  //and note mongoose.connect()gives the promise
});

// imp
/// if the database is empty ie doesnot contain any collection then it will not show though it is made

// just check another one
// const mongoose =require("mongoose")
//mongoose are use to connect node js or express js with database ie mongodb
// imp mongodb://localhost:27017/database2 is important
//if database2 is is not present then it will make database2 and connect noder or express js to database 2
//if database2 is already present then it will connect node ore express js to database2

//note useUnifiedTopology: true and useNewUrlParser are copied form erro
//if other error are occur just copy it and past it

//connnection database to nodejs or expressjs
//syntax
// mongoose.connect("path of database/database1",{if require..through in error}).then().catch()
// mongoose.connect("mongodb://localhost:27017/database2",{ useNewUrlParser: true,useUnifiedTopology: true }).then(()=>{console.log("conection is successfull")}).catch((err)=>{console.log("connection is gone")})
