//creating server
const express = require("express");
const app = express();
app.listen(8000, () => {
  console.log("server is listening at port 8000");
});

// middleware has one extra parameter ie next
// global middleware means
//this middleare works for all the routes below it
const middleware1 = (req, res, next) => {
  console.log("i am first global middleware");
  next();
};
const middleware2 = (req, res, next) => {
  console.log("i am fsecond global middleware");
  next();
};
const middleware3 = (req, res, next) => {
  console.log("i am third middleware(but not global");
  next();
};

app.use(middleware1);
// vvvimp********************************
//the middlware1 will work for all routes below app.use(middlware1) code
app.get("/home", middleware3, (req, res) => {
  // vvimp
  // in this given exaple

  //middleware3  is only use for "/home" route

  res.send("hello");
});

app.use(middleware2);
// vvvimp********************************
//the middlware2 will work for all routes below app.use(middlware2) code
app.get("/", middleware3, (req, res) => {
  res.send("hello");
});
// for route "/home" it has only 2 middleware middleware1 and middleware 3 fist middleare1 execute then middleware 3 execute
// for route "/" it has only 3 middleware middleware1 ,middleware 2,and middleware 3 fist middleare1 execute then middleware 2 execute then middleware 3 execute

//use of middleware
//validation using middlear

const validate = (req, res, next) => {
  if (req.params.name === "nitan") {
    //let also give surname
    // sending some value from middleware
    //it is possible  from request
    req.surname = "thapa";

    next();
  } else {
    res.send("soory you are not authorized person");
  }
};
app.get("/abc/:name", validate, (req, res) => {
  res.send(`my name is ${req.params.name} ${req.surname}`);
});
