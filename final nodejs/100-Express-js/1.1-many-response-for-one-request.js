




const express=require("express");
const serverobj=express();


serverobj.listen(8000,()=>{
    console.log('server is listening at localhost:8000')
})


//let make two respond for / request
//if you do this 
//then first respond is execute
//then the respond is end it means   respond is totally end
//thus it does not listen other respond
serverobj.get("/",(req,res)=>
{
    res.send("this is first home page")
    // to check status go to the inspect and click on network

})
serverobj.get("/",(req,res)=>
{
    res.send("this is second or other home page")
 

})
// note you might be thinking the second request will overwrite first request
//then you are wrong
//once first  respond is enter the respond is end thus it will not listen second respond

