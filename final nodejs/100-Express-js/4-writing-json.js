


//writhing json response

const express=require("express");
const serverobj=express();


serverobj.listen(8000,()=>{
    console.log('server is listening at localhost:8000')
})

serverobj.get("/",(req,res)=>
{
    //sending json
    res.send([{name:"nitan",sur:"thapa"}])
    //note by default
    //res.send perform strigify operation to object
    //and send as a response

    //note 
    // res.send([{},{}]) can be replace by res.json([{},{}])...meaning send json response
    //it is recommend to use res.json for sending json  response
    

})

