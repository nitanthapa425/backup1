


//requiring express
const express = require("express");
//making server
const app=express();
//listening server
app.listen(8000,()=>{
    console.log("server is listning in localhost:8000")
})

// making header file as reusalbe
//for this we have to register it folder as partial
const hbs=require("hbs")
hbs.registerPartials("./partials")

//setting view engine
app.set("view engine","hbs")
//giving the views path
app.set("views","./templates")

app.get("/",(req,res)=>{
    res.render("./home.hbs",{home:"home"})
    // firs it go to the views path folder
    // search./index.hbs in views path
})
app.get("/about",(req,res)=>{
    res.render("./about.hbs",{about:"about"})
    // firs it go to the views path folder
    // search./index.hbs in views path
})
app.get("/contact",(req,res)=>{
    res.render("./contact.hbs",{contact:"contact"})
    // firs it go to the views path folder
    // search./index.hbs in views path
})
app.get("/*",(req,res)=>{
    res.render("./error.hbs",{error:"404 page oops the page is not available"})
    // firs it go to the views path folder
    // search./index.hbs in views path
})
