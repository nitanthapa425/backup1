const express = require("express");
const app = express();
app.listen(8000, () => {
  console.log("server is listning in localhost:8000");
});
//chain Router
//chainRoute

//all methode will run for all get post put  delete methode

//if you apply get methode firs .all methode is run then get methode will run
app
  .route("/")
  .all((req, res, next) => {
    console.log("i am all methode and is use for common operation");
    next();
  })
  .get((req, res) => {
    res.send("i am get methode");
  })
  .post((req, res) => {
    res.send("i am post methode");
  })
  .put((req, res) => {
    res.send("i am put methode");
  })
  .delete((req, res) => {
    res.send("i am delete methode");
  });
