
// partial are just like component in  react
// in react we call it component
//in node js we call it partial


//requiring express
const express=require("express");
const app=express();
app.listen(8000,()=>{
    console.log("server is listening at localhost:8000")
})

//authoring the file to be reuse for that you have to require hbs

const hbs=require("hbs")
// note even for settting view engine it is not necessary to import hbs but
//to register folder as partials we have to require hbs


// registering folder as partials
hbs.registerPartials("./partial")
// in react any file can be reuse
//bute in node js
// those file whose own folder is register as partials can be reuse


// it means footer header file can be reuse
// check can not be reuse
//and  about home can not be reuse




//giving dynamic response on request

//setting the view-enginnew as hbs
app.set("view engine","hbs")
//giveing the views path location
app.set('views',"./templates")

app.get("/",(req,res)=>{
    res.render("./home.hbs",{home:"home"})
    // *********************************************imp***************************************8
    // note home is only pass to home.hbs file
    //ie home key is not pass to ./about.hbs
    //it means you can not call home in ./about.hbs fiel
    // we know render automatically go to the views path
})
app.get("/about",(req,res)=>{
    res.render("./about.hbs",{about:"about"})
    // we hnow render automatically go to the views path
})


