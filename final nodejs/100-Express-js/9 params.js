//making server
const express = require('express')
const app=express()

app.listen("8000",()=>{
    console.log("you are listening at port 8000")
})

app.get("/",(req,res)=>{
    res.send("welcome to home page")
})

app.get("/a/:param1/:param2",(req,res)=>{
    res.send(req.params)
    // {"param1":"p","param2":"1"}

})

app.get("/a/:param1",(req,res)=>{
    res.send(req.params.param1)

})

// ?
//? means wheater there is param occurr or not just 
// app.get("/:a") it means to enter there must be present of a
app.get("/:a?",(req,res)=>{
    res.send("wheater a exist or not just enter")
})


