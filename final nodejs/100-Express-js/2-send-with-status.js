



//send with status
//by default for good bape 200 status is send automatically
//and by default for not found page 404 status is send automatically
//it means it is not necessary to send status
//but if you want to send status then


const express=require("express");
const serverobj=express();


serverobj.listen(8000,()=>{
    console.log('server is listening at localhost:8000')
})

serverobj.get("/",(req,res)=>
{
    res.status(201).send("you are in home page")
    // to check status go to the inspect anc click on network

})

