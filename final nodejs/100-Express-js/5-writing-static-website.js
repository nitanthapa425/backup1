//one way of giving path
// const path=require("path")
// const staticPath=path.join(__dirname,"./")
//here ./ menas in _dirname folder
//note __dirname represent the folder name of this working file

// const staticPath=path.join("./")
// imp const staticPath=path.join(__dirname,"./index.html")
//dont do like that because we only give the folder name where html file lies

// builtin middleware
const express = require("express");
const serverobj = express();

// const staticPath="./public/html"
//or
// const staticPath=`${__dirname}`
const path = require("path");
const staticPath = path.join(__dirname, "./public/html");
//  not dont do like that const staticPath="./index.html" because we only give the folder name where html file lies

serverobj.use(express.static(staticPath));
serverobj.get("/", (req, res) => {
  res.send("you are in home page");
});

serverobj.listen(8000, () => {
  console.log("server is listening at localhost:8000");
});

serverobj.get("/about", (req, res) => {
  res.send("you are in about page");
});

// imp........."/" this code does not work
//because one the rquest is / the code is execute form top to buttom
//and it get there is  one response for / and the the response is end

// this code doesnot work if other response is ther form same url
//not it is valid for other situation also
