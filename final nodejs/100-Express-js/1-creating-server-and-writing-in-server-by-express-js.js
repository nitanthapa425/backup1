

//requiring express js module

const express=require("express")
//require("express")..return a function
//and if you call that function...it will return a server object
 
const server=express();
//here express() return a sever object
//note this code also create server


//once you create server you have to listen request fro and give responce from port
server.listen(8000,"127.0.0.1",()=>{
    // note you can neglet host name ie "127.0.0.1"

    console.log("server is listening request at localhost:8000")
})


server.get("/",(req,res)=>{
    // "/" means exact orginal url
    //req and res are object

    res.send("this is home page i am ")
    // here no need to do res.end res.write all thing are done by  res.send
})
server.get("/about",(req,res)=>{
    // "/about" means exact orginalurl/about
    

    res.send("this is about page i am ")
    
})

//note if you request other than / and /about it will autoaticaly show  /Cannot GET /ram
// and we do not have to give 404 in status it will automatically get
