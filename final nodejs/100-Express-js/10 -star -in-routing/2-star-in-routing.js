// *****************imp***********************************************
// flow or match of url (request) is very importaint in routing



//requireing express
const express=require("express")
const app=express()
//listening app server
app.listen(8000,()=>{
    console.log("server is listening at localhost:8000")
})

//*****************************************imp********************************* */
// note the flow of matches is very important
//what happen request(url) is enter in the browser
// when url is enter 
// first it see first condition "/"
// then it see first condition "/about"
// then it see third condition "/contact"
//then it see fouth condition "/about/*"
//then it see five conditon for matches

//during match of url if the condition matches(if url matches)
//respond is send for that matches
app.get("/",(req,res)=>{
    res.send("you are in home page")
})
app.get("/about",(req,res)=>{
    res.send("you are in about page")
})

app.get("/*",(req,res)=>{
    res.send("404 page,page is not available")
})
// imp*********************************
//here /* lies before /contact
//when /contact request is done
//firs is see /....does not matches
//then it  see /about....does not matches
//then it see /* ....matches because /* means homeurl/(jayvayeni)
//thus res.send("404 page",page is not available)  respond is given to the clint
//ie /contact respond is not given because respond of first matches is given


// thus /* is place at last
app.get("/contact",(req,res)=>{
    res.send("you are in contact page")
})

app.get("/about/*",(req,res)=>{

    res.send("404page, this about about about page is not availabe")
})






