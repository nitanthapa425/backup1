//requireing express
const express = require("express");
const app = express();
//listening app server
app.listen(8000, () => {
  console.log("server is listening at localhost:8000");
});

//*****************************************imp********************************* */
// note the flow of matches is very important
//what happen request(url) is enter in the browser
// when url is enter
// first it see first condition "/"
// then it see first condition "/about"
// then it see third condition "/contact"
//then it see fouth condition "/about/*"
//then it see five conditon for matches

//during match of url if the condition matches(if url matches)
//respond is send for that matches
app.get("/", (req, res) => {
  res.send({ name: "nitan", age: "25" });
});
app.get("/about", (req, res) => {
  res.send("you are in about page");
});
app.get("/contact", (req, res) => {
  res.send("you are in contact page");
});

app.get("/about/*", (req, res) => {
  //note but in place of * the must be some thing otherwish it will not work ie for /about thie route will not hit
  // /about/* it means
  // homeurl/about/(jayvayeni)
  res.send("404page, this about about about page is not availabe");
});

app.get("/*", (req, res) => {
  res.send("404 page,page is not available");
});

// vvvvvvvvvvvvvvvvimppppppppppppp
//   /*  means home (pachi) jeyvayeni yo bitrako response pathedincha
// * mens jevayeni

// Q what if /* code is just after the /about/*
//ans then /about/* will not work due to the first match in /*
//you will find the answer in 2-star-in-routing

// *******imp********
//see 2-star-in-routing.js
//the concept will be very clear
