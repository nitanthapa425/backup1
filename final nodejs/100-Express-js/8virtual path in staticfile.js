//making server
const express = require('express')
const app=express()
app.listen(4000,()=>{
    console.log("server is listening at port 4000")
})

const staticPath="./public"
// a program can have more than one static folder
app.use("/abc",express.static(staticPath)) 
app.use("/abc",express.static("./public2")) 
//here /abc is a virtual path.....this is done for security purpose
//to get image in browser we must do
//localhost:4000/abc/g7.jpg 

app.get("/",(req,res)=>{
    // to send file use sendFile     
    // note for sendFile path most be absolute use __dirname
   res.sendFile(__dirname+"/myhtml.html")
    

}) 