







const express =require("express")
//making server
const app =express();

//listening server
app.listen(8000,()=>{
    console.log("server is listening at localhost:8000")
})

//if you had use hbs view-engine then you have to set view-engine
app.set("view engine","hbs")

//changing the views path
//by default views path...... is the views folder path.... if you want to change views path then

// setting views path
app.set("views","./viewsfolderchange")

app.get("/",(req,res)=>{


    res.render("./home.hbs",{name:"nitan",sur:"thapa"})
    //when render is use by default it goes to views path and search home.hbs
    // no need to add ./
  
})