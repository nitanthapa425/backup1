import express from "express";
let expApp = express();
let port = process.env.PORT || "8000";

// this is the best routing practice
//define route
//and defined post ,get , patch, delete for that route

expApp
  .route("/")

  .all((req, res, next) => {
    console.log("i will execute for all post, get,patch, delete methode");
    next();
    //we most pass next methode to execute other other wise  we can not
  })
  .post((req, res) => {
    console.log("i am post");
    // not for post  first all methode will run and post methode will run

    res.status(201).json([{ name: "nitan", age: "27", married: false }]);
  })
  .get((req, res) => {
    console.log("i am get");
    res.status(201).json([{ name: "nitan", age: "27", married: false }]);
  })
  .patch((req, res) => {
    console.log("i am patch");

    res.status(201).json([{ name: "nitan", age: "27", married: false }]);
  })
  .delete((req, res) => {
    console.log("i am delete");

    res.status(201).json([{ name: "nitan", age: "27", married: false }]);
  });

expApp.listen(port, () => {
  console.log("server is listening at http://localhost:3000");
});
