





//req.query


// lets suppose url in browser is 
//localhost:8000/about?name=nitan&sur=thapa
//here name=nitan&sur=thapa is the query string
//? indicates the query start
//note name is key and nitan is value
//sur is key and thapa is value
//here e quotes is not use


//requiring express moudule
const express= require("express");
const app=express();

app.listen(8000,()=>{
    console.log("server is listening at 8000 port")
})


app.get("/about",(req,res)=>{


    console.log(req.query)
    // req.query gives the object
    //ie req.query={name="nitan",sur="thapa"}

    //to get name
    console.log(req.query.name)

    //to get sur
    console.log(req.query.sur);
    
   
    
   res.send()
   //res.end() can also be use
})