
const express=require("express")
const app=express()
port=8000
app.listen(port,()=>{
    console.log(`server is listening at ${port}`)
})


// let suppose url is
http://localhost:8000/about?name=nitan&sur=thapa&age=26
// ? indicate query string starts
//before ? they are routes
//after ?they are  querystring
//& is use to  seperate the parameters
//there are 3 parameter //name=nitan , sur=thapa ,age=25
app.get("/about",(req,res)=>{
    console.log(req.query)
   res.send("about page")
})
