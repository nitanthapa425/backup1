

const collection1=require("../models/models.js")
const express=require("express")
const router=express.Router();
const {call1,call2}=require("../controller/controller.js")

router.get("/call1",call1)
router.get("/call2",call2)

router.post("/push",async (req,res)=>{

    const document1= new collection1(req.body)

    const result=await document1.save()
    res.send(result)



})

module.exports=router