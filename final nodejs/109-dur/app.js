
//connecting with server
const express=require("express")
const server=express()
port=process.env.PORT||8000
server.listen(port,()=>{
    console.log("server is listening at port 8000")
})


//connecting with databases
require("./db/conn.js")

server.use(express.json())

const router=require("./routes/routes")


server.use(router)



