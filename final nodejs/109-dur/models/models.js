

const mongoose=require("mongoose")

const struc=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    date:{
        type:String,
        default:new Date().toLocaleDateString()
    }
})

//creating model
const collection1=new mongoose.model("collection1",struc)

module.exports=collection1