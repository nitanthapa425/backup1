//here we only focuson authentication

//to store cookies in browser we do not need any dependencies
//but to get cookies form browser we noee dependiees ie cookie-parser
//for this npm i cookie-parser
// then const cookieParser=require("cookie-parser")
//then server.use(cookieParser())
//then to get token valure token=req.cookies.cookiesname

//creating tokens

//to generate token first lets require jwt
const jwt = require("jsonwebtoken");
//making server and connecting with server
const express = require("express");

// requiring cookie-parser to get cookies (token) from browser
const cookieParser = require("cookie-parser");
//note you have to use cookie parser as middleware
const server = express();
port = process.env.PORT || 8000;
server.listen(port, () => {
  console.log("server is listening at port 8000");
});

//to use form data by res.body
server.use(express.urlencoded({ extended: false }));

//to get token from cookies of browser we have to use cookieParser as middleware
server.use(cookieParser());

//making database and connecting with database
//requiring mongoose
const mongoose = require("mongoose");
//connecting nodejs with database2
mongoose.connect("mongodb://localhost:27017/authentication", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});

//creating schema for macking collection
const formSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  conformedpassword: {
    type: String,
    required: true,
  },

  //    for authentication purpose
  //tokens is array of object
  //if you are doing by cookies then dont do it
  tokens: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],
});

//creating collection
const collection1 = new mongoose.model("colection1", formSchema);

//setting vieenging
server.set("view engine", "hbs");
// giving path of views folder
server.set("views", "./templates");

//register form
server.get("/register", (req, res) => {
  res.render("./register.hbs");
});
//login form
server.get("/login", (req, res) => {
  res.render("./login.hbs");
});

server.get("/clearcookies", (req, res) => {
  // cookie2 is clear
  res.clearCookie("cookie2");
  res.send("cookie clear");
});

//getting cookies
server.get("/getcookies", async (req, res) => {
  // console.log(req.cookies.cookie2)
  //req.cookies.cookie2 gives the token
  token = req.cookies.cookie2;
  try {
    const obj = jwt.verify(token, "mynameisnitanthapaandiliveingagalphedi");
    res.send(obj);
    //getting the document of given id
    const result = await collection1.find({ _id: obj._id });
    console.log(result);
  } catch (error) {
    res.send(error);
  }
});
server.post("/register", async (req, res) => {
  try {
    const document1 = new collection1({
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      conformedpassword: req.body.conformedpassword,
      // till now there is not token thus we don not mention
      //but in place of that token it will tak []..emptyp array
    });
    // imp till now
    // document1={.......,tokens=[]}
    ///it means we do not give token thus token will take empty array
    //before saving lets generete token for each registerign document

    const token = await jwt.sign(
      { _id: document1._id },
      "mynameisnitanthapaandiliveingagalphedi",
      {
        expiresIn: "2 minutes", //or "2 seconds"
      }
    );

    document1.tokens = document1.tokens.concat({ token: token });
    //    note imp in any object (even in documen1 which is objct one id is automatically placed)
    //thues for token alson one id will be automatically place
    //ie tokens=[{_id:...,token:token}]
    // or// document1.tokens.push({token:token})
    // or// document1.tokens[0]={token:token}
    //    console.log(document1)

    // lets add tokens in cookies
    //not to use cookies we dont need to download extra things
    res.cookie("cookiename1", token, {
      expires: new Date(Date.now() + 300000),
      // httpOnly:true
      //note if you don menthion expires then
      //by default the cookies will we disappear within in a second
      //ie if you close brower and check then their will not be the given cookie
    });

    // lets save document if password matches
    if (req.body.password === req.body.conformedpassword) {
      const result = await document1.save();
      res.send(result);
    } else {
      res.send("password doesnot matches");
    }
  } catch (e) {
    res.send("error has occured");
  }
});

//login home page only if
//the email and password matches withe the email and password of the registerform (in database)
server.post("/login", async (req, res) => {
  //getting the password and email form the login form
  const { password, email } = req.body;

  //checking weather the given email exist in database in given collection formdatabase

  try {
    //or use findOne({email:email}) which gives the first match
    const result = await collection1.find({ email: email });
    // *imp////////////////////////////////////////////////////////////////////////////////////////////
    //generating token and adding the token in cookies
    //generaating token for the login the sepecific user
    const token = await jwt.sign(
      { _id: result[0]._id.toString() },
      "mynameisnitanthapaandiliveingagalphedi",
      {
        expiresIn: "2 minutes",
      }
    );
    //adding cookies
    res.cookie("cookie2", token, {
      expires: new Date(Date.now() + 500000),
    });
    //   console.log(result[0].tokens[0].token)
    //   console.log(token)
    if (result.join("")) {
      //   simply if email exist
      //if pasprot matches
      if (password == result[0].password) {
        //   accessing home page only if login password and database password matches
        res.render("./home.hbs");
      } else {
        res.send("sorry password doesnot matches");
      }
    } else {
      //if email dont exist
      res.send("sorry no email in server");
    }
  } catch (e) {
    res.status(404).send("invalid email or password sure");
  }
});
