const add = (a, b) => a + b;

const sub = (a, b) => a - b;

const mul = (a, b) => a * b;

const names = "nitan";

module.exports = { add, sub, mul, names };
// it means we can use add,sub mul,names in other file where it is imported
// curly bracket is use because more than one is exported
