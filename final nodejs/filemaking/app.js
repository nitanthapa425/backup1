const express = require("express");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

///////////////////////////////////////////imp//////////////////////
// defining static path
app.use(express.static("./public/upload"));
port = 8000;
app.listen(port, () => {
  console.log(`server is listening at ${port}`);
});

// to upload file
//we don not upload file in moono db server
//we upload it in a system
//in moongo db server we pass the image filename

//huse we need two main thing
// destination and filename
//in destination we give the folder name where the file has to be uploaded
//in mongo db server we send the filename

// / *******************************
//for multiple filew
//replace res.file by res.files
//single by array
//and add multiple in img tag of home.hbs

// to upload we have to use multer packeage npm i multer
const multer = require("multer");

// to use path lets export path name
//note to use multer no need to export path
const path = require("path");

//lets define multer
const storage = multer.diskStorage({
  destination: "./public/upload",
  // it is goo syntax
  // vvvvimp***********************************
  //note in destination   ./ means the main folder where the all project lies
  //ie 111-file-uploac
  //note if "./public/upload" folder is not available then it will automatically make by this code
  //note here  there is no role of any staticpath ie independent of staticpath
  //note if  destination :"../public/upload"
  //then then public/upload folder will be make outside the 111-file-upload

  filename: (req, file, cb) => {
    console.log(file);
    // file is just like to req.file in  register
    // it will show {
    //     fieldname: 'imagesfile',
    //     originalname: 'nitan.jpg',
    //     encoding: '7bit',
    //     mimetype: 'image/jpeg'
    //   }
    // other important thing...just to know
    console.log(path.extname(file.originalname));
    // to use pathe you have to import path
    // it will give the extension part
    // .jpg
    cb(null, Date.now() + file.originalname);
    // null requresent error is null
    //Date.now()+file.originalname means filename willl be nowdate and itsorginalnamae
    //note Date.now()is given to avoid conflict when other same file name is added
  },
});
const upload = multer({
  storage: storage,
  // no need to do filter (you can skip filter part) but for knowledge
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype == "image/jpeg" ||
      file.mimetype == "image/jpg" ||
      file.mimetype == "image/JPG" ||
      file.mimetype == "image/JPEG" ||
      file.mimetype == "image/svg"
    ) {
      cb(null, true);
      // it measn pass such type of file
    } else {
      cb(new Error("File is not supported"), false);
      //false meanse doesnot pass
    }
  },
  limit: {
    fileSize: 1024 * 1024 * 2,
    // it meas pass ony those file whose file is less or equal to 1024*1024*2
  },
}).single("imagesfile");
// "imagesfile" it is from the name of the form

// for multiple file to appload
//place array("imagesfile", maximum no of file to send)
//array("imagesfile", 5)

app.post("/", upload, (req, res) => {
  // console.log(req.body)

  console.log("helo");
  res.send("got it");
});
