

// here we will lear
//how to generate token from combination of object and secret key

//and verify weather the given token is made from the secret key


//to use jwe you have to download
//npm i jsonwebtoken
//requireing jwt
const jwt=require("jsonwebtoken")
//creating token
const createverifytoken = async ()=>{
    //just think sign as signeture
    //syntax
    // const token=await jwt.sign({key:value},"secret key")
    // note the secret key must be at least 32 character note the longer the better
    //it is recommended the place secretkey in the .env file

//imp************************    
    //note the token will be made form
    //secret key and any unique key and value pair
    const token=await jwt.sign({name:"1234"},"mynameisnitanthapaandiliveingagalphedi",{
        expiresIn:"2 minutes"//or "2 seconds"

        //exporesIn :"2 minutes"..means the token will be expire in 2 miniutes
        //you might be thinikin  from where for this we will learn later
    })
    console.log(token)

    //verifing token
    // syntax
    //const object=await jwt.verify(token,secretkey)
    ///it check weather the token is made form the given secretkey
    //if the token is made form to given secretekey then it will gives the objectpart from which the token is made
    //ie in the given example it will give {name:"1234",exkp:1615632898 }
    //if the token doesnot made from the secret key then it will show error
    const obj=await jwt.verify(token,"mynameisnitanthapaandiliveingagalphedi")
    console.log(obj)

}
createverifytoken();