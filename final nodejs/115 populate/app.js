const express = require("express");
//requiren object id

const server = express();
server.listen(8000, () => {
  console.log("serveri is listening at port 8000");
});
//to use req.body from body
server.use(express.json());
// server.use(express.urlencoded())

//connectint nodejs to database
const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
mongoose
  .connect("mongodb://localhost/populate", {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log("connected to the database");
  });

//creating schema // for product

const productSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  des: {
    type: String,
    required: true,
  },
});

//creating schema for order

const orderSchema = mongoose.Schema({
  orderItem: {
    type: ObjectId,
    // vvimp**********************************
    // imp ref contain the collection name of database note it must be from database
    ref: "product",
    //once ref is product then then it contain ithe id from product collection only
    // vvvvvvvvvvvvvvvvvvvviiiiiiiiiiiiiiiiiiiiiimmmmmmmmmmmmmmmmmmpppppppppppppppp
    // note product is collection but note in database it will be products but note only use product because we do not mention the s which is added
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
});

//creating model or collection
const product = mongoose.model("product", productSchema);

server.post("/product", async (req, res) => {
  const document = new product({
    name: req.body.name,
    price: req.body.price,
    des: req.body.des,
  });

  const result = await document.save();

  res.send(result);
});

//creating modeel for orderItem
const order = mongoose.model("order", orderSchema);

server.post("/order", async (req, res) => {
  const document = new order({
    // imp note the orderItem must be one of th product it
    orderItem: req.body.orderItem,
    // note in databas only the id is store in orderItem
    //but you have power to show all information of that object point by that id
    quantity: req.body.quantity,
  });

  const result = await document.save();
  res.send(result);
});

// getting the order

server.get("/order", async (req, res) => {
  // if you do await order.find() then in place of orderItem you will get only valu as id
  // const result= await order.find()
  // /but you have power to show all information of that object point by that id
  // if you want to get all detail of  that object point by that id then use populate(nameofthoskeywhichholdid)
  //ie populate("orderItem")
  //it means the value of orderItem is object which hold all information of that id .....

  // const result= await order.find().populate("orderItem")

  // but if you want ot show only price information then do
  // const result= await order.find().populate("orderItem","price ")
  // note name,des informmation is not show

  // but if you want ot show only price and name information then do
  const result = await order.find().populate("orderItem", "price  name ");
  // note name,des informmation is not so

  res.send(result);
});
