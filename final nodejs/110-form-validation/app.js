//here we will learn to make
//registation form.....sending the data of form to server
//login form
//login home page if the password matches the password  in database of register form
//hashing ....placing hashed password in databpase in place of real password
//
//for validator

//making server and connecting with server
const express = require("express");
const server = express();
port = process.env.PORT || 8000;
server.listen(port, () => {
  console.log("server is listening at port 8000");
});
//*******************imp********************* */
// server.use(express.json) is use to use res.body in postman
// server.use(express.json())

//server.use(express.urlencoded({extended:false})) is use to use res.body form form body
//to use form data
server.use(express.urlencoded({ extended: false }));

const { body, validationResult } = require("express-validator");

////////////////////////////////////////hasing////////////////////////////////////////////
// for hasing purpose lest import
//note hasing is one of process ie can be encrypt but can not be decrypt
//requiring hasing by using bcryptjs to use bcryptjs you have to
//instal npm i bcryptjs
const dcryptjs = require("bcryptjs");

// to convert any string to hash code
//const hashcode=await dcryptjs.hash("string",number from 1 to 12 but 10 is better or use higher )

//note we can convert any string to hash cod
//but we can not conver hash code to string which is very goood for security purpose

//to compare weather the string hash code is given hash code
//const bool=await dcryptjs.compare("string","hashcode")

//making database and connecting with database
//requiring mongoose
const mongoose = require("mongoose");
//connecting nodejs with database2
mongoose.connect("mongodb://localhost:27017/formdatabase", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: true,
});

//creating schema for macking collection
const formSchema = new mongoose.Schema({
  // imp username is not same as the name
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  conformedpassword: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    required: true,
  },
  likemilk: {
    type: String,
    default: "false",
  },
  comment: {
    type: String,
    required: true,
  },
});

//creating collection
const collection1 = new mongoose.model("colection1", formSchema);

//setting vieenging
server.set("view engine", "hbs");
// giving path of views folder
server.set("views", "./templates");

//register form
server.get("/register", (req, res) => {
  res.render("./register.hbs");
});
//login form
server.get("/login", (req, res) => {
  res.render("./login.hbs");
});
server.post(
  "/register",
  body("email", "must be valid email").isEmail(),
  body("username", "must not be empty").isEmpty(),
  body("password", "must not be empty").isEmpty(),
  async (req, res) => {
    const errors = validationResult(req);
    console.log(errors);
    res.render("./register.hbs");

    // try
    // {
    //     //lets perform hasing for password
    //     // const hashpassword=await dcryptjs.hash(re1.body.password,10)
    //     //note in place of 10 you can use 11,12,1,2,...but 10 is best
    //     //but note as you increas the number it will be more secure and takes more time
    //     const hashpassword=await dcryptjs.hash(req.body.password,10)
    //     const hashconformedpassword=await dcryptjs.hash(req.body.conformedpassword,10)
    //     // res.write(hashpassword)
    //     // res.write(hashconformedpassword)
    //     // res.send()

    //         const document1=new collection1({
    //             // we make mistke by placing re1 in place of req
    //             // re1.body.username...here this username is the name
    //             username:req.body.username,
    //             email:req.body.email,
    //             password:hashpassword,
    //             conformedpassword:hashconformedpassword,
    //             gender:req.body.gender,
    //             country:req.body.country,
    //             likemilk:req.body.likemilk,
    //             comment:req.body.comment
    //         })

    //         // lets save document if password matches
    //         if(req.body.password===req.body.conformedpassword)
    //         {
    //             const result = await document1.save()
    //             res.send(result)
    //         }
    //         else
    //         {
    //             res.send("password doesnot matches")
    //         }

    // }
    // catch(e)
    // {
    //   res.send("error has occured")
    // }
  }
);

//login home page only if
//the email and password matches withe the email and password of the registerform (in database)
server.post("/login", async (req, res) => {
  //getting the password and email form the login form
  const { password, email } = req.body;

  //checking weather the given email exist in database in given collection formdatabase

  try {
    //or use findOne({email:email}) which gives the first match
    const result = await collection1.find({ email: email });
    if (result.join("")) {
      //   simply if email exist
      //syntax
      //   const mat=await dcryptjs.compare("string","hashcode")
      //this check weather the string hascode is same as hashcode given
      const matches = await dcryptjs.compare(password, result[0].password);
      //   here password is come from the login page
      //result[0].passowrd is come form the server which is hashed password
      //not imp*********************888888**********
      //same string has different hashed code

      if (matches) {
        //   accessing home page only if login password and database password matches
        res.render("./home.hbs");
      } else {
        res.send("sorry password doesnot matches");
      }
    } else {
      //if email dont exist
      res.send("sorry no email in server");
    }
  } catch (e) {
    res.status(404).send("invalid email or password sure");
  }
});
