//firs we have to make server then connect nodejs with server
//then we have to make database then connect nodejs with database

//and the document are created,update read delete
//according to the request
//ie docuement code are wirtten on request


//in this file we will lear all thing of resp api
// post,get patch,delete
//router

//making server
const express=require("express")

const server=express()



// /***************imp***************** */
// to use req.body you muse use server.use(express.json)
//note use is a middleware
server.use(express.json())

server.listen("8000",()=>{
    console.log("server is listening at port number 8000")
})
//connecting to database
const mongoose=require("mongoose")

//connecting with database
// you have done mistake by using locol instead of local
mongoose.connect("mongodb://localhost:27017/apidatabase",{
    useNewUrlParser: true,
useUnifiedTopology: true,
useCreateIndex:true,
useFindAndModify:true 
}).then(()=>{console.log("connected to database apidatabase")})

//creating schema

const struc=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    date:{
        type:String,
        default:new Date().toLocaleDateString()
    }
})

//creating model
const collection1=new mongoose.model("collection1",struc)


//adding document by the help of mongooes

// const document1=new collection1({
//     name:"nitan"
// })
// collection1.insertMany([document1])


//adding document by the help of post method
server.post("/",(req,res)=>{


    // imp***********
    //while adding document in server
    //it is recommend that in post may
//tik body tik raw and tik json and add data int the form of json

// when request is send from post method by the postman
//then this server git the response



// vvimp
//in postman
//while making document we can pass only one document at a time
// the document must be only one

// note the document is define in post method
    const document1=new collection1(req.body)
    // imp*********************
    //to use req.body you  muse do server.use(express.json())
// always remember document1 always hold one document
    const passapi =async ()=>{
//to pass document we use document1.save()
// difference         betetween mongoooes            and api while sending data is
//mongooes use collection.insertMany([documen1,documen2])
//but by using post man
//document are place in severr by document.save() note document.save() is a promise
    
try
{

    const result= await document1.save()
            res.status(201).send(result)
            ///if document is save successfully in server then we send 201 stauts
            //generally for post and updatate 201 stauts is use
}
catch(e) 
{
    res.status(400).send("the document is not save in server due to some problem")
}
        

    }

    passapi();

//  res.send(document)

})







//getting all data from the database by the help of api

server.get("/about",async (req,res)=>{
    //getting is same as getting data by mongooes
try
{

    const  result= await collection1.find()
    res.status(201).send(result)
}
catch(e)
{
    res.status(500).send("data can not be get from server due to some error")
}

})

//getting one data according to url
server.get("/about/:check/:cd",async (req,res)=>{
    //getting is same as getting data by mongooes
try
{

    //in place of :check you can enter any thing
    //in place of :cd you can entered any

    // imp to use param no need to requir
    //re1.params={check:"entered value",cd:"entered value"}
    const check=req.params.check
    // cd is take just for learnig purpose
    const cd=req.params.cd
    console.log(req.params)
    

    const  result= await collection1.find({_id:check})
    res.status(200).send(result)
    //status(200) if server easily give data
}
catch(e)
{
    res.status(500).send("data can not be get from server due to some error")
    // status 500 is because if server can not  give data
}



})


//updating
server.patch("/update",async (req,res)=>{

        // imp***********
    //while updating document in database
    //it is recommend that in post may
//tik body tik raw and tik json and add data int the form of json

    //updating a specifie object
    //if you want to update according to url then place :n or any thing
try
{

    const  result= await collection1.updateMany({name:"hari"},{$set:req.body})
    res.send(result)
}
catch(e)
{
    res.send("data can not be get from server due to some error")
}

})

//deleting
server.delete("/delete",async (req,res)=>{
    //delete a specifie object
    //if you want to delete according to url then place :n or any thing
try
{

    const  result= await collection1.deleteMany({name:"subum"})
    res.send(result)
}
catch(e)
{
    res.status(500).send("data can not be get from server due to some error")
}

})


// router
//to use router 
//first we have to define it
//not need to download it from npm
const router=new express.Router()

//using middleware
//to use router we must use middleware
server.use(router)

//third
// till now we have use server.get ,server.post ,server.patch,server.delete
///now this can be also done by router.get,router.post,router.patch,router.delete

router.get("/call",(req,res)=>{

    res.send("server.get is replace by router.get")

})









