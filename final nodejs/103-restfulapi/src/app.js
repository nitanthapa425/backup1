// vvvimp*************************************
//note in browser the get http is use
// ie browser use get request
//ie in browser you can only get the resopnce send by get method
//ie you can not get the response send by post,patch ,delete method
//to get response send by post,patch,delete methode use postman application(note postmal also give get response)
const express=require("express")

const server=express()
// 8888**************************imp*************
// const port=process.env.PORT||8000
// not need to do const port=process.env.PORT||8000..but it is said to be good practice
//you can also do port:8000
const port=8000;



server.listen(port,()=>{console.log(`server is listening at ${port} port`)})


//if the url is home and http method is get the give response "hello i am in home page"
server.get("/",(req,res)=>{
    res.send("hello i am in home page")
})
//if the url is home/about and http method is get the give response "hello i am in about page"
server.get("/about",(req,res)=>{
    res.send("hello i am in about page")
})
//if the url is home/about and http method is post then give response " i am in about page from post method"
server.post("/about",(req,res)=>{
    res.write(" i am about page form post method")
    res.send()
})
//if the url is home/student and http method is post then give response "i am from post man"
server.post("/students",(req,res)=>{
    res.write(" i am from post man")
    res.send()
})
//if the url is home/delete and http method is delete then give response "i am from delete man"
server.delete("/delete",(re1,res)=>{
    res.write("i am form delet request")
    res.send()
})
//if the url is home/patch and http method is patch then give response "i am from patch request"
server.patch("/patch",(re1,res)=>{
    res.write("i am form patch request")
    res.send()
})
//if the url is home/about and http method is patch then give response "i am from about from patch request"
server.patch("/about",(re1,res)=>{
    res.write("i am form about from patch request")
    res.send()
})


  