


const mongoose=require("mongoose")
const {ObjectId}=mongoose.Schema
// to use ObjectId 
//creating schema
const productSchema=new mongoose.Schema({

        product_name:{
        type:String,
        required:true,
        trim:true
    },
    product_price:{
        type:Number,
        required:true,
        trim:true

    },
    product_quantity:{
        type:Number,
        required:true,
        trim:true

    },
    category:{
        type:ObjectId,
        // required:true,
        ref:'categoryCollection'
             // imp ref categoryCollection means
        //the cotegory only contain the object if of categoryCollection only
        //in postman category is define as
        // category:"604f2cfa0f30b90fa89180fe"

        // if ref:"categoryCollection " then it will take id object of any collectionn

    },
    product_description:{
        type:String,
        required:true,
        minLength:20
    },
    product_rating:{
        type:Number,
        default:0,
        maxLength:5
    },
    product_image:{
        type:String,
        required:true,
        trim:true
    }



},{timestamps:true})

//creating collectiong
const productcollection=new mongoose.model("productcollection",productSchema)

module.exports=productcollection





// const mongoose=require("mongoose")
// const {ObjectId}=mongoose.Schema
// const productSchema=new mongoose.Schema({

// },{timestamps:true})

// const Product=mongoose.model('Product',productSchema)

// module.exports=Product