const categoryCollection = require("../model/categoryModel");

const categoryfun = async (req, res) => {
  try {
    const document = new categoryCollection({
      name: req.body.name,
    });

    const result = await document.save();
    res.status(201).send(document);
  } catch {
    res.status(400).send("can not save to the database");
  }
};

module.exports = categoryfun;
