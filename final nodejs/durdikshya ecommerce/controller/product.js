const productcollection = require("../model/productModel");

const categoryCollection = require("../model/categoryModel");

exports.productfun = async (req, res) => {
  console.log("i will be call after middleware");
  try {
    const document = new productcollection({
      product_name: req.body.product_name,
      product_price: req.body.product_price,
      product_quantity: req.body.product_quantity,
      category: req.body.category,
      product_image: req.file.path,
      product_description: req.body.product_description,
      product_rating: req.body.product_rating,
    });

    const result = await document.save();
    res.status(201).send(document);
  } catch {
    res.status(400).send("can not save to the database");
  }
};

//To get All Product
exports.getAllProducts = (req, res) => {
  productcollection
    .find()
    .populate("category", "name")
    // show name only(form categorySchema) in category
    // note .populate("category is the key in productschema","name is the key of categorycollectiond")
    // here name key is provided to show name only in the category
    //but note id is automatically show
    .exec((error, products) => {
      if (error || !products) {
        return res.json({ error: "products not found" });
      }
      res.json({ products });
    });
};
