
const multer=require("multer")
const path=require("path")
const fs=require('fs')

const storage=multer.diskStorage({
    destination:(req,file,cb)=>{
        let fileDestination='public/uploads/'
        // dont use ./..

        //check if directory exists
        if(!fs.existsSync(fileDestination))
        {
            fs.mkdirSync(fileDestination,{recursive:true})
            //recursive:true creates parent folder as well as sub folders
            cd(null,fileDestination)
        }
        else 
        {
            cd(null,fileDestination)
        }
    },

    filename:(req,file,cb)=>{
        //path.basename('/foo/bar/baz/asdf/quux.html')
        //Returns:'quuz.html'

        //path.basename('/foo/bar/baz/asdf/quux.html','.html');
        //Returns:'quux'

        let filname=path.basename(file.originalname,path.extname(file.originalname))
        let ext=path.extname(file.originalname)
        cb(null,filename+"_"+Date.now()+ext)

    }


})

let imageFileFilter=(req,file,cb)=>{

    if(!file.originalname.match(/\.(jpg|jpeg|png|svg|JPG|JPEG|PNG|SVG)$/))
    {
        return cb(new Error('you can upload an image file only'),false)
    }

    cb(null,true)

}

let upload =multer({
    storage:storage,
    fileFilter:imageFileFilter,
    limit:{
        fileSize:1024*1024*2
    }
})

module.exports=upload