
const express= require("express");
const {productfun} = require("../controller/product");
const {getAllProducts} = require("../controller/product");
const upload = require("../middleware/file-upload");
const productvalidator = require("../validator/productvalidator");
const router=express.Router();
// ,productvalidator
// , upload.single('product_image')
router.post("/postproduct" ,productfun)
router.get('/getproduct',getAllProducts)
// here productvalidator is a middleware
// note first productvalidator will run then productfun will run
//but note to go to productfunc  we have to use next()
module.exports=router;

// module.exports=router;


// const express=require("express")
// const productfun = require("../controller/product")
// const router=express.Router()

// router.post('/postproduct',productfun)

// module.exports=router