const { parse } = require("path");
const path = require("path");

// finding the folder name or whole working file name path

console.log("directory name", __dirname); //C:\Users\acer\Desktop\all programs\final nodejs\5-path-module   ...gives whole path of dir where file lies
console.log("file name", __filename); // C:\Users\acer\Desktop\all programs\final nodejs\5-path-module\1-parse-method.js ...gives whole path including file name

//path module

//finding dir, filename, ext of  any path
dicpath = path.parse(
  "C:UsersNitanthapaDesktop/nodejs-tut/5-path-module/1-parse-method.js"
);
console.log(dicpath);

// path module help to valdate different file
//supose if you want to upload .js file only
// path.parse("any file total location")
// it gives the dictionaly
// {
// no need to tension for begineer just know ext paramenter
//     root:"root location file ie start location of  the file"
//     dir:"..."
//     base:"gives the location of all file"
//     ext:".js or .txt or .py or .html"  //note if .js.mjs it will give .mjs
//     name:"file name"
// }
// {
//     root: 'C:',
//     dir: 'C:UsersNitanthapaDesktop/nodejs-tut/5-path-module',
//     base: '1-parse-method.js',
//     ext: '.js',
//     name: '1-parse-method'
//   }

// console.log(dicpath.ext);
// console.log(dicpath.name);
// console.log(dicpath.base);
// console.log(dicpath.root);
// console.log(dicpath.dir);

console.log(path.parse(__filename));
//gives
// {
//     root: 'C:\\',
//     dir: 'C:\\Users\\acer\\Desktop\\all programs\\final nodejs\\5-path-module',
//     base: '1-parse-method.js',
//     ext: '.js',
//     name: '1-parse-method'
//   }

//instead of using path.parsh( ) methode you can use path.dirname(path) path.filename(path)  path.ext (path) but parth.parse is best

//parse.join //methode

console.log(path.join(__filename, "/a", "b")); ///C:\Users\acer\Desktop\all programs\final nodejs\5-path-module\1-parse-method.js\a\b
//note you cnd add path by /a or a only
console.log(path.join(__filename, "a", "b", "..")); //C:\Users\acer\Desktop\all programs\final nodejs\5-path-module\1-parse-method.js\a
//vvvimp just know .. remove one /component he   if /a/b  it remove /b and result is /a

//to remove two /component
console.log(path.join(__filename, "a", "b", "..", "..")); ///C:\Users\acer\Desktop\all programs\final nodejs\5-path-module\1-parse-method.js
