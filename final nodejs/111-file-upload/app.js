

//making server and connecting with server
const express=require("express")
const server=express()
port=8000
server.listen(port,()=>{
    console.log("server is listening at port 8000")
})
//*******************imp********************* */
// server.use(express.json) is use to use res.body in postman
// server.use(express.json())

//server.use(express.urlencoded({extended:false})) is use to use res.body form form body
//to use form data
server.use(express.json())
server.use(express.urlencoded({extended:false}))

///////////////////////////////////////////imp//////////////////////
// defining static path
server.use(express.static("./public/upload"))
// vvvimp note express.static is not use for multer or to upload and get file
//but it is use because the images and css are seen in staticpath
//here ./public/upload is made static because our image are in upload





//making database and connecting with database
//requiring mongoose
const mongoose=require("mongoose")
const upload = require("./middleware/upload")
//connecting nodejs with database2
mongoose.connect("mongodb://localhost:27017/fileuploaddatabase",
{ useNewUrlParser: true,
useUnifiedTopology: true,
useCreateIndex:true,
useFindAndModify:true })


//creating schema for macking collection
const formSchema=new mongoose.Schema(
    { 
        // imp username is not same as the name 
       username:{
           type:String,
           required:true
           
       },
       image:{
           type:String,
           required:true
       }
 

    }
)

//creating collection
const collection1=new mongoose.model("colection1",formSchema)



//setting vieenging
server.set("view engine","hbs")
// giving path of views folder
server.set("views","./templates")

//register form
server.get("/register",(req,res)=>{
    res.render("./register.hbs")

})

server.post("/register",upload,async (req,res)=>{
    // if enctype is written in the form
    //then multer() middlewar must be use otherwise it will show error

    try 
    {

           
        console.log(req.file)
        // {
        //     fieldname: 'imagesfile',
        //     originalname: 'nitan.jpg',
        //     encoding: '7bit',
        //     mimetype: 'image/jpeg',
        //     destination: './public/upload',
        //     filename: '1616058251507nitan.jpg',
        //     path: 'public\\upload\\1616058251507nitan.jpg',
        //     size: 72645
        //   }

        console.log(req.body)
        // [Object: null prototype] { username: 'nitan thapa' }
       
            const document1=new collection1({
                // we make mistke by placing re1 in place of req
                // re1.body.username...here this username is the name


                username:req.body.username,
                // for other we use req.body
                //but for file we use req.file
                image:req.file.filename
                // it is better to use filename instead of path

                // ******************imp******************
                //note 
                // req.body.title gives the tile while you enter the file


                // for multiple files 
                //plase req.files in place of files







               
            })

            
                const result = await document1.save()
                console.log(result)
                res.send("send suss")
                // res.render("./home.hbs",{img:result.image})

            

    }
    catch(e) 
    {
      res.send("error has occured")
    }

})







