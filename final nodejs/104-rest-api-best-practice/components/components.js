


const express=require("express")
//no need to write new
const router=new express.Router()



//third
// till now we have use server.get ,server.post ,server.patch,server.delete
///now this can be also done by router.get,router.post,router.patch,router.delete

router.get("/call",(req,res)=>{

    res.send("i am called form get")

})
router.post("/call",(req,res)=>{

    res.send("i am called from post")

})

module.exports=router