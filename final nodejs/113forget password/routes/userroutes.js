const express=require("express");
const jwt=require("jsonwebtoken")
const hbs=require("hbs")
const nodemailer = require("nodemailer");
const collection1 = require("../models/usermodels");
//to use router()
const userrouter=express.Router();


userrouter.get("/register",(req,res)=>{
    res.render("./register.hbs")
})
userrouter.post("/register",async(req,res)=>{
    console.log(req.body.email)

    try 
    {
            //creating document 
    const document1=new collection1({
        username:req.body.username,
        email:req.body.email,
        password:req.body.password

    })
    console.log(document1)

    const result=await document1.save()
    console.log("register succsfully")
    res.send(result)

    }
    catch(err)
    {
        res.send("cannot save to the database")
    }
   

})
userrouter.get("/login",(req,res)=>{
    res.render("./login.hbs")
})
userrouter.post("/login",async (req,res)=>{
    try 
    {

  const  {email,password}= req.body

    const result= await collection1.find({email:email})
    console.log(result)
    if(result.length==0)
    {
        res.status(404).send("email does not exist")
    } 
    else
    {
        if(password==result[0].password)
        {
            console.log("login succsfully")
            res.send(result)
        }
        else 
        {
            res.send("password doesnot matches")
        }

    }
}
catch(err)
{
    res.send("error has occur")
}



    
})


userrouter.get("/resetpassword",(req,res)=>{

    res.render("./resetpassword")

})
userrouter.post("/resetpassword",async (req,res)=>{
    try
{
    const {email}=req.body
    const result=await collection1.find({email:email})
    if(result.length==0)
    {
        res.send("email does not exist")
    }
    else 
    {

        // *********************** for one time login
        const secretkey=process.env.SECRETKEY+result[0].password
        // ********************for one time logine

        // generating token
        const token=await jwt.sign({email:email},secretkey,{expiresIn:"10m"})
        // it means after .. the token is expire ie the token does not work
        console.log(token)
        

async function main() {
    try
    {


//sending mail
  let transporter = nodemailer.createTransport({

    service:"gmail",
    auth: {
      user:`nitanthapa425@gmail.com`, // generated ethereal user
      pass: `15110425`, // generated ethereal password
    },
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'nitanthapa425@gmail.com', // sender address
    to: "nitanthapa123@gmail.com", // list of receivers
    // to: "nitanthapa123@gmail.com,sharmilathapa422@gmail.com", // for multiple receivers
    subject: "Hello ✔", // Subject line
    text: `your password is ${result[0].password} but if you want to reset then click http://localhost:3000/rpassword/${token}/${email}`, // plain text body
// /**********************imp************* */
// email is send for the purpose of one time login
//because to ge the password
  });
  res.send(`your password is ${result[0].password} but if you want to reset then click http://localhost:3000/rpassword/${token}/${email}`)

    }
    catch(err)
    {
        console.log("error has occured")
    }

}

main()
    }
}
catch(err)
{
res.send("error has occured")
}




})
userrouter.get("/rpassword/:token/:email",async (req,res)=>{
    const {token,email}=req.params
    const result=await collection1.find({email:email})
    
    const secretkey=process.env.SECRETKEY+result[0].password
try 
{
    const payload=jwt.verify(token,secretkey)
    console.log(payload.email)
   
    res.render("rpassword",{email:payload.email})
}

catch(err)
{
    console.log(token)
    res.send("token is expired so send verification link again")
    
}

 
})
userrouter.post("/rpassword/:email",async (req,res)=>{
    const {email}=req.params
    const {password}=req.body
    const result=await collection1.updateMany({email:email},{$set:{password:password}})
    // const result=await collection1.updateMany({no:11},{$set:{no:12}})
    res.send(result)
})

module.exports=userrouter;

