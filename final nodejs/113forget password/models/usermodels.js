const mongoose = require('mongoose')
const registerSchema=new mongoose.Schema({
    username:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    }

})

//creating collection
const collection1=new mongoose.model('collection1',registerSchema)

module.exports=collection1