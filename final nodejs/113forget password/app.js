
// **********************************
// for more information see yoursTruly videos for forget password
//requiring dotenve to use .env
require("dotenv").config()

// connecting to the database
require("./db/conn")

//making server and connecting with server
 const express = require('express')
 const userrouter=require("./routes/userroutes")
 const server = express()
 const port=process.env.PORT
 server.listen(port,()=>{
     console.log(`server is listening at port ${port}`)
 })

 server.use(express.urlencoded({extended:false}))

 //setting viewengine
 server.set("view engine","hbs")
 //giving the path of view enginee
 server.set("views","views")


 //server //routing
 server.use(userrouter)

