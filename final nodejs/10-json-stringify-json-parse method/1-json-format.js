


// json...it stands for javascript object notation
// we make json file by .json
// note we can not call json element as it is string unlike object

// 1)

obj={name:"nitan",sur:"thapa",age:26,}

console.log(obj);


//json.stringify(obj)  is a json ie javascript object notation

console.log(JSON.stringify(obj));
// {"name":"nitan","sur":"thapa","age":26}
//in object key is not written inside double quotes........object is object
//in json key is writen inside double quotes.............and json is strign
console.log(typeof(JSON.stringify(obj)));
// json is a string


// 2)
arr=[{name:"nitan",age:26},{name:"jackma",age:52},{name:"billgates",age:67},{name:"chaudhari",age:58}]

// Q  this a json format answer is no
//because in json formate the key must be wrap by double quotes


//converting arr in json format
arr1= JSON.stringify(arr)
//  JSON.stringify(arr)  does two thing
//first it make arr1 as string by placcing whole arr1 in side ""
//second it wrap key by ""
console.log(arr1);
console.log(typeof(arr1));

// '[{"name":"nitan","age":26},{"name":"jackma","age":52},{"name":"billgates","age":67},{"name":"chaudhari","age":58}]'
// string

//converting  arr1 to simple list
arr2=JSON.parse(arr1)
console.log(arr2);
console.log(typeof(arr2))
// [
//     { name: 'nitan', age: 26 },
//     { name: 'jackma', age: 52 },
//     { name: 'billgates', age: 67 },
//     { name: 'chaudhari', age: 58 }
//   ]
//   object


num=3;
//converting to it into string
s=JSON.stringify(num);
// it simply convert num to string
console.log(s);
console.log(typeof(s));
// 3
// string

//converting it to orginal form ie number
s1= JSON.parse(s)
console.log(s1);
console.log(typeof(s1));
// 3
// number


lis=[1,2,3]
//converting it to string
lis1=JSON.stringify(lis)
console.log(lis1);
console.log(typeof(lis1));
// [1,2,3]
// string

//converting back to normal form
lis2=JSON.parse(lis1)
console.log(lis2);
console.log(typeof(lis2))
// [ 1, 2, 3 ]
// object...which is array

lis.forEach((value,i)=>{
console.log(value);
})


//in simple word 
//  JSON.stringfy(var)
// ....wrap var by ''
// .....and wrap key by ''

// JSON.parse('var')
//... it just remove '' in var which is wrapped by stringify
//....and it just remove ''  in key which is wrapped by stringify;

//Q to use parse does it is necessary that(the string must be made by stringify method)
// answer is no
s="6";
console.log(s);
num1=JSON.parse(s);
console.log(num1);
console.log(typeof(num1))
// 6
// number

//it means parse just remove single quotes 
// note to use parsee it is not necessary that the string is made by stringify method



