


const EventEmitter=require("events")
// note you can change name also
//note baiscally require("...")gives object
//but require("events") gives class

const event=new EventEmitter() //making class object 

//creating own event
event.on('saymyname',()=>{
    console.log("saymyname1 event is trigger")
})
event.on('saymyname',()=>{
    console.log("saymyname2 event is trigger")
})
 

//calling event
event.emit('saymyname')
// sayname1 event is trigger
//saymyname2 event is trigger
 // note when saymyname event is call or done
// it call all event handler 