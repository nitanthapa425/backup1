


//  we have synchronous and asynchronous file system
//from 6 we are going ot learn asynchronous file system

//here we are learning synchronous file system
// file structure
// 1) ./  indicate the folder where the  (current working file)
// 2) ../ indicatet the folder one step before the current folder(ie the folder indicated by nuber 1)
// 3) .../  does not working

// 4)./../ indicate the same folder  as indicated by number 2

// 5) ../../ indicate the one step before the folder indicate by 4 (or folder indicate by 2)

// lets create file.txt
// core module means inbuilt module

//for this we have to import file stystem core module


const fs= require("fs");
//*******************************imp********************************************** */
//note inplace of const fs we can change its name const f1...


fs.writeFileSync("./file1.txt","i will creat file.txt if not exist and write the text \n if the file.txt already exist i will delete all the content and write new content")

// if file.txt does not exist
// make file.txt file if file.txt does not exist 
//and write the text in it

//if file.txt already exist
//then i will delete all data  of the file.txt and
//i will write new data in it


// note you can make file1.txt in any folder