//searching
    find
        find has control over obj
    select
        select has control over obj properties
        -fullName -password email  (dont do this either use all - or use all +)
//sorting
    sort
//pagination
    limit
    skip

// what ever the sequence our order is find, sort, select, skip, limit 


User =[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

User.find({name:"nitan"})

output =[
    [
    {name:"nitan",age:29, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"nitan",age:29, isMarried:true},
]


find({name:"nitan",age:29})

output = [
    {name:"nitan",age:29, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
]

find({age:27})
output = [
    {name:"narendra",age:27, isMarried:false},
]


find({age:"27"})  (in searching type does not matter)
output = [
    {name:"narendra",age:27, isMarried:false},
]


find({age:22, isMarried:"false"})
output = [
    {name:"kriston",age:22, isMarried:false},

]


//number


find({age:{$gt:25}})

output  = [
{name:"nitan",age:29, isMarried:false},
{name:"nitan",age:26, isMarried:true},
 {name:"nitan",age:29, isMarried:true},
{name:"narendra",age:27, isMarried:false},

]

//same for less than,  greater than equal, less than equal,equal, not equal, includes [30, 40, 50], not includes [30,40,50],range
find({age:{$lt:25}})
find({age:{$gte:25}})
find({age:{$lte:25}})
find({age:25})
find({age:{$ne:25}})
find({age:{$in:[30,40,50]}})
find({age:{$nin:[30,40,50]}})
find({age:{$gte:18,$lte:60}})


//string

includes ["nitan", "ram", "har"], not includes ["nitan", "ram"]
find({name:{$in:["nitan","ram","hari]}})
find({name:{$nin:["nitan","ram","hari]}})




exact searching and regex searching(not exact searching)

 User =[
    {name:"ni1t",age:29, isMarried:false},
     {name:"sand2inip",age:25, isMarried:false},
     {name:"ni",age:26, isMarried:true},
     {name:"ris3hav",age:20, isMarried:false},
     {name:"nitan",age:29, isMarried:true},
     {name:"chhimi",age:15, isMarried:true},
     {name:"narendran",age:27, isMarried:false},
     {name:"Nitan",age:16, isMarried:false},
     {name:"nitanthapa",age:22, isMarried:false},
 ]

exact searching
 find({name:"nitan"}) //exact searching
 output =[
    {name:"nitan",age:29, isMarried:true},

 ]

//regex searching   => wrapper of regex is //
"nitan",  /nitan/

find({name:/nitan/})

output = [
     {name:"nitan",age:29, isMarried:true},
  {name:"nitanthapa",age:22, isMarried:false},

]


find({name:/nitan/i})
output = [
      {name:"nitan",age:29, isMarried:true},
     {name:"Nitan",age:16, isMarried:false},
 {name:"nitanthapa",age:22, isMarried:false},

]

find({name:/ni/})
output = [
     {name:"ni1t",age:29, isMarried:false},
 {name:"sand2inip",age:25, isMarried:false},
  {name:"ni",age:26, isMarried:true},
  {name:"nitan",age:29, isMarried:true},
{name:"nitanthapa",age:22, isMarried:false},


]


find({name:/^ni/})//starts with ni

output = [ {name:"ni1t",age:29, isMarried:false},
 {name:"ni",age:26, isMarried:true},
 {name:"nitan",age:29, isMarried:true},
{name:"nitanthapa",age:22, isMarried:false},
]

find({name:/n$/})//ends with n
output = [
    {name:"nitan",age:29, isMarried:true},
 {name:"narendran",age:27, isMarried:false},
{name:"Nitan",age:16, isMarried:false},

]

// name whose length is greater than or equal to four
//copy past from chat gpt
find({name:/\w{4,}/})

// name whose length is exactly 5
find({name:/^\w{5}$/})




sort
  //for sorting
  [
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"c",age:40, isMarried:false},
  
]

find({}).sort("name")

//output
[
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"c",age:40, isMarried:false},
]
 find({}).sort("-name")
 [
    {name:"c",age:40, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ac",age:29, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"ab",age:50, isMarried:false},
]
 find({}).sort("name age ")
 [
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"c",age:40, isMarried:false},
]

 find({}).sort("name -age")
 [
    {name:"ab",age:60, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"c",age:40, isMarried:false},
]



Pagination (limit or skip)


[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]


find({}).limit("2")
[
     {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
]

find({}).skip("8")
[
     {name:"kriston",age:22, isMarried:false},
]

// what ever the sequence our order is find, sort, select, skip, limit 
find({}).limit("5").skip("2")//  find({}).skip("2").limit("5")
[
     {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},

]




*********practice
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]


find({name:/n/}).select("name isMarried).sort("name).limit("2").skip("1")
//find({name:/n/}).sort("name").select("name isMarried").skip("1").limit("2)
[
 {name:"narendra", isMarried:false},
{name:"nitan", isMarried:false},

]


























