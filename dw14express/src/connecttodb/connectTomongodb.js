import mongoose from "mongoose";

let connectToMongoDb = () => {
  mongoose.connect("mongodb://0.0.0.0:27017/dw14");
  console.log("app is connected to database successfully.");
};

export default connectToMongoDb;
