import { Users } from "../schema/model.js";

export let createUser = async (req, res, next) => {
  let data = req.body;
  //save data to Users table
  try {
    let result = await Users.create(data);
    res.status(200).json({
      success: true,
      message: "user created successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

/* 
fullName, 
password,
email,
gender,
address

find select
sort
limit
skip

localhost:8000/user => give all user
localhost:8000/user?fullName=nitan   => give user whose fullName is nitan
localhost:8000/user?fullName=nitan&gender=male   => give user whose fullName is nitan and gender is male
localhost:8000/user?gender=male&sort=name  => give user whose fullName is nitan and gender is male
localhost:8000/user?gender=male&limit=2&page=3
limit=2
page =3

pagination, limit, skip
User.find({}).limit(limit).skip((page-1)*limit)
User.find({}).limit(2).skip(4)

[
  1,
  2,

  3
  4

  5
  6

  7
  8 

  9
]

*/

export let readAllUser = async (req, res, next) => {
  let { sort, select, limit, page, ...myQuery } = req.query; //{gender:"male",sort:"name",select="-password"}
  //sort="fullName"

  try {
    let result = await Users.find(myQuery)
      .sort(sort)
      .select(select)
      .limit(limit) //it gives all User in array of object
      .skip((page - 1) * limit);
    res.status(200).json({
      success: true,
      message: "user read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let readSpecificUser = async (req, res, next) => {
  try {
    let result = await Users.findById(req.params.id);
    res.status(200).json({
      success: true,
      message: "user read successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let updateUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      // runValidators: true,
    });
    res.status(200).json({
      success: true,
      message: "User updated successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteUser = async (req, res, next) => {
  try {
    let result = await Users.findByIdAndDelete(req.params.id);
    res.status(200).json({
      success: true,
      message: "User deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};
