import { Router } from "express";
import {
  createTeacher,
  deleteTeacher,
  readAllTeacher,
  readSpecificTeacher,
  updateTeacher,
} from "../controller/teacherController.js";

/* import {
  createTeacher,
  deleteTeacher,
  readAllTeacher,
  readSpecificTeacher,
  updateTeacher,
} from "../controller/teacherController.js"; */

let teacherRouter = Router();

teacherRouter
  .route("/") //localhost:8000/teacher
  .post(createTeacher)
  .get(readAllTeacher);

teacherRouter
  .route("/:id") //localhost:8000/teacher/any
  .get(readSpecificTeacher)
  .patch(updateTeacher)
  .delete(deleteTeacher);

export default teacherRouter;

/* 
name => string=> required
price => number => required
quantity =>number => required
description => string => required


create teacher
get all teacher

get specific teacher


Teacher.create(req.body)
Teacher.find({})
Teacher.findById(req.params.id)
Teacher.findByIdAndUpdate(req.params.id,req.body)
Teacher.findByIdAndDelete(req.params.id)

*/
