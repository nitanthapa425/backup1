import { Router } from "express";
import { handleSingleFile } from "../controller/fileController.js";

let fileRouter = Router();

fileRouter
  .route("/single") //localhost:8000/file/single
  .post(handleSingleFile);

export default fileRouter;
