import { model } from "mongoose";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import teacherSchema from "./teacherSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Products = model("Products", productSchema);
export let Users = model("Users", userSchema);
export let Teachers = model("Teachers", teacherSchema);
export let Reviews = model("Reviews", reviewSchema);

//firstLetterCapital
// variable name and model name must be same
