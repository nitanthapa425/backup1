import { Module } from '@nestjs/common';
import { UploadfileService } from './uploadfile.service';
import { UploadfileController } from './uploadfile.controller';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  controllers: [UploadfileController],
  providers: [UploadfileService],
})
export class UploadfileModule {}
