import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, Req, UploadedFile, UploadedFiles } from '@nestjs/common';
import { UploadfileService } from './uploadfile.service';
import { CreateUploadfileDto } from './dto/create-uploadfile.dto';
import { UpdateUploadfileDto } from './dto/update-uploadfile.dto';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import {diskStorage} from "multer"
import { multerConfig } from './multer.config';
@Controller('uploadfile')
export class UploadfileController {
  constructor(private readonly uploadfileService: UploadfileService) {}

  @Post()
    @UseInterceptors(FileInterceptor("file",multerConfig))
  create(@UploadedFile() file: any) {
    return this.uploadfileService.create(file);
  }
  // for multiple file upload use FilesInterceptor and @UploadedFiles
  // @UseInterceptors(FilesInterceptor('files', 10, multerConfig))
  // async uploadFiles(@UploadedFiles() files) {
  //   console.log(files);
  // }


  @Get()
  findAll() {
    return this.uploadfileService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.uploadfileService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUploadfileDto: UpdateUploadfileDto) {
    return this.uploadfileService.update(+id, updateUploadfileDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.uploadfileService.remove(+id);
  }
}
