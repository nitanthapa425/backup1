import { Injectable } from '@nestjs/common';
import { CreateUploadfileDto } from './dto/create-uploadfile.dto';
import { UpdateUploadfileDto } from './dto/update-uploadfile.dto';

@Injectable()
export class UploadfileService {
  create(file:any) {
    console.log(file)
    return {
      success:true,
      message:"file uploaded successfully",
      link:`http://localhost:8000/${file.filename}`
    }
  }

  findAll() {
    return `This action returns all uploadfile`;
  }

  findOne(id: number) {
    return `This action returns a #${id} uploadfile`;
  }

  update(id: number, updateUploadfileDto: UpdateUploadfileDto) {
    return `This action updates a #${id} uploadfile`;
  }

  remove(id: number) {
    return `This action removes a #${id} uploadfile`;
  }
}
