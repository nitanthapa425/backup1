// multer.config.ts
import { diskStorage } from 'multer';
import  { extname } from "path"

let storage = diskStorage({
    destination: './uploads', // Specify the destination folder for uploaded files . means root folder
    filename: (req, file, callback) => {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
      callback(null, file.fieldname + '-' + uniqueSuffix);
    },
  })
  
let limits= {
    fileSize: 1024 * 1024*1, // Specify the size limit in bytes (e.g., 1MB)
  }
// let fileFilter =  (req, file, callback) => {
//     // Check file size before saving
//     if (file.size > 1024 * 1024) {
//       return callback(new Error('File size exceeds the limit.'));
//     }
//     // Allow file upload
//     callback(null, true);
//   }


let fileFilter = (req, file, cb) => {
    let validExtensions = [
      ".jpeg",
      ".jpg",
      ".JPG",
      ".JPEG",
      ".png",
      ".svg",
      ".doc",
      ".pdf",
      ".mp4",
      ".PNG",
    ];
  
    let originalName = file.originalname;
    let originalExtension = extname(originalName); //note path module is inbuilt module(package) of node js (ie no need to install path package)
    let isValidExtension = validExtensions.includes(originalExtension);
  
    if (isValidExtension) {
      cb(null, true);
      //true =>it means  pass such type of file
      //note null represent error since there is no error thus error is null
    } else {
      cb(new Error("File is not supported"), false);
  
      //false means don't pass such type of file
    }
  };

export const multerConfig = {
  storage: storage,
  limits: limits,
  fileFilter:fileFilter
  
  
};