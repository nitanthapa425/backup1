import { BadRequestException, ForbiddenException, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CatException } from "./cats.exception";
import { CustomForbiddenException } from "./middleware/error.middleware";

@Injectable()
export class CatsService {
    create():string{
       
        // throw new CatException()
        // throw new HttpException("XYZ",HttpStatus.NOT_FOUND)
        // throw new HttpException({
        //     success:false,
        //     message:"error"
        // },HttpStatus.NOT_FOUND)

        // throw new ForbiddenException()
        // throw new CustomForbiddenException()
        throw new CustomForbiddenException('You do not have permission for this operation.', true,300);
        return "create cat" 
    }
    readAll():string{
        return "read all cat"
    }
    readDetails():string{
        return "read cat details"
    }
    update():string{
        return "update cat"
    }
    delete():string{
        return "delete cat"
    }


}