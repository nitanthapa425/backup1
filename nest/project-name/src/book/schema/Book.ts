// import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";


// export type BookDocument= Book & Document;
// @Schema()
// export class Book{
//     @Prop()
//     title:string;

//     @Prop()
//     author:string;

//     @Prop()
//     published:number;
// }

// export const BookSchema = SchemaFactory.createForClass(Book);import * as mongoose from 'mongoose';
import * as mongoose from 'mongoose';
export const BookSchema = new mongoose.Schema({
    title: { type: String, required: true },
    author: { type: String, required: true },
    published: { type: Number, required: true },
  });
  
  export interface BookDocument extends mongoose.Document {
    title: string;
    author: string;
    published: number;
  }
  
  export const Book = mongoose.model<BookDocument>('Book', BookSchema);