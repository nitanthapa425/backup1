// book.service.ts
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { BookDocument } from './schema/Book';


@Injectable()
export class BookService {
  constructor(@InjectModel('Book') private readonly bookModel: Model<BookDocument>) {}

  async createBook(body:BookDocument): Promise<BookDocument> {
    const newBook = new this.bookModel(body);
    return newBook.save();
  }

  async getAllBooks(): Promise<BookDocument[]> {
    return this.bookModel.find()
  }

  async getBookById(id: string): Promise<BookDocument | null> {
    return this.bookModel.findById(id)
  }

  async updateBook(id: string, updateFields: Partial<BookDocument>): Promise<BookDocument | null> {
    return this.bookModel.findByIdAndUpdate(id, updateFields, { new: true })
  }

  async deleteBook(id: string): Promise<BookDocument | null> {
    return this.bookModel.findByIdAndDelete(id)
  }
}
