import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BookSchema } from './schema/Book';

@Module({
  imports:[MongooseModule.forFeature(
    [{
      name:"Book",
      // name:"ModelName" but it is recommend to use Book.name which is the class name
      schema:BookSchema

    }])],
  controllers: [BookController],
  providers: [BookService],
})
export class  BookModule {}
