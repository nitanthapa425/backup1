// book.controller.ts
import { Controller, Get, Post, Patch, Delete, Param, Body } from '@nestjs/common';
import { BookService } from './book.service';
import { BookDocument } from './schema/Book';


@Controller('books')
export class BookController {
  constructor(private readonly bookService: BookService) {}

  @Post()
  async createBook(@Body() body:BookDocument) {

    try {
      let data =await  this.bookService.createBook(body)
      return{
        success:true,
        message:"Book created successfully.",
        data:data
      }
      
    } catch (error) {
      // throw new Error(error.message)
      // return{
      //   success:false,
      //   message:"Book created successfully.",
      //   // data:data
      // }

      
      
    }
   
    
  }

  @Get()
  async getAllBooks() {
    return{
      success:true,
      message:"Book read successfully.",
      data:await  this.bookService.getAllBooks()
    }
 
  }

  @Get(':id')
  async getBookById(@Param('id') id: string) {
    return{
      success:true,
      message:"Book read successfully.",
      data:await  this.bookService.getBookById(id)
    }
  }

  @Patch(':id')
  async updateBook(@Param('id') id: string, @Body() updateFields: Partial<BookDocument>) {
    return{
      success:true,
      message:"Book updated successfully.",
      data:await  this.bookService.updateBook(id, updateFields)
    }
  }

  @Delete(':id')
  async deleteBook(@Param('id') id: string) {
    return{
      success:true,
      message:"Book deleted successfully.",
      data:await  this.bookService.deleteBook(id)
    }

  }
}
