// http-exception.filter.ts

import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { ForbiddenException } from '@nestjs/common';


@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const defaultStatus = exception.getStatus();
    const exceptionResponse = exception.getResponse(); // This will give you the entire response object

    // Extracting properties from the exception response
    const { message, status, statusCode } = exceptionResponse as {
      message: string;
      status: boolean;
      statusCode: number;
    };

    response.status(statusCode).json({
      status: status||defaultStatus,
      timestamp: new Date().toISOString(),
      path: request.url,
      message,

    });
  }
}




export class CustomForbiddenException extends ForbiddenException {
  constructor(message: string, status: boolean, statusCode: number) {
    super({ message, status, statusCode });
  }
}