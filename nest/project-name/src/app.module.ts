import { Module } from '@nestjs/common';
import { CatsController } from './app.controller';
import { CatsService } from './cats.service';
import { UserModule } from './user/user.module';
import {ConfigModule, ConfigService} from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose';
import { BookModule } from './book/book.module';
import { UploadfileModule } from './uploadfile/uploadfile.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal:true,
      envFilePath:['.env','.development.env']
    }),
    MongooseModule.forRootAsync({
      imports:[ConfigModule],
      useFactory:(configService:ConfigService)=>({uri:configService.get("MONGO_URl")}),
      inject:[ConfigService]
    }),
  
    UserModule,
  
    BookModule,
  
    UploadfileModule
  
  ],
  controllers: [CatsController],
  providers: [CatsService],
})
export class AppModule {
  constructor() {
    console.log('Hello nests jsjjjasdfsdfgasdf');
  }
}
