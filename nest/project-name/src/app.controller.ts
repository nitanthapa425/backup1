
import { Body, Controller, DefaultValuePipe, Delete, Get, HttpCode, HttpStatus, Param, ParseArrayPipe, ParseIntPipe, Patch, Post, Query, Req, Res, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { CatsService } from './cats.service';
import { PageToNumberPipe } from './cats.pipe';
import { CreateCatDto } from './app.dto';
import { config } from 'process';
import { CatGaurd } from './cat.gaurd';
@Controller('cat')
export class CatsController {
  constructor(private catService:CatsService){}

  @UsePipes(ValidationPipe)
  @UseGuards(CatGaurd)
  @Post()
  create(@Body() body:CreateCatDto,@Req() req:any ):any {
    console.log(process.env.SECRET_KEY)
    console.log("***",process.env.EMAIL)
    console.log(body)
    console.log(req.infoObj)

    //  res.status(400).json("hello000");
    return this.catService.create()
  }

  @Get()
  @HttpCode(206)//giving status code
  readAll(@Req() req:any ,@Query() query:any,  @Param() param:any, @Body() body):any{
    // console.log("query", query)
    // console.log("param",param)
    // console.log("body",body)
    // res.status(HttpStatus.OK).json("hello")
    console.log("hello")
    return "hello"


  }
  @Get(":id")
  readDetails(@Query() query:any, @Req() req:any, @Param() param:any):any{
    // console.log("query", req.query)
    // console.log("param",req.params)
    // console.log("body",req.body)
    // return param
    return this.catService.readDetails()

  }
  @Patch(':id')
  //learning pipes are are use for dynamic routes and params
  // it is used for validation and manipulation
  update(
    @Param("id", new ParseIntPipe()) id:number,
  //  @Query("page",new DefaultValuePipe(1), new ParseIntPipe()) page:string,
   @Query("page",new PageToNumberPipe()) page:string,
  
   @Query("names", new ParseArrayPipe({items:String,separator:",",optional: true })) names?:string[],
 
   )
   {
    console.log(page)
    console.log(names)
    return this.catService.update()
    
  }
  @Delete(':id')
  delete() {
    return this.catService.delete()
    
  }
 


}