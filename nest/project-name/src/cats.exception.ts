import { HttpException, HttpStatus } from "@nestjs/common";

export class CatException extends HttpException {
    constructor() {
      super('This is my custum exception', HttpStatus.FORBIDDEN);
    }
  }