import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NextFunction, Request, Response } from 'express';
import { HttpExceptionFilter } from './middleware/error.middleware';
import * as path from "path"
import { NestExpressApplication } from '@nestjs/platform-express';


// global middleware
let globalMiddleware1 = (req:Request, res:Response,next:NextFunction) => {
  console.log("i am global Middleware 1")
  next()
}
let globalMiddleware2 = (req:Request, res:Response,next:NextFunction) => {
  console.log("i am global Middleware 2")
  next()
}

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // app.use(globalMiddleware1,globalMiddleware2);/
  app.useStaticAssets(path.join(__dirname,"../uploads"))
   app.useGlobalFilters(new HttpExceptionFilter()); 
  await app.listen(3000);
}
bootstrap();
