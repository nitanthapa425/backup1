import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class PageToNumberPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata): number {
    // Convert the input value to a number
    let pageNumber: number = parseInt(value, 10);

    // If the conversion fails or the value is not a valid number, set the default value to 1
    if (isNaN(pageNumber) || pageNumber <= 0) {
      pageNumber = 1;
    }

    return pageNumber;
  }
}