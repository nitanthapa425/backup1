/**
 * Copyright (C) Two Pangra
 */

/**
 * the helper
 *
 * @author      Arjun Subedi
 * @version     1.0
 */
const joi = require('joi');
const co = require('co');
const bluebird = require('bluebird');
const bcrypt = require('bcryptjs');
const config = require('config');
const httpStatus = require('http-status');
const _ = require('lodash');
const fs = require('fs');
const util = require('util');
const errors = require('common-errors');
const nodemailer = require('nodemailer');
const messagebird = require('messagebird')('7M4tFn9ghJmJ3S35UVMqqIp49');
const logger = require('./logger');
const {
  VehicleDetail,
  User,
  LogSchemaModel,
  LogManagementModel,
  NotificationModel,
  Client,
  PostExpiryDateModel
} = require('../models');
const { getDiff } = require('../utils/diff');

bluebird.promisifyAll(bcrypt);

/**
 * Wrap async function to standard express function
 * @param {Function} fn the async function
 * @returns {Function} the wrapped function
 */
function wrapExpress(fn) {
  return (req, res, next) => co(fn(req, res, next)).catch(next);
}

/**
 * add toObject transform to mongoose schema
 * @param schema Mongoose Schema
 * @param transformFunc the toObject transform function
 */

/* eslint-disable no-param-reassign */
function pluginSchemaToObject(schema, transformFunc) {
  if (!schema.options.toObject) {
    schema.options.toObject = {};
  }
  if (!transformFunc) {
    transformFunc = (doc, ret) => {
      const sanitized = _.omit(ret, '__v', '_id');
      sanitized.id = doc._id;
      return sanitized;
    };
  }
  schema.options.toObject.transform = transformFunc;
}

/**
 * Validate that the hash is actually the hashed value of plain text
 * @param {String} text the text to validate
 * @param {String} hash the hash to validate
 * @returns {Boolean} whether it is valid or not
 */
async function validateHash(text, hash) {
  const value = await bcrypt.compareSync(text, hash);
  return value;
}

/**
 * Wrap all async from object
 * @param {Object} obj the object (controller exports)
 * @returns {Object|Array} the wrapped object
 */
function autoWrapExpress(obj) {
  if (_.isArray(obj)) {
    return obj.map(autoWrapExpress);
  }
  if (_.isFunction(obj)) {
    if (obj.constructor.name === 'AsyncFunction') {
      return wrapExpress(obj);
    }
    return obj;
  }
  _.each(obj, (value, key) => {
    obj[key] = autoWrapExpress(value);
  });
  return obj;
}

/**
 * Helper method to sanitize the Array
 * Sanitization means convert the mongoose model into plain javascript object
 *
 * @param array the array to sanitize
 * @param method the sanitize method
 */
function sanitizeArray(array, method) {
  const newMethod = method || 'toObject';
  if (_.isArray(array)) {
    const response = [];
    _.forEach(array, (single) => {
      response.push(single[newMethod]());
    });
    return response;
  }
  return array.toObject();
}

/**
 * Hash the given text.
 * @param {String} passwordText the password text to hash
 * @returns {String} the hashed string
 */
async function hashString(passwordText) {
  const value = await bcrypt.hashAsync(passwordText, config.PASSWORD_HASH_SALT_LENGTH);
  return value;
}

/**
 * get a random string
 * @param {number} length the length
 * @param {String} characters the characters
 * @returns {String} the random string
 */
function getRandomString(length, characters) {
  const newLength = length || 40;
  const $characters = characters || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!';
  let text = '';
  for (let i = 0; i < newLength; i += 1) {
    text += $characters.charAt(Math.floor(Math.random() * $characters.length));
  }
  return text;
}

/**
 * get host url with api version, example `http://localhost:3000/api/v1`
 * @returns {String} the url
 */
function getHostUrlWithApiVersion() {
  return `${config.HOST_URL}${config.API_VERSION}`;
}

/**
 * get host url
 * @returns {String} the url
 */
function getHostUrl() {
  return `${config.HOST_URL}`;
}

/**
 * checks if entity exists or not
 * @param {Object} model the model
 * @param {Object} query the query
 * @param {string} message the error message
 * @returns {Object} the found entity
 */
async function ensureEntityExists(model, query, message) {
  const entity = await model.findOne(query);
  if (!entity) {
    throw new errors.NotFoundError(message || `cannot find entity where: ${JSON.stringify(query)}`);
  }
  return entity;
}

/**
 * checks if entity exists or not
 * @param {Object} model the model
 * @param {Object} query the query
 * @param {string} message the error message
 * @returns {Object} the found entity
 */
async function ensureEntityExistsInNested(model, query, query2, message) {
  const entity = await model.findOne(query, query2);
  if (!entity) {
    throw new errors.NotFoundError(message || `cannot find entity where: ${JSON.stringify(query)}`);
  }
  return entity;
}
/**
 * check if value  exist or not.
 * @param {Object} model the model
 * @param {Object} query the query
 * @param {String} value the value
 */
async function ensureValueIsUnique(model, query, value) {
  const entity = await model.findOne(query);
  if (entity) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `${value} already exists`);
  }
}
/**
 * checks if user has entity access or not
 * @param {Object} model the model
 * @param {Object} query the model
 * @param {String} userId the user id
 * @param {String} message the message
 * @returns {Object} the found entity
 */
async function ensureEntityAccess(model, query, userId, message) {
  const entity = await model.findOne(query);
  if (!entity) {
    throw new errors.NotFoundError(`cannot find entity where: ${JSON.stringify(query)}`);
  }
  if (entity.createdBy.toString() !== userId) {
    throw new errors.NotPermittedError(message);
  }
  return entity;
}

/**
 * ensures that folder exists in the path
 * @param {String} folderPath the folder path
 */
async function ensureDirectoryExists(folderPath) {
  // create directory
  if (!fs.existsSync(folderPath)) {
    throw new errors.NotFoundError(`the directory ${folderPath} not exists`);
  }
}

/**
 * create directory
 * @param {String} directory the directory
 */
async function createDirectory(directory) {
  if (!fs.existsSync(directory)) {
    await fs.mkdirSync(directory);
  }
}

/**
 * removes files from directory
 * @param {String} directory the directory
 */
async function removeDirectoryFiles(directory) {
  try {
    await ensureDirectoryExists(directory);
    const files = await fs.readdirSync(directory);
    const unlinkPromises = files.map((filename) => fs.unlinkSync(`${directory}/${filename}`));
    await Promise.all(unlinkPromises);
  } catch (ex) {
    logger.error(ex);
    throw ex;
  }
}

/**
 * ensures if user is verified
 * @param {Object} user the user
 */
function ensureUserVerified(user) {
  if (!user.verified) {
    throw new errors.NotPermittedError(`email ${user.email} is not verified`);
  }
}

/**
 * ensures if client is verified
 * @param {Object} user the user
 */
function ensureClientVerified(user) {
  if (!user.phoneVerified) {
    throw new errors.NotPermittedError(`Mobile ${user.mobile} is not verified`);
  }
}

/**
 * gets user by email id
 * @param {String} email the email id
 * @returns {Object} the user information
 */
async function ensureUserActive() {
  const user = await User.findOne({ isActive: true });
  return user;
}

/**
 * gets user by email id
 * @param {String} email the email id
 * @returns {Object} the user information
 */
async function ensureUserVerifiedTrue() {
  const user = await User.findOne({ isVerified: true });
  return user;
}

/**
 * transporter config for sending emails
 */
const transporter = nodemailer.createTransport(_.extend(config.email, { logger }), {
  from: `${config.email.auth.user}`
});

/**
 * sends email to the provided email id
 * @param {Object} emailEntity the email entity
 * @returns {Promise}
 */
async function sendEmail(emailEntity) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(emailEntity, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

sendEmail.schema = {
  emailEntity: joi
    .object()
    .keys({
      to: joi.string().required(),
      from: joi.string().optional().allow(''),
      replyTo: joi.string().optional().allow(''),
      subject: joi.string().required(),
      text: joi.string(),
      html: joi.string()
    })
    .required()
};

/* Send OTP code  */
async function sendOTP(mobileNumber) {
  mobileNumber = `+977${mobileNumber}`;
  return new Promise((resolve, reject) => {
    const params = {
      originator: 'TestMessage',
      type: 'sms'
    };
    messagebird.verify.create(mobileNumber, params, (err, response) => {
      if (err) {
        reject(err);
      }
      resolve(response);
    });
  });
}
/**
 * sends Otp to the provided email id
 * @param {Object} mobileEntity the email entity
 * @returns {Promise}
 */
// async function sendOTP(mobileEntity) {
// const params = {
//   originator: 'TestMessage',
//   recipients: [
//     mobileEntity
// ],
//   'body': 'This is a test message'
// };
//   messagebird.messages.create(
//     mobileEntity,
//     {
//       originator: 'Code',
//       template: 'Your verification code is %token.'
//     },
//     (err, response) => {
//       if (err) {
//         return console.log(err);
//       }
//       return console.log(response);
//     }
//   );
// }

/* Send OTP validation */
sendOTP.schema = {
  mobileEntity: joi
    .object()
    .keys({
      originator: joi.string().required(),
      from: joi.string().optional().allow(''),
      recipients: joi.string().optional().allow(''),
      body: joi.string().required()
    })
    .required()
};

/* Check  */
async function checkDublicateVehicleName(entity) {
  const vehicleDetail = await VehicleDetail.findOne({ vehicleName: entity.vehicleName });
  if (vehicleDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `vehicleDetail name ${entity.vehicleName} already exists`);
  }
}

/* Capitalize first letter of word */
function capitalize(value) {
  // const _value = value.trim()[0].toUpperCase() + value.slice(1).toLowerCase();
  if (typeof value !== 'undefined' && value !== '') {
    return value.trim()[0].toUpperCase() + value.slice(1).toLowerCase();
  }
  return '';
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
// async function sendOTPInMobile(recipients) {
// //  const mobileEntity =  recipients,
// //     {
// //       originator: 'Code',
// //       template: 'Your verification code is %token.'
// //     },
//   console.log('mobileEntity', recipients);
//   await sendOTP(recipients);
// }

/* Log for activity logs */
const plugin = function (schema) {
  schema.post('init', (doc) => {
    doc._original = doc.toObject({ transform: false });
  });
  schema.pre('save', function (next) {
    if (this.isNew) {
      next();
    } else {
      this._diff = getDiff(this, this._original);
      next();
    }
  });

  schema.methods.log = function (data) {
    data.diff = {
      before: this._original,
      after: this._diff
    };
    return LogSchemaModel.create(data);
  };
};

async function createLogManagement(userId, logType, category, logDescriptions, accessLevel) {
  let logDescription = '';
  let createdByName = {};
  if (accessLevel === 'CLIENT') {
    createdByName = await Client.findOne({ _id: userId });
  } else {
    createdByName = await User.findOne({ _id: userId });
  }

  if (logType === 'Add Data') {
    logDescription = `${logDescriptions} added successfully`;
  } else if (logType === 'Update Data') {
    logDescription = `${logDescriptions} updated successfully`;
  } else if (logType === 'Delete Data') {
    logDescription = `${logDescriptions} deleted successfully`;
  } else if (logType === 'Add in Landing Page') {
    logDescription = `${logDescriptions} added in landing page successfully`;
  } else if (logType === 'Add Cart') {
    logDescription = `${logDescriptions} has been added to cart successfully`;
  } else if (logType === "Add Seller's Vehicle") {
    logDescription = `${logDescriptions} has been added successfully`;
  } else {
    logDescription = `${logDescriptions} removed from landing page successfully`;
  }

  const logDetail = {
    createdBy: userId,
    createdByName: createdByName.fullName,
    logType,
    category,
    logDescription
  };
  return LogManagementModel.create(logDetail);
}
async function createSellerLogManagement(userId, logType, category, logDescriptions, accessLevel) {
  let logDescription = '';
  let createdByName = {};
  if (accessLevel === 'CLIENT') {
    createdByName = await Client.findOne({ _id: userId });
  } else {
    createdByName = await User.findOne({ _id: userId });
  }

  if (logType === 'Add Sellers Vehicle') {
    logDescription = `${logDescriptions} added successfully`;
  } else if (logType === 'Get All Data') {
    logDescription = `${logDescriptions} updated successfully`;
  } else if (logType === 'Delete Data') {
    logDescription = `${logDescriptions} deleted successfully`;
  } else if (logType === 'Update Data') {
    logDescription = `${logDescriptions} added in landing page successfully`;
  } else if (logType === 'Get Data') {
    logDescription = `${logDescriptions} has been added to cart successfully`;
  }

  const logDetail = {
    createdBy: userId,
    createdByName: createdByName.fullName,
    logType,
    category,
    logDescription
  };
  return LogManagementModel.create(logDetail);
}

/* Create Notification  */
async function createNotification(
  notificationType,
  notificationCategory,
  accessLevel,
  itemId,
  userId,
  sellerId,
  vehicleName,
  imageUrl
) {
  let content = '';
  let buyer = {};
  let seller = {};

  buyer = await Client.findById(userId);

  if (buyer === {} || buyer === null) {
    buyer = await User.findById(userId);
  }

  seller = await Client.findById(sellerId);

  if (seller === {} || seller === null) {
    seller = await User.findById(sellerId);
  }

  if (accessLevel === 'BUYER' && notificationType === 'Add' && notificationCategory !== 'Book') {
    content = `You have successfully added ${vehicleName} in ${notificationCategory}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Add' && notificationCategory !== 'Book') {
    content = `${buyer.fullName} have added your vehicle ${vehicleName} in ${notificationCategory}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Delete' && notificationCategory !== 'Book') {
    content = `You have successfully deleted ${vehicleName} from ${notificationCategory}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Delete' && notificationCategory !== 'Book') {
    content = `${buyer.fullName} have deleted your vehicle ${vehicleName} from ${notificationCategory}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Add' && notificationCategory === 'Book') {
    content = `You have successfully booked ${vehicleName}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Add' && notificationCategory === 'Book') {
    content = `${buyer.fullName} have booked your vehicle ${vehicleName}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Delete' && notificationCategory === 'Book') {
    content = `You have successfully cancel your booked item ${vehicleName}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Delete' && notificationCategory === 'Book') {
    content = `${buyer.fullName} have cancelled booking of your vehicle ${vehicleName}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Meeting' && notificationCategory === 'Vehicle Sell') {
    content = `Meeting schedule have been send in your mail for ${vehicleName} vehicle buy`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Meeting' && notificationCategory === 'Vehicle Sell') {
    content = `Meeting schedule have been send in your mail for your ${vehicleName} vehicle sell`;
  }

  const data = {
    notificationType,
    content,
    accessLevel,
    itemId,
    createdBy: accessLevel === 'SELLER' ? sellerId : userId,
    createdByName: accessLevel === 'SELLER' ? seller.fullName : buyer.fullName,
    imageUrl
  };
  return NotificationModel.create(data);
}

async function createAdminNotification(
  notificationType,
  notificationCategory,
  accessLevel,
  itemId,
  userId,
  sellerId,
  vehicleName,
  imageUrl
) {
  let content = '';
  let buyer = {};
  let seller = {};

  buyer = await Client.findById(userId);

  if (buyer === {} || buyer === null) {
    buyer = await User.findById(userId);
  }

  seller = await Client.findById(sellerId);

  if (seller === {} || seller === null) {
    seller = await User.findById(sellerId);
  }

  const adminList = await User.find({ accessLevel });

  if (accessLevel === 'ADMIN' && notificationType === 'Add' && notificationCategory === 'Cart') {
    content = `${buyer.fullName} have successfully added ${vehicleName} vehicle in ${notificationCategory}`;
  }

  if (accessLevel === 'SUPER_ADMIN' && notificationType === 'Add' && notificationCategory === 'Cart') {
    content = `${buyer.fullName} have successfully added ${vehicleName} vehicle in ${notificationCategory}`;
  }

  if (accessLevel === 'ADMIN' && notificationType === 'Delete' && notificationCategory === 'Cart') {
    content = `${buyer.fullName} have deleted ${vehicleName} vehicle from ${notificationCategory}`;
  }

  if (accessLevel === 'SUPER_ADMIN' && notificationType === 'Delete' && notificationCategory === 'Cart') {
    content = `${buyer.fullName} have deleted ${vehicleName} vehicle from ${notificationCategory}`;
  }

  if (accessLevel === 'ADMIN' && notificationType === 'Unsold' && notificationCategory === 'Vehicle Sell') {
    content = `${buyer.fullName} have marked ${vehicleName} vehicle as unsold`;
  }

  if (accessLevel === 'SUPER_ADMIN' && notificationType === 'Unsold' && notificationCategory === 'Vehicle Sell') {
    content = `${buyer.fullName} have marked ${vehicleName} vehicle as unsold`;
  }

  if (accessLevel === 'ADMIN' && notificationType === 'Sold' && notificationCategory === 'Vehicle Sell') {
    content = `${buyer.fullName} have marked ${vehicleName} vehicle as sold`;
  }

  if (accessLevel === 'SUPER_ADMIN' && notificationType === 'Sold' && notificationCategory === 'Vehicle Sell') {
    content = `${buyer.fullName} have marked ${vehicleName} vehicle as sold`;
  }

  if (accessLevel === 'SUPER_ADMIN' && notificationType === 'Meeting' && notificationCategory === 'Vehicle Sell') {
    content = `Meeting schedule have been send in your mail for ${vehicleName} vehicle`;
  }

  if (accessLevel === 'ADMIN' && notificationType === 'Meeting' && notificationCategory === 'Vehicle Sell') {
    content = `Meeting schedule have been send in your mail for ${vehicleName} vehicle`;
  }

  adminList.map((res) => {
    const data = {
      notificationType,
      content,
      accessLevel,
      itemId,
      createdBy: res._id,
      createdByName: '',
      imageUrl
    };
    return NotificationModel.create(data);
  });

  return 'success';
}

/**
 * send meeting schedule detail to superadmin, admin, buyer, seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function adminEmailMeetingSchedule(adminEmail, buyer, seller, vehicleName, productCode, bookId, frontendURL) {
  const emailContent = util.format(
    `Dear Admin, <br>
    Buyer with the name: ${buyer} has placed a meeting to buy vehicle of seller name: ${seller}.
    The book vehicle is <a href="${frontendURL}/vehicle-listing/${productCode}"> Click here </a>
    To see the vehicle link. 
    To see the book detail <a href="${frontendURL}/book/${bookId}">Click here </a> `
  );
  const emailEntity = {
    subject: 'Seller vehicle booked',
    to: adminEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to superadmin, admin, buyer, seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function superAdminEmailMeetingSchedule(
  superadminEmail,
  buyer,
  seller,
  vehicleName,
  productCode,
  bookId,
  frontendURL
) {
  const emailContent = util.format(
    `Dear Super Admin, <br>
    Meeting has been placed by buyer name: ${buyer}. To buy seller name: ${seller} vehicle.
    To book vehicle : ${vehicleName}
    The book vehicle is: <a href="${frontendURL}/vehicle-listing/${productCode}"> link </a>
    To see the book detail <a href="${frontendURL}/book/${bookId}"> link </a>`
  );
  const emailEntity = {
    subject: 'Seller vehicle booked',
    to: superadminEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to buyer, seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function buyerEmailMeetingSchedule(buyerEmail, seller, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    `Dear ${seller}, <br>
    The vehicle you have booked
    <a href="${frontendURL}/vehicle-listing/${productCode}"> link </a>
    has been scheule a meeting.
    Please contact two pangra on email 
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Meeting Scheule for Buying Vehicle',
    to: buyerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}
/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sellerEmailMeetingSchedule(sellerEmail, seller, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear ${seller}, <br>
    The vehicle you have poseted to sell
     <a href="${frontendURL}/vehicle-listing/${productCode}">link</a> has been scheule a meeting. Please contact two pangra admin on email 
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Meeting Schedule for buying Vehicle',
    to: sellerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function notifySellerBookedVehicle(sellerEmail, seller, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear ${seller}, <br>
    The vehicle you have poseted to sell
     <a href="${frontendURL}/vehicle-listing/${productCode}">link</a> has been booked. Please contact two pangra admin on email 
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Your vehicle has been booked',
    to: sellerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function notifyBuyerBookedVehicle(sellerEmail, buyer, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear ${buyer}, <br>
    You have book the vehicle:
    <a href="${frontendURL}/vehicle-listing/${productCode}">link</a> successfully. Please contact two pangra admin on email 
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Booked Vehicle for Buying',
    to: sellerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendExpiryEmailToSeller(sellerEmail, seller, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear ${seller}, <br>
    The ad you have <a href="${frontendURL}/vehicle-listing/${productCode}">posted</a> in two pangra will be renewed after five days.
    If you don't want to renew the post. Please click to <a href="${frontendURL}/vehicle-listing/${productCode}">remove post</a>
    . Please contact two pangra admin on email
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Post Expiry Soon',
    to: sellerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function adFeatureEmailToSeller(sellerEmail, seller, productCode, adminEmail, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear ${seller}, <br>
    The <a href="${frontendURL}/vehicle-listing/${productCode}">vehicle</a> you have wanted to list on  feature listing has free space for you.
    If you  want to post your vehicle on feature list
     the post. Please click to <a href="${frontendURL}/featured/${productCode}">add to feature</a>
    . Please contact two pangra admin on email
    : ${adminEmail} or call on the helpline +9779862972729 for further enquiry.`
  );
  const emailEntity = {
    subject: 'Post Your add on feature',
    to: sellerEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

/* Create Notification  */
async function createSellerNotification(
  notificationType,
  notificationCategory,
  accessLevel,
  itemId,
  userId,
  sellerId,
  vehicleName,
  imageUrl
) {
  let content = '';
  let buyer = {};
  let seller = {};

  buyer = await Client.findById(userId);

  if (buyer === {} || buyer === null) {
    buyer = await User.findById(userId);
  }

  seller = await Client.findById(sellerId);

  if (seller === {} || seller === null) {
    seller = await User.findById(sellerId);
  }

  if (accessLevel === 'SELLER' && notificationType === 'Add' && notificationCategory === 'Sellers Vehicle') {
    content = `You have successfully added ${vehicleName} in ${notificationCategory}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Add' && notificationCategory !== 'Book') {
    content = `${buyer.fullName} have added your vehicle ${vehicleName} in ${notificationCategory}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Delete' && notificationCategory !== 'Book') {
    content = `You have successfully deleted ${vehicleName} from ${notificationCategory}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Delete' && notificationCategory !== 'Book') {
    content = `${buyer.fullName} have deleted your vehicle ${vehicleName} from ${notificationCategory}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Add' && notificationCategory === 'Book') {
    content = `You have successfully booked ${vehicleName}`;
  }

  if (accessLevel === 'SELLER' && notificationType === 'Add' && notificationCategory === 'Book') {
    content = `${buyer.fullName} have booked your vehicle ${vehicleName}`;
  }

  if (accessLevel === 'BUYER' && notificationType === 'Delete' && notificationCategory === 'Book') {
    content = `You have successfully cancel your booked item ${vehicleName}`;
  }

  const data = {
    notificationType,
    content,
    accessLevel,
    itemId,
    createdBy: accessLevel === 'SELLER' ? sellerId : userId,
    createdByName: accessLevel === 'SELLER' ? seller.fullName : buyer.fullName,
    imageUrl
  };
  return NotificationModel.create(data);
}
/**
 * send meeting schedule detail to seller
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendOfferPriceEmail(
  buyerName,
  buyerId,
  offerPrice,
  adminEmail,
  sellerName,
  sellerId,
  sellerVehicleId,
  frontendURL
) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `Dear Admin, <br>
    Buyer with the name: ${buyerName} has offerd a price of ${offerPrice} for
     <a href="${frontendURL}/vehicle-listing/${sellerVehicleId}">vehicle</a> of ${sellerName}. <br>
     To confirm the offer please <a href="${frontendURL}offerPrice/view/"> Click here. </a> <br>
     To see the buyer detail <a href="${frontendURL}/customer/details/${buyerId}"> click here. </a> <br>
     To see the seller detail <a href="${frontendURL}/customer/details/${sellerId}"> click here. </a>`
  );
  const emailEntity = {
    subject: 'Buyer Offered Price for Seller Vehicle',
    to: adminEmail,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}
// eslint-disable-next-line consistent-return
async function updateExpiryDate(postId, postExpiryDate, status) {
  const date = postExpiryDate.setMonth(postExpiryDate.getMonth() + 1);
  /* Check if date matches to current date */
  if (status === true) {
    if (postExpiryDate === new Date()) {
      const value = await PostExpiryDateModel.update({ _id: postId }, { postExpiryDate: date });
      return value;
    }
  }
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendBookVehicleDetail(email, name, productCode, clientId, frontendURL) {
  const emailContent = util.format(
    config.bookVehicleSellContent,
    `${name} has booked product with code: ${productCode}. <br><br>
  <br> To see the detail of customer <a href="${frontendURL}//signup/get/${clientId}">click here</a> <br> <br> 
  Please click the link for full detail of the seller's vehicle. ${frontendURL}/vehicle-listing/${productCode}`
  );
  const emailEntity = {
    subject: 'Seller vehicle booked',
    to: email,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await sendEmail(emailEntity);
}

module.exports = {
  pluginSchemaToObject,
  validateHash,
  autoWrapExpress,
  hashString,
  sanitizeArray,
  getRandomString,
  getHostUrlWithApiVersion,
  ensureEntityExists,
  ensureUserVerified,
  ensureValueIsUnique,
  ensureEntityAccess,
  ensureDirectoryExists,
  createDirectory,
  removeDirectoryFiles,
  getHostUrl,
  ensureUserActive,
  ensureUserVerifiedTrue,
  sendEmail,
  sendOTP,
  // sendOTPInMobile,
  checkDublicateVehicleName,
  capitalize,
  ensureClientVerified,
  plugin,
  createNotification,
  createLogManagement,
  createAdminNotification,
  createSellerLogManagement,
  adminEmailMeetingSchedule,
  superAdminEmailMeetingSchedule,
  buyerEmailMeetingSchedule,
  sellerEmailMeetingSchedule,
  notifySellerBookedVehicle,
  notifyBuyerBookedVehicle,
  sendExpiryEmailToSeller,
  updateExpiryDate,
  adFeatureEmailToSeller,
  ensureEntityExistsInNested,
  sendOfferPriceEmail,
  createSellerNotification,
  sendBookVehicleDetail
};
