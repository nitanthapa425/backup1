/**
 * Copyright (C) Two Pangra
 */

/**
 * the Error Middleware
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const errors = require('common-errors');
const httpStatus = require('http-status');
const logger = require('./logger');

// default message to show on error
const DEFAULT_MESSAGE = 'Internal server error';

/**
 * The error middleware function
 * @param {Object} err the error that is thrown in the application
 * @param {Object} req the express request instance
 * @param {Object} res the express response instance
 */

// eslint-disable-next-line no-unused-vars
function middleware(err, req, res, next) {
  logger.logFullError(err, req.method, req.url);
  let errorStatus;
  let errorMessage;
  if (err.isJoi) {
    errorStatus = httpStatus.BAD_REQUEST;
    errorMessage = err.details;
    res.status(errorStatus).json({
      message: errorMessage
    });
  } else if (err.errors) {
    errorStatus = httpStatus.BAD_REQUEST;
    errorMessage = err.errors;
    res.status(errorStatus).json({ message: errorMessage });
  } else {
    const httpError = new errors.HttpStatusError(err);
    if (err.statusCode >= httpStatus.INTERNAL_SERVER_ERROR) {
      httpError.message = DEFAULT_MESSAGE;
    }
    errorStatus = httpError.statusCode;
    errorMessage = httpError.message || DEFAULT_MESSAGE;
    res.status(errorStatus).json({ message: errorMessage });
  }
}

module.exports = () => middleware;
