/**
 * Copyright (C) Two Pangra
 */

/**
 * the security service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const rateLimit = require('express-rate-limit');

const authLimiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 20,
  skipSuccessfulRequests: true
});

module.exports = {
  authLimiter
};
