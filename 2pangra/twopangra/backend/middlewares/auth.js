/**
 * Copyright (C) Two Pangra
 */

/**
 * the auth
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const expressJwt = require('express-jwt');
const config = require('config');
const httpStatus = require('http-status');
const userService = require('../services/user.service');
const ClientService = require('../services/client.service');

const expressAuthentication = expressJwt({
  secret: config.JWT_SECRET,
  algorithms: ['HS256']
});

const isAuthenticated = async (req, res, next) => {
  if (!req.user) return res.status(401).send({ code: httpStatus.UNAUTHORIZED, message: 'Unauthorized' });
  let user = {};
  user = await userService.getUserById(req.user.id);
  if (user === {}) {
    user = await ClientService.getClientById(req.user.id);
  }

  if (user.isActive === false) {
    return res.status(401).send({
      code: httpStatus.UNAUTHORIZED,
      message: 'Unauthorized. User Inactive'
    });
  }
  req.user = { ...req.user };
  return next();
};

// const isClientAuthenticated = async (req, res, next) => {
//   if (!req.user) return res.status(401).send({ code: httpStatus.UNAUTHORIZED, message: 'Unauthorized' });
//   const user = await ClientService.getClientById(req.user.id);
//   if (user.status === false) {
//     return res.status(401).send({
//       code: httpStatus.UNAUTHORIZED,
//       message: 'Unauthorized. Client Inactive'
//     });
//   }
//   req.user = { ...req.user };
//   return next();
// };

const isAuthorized = (options) => (req, res, next) => {
  const { accessLevel } = req.user;
  if (options.includes(accessLevel)) return next();
  return res.status(403).send(`{message: "unauthorized. Required Role ${options} "}`);
};

module.exports = {
  isAuthenticated,
  isAuthorized,
  expressAuthentication
  // isClientAuthenticated
};
