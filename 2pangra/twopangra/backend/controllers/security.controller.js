/**
 * Copyright (C) Two Pangra
 */

/**
 * the security service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const securityService = require('../services/security.service');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');

/**
 * user login with email password
 * @param req the http request
 * @param res the http response
 */
const login = catchAsync(async (req, res) => {
  res.json(await securityService.login(req.body));
});

/**
 * does sign up process
 * @param {Object} req the http request
 * @param {Object} res the http response
 */ const signUp = catchAsync(async (req, res) => {
  res.json(await securityService.signUp(req.body));
});

/**
 * handles the confirm email
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const confirmEmail = catchAsync(async (req, res) => {
  res.json(await securityService.confirmEmail(req.body));
});

/**
 * handles the get my profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMyProfile = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await securityService.getMyProfile(userId));
});

/**
 * handles the update own profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateMyProfile = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const userId = req.user.id;
  const dateOfBirth = new Date(req.body.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');
  const userDetail = {
    ...req.body,
    fullName,
    dob
  };
  res.json(await securityService.updateMyProfile(userId, userDetail));
});

/**
 * does forgot password process
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const forgotPassword = catchAsync(async (req, res) => {
  res.json(await securityService.forgotPassword(req.body));
});

/**
 * does reset password process
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const resetPassword = catchAsync(async (req, res) => {
  res.json(await securityService.resetPassword(req.body));
});

/**
 * handles changePassword
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const changePassword = catchAsync(async (req, res) => {
  res.json(await securityService.changePassword(req.user.id, req.body));
});

/**
 * handles logout
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const logout = catchAsync(async (req, res) => {
  res.json(await securityService.logout(req.user.id));
});

const validateUser = catchAsync(async (req, res) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(' ')[1];

  const result = await securityService.validateUser(token);

  res.json(result);
});

const getAllLogManagement = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await securityService.getAllLogManagement(search, options));
});

const getAllNotification = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await securityService.getAllNotification(search, options, req.app.locals.io));
});

const markNotificationAsRead = catchAsync(async (req, res) => {
  res.json(await securityService.markNotificationAsRead(req.body.notificationId));
});

const markNotificationAsUnread = catchAsync(async (req, res) => {
  res.json(await securityService.markNotificationAsUnread(req.body.notificationId));
});

const deleteNotification = catchAsync(async (req, res) => {
  res.json(await securityService.deleteNotification(req.body.notificationId));
});

const readAllNotification = catchAsync(async (req, res) => {
  res.json(await securityService.readAllNotification(req.user.id));
});

module.exports = {
  login,
  signUp,
  getMyProfile,
  updateMyProfile,
  forgotPassword,
  resetPassword,
  confirmEmail,
  changePassword,
  logout,
  validateUser,
  getAllLogManagement,
  getAllNotification,
  markNotificationAsRead,
  markNotificationAsUnread,
  deleteNotification,
  readAllNotification
};
