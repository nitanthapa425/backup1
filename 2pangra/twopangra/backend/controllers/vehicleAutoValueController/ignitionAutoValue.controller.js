/**
 * Copyright (C) Tuki Logic
 */

/**
 * Ignition Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { IgnitionAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create ignition Controller
  */
const createIgnitionController = catchAsync(async (req, res) => {
  const ignitionAutoValue = await IgnitionAutoValueService.createIgnitionService(req.body);
  res.status(httpStatus.CREATED).send(ignitionAutoValue);
});

/**
 * get ignition information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getIgnitionController = catchAsync(async (req, res) => {
  res.json(await IgnitionAutoValueService.getIgnitionService());
});

module.exports = {
  createIgnitionController,
  getIgnitionController
};
