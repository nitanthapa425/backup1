/**
 * Copyright (C) Tuki Logic
 */

/**
 * Instrumentation Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { InstrumentationAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create instrumentation Controller
  */
const createInstrumentationController = catchAsync(async (req, res) => {
  const instrumentationAutoValue = await InstrumentationAutoValueService.createInstrumentationService(req.body);
  res.status(httpStatus.CREATED).send(instrumentationAutoValue);
});

/**
 * get instrumentation information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getInstrumentationController = catchAsync(async (req, res) => {
  res.json(await InstrumentationAutoValueService.getInstrumentationService());
});

module.exports = {
  createInstrumentationController,
  getInstrumentationController
};
