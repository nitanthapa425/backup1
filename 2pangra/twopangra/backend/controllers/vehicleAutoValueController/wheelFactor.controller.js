/**
 * Copyright (C) Two Pangra
 */

/**
 * Dimensional Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { WheelFactorAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create instrumentation Controller
  */
const createFrontWheelTypeController = catchAsync(async (req, res) => {
  const value = await WheelFactorAutoValueService.createFrontWheelTypeService(req.body);
  res.status(httpStatus.CREATED).send(value);
});

/**
 * get instrumentation information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontWheelTypeController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getFrontWheelTypeService());
});

/**
  * Create  front wheel type Controller
  */
const createRearWheelTypeController = catchAsync(async (req, res) => {
  const value = await WheelFactorAutoValueService.createRearWheelTypeService(req.body);
  res.status(httpStatus.CREATED).send(value);
});

/**
 * get instrumentation information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const getRearWheelTypeController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getRearWheelTypeService());
});

/**
  * Create Overall Width Controller
  */
const createFrontWheelSizeController = catchAsync(async (req, res) => {
  const overallWidthAutoValue = await WheelFactorAutoValueService.createFrontWheelSizeService(req.body);
  res.status(httpStatus.CREATED).send(overallWidthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontWheelSizeController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getFrontWheelSizeService());
});

/**
  * Create Overall Width Controller
  */
const createRearWheelSizeController = catchAsync(async (req, res) => {
  const overallWidthAutoValue = await WheelFactorAutoValueService.createRearWheelSizeService(req.body);
  res.status(httpStatus.CREATED).send(overallWidthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getRearWheelSizeController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getRearWheelSizeService());
});

/**
  * Create Overall Length Controller
  */
const createRearTyreController = catchAsync(async (req, res) => {
  const overallLengthAutoValue = await WheelFactorAutoValueService.createRearTyreService(req.body);
  res.status(httpStatus.CREATED).send(overallLengthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getRearTyreController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getRearTyreService());
});
/**
  * Create Overall Length Controller
  */
const createFrontTyreController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await WheelFactorAutoValueService.createFrontTyreService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontTyreController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getFrontTyreService());
});
/**
  * Create Overall Length Controller
  */
const createFrontWheelDriveController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await WheelFactorAutoValueService.createFrontWheelDriveService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontWheelDriveController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getFrontWheelDriveService());
});
/**
  * Create Overall Length Controller
  */
const createAlloyWheelController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await WheelFactorAutoValueService.createAlloyWheelService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAlloyWheelController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getAlloyWheelService());
});
/**
  * Create Overall Length Controller
  */
const createSteelRimsController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await WheelFactorAutoValueService.createSteelRimsService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSteelRimsController = catchAsync(async (req, res) => {
  res.json(await WheelFactorAutoValueService.getSteelRimsService());
});

module.exports = {
  createFrontWheelTypeController,
  getFrontWheelTypeController,
  createRearWheelTypeController,
  getRearWheelTypeController,
  createFrontWheelSizeController,
  getFrontWheelSizeController,
  createRearWheelSizeController,
  getRearWheelSizeController,
  createRearTyreController,
  getRearTyreController,
  createFrontTyreController,
  getFrontTyreController,
  createFrontWheelDriveController,
  getFrontWheelDriveController,
  createAlloyWheelController,
  getAlloyWheelController,
  createSteelRimsController,
  getSteelRimsController,
};
