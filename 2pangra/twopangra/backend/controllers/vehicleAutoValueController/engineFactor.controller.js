/**
 * Copyright (C) Two Pangra
 */

/**
 * Dimensional Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { EngineFactorAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create instrumentation Controller
  */
const createDisplacementController = catchAsync(async (req, res) => {
  const value = await EngineFactorAutoValueService.createDisplacementService(req.body);
  res.status(httpStatus.CREATED).send(value);
});

/**
 * get instrumentation information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getDisplacementController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getDisplacementService());
});

/**
  * Create Overall Width Controller
  */
const createFuelEfficiencyController = catchAsync(async (req, res) => {
  const overallWidthAutoValue = await EngineFactorAutoValueService.createFuelEfficiencyService(req.body);
  res.status(httpStatus.CREATED).send(overallWidthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFuelEfficiencyController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getFuelEfficiencyService());
});

/**
  * Create Overall Length Controller
  */
const createEngineTypeController = catchAsync(async (req, res) => {
  const overallLengthAutoValue = await EngineFactorAutoValueService.createEngineTypeService(req.body);
  res.status(httpStatus.CREATED).send(overallLengthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getEngineTypeController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getEngineTypeService());
});
/**
  * Create Overall Length Controller
  */
const createNoOfCylinderController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createNoOfCylinderService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getNoOfCylinderController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getNoOfCylinderService());
});
/**
  * Create Overall Length Controller
  */
const createValvesPerCylinderController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createValvesPerCylinderService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getValvesPerCylinderController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getValvesPerCylinderService());
});
/**
  * Create Overall Length Controller
  */
const createValveConfigurationController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createValveConfigurationService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getValveConfigurationController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getValveConfigurationService());
});
/**
  * Create Overall Length Controller
  */
const createFuelSupplySystemController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createFuelSupplySystemService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFuelSupplySystemController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getFuelSupplySystemService());
});
/**
  * Create Overall Length Controller
  */
const createMaximumPowerDoddleHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createMaximumPowerDoddleHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMaximumPowerDoddleHeightController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getMaximumPowerDoddleHeightService());
});
/**
  * Create Overall Length Controller
  */
const createMaximumTorqueDoddleHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createMaximumTorqueDoddleHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMaximumTorqueDoddleHeightController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getMaximumTorqueDoddleHeightService());
});
/**
  * Create Overall Length Controller
  */
const createLubricationDoddleHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createLubricationDoddleHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getLubricationDoddleHeightController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getLubricationDoddleHeightService());
});
/**
  * Create Overall Length Controller
  */
const createEngineOilController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createEngineOilService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getEngineOilController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getEngineOilService());
});
/**
  * Create Overall Length Controller
  */
const createAirCleanerDoddleHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createAirCleanerDoddleHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAirCleanerDoddleHeightController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getAirCleanerDoddleHeightService());
});
/**
  * Create Overall Length Controller
  */
const createBoreXStrokeController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createBoreXStrokeService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBoreXStrokeController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getBoreXStrokeService());
});
/**
  * Create Overall Length Controller
  */
const createCompressionRatioController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await EngineFactorAutoValueService.createCompressionRatioService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getCompressionRatioController = catchAsync(async (req, res) => {
  res.json(await EngineFactorAutoValueService.getCompressionRatioService());
});

module.exports = {
  createDisplacementController,
  getDisplacementController,
  createFuelEfficiencyController,
  getFuelEfficiencyController,
  createEngineTypeController,
  getEngineTypeController,
  createNoOfCylinderController,
  getNoOfCylinderController,
  createValvesPerCylinderController,
  getValvesPerCylinderController,
  createValveConfigurationController,
  getValveConfigurationController,
  createFuelSupplySystemController,
  getFuelSupplySystemController,
  createMaximumPowerDoddleHeightController,
  getMaximumPowerDoddleHeightController,
  createMaximumTorqueDoddleHeightController,
  getMaximumTorqueDoddleHeightController,
  createLubricationDoddleHeightController,
  getLubricationDoddleHeightController,
  createEngineOilController,
  getEngineOilController,
  createAirCleanerDoddleHeightController,
  getAirCleanerDoddleHeightController,
  createBoreXStrokeController,
  getBoreXStrokeController,
  createCompressionRatioController,
  getCompressionRatioController
};
