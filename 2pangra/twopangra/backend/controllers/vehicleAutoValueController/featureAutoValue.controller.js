/**
 * Copyright (C) Tuki Logic
 */

/**
 * Feature Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { FeatureAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create Feature Controller
  */
const createFeatureController = catchAsync(async (req, res) => {
  const featureAutoValue = await FeatureAutoValueService.createFeatureService(req.body);
  res.status(httpStatus.CREATED).send(featureAutoValue);
});

/**
 * get feature information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFeatureController = catchAsync(async (req, res) => {
  res.json(await FeatureAutoValueService.getFeatureService());
});

module.exports = {
  createFeatureController,
  getFeatureController
};
