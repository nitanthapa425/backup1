/**
 * Copyright (C) Two Pangra
 */

/**
 * Dimensional Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { DimensionalFactorAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create instrumentation Controller
  */
const createWheelBaseController = catchAsync(async (req, res) => {
  const wheelBaseAutoValue = await DimensionalFactorAutoValueService.createWheelBaseService(req.body);
  res.status(httpStatus.CREATED).send(wheelBaseAutoValue);
});

/**
 * get instrumentation information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getWheelBaseController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getWheelBaseService());
});

/**
  * Create Overall Width Controller
  */
const createOverallWidthController = catchAsync(async (req, res) => {
  const overallWidthAutoValue = await DimensionalFactorAutoValueService.createOverallWidthService(req.body);
  res.status(httpStatus.CREATED).send(overallWidthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getOverallWidthController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getOverallWidthService());
});

/**
  * Create Overall Length Controller
  */
const createOverallLengthController = catchAsync(async (req, res) => {
  const overallLengthAutoValue = await DimensionalFactorAutoValueService.createOverallLengthService(req.body);
  res.status(httpStatus.CREATED).send(overallLengthAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getOverallLengthController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getOverallLengthService());
});
/**
  * Create Overall Length Controller
  */
const createOverallHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await DimensionalFactorAutoValueService.createOverallHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getOverallHeightController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getOverallHeightService());
});
/**
  * Create Overall Length Controller
  */
const createGroundClearanceController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await DimensionalFactorAutoValueService.createGroundClearanceService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getGroundClearanceController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getGroundClearanceService());
});
/**
  * Create Overall Length Controller
  */
const createBootSpaceController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await DimensionalFactorAutoValueService.createBootSpaceService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBootSpaceController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getBootSpaceService());
});
/**
  * Create Overall Length Controller
  */
const createKerbWeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await DimensionalFactorAutoValueService.createKerbWeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getKerbWeightController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getKerbWeightService());
});
/**
  * Create Overall Length Controller
  */
const createDoddleHeightController = catchAsync(async (req, res) => {
  const overallHeightAutoValue = await DimensionalFactorAutoValueService.createDoddleHeightService(req.body);
  res.status(httpStatus.CREATED).send(overallHeightAutoValue);
});

/**
 * get Over all width information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getDoddleHeightController = catchAsync(async (req, res) => {
  res.json(await DimensionalFactorAutoValueService.getDoddleHeightService());
});

module.exports = {
  createWheelBaseController,
  getWheelBaseController,
  createOverallWidthController,
  getOverallWidthController,
  createOverallLengthController,
  getOverallLengthController,
  createOverallHeightController,
  getOverallHeightController,
  createGroundClearanceController,
  getGroundClearanceController,
  createBootSpaceController,
  getBootSpaceController,
  createKerbWeightController,
  getKerbWeightController,
  createDoddleHeightController,
  getDoddleHeightController
};
