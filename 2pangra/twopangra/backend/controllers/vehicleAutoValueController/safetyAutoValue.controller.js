/**
 * Copyright (C) Tuki Logic
 */

/**
 * Safety Auto Value Controller
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const httpStatus = require('http-status');
const { SafetyAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create Safety Controller
  */
const createSafetyController = catchAsync(async (req, res) => {
  const safetyAutoValue = await SafetyAutoValueService.createSafetyService(req.body);
  res.status(httpStatus.CREATED).send(safetyAutoValue);
});

/**
 * get safety information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSafetyController = catchAsync(async (req, res) => {
  res.json(await SafetyAutoValueService.getSafetyService());
});

module.exports = {
  createSafetyController,
  getSafetyController
};
