/**
 * Copyright (C) Tuki Logic
 */

/**
 * Ignition Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { TechnicalAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');
const { capitalize } = require('../../common/helper');

/**
  * Create Front Brake Controller
  */
const createFrontBrakeController = catchAsync(async (req, res) => {
  const entity = req.body;

  /* Capitalize frontBrakeSystem and backBrakeSystem */
  const frontBrakeSystem = capitalize(entity.frontBrakeSystem);
  const vehicleDetail = {
    ...req.body,
    frontBrakeSystem,
  };
  const frontBrakeAutoValue = await TechnicalAutoValueService.createFrontBrakeService(vehicleDetail);
  res.status(httpStatus.CREATED).send(frontBrakeAutoValue);
});

/**
 * get back brake information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontBrakeController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getFrontBrakeService());
});

/**
  * Create Front Brake Controller
  */
const createBackBrakeController = catchAsync(async (req, res) => {
  const entity = req.body;

  /* Capitalize frontBrakeSystem and backBrakeSystem */
  const backBrakeSystem = capitalize(entity.backBrakeSystem);
  const vehicleDetail = {
    ...req.body,
    backBrakeSystem
  };
  const backBrakeAutoValue = await TechnicalAutoValueService.createBackBrakeService(vehicleDetail);
  res.status(httpStatus.CREATED).send(backBrakeAutoValue);
});

/**
 * get back brake information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBackBrakeController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getBackBrakeService());
});

/**
  * Create Front Suspension Controller
  */
const createFrontSuspensionController = catchAsync(async (req, res) => {
  const frontSuspensionAutoValue = await TechnicalAutoValueService.createFrontSuspensionService(req.body);
  res.status(httpStatus.CREATED).send(frontSuspensionAutoValue);
});

/**
 * get front suspension information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFrontSuspensionController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getFrontSuspensionService());
});

/**
  * Create Rear Suspension Controller
  */
const createRearSuspensionController = catchAsync(async (req, res) => {
  const rearSuspensionAutoValue = await TechnicalAutoValueService.createRearSuspensionService(req.body);
  res.status(httpStatus.CREATED).send(rearSuspensionAutoValue);
});

/**
 * get rear suspension information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getRearSuspensionController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getRearSuspensionService());
});

/**
  * Create No. of Gear Controller
  */
const createNoOfGearsController = catchAsync(async (req, res) => {
  const noOfGearAutoValue = await TechnicalAutoValueService.createNoOfGearsService(req.body);
  res.status(httpStatus.CREATED).send(noOfGearAutoValue);
});

/**
 * get no. of gear information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getNoOfGearsController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getNoOfGearsService());
});

/**
  * Create drive type Controller
  */
const createDriveTypeController = catchAsync(async (req, res) => {
  const driveType = await TechnicalAutoValueService.createDriveTypeService(req.body);
  res.status(httpStatus.CREATED).send(driveType);
});

/**
 * get drive type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getDriveTypeController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getDriveTypeService());
});

/**
  * Create clutch Controller
  */
const createClutchTypeController = catchAsync(async (req, res) => {
  const clutchType = await TechnicalAutoValueService.createClutchTypeService(req.body);
  res.status(httpStatus.CREATED).send(clutchType);
});

/**
 * get clutch type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getClutchTypeController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getClutchTypeService());
});

/**
  * Create Gear Pattern Controller
  */
const createGearPatternController = catchAsync(async (req, res) => {
  const gearPattern = await TechnicalAutoValueService.createGearPatternService(req.body);
  res.status(httpStatus.CREATED).send(gearPattern);
});

/**
 * get gear pattern information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getGearPatternController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getGearPatternService());
});

/**
  * Create headlight Controller
  */
const createHeadlightController = catchAsync(async (req, res) => {
  const headlight = await TechnicalAutoValueService.createHeadlightService(req.body);
  res.status(httpStatus.CREATED).send(headlight);
});

/**
 * get headlight information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getHeadlightController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getHeadlightService());
});

/**
  * Create taillight Controller
  */
const createTaillightController = catchAsync(async (req, res) => {
  const taillight = await TechnicalAutoValueService.createTaillightService(req.body);
  res.status(httpStatus.CREATED).send(taillight);
});

/**
 * get taillight information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getTaillightController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getTaillightService());
});

/**
  * Create starter Controller
  */
const createStarterController = catchAsync(async (req, res) => {
  const starter = await TechnicalAutoValueService.createStarterService(req.body);
  res.status(httpStatus.CREATED).send(starter);
});

/**
 * get starter information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getStarterController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getStarterService());
});

/**
  * Create battery Controller
  */
const createBatteryController = catchAsync(async (req, res) => {
  const battery = await TechnicalAutoValueService.createBatteryService(req.body);
  res.status(httpStatus.CREATED).send(battery);
});

/**
 * get battery information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBatteryController = catchAsync(async (req, res) => {
  res.json(await TechnicalAutoValueService.getBatteryService());
});

module.exports = {
  createFrontBrakeController,
  getFrontBrakeController,
  createBackBrakeController,
  getBackBrakeController,
  createFrontSuspensionController,
  getFrontSuspensionController,
  createRearSuspensionController,
  getRearSuspensionController,
  createNoOfGearsController,
  getNoOfGearsController,
  createDriveTypeController,
  getDriveTypeController,
  createClutchTypeController,
  getClutchTypeController,
  createGearPatternController,
  getGearPatternController,
  createHeadlightController,
  getHeadlightController,
  createTaillightController,
  getTaillightController,
  createStarterController,
  getStarterController,
  createBatteryController,
  getBatteryController
};
