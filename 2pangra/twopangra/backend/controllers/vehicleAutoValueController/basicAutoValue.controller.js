/**
 * Copyright (C) Tuki Logic
 */

/**
 * Vehicle Auto Value Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const { BasicAutoValueService } = require('../../services');
const catchAsync = require('../../utils/catchAsync');

/**
  * Create Vehicle Type Controller
  */
const createVehicleTypeController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createVehicleTypeService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get vechicle type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleTypeController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getVehicleTypeService());
});

/**
  * Create Transmission Type Controller
  */
const createTransmissionTypeController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createTransmissionTypeService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get transmission type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getTransmissionTypeController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getTransmissionTypeService());
});

/**
  * Create Fuel Type Controller
  */
const createFuelTypeController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createFuelTypeService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get fuel type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFuelTypeController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getFuelTypeService());
});

/**
  * Create Body Type Controller
  */
const createBodyTypeController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createBodyTypeService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get body type information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBodyTypeController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getBodyTypeService());
});

/**
  * Create Seating Capacity Controller
  */
const createSeatingCapacityController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createSeatingCapacityService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get seating capacity information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSeatingCapacityController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getSeatingCapacityService());
});

/**
  * Create Fuel Capacity Controller
  */
const createFuelCapacityController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createFuelCapacityService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get fuel capacity information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getFuelCapacityController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getFuelCapacityService());
});

/**
  * Create Mileage Controller
  */
const createMileageController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createMileageService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get mileage information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMileageController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getMileageService());
});
/**
  * Create Mileage Controller
  */
const createMarketPriceController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createMarketPriceService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get mileage information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMarketPriceController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getMarketPriceService());
});
/**
  * Create Other Controller
  */
const createOtherDetailController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createOtherDetailService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get Other information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getOtherDetailController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getOtherDetailService());
});

/**
  * Create Other Controller
  */
const createMotorPowerController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createMotorPowerService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get Other information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMotorPowerController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getMotorPowerService());
});

/**
  * Create Other Controller
  */
const createSpeedometerController = catchAsync(async (req, res) => {
  const result = await BasicAutoValueService.createSpeedometerService(req.body);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get Other information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSpeedometerController = catchAsync(async (req, res) => {
  res.json(await BasicAutoValueService.getSpeedometerService());
});

module.exports = {
  createVehicleTypeController,
  getVehicleTypeController,
  createTransmissionTypeController,
  getTransmissionTypeController,
  createFuelTypeController,
  getFuelTypeController,
  createBodyTypeController,
  getBodyTypeController,
  createSeatingCapacityController,
  getSeatingCapacityController,
  createFuelCapacityController,
  getFuelCapacityController,
  createMileageController,
  getMileageController,
  createMarketPriceController,
  getMarketPriceController,
  createOtherDetailController,
  getOtherDetailController,
  createMotorPowerController,
  getMotorPowerController,
  createSpeedometerController,
  getSpeedometerController
};
