/**
 * Copyright (C) Two Pangra
 */

/**
 * the user controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const httpStatus = require('http-status');
// eslint-disable-next-line no-use-before-define
const { messageBirdApiKey } = require('../config/default');
const { UserService } = require('../services');
const { User } = require('../models');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');
// eslint-disable-next-line import/order
const messagebird = require('messagebird')(messageBirdApiKey);
/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getUserProfileList(search, options));
});

/* Get Deleted User List   */
const getDeletedUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getDeletedUserProfileList(search, options));
});

/* Get User Profile By Id */
const getUserProfileByIdController = catchAsync(async (req, res) => {
  const userId = req.params.id;
  res.send(await UserService.getUserProfileByIdService(userId));
});

/* Get User Profile By Id */
const updateUserProfileByIdController = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const updatedBy = req.user.id;
  const userId = req.params.id;
  const dateOfBirth = new Date(req.body.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');
  const userDetail = {
    ...req.body,
    fullName,
    updatedBy,
    dob
  };

  res.send(await UserService.updateUserProfileByIdService(userId, userDetail));
});

/* Get User Profile By Id */
const deleteUserProfileByIdController = catchAsync(async (req, res) => {
  const deleteId = req.params.id;
  const userId = req.user.id;
  res.send(await UserService.deleteUserProfileByIdService(userId, deleteId));
});

/* Get Deleted User Profile By Id */
const getDeletedUserById = catchAsync(async (req, res) => {
  const userId = req.params.id;
  res.send(await UserService.getDeletedUserById(userId));
});

/* Get User Profile By Id */
const revertDeletedUserById = catchAsync(async (req, res) => {
  const adminId = req.user.id;
  const userId = req.params.id;
  res.send(await UserService.revertDeletedUserById(userId, adminId));
});

// eslint-disable-next-line no-unused-vars
const sendMessage = catchAsync(async (req, res) => {
  // const { number } = req.body;

  // // eslint-disable-next-line no-console
  // console.log(res);

  // messagebird.verify.create(number, {
  //   template: 'Your verification code is %token'
  // });
  const { number } = req.body;

  // Make request to Verify API
  messagebird.verify.create(
    number,
    {
      originator: 'Code',
      template: 'Your verification code is %token.'
    },
    (err, response) => {
      if (err) {
        // Request has failed
        // eslint-disable-next-line no-console
        console.log(err);
        return { error: err.errors[0].description };
      }
      // Request was successful
      // eslint-disable-next-line no-console
      console.log(response);
      const { id } = response;
      const values = {
        ...req.body,
        id
      };
      User.create(values);
      return { id: response.id };
    }
  );
});

// eslint-disable-next-line no-unused-vars
const verifyOTPcode = catchAsync(async (req, res) => {
  const { id } = req.body;
  const { token } = req.body;
  messagebird.verify.verify(id, token, (err, response) => {
    if (err) {
      // Verification has failed
      // eslint-disable-next-line no-console
      console.log(err);
      return { error: err.errors[0].description, id };
    }
    // Verification was successful
    // eslint-disable-next-line no-console
    console.log(response);
    return response;
  });
});

module.exports = {
  getUserProfileList,
  getDeletedUserProfileList,
  getUserProfileByIdController,
  updateUserProfileByIdController,
  deleteUserProfileByIdController,
  getDeletedUserById,
  revertDeletedUserById,
  sendMessage,
  verifyOTPcode
};
