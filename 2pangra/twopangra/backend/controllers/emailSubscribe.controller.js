// const httpStatus = require('http-status');
// const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { emailSubscribeService } = require('../services');

const collectAllEmail = catchAsync(async (req, res) => {
  res.send(await emailSubscribeService.collectAllEmail(req.body));
});

module.exports = {
  collectAllEmail,
};
