/**
 * Copyright (C) Two Pangra
 */

/**
 * the user controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const httpStatus = require('http-status');
const { ClientService } = require('../services');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');

/**
 * does sign up process
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const registerClient = catchAsync(async (req, res) => {
  res.json(await ClientService.registerClient(req.body));
});

/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getClientProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await ClientService.getClientProfileList(search, options));
});

/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getDeletedClientProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await ClientService.getDeletedClientProfileList(search, options));
});

/* Get User Profile By Id */
const getClientProfileByIdController = catchAsync(async (req, res) => {
  const clientId = req.params.id;
  res.send(await ClientService.getClientProfileByIdService(clientId));
});

/* Get Deleted Client Profile By Id */
const getDeletedClientProfileById = catchAsync(async (req, res) => {
  const clientId = req.params.id;
  res.send(await ClientService.getDeletedClientProfileById(clientId));
});

/* Updte Client Profile By Id */
const updateClientProfileByIdController = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName !== ''
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const userId = req.params.id;
  const dateOfBirth = new Date(req.body.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');
  const userDetail = {
    ...req.body,
    fullName,
    dob
  };

  res.send(await ClientService.updateClientProfileByIdService(userId, userDetail));
});

/* Get login Client Profile */
const getClientProfile = catchAsync(async (req, res) => {
  const clientId = req.user.id;
  res.send(await ClientService.getClientProfile(clientId));
});

/* Updte login Client Profile */
const updateClientProfile = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName !== ''
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const userId = req.user.id;
  const userDetail = {
    ...req.body,
    fullName
  };

  res.send(await ClientService.updateClientProfile(userId, userDetail));
});

/* Get User Profile By Id */
const deleteClientProfileByIdController = catchAsync(async (req, res) => {
  const deleteId = req.params.id;
  const userId = req.user.id;
  res.send(await ClientService.deleteClientProfileByIdService(userId, deleteId));
});

/* Get Client Profile By Id */
const revertDeletedClientById = catchAsync(async (req, res) => {
  const adminId = req.user.id;
  const clientId = req.params.id;
  res.send(await ClientService.revertDeletedClientById(clientId, adminId));
});

/* Send OTP on Client Phone  */
const sendOTPSms = catchAsync(async (req, res) => {
  const value = req.body;
  res.send(await ClientService.sendOTPSms(value));
});

/**
 * client login with number password
 * @param req the http request
 * @param res the http response
 */
const clientLogin = catchAsync(async (req, res) => {
  res.json(await ClientService.clientLogin(req.body));
});

/**
 * client login with number password
 * @param req the http request
 * @param res the http response
 */
const verifyOTPSms = catchAsync(async (req, res) => {
  res.json(await ClientService.verifyOTPSms(req.body));
});

/**
 * client login with number password
 * @param req the http request
 * @param res the http response
 */
const getBalance = catchAsync(async (req, res) => {
  res.json(await ClientService.getBalance());
});

/**
 * client forgot password reset  with number
 * @param req the http request
 * @param res the http response
 */
const forgotClientPassword = catchAsync(async (req, res) => {
  res.json(await ClientService.forgotClientPassword(req.query));
});

/**
 * client reset password with mobile number
 * @param req the http request
 * @param res the http response
 */
const resetClientPassword = catchAsync(async (req, res) => {
  res.json(await ClientService.resetClientPassword(req.body));
});

/**
 * handles changePassword
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const changeClientPassword = catchAsync(async (req, res) => {
  res.json(await ClientService.changeClientPassword(req.user.id, req.body));
});

/**
 * handles changePassword
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const permanentDeleteClientById = catchAsync(async (req, res) => {
  res.json(await ClientService.permanentDeleteClientById(req.params.id));
});

/**
 * handles client Logout
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const clientLogout = catchAsync(async (req, res) => {
  res.json(await ClientService.clientLogout(req.user.id));
});
/**
 * create customer location
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createLocation = catchAsync(async (req, res) => {
  res.json(await ClientService.createLocation(req.user.id, req.body));
});

/**
 * Get customer locations
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getLocations = catchAsync(async (req, res) => {
  res.json(await ClientService.getLocations(req.user.id));
});

/**
 * Get customer locations
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getLocationsById = catchAsync(async (req, res) => {
  res.json(await ClientService.getLocationsById(req.user.id, req.params.id));
});

/**
 * Get customer locations
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateLocationsById = catchAsync(async (req, res) => {
  res.json(await ClientService.updateLocationsById(req.user.id, req.params.id, req.body));
});
/**
 * Get customer locations
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteLocationsById = catchAsync(async (req, res) => {
  res.json(await ClientService.deleteLocationsById(req.user.id, req.params.id));
});

module.exports = {
  registerClient,
  getClientProfileList,
  getDeletedClientProfileList,
  getClientProfileByIdController,
  updateClientProfileByIdController,
  deleteClientProfileByIdController,
  getDeletedClientProfileById,
  revertDeletedClientById,
  getClientProfile,
  updateClientProfile,
  clientLogin,
  sendOTPSms,
  verifyOTPSms,
  getBalance,
  forgotClientPassword,
  resetClientPassword,
  changeClientPassword,
  permanentDeleteClientById,
  clientLogout,
  createLocation,
  getLocations,
  getLocationsById,
  updateLocationsById,
  deleteLocationsById
};
