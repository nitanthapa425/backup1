/**
 * Copyright (C) Two Pangra
 */

/**
 * the controller entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports.userProfileController = require('./user.controller');
module.exports.clientProfileController = require('./client.controller');
module.exports.securityController = require('./security.controller');
module.exports.brandVehicleController = require('./brandVehicle.controller');
module.exports.vehicleDetailController = require('./vehicleDetail.controller');
module.exports.imageUploadController = require('./imageUpload.controller');
module.exports.basicAutoValueController = require('./vehicleAutoValueController/basicAutoValue.controller');
module.exports.instrumentationAutoValueController = require('./vehicleAutoValueController/instrumentationAutoValue.controller');
module.exports.dimensionalAutoValueController = require('./vehicleAutoValueController/dimensionalFactor.controller');
module.exports.engineFactorAutoValueController = require('./vehicleAutoValueController/engineFactor.controller');
module.exports.wheelFactorAutoValueController = require('./vehicleAutoValueController/wheelFactor.controller');
module.exports.safetyAutoValueController = require('./vehicleAutoValueController/safetyAutoValue.controller');
module.exports.featureAutoValueController = require('./vehicleAutoValueController/featureAutoValue.controller');
module.exports.ignitionAutoValueController = require('./vehicleAutoValueController/ignitionAutoValue.controller');
module.exports.technicalAutoValueController = require('./vehicleAutoValueController/technicalAutoValue.controller');
module.exports.draftVehicleController = require('./draftVehicle.controller');
module.exports.vehicleSellController = require('./vehicleSell.controller');
module.exports.geoLocationController = require('./geolocation.controller');
module.exports.emailSubscribeController = require('./emailSubscribe.controller');
module.exports.commentController = require('./comment.controller');
