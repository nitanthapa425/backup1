/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const _ = require('lodash');
const { BrandVehicle, BrandModel } = require('../models');
const BrandVehicleService = require('../services/brandVehicle.service');
const catchAsync = require('../utils/catchAsync');
/**
 * post BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createBrandVehicle = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const vehicle = _.extend(entity, {
    brandVehicleName: entity.brandVehicleName,
    createdBy: userId,
    createdAt: Date.now()
  });
  const brandVehicle = await BrandVehicleService.createBrandVehicle(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 * post Brand Model information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createBrandModelController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const brand = await BrandVehicle.findById(entity.brandId);

  const vehicle = _.extend(entity, {
    modelName: entity.modelName,
    createdBy: userId,
    createdAt: Date.now(),
    updatedAt: Date.now(),
    brandName: brand.brandVehicleName
  });
  const brandVehicle = await BrandVehicleService.createBrandModelService(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 *  Updates Brand Model
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateBrandModelController = catchAsync(async (req, res) => {
  const modelId = req.params.id;
  const { modelName } = req.body;
  const userId = req.user.id;
  const modelDetail = {
    ...req.body,
    modelName,
    updatedBy: userId
  };

  res.json(await BrandVehicleService.updateBrandModelService(modelId, modelDetail));
});

/**
 * post Brand Model information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createBrandSubModelController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const brandModel = await BrandModel.findById(entity.modelId);

  const vehicle = _.extend(entity, {
    varientName: entity.varientName,
    createdBy: userId,
    createdAt: Date.now(),
    updatedAt: Date.now(),
    modelName: brandModel.modelName,
    brandName: brandModel.brandName
  });
  const brandVehicle = await BrandVehicleService.createBrandSubModelService(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 *  Updates Sub Brand Model
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateSubBrandModelController = catchAsync(async (req, res) => {
  const subModelId = req.params.id;
  const { varientName } = req.body;
  const userId = req.user.id;
  const variantDetail = {
    ...req.body,
    varientName,
    updatedBy: userId
  };
  res.json(await BrandVehicleService.updateSubBrandModelService(subModelId, variantDetail));
});

/**
 * get Brand Model By Brand Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandModelByBrandIdController = catchAsync(async (req, res) => {
  const { brandId } = req.params;
  res.json(await BrandVehicleService.getBrandModelByBrandIdService(brandId));
});

/**
 * get Brand Model By Brand Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandSubModelByModelIdController = catchAsync(async (req, res) => {
  const { modelId } = req.params;
  res.json(await BrandVehicleService.getBrandSubModelByModelIdService(modelId));
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandVehicleDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandVehicleService.getBrandVehicleDetails(options, search));
});

/**
 * get brand information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandWithoutAuth = catchAsync(async (req, res) => {
  res.json(await BrandVehicleService.getBrandWithoutAuth());
});

const getBrandWithoutAuthLimit = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandVehicleService.getBrandWithoutAuthLimit(options, search));
});

const getHomePageBrandWithoutAuth = catchAsync(async (req, res) => {
  res.json(await BrandVehicleService.getHomePageBrandWithoutAuth());
});

/**
 * get BrandVehicle information By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandVehicleDetail = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandVehicleService.getBrandVehicleDetail(id));
});

/**
 *  Updates BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateBrandVehicle = catchAsync(async (req, res) => {
  const brandId = req.params.id;
  const userId = req.user.id;
  const { brandVehicleName } = req.body;
  const brandDetail = {
    ...req.body,
    brandVehicleName,
    updatedBy: userId
  };

  res.json(await BrandVehicleService.updateBrandVehicle(brandId, brandDetail));
});

/**
 *  Deletes BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteBrandVehicle = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandVehicleService.deleteBrandVehicle(req.params.id, userId));
});

/**
 * add Brand Model in home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addBrandInHomePage = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandVehicleService.addBrandInHomePage(req.params.id, userId));
});
/**
 * get Brand Model By Brand Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeBrandInHomePage = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandVehicleService.removeBrandInHomePage(req.params.id, userId));
});

/**
 * Add Brand Image in Brand BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const uploadBrandImage = catchAsync(async (req, res) => {
  const entity = {
    ...req.body,
    uploadBrandImage: req.file.originalname && req.file.originalname.length > 0 ? req.file.originalname : ''
  };
  res.json(await BrandVehicleService.uploadBrandImages(req.params.brandVehicleId, entity));
});

/**
 * get All information of  Brand Model  information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllBrandModelController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandVehicleService.getAllBrandModelService(search, options));
});

/**
 * get All information of Sub Brand Model  information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllBrandSubModelController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandVehicleService.getAllSubBrandModelService(search, options));
});

/**
 * get Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandModelByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandVehicleService.getBrandModelByIdService(id));
});

/**
 * get Sub Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSubBrandModelByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandVehicleService.getSubBrandModelByIdService(id));
});

/**
 * Delete Sub Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteSubBrandModelByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await BrandVehicleService.deleteSubBrandModelByIdService(id, userId));
});

/**
 * Delete  Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteBrandModelController = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await BrandVehicleService.deleteBrandModelByIdService(id, userId));
});

/**
 * Create Fuel Capacity Controller
 */
const creatVehicleNameValueController = catchAsync(async (req, res) => {
  const { variantName } = req.body;
  const variantDetail = {
    ...req.body,
    variantName
  };
  const result = await BrandVehicleService.creatVehicleNameValueService(variantDetail);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get fuel capacity information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleNameValueController = catchAsync(async (req, res) => {
  res.json(await BrandVehicleService.getVehicleNameValueService());
});
module.exports = {
  createBrandVehicle,
  getBrandVehicleDetails,
  getBrandVehicleDetail,
  updateBrandVehicle,
  deleteBrandVehicle,
  addBrandInHomePage,
  removeBrandInHomePage,
  uploadBrandImage,
  createBrandModelController,
  createBrandSubModelController,
  getBrandModelByBrandIdController,
  getBrandSubModelByModelIdController,
  getAllBrandModelController,
  getBrandModelByIdController,
  getSubBrandModelByIdController,
  getAllBrandSubModelController,
  deleteSubBrandModelByIdController,
  deleteBrandModelController,
  updateBrandModelController,
  updateSubBrandModelController,
  creatVehicleNameValueController,
  getVehicleNameValueController,
  getBrandWithoutAuth,
  getHomePageBrandWithoutAuth,
  getBrandWithoutAuthLimit
};
