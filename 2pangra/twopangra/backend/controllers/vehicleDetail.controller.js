/**
 * Copyright (C) Two Pangra
 */

/**
 * Vehicle Detail Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const _ = require('lodash');
const { BrandVehicle, BrandModel, VarientModel } = require('../models');
const vehicleDetailService = require('../services/vehicleDetail.service');
const BrandVehicleService = require('../services/brandVehicle.service');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');

/**
 * post BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

/**
 * Create ClientProfile
 */
const createVehicleDetail = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const brand = await BrandVehicleService.getBrandVehicleDetail(entity.brandId);
  const brandModel = await BrandVehicleService.getBrandModelByIdService(entity.modelId);
  let brandSubModel = {};
  if (entity.varientId !== '') {
    brandSubModel = await BrandVehicleService.getSubBrandModelByIdService(entity.varientId);
  }

  // const date = new Date(entity.makeYear);
  // const makeYears = date.getFullYear();

  // const newDate = date.setDate(date.getDate() + 1);
  // const timeFormat = new Date(newDate);
  // const makeYearDate = timeFormat.toISOString();

  // const makeYears = timeFormat.getFullYear();

  const otherDetail = typeof entity.otherDetail !== 'undefined' && entity.otherDetail !== '' ? entity.otherDetail.concat(' ') : '';
  const varient = brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' && brandSubModel.varientName !== ''
    ? brandSubModel.varientName.concat(' ')
    : ' ';
  const modelShowName = varient !== '' ? brandModel.modelName.concat('') : brandModel.modelName;

  /* Capitalize frontBrakeSystem and backBrakeSystem */
  const frontBrakeSystem = capitalize(entity.technicalFactor.frontBrakeSystem);
  const backBrakeSystem = capitalize(entity.technicalFactor.backBrakeSystem);

  const vehicle = _.extend(entity, {
    createdBy: userId,
    updatedBy: userId,
    brandName: brand.brandVehicleName,
    modelName: brandModel.modelName,
    varientName:
      brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' ? brandSubModel.varientName : '',
    // eslint-disable-next-line prefer-template
    vehicleName: `${
      brand.brandVehicleName.concat(' ') + modelShowName.concat(' ') + varient.concat(' ') + otherDetail + entity.engineFactor.displacement
    }cc`,
    // makeYear: makeYearDate,
    technicalFactor: {
      ...entity.technicalFactor,
      frontBrakeSystem,
      backBrakeSystem
    }
  });

  const vehicleDetail = await vehicleDetailService.createVehicleDetail(vehicle);
  res.status(httpStatus.CREATED).send(vehicleDetail);
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await vehicleDetailService.getVehicleDetails(search, options));
});

/**
 * Get Specific Vehicle Detail by Id
 */
const getVehicleDetail = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await vehicleDetailService.getVehicleDetail(id));
});

/**
 *  Updates BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateVehicleDetail = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const vehicleId = req.params.id;

  const brand = await BrandVehicle.findById(entity.brandId);
  const brandModel = await BrandModel.findById(entity.modelId);
  let brandSubModel = {};
  if (entity.varientId !== '') {
    brandSubModel = await VarientModel.findById(entity.varientId);
  }

  const otherDetail = typeof entity.otherDetail !== 'undefined' && entity.otherDetail !== '' ? entity.otherDetail.concat(' ') : '';
  const varient = brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' && brandSubModel.varientName !== ''
    ? brandSubModel.varientName.concat(' ')
    : ' ';
  const modelShowName = varient !== '' ? brandModel.modelName.concat('') : brandModel.modelName;

  /* Capitalize frontBrakeSystem and backBrakeSystem */
  const frontBrakeSystem = capitalize(entity.technicalFactor.frontBrakeSystem);
  const backBrakeSystem = capitalize(entity.technicalFactor.backBrakeSystem);

  const vehicle = _.extend(entity, {
    createdBy: userId,
    updatedBy: userId,
    brandName: brand.brandVehicleName,
    modelName: brandModel.modelName,
    varientName:
      brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' ? brandSubModel.varientName : '',
    // eslint-disable-next-line prefer-template
    vehicleName: `${
      brand.brandVehicleName.concat(' ') + modelShowName.concat(' ') + varient.concat(' ') + otherDetail + entity.engineFactor.displacement
    }cc`,
    technicalFactor: {
      ...entity.technicalFactor,
      frontBrakeSystem,
      backBrakeSystem
    }
  });
  res.json(await vehicleDetailService.updateVehicleDetail(vehicleId, vehicle));
});

/**
 *  Deletes BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteVehicleDetail = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await vehicleDetailService.deleteVehicleDetail(id, userId));
});

/**
 *  Upload Image
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const uploadVehicleImages = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await vehicleDetailService.uploadVehicleImages(id));
});

/**
 *  Search  BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleDraftDetailsController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await vehicleDetailService.getVehicleDraftDetailsService(search, options));
});

/**
 * Get Specific Draft Vehicle Detail by Id
 */
const getVehicleDraftDetailsByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await vehicleDetailService.getVehicleDraftDetailsByIdService(id));
});

/**
 * create Vehicle Draft to  Vehicle Service Detail by Id
 */
const createVehicleDraftToVehicleController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const draftId = req.params.id;

  const brand = await BrandVehicleService.getBrandVehicleDetail(entity.brandId);
  const brandModel = await BrandVehicleService.getBrandModelByIdService(entity.modelId);
  let brandSubModel = {};
  if (entity.varientId !== '') {
    brandSubModel = await BrandVehicleService.getSubBrandModelByIdService(entity.varientId);
  }

  // const date = new Date(entity.makeYear);
  // const makeYears = date.getFullYear();

  // const newDate = date.setDate(date.getDate() + 1);
  // const timeFormat = new Date(newDate);
  // const makeYearDate = timeFormat.toISOString();

  const otherDetail = typeof entity.otherDetail !== 'undefined' && entity.otherDetail !== '' ? entity.otherDetail.concat(' ') : '';
  const varient = brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' && brandSubModel.varientName !== ''
    ? brandSubModel.varientName.concat(' ')
    : ' ';
  const modelShowName = varient !== '' ? brandModel.modelName.concat('') : brandModel.modelName;

  /* Capitalize frontBrakeSystem and backBrakeSystem */
  const frontBrakeSystem = capitalize(entity.technicalFactor.frontBrakeSystem);
  const backBrakeSystem = capitalize(entity.technicalFactor.backBrakeSystem);

  const vehicle = _.extend(entity, {
    createdBy: userId,
    updatedBy: userId,
    brandName: brand.brandVehicleName,
    modelName: brandModel.modelName,
    varientName:
      brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' ? brandSubModel.varientName : '',
    // eslint-disable-next-line prefer-template
    vehicleName: `${
      brand.brandVehicleName.concat(' ') + modelShowName.concat(' ') + varient.concat(' ') + otherDetail + entity.engineFactor.displacement
    }cc`,
    // makeYear: makeYearDate,
    technicalFactor: {
      ...entity.technicalFactor,
      frontBrakeSystem,
      backBrakeSystem
    }
  });
  res.json(await vehicleDetailService.createVehicleDraftToVehicleService(draftId, vehicle));
});

const getAllVehicleValueController = catchAsync(async (req, res) => {
  res.send(await vehicleDetailService.getAllVehicleValueService());
});

module.exports = {
  createVehicleDetail,
  getVehicleDetails,
  getVehicleDetail,
  updateVehicleDetail,
  deleteVehicleDetail,
  uploadVehicleImages,
  getVehicleDraftDetailsController,
  getVehicleDraftDetailsByIdController,
  createVehicleDraftToVehicleController,
  getAllVehicleValueController
};
