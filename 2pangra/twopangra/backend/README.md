# Dui-Pangra

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:6514f786bd544b31855cc62da7e4744a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:6514f786bd544b31855cc62da7e4744a?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:6514f786bd544b31855cc62da7e4744a?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Susan7872/twopangra.git
git branch -M main
git push -uf origin main
```

## To Check for Lint

npm run lint

## To Fix lint error automatically as much as possible

npm run lint-fix

# Add env file

Copy (ctrl + c) env example file on the root folder and paste(ctrl + v) on the same root folder
Change name of the folder to .env

## To run backend

npm start

## To test api

1. Go to docx/api-docs
2. Import both json file into postman and test api

## .env

PORT=3100
MONGODB_URI=mongodb://admin:admin@cluster0-shard-00-00.3rnsz.mongodb.net:27017,cluster0-shard-00-01.3rnsz.mongodb.net:27017,cluster0-shard-00-02.3rnsz.mongodb.net:27017/2pangra?ssl=true&replicaSet=atlas-wumtl1-shard-0&authSource=admin&retryWrites=true&w=majority
EMAIL_SERVER = smtp.gmail.com
EMAIL_PORT = 465
EMAIL_USER = support@arsgroup.com.au
EMAIL_PASS = Admin@ARS1
CLOUDINARY_CLOUD_NAME = doynwnic5
CLOUDINARY_API_KEY = 772325534715938
CLOUDINARY_API_SECRET = -kUEVq9egSMerEy5VbgTFULjoeA

#message bird api key
message-bird-api-key=JdrLK4GSZQQEEijEHxFiqNpQY