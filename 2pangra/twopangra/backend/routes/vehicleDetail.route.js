/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { vehicleDetailController } = require('../controllers');
const validate = require('../middlewares/validate');
const { uploadMultipleFile } = require('../middlewares/uploadImageMiddleware');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');
const { createVehicleDetailValidate } = require('../validations/validateSchema');

const {
  expressAuthentication,
  isAuthenticated,
  isAuthorized,
} = require('../middlewares/auth');

const router = express.Router();

router
  .route('/vehicleDetail')
  .get(validate(paramsValidation.pagination), paginate, vehicleDetailController.getVehicleDetails)
  .post(
    validate(createVehicleDetailValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleDetailController.createVehicleDetail
  );

router
  .route('/vehicleDetail/:id')
  .get(vehicleDetailController.getVehicleDetail)
  .put(
    validate(createVehicleDetailValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleDetailController.updateVehicleDetail
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleDetailController.deleteVehicleDetail
  );

router
  .route('/uploadVehicleImage/vehicleDetail/:vehicleDetail')
  .post(uploadMultipleFile, vehicleDetailController.uploadVehicleImages);

router
  .route('/vehicleDraftDetails')
  .get(validate(paramsValidation.pagination), paginate, vehicleDetailController.getVehicleDraftDetailsController);

router
  .route('/vehicleDraftDetails/:id')
  .get(vehicleDetailController.getVehicleDraftDetailsByIdController);

router
  .route('/saveDraftToSource/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleDetailController.createVehicleDraftToVehicleController
  );

router
  .route('/getAllVehicleValue')
  .get(validate(paramsValidation.pagination), paginate, vehicleDetailController.getAllVehicleValueController);

module.exports = router;
