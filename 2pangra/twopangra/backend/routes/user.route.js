/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { userProfileController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router
  .route('/user')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    userProfileController.getUserProfileList
  );

router
  .route('/user/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    userProfileController.getUserProfileByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    userProfileController.updateUserProfileByIdController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    userProfileController.deleteUserProfileByIdController
  );

router
  .route('/deletedUser')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    paginate,
    userProfileController.getDeletedUserProfileList
  );

router
  .route('/deletedUser/:id')
  .get(expressAuthentication, isAuthenticated, isAuthorized('SUPER_ADMIN'), userProfileController.getDeletedUserById);

router
  .route('/revertUser/:id')
  .post(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    userProfileController.revertDeletedUserById
  );

router.route('/sendMessage').post(userProfileController.sendMessage);

module.exports = router;
