/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { securityController } = require('../controllers');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const validate = require('../middlewares/validate');
const paramsValidation = require('../validations/validateSchema');
const { paginate } = require('../middlewares/pagination');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router.route('/login').post(securityController.login);

router.route('/signUp').post(securityController.signUp);

router.route('/me/profile').get(expressAuthentication, isAuthenticated, securityController.getMyProfile);

router
  .route('/me/profile')
  .put(
    validate(paramsValidation.userUpdate),
    expressAuthentication,
    isAuthenticated,
    securityController.updateMyProfile
  );

router.route('/confirmEmail').post(securityController.confirmEmail);

router.route('/forgotPassword').post(securityController.forgotPassword);

router.route('/resetPassword').post(securityController.resetPassword);

router.route('/changePassword').post(expressAuthentication, isAuthenticated, securityController.changePassword);

router.route('/logout').post(expressAuthentication, isAuthenticated, securityController.logout);

router.route('/validate-user').get(expressAuthentication, securityController.validateUser);

router
  .route('/log-management')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    paginate,
    securityController.getAllLogManagement
  );

router
  .route('/notification')
  .get(expressAuthentication, isAuthenticated, paginate, securityController.getAllNotification);

router
  .route('/notification/mark-as-read')
  .patch(expressAuthentication, isAuthenticated, securityController.markNotificationAsRead);

router
  .route('/notification/mark-as-unread')
  .patch(expressAuthentication, isAuthenticated, securityController.markNotificationAsUnread);

router
  .route('/notification/delete')
  .delete(expressAuthentication, isAuthenticated, securityController.deleteNotification);

router
  .route('/notification/read-all')
  .patch(expressAuthentication, isAuthenticated, securityController.readAllNotification);

module.exports = router;
