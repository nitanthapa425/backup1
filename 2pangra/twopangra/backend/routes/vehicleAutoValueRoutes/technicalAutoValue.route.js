/**
 * Copyright (C) Two Pangra
 */

/**
 * the Ignition Auto Value Route
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const express = require('express');
const { technicalAutoValueController } = require('../../controllers');
const { fileUploadMiddleware } = require('../../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/frontBrake')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createFrontBrakeController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getFrontBrakeController
  );

router
  .route('/backBrake')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createBackBrakeController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getBackBrakeController
  );

router
  .route('/frontSuspension')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createFrontSuspensionController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getFrontSuspensionController
  );

router
  .route('/rearSuspension')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createRearSuspensionController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getRearSuspensionController
  );

router
  .route('/noOfGears')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createNoOfGearsController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getNoOfGearsController
  );

router
  .route('/driveType')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createDriveTypeController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getDriveTypeController
  );

router
  .route('/clutchType')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createClutchTypeController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getClutchTypeController
  );

router
  .route('/gearPattern')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createGearPatternController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getGearPatternController
  );

router
  .route('/headlight')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createHeadlightController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getHeadlightController
  );

router
  .route('/taillight')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createTaillightController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getTaillightController
  );

router
  .route('/starter')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createStarterController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getStarterController
  );

router
  .route('/battery')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.createBatteryController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    technicalAutoValueController.getBatteryController
  );

module.exports = router;
