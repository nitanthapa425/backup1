/**
 * Copyright (C) Two Pangra
 */

/**
 * the Instrumentation Auto Value Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { wheelFactorAutoValueController } = require('../../controllers');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/frontWheelType')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createFrontWheelTypeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getFrontWheelTypeController
  );

router
  .route('/rearWheelType')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createRearWheelTypeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getRearWheelTypeController
  );

router
  .route('/frontWheelSize')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createFrontWheelSizeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getFrontWheelSizeController
  );

router
  .route('/rearWheelSize')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createRearWheelSizeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getRearWheelSizeController
  );

router
  .route('/rearTyre')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createRearTyreController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getRearTyreController
  );
router
  .route('/frontTyre')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createFrontTyreController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getFrontTyreController
  );
router
  .route('/frontWheelDrive')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createFrontWheelDriveController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getFrontWheelDriveController
  );
router
  .route('/alloyWheel')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createAlloyWheelController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getAlloyWheelController
  );
router
  .route('/steelRims')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.createSteelRimsController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    wheelFactorAutoValueController.getSteelRimsController
  );

module.exports = router;
