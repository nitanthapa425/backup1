/**
 * Copyright (C) Two Pangra
 */

/**
 * the Instrumentation Auto Value Route
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const express = require('express');
const { dimensionalAutoValueController } = require('../../controllers');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/wheelBase')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createWheelBaseController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getWheelBaseController
  );

router
  .route('/overallWidth')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createOverallWidthController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getOverallWidthController
  );

router
  .route('/overallLength')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createOverallLengthController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getOverallLengthController
  );
router
  .route('/overallHeight')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createOverallHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getOverallHeightController
  );
router
  .route('/groundClearance')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createGroundClearanceController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getGroundClearanceController
  );
router
  .route('/bootSpace')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createBootSpaceController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getBootSpaceController
  );
router
  .route('/kerbWeight')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createKerbWeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getKerbWeightController
  );
router
  .route('/doddleHeight')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.createDoddleHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    dimensionalAutoValueController.getDoddleHeightController
  );

module.exports = router;
