/**
 * Copyright (C) Two Pangra
 */

/**
 * the Ignition Auto Value Route
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const express = require('express');
const { ignitionAutoValueController } = require('../../controllers');
const { fileUploadMiddleware } = require('../../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/ignition')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    ignitionAutoValueController.createIgnitionController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    ignitionAutoValueController.getIgnitionController
  );

module.exports = router;
