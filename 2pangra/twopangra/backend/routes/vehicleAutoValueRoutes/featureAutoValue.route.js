/**
 * Copyright (C) Two Pangra
 */

/**
 * the Feature Auto Value Route
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const express = require('express');
const { featureAutoValueController } = require('../../controllers');
const { fileUploadMiddleware } = require('../../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/feature')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    featureAutoValueController.createFeatureController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    featureAutoValueController.getFeatureController
  );

module.exports = router;
