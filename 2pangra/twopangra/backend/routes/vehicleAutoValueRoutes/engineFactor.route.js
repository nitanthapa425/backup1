/**
 * Copyright (C) Two Pangra
 */

/**
 * the Instrumentation Auto Value Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { engineFactorAutoValueController } = require('../../controllers');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/displacement')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createDisplacementController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getDisplacementController
  );

router
  .route('/fuelEfficiency')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createFuelEfficiencyController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getFuelEfficiencyController
  );

router
  .route('/engineType')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createEngineTypeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getEngineTypeController
  );
router
  .route('/noOfCylinder')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createNoOfCylinderController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getNoOfCylinderController
  );
router
  .route('/valvesPerCylinder')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createValvesPerCylinderController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getValvesPerCylinderController
  );
router
  .route('/valveConfiguration')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createValveConfigurationController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getValveConfigurationController
  );
router
  .route('/fuelSupplySystem')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createFuelSupplySystemController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getFuelSupplySystemController
  );
router
  .route('/maximumPower')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createMaximumPowerDoddleHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getMaximumPowerDoddleHeightController
  );
router
  .route('/maximumTorque')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createMaximumTorqueDoddleHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getMaximumTorqueDoddleHeightController
  );
router
  .route('/lubrication')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createLubricationDoddleHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getLubricationDoddleHeightController
  );
router
  .route('/engineOil')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createEngineOilController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getEngineOilController
  );
router
  .route('/airCleaner')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createAirCleanerDoddleHeightController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getAirCleanerDoddleHeightController
  );
router
  .route('/boreXStroke')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createBoreXStrokeController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getBoreXStrokeController
  );
router
  .route('/compressionRatio')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.createCompressionRatioController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    engineFactorAutoValueController.getCompressionRatioController
  );

module.exports = router;
