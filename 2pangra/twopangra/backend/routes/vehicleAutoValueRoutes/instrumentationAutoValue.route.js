/**
 * Copyright (C) Two Pangra
 */

/**
 * the Instrumentation Auto Value Route
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const express = require('express');
const { instrumentationAutoValueController } = require('../../controllers');
const { fileUploadMiddleware } = require('../../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../../middlewares/auth');

const router = express.Router();

router
  .route('/instrumentation')
  .post(fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    instrumentationAutoValueController.createInstrumentationController)
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    instrumentationAutoValueController.getInstrumentationController
  );

module.exports = router;
