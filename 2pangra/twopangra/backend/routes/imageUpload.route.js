/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { imageUploadController } = require('../controllers');
const { uploadMultipleFile } = require('../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated } = require('../middlewares/auth');

const router = express.Router();
router.route('/uploadImages')
  .post(
    uploadMultipleFile,
    expressAuthentication,
    isAuthenticated,
    imageUploadController.uploadImages
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    imageUploadController.getAllImages
  );

router.route('/deleteImage/:fileName')
  .delete(
    uploadMultipleFile,
    expressAuthentication,
    isAuthenticated,
    imageUploadController.deleteImages
  );

module.exports = router;
