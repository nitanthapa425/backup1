/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { brandVehicleController } = require('../controllers');
const validate = require('../middlewares/validate');
const { fileUploadMiddleware } = require('../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');

const router = express.Router();

router
  .route('/brandVehicle')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    brandVehicleController.getBrandVehicleDetails
  )
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.createBrandVehicle
  );

router.route('/brandVehicleNoAuth').get(brandVehicleController.getBrandWithoutAuth);

router.route('/brandVehicleNoAuthLimit').get(paginate, brandVehicleController.getBrandWithoutAuthLimit);

router.route('/homeBrandVehicleNoAuth').get(brandVehicleController.getHomePageBrandWithoutAuth);

router
  .route('/addBrandInHomePage/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.addBrandInHomePage
  );

router
  .route('/removeBrandInHomePage/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.removeBrandInHomePage
  );

router
  .route('/brandVehicle/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getBrandVehicleDetail
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.updateBrandVehicle
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.deleteBrandVehicle
  );

router
  .route('/vehicleModel')
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.createBrandModelController
  )
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    brandVehicleController.getAllBrandModelController
  );

router
  .route('/vehicleModel/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getBrandModelByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.updateBrandModelController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.deleteBrandModelController
  );

router
  .route('/varient')
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.createBrandSubModelController
  )
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    brandVehicleController.getAllBrandSubModelController
  );

router
  .route('/varient/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getSubBrandModelByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.updateSubBrandModelController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.deleteSubBrandModelByIdController
  );

router
  .route('/varientByModelId/:modelId')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getBrandSubModelByModelIdController
  );

router
  .route('/vehicleModelByBrandId/:brandId')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getBrandModelByBrandIdController
  );

router
  .route('/variantName')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.creatVehicleNameValueController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandVehicleController.getVehicleNameValueController
  );

module.exports = router;
