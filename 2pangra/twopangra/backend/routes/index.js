/**
 * Copyright (C) Two Pangra
 */

/**
 * the route entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const SecurityRoutes = require('./security.routes');
const UserRoute = require('./user.route');
const ClientRoute = require('./client.route');
const brandVehicleRoute = require('./brandVehicle.route');
const vehicleDetailRoute = require('./vehicleDetail.route');
const uploadImageRoute = require('./imageUpload.route');
const basicAutoValueRoute = require('./vehicleAutoValueRoutes/basicAutoValue.route');
const instrumentationAutoValueRoute = require('./vehicleAutoValueRoutes/instrumentationAutoValue.route');
const dimensionalAutoValueRoute = require('./vehicleAutoValueRoutes/dimensionalFactor.route');
const engineAutoValueRoute = require('./vehicleAutoValueRoutes/engineFactor.route');
const wheelAutoValueRoute = require('./vehicleAutoValueRoutes/wheelFactor.route');
const safetyAutoValueRoute = require('./vehicleAutoValueRoutes/safetyAutoValue.route');
const featureAutoValueRoute = require('./vehicleAutoValueRoutes/featureAutoValue.route');
const ignitionAutoValueRoute = require('./vehicleAutoValueRoutes/ignitionAutoValue.route');
const technicalAutoValueRoute = require('./vehicleAutoValueRoutes/technicalAutoValue.route');
const draftVehicleRoute = require('./draftVehicle.route');
const vehicleSellRoute = require('./vehicleSell.route');
const geoLocationRoute = require('./geolocation.route');
const emailSubscribeRoute = require('./emailSubscribe.route');
const commentRoute = require('./comment.route');

const apiRouter = express.Router();

const mainRoute = '/';
const seller = '/seller';
const client = '/client';
const vehicleAutoRoute = '/vehicleAutoValue';
const geoLocation = '/geo-location';
const emailSubscribe = '/email';
const comment = '/comment';

const defaultRoutes = [
  {
    path: mainRoute,
    route: UserRoute
  },
  {
    path: client,
    route: ClientRoute
  },
  {
    path: mainRoute,
    route: brandVehicleRoute
  },
  {
    path: mainRoute,
    route: vehicleDetailRoute
  },
  {
    path: mainRoute,
    route: uploadImageRoute
  },
  {
    path: mainRoute,
    route: draftVehicleRoute
  },
  {
    path: mainRoute,
    route: SecurityRoutes
  },
  {
    path: vehicleAutoRoute,
    route: basicAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: instrumentationAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: dimensionalAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: engineAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: wheelAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: safetyAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: featureAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: ignitionAutoValueRoute
  },
  {
    path: vehicleAutoRoute,
    route: technicalAutoValueRoute
  },
  {
    path: seller,
    route: vehicleSellRoute
  },
  {
    path: geoLocation,
    route: geoLocationRoute
  },
  {
    path: emailSubscribe,
    route: emailSubscribeRoute
  },
  {
    path: comment,
    route: commentRoute
  }
];

defaultRoutes.forEach((route) => {
  apiRouter.use(route.path, route.route);
});

module.exports = apiRouter;

// module.exports = userRouter;
