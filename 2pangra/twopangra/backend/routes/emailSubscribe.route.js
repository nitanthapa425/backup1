const express = require('express');
const validate = require('../middlewares/validate');
const paramsValidationSchema = require('../validations/validateSchema');
const { emailSubscribeController } = require('../controllers');

const router = express.Router();

router.route('/subscribe').post(validate(paramsValidationSchema.subscribeEmail), emailSubscribeController.collectAllEmail);

module.exports = router;
