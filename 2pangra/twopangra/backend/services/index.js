/**
 * Copyright (C) Two Pangra
 */

/**
 * the Service Entry Point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports.UserService = require('./user.service');
module.exports.SecurityService = require('./security.service');
module.exports.ClientService = require('./client.service');
module.exports.BrandVehicleService = require('./brandVehicle.service');
module.exports.VehicleDetailService = require('./vehicleDetail.service');
module.exports.ImageUploadService = require('./imageUpload.service');
module.exports.BasicAutoValueService = require('./vehicleAutoValueService/basicAutoValue.service');
module.exports.InstrumentationAutoValueService = require('./vehicleAutoValueService/instrumentationAutoValue.service');
module.exports.DimensionalFactorAutoValueService = require(
  './vehicleAutoValueService/dimensionalFactorAutoValue.service'
);
module.exports.EngineFactorAutoValueService = require(
  './vehicleAutoValueService/engineFactorAutoValue.service'
);
module.exports.WheelFactorAutoValueService = require(
  './vehicleAutoValueService/wheelFactorAutoValue.service'
);
module.exports.SafetyAutoValueService = require('./vehicleAutoValueService/safetyAutoValue.service');
module.exports.FeatureAutoValueService = require('./vehicleAutoValueService/featureAutoValue.service');
module.exports.IgnitionAutoValueService = require('./vehicleAutoValueService/ignitionAutoValue.service');
module.exports.TechnicalAutoValueService = require('./vehicleAutoValueService/technicalAutoValue.service');
module.exports.DraftVehicleService = require('./draftVehicle.service');
module.exports.VehicleSellService = require('./vehicleSell.service');
module.exports.GeoLocationService = require('./geoLocation.service');
module.exports.emailSubscribeService = require('./emailSubscribe.service');
module.exports.commentService = require('./comment.service');
