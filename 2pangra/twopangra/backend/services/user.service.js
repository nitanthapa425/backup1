/**
 * Copyright (C) Two Pangra
 */

/**
 * the User Service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const _ = require('lodash');
const errors = require('common-errors');
const httpStatus = require('http-status');
const helper = require('../common/helper');
const { User } = require('../models');

/* Get User data for authentication */
const getUserById = async (userId) => User.find({ _id: userId });

/**
 * Get  User Data
 * @param userId
 */
const getUserProfileList = async (search, options) => {
  search.$or = [{ isActive: true }, { isActive: { $exists: false } }];
  return User.paginate(search, options);
};

/**
 * Get Deleted User
 * @param userId
 */

const getDeletedUserProfileList = async (search, options) => {
  search.$or = [{ isActive: false }];

  return User.paginate(search, options);
};

/**
 * Get User By id
 * @param userId
 */

const getUserProfileByIdService = async (userId) => {
  let user = await User.findOne({
    _id: userId
  });
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  return user;
};

/* Update User Profile by Id */
async function updateUserProfileByIdService(userId, entity) {
  const user = await helper.ensureEntityExists(User, { _id: userId }, 'The User with Id does not exist.');

  if (!user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'User is not found. Try Refreshing the page');
  }

  _.assignIn(user, { ...entity });

  await user.save();

  return {
    status: 200,
    message: 'User has been updated successfully'
  };
}

/* Delete User Super admin by id */
/**
 * Delete Brand Model By id
 * @returns {Object} Search result
 */
async function deleteUserProfileByIdService(userId, deleteId) {
  const user = await helper.ensureEntityExists(User, { _id: deleteId }, 'The User does not exist.');
  if (user.isActive === false) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'User is already deleted.');
  } else {
    user.isActive = false;
    user.deletedBy = deleteId;
    user.updatedAt = Date.now();
    user.save();
  }

  return {
    status: 200,
    message: 'User is deleted successfully.'
  };
}

/**
 * Get Deleted User By id
 * @param userId
 */

const getDeletedUserById = async (userId) => {
  const user = await User.findOne({
    _id: userId
  });
  return user;
};

/**
 * Revert Deleted User By id
 * @param userId
 */

const revertDeletedUserById = async (userId, adminId) => {
  const user = await helper.ensureEntityExists(User, { _id: userId }, 'The User does not exist.');
  if (user.isActive === true) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'User is already active.');
  } else {
    user.isActive = true;
    user.updatedBy = adminId;
    user.updatedAt = Date.now();
    user.save();
  }

  return {
    status: 200,
    message: 'User is activated successfully.'
  };
};

module.exports = {
  getUserProfileList,
  getUserProfileByIdService,
  getDeletedUserProfileList,
  getUserById,
  updateUserProfileByIdService,
  deleteUserProfileByIdService,
  getDeletedUserById,
  revertDeletedUserById
};
