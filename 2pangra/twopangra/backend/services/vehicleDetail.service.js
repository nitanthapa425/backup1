/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const _ = require('lodash');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const {
  VehicleDetail,
  VehicleSellModel,
  draftVehicleModel,
  SeatingCapacityModel,
  FuelCapacityModel,
  VehicleTypeValueModel,
  TransmissionTypeValueModel,
  FuelTypeValueModel,
  BodyTypeModel,
  MileageModel,
  WheelBaseModel,
  OverallWidthModel,
  OverallLengthModel,
  OverallHeightModel,
  GroundClearanceModel,
  BootSpaceModel,
  KerbWeightModel,
  DoddleHeightModel,
  DisplacementModel,
  EngineTypeModel,
  NoOfCylinderModel,
  ValvesPerCylinderModel,
  ValveConfigurationModel,
  FuelSupplySystemModel,
  MaximumPowerModel,
  MaximumTorqueModel,
  LubricationModel,
  EngineOilModel,
  AirCleanerModel,
  BoreXStrokeModel,
  CompressionRatioModel,
  FrontWheelTypeModel,
  RearWheelTypeModel,
  FrontWheelSizeModel,
  RearWheelSizeModel,
  RearTyreModel,
  FrontTyreModel,
  FrontWheelDriveModel,
  SteelRimsModel,
  InstrumentationModel,
  SafetyModel,
  FeatureModel,
  IgnitionModel,
  FrontBrakeSystemModel,
  BackBrakeSystemModel,
  FrontSuspensionModel,
  RearSuspensionModel,
  NoOfGearsModel,
  DriveTypeModel,
  ClutchTypeModel,
  GearPatternModel,
  HeadlightModel,
  TaillightModel,
  StarterModel,
  BatteryModel,
  MarketPriceModel,
  OtherDetailModel,
  MotorPowerModel
} = require('../models');
// const { createVehicleDetailValidate } = require('../validations/validateSchema');
/**
 * @param {Object} entity the new vehicleDetail
 * @returns {Object} the entity
 */
const createVehicleDetail = async (tag) => {
  const value = VehicleDetail.create(tag);

  if (value) {
    await helper.createLogManagement(tag.createdBy, 'Add Data', 'Vehicle Details', 'Vehicle Detail');
  }

  return value;
};

/**
 * @returns {Object} get  all vehicleDetail
 */
const getVehicleDetails = async (search, options) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const value = VehicleDetail.paginate(search, optionValue);
  return value;
};
/**
 * @returns {Object} get  Specific Vehicle Detail
 */
const getVehicleDetail = async (tagId) => {
  const vehicleDetailById = await VehicleDetail.findOne({ _id: tagId });
  if (!vehicleDetailById) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle Detail by Id is not found Try refreshing page.');
  }
  return vehicleDetailById;
};

/**
 * handles the update  Vehicle Details
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateVehicleDetail(vehicleId, vehicleDetailBody) {
  const vehicleDetail = await helper.ensureEntityExists(VehicleDetail, { _id: vehicleId }, 'Vehicle does not exist.');

  if (!vehicleDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle is not found. Try Refreshing the page');
  }
  _.assignIn(vehicleDetail, { ...vehicleDetailBody });

  if (vehicleDetail) {
    await helper.createLogManagement(vehicleDetailBody.updatedBy, 'Update Data', 'Vehicle Details', 'Vehicle Detail');
  }

  await vehicleDetail.save();

  return {
    status: 200,
    message: 'Vehicle Details updated successfully.'
  };
}

/**
 * Delete vehicleDetail
 * @returns {Object} Search result
 */
/**
 * delete client profile by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteVehicleDetail(deleteId, userId) {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleDetail,
    { _id: deleteId },
    'The Vehicle detail does not exist.'
  );
  const sellerData = await VehicleSellModel.fineOne({ vehicleId: deleteId });
  if (sellerData) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'This vehicle detail is used in vehicle sell. First delete other seller vehicle with this vehicle detail'
    );
  }
  await VehicleSellModel.deleteOne({ vehicleId: deleteId });
  vehicleDetail.remove();

  if (vehicleDetail) {
    await helper.createLogManagement(userId, 'Delete Data', 'Vehicle Details', 'Vehicle Detail');
  }

  return {
    status: 200,
    message: 'Vehicle detail is deleted Successfully'
  };
}

deleteVehicleDetail.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/* Upload Brand Image In vehicleDetail  */
const uploadVehicleImages = async (vehicleId, tag) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleDetail,
    { _id: vehicleId },
    `The Brand Vehicle ${vehicleId} does not exist.`
  );
  vehicleDetail.vehicleImgPath.push(...tag.vehicleImgPath);

  await vehicleDetail.save();

  return vehicleDetail;
};

/**
 * Search Vehicle Detail
 * @param {String} name the any in Vehicle
 */
async function getVehicleDraftDetailsService(search, options) {
  search.$and = [
    {
      $or: [
        { status: 'draft' },
        { 'basicFactor.basicStatus': 'draft' },
        { 'optionFeature.optionStatus': 'draft' },
        { 'technicalFactor.technicalStatus': 'draft' },
        { 'dimensionalFactor.dimensionStatus': 'draft' },
        { 'engineFactor.engineStatus': 'draft' },
        { 'wheelTyreFactor.wheelTyreStatus': 'draft' }
      ]
    }
  ];
  const value = VehicleDetail.paginate(search, options);
  return value;
}

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const getVehicleDraftDetailsByIdService = async (tagId) => {
  const draftValue = await helper.ensureEntityExists(
    draftVehicleModel,
    { _id: tagId },
    'The Draft Vehicle Detail of user does not exist.'
  );
  return draftValue[0];
};

/**
 * @returns {Object} Create Draft and save to vehicle service
 */
const createVehicleDraftToVehicleService = async (tagId, vehicle) => {
  // field and moving it to the destination collection
  const draftValue = await helper.ensureEntityExists(
    draftVehicleModel,
    { _id: tagId },
    'The Draft Vehicle does not exist.'
  );

  const value = _.omit(draftValue.toObject(), '_id');
  const draftValues = {
    value,
    ...vehicle
  };
  const result = await VehicleDetail.create(draftValues);

  draftValue.remove();

  if (result) {
    await helper.createLogManagement(
      vehicle.createdBy,
      'Add Data',
      'Draft Vehicle Details',
      'Draft vehicle to vehicle list'
    );
  }

  return { messaage: 'Draft value has been  submitted successfully.' };
};

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const getAllVehicleValueService = async () => {
  const collections = [
    SeatingCapacityModel, // 0
    FuelCapacityModel, // 1
    VehicleTypeValueModel, // 2
    TransmissionTypeValueModel, // 3
    FuelTypeValueModel, // 4
    BodyTypeModel, // 5
    MileageModel, // 6
    WheelBaseModel, // 7
    OverallWidthModel, // 8
    OverallLengthModel, // 9
    OverallHeightModel, // 10
    GroundClearanceModel, // 11
    BootSpaceModel, // 12
    KerbWeightModel, // 13
    DoddleHeightModel, // 14
    DisplacementModel, // 15
    EngineTypeModel, // 17
    NoOfCylinderModel, // 18
    ValvesPerCylinderModel, // 19
    ValveConfigurationModel, // 20
    FuelSupplySystemModel, // 21
    MaximumPowerModel, // 22
    MaximumTorqueModel, // 23
    LubricationModel, // 24
    EngineOilModel, // 25
    AirCleanerModel, // 26
    BoreXStrokeModel, // 27
    CompressionRatioModel, // 28
    FrontWheelTypeModel, // 29
    RearWheelTypeModel, // 30
    FrontWheelSizeModel, // 31
    RearWheelSizeModel, // 32
    RearTyreModel, // 33
    FrontTyreModel, // 34
    FrontWheelDriveModel, // 35
    SteelRimsModel, // 36
    InstrumentationModel, // 37
    SafetyModel, // 38
    FeatureModel, // 39
    IgnitionModel, // 40
    FrontBrakeSystemModel, // 41
    BackBrakeSystemModel, // 42
    FrontSuspensionModel, // 43
    RearSuspensionModel, // 44
    NoOfGearsModel, // 45
    DriveTypeModel, // 46
    ClutchTypeModel, // 47
    GearPatternModel, // 48
    HeadlightModel, // 49
    TaillightModel, // 50
    StarterModel, // 51
    BatteryModel, // 52
    MarketPriceModel, // 53
    OtherDetailModel, // 54
    MotorPowerModel // 55
  ];

  const result = [];

  const collectionData = await collections.map(async (res, index) => {
    const collection = await collections[index].find({}, (err, foundCollection) => {
      if (err) {
        return 'Error Occurred';
      }
      result[index] = foundCollection;

      return result[index];
    });

    return collection;
  });
  await Promise.all(collectionData);

  return {
    seatingCapacity: result[0],
    fuelCapacity: result[1],
    vehicleType: result[2],
    transmissionType: result[3],
    fuelType: result[4],
    bodyType: result[5],
    mileage: result[6],
    wheelBase: result[7],
    overallWidth: result[8],
    overallLength: result[9],
    overallHeight: result[10],
    groundClearance: result[11],
    bootSpace: result[12],
    kerbWeight: result[13],
    doddleHeight: result[14],
    displacement: result[15],
    engineType: result[16],
    noOfCylender: result[17],
    valvesPerCylinder: result[18],
    valvesConfiguration: result[19],
    fuelSupply: result[20],
    maximumPower: result[21],
    maximumTorque: result[22],
    lubrication: result[23],
    engineOil: result[24],
    airCleaner: result[25],
    boreXStoke: result[26],
    compressionRatio: result[27],
    frontWheelType: result[28],
    rearWheelType: result[29],
    frontWheelSize: result[30],
    rearWheelSize: result[31],
    rearTyre: result[32],
    frontTyre: result[33],
    frontWheelDrive: result[34],
    steelRims: result[35],
    instrumentation: result[36],
    safety: result[37],
    feature: result[38],
    ignition: result[39],
    frontBrake: result[40],
    backBrake: result[41],
    frontSuspension: result[42],
    rearSuspension: result[43],
    noOfGear: result[44],
    driveType: result[45],
    clutchType: result[46],
    gearPattern: result[47],
    headlight: result[48],
    taillight: result[49],
    starter: result[50],
    battery: result[51],
    market: result[52],
    otherDetail: result[53],
    motorPower: result[54]
  };
};

module.exports = {
  createVehicleDetail,
  getVehicleDetails,
  getVehicleDetail,
  updateVehicleDetail,
  deleteVehicleDetail,
  uploadVehicleImages,
  getVehicleDraftDetailsService,
  getVehicleDraftDetailsByIdService,
  createVehicleDraftToVehicleService,
  getAllVehicleValueService
};
