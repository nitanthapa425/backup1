/**
 * Copyright (C) Tuki Logic
 */

/**
 * the feature auto value service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

const { FeatureModel } = require('../../models');

/**
 * @param {Object} entity the new feature
 * @returns {Object} the entity
 */

async function createFeatureService(entity) {
  const value = await FeatureModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all instrumentation
 */

const getFeatureService = async () => {
  const results = await FeatureModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createFeatureService,
  getFeatureService
};
