/**
 * Copyright (C) Tuki Logic
 */

/**
 * the basic auto value service
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const {
  VehicleTypeValueModel, TransmissionTypeValueModel, FuelTypeValueModel,
  BodyTypeModel, SeatingCapacityModel, FuelCapacityModel, MileageModel, MarketPriceModel, OtherDetailModel, MotorPowerModel, SpeedoMeterModel
} = require('../../models');
// const { existValidation } = require('../../utils/helper');

/**
   * @param {Object} entity the new vehicle type
   * @returns {Object} the entity
   */

async function createVehicleTypeService(entity) {
  const value = await VehicleTypeValueModel.create(entity);
  return value;
}
/**
 * @returns {Object} get  all vehicle type
 */

const getVehicleTypeService = async () => {
  const results = await VehicleTypeValueModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new transmission typw
   * @returns {Object} the entity
   */

async function createTransmissionTypeService(entity) {
  const value = await TransmissionTypeValueModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all transmission type
 */

const getTransmissionTypeService = async () => {
  const results = await TransmissionTypeValueModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new fuel type
   * @returns {Object} the entity
   */

async function createFuelTypeService(entity) {
  const value = await FuelTypeValueModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all fuel type
 */

const getFuelTypeService = async () => {
  const results = await FuelTypeValueModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new body type
   * @returns {Object} the entity
   */

async function createBodyTypeService(entity) {
  const value = await BodyTypeModel.create(entity);
  return value;
  // let bodyType = await BodyTypeModel.findOne({ bodyType: entity.bodyType });
  // existValidation(bodyType, entity.bodyType);
  // bodyType = new BodyTypeModel(entity);
  // bodyType.save();

  // return {
  //   status: 201,
  //   message: `${entity.bodyType} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all body type
 */

const getBodyTypeService = async () => {
  const results = await BodyTypeModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new seating capacity
   * @returns {Object} the entity
   */

async function createSeatingCapacityService(entity) {
  const value = await SeatingCapacityModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all seating capacity
 */

const getSeatingCapacityService = async () => {
  const results = await SeatingCapacityModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new fuel capacity
   * @returns {Object} the entity
   */

async function createFuelCapacityService(entity) {
  // createIndex(FuelCapacityModel);
  const value = await FuelCapacityModel.create(entity);
  return value;
}
//   let fuelCapacity = await FuelCapacityModel.findOne({ fuelCapacity: entity.fuelCapacity });
//   existValidation(fuelCapacity, entity.fuelCapacity);
//   fuelCapacity = new FuelCapacityModel(entity);
//   fuelCapacity.save();

//   return {
//     status: 201,
//     message: `${entity.fuelCapacity} saved successfully!`
//   };
// }

/**
 * @returns {Object} get  all fuel capacity
 */

const getFuelCapacityService = async () => {
  const results = await FuelCapacityModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new mileage
   * @returns {Object} the entity
   */

async function createMileageService(entity) {
  const value = await MileageModel.create(entity);
  return value;
}

// let mileage = await MileageModel.findOne({ mileage: entity.mileage });
// existValidation(mileage, entity.mileage);
// mileage = new MileageModel(entity);
// mileage.save();

// return {
//   status: 201,
//   message: `${entity.mileage} saved successfully!`
// };

/**
 * @returns {Object} get  all mileage
 */

const getMileageService = async () => {
  const results = await MileageModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the market price
   * @returns {Object} the entity
   */

async function createMarketPriceService(entity) {
  const value = await MarketPriceModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  market price
 */

const getMarketPriceService = async () => {
  const results = await MarketPriceModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the other detail
   * @returns {Object} the entity
   */

async function createOtherDetailService(entity) {
  const value = await OtherDetailModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all other detail
 */

const getOtherDetailService = async () => {
  const results = await OtherDetailModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the additional feature
   * @returns {Object} the entity
   */

async function createMotorPowerService(entity) {
  const value = await MotorPowerModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all other detail
 */

const getMotorPowerService = async () => {
  const results = await MotorPowerModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the additional feature
   * @returns {Object} the entity
   */

async function createSpeedometerService(entity) {
  const value = await SpeedoMeterModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all other detail
 */

const getSpeedometerService = async () => {
  const results = await SpeedoMeterModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createVehicleTypeService,
  getVehicleTypeService,
  createTransmissionTypeService,
  getTransmissionTypeService,
  createFuelTypeService,
  getFuelTypeService,
  createBodyTypeService,
  getBodyTypeService,
  createSeatingCapacityService,
  getSeatingCapacityService,
  createFuelCapacityService,
  getFuelCapacityService,
  createMileageService,
  getMileageService,
  createMarketPriceService,
  getMarketPriceService,
  createOtherDetailService,
  getOtherDetailService,
  createMotorPowerService,
  getMotorPowerService,
  createSpeedometerService,
  getSpeedometerService
};
