/**
 * Copyright (C) Tuki Logic
 */

/**
 * the safety auto value service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

const { SafetyModel } = require('../../models');

/**
 * @param {Object} entity the new safety
 * @returns {Object} the entity
 */

async function createSafetyService(entity) {
  const value = await SafetyModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all safety
 */

const getSafetyService = async () => {
  const results = await SafetyModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createSafetyService,
  getSafetyService
};
