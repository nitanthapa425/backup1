/**
 * Copyright (C) Two Logic
 */

/**
 * the vehicle auto value service
 *
 * @author      Two Logic
 * @version     1.0
 */

const { InstrumentationModel } = require('../../models');

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createInstrumentationService(entity) {
  const value = await InstrumentationModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all instrumentation
 */

const getInstrumentationService = async () => {
  const results = await InstrumentationModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createInstrumentationService,
  getInstrumentationService
};
