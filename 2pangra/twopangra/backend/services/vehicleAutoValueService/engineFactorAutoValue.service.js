/**
 * Copyright (C) Two Logic
 */

/**
 * the vehicle auto value service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const { existValidation } = require('../../utils/helper');
const {
  DisplacementModel,
  FuelEfficiencyModel,
  EngineTypeModel,
  NoOfCylinderModel,
  ValvesPerCylinderModel,
  ValveConfigurationModel,
  FuelSupplySystemModel,
  MaximumPowerModel,
  MaximumTorqueModel,
  LubricationModel,
  EngineOilModel,
  AirCleanerModel,
  BoreXStrokeModel,
  CompressionRatioModel
} = require('../../models');

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createDisplacementService(entity) {
  const value = await DisplacementModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getDisplacementService = async () => {
  const results = await DisplacementModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFuelEfficiencyService(entity) {
  const value = await FuelEfficiencyModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFuelEfficiencyService = async () => {
  const results = await FuelEfficiencyModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createEngineTypeService(entity) {
  const value = await EngineTypeModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getEngineTypeService = async () => {
  const results = await EngineTypeModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createNoOfCylinderService(entity) {
  const value = await NoOfCylinderModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getNoOfCylinderService = async () => {
  const results = await NoOfCylinderModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createValvesPerCylinderService(entity) {
  const value = await ValvesPerCylinderModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getValvesPerCylinderService = async () => {
  const results = await ValvesPerCylinderModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createValveConfigurationService(entity) {
  const value = await ValveConfigurationModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getValveConfigurationService = async () => {
  const results = await ValveConfigurationModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFuelSupplySystemService(entity) {
  const value = await FuelSupplySystemModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFuelSupplySystemService = async () => {
  const results = await FuelSupplySystemModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createMaximumPowerDoddleHeightService(entity) {
  const value = await MaximumPowerModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getMaximumPowerDoddleHeightService = async () => {
  const results = await MaximumPowerModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createMaximumTorqueDoddleHeightService(entity) {
  const value = await MaximumTorqueModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getMaximumTorqueDoddleHeightService = async () => {
  const results = await MaximumTorqueModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createLubricationDoddleHeightService(entity) {
  const value = await LubricationModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getLubricationDoddleHeightService = async () => {
  const results = await LubricationModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createEngineOilService(entity) {
  const value = await EngineOilModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getEngineOilService = async () => {
  const results = await EngineOilModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createAirCleanerDoddleHeightService(entity) {
  const value = await AirCleanerModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getAirCleanerDoddleHeightService = async () => {
  const results = await AirCleanerModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createBoreXStrokeService(entity) {
  const value = await BoreXStrokeModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getBoreXStrokeService = async () => {
  const results = await BoreXStrokeModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createCompressionRatioService(entity) {
  const value = await CompressionRatioModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getCompressionRatioService = async () => {
  const results = await CompressionRatioModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createDisplacementService,
  getDisplacementService,
  createFuelEfficiencyService,
  getFuelEfficiencyService,
  createEngineTypeService,
  getEngineTypeService,
  createNoOfCylinderService,
  getNoOfCylinderService,
  createValvesPerCylinderService,
  getValvesPerCylinderService,
  createValveConfigurationService,
  getValveConfigurationService,
  createFuelSupplySystemService,
  getFuelSupplySystemService,
  createMaximumPowerDoddleHeightService,
  getMaximumPowerDoddleHeightService,
  createMaximumTorqueDoddleHeightService,
  getMaximumTorqueDoddleHeightService,
  createLubricationDoddleHeightService,
  getLubricationDoddleHeightService,
  createEngineOilService,
  getEngineOilService,
  createAirCleanerDoddleHeightService,
  getAirCleanerDoddleHeightService,
  createBoreXStrokeService,
  getBoreXStrokeService,
  createCompressionRatioService,
  getCompressionRatioService
};
