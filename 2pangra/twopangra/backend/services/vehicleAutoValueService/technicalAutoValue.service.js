/**
 * Copyright (C) Tuki Logic
 */

/**
 * the technical auto value service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

// const { existValidation } = require('../../utils/helper');
const {
  FrontBrakeSystemModel,
  BackBrakeSystemModel,
  FrontSuspensionModel,
  RearSuspensionModel,
  NoOfGearsModel,
  DriveTypeModel,
  ClutchTypeModel,
  GearPatternModel,
  HeadlightModel,
  TaillightModel,
  StarterModel,
  BatteryModel
} = require('../../models');

/**
 * @param {Object} entity the new front brake
 * @returns {Object} the entity
 */

async function createFrontBrakeService(entity) {
  const value = await FrontBrakeSystemModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all front brake
 */

const getFrontBrakeService = async () => {
  const results = await FrontBrakeSystemModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new back brake
 * @returns {Object} the entity
 */

async function createBackBrakeService(entity) {
  const value = await BackBrakeSystemModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all back brake
 */

const getBackBrakeService = async () => {
  const results = await BackBrakeSystemModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new front suspension
 * @returns {Object} the entity
 */

async function createFrontSuspensionService(entity) {
  const value = await FrontSuspensionModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all front suspension
 */

const getFrontSuspensionService = async () => {
  const results = await FrontSuspensionModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new rear suspension
 * @returns {Object} the entity
 */

async function createRearSuspensionService(entity) {
  const value = await RearSuspensionModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all rear suspension
 */

const getRearSuspensionService = async () => {
  const results = await RearSuspensionModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new no. of gears
 * @returns {Object} the entity
 */

async function createNoOfGearsService(entity) {
  const value = await NoOfGearsModel.create(entity);
  return value;
  // let noOfGears = await NoOfGearsModel.findOne({ noOfGears: entity.noOfGears });
  // existValidation(noOfGears, entity.noOfGears);
  // noOfGears = new NoOfGearsModel(entity);
  // noOfGears.save();

  // return {
  //   status: 201,
  //   messsage: `${entity.noOfGear} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all no. of gears
 */

const getNoOfGearsService = async () => {
  const results = await NoOfGearsModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new drive type
 * @returns {Object} the entity
 */

async function createDriveTypeService(entity) {
  const value = await DriveTypeModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all drive type
 */

const getDriveTypeService = async () => {
  const results = await DriveTypeModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new clutch type
 * @returns {Object} the entity
 */

async function createClutchTypeService(entity) {
  const value = await ClutchTypeModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all clutch type
 */

const getClutchTypeService = async () => {
  const results = await ClutchTypeModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new gear pattern
 * @returns {Object} the entity
 */

async function createGearPatternService(entity) {
  const value = await GearPatternModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all gear pattern
 */

const getGearPatternService = async () => {
  const results = await GearPatternModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new headlight
 * @returns {Object} the entity
 */

async function createHeadlightService(entity) {
  const value = await HeadlightModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all headlight
 */

const getHeadlightService = async () => {
  const results = await HeadlightModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new taillight
 * @returns {Object} the entity
 */

async function createTaillightService(entity) {
  const value = await TaillightModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all taillight
 */

const getTaillightService = async () => {
  const results = await TaillightModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new starter
 * @returns {Object} the entity
 */

async function createStarterService(entity) {
  const value = await StarterModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all starter
 */

const getStarterService = async () => {
  const results = await StarterModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new battery
 * @returns {Object} the entity
 */

async function createBatteryService(entity) {
  const value = await BatteryModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all battery
 */

const getBatteryService = async () => {
  const results = await BatteryModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createFrontBrakeService,
  getFrontBrakeService,
  createBackBrakeService,
  getBackBrakeService,
  createFrontSuspensionService,
  getFrontSuspensionService,
  createRearSuspensionService,
  getRearSuspensionService,
  createNoOfGearsService,
  getNoOfGearsService,
  createDriveTypeService,
  getDriveTypeService,
  createClutchTypeService,
  getClutchTypeService,
  createGearPatternService,
  getGearPatternService,
  createHeadlightService,
  getHeadlightService,
  createTaillightService,
  getTaillightService,
  createStarterService,
  getStarterService,
  createBatteryService,
  getBatteryService
};
