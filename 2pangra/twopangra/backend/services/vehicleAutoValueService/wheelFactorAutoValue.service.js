/**
 * Copyright (C) Two Logic
 */

/**
 * the vehicle auto value service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const { existValidation } = require('../../utils/helper');
const {
  FrontWheelTypeModel,
  RearWheelTypeModel,
  FrontWheelSizeModel,
  RearWheelSizeModel,
  RearTyreModel,
  FrontTyreModel,
  FrontWheelDriveModel,
  AlloyWheelModel,
  SteelRimsModel
} = require('../../models');

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFrontWheelTypeService(entity) {
  const frontWheelType = await FrontWheelTypeModel.create(entity);
  // let wheelType = await WheelTypeModel.findOne({ wheelType: entity.wheelType });
  // existValidation(wheelType, entity.wheelType);
  // wheelType = await new WheelTypeModel(entity);
  // wheelType.save();

  return frontWheelType;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFrontWheelTypeService = async () => {
  const results = await FrontWheelTypeModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createRearWheelTypeService(entity) {
  const rearWheelType = await RearWheelTypeModel.create(entity);
  // let wheelType = await WheelTypeModel.findOne({ wheelType: entity.wheelType });
  // existValidation(wheelType, entity.wheelType);
  // wheelType = await new WheelTypeModel(entity);
  // wheelType.save();

  return rearWheelType;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getRearWheelTypeService = async () => {
  const results = await RearWheelTypeModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFrontWheelSizeService(entity) {
  const value = FrontWheelSizeModel.create(entity);
  // let wheelSize = await WheelSizeModel.findOne({ wheelSize: entity.wheelSize });
  // existValidation(wheelSize, entity.wheelSize);
  // wheelSize = new WheelSizeModel(entity);
  // wheelSize.save();

  return value;
  // return {
  //   status: 201,
  //   message: `${entity.wheelSize} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFrontWheelSizeService = async () => {
  const results = await FrontWheelSizeModel.find().collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createRearWheelSizeService(entity) {
  const value = await RearWheelSizeModel.create(entity);
  // let wheelSize = await WheelSizeModel.findOne({ wheelSize: entity.wheelSize });
  // existValidation(wheelSize, entity.wheelSize);
  // wheelSize = new WheelSizeModel(entity);
  // wheelSize.save();

  return value;
  // return {
  //   status: 201,
  //   message: `${entity.wheelSize} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getRearWheelSizeService = async () => {
  const results = await RearWheelSizeModel.find().collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createRearTyreService(entity) {
  const value = RearTyreModel.create(entity);
  // let rearTyre = await RearTyreModel.findOne({ rearTyre: entity.rearTyre });
  // existValidation(rearTyre, entity.rearTyre);
  // rearTyre = new RearTyreModel(entity);
  // rearTyre.save();
  return value;
  // return {
  //   status: 201,
  //   message: `${entity.rearTyre} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getRearTyreService = async () => {
  const results = await RearTyreModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFrontTyreService(entity) {
  const value = FrontTyreModel.create(entity);
  // let frontTyre = await FrontTyreModel.findOne({ frontTyre: entity.frontTyre });
  // existValidation(frontTyre, entity.frontTyre);
  // frontTyre = new FrontTyreModel(entity);
  // frontTyre.save();
  return value;
  // return {
  //   status: 201,
  //   message: `${entity.frontTyre} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFrontTyreService = async () => {
  const results = await FrontTyreModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createFrontWheelDriveService(entity) {
  const value = FrontWheelDriveModel.create(entity);
  // let frontWheelDrive = await FrontWheelDriveModel.findOne({ frontWheelDrive: entity.frontWheelDrive });
  // existValidation(frontWheelDrive, entity.frontWheelDrive);
  // frontWheelDrive = new FrontWheelDriveModel(entity);
  // frontWheelDrive.save();
  return value;
  // return {
  //   status: 201,
  //   message: `${entity.frontWheelDrive} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getFrontWheelDriveService = async () => {
  const results = await FrontWheelDriveModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createAlloyWheelService(entity) {
  const value = AlloyWheelModel.create(entity);
  // let alloyWheel = await AlloyWheelModel.findOne({ alloyWheel: entity.alloyWheel });
  // existValidation(alloyWheel, entity.alloyWheel);
  // alloyWheel = new AlloyWheelModel(entity);
  // alloyWheel.save();
  return value;
  // return {
  //   status: 201,
  //   message: `${entity.alloyWheel} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getAlloyWheelService = async () => {
  const results = await AlloyWheelModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createSteelRimsService(entity) {
  const value = SteelRimsModel.create(entity);
  // let steelRims = await SteelRimsModel.findOne({ steelRims: entity.steelRims });
  // existValidation(steelRims, entity.steelRims);
  // steelRims = new SteelRimsModel(entity);
  // steelRims.save();
  return value;
  // return {
  //   status: 201,
  //   message: `${entity.steelRims} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getSteelRimsService = async () => {
  const results = await SteelRimsModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createFrontWheelTypeService,
  getFrontWheelTypeService,
  createRearWheelTypeService,
  getRearWheelTypeService,
  createFrontWheelSizeService,
  getFrontWheelSizeService,
  createRearWheelSizeService,
  getRearWheelSizeService,
  createRearTyreService,
  getRearTyreService,
  createFrontTyreService,
  getFrontTyreService,
  createFrontWheelDriveService,
  getFrontWheelDriveService,
  createAlloyWheelService,
  getAlloyWheelService,
  createSteelRimsService,
  getSteelRimsService
};
