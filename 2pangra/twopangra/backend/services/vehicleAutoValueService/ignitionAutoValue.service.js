/**
 * Copyright (C) Tuki Logic
 */

/**
 * the ignition auto value service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

const { IgnitionModel } = require('../../models');

/**
 * @param {Object} entity the new ignition
 * @returns {Object} the entity
 */

async function createIgnitionService(entity) {
  const value = await IgnitionModel.create(entity);
  return value;
}

/**
 * @returns {Object} get  all ignition
 */

const getIgnitionService = async () => {
  const results = await IgnitionModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createIgnitionService,
  getIgnitionService
};
