/**
 * Copyright (C) Two Logic
 */

/**
 * the vehicle auto value service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const { existValidation } = require('../../utils/helper');
const {
  WheelBaseModel, OverallWidthModel, OverallLengthModel,
  OverallHeightModel, GroundClearanceModel, BootSpaceModel, KerbWeightModel, DoddleHeightModel

} = require('../../models');

/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createWheelBaseService(entity) {
  const value = await WheelBaseModel.create(entity);
  // let wheelBase = await WheelBaseModel.findOne({ wheelBase: entity.wheelBase });
  // existValidation(wheelBase, entity.wheelBase);
  // wheelBase = new WheelBaseModel(entity);
  // wheelBase.save();

  // return {
  //   status: 201,
  //   message: `${entity.wheelBase} saved successfully!`
  // };
  return value;
}

/**
 * @returns {Object} get  all wheelBase
 */

const getWheelBaseService = async () => {
  const results = await WheelBaseModel.find({}).collation({ locale: 'en' });

  return results;
};

/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createOverallWidthService(entity) {
// const validate = { runValidators: true };
  const value = await OverallWidthModel.create(entity);
  return value;
  // const id = req.params.id;
  // const client = req.body;
  // Client.update({_id: id}, client, { runValidators: true }, function(err) {
  //   ....
  // });
  // let overallWidth = await OverallWidthModel.findOne({ overallWidth: entity.overallWidth });
  // existValidation(overallWidth, entity.overallWidth);
  // overallWidth = new OverallWidthModel(entity);
  // overallWidth.save();

  // return {
  //   status: 201,
  //   message: `${entity.overallWidth} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getOverallWidthService = async () => {
  const results = await OverallWidthModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createOverallLengthService(entity) {
  const value = await OverallLengthModel.create(entity);
  return value;

  // let overallLength = await OverallLengthModel.findOne({ overallLength: entity.overallLength });
  // existValidation(overallLength, entity.overallLength);
  // overallLength = new OverallLengthModel(entity);
  // overallLength.save();

  // return {
  //   status: 201,
  // message: `${entity.overallLength} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getOverallLengthService = async () => {
  const results = await OverallLengthModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createOverallHeightService(entity) {
  const value = await OverallHeightModel.create(entity);
  return value;
  // let overallHeight = await OverallHeightModel.findOne({ overallHeight: entity.overallHeight });
  // existValidation(overallHeight, entity.overallHeight);
  // overallHeight = new OverallHeightModel(entity);
  // overallHeight.save();

  // return {
  //   status: 201,
  //   message: `${entity.overallHeight} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getOverallHeightService = async () => {
  const results = await OverallHeightModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createGroundClearanceService(entity) {
  const value = await GroundClearanceModel.create(entity);
  return value;
  // let groundClearance = await GroundClearanceModel.findOne({ groundClearance: entity.groundClearance });
  // existValidation(groundClearance, entity.groundClearance);
  // groundClearance = new GroundClearanceModel(entity);
  // groundClearance.save();

  // return {
  //   status: 201,
  //   message: `${entity.groundClearance} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getGroundClearanceService = async () => {
  const results = await GroundClearanceModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createBootSpaceService(entity) {
  const value = await BootSpaceModel.create(entity);
  return value;
  // let bootSpace = await BootSpaceModel.findOne({ bootSpace: entity.bootSpace });
  // existValidation(bootSpace, entity.bootSpace);
  // bootSpace = new BootSpaceModel(entity);
  // bootSpace.save();

  // return {
  //   status: 201,
  //   message: `${entity.bootSpace} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getBootSpaceService = async () => {
  const results = await BootSpaceModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createKerbWeightService(entity) {
  const value = await KerbWeightModel.create(entity);
  return value;
  // let kerbWeight = await KerbWeightModel.findOne({ kerbWeight: entity.kerbWeight });
  // existValidation(kerbWeight, entity.kerbWeight);
  // kerbWeight = new KerbWeightModel(entity);
  // kerbWeight.save();

  // return {
  //   status: 201,
  //   message: `${entity.kerbWeight} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getKerbWeightService = async () => {
  const results = await KerbWeightModel.find({}).collation({ locale: 'en' });

  return results;
};
/**
   * @param {Object} entity the new brandVehicle
   * @returns {Object} the entity
   */

async function createDoddleHeightService(entity) {
  const value = await DoddleHeightModel.create(entity);
  return value;
  // let doddleHeight = await DoddleHeightModel.findOne({ doddleHeight: entity.doddleHeight });
  // existValidation(doddleHeight, entity.doddleHeight);
  // doddleHeight = new DoddleHeightModel(entity);
  // doddleHeight.save();

  // return {
  //   status: 201,
  //   message: `${entity.doddleHeight} saved successfully!`
  // };
}

/**
 * @returns {Object} get  all wheelBase
 */

const getDoddleHeightService = async () => {
  const results = await DoddleHeightModel.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createWheelBaseService,
  getWheelBaseService,
  createOverallWidthService,
  getOverallWidthService,
  createOverallLengthService,
  getOverallLengthService,
  createOverallHeightService,
  getOverallHeightService,
  createGroundClearanceService,
  getGroundClearanceService,
  createBootSpaceService,
  getBootSpaceService,
  createKerbWeightService,
  getKerbWeightService,
  createDoddleHeightService,
  getDoddleHeightService
};
