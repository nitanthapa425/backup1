// const httpStatus = require('http-status');
// const errors = require('common-errors');
const { EmailSubscribe } = require('../models');

const collectAllEmail = async (emailBody) => {
  const value = await EmailSubscribe.create(emailBody);
  return value;
};
module.exports = {
  collectAllEmail,
};
