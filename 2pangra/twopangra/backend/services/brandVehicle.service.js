/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const {
  BrandVehicle, BrandModel, VarientModel, VaraiantNameValueModel
} = require('../models');

/**
 * @returns {Object} get  Specific Sub Model By Id
 */
const getSubBrandModelByIdService = async (tagId) => {
  const brandVehicle = await VarientModel.findOne({ _id: tagId }).populate('modelId');
  if (!brandVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Variant is not found. Try Refreshing the page');
  }

  return brandVehicle;
};

/**
 * @returns {Object} get  Specific Brand Detail
 */
const getBrandModelByIdService = async (tagId) => {
  const brandVehicle = await BrandModel.findOne({ _id: tagId }).populate('brandId');
  if (!brandVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Brand Model with id is not found. Try Refreshing the page');
  }

  return brandVehicle;
};

/**
 * @param {Object} entity the new brandVehicle
 * @returns {Object} the entity
 */

async function createBrandVehicle(entity) {
  const value = await BrandVehicle.create(entity);

  if (value) {
    await helper.createLogManagement(entity.createdBy, 'Add Data', 'Brands', entity.brandName);
  }

  return value;
}

async function createBrandModelService(entity) {
  const modelVehicle = await BrandModel.findOne({ modelName: entity.modelName, brandId: entity.brandId });

  if (modelVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Same brand cannot have multiple model');
  }

  const value = await BrandModel.create(entity);

  if (value) {
    await helper.createLogManagement(entity.createdBy, 'Add Data', 'Models', 'Model');
  }

  return value;
}

async function createBrandSubModelService(entity) {
  /* Check whether variant name exist or not */
  const verientVehicle = await VarientModel.findOne({ varientName: entity.varientName, modelId: entity.modelId });

  if (verientVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Same model cannot have multiple variant');
  }
  const value = VarientModel.create(entity);

  if (value) {
    await helper.createLogManagement(entity.createdBy, 'Add Data', 'Variants', 'Variant');
  }

  return value;
}

/**
 * @returns {Object} get  Specific Brand Model
 */
const getBrandModelByBrandIdService = async (brandId) => {
  const brandModel = await BrandModel.findOne({ brandId });

  if (!brandModel) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Brand Model is not found. Try Refreshing the page');
  }

  const value = await BrandModel.find({ brandId });

  return value;
};

/**
 * @returns {Object} get  Specific Brand Sub Model
 */
const getBrandSubModelByModelIdService = async (modelId) => {
  const varient = await VarientModel.findOne({ modelId });

  if (!varient) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Varient is not found. Try Refreshing the page');
  }

  const value = await VarientModel.find({ modelId });

  return value;
};

/**
 * @returns {Object} get  all brandVehicle
 */

const getBrandVehicleDetails = async (options, search) => {
  const results = await BrandVehicle.paginate(search, options);
  return results;
};

const getBrandWithoutAuth = async () => {
  const results = BrandVehicle.find({});

  const reverseResult = (await results).map((res) => ({
    brandId: res.id,
    brandName: res.brandVehicleName
  }));
  return reverseResult.reverse();
};

const getBrandWithoutAuthLimit = async (options, search) => {
  const results = await BrandVehicle.paginate(search, options);
  return results;
};

const getHomePageBrandWithoutAuth = async () => {
  const results = BrandVehicle.find({
    hasInHomePage: true
  });

  const reverseResult = (await results).map((res) => ({
    brandId: res.id,
    brandName: res.brandVehicleName,
    brandImage: res.uploadBrandImage
  }));
  return reverseResult;
};

/**
 * @returns {Object} get  Specific Brand Detail
 */
const getBrandVehicleDetail = async (tagId) => {
  const brandVehicle = await BrandVehicle.findOne({ _id: tagId });
  if (!brandVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Brand Detail is not found. Try Refreshing the page');
  }

  return brandVehicle;
};

/**
 * handles the update  brandVehicle
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateBrandVehicle(brandVehicleId, updateBody) {
  const value = await BrandVehicle.updateOne({ _id: brandVehicleId }, updateBody);

  if (value) {
    await helper.createLogManagement(updateBody.updatedBy, 'Update Data', 'Brands', 'Brand');
  }

  return value;
}

/**
 * Delete brandVehicle
 * @returns {Object} Search result
 */
async function deleteBrandVehicle(deleteId, userId) {
  const brandVehicle = await helper.ensureEntityExists(BrandVehicle, { _id: deleteId }, 'The Brand does not exist.');
  const brandInModel = await BrandModel.findOne({ brandId: deleteId });
  // eslint-disable-next-line eqeqeq
  if (brandInModel !== null && typeof brandInModel.brandId !== 'undefined' && brandInModel.brandId == deleteId) {
    throw new errors.HttpStatusError(401, "You cannot delete this brand before deleting it's model");
  }
  brandVehicle.remove();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Brands', 'Brand');
  }

  return {
    status: 201,
    message: 'Brand has been deleted successfully'
  };
}

deleteBrandVehicle.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/* Add brand in Home Page */
const addBrandInHomePage = async (brandVehicleId, userId) => {
  const brandVehicle = await helper.ensureEntityExists(
    BrandVehicle,
    { _id: brandVehicleId },
    'Brand Vehicle does not exist'
  );
  if (brandVehicle.hasInHomePage === true) {
    throw new errors.HttpStatusError(401, 'Brand is already added to Home Page');
  }
  brandVehicle.hasInHomePage = true;
  await brandVehicle.save();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Add in Landing Page', 'Brands', 'Brand');
  }

  return {
    code: 201,
    message: 'Brand has been added to Home page'
  };
};

/* Remove brand from Home Page */
const removeBrandInHomePage = async (brandVehicleId, userId) => {
  const brandVehicle = await helper.ensureEntityExists(
    BrandVehicle,
    { _id: brandVehicleId },
    'Brand Vehicle does not exist'
  );
  if (brandVehicle.hasInHomePage === false) {
    throw new errors.HttpStatusError(401, 'Brand is already removed from Home Page');
  }
  brandVehicle.hasInHomePage = false;
  await brandVehicle.save();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Remove from Landing Page', 'Brands', 'Brand');
  }

  return {
    code: 201,
    message: 'Brand has been removed from Home page'
  };
};

/* Upload Brand Image In brandVehicle  */
const uploadBrandImages = async (brandVehicleId, entity) => {
  const brandVehicle1 = await helper.ensureEntityExists(
    BrandVehicle,
    { _id: brandVehicleId },
    `The Brand Vehicle ${brandVehicleId} does not exist.`
  );
  brandVehicle1.uploadBrandImage = entity.uploadBrandImage;
  await brandVehicle1.save();

  return {
    status: 200,
    message: 'Brand image has been added successfully'
  };
};

/**
 * @returns {Object} get  all Brand Models
 */

const getAllBrandModelService = async (search, options) => {
  const paginateResult = await BrandModel.paginate(search, options);

  return paginateResult;
};
/**
 * @returns {Object} get  all SUb Brand Models
 */

const getAllSubBrandModelService = async (search, options) => {
  const paginateResult = await VarientModel.paginate(search, options);
  return paginateResult;
};

/**
 * Delete Sub Model By Id
 * @returns {Object} Search result
 */
async function deleteSubBrandModelByIdService(deleteId, userId) {
  const brandVehicle = await helper.ensureEntityExists(
    VarientModel,
    { _id: deleteId },
    'The Sub Brand  does not exist.'
  );
  brandVehicle.remove();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Variants', 'Variant');
  }

  return {
    status: 201,
    message: 'Varient been deleted successfully'
  };
}

deleteSubBrandModelByIdService.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/**
 * Delete Brand Model By id
 * @returns {Object} Search result
 */
async function deleteBrandModelByIdService(deleteId, userId) {
  const brandVehicle = await helper.ensureEntityExists(
    BrandModel,
    { _id: deleteId },
    'The Brand Model does not exist.'
  );
  const modelInVariant = await VarientModel.findOne({ modelId: deleteId });
  // eslint-disable-next-line eqeqeq
  if (modelInVariant !== null && typeof modelInVariant.modelId !== 'undefined' && modelInVariant.modelId == deleteId) {
    throw new errors.HttpStatusError(401, "You cannot delete this model before deleting it's variant");
  }
  brandVehicle.remove();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Models', 'Model');
  }

  return {
    status: 201,
    message: 'Brand Model has been deleted successfully'
  };
}

deleteBrandModelByIdService.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/**
 * handles the update  brandVehicle
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateSubBrandModelService(varientId, entity) {
  /* Check whether variant name exist or not */
  const verientVehicle = await VarientModel.findOne({ varientName: entity.varientName, modelId: entity.modelId });

  if (verientVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Same model cannot have multiple variant');
  }
  const value = await VarientModel.updateMany({ _id: varientId }, entity);

  if (value) {
    await helper.createLogManagement(entity.updatedBy, 'Update Data', 'Variants', 'Variant');
  }

  return value;
}

/**
 * handles the update  brandVehicle
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateBrandModelService(brandModelVehicleId, entity) {
  const opts = { runValidators: true };
  const value = await BrandModel.updateOne({ _id: brandModelVehicleId }, entity, opts);

  if (value) {
    await helper.createLogManagement(entity.updatedBy, 'Update Data', 'Models', 'Model');
  }

  return value;
}

/**
 * @param {Object} entity the additional feature
 * @returns {Object} the entity
 */

async function creatVehicleNameValueService(entity) {
  const value = await VaraiantNameValueModel.create(entity);

  return value;
}

/**
 * @returns {Object} get  all other detail
 */

const getVehicleNameValueService = async () => {
  const results = await VaraiantNameValueModel.find({}).collation({ locale: 'en' });

  return results;
};
module.exports = {
  createBrandVehicle,
  getBrandVehicleDetails,
  getBrandVehicleDetail,
  updateBrandVehicle,
  deleteBrandVehicle,
  addBrandInHomePage,
  removeBrandInHomePage,
  uploadBrandImages,
  createBrandModelService,
  createBrandSubModelService,
  getBrandModelByBrandIdService,
  getBrandSubModelByModelIdService,
  getAllBrandModelService,
  getBrandModelByIdService,
  getSubBrandModelByIdService,
  getAllSubBrandModelService,
  deleteSubBrandModelByIdService,
  deleteBrandModelByIdService,
  updateSubBrandModelService,
  updateBrandModelService,
  creatVehicleNameValueService,
  getVehicleNameValueService,
  getBrandWithoutAuth,
  getHomePageBrandWithoutAuth,
  getBrandWithoutAuthLimit
};
