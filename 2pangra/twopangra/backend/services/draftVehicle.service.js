/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

const _ = require('lodash');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const { draftVehicleModel } = require('../models');
/**
 * @param {Object} entity the new vehicle Draft Detail
 * @returns {Object} the entity
 */
const createDraftVehicleDetailService = async (tag) => {
  const value = draftVehicleModel.create(tag);

  if (value) {
    await helper.createLogManagement(tag.createdBy, 'Add Data', 'Draft Vehicle Details', 'Draft Vehicle Detail');
  }

  return value;
};

/**
 * Search Vehicle Detail
 * @param {String} name the any in Vehicle
 */
async function getDraftVehicleDetailService(search, options) {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const value = draftVehicleModel.paginate(search, optionValue);
  return value;
}

/**
 * Get User Vehicle Draft Detail By UserId
 * @param {Id} userId the any in Vehicle Draft of User
 */

const getDraftByUserIdService = async (userId) => {
  const userDraft = await draftVehicleModel.find({ createdBy: userId }).select('updatedAt vehicleName');
  return userDraft;
};

/**
 * Get Vehicle Draft Detail By Id
 * @param {Id} draftId the any in Vehicle Draft
 */

const getVehicleDraftDetailByIdService = async (draftId) => {
  const userDraft = await draftVehicleModel.findOne({ _id: draftId });
  return userDraft;
};

/**
 * Delete Vehicle Draft Detail By Id
 * @param {Id} draftId the any in Vehicle Draft
 */

const deleteVehicleDraftDetailByIdService = async (deleteId, userId) => {
  const draftVehicle = await helper.ensureEntityExists(draftVehicleModel, { _id: deleteId }, 'Draft does not exist.');
  draftVehicle.remove();

  if (draftVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Draft Vehicle Details', 'Draft Vehicle Detail');
  }

  return {
    status: 200,
    message: 'Draft is deleted Successfully'
  };
};

/**
 * Update Vehicle Draft Detail By Id
 * @param {Id} draftId the any in Vehicle Draft
 */

const updateVehicleDraftDetailByIdService = async (vehicleId, vehicleDetailBody) => {
  const vehicleDetail = await helper.ensureEntityExists(
    draftVehicleModel,
    { _id: vehicleId },
    'Vehicle Draft does not exist.'
  );

  if (!vehicleDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle Draft is not found. Try Refreshing the page');
  }
  _.assignIn(vehicleDetail, { ...vehicleDetailBody });

  await vehicleDetail.save();

  if (vehicleDetail) {
    await helper.createLogManagement(
      vehicleDetailBody.updatedBy,
      'Update Data',
      'Draft Vehicle Details',
      'Draft Vehicle Detail'
    );
  }

  return {
    status: 200,
    message: 'Vehicle Draft Details updated successfully.'
  };
};

module.exports = {
  createDraftVehicleDetailService,
  getDraftVehicleDetailService,
  getDraftByUserIdService,
  getVehicleDraftDetailByIdService,
  updateVehicleDraftDetailByIdService,
  deleteVehicleDraftDetailByIdService
};
