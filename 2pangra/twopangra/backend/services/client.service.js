/* eslint-disable indent */
/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Two Pangra
 */

/**
 * the Client Service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const _ = require('lodash');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const config = require('config');
const messagebird = require('messagebird')('7M4tFn9ghJmJ3S35UVMqqIp49');
const errors = require('common-errors');
const joi = require('joi');

// const fetch = require('node-fetch');
const helper = require('../common/helper');
const { Client, VehicleSellModel } = require('../models');
const { capitalize } = require('../common/helper');

/* Get Client data for authentication */
const getClientById = async (clientId) => Client.findOne({ _id: clientId });

/* Get Client data for authentication */
// const getClientByEmail = async (email) => Client.findOne({ email });
/* Get Client data for authentication */
const getClientByMobile = async (mobile) => Client.findOne({ mobile });
/**
 * does sign up process
 * @param {Object} entity the request body entity
 * @returns {Object} the sign client information
 */
async function registerClient(entity) {
  let client = await getClientByMobile(entity.mobile);
  const password = await helper.hashString(entity.password);

  if (client) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Mobile ${entity.mobile} already exists`);
  }
  const { mobile } = entity;

  const dateOfBirth = new Date(entity.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');

  const sendOtp = await helper.sendOTP(entity.mobile);
  const verificationToken = helper.getRandomString(25);
  client = _.extend(entity, {
    email: entity.email,
    userName: entity.userName,
    password,
    otpId: sendOtp.id,
    verificationToken,
    firstName: capitalize(entity.firstName),
    middleName: capitalize(entity.middleName),
    lastName: capitalize(entity.lastName),
    fullName:
      entity.middleName !== ''
        ? capitalize(entity.firstName).concat(' ') +
          capitalize(entity.middleName).concat(' ') +
          capitalize(entity.lastName)
        : capitalize(entity.firstName).concat(' ') + capitalize(entity.lastName),
    role: entity.role,
    dob,
    mobile,
    verified: false
  });
  client = new Client(client);
  await client.save();
  return {
    code: 201,
    mobile,
    id: sendOtp.id
  };
}

/**
 * Get  Client Data
 * @param clientId
 */
const getClientProfileList = async (search, options) => {
  search.$or = [{ isActive: true }, { isActive: { $exists: false } }];
  return Client.paginate(search, options);
};

/**
 * Get Deleted Client
 * @param clientId
 */

const getDeletedClientProfileList = async (search, options) => {
  search.$or = [{ isActive: false }];

  return Client.paginate(search, options);
};

/**
 * Get Client By id
 * @param clientId
 */

const getClientProfileByIdService = async (clientId) => {
  let client = await helper.ensureEntityExists(Client, { _id: clientId }, 'The Client with Id does not exist.');
  client = _.omit(
    client.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  return client;
};

/**
 * Get Deleted Client By id
 * @param clientId
 */

const getDeletedClientProfileById = async (clientId) => {
  const client = await Client.findOne({
    _id: clientId
  });
  return client;
};
/* Update Client Profile by Id */
async function updateClientProfileByIdService(clientId, entity) {
  const client = await helper.ensureEntityExists(Client, { _id: clientId }, 'The Client with Id does not exist.');

  if (!client) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Client is not found. Try Refreshing the page');
  }

  _.assignIn(client, { ...entity });

  await client.save();

  return {
    isActive: 200,
    message: 'Client has been updated successfully'
  };
}

/**
 * Get Client By id
 * @param clientId
 */

const getClientProfile = async (clientId) => {
  let client = await helper.ensureEntityExists(Client, { _id: clientId }, 'The Client with Id does not exist.');
  client = _.omit(
    client.toObject(),
    'password',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  return client;
};

/* Update Client Profile by Id */
async function updateClientProfile(clientId, entity) {
  const client = await helper.ensureEntityExists(Client, { _id: clientId }, 'The Client with Id does not exist.');

  if (!client) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Client is not found. Try Refreshing the page');
  }
  _.assignIn(client, { ...entity });

  await client.save();
  return client;
}
/* Delete Client Super admin by id */
/**
 * Delete Brand Model By id
 * @returns {Object} Search result
 */
async function deleteClientProfileByIdService(clientId, deleteId) {
  const client = await helper.ensureEntityExists(Client, { _id: deleteId }, 'The Client does not exist.');
  const ifCustomerHasPostSellItem = await VehicleSellModel.findOne({ createdBy: clientId });
  if (ifCustomerHasPostSellItem) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Customer has posted vehicles for sale. You must delete vehicle detail before');
  }
  if (client.isActive === false) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'Client is already deleted.');
  } else {
    client.isActive = false;
    client.deletedBy = deleteId;
    client.updatedAt = Date.now();
    client.save();
  }

  return {
    isActive: 200,
    message: 'Client is deleted successfully.'
  };
}

/**
 * Revert Deleted Client By id
 * @param clientId
 */

const revertDeletedClientById = async (clientId, adminId) => {
  const client = await helper.ensureEntityExists(Client, { _id: clientId }, 'The Client does not exist.');
  if (client.isActive === true) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'Client is already active.');
  } else {
    client.isActive = true;
    client.updatedBy = adminId;
    client.updatedAt = Date.now();
    client.save();
  }

  return {
    isActive: 200,
    message: 'Client is activated successfully.'
  };
};

/**
 * Send OTP to Mobile Phone
 * @param req.body
 */

const sendOTPSms = async (entity) => {
  const { mobile } = entity;
  const sendOtp = await helper.sendOTP(entity.mobile);
  const clientDetail = await Client.findOne({ mobile: entity.mobile });

  clientDetail.otpId = sendOtp.id;

  clientDetail.save();
  return {
    sendOtp,
    mobile
  };
};

const verifyOTPSms = async (value) => {
  const { id, token } = value;
  return new Promise((resolve, reject) => {
    messagebird.verify.verify(id, token, async (err, response) => {
      if (err) {
        reject(err);
      }
      if (response.isActive === 'verified') {
        const clientDetail = await Client.findOne({ otpId: id });

        clientDetail.phoneVerified = true;
        clientDetail.isActive = true;

        clientDetail.save();

        resolve({
          code: 201,
          message: 'Phone verified successfully'
        });
      }
    });
  });
};

// Get your balance
const getBalance = async () => {
  // eslint-disable-next-line consistent-return
  const value = await messagebird.balance.read((err, data) => {
    if (err) {
      return err;
    }
    return data;
  });
  return value;
};

/* checks client authentication using email and password
 * @param {Object} entity the entity
 * @returns {Object} the client information with token
 */
async function clientLogin(entity) {
  let client;
  if (entity.mobile) {
    client = await helper.ensureEntityExists(
      Client,
      { mobile: entity.mobile },
      `Sorry, we could not find any client with   ${entity.mobile} registered with us.`
    );
  }
  if (client.isActive === true) {
    const matched = await helper.validateHash(entity.password, client.password);

    if (!matched) {
      throw new errors.HttpStatusError(401, 'Wrong mobile number or password.');
    }

    helper.ensureClientVerified(client);

    // generate JWT token
    const token = jwt.sign(_.pick(client, ['id', 'fullName', 'email', 'accessLevel']), config.JWT_SECRET, {
      expiresIn: config.JWT_EXPIRATION
    });
    client.accessToken = token;
    await client.save();
    client = _.omit(client.toObject(), 'password', 'verificationToken', 'forgotPasswordToken', '__v');
    return {
      client
    };
  }
  throw new errors.NotPermittedError('This account is no longer available.');
}

/**
 * does forgot password process
 * @param {Object} entity the request body entity
 * @returns {Object} the client information
 */
async function forgotClientPassword(entity) {
  const client = await helper.ensureEntityExists(
    Client,
    { mobile: entity.mobile },
    `${entity.mobile} could not be found`
  );

  // send an otp in mobile
  const OTPresponse = await helper.sendOTP(entity.mobile);

  // update client information in database
  client.forgotOTPId = OTPresponse.id;
  await client.save();

  return {
    OTPId: OTPresponse.id,
    clientMobile: client.mobile,
    message: 'Please check your phone for the next step for password reset.'
  };
}

/**
 * handles the reset password
 * @param {Object} entity the request body
 * @returns {String} the success or failure
 */
async function resetClientPassword(entity) {
  const client = await helper.ensureEntityExists(Client, { forgotOTPId: entity.id }, 'User is not register');

  const newMatched = await helper.validateHash(entity.newPassword, client.password);
  if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else if (verifyOTPSms(entity)) {
    client.password = await helper.hashString(entity.newPassword);
    await client.save();
  } else {
    throw new errors.AuthenticationRequiredError('Sorry but your verification token is not valid.');
  }

  return { message: 'Your password has been reset successfully, please log in to continue!' };
}

/**
 * handles the update password
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function changeClientPassword(userId, entity) {
  const client = await helper.ensureEntityExists(Client, { _id: userId });
  const matched = await helper.validateHash(entity.oldPassword, client.password);
  const newMatched = await helper.validateHash(entity.newPassword, client.password);

  if (!matched) {
    throw new errors.HttpStatusError(401, 'Your old password is wrong.');
  } else if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else {
    client.password = await helper.hashString(entity.newPassword);
    await client.save();
  }
  return { message: 'Your Password has been changed successfully.' };
}

changeClientPassword.schema = {
  userId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        // .regex(/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/)
        .regex(/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/)
        .required(),
      oldPassword: joi
        .string()
        // .regex(/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/)
        .regex(/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/)
        .required()
    })
    .required()
};

/**
 * Permanently Delete Client by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const permanentDeleteClientById = async (clientId) => {
  const client = await getClientById(clientId);
  if (!client) {
    // throw new errors.HttpStatusError(401, 'Your old password is wrong.');
    throw new errors.HttpStatusError.NOT_FOUND('client is not found');
  }

  await Client.deleteOne({ _id: clientId });
  return client;
};

/**
 * handles logout of Client
 * @param {Object} userId the user id
 */
async function clientLogout(clientId) {
  const client = await helper.ensureEntityExists(Client, { _id: clientId });
  if (!client.accessToken) {
    throw new errors.NotPermittedError('client is already logged out');
  }
  client.accessToken = null;
  await client.save();
  return { message: 'Client is logout successfully' };
}

clientLogout.schema = {
  clientId: joi.string().required()
};

/* Create Location of Customer */
const createLocation = async (customerId, locationBody) => {
  const clientDetail = await helper.ensureEntityExists(Client, { _id: customerId });
  if (!clientDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }

  clientDetail.location.push(locationBody);
  clientDetail.save();
  return clientDetail.location;
};

/* Get Locations of Customer */
const getLocations = async (customerId) => {
  const clientDetail = await helper.ensureEntityExists(Client, { _id: customerId });
  if (!clientDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }

  return clientDetail.location;
};

/* Get Location of Customer */
const getLocationsById = async (customerId, locationId) => {
  const clientDetail = await helper.ensureEntityExists(Client, { _id: customerId });
  if (!clientDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }

  const loc = await Client.location.id({ _id: locationId });
  return loc;
};

/* Get Location of Customer */
const deleteLocationsById = async (customerId, locationId) => {
  const clientDetail = await helper.ensureEntityExists(Client, { _id: customerId });
  if (!clientDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }
  const location = await Client.location.id({ _id: locationId }, 'Location is not found');
  if (!location || typeof location === 'undefined') {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }

  const loc = await Client.location.id({ _id: locationId }).remove();
  if (loc) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  } else {
    return {
      status: 201,
      message: 'Location is delete successfully.'
    };
  }
};

/* Update Location of Customer */
const updateLocationsById = async (customerId, locationId, locationBody) => {
  const clientDetail = await helper.ensureEntityExists(Client, { _id: customerId });
  if (!clientDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Customer does not exist');
  }

  let loc = await clientDetail.location.id({ _id: locationId });
  // eslint-disable-next-line no-unused-vars
  loc = locationBody;
  clientDetail.save();
  return {
    status: 201,
    message: 'Client location is updated successfully'
  };
};

module.exports = {
  registerClient,
  getClientProfileList,
  getClientProfileByIdService,
  getDeletedClientProfileList,
  getClientById,
  updateClientProfileByIdService,
  deleteClientProfileByIdService,
  getDeletedClientProfileById,
  revertDeletedClientById,
  getClientProfile,
  updateClientProfile,
  sendOTPSms,
  clientLogin,
  verifyOTPSms,
  getBalance,
  forgotClientPassword,
  resetClientPassword,
  changeClientPassword,
  permanentDeleteClientById,
  clientLogout,
  createLocation,
  getLocations,
  getLocationsById,
  updateLocationsById,
  deleteLocationsById
};
