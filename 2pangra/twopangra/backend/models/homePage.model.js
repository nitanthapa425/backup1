const Schema = require('mongoose');

const uploadBannerImageSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },

  { _id: false }
);

module.exports = {
  uploadBannerImageSchema
};
