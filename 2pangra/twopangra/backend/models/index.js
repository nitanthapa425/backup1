/**
 * Copyright (C) Two Pangra
 */

/**
 * the model entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const config = require('config');
const db = require('../datasource').getDb(config.db.url, config.db.poolSize);
const { UserSchema } = require('./user.model');
const {
  BrandVehicleSchema, ModelSchema, VarientSchema, VaraiantNameValueSchema
} = require('./brandVehicle.model');
const {
  VehicleTypeSchema,
  TransmissionTypeSchema,
  FuelTypeSchema,
  BodyTypeSchema,
  SeatingCapacitySchema,
  FuelCapacitySchema,
  MileageSchema,
  MarketPriceSchema,
  OtherDetailSchema
} = require('./basicAutoValue.model');
const { VehicleDetailSchema } = require('./vehicleDetail.model');
const { ImageSchema } = require('./image.model');
const {
  InstrumentationSchema,
  SafetySchema,
  FeaturesSchema,
  IgnitionTypeSchema,
  MotorPowerSchema,
  SpeedoMeterSchema
} = require('./optionFeatureAutoValue.model');
const {
  WheelBaseSchema,
  OverallWidthSchema,
  OverallLengthSchema,
  OverallHeightSchema,
  GroundClearanceSchema,
  BootSpaceSchema,
  KerbWeightSchema,
  DoddleHeightSchema
} = require('./dimensionalFactor.model');
const {
  DisplacementSchema,
  EngineTypeSchema,
  NoOfCylinderSchema,
  ValvesPerCylinderSchema,
  ValveConfigurationSchema,
  FuelSupplySystemSchema,
  MaximumPowerSchema,
  MaximumTorqueSchema,
  LubricationSchema,
  EngineOilSchema,
  AirCleanerSchema,
  BoreXStrokeSchema,
  CompressionRatioSchema
} = require('./engineFactor.model');
const {
  FrontWheelType,
  RearWheelType,
  FrontWheelSize,
  RearWheelSize,
  RearTyreSchema,
  FrontTyreSchema,
  FrontWheelDriveSchema,
  AlloyWheelSchema,
  SteelRimsSchema
} = require('./wheelFactor.model');
const {
  FrontBrakeSystemSchema,
  BackBrakeSystemSchema,
  FrontSuspensionSchema,
  RearSuspensionSchema,
  NoOfGearsSchema,
  DriveTypeSchema,
  ClutchTypeSchema,
  GearPatternSchema,
  HeadlightSchema,
  TaillightSchema,
  StarterSchema,
  BatterySchema
} = require('./technicalAutoValue.model');
const { draftVehicleSchema } = require('./draftVehicle.model');
const { VehicleSellSchema } = require('./vehicleSell.model');
const { VehicleSellDraftSchema } = require('./vehicleSellDraft.model');
const { ClientSchema } = require('./client.model');
const { LogSchema } = require('./logSchema.model');
const { cartSchema } = require('./cart.model');
const { wishlistSchema } = require('./wishlist.model');
const { bookSchema } = require('./book.model');
const { meetingSchedule } = require('./meetingSchedule.model');
const { municipalitySchema, toleSchema } = require('./geoLocation.model');
const { logManagementSchema } = require('./logManagement.model');
const { notificationSchema } = require('./notification.model');
const { emailSubscribe } = require('./emailSubscribe.model');
const { postExpiryDateSchema } = require('./postExpiryDate.model');
const { commentSchema } = require('./comment.model');
const { offerSchema } = require('./offer.model');

module.exports = {
  db,
  User: db.model('User', UserSchema),
  Client: db.model('Client', ClientSchema),
  BrandVehicle: db.model('BrandVehicle', BrandVehicleSchema),
  BrandModel: db.model('BrandModel', ModelSchema),
  VarientModel: db.model('VarientModel', VarientSchema),
  CartModel: db.model('CartModel', cartSchema),
  EmailSubscribe: db.model('EmailSubscribe', emailSubscribe),
  PostExpiryDateModel: db.model('PostExpiryDateModel', postExpiryDateSchema),
  Comment: db.model('Comment', commentSchema),
  WishlistModel: db.model('WishlistModel', wishlistSchema),
  BookModel: db.model('BookModel', bookSchema),
  MeetingSchedule: db.model('MeetingSchedule', meetingSchedule),
  VaraiantNameValueModel: db.model('VaraiantNameValueModel', VaraiantNameValueSchema),
  VehicleTypeValueModel: db.model('VehicleTypeValue', VehicleTypeSchema),
  TransmissionTypeValueModel: db.model('TransmissionTypeValue', TransmissionTypeSchema),
  FuelTypeValueModel: db.model('FuelTypeValueModel', FuelTypeSchema),
  BodyTypeModel: db.model('BodyTypeModel', BodyTypeSchema),
  SeatingCapacityModel: db.model('SeatingCapacityModel', SeatingCapacitySchema),
  FuelCapacityModel: db.model('FuelCapacityModel', FuelCapacitySchema),
  MileageModel: db.model('MileageModel', MileageSchema),
  VehicleDetail: db.model('VehicleDetail', VehicleDetailSchema),
  Image: db.model('Image', ImageSchema),
  WheelBaseModel: db.model('WheelBaseModel', WheelBaseSchema),
  OverallWidthModel: db.model('OverallWidthModel', OverallWidthSchema),
  OverallLengthModel: db.model('OverallLengthModel', OverallLengthSchema),
  OverallHeightModel: db.model('OverallHeightModel', OverallHeightSchema),
  GroundClearanceModel: db.model('GroundClearanceModel', GroundClearanceSchema),
  BootSpaceModel: db.model('BootSpaceModel', BootSpaceSchema),
  KerbWeightModel: db.model('KerbWeightModel', KerbWeightSchema),
  DoddleHeightModel: db.model('DoddleHeightModel', DoddleHeightSchema),
  DisplacementModel: db.model('DisplacementModel', DisplacementSchema),
  // FuelEfficiencyModel: db.model('FuelEfficiencyModel', FuelEfficiencySchema),
  EngineTypeModel: db.model('EngineTypeModel', EngineTypeSchema),
  NoOfCylinderModel: db.model('NoOfCylinderModel', NoOfCylinderSchema),
  ValvesPerCylinderModel: db.model('ValvesPerCylinderModel', ValvesPerCylinderSchema),
  ValveConfigurationModel: db.model('ValveConfigurationModel', ValveConfigurationSchema),
  FuelSupplySystemModel: db.model('FuelSupplySystemModel', FuelSupplySystemSchema),
  MaximumPowerModel: db.model('MaximumPowerModel', MaximumPowerSchema),
  MaximumTorqueModel: db.model('MaximumTorqueModel', MaximumTorqueSchema),
  LubricationModel: db.model('LubricationModel', LubricationSchema),
  EngineOilModel: db.model('EngineOilModel', EngineOilSchema),
  AirCleanerModel: db.model('AirCleanerModel', AirCleanerSchema),
  BoreXStrokeModel: db.model('BoreXStrokeModel', BoreXStrokeSchema),
  CompressionRatioModel: db.model('CompressionRatioModel', CompressionRatioSchema),
  FrontWheelTypeModel: db.model('FrontWheelTypeModel', FrontWheelType),
  RearWheelTypeModel: db.model('RearWheelTypeModel', RearWheelType),
  FrontWheelSizeModel: db.model('FrontWheelSizeModel', FrontWheelSize),
  RearWheelSizeModel: db.model('RearWheelSizeModel', RearWheelSize),
  RearTyreModel: db.model('RearTyreModel', RearTyreSchema),
  FrontTyreModel: db.model('FrontTyreModel', FrontTyreSchema),
  FrontWheelDriveModel: db.model('FrontWheelDriveModel', FrontWheelDriveSchema),
  AlloyWheelModel: db.model('AlloyWheelModel', AlloyWheelSchema),
  SteelRimsModel: db.model('SteelRimsModel', SteelRimsSchema),
  InstrumentationModel: db.model('InstrumentationModel', InstrumentationSchema),
  SafetyModel: db.model('SafetyModel', SafetySchema),
  FeatureModel: db.model('FeatureModel', FeaturesSchema),
  IgnitionModel: db.model('IgnitionModel', IgnitionTypeSchema),
  FrontBrakeSystemModel: db.model('FrontBrakeSystemModel', FrontBrakeSystemSchema),
  BackBrakeSystemModel: db.model('BackBrakeSystemModel', BackBrakeSystemSchema),
  FrontSuspensionModel: db.model('FrontSuspensionModel', FrontSuspensionSchema),
  RearSuspensionModel: db.model('RearSuspensionModel', RearSuspensionSchema),
  NoOfGearsModel: db.model('NoOfGearsModel', NoOfGearsSchema),
  DriveTypeModel: db.model('DriveTypeModel', DriveTypeSchema),
  ClutchTypeModel: db.model('ClutchTypeModel', ClutchTypeSchema),
  GearPatternModel: db.model('GearPatternModel', GearPatternSchema),
  HeadlightModel: db.model('HeadlightModel', HeadlightSchema),
  TaillightModel: db.model('TaillightModel', TaillightSchema),
  StarterModel: db.model('StarterModel', StarterSchema),
  BatteryModel: db.model('BatteryModel', BatterySchema),
  MarketPriceModel: db.model('MarketPriceModel', MarketPriceSchema),
  OtherDetailModel: db.model('OtherDetailModel', OtherDetailSchema),
  MotorPowerModel: db.model('MotorPowerModel', MotorPowerSchema),
  SpeedoMeterModel: db.model('SpeedoMeterModel', SpeedoMeterSchema),
  draftVehicleModel: db.model('draftVehicleModel', draftVehicleSchema),
  VehicleSellModel: db.model('VehicleSellModel', VehicleSellSchema),
  VehicleSellDraftModel: db.model('VehicleSellDraftModel', VehicleSellDraftSchema),
  LogSchemaModel: db.model('LogSchemaModel', LogSchema),
  GeoLocationSchemaModel: db.model('GeoLocationSchemaModel', municipalitySchema),
  fullLocationModel: db.model('fullLocationModel', toleSchema),
  LogManagementModel: db.model('LogManagementModel', logManagementSchema),
  NotificationModel: db.model('NotificationModel', notificationSchema),
  OfferModel: db.model('OfferModel', offerSchema)
};
