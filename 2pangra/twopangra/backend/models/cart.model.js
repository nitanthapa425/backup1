/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const cartSchema = new Schema(
  {
    clientId: {
      type: String
    },
    sellerVehicleId: {
      type: String
    },
    vehicleName: {
      type: String,
    },
    vehicleImage: [{
      imageUrl: {
        type: String,
        required: [true, 'At least a image is required']
      },
      publicKey: {
        type: String,
        required: [true, 'Public key is required']
      }
    },
    { id: false }],
    price: {
      type: Number
    },
    color: {
      type: String
    },
    totalPrice: {
      type: Number
    },
    numViews: {
      type: Number
    },
    bikeDriven: {
      type: Number
    },
    combinelocation: {
      type: String
    },
    ownnershipcount: {
      type: Number
    },
    isVerified: {
      type: Boolean
    },
    status: {
      type: Boolean
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

cartSchema.plugin(mongoosePaginate);
module.exports = {
  cartSchema
};
