/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */
const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

// const ClientAddressSchema = new Schema({
//   label: {
//     type: String,
//     enum: ['residential', 'permanent', 'billing']
//   },
//   country: {
//     type: String
//   },
//   province: {
//     type: String
//   },
//   district: {
//     type: String
//   },
//   municipalityVdc: {
//     type: String
//   },
//   combineLocation: {
//     type: String
//     // combine save province district and municipality
//   },
//   postalCode: {
//     type: String
//   },
//   street: {
//     type: String
//   },
//   nearbyLocation: {
//     type: String
//   },
//   // isPermanent: {
//   //   type: String
//   // },
//   geoLocation: {
//     type: {
//       type: String, // Don't do `{ location: { type: String } }`
//       enum: ['Point'] // 'location.type' must be 'Point'
//     },
//     coordinates: {
//       type: [Number]
//     }
//   }
// });
const MeetingScheduleSchema = new Schema(
  {
    meetingDateTime: {
      type: Date
    },
    combineLocation: {
      type: String
    },
    nearByLocation: {
      type: String
    },
    ownershipCount: {
      type: String
    },
    meetingPlace: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

const bookSchema = new Schema(
  {
    clientId: {
      type: String
    },
    vehicleId: {
      type: String
    },
    clientName: {
      type: String
    },
    sellerVehicleId: {
      type: Schema.Types.ObjectId,
      ref: 'VehicleSellModel'
    },
    // cartId: {
    //   type: String
    // },
    vehicleName: {
      type: String,
    },

    clientMobile: {
      type: String,
    },
    vehicleImage: [{
      imageUrl: {
        type: String,
        required: [true, 'At least a image is required']
      },
      publicKey: {
        type: String,
        required: [true, 'Public key is required']
      }
    },
    { id: false }],
    price: {
      type: Number
    },
    color: {
      type: String
    },
    totalPrice: {
      type: Number
    },
    bikeDriven: {
      type: Number
    },
    combineLocation: {
      type: String
    },
    ownershipCount: {
      type: String
    },
    isVerified: {
      type: Boolean
    },
    numViews: {
      type: Number
    },
    meetingSchedule: MeetingScheduleSchema,
    status: {
      type: Boolean
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

bookSchema.plugin(mongoosePaginate);
module.exports = {
  bookSchema
};
