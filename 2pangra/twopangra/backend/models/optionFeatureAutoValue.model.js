/**
 * Copyright (C) Dui Pangra
 */

/**
 * The Option Feature Auto Value Detail Schema
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');

const InstrumentationSchema = new Schema({
  instrumentation: {
    type: String,
    unique: true,
    minlength: [2, 'Instrumentation must be at least 2.'],
    maxlength: [150, 'Instrumentation must be at must 150.']
  }
});

const SafetySchema = new Schema({
  safety: {
    type: String,
    unique: true,
    minlength: [2, 'Safety must be at least 2.'],
    maxlength: [150, 'Safety must be at must 150.']
  }
});

const FeaturesSchema = new Schema({
  feature: {
    type: String,
    unique: true,
    minlength: [2, 'Feature must be at least 2.'],
    maxlength: [150, 'Feature must be at must 150.']
  }
});

const IgnitionTypeSchema = new Schema({
  ignitionType: {
    type: String,
    unique: true,
    // maxlength: [50, 'Please, fill the ignition type with maximum 50 characters only.']
    minlength: [2, 'Ignition Type must be at least 2.'],
    maxlength: [150, 'Ignition Type must be at must 150.']
  }
});

// /* This model is for electric bike */
// const Speedometer = new Schema({
//   speedometer: {
//     type: String,
//     unique: true
//   }
// });
// /* This model is for electric bike */
// const TripMeter = new Schema({
//   tripMeter: {
//     type: String,
//     unique: true
//   }
// });

/* This model is for electric bike */
const MotorPowerSchema = new Schema({
  motorPower: {
    type: String,
    unique: true,
    minlength: [2, 'Motor Power must be at least 2'],
    maxlength: [150, 'Motor Power must be at must 150']
  }
});

/* This model is for electric bike */
const SpeedoMeterSchema = new Schema({
  speedometer: {
    type: String,
    unique: true
  }
});

module.exports = {
  InstrumentationSchema,
  SafetySchema,
  FeaturesSchema,
  IgnitionTypeSchema,
  MotorPowerSchema,
  SpeedoMeterSchema
};
