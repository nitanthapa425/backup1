/**
 * Copyright (C) Dui Pangra
 */

/**
 * the Vehicle Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { validateMaxTenNum, validateFloatNumLess100 } = require('../utils/helper');

const LocationSchema = new Schema(
  {
    country: {
      type: String,
      default: 'Nepal',
      required: [true, 'Country is required']
    },
    combineLocation: {
      type: String
    },
    nearByLocation: {
      type: String
    },
    status: {
      type: String,
      enum: ['Location']
    },
    geoLocation: {
      type: {
        type: String, // Don't do `{ location: { type: String } }`
        enum: ['Point'] // 'location.type' must be 'Point'
      },
      coordinates: {
        type: [Number]
      }
    }
  },
  { id: false }
);

const billBookImageSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    }
  },
  { _id: false }
);

const bikeImageSchema = new Schema(
  {
    imageUrl: {
      type: String,
      required: [true, 'At least a image is required']
    },
    publicKey: {
      type: String,
      required: [true, 'Public key is required']
    }
  },
  { _id: false }
);

const VehicleSellSchema = new Schema(
  {
    bikeImagePath: [bikeImageSchema],
    billBookImagePath: [billBookImageSchema],
    location: LocationSchema,
    vehicleName: {
      type: String
    },
    brandName: {
      type: String
    },
    modelName: {
      type: String
    },
    varientName: {
      type: String
    },
    bikeDriven: {
      type: String,
      minlength: 0,
      maxlength: [6, 'Please, fill the bike driven less than 10 lakh']
    },
    expectedPrice: {
      type: String,
      maxlength: [7, 'Please fill expected price less than 1 crore'],
      required: true
    },
    isNegotiable: {
      type: String,
      enum: ['Yes', 'No', 'N/A']
    },
    bikeNumber: {
      type: String,
      maxlength: [20, 'Please fill bike number less than 20 character.']
    },
    lotNumber: {
      type: String,
      // required: true,
      maxlength: [3, 'Please fill lot number with less than 3 character.']
    },
    note: {
      type: String
    },
    color: [
      {
        type: String
      }
    ],
    condition: {
      type: String,
      enum: ['Brand New', 'Like New', 'Excellent', 'Good/Fair', 'Not Working']
    },
    mileage: {
      type: String,
      validate: [validateFloatNumLess100, 'Please fill a Mileage with number less than 100.']
    },
    ownershipCount: {
      type: String,
      validate: [validateMaxTenNum, 'Please fill the ownership count with appropriate value Eg-: 5']
    },
    makeYear: {
      type: Date
    },
    fuelType: {
      type: String
    },
    vehicleType: {
      type: String
    },
    postExpiryDate: {
      type: Date,
      required: [true, 'Post expiry is required.']
    },
    hasAccident: {
      type: String,
      enum: ['Yes', 'No', 'N/A']
    },
    suggestedPrice: {
      type: String,
      minlength: [4, 'Price cannot be less than 10000'],
      maxlength: [9, 'Price cannot be more than 99 crore']
    },
    productStatus: {
      type: String
    },
    isApproved: {
      type: Boolean,
      default: true
    },
    unapprovedReason: {
      type: String
    },
    isVerified: {
      type: Boolean,
      default: false
    },
    usedFor: {
      type: String
    },
    vehicleDetailId: {
      type: Schema.Types.ObjectId,
      ref: 'VehicleDetail',
      required: [true, 'Vehicle Detail Id is required.']
    },
    hasInFeature: {
      type: Boolean,
      default: false
    },
    hasInFeaturePending: {
      type: Boolean,
      default: false
    },
    hasInFeaturePendingDate: {
      type: Date
    },
    clientAddToFeature: {
      type: Boolean,
      default: false
    },
    isBooked: {
      type: Boolean,
      default: false
    },
    isSold: {
      type: Boolean,
      default: false
    },
    isSoldMarkedBy: {
      type: String
    },
    hasInHotDeal: {
      type: Boolean,
      default: false
    },
    numViews: {
      type: Number
    },
    clientId: {
      type: String
    },
    featureTime: {
      type: Date
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    status: {
      type: Boolean,
      default: true
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

// /* Make first letter to uppercase of back brake system */
// TechnicalSchema.pre('save', function (next) {
//   this.backBrakeSystem = this.backBrakeSystem.trim()[0].toUpperCase() + this.backBrakeSystem.slice(1).toLowerCase();
//   next();
// });

// /* Make first letter to uppercase of front brake system */
// VehicleSellSchema.pre('save', function (next) {
//   this.color = this.color.trim()[0].toUpperCase() + this.color.slice(1).toLowerCase();
//   next();
// });

VehicleSellSchema.plugin(mongoosePaginate);
module.exports = {
  VehicleSellSchema
};
