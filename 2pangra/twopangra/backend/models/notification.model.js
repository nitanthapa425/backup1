/**
 * Copyright (C) Tuki Logic
 */

/**
 * the log management schema
 * @author      SUSAN DHAKAL
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const notificationSchema = new Schema(
  {
    notificationType: {
      type: String,
      enum: ['Add', 'Delete', 'Update', 'Sold', 'Unsold', 'Meeting']
    },
    notificationCategory: {
      type: String,
      enum: ['Cart', 'Wishlist', 'Book', 'Vehicle Sell']
    },
    content: {
      type: String,
      required: true
    },
    isRead: {
      type: Boolean,
      default: false
    },
    accessLevel: {
      type: String,
      enum: ['SUPER_ADMIN', 'ADMIN', 'SELLER', 'BUYER']
    },
    status: {
      type: String,
      default: 'active'
    },
    itemId: {
      type: String
    },
    imageUrl: {
      type: String
    },
    createdBy: {
      type: String
    },
    createdByName: {
      type: String
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

notificationSchema.plugin(mongoosePaginate);

module.exports = {
  notificationSchema
};
