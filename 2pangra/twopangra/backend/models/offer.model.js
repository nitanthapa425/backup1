/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const offerSchema = new Schema(
  {
    sellerVehicleId: {
      type: Schema.Types.ObjectId,
      ref: 'VehicleSellModel'
    },
    vehicleName: {
      type: String
    },
    offerPrice: {
      type: String,
      minlength: [5, 'Minimum final Offer price should be minimim 10 thousand.'],
      maxlength: [8, 'Maximum final offer price should be maximum 1 crore.']
    },
    realPrice: {
      type: String
    },
    status: {
      type: String,
      enum: ['pending', 'approved', 'rejected', 'final offer']
    },
    rejectRemarks: {
      type: String
    },
    finalOffer: {
      type: String,
      minlength: [5, 'Minimum final Offer price should be minimim 10 thousand.'],
      maxlength: [8, 'Maximum final offer price should be maximum 1 crore.']
    },
    buyerName: {
      type: String
    },
    sellerId: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    sellerName: {
      type: String
    },
    createdAt: {
      type: Date
    },
    updatedAt: {
      type: Date
    },
    clientName: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

offerSchema.plugin(mongoosePaginate);
module.exports = {
  offerSchema
};
