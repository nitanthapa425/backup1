/**
 * Copyright (C) Tuki Logic
 */

/**
 * the log management schema
 * @author      SUSAN DHAKAL
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const logManagementSchema = new Schema(
  {
    logType: {
      type: String,
      enum: ['Add Data', 'Update Data', 'Delete Data', 'Add in Landing Page', 'Remove from Landing Page', 'Add Cart', 'Add Sellers Vehicle'],
      required: true
    },
    category: {
      type: String,
      enum: ['Users', 'Vehicle Details', 'Draft Vehicle Details', 'Brands', 'Models', 'Variants', 'Carts', 'Sellers Vehicle'],
      required: true
    },
    logDescription: {
      type: String,
      required: true
    },
    status: {
      type: String,
      default: 'active'
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    createdByName: {
      type: String
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

logManagementSchema.plugin(mongoosePaginate);

module.exports = {
  logManagementSchema
};
