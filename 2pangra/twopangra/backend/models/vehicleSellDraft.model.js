/**
 * Copyright (C) Dui Pangra
 */

/**
 * the Vehicle Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { validateMaxTenNum, validateFloatNumLess100 } = require('../utils/helper');

const VehicleSellDraftSchema = new Schema(
  {
    bikeImagePath: [
      {
        imageUrl: {
          type: String,
          required: [true, 'At least a image is required']
        },
        publicKey: {
          type: String,
          required: [true, 'Public key is required']
        }
      }
    ],
    billBookImagePath: [
      {
        imageUrl: {
          type: String,
          required: [true, 'At least a image is required']
        },
        publicKey: {
          type: String,
          required: [true, 'Public key is required']
        }
      }
    ],
    bikeDriven: {
      type: String,
      minlength: 1,
      maxlength: [6, 'Please, fill the bike driven less than 10 lakh']
    },
    expectedPrice: {
      type: String,
      maxlength: [7, 'Please fill expected price less than 1 crore']
    },
    isNegotiable: {
      type: String,
      enum: ['Yes', 'No', 'N/A']
    },
    bikeNumber: {
      type: String,
      maxlength: [20, 'Please fill bike number less than 20 character.']
    },
    note: {
      type: String
    },
    color: [
      {
        type: String
      }
    ],
    condition: {
      type: String
    },
    mileage: {
      type: String,
      validate: [validateFloatNumLess100, 'Please fill a Mileage with number less than 100.']
    },
    ownershipCount: {
      type: String,
      validate: [validateMaxTenNum, 'Please fill the ownership count with appropriate value Eg-: 5']
    },
    postExpiryDate: {
      type: Date,
      required: [true, 'Post expiry is required.']
    },
    hasAccident: {
      default: false,
      type: Boolean
    },
    suggestedPrice: {
      type: String,
      minlength: [4, 'Price cannot be less than 10000'],
      maxlength: [9, 'Price cannot be more than 99 crore']
    },
    productStatus: {
      type: String
    },
    isApproved: {
      default: false,
      type: Boolean
    },
    unapprovedReason: {
      type: String
    },
    usedFor: {
      type: String
    },
    vehicleName: {
      type: String
    },
    featureTime: {
      type: Date
    },
    vehicleDetailId: {
      type: Schema.Types.ObjectId,
      ref: 'VehicleDetail',
      required: [true, 'Vehicle Detail Id is required.']
    },
    clirntId: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

VehicleSellDraftSchema.plugin(mongoosePaginate);
module.exports = {
  VehicleSellDraftSchema
};
