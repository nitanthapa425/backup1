/**
 * Copyright (C) Dui Pangra
 */

/**
 * The Dimensional Feature Auto Value Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const WheelBaseSchema = new Schema({
  wheelBase: {
    type: Number,
    unique: true
  }
});

const OverallWidthSchema = new Schema({
  overallWidth: {
    type: Number,
    unique: true
  }
});

const OverallLengthSchema = new Schema({
  overallLength: {
    type: Number,
    unique: true
  }
});

const OverallHeightSchema = new Schema({
  overallHeight: {
    type: Number,
    unique: true
  }
});

const GroundClearanceSchema = new Schema({
  groundClearance: {
    type: Number,
    unique: true
  }
});

const BootSpaceSchema = new Schema({
  bootSpace: {
    type: Number,
    unique: true
  }
});

const KerbWeightSchema = new Schema({
  kerbWeight: {
    type: Number,
    unique: true
  }
});

const DoddleHeightSchema = new Schema({
  doddleHeight: {
    type: Number,
    unique: true
  }
});

module.exports = {
  WheelBaseSchema,
  OverallWidthSchema,
  OverallLengthSchema,
  OverallHeightSchema,
  GroundClearanceSchema,
  BootSpaceSchema,
  KerbWeightSchema,
  DoddleHeightSchema
};
