/**
 * Copyright (C) Dui Pangra
 */

/**
 * The Technical Auto Value Detail Schema
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');

const FrontBrakeSystemSchema = new Schema({
  frontBrakeSystem: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the front brake system with maximum 150 characters only.']
  }
});

const BackBrakeSystemSchema = new Schema({
  backBrakeSystem: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the back brake system with maximum 150 characters only.']
  }
});

const FrontSuspensionSchema = new Schema({
  frontSuspension: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the front suspension with maximum 150 characters only.']
  }
});

const RearSuspensionSchema = new Schema({
  rearSuspension: {
    type: String,
    unique: true
  }
});

const NoOfGearsSchema = new Schema({
  noOfGears: {
    type: Number,
    unique: true,
    min: [1, 'Number of Gears must be at least 1.'],
    max: [10, 'Number of Gears must be at must 10.']

    // validate: [validateMaxTenNum, 'Please fill a Number Of Gears with less than 10 number only.']
  }
});

const DriveTypeSchema = new Schema({
  driveType: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the drive type with maximum 150 characters only.']
  }
});

const ClutchTypeSchema = new Schema({
  clutchType: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the clutch type with maximum 150 characters only.']
  }
});

const GearPatternSchema = new Schema({
  gearPattern: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the gear type with maximum 150 characters only.']
  }
});

const HeadlightSchema = new Schema({
  headlight: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the head light with maximum 150 characters only.']
  }
});

const TaillightSchema = new Schema({
  taillight: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the tail light with maximum 150 characters only.']
  }
});

const StarterSchema = new Schema({
  starter: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the starter with maximum 150 characters only.']
  }
});

const BatterySchema = new Schema({
  battery: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the battery with maximum 150 characters only.']
  }
});

module.exports = {
  FrontBrakeSystemSchema,
  BackBrakeSystemSchema,
  FrontSuspensionSchema,
  RearSuspensionSchema,
  NoOfGearsSchema,
  DriveTypeSchema,
  ClutchTypeSchema,
  GearPatternSchema,
  HeadlightSchema,
  TaillightSchema,
  StarterSchema,
  BatterySchema
};
