/**
 * Copyright (C) Dui Pangra
 */

/**
 * The Dimensional Feature Auto Value Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const DisplacementSchema = new Schema({
  displacement: {
    type: Number,
    unique: true,
    min: [1, 'Displacement must be at least 1'],
    max: [1000, 'Displacement must be at must 1000']
  }
});

// const FuelEfficiencySchema = new Schema({
//   fuelEfficiency: {
//     type: Number,
//     unique: true,
//   }
// });

const EngineTypeSchema = new Schema({
  engineType: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the engine type with maximum 150 characters only.']
  }
});

const NoOfCylinderSchema = new Schema({
  noOfCylinder: {
    type: Number,
    unique: true,
    min: [0, 'No of cylinder must be at least 0.'],
    max: [5, 'No of cylinder must be at must 5.']
  }
});

const ValvesPerCylinderSchema = new Schema({
  valvesPerCylinder: {
    type: Number,
    unique: true,
    min: [0, 'Valves per cylinder must be at least 0.'],
    max: [5, 'Valves per cylinder must be at must 5.']
  }
});
const ValveConfigurationSchema = new Schema({
  valveConfiguration: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the valve type with maximum 150 characters only.']
  }
});

const FuelSupplySystemSchema = new Schema({
  fuelSupplySystem: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the valve type with maximum 150 characters only.']
  }
});

const MaximumPowerSchema = new Schema({
  maximumPower: {
    type: String,
    unique: true,
    minlength: [2, 'Please, fill the maximum Power with minimum 2 characters only.'],
    maxlength: [150, 'Please, fill the maximum Power with maximum 150 characters only.']
  }
});

const MaximumTorqueSchema = new Schema({
  maximumTorque: {
    type: String,
    unique: true
  }
});

const LubricationSchema = new Schema({
  lubrication: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the lubrication  with maximum 150 characters only.']
  }
});

const EngineOilSchema = new Schema({
  engineOil: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the engine oil with maximum 150 characters only.']
  }
});

const AirCleanerSchema = new Schema({
  airCleaner: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the air cleaner with maximum 150 characters only.']
  }
});

const BoreXStrokeSchema = new Schema({
  boreXStroke: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the boreXStroke with maximum 150 characters only.']
  }
});

const CompressionRatioSchema = new Schema({
  compressionRatio: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the compression ratio with maximum 150 characters only.']
  }
});

module.exports = {
  DisplacementSchema,
  // FuelEfficiencySchema,
  EngineTypeSchema,
  NoOfCylinderSchema,
  ValvesPerCylinderSchema,
  ValveConfigurationSchema,
  FuelSupplySystemSchema,
  MaximumPowerSchema,
  MaximumTorqueSchema,
  LubricationSchema,
  EngineOilSchema,
  AirCleanerSchema,
  BoreXStrokeSchema,
  CompressionRatioSchema
};
