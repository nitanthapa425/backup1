/**
 * Copyright (C) Dui Pangra
 */

/**
 * the Vehicle Detail Schema
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const {
  validateNumber,
  validateMax1500Num,
  validateMaxSeventyNum,
  validateMaxOneHunFiftyChar,
  validateMaxFiveNum,
  validateMaxTenNum,
  validateFloatNumLess100,
  validateFloatNum
} = require('../utils/helper');

const BasicFactorSchema = new Schema(
  {
    transmissionType: {
      type: String,
      validate: [
        validateMaxOneHunFiftyChar,
        'Please, fill the transmission type with minimum 2 and  maximum 150 characters only.'
      ]
      // minlength: 2,
      // maxlength: [150, 'Please, fill the transmission type with maximum 150 characters only.'],
    },
    bodyType: {
      type: String,
      validate: [validateMaxOneHunFiftyChar, 'Please, fill the body type with minimum 2 and  maximum 150 characters only.']
      // minlength: 2,
      // maxlength: [150, 'Please, fill the body type with minimum 2 and  maximum 150 characters only.'],
    },
    seatingCapacity: {
      type: String,
      // maxlength: [1, 'Please fill Seating Capacity with relevant number.'],
      validate: [validateMaxFiveNum, 'Please fill a Seating Capacity with less tha 5 number.']
    },
    fuelCapacity: {
      type: String,
      // maxlength: [5, 'Please fill fuel Capacity with  relevant number.'],
      // required: true,
      validate: [validateMaxSeventyNum, 'Please fill a Fuel Capacity with minimum 2 maximum 70 litre.']
    },
    mileage: {
      type: String,
      // max: [100, 'Please fill mileage below 100.']
      validate: [validateFloatNumLess100, 'Please fill a Mileage with number less than 100.']
    },
    color: {
      type: String
    }
  },
  { _id: false }
);

const OptionFeatureSchema = new Schema(
  {
    instrumentation: [
      {
        type: String
      }
    ],
    safety: [
      {
        type: String
      }
    ],
    features: [
      {
        type: String
      }
    ],
    ignitionType: {
      type: String,
      maxlength: [150, 'Please, fill the ignition type with maximum 150 characters only.']
      // required: true
    },
    //  model of electric bike
    chargingPoint: {
      type: Boolean
    },
    //  model of electric bike
    speedometer: {
      type: String,
      maxlength: [150, 'Please, fill the speedometer type with maximum 150 characters only.']
    },
    //  model of electric bike
    tripMeter: {
      type: String,
      maxlength: [150, 'Please, fill the trip meter type with maximum 150 characters only.']
    },
    //  model of electric bike
    passSwitch: {
      type: Boolean
    },
    //  model of electric bike
    clock: {
      type: Boolean
    },
    //  model of electric bike
    ridingModes: {
      type: Boolean
    },
    //  model of electric bike
    navigation: {
      type: Boolean
    },
    //  model of electric bike
    chargingAtHome: {
      type: Boolean
    },
    //  model of electric bike
    chargingAtChargingStation: {
      type: Boolean
    }
  },
  { _id: false }
);

const TechnicalSchema = new Schema(
  {
    frontBrakeSystem: {
      type: String,
      validate: [validateMaxOneHunFiftyChar, 'Please, fill the front brake system with maximum 150 characters only.']
    },
    backBrakeSystem: {
      type: String,
      // maxlength: [150, 'Please, fill the back brake system with maximum 150 characters only.']
      validate: [validateMaxOneHunFiftyChar, 'Please, fill the back brake system with maximum 150 characters only.']
    },
    frontSuspension: {
      type: String,
      maxlength: [150, 'Please, fill the front suspension with maximum 150 characters only.']
      // required: true
    },
    rearSuspension: {
      type: String
      // required: true
    },
    noOfGears: {
      type: String,
      // max: [5, 'Please fill gears with relevant number'],
      // required: true,
      validate: [validateMaxTenNum, 'Please fill a Number Of Gears with less than 10 number only.']
    },
    driveType: {
      type: String,
      maxlength: [150, 'Please, fill the drive type with maximum 150 characters only.']
      // required: true
    },
    clutchType: {
      type: String,
      // maxlength: [150, 'Please, fill the clutch type with maximum 150 characters only.']
      validate: [validateMaxOneHunFiftyChar, 'Please, fill the clutch type with maximum 150 characters only.']
    },
    gearPattern: {
      type: String,
      maxlength: [150, 'Please, fill the gear type with maximum 150 characters only.']
    },
    headlight: {
      type: String,
      maxlength: [150, 'Please, fill the head light with maximum 150 characters only.']
    },
    taillight: {
      type: String,
      maxlength: [150, 'Please, fill the tail light with maximum 150 characters only.']
    },
    starter: [
      {
        type: String,
        maxlength: [150, 'Please, fill the starter with maximum 150 characters only.']
      }
    ],
    battery: {
      type: String,
      maxlength: [150, 'Please, fill the battery with maximum 150 characters only.']
    },
    //  model of electric bike
    lowBatteryIndicator: {
      type: Boolean
    }
  },
  { _id: false }
);

const DimensionalSchema = new Schema(
  {
    wheelBase: {
      type: String,
      validate: [validateFloatNum, 'Please fill a Wheel Base with number format only.']
    },
    overallWidth: {
      type: String,
      validate: [validateFloatNum, 'Please fill a Over All Width Base with number format only.']
    },
    overallLength: {
      type: String,
      validate: [validateFloatNum, 'Please fill a Over All Length with number format only.']
    },
    overallHeight: {
      type: String,
      validate: [validateFloatNum, 'Please fill a Over All Height with number format only.']
    },
    groundClearance: {
      type: String,
      // required: true,
      validate: [validateFloatNum, 'Please fill a Kerb Weight with number format only.']
    },
    // Only on Scooter
    bootSpace: {
      type: String,
      validate: [validateFloatNum, 'Please fill a boot space with number format only.']
    },
    kerbWeight: {
      type: String,
      // required: true,
      validate: [validateFloatNum, 'Please fill a Kerb Weight with number format only.']
    },
    doddleHeight: {
      type: String,
      validate: [validateFloatNum, 'Please fill a Doodle Height in number format only.']
    }
  },
  { _id: false }
);

const EngineSchema = new Schema(
  {
    motorPower: {
      type: String,
      validate: [validateMaxOneHunFiftyChar, 'Please fill the motor power less than 150 character']
    },
    displacement: {
      type: String,
      validate: [validateMax1500Num, 'Please fill a Displacement less than 1500 number only.']
      // required: true
    },
    // fuelEfficiency: {
    //   type: String,
    //   // required: true,
    //   validate: [validateFloatNum, 'Please fill a Fuel Efficiency in number format only.']
    // },
    engineType: [
      {
        type: String
        // maxlength: [150, 'Please, fill the engine type with maximum 150 characters only.']
        // required: true
      }
    ],
    noOfCylinder: {
      type: String,
      // required: true,
      validate: [validateNumber, 'Please fill a valid No Of Cylinder in number format only.']
    },
    valvesPerCylinder: {
      type: String,
      // required: true,
      validate: [validateNumber, 'Please fill a valid Valves Per Cylinder in number format only.']
    },
    valveConfiguration: {
      type: String,
      maxlength: [150, 'Please, fill the valve type with maximum 150 characters only.']
      // required: true
    },
    fuelSupplySystem: {
      type: String,
      maxlength: [150, 'Please, fill the fuel supply system with maximum 150 characters only.']
      // required: true
    },
    maximumPower: {
      type: String,
      maxlength: [150, 'Please, fill the maximum power  with maximum 150 characters only.']
      // required: true
    },
    maximumTorque: {
      type: String,
      maxlength: [150, 'Please, fill the fuel maximum torque with maximum 150 characters only.']
      // required: true
    },
    lubrication: {
      type: String,
      maxlength: [150, 'Please, fill the lubrication  with maximum 150 characters only.']
      // required: true
    },
    engineOil: {
      type: String,
      maxlength: [150, 'Please, fill the engine oil with maximum 150 characters only.']
      // required: true
    },
    airCleaner: {
      type: String,
      maxlength: [150, 'Please, fill the air cleaner with maximum 150 characters only.']
      // required: true
    },
    boreXStroke: {
      type: String,
      maxlength: [150, 'Please, fill the boreXStroke with maximum 150 characters only.']
      // required: true
    },
    compressionRatio: {
      type: String,
      maxlength: [150, 'Please, fill the compression ratio with maximum 150 characters only.']
      // required: true
    }
    //  model of electric bike
  },
  { _id: false }
);

const WheelTyreSchema = new Schema(
  {
    frontWheelType: {
      type: String
      // validate: [validateFloatNum, 'Please fill a front wheel size with number format only.']
    },
    rearWheelType: {
      type: String
      // validate: [validateFloatNum, 'Please fill a rear wheel size number format only.']
    },
    frontWheelSize: {
      type: String,
      // required: true,
      validate: [validateFloatNum, 'Please fill a wheel size in relevant format only.']
    },
    rearWheelSize: {
      type: String,
      // required: true,
      validate: [validateFloatNum, 'Please fill a wheel size in relevant format only.']
    },
    rearTyre: {
      type: String,
      maxlength: [150, 'Please, fill the rear type with maximum 150 characters only.']
      // required: true
    },
    frontTyre: {
      type: String,
      maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
      // required: true
    },
    frontWheelDrive: {
      type: String,
      maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
      // required: true
    },
    // alloyWheel: {
    //   type: String,
    //   maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.'],
    //   required: true
    // },
    steelRims: {
      type: String,
      maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
      // required: true
    }
  },
  { _id: false }
);

const vehicleImgPathSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    }
  },
  { _id: false }
);

const draftVehicleSchema = new Schema(
  {
    brandId: {
      type: Schema.Types.ObjectId,
      ref: 'BrandVehicle'
    },
    brandName: {
      type: String
    },
    modelId: {
      type: Schema.Types.ObjectId,
      ref: 'BrandModel'
    },
    modelName: {
      type: String
    },
    varientId: {
      type: String
      // type: Schema.Types.ObjectId,
      // ref: 'VarientModel'
    },
    varientName: {
      type: String,
    },
    otherDetail: {
      type: String,
      validate: [validateMaxOneHunFiftyChar, 'Please, fill the sub-varaint with minimum 2 and maximum 150 characters only.']
    },
    vehicleName: {
      type: String
    },
    status: String,
    vehicleType: {
      type: String
    },
    fuelType: {
      type: String,
      enum: ['PETROL', 'DIESEL', 'ELECTRIC'],
      required: true
    },
    // makeYear: {
    //   type: Date
    //   // validate: [validateDate, 'Please fill a valid make year in date format only.'],
    // },
    vehicleImgPath: [vehicleImgPathSchema],
    marketPrice: {
      type: String,
      maxlength: [7, 'Please fill a Market Price less than 1 crore.']
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    basicFactor: BasicFactorSchema,
    optionFeature: OptionFeatureSchema,
    technicalFactor: TechnicalSchema,
    dimensionalFactor: DimensionalSchema,
    engineFactor: EngineSchema,
    wheelTyreFactor: WheelTyreSchema
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

// /* Make first letter to uppercase of back brake system */
// TechnicalSchema.pre('save', function (next) {
//   this.backBrakeSystem = this.backBrakeSystem.trim()[0].toUpperCase() + this.backBrakeSystem.slice(1).toLowerCase();
//   next();
// });

// /* Make first letter to uppercase of front brake system */
// TechnicalSchema.pre('save', function (next) {
//   this.frontBrakeSystem = this.frontBrakeSystem.trim()[0].toUpperCase() + this.frontBrakeSystem.slice(1).toLowerCase();
//   next();
// });

draftVehicleSchema.plugin(mongoosePaginate);
module.exports = {
  draftVehicleSchema
};
