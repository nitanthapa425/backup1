/**
 * Copyright (C) Dui Pangra
 */

/**
 * the Basic Auto Value Detail Schema
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');

const VehicleTypeSchema = new Schema({
  vehicleType: {
    type: String,
    enum: ['BIKE', 'SCOOTER'],
    unique: true
  }
});

const TransmissionTypeSchema = new Schema({
  transmissionType: {
    type: String,
    minlength: 2,
    unique: true,
    maxlength: [150, 'Please, fill the transmission type with maximum 150 characters only.']
  }
});

const FuelTypeSchema = new Schema({
  fuelType: {
    type: String,
    enum: ['PETROL', 'DIESEL', 'ELECTRIC'],
    unique: true
  }
});

const BodyTypeSchema = new Schema({
  bodyType: {
    type: String,
    minlength: 2,
    unique: true,
    maxlength: [150, 'Please, fill the body type with maximum 150 characters only.']
  }
});

const SeatingCapacitySchema = new Schema({
  seatingCapacity: {
    type: Number,
    unique: true,
    min: [1, 'Seating Capacity Must not be less than 1.'],
    max: [5, 'Seating Capacity must be at must 5.']
    // validate: [validateMaxFiveNum, 'Please fill a Seating Capacity with number less than 5.']
  }
});

const FuelCapacitySchema = new Schema({
  fuelCapacity: {
    type: Number,
    unique: true,
    min: [1, 'Please fill fuel Capacity with minumum value of '],
    max: [70, 'Fuel Capacity must be at must 70.']
    // validate: [validateMaxSeventyNum, 'Please fill a Fuel Capacity with number less than 70 litre.']
  }
});

const MileageSchema = new Schema({
  mileage: {
    type: Number,
    unique: true,
    // min: [5, 'Please fill mileage with minumum value of 5'],
    max: [100, 'Please fill mileage with maximum value of 100']
  }
});
const WheelBaseSchema = new Schema({
  wheelBase: {
    type: String,
    unique: true
  }
});
const MarketPriceSchema = new Schema({
  marketPrice: {
    type: Number,
    min: [10000, 'Please, Fill market Price minimum 10000'],
    max: [10000000, 'Please, Fill market Price minimum 10000'],
    unique: true,
  }
});
const OtherDetailSchema = new Schema({
  otherDetail: {
    type: String,
    minlength: [2, 'Please, fill the variant name with maximum 2 characters only.'],
    maxlength: [150, 'Please, fill the variant name with maximum 150 characters only.']
  }
});

module.exports = {
  VehicleTypeSchema,
  TransmissionTypeSchema,
  FuelTypeSchema,
  BodyTypeSchema,
  SeatingCapacitySchema,
  FuelCapacitySchema,
  MileageSchema,
  WheelBaseSchema,
  MarketPriceSchema,
  OtherDetailSchema
};
