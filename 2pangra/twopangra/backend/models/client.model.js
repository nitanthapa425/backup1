/**
 * Copyright (C) Two Pangra
 */

/**
 * the user  model
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { requiredLocationForBook } = require('../utils/helper');

const LocationSchema = new Schema(
  {
    label: {
      type: String,
      enum: ['residential', 'permanent', 'billing']
    },
    country: {
      type: String,
      default: 'Nepal'
    },
    province: {
      type: String,
      required: [requiredLocationForBook, 'Province is required']
    },
    district: {
      type: String,
      required: [requiredLocationForBook, 'District is required']
    },
    municipalityVdc: {
      type: String,
      required: [requiredLocationForBook, 'Municipality/VDC is required']
    },
    wardNo: {
      type: String,
      required: [requiredLocationForBook, 'Ward No. is required']
    },
    combineLocation: {
      type: String
    },
    streetRoadName: {
      // need to be disscuss
      type: String
    },
    nearByLocation: {
      type: String
    },
    status: {
      type: String,
      enum: ['Location']
    },
    geoLocation: {
      type: {
        type: String, // Don't do `{ location: { type: String } }`
        enum: ['Point'] // 'location.type' must be 'Point'
      },
      coordinates: {
        type: [Number]
      }
    }
  },
  { id: false }
);
const ProfileImageSchema = new Schema(
  {
    imageUrl: { type: String },
    publicKey: { type: String }
  },
  { _id: false }
);

const ClientSchema = new Schema(
  {
    firstName: { type: String },
    middleName: { type: String },
    lastName: { type: String },
    fullName: { type: String },
    email: {
      type: String,
      lowercase: true,
      trim: true
    },
    password: { type: String, private: true },
    otpId: {
      type: String
    },
    forgotOTPId: {
      type: String
    },
    profileImagePath: ProfileImageSchema,
    mobile: {
      type: String,
      unique: true
    },
    additionalMobile: {
      type: String
    },
    addedByAdmin: {
      type: Boolean,
      default: false
    },
    gender: { type: String, enum: ['Male', 'Female', 'Other'] },
    dob: { type: Date },
    isActive: { type: Boolean, default: false },
    accessLevel: { type: String, default: 'CLIENT' },
    emailVerified: { type: Boolean, default: false },
    phoneVerified: { type: Boolean, default: false },
    receiveNewsletter: { type: Boolean, default: true },
    forgotPasswordToken: { type: String },
    lastLoginAt: { type: Date },
    verificationToken: { type: String },
    accessToken: { type: String },
    location: [LocationSchema],
    customerDescription: { type: String }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;

        // Keep only necessary details for GET requests
        delete ret.passwordHash;
        delete ret.verificationToken;
        delete ret.forgotPasswordToken;
        delete ret.accessToken;
        return ret;
      }
    }
  }
);

/**
 * Check if number is taken
 * @param {string} number - The user's number
 * @param {ObjectId} [excludeUserId] - The id of the user to be excluded
 * @returns {Promise<boolean>}
 */
ClientSchema.statics.isNumberTaken = async function (mobile, excludeUserId) {
  const client = await this.findOne({ mobile, _id: { $ne: excludeUserId } });
  return !!client;
};
ClientSchema.plugin(mongoosePaginate);
module.exports = {
  ClientSchema
};
