/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
// const uniqueValidator = require('mongoose-beautiful-unique-validation');
const { Schema } = require('mongoose');

const VaraiantNameValueSchema = new Schema({
  variantName: {
    type: String,
    minlength: [2, 'Please, fill the variant name with maximum 2 characters only.'],
    maxlength: [150, 'Please, fill the variant name with maximum 150 characters only.']
  }
});

const VarientSchema = new Schema({
  varientName: {
    type: String,
    // unique: true,
    required: [true, 'A variant must have a name.']
  },
  varientDescription: {
    type: String
  },
  modelId: {
    type: Schema.Types.ObjectId,
    ref: 'BrandModel',
    required: true
  },
  modelName: {
    type: String
  },
  brandName: {
    type: String
  },
  status: {
    type: String
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

// Apply the uniqueValidator plugin to VarientSchema.
// VarientSchema.plugin(uniqueValidator);

const uploadBrandImageSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    }
  },
  { _id: false }
);

const companyImageSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    }
  },
  { _id: false }
);

const BrandVehicleSchema = new Schema(
  {
    brandVehicleName: {
      type: String,
      unique: true,
      required: [true, 'A brand vehicle must have a name']
    },
    brandVehicleDescription: {
      type: String
    },
    uploadBrandImage: uploadBrandImageSchema,

    companyImage: companyImageSchema,
    hasInHomePage: {
      type: Boolean,
      default: false
    },
    status: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

// Apply the uniqueValidator plugin to BrandVehicleSchema.
// BrandVehicleSchema.plugin(uniqueValidator);

const ModelSchema = new Schema({
  modelName: {
    type: String,
    // unique: true,
    required: [true, 'A brand model must have a name.']
  },
  modelDescription: {
    type: String
  },
  brandId: {
    type: Schema.Types.ObjectId,
    ref: 'BrandVehicle',
    required: true
  },
  brandName: {
    type: String
  },
  status: {
    type: String
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

BrandVehicleSchema.plugin(mongoosePaginate);
ModelSchema.plugin(mongoosePaginate);
VarientSchema.plugin(mongoosePaginate);

module.exports = {
  BrandVehicleSchema,
  ModelSchema,
  VarientSchema,
  VaraiantNameValueSchema
};
