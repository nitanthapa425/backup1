/**
 * Copyright (C) Dui Pangra
 */

/**
 * The Dimensional Feature Auto Value Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
// const { validateFloatingNumber } = require('../utils/helper');

const FrontWheelType = new Schema({
  frontWheelType: {
    type: String,
    unique: true,
    minlength: [2, 'Please, fill the front wheel type with minimum 2 characters only.'],
    maxlength: [150, 'Please, fill the front wheel type  with maximum 150 characters only.']
  }
});
const RearWheelType = new Schema({
  rearWheelType: {
    type: String,
    unique: true,
    minlength: [2, 'Please, fill the rear wheel type with minimum 2 characters only.'],
    maxlength: [150, 'Please, fill the rear wheel type with maximum 150 characters only.']
  }
});

const FrontWheelSize = new Schema({
  frontWheelSize: {
    type: Number,
    unique: true
    // min: [1, 'Wheel Size must be at least 1'],
    // max: [150, 'Wheel Size must be at must 150']
    // validate: [validateFloatingNumber, 'Please fill a wheel size in relevant format only.']
  }
});

const RearWheelSize = new Schema({
  rearWheelSize: {
    type: Number,
    unique: true
    // min: [1, 'Wheel Size must be at least 1'],
    // max: [150, 'Wheel Size must be at must 150']
  }
});

const RearTyreSchema = new Schema({
  rearTyre: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the rear type with maximum 150 characters only.']
  }
});

const FrontTyreSchema = new Schema({
  frontTyre: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
  }
});
const FrontWheelDriveSchema = new Schema({
  frontWheelDrive: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
  }
});
const AlloyWheelSchema = new Schema({
  alloyWheel: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
  }
});
const SteelRimsSchema = new Schema({
  steelRims: {
    type: String,
    unique: true,
    maxlength: [150, 'Please, fill the wheel type with maximum 150 characters only.']
  }
});

module.exports = {
  FrontWheelType,
  RearWheelType,
  FrontWheelSize,
  RearWheelSize,
  RearTyreSchema,
  FrontTyreSchema,
  FrontWheelDriveSchema,
  AlloyWheelSchema,
  SteelRimsSchema
};
