const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  mode: 'jit',
  purge: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
    './src/container/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: '1rem',

      screens: {
        xl: '1250px',
      },
    },
    extend: {
      fontFamily: {
        sans: ["'Nunito'", ...defaultTheme.fontFamily.serif],
      },
      colors: {
        primary: {
          light: '#A7EB4A',
          DEFAULT: '#8DC63F',
          dark: '#75A535',
        },

        secondary: {
          light: '#ee9433',
          DEFAULT: '#d49653',
          dark: '#ca7314',
        },

        error: {
          DEFAULT: '#ec1c24',
          dark: '#a01319',
        },

        textColor: '#383838',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms')],
};
