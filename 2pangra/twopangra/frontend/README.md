This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

## To Check for Lint

npm run lint

## To Fix lint error automatically as much as possible

npm run lint-fix

## To run frontend

npm run dev

## To upload file

1. Create a folder in root path (uploads/twopangraimg)

## .env of frontend

1. For local add
   NEXT_PUBLIC_API_URL=http://localhost:3100/api/v1

2. For heroku server add
   NEXT_PUBLIC_API_URL=https://duipangra-bd.herokuapp.com/api/v1
