import * as yup from 'yup';

export const brandvalidationSchema = yup.object({
  brandVehicleName: yup.string().required('Brand Name is required.'),
  companyImage: yup.object().required('Company Image is required.'),
  uploadBrandImage: yup.object().required('Brand Image is required.'),
  brandVehicleDescription: yup
    .string()
    .max(1000, 'Brand Description must be at must 1000 characters'),
});
