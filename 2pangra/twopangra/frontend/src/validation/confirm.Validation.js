import * as yup from 'yup';
import { VerifyConfirmPassword, PasswordValidation } from './yupValidations';

export const VerifyEmailSchema = yup.object({
  passwordHash: PasswordValidation('New Password'),
  confirmPassword: VerifyConfirmPassword('Confirm Password'),
});
