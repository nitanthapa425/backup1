import * as yup from 'yup';

export const modelvalidationSchema = yup.object({
  modelName: yup.string().required('Model Name is required.'),
  brandId: yup.string().required('Brand Name is required.'),
  modelDescription: yup
    .string()
    .max(1000, 'Model Description must be at must 1000 characters.'),
});
