import * as yup from 'yup';
import {
  PasswordValidations,
  ConfirmPasswords,
  PhoneValidations,
} from './yupValidations';

export const customerValidationSchema = yup.object({
  firstName: yup
    .string()
    .required('First Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'First Name must be at must 30 characters long'),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Middle Name must be at must 30 characters long'),
  lastName: yup
    .string()
    .required('Last Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Last Name must be at must 30 characters long'),
  password: PasswordValidations(),
  confirmPassword: ConfirmPasswords(),
  mobile: PhoneValidations(),
  // email: yup.string(),
  // dob: yup.string().required('Date of Birth is required.'),
  // gender: yup.string().required('Gender is required.'),
});

export const customerLoginValidationSchema = yup.object({
  mobile: PhoneValidations(),
  password: yup.string().required('Password is required.'),
});

export const customerUpdateValidationSchema = yup.object({
  firstName: yup
    .string()
    .required('First Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'First Name must be at must 30 characters long'),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Middle Name must be at must 30 characters long'),
  lastName: yup
    .string()
    .required('Last Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Last Name must be at must 30 characters long'),
  mobile: PhoneValidations(),
  // email: yup.string(),
  dob: yup.string().required('Date of Birth is required.'),
  gender: yup.string().required('Gender is required.'),
  profileImagePath: yup.object(),
});

export const customerMyUpdateValidationSchema = yup.object({
  firstName: yup
    .string()
    .required('First Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'First Name must be at must 30 characters long'),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Middle Name must be at must 30 characters long'),
  lastName: yup
    .string()
    .required('Last Name is required.')
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      'Only alphabets are allowed for this field.'
    )
    .max(30, 'Last Name must be at must 30 characters long'),
  mobile: PhoneValidations(),
  // email: yup.string(),
  dob: yup.string().required('Date of Birth is required.'),
  gender: yup.string().required('Gender is required.'),
  profileImagePath: yup.object(),
  location: yup.object({
    combineLocation: yup.string().required('Location is required.'),
    nearByLocation: yup.string().required('Near by location is required.'),
  }),
});

export const customerForgotValidationSchema = yup.object({
  mobile: PhoneValidations(),
});
export const customerOtpValidationSchema = yup.object({
  otp: yup.string().required('OTP Code is required.'),
});
export const PhoneVerificationValidationSchema = yup.object({
  token: yup.string().required('OTP Code is required.'),
});
