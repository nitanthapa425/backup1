import * as yup from 'yup';

export const subModelValidationSchema = yup.object({
  varientName: yup.string().required('Variant Name is required.'),
  modelId: yup.string().required('Model Name is required.'),
  varientDescription: yup
    .string()
    .max(1000, 'Variant Description must be at must 1000 characters.'),
});
