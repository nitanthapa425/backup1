import * as yup from 'yup';
import { ConfirmPassword, PasswordValidation } from './yupValidations';

export const PasswordUpdateSchema = yup.object({
  currentPassword: yup.string().required('Current Password is required'),
  newPassword: PasswordValidation('New Password'),
  confirmPassword: ConfirmPassword('Confirm Password'),
});
