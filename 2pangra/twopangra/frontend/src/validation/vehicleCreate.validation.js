import * as yup from 'yup';
import { NumberWithLimit } from './yupValidations';

export const vechileCreateValidation = [
  yup.object({
    brandId: yup.string().required('Brand is required.'),
    modelId: yup.string().required('Model is required.'),
    varientId: yup.string(),
    marketPrice: yup
      .number()
      .typeError('Market price must be number.')
      .required('Market Price is required.')
      .min(10000, 'Market price must be at least 10000')
      .max(10000000, 'Market price must be at must 10000000'),
    vehicleType: yup.string().required('Vehicle Type is required.'),
    fuelType: yup.string().required('Fuel Type is required.'), // need to validate with enum
    vehicleImgPath: yup.array().min(1, `At least a image is required.`),
    otherDetail: yup
      .string()
      .min(2, 'Sub-Variant must be at least 2 characters')
      .max(150, 'Sub-Variant must be at must 150 characters.'),
  }),
  yup.object({
    basicFactor: yup.object().shape({
      transmissionType: yup
        .string()
        .required('Transmission Type is required.')
        .min(2, 'Transmission Type must be at least 2.')
        .max(150, 'Transmission Type must be at must 150.'),
      bodyType: yup
        .string()
        .required('Body Type is required.')
        .min(2, 'Body Type must be at least 2.')
        .max(150, 'Body Type must be at must 150.'),
      mileage: yup
        .number()
        .min(0, 'Mileage must not be at least 5.')
        .max(100, 'Mileage must be at must 100.')
        .typeError('Mileage must be number.')
        .required('Mileage is required.'),
      seatingCapacity: yup
        .number()
        .min(1, 'Seating Capacity must be at least 1.')
        .max(5, 'Seating Capacity must be at must 5.')
        .typeError('Seating Capacity must be number.')
        .required('Seating Capacity is required.'),
      fuelCapacity: yup
        .number()
        .min(1, 'Fuel Capacity must be at least 1.')
        .max(70, 'Fuel Capacity must be at must 70.')
        .typeError('Fuel capacity must be number.')
        .required('Fuel Capacity is required.'),
    }),
  }),

  yup.object({
    optionFeature: yup.object().shape({
      instrumentation: yup
        .array()
        .min(1, `Instrumentation is required.`)
        .of(
          yup
            .string()
            .max(150, 'Instrumentation must be at must 150 characters.')
            .min(2, 'Instrumentation must be at least 2 characters.')
        ),
      safety: yup
        .array()
        .min(1, `Safety is required.`)
        .of(
          yup
            .string()
            .max(150, 'Safety must be at must 150 characters.')
            .min(2, 'Safety must be at least 2 characters.')
        ),
      ignitionType: yup
        .string()
        .min(2, 'Ignition Type must be at least 2 characters.')
        .max(150, 'Ignition Type must be at must 150 characters.')
        .required('IgnitionType is required.'),
      features: yup
        .array()
        .min(1, `Features are required.`)
        .of(
          yup
            .string()
            .max(150, 'Features must be at must 150 characters.')
            .min(2, 'Features must be at least 2 characters.')
        ),
    }),
  }),

  yup.object({
    technicalFactor: yup.object().shape({
      technicalVehicleType: yup.string(),
      frontBrakeSystem: yup
        .string()
        .min(2, 'Front Brake System must be at least 2 characters.')
        .max(150, 'Front Brake System must be at must 150 characters.')
        .required('Front Brake System is required.'),
      backBrakeSystem: yup
        .string()
        .min(2, 'Back Brake System must be at least 2 characters.')
        .max(150, 'Back Brake System must be at must 150 characters.')
        .required('Back Brake System is required.'),
      frontSuspension: yup
        .string()
        .min(2, 'Front Suspension must be at least 2 characters.')
        .max(150, 'Front Suspension must be at must 150 characters.')
        .required('Front Suspension is required.'),
      rearSuspension: yup
        .string()
        .min(2, 'Rear Suspension must be at least 2 characters.')
        .max(150, 'Rear Suspension must be at must 150 characters.')
        .required('Rear Suspension is required.'),

      noOfGears: yup.number().when('technicalVehicleType', {
        is: (technicalVehicleType) => {
          return technicalVehicleType === 'BIKE';
        },
        then: yup
          .number()
          .min(1, `Number of Gears must be at least 1.`)
          .max(10, `Number of Gears must be at must 10.`)
          .typeError(`Only numbers are allowed for this field.`)
          .required(`Number of Gears is required.`),
        otherwise: yup.number().nullable(),
      }),

      driveType: yup
        .string()
        .min(2, 'Drive Type must be at least 2 characters.')
        .max(150, 'Drive Type must be at must 150 characters.')
        .required('Drive Type is required.'),
      clutchType: yup.string().when('technicalVehicleType', {
        is: 'BIKE',
        then: yup
          .string()
          .min(2, 'Clutch Type must be at least 2 characters.')
          .max(150, 'Clutch Type must be at must 150 characters.')
          .required(`Clutch Type is required.`),
        otherwise: yup.string(),
      }),
      gearPattern: yup.string().when('technicalVehicleType', {
        is: 'BIKE',
        then: yup
          .string()
          .min(2, 'Gear Pattern must be at least 2 characters.')
          .max(150, 'Gear Pattern must be at must 150 characters.')
          .required(`Gear Pattern is required.`),
        otherwise: yup.string(),
      }),
      headlight: yup
        .string()
        .min(2, 'HeadLight must be at least 2 characters.')
        .max(150, 'HeadLight must be at must 150 characters.')
        .required('HeadLight/HeadLamp is required.'),
      taillight: yup
        .string()
        .min(2, 'TailLight must be at least 2 characters.')
        .max(150, 'TailLight must be at must 150 characters.')
        .required('TailLight/TailLamp is required.'),
      starter: yup
        .array()
        .min(1, `Starter is required.`)
        .of(
          yup
            .string()
            .max(150, 'Starter must be at must 150 characters.')
            .min(2, 'Starter must be at least 2 characters.')
        ),
      battery: yup
        .string()
        .min(2, 'Battery must be at least 2 characters.')
        .max(150, 'Battery must be at must 150 characters.')
        .required('Battery is required.'),
    }),

    dimensionalFactor: yup.object().shape({
      dimensionalVehicleType: yup.string(),
      wheelBase: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`wheel Base is required.`),
      overallWidth: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Overall Width is required.`),
      overallLength: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Overall Length is required.`),
      overallHeight: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Overall Height is required.`),
      groundClearance: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Ground Clearance is required.`),
      bootSpace: yup.number().when('dimensionalVehicleType', {
        is: (dimensionalVehicleType) => {
          return dimensionalVehicleType === 'SCOOTER';
        },
        then: yup
          .number()
          .typeError(`Only numbers are allowed for this field.`)
          .required(`Bootspace is required.`),
        otherwise: yup.number(),
      }),
      kerbWeight: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Kerb Weight is required.`),
      doddleHeight: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Seat Height is required.`),
    }),

    wheelTyreFactor: yup.object().shape({
      frontWheelSize: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Front Wheel Size is required.`),
      rearWheelSize: yup
        .number()
        .typeError(`Only numbers are allowed for this field.`)
        .required(`Rear Wheel Size is required.`),

      rearWheelType: yup
        .string()
        .min(2, 'Rear Wheel Type must be at least 2 characters.')
        .max(150, 'Rear Wheel Type must be at must 150 characters.')
        .required('Rear Wheel Type is required.'),
      frontWheelType: yup
        .string()
        .min(2, 'Front Wheel Type must be at least 2 characters.')
        .max(150, 'Front Wheel Type must be at must 150 characters.')
        .required('Front Wheel Type is required.'),
      steelRims: yup
        .string()
        .min(2, 'Steel Rims must be at least 2 characters.')
        .max(150, 'Steel Rims must be at must 150 characters.')
        .required('Steel Rims is required.'),
    }),

    engineFactor: yup.object().shape({
      displacement: NumberWithLimit('Displacement', 1, 1500),
      engineType: yup
        .array()
        .min(1, `Engine Type is required.`)
        .of(
          yup
            .string()
            .min(2, 'Engine Type must be at least 2 characters.')
            .max(150, 'Engine Type must be at must 150 characters.')
        ),
      noOfCylinder: NumberWithLimit('No of Cylinders', 0, 5),
      valvesPerCylinder: NumberWithLimit('Valves Per Cylinder', 0, 5),
      valveConfiguration: yup
        .string()
        .min(
          2,
          'Valve Configuration/Valve Train must be at least 2 characters.'
        )
        .max(
          50,
          'Valve Configuration/Valve Train must be at must 150 characters.'
        )
        .required('Valve Configuration/Valve Train is required characters.'),
      fuelSupplySystem: yup
        .string()
        .min(2, 'Fuel Supply System must be at least 2 characters.')
        .max(150, 'Fuel Supply System must be at must 150 characters.')
        .required('Fuel Supply System is required.'),
      engineOil: yup
        .string()
        .required('Engine Oil is required.')
        .min(2, 'Engine Oil must be at least 2 characters.')
        .max(150, 'Engine Oil must be at must 150 characters.'),
      maximumPower: yup
        .string()
        .required('Maximum Power is required.')
        .min(2, 'Maximum Power must be at least 2 characters.')
        .max(150, 'Maximum Power must be at must 150 characters.'),
      maximumTorque: yup
        .string()
        .required('Maximum Torque is required.')
        .min(2, 'Maximum Torque must be at least 2 characters.')
        .max(150, 'Maximum Torque must be at must 150 characters.'),
      lubrication: yup
        .string()
        .min(2, 'Lubrication must be at least 2 characters.')
        .max(150, 'Lubrication must be at must 150 characters.')
        .required('Lubrication is required.'),
      airCleaner: yup
        .string()
        .min(2, 'Air Cleaner must be at least 2 characters.')
        .max(150, 'Air Cleaner must be at must 150 characters.')
        .required('Air Cleaner is required.'),
      boreXStroke: yup
        .string()
        .min(2, 'Bore X Stroke must be at least 2 characters.')
        .max(150, 'Bore X Stroke must be at must 150 characters.')
        .required('Bore X Stroke is required.'),
      compressionRatio: yup
        .string()
        .min(2, 'Compression Ratio must be at least 2 characters.')
        .max(150, 'Compression Ratio must be at must 150 characters.')
        .required('Compression Ratio is required.'),
      motorPower: yup.string().when('engineFactorFuelType', {
        is: (engineFactorFuelType) => {
          return engineFactorFuelType === 'ELECTRIC';
        },
        then: yup
          .string()
          .min(2, 'Motor Power must be at least 2 characters.')
          .max(150, 'Motor Power must be at must 150 characters.')
          .required(`Motor Power is required.`),
        otherwise: yup.string().nullable(),
      }),
    }),
  }),
];
