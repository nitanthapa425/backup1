import * as yup from 'yup';

export const LocationValidationSchema = yup.object({
  // location: yup.object({
  combineLocation: yup.string().required('Location is required.'),
  nearByLocation: yup.string().required('Near by location is required.'),
  // }),
});
