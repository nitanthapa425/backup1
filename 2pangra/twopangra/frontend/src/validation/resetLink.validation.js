import * as yup from 'yup';

export const emailLinkSchema = yup.object({
  email: yup.string().email('Email is invalid.').required('Email is required.'),
});

export const emailSchema = yup.object({
  email: yup.string().email('Email is invalid.'),
});
export const messageSchema = yup.string({
  message: yup.string('Message is invalid.'),
  // vehicleId: yup.string('Message is invalid.'),
  // email: yup.string('Message is invalid.'),
});
export const replyMessageSchema = yup.string({
  replyMessage: yup.string('Message is invalid.'),
  // vehicleId: yup.string('Message is invalid.'),
  // email: yup.string('Message is invalid.'),
});
