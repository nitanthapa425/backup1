import { beforeAfterDate } from 'utils/beforeAfterdate';
import { nNumberArray } from 'utils/nNumberOfArray';

export const role = {
  admin: 'ADMIN',
  super_admin: 'SUPER_ADMIN',
  user: 'USER',
};

export const color = [
  'Beige',
  'Black',
  'Blue',
  'Brown',
  'Gold',
  'Green',
  'Grey',
  'Maroon',
  'Orange',
  'Purple',
  'Red',
  'Silver',
  'White',
  'Yellow',
  'Other',
];

export const usedFor = [
  'Not Available',
  '1 Month',
  '3 Months',
  '6 Months',
  '1 Year',
  '2 Years',
  '3 Years',
  '4 Years',
  '5 Years',
  '6 Years',
  '7 Years',
  '8 Years',
  '9 Years',
  '10 Years',
  'More than 10',
];

export const postExpiryDate = [
  {
    label: '1 Month',
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 2
      }-${new Date().getDate()}`
    ),
  },
  {
    label: '3 Months',
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 4
      }-${new Date().getDate()}`
    ),
  },
  {
    label: '6 Months',
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 7
      }-${new Date().getDate()}`
    ),
  },
  {
    label: '9 Months',
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 10
      }-${new Date().getDate()}`
    ),
  },
  {
    label: '1 Year',
    value: new Date(
      `${new Date().getFullYear() + 1}-${
        new Date().getMonth() + 1
      }-${new Date().getDate()}`
    ),
  },
];

export const featureDate = [
  {
    label: '3 Days',
    value: beforeAfterDate(new Date(), 3, 0, 0),
  },
  {
    label: '7 Days',
    value: beforeAfterDate(new Date(), 7, 0, 0),
  },
  {
    label: '15 Days',
    value: beforeAfterDate(new Date(), 15, 0, 0),
  },
];

export const condition = [
  'Brand New',
  'Like New',
  'Excellent',
  'Good/Fair',
  'Not Working',
  'N/A',
];

// export const wardNumber = [
//   '1',
//   '2',
//   '3',
//   '4',
//   '5',
//   '6',
//   '7',
//   '8',
//   '9',
//   '10',
//   '11',
//   '12',
//   '12',
//   '13',
//   '14',
//   '15',
//   '16',
//   '17',
//   '18',
//   '19',
//   '20',
//   '21',
//   '22',
//   '23',
//   '24',
//   '25',
// ];

export const ownershipCount = nNumberArray(5);
export const budget = ['Below 1 Lakh ', '1 Lakh - 2 Lakh', 'Above 2 Lakh'];
export const verified = [
  {
    label: 'Verified',
    value: true,
  },
  {
    label: 'Unverified',
    value: false,
  },
];
export const approved = [
  {
    label: 'Approved',
    value: true,
  },
  {
    label: 'Unapproved',
    value: false,
  },
];
export const sold = [
  {
    label: 'Sold',
    value: true,
  },
  {
    label: 'Unsold',
    value: false,
  },
];
