import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
// import UserHeader from 'components/Header/UserHeader';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
// import SearchWithSelect from 'components/Search/SearchSingeData';
import Card from 'components/Card';
import Button from 'components/Button';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import ButtonTabs from 'components/Button/ButtonTabs';
import LoadingCard from 'components/LoadingCard';
// import RangeSlider from 'components/RangeSlider/RangeSlider';
import Footer from 'components/Footer';
import {
  useReadBrandNoAuthQuery,
  useReadFeaturedBrandsNoAuthQuery,
} from 'services/api/brand';
import { useReadAllDistrictQuery } from 'services/api/location';
import { useReadSellerQuery } from 'services/api/seller';
// import { arrayToString } from 'utils/arrtoString';
import { useSelector } from 'react-redux';
import ModalVideo from 'react-modal-videojs';
import 'video.js/dist/video-js.css';
import ReactModal from 'components/ReactModal/ReactModal';
import CustomerLoginPopUp from 'components/LoginPopUp';
// import Select from 'components/Select';
// import { budget } from 'constant/constant';

const responsive = {
  0: {
    items: 1,
  },

  768: { items: 2 },
  1024: { items: 3 },
};

const responsive2 = {
  0: { items: 1 },
  768: { items: 2 },
  1024: { items: 4 },
};

const Homepage = () => {
  const router = useRouter();
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const featuredVehicleQuery =
    '?search="hasInFeature":true,"isApproved":true&noRegex=hasInFeature,isApproved&limit=10&sortBy=createdAt&sortOrder=-1';
  // const recentVehicleQuery = '?search="isApproved":true&noRegex=isApproved&sortBy="createdAt"&sortOrder=-1&limit=10';
  const [recentVehicleQuery, setRecentVehicleQuery] = useState(
    '?search="isApproved":true,"vehicleType":"BIKE"&noRegex=isApproved,vehicleType&sortBy=createdAt&sortOrder=-1&limit=10&type=vehicleSell'
  );
  const [budgetRange, setBudgetRange] = useState(
    '?search="isApproved":true,"expectedPrice":"0-100000"&noRegex=expectedPrice,isApproved&sortBy=createdAt&sortOrder=-1&limit=10&type=vehicleSell'
  );
  const [featItems, setFeatItems] = useState([]);
  const [budgetItems, setBudgetItems] = useState([]);
  const [recentItems, setRecentItems] = useState([]);

  // The Bike and Scooter filter in Recently added
  const [recentVehTypeQuery, setRecentVehTypeQuery] = useState('BIKE');
  // Budget filter in Browse by budget which will be sent in query in view all
  const [budgetQuery, setBudgetQuery] = useState('0-100000');
  const [showVideos, setShowVideos] = useState(false);

  // For Find filter
  // const [selectedBrands, setSelectedBrands] = useState({
  //   id: 'brandName',
  //   value: '',
  // });

  // const [selectedDistricts, setSelectedDistricts] = useState({
  //   id: 'district',
  //   value: '',
  // });

  const homeFilterBudgetOptions = [
    '0-100000',
    '100000-300000',
    '300000-10000000',
  ];
  // const [selectedBudgetRange, setSelectedBudgetRange] = useState([0, 10000000]);
  // const [selectedBudget, setSelectedBudget] = useState({
  //   id: 'expectedPrice',
  //   value: '0-10000000',
  // });
  const [selectedBrand, setSelectedBrand] = useState('');
  const [selectedBudgetRange, setSelectedBudgetRange] = useState('');
  const [selectedMakeYear, setSelectedMakeYear] = useState(null);
  const [selectedDistrict, setSelectedDistrict] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  const { data: brandData, isLoading: isLoadingBrandData } =
    useReadBrandNoAuthQuery();
  const { data: districtData, isLoading: isLoadingDistrictData } =
    useReadAllDistrictQuery();

  const { data: featuredVehicleData, isFetching: isLoadingFeaturedVehicle } =
    useReadSellerQuery(featuredVehicleQuery);

  const { data: byBudgetData, isFetching: isLoadingByBudget } =
    useReadSellerQuery(budgetRange);

  const { data: recentVehicleData, isFetching: isLoadingRecentVehicle } =
    useReadSellerQuery(recentVehicleQuery);

  const { data: featBrandsData, isFetching: isLoadingFeatBrands } =
    useReadFeaturedBrandsNoAuthQuery();

  const handleBudgetRange = (range) => {
    setBudgetQuery(range);
    setBudgetRange(
      `?search="isApproved":true,"expectedPrice":"${range}"&noRegex=expectedPrice,isApproved&sortBy=createdAt&sortOrder=-1&limit=10&type=vehicleSell`
    );
  };

  const handleRecentVehicleType = (type) => {
    setRecentVehTypeQuery(type);
    setRecentVehicleQuery(
      `?search="isApproved":true,"vehicleType":"${type}"&noRegex=isApproved,vehicleType&sortBy=createdAt&sortOrder=-1&limit=10&type=vehicleSell`
    );
  };

  const handleSellBike = (route, token) => {
    if (token) {
      router.push(route);
    } else {
      // router.push('/customer/login');
      setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleFindBike = () => {
    // router.push(
    //   `/vehicle-listing?find=true&brand=${selectedBrands?.value
    //   }&budget=${selectedBudgetRange}&makeYear=${selectedMakeYear ?
    //     selectedMakeYear?.getFullYear().toString() : ""
    //   }&district=${selectedDistricts?.value}&`
    // )
    router.push(
      `/vehicle-listing?find=true&brand=${selectedBrand}&budget=${selectedBudgetRange}&makeYear=${
        selectedMakeYear ? selectedMakeYear?.getFullYear().toString() : ''
      }&district=${selectedDistrict}&`
    );
  };

  useEffect(() => {
    if (featuredVehicleData) {
      setFeatItems(
        featuredVehicleData?.docs?.map((item, i) => {
          // const locValue = item?.location?.combineLocation?.substring(item?.location?.combineLocation?.lastIndexOf(' ')+1);
          // console.log("item************", locValue)
          return (
            <Card
              imgPath={item?.bikeImagePath[0]?.imageUrl}
              name={item?.vehicleName}
              subName={item?.expectedPrice}
              linkPath={`/vehicle-listing/${item.id}`}
              index={i}
              key={i}
              id={item.id}
              isSold={item.isSold}
              isVerified={item.isVerified}
              location={item.location.combineLocation}
            />
          );
        })
      );
    }
  }, [featuredVehicleData]);

  useEffect(() => {
    if (recentVehicleData) {
      setRecentItems(
        recentVehicleData?.docs.map((item, i) => {
          return (
            <Card
              imgPath={item?.bikeImagePath[0]?.imageUrl}
              name={item?.vehicleName}
              subName={item?.expectedPrice}
              linkPath={`/vehicle-listing/${item.id}`}
              index={i}
              key={i}
              id={item.id}
              isSold={item.isSold}
              isVerified={item.isVerified}
            />
          );
        })
      );
    }
  }, [recentVehicleData]);

  useEffect(() => {
    if (byBudgetData) {
      setBudgetItems(
        byBudgetData?.docs.map((item, i) => {
          return (
            <Card
              imgPath={item?.bikeImagePath[0]?.imageUrl}
              name={item?.vehicleName}
              subName={item?.expectedPrice}
              linkPath={`/vehicle-listing/${item.id}`}
              index={i}
              key={i}
              id={item.id}
              isSold={item.isSold}
              isVerified={item.isVerified}
            />
          );
        })
      );
    }
  }, [byBudgetData]);

  return (
    <div>
      {/* <UserHeader /> */}

      <main id="main">
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}
        <section className="c-banner-section">
          <div className="banner-image-holder h-[250px] md:h-[380px]">
            <img
              src="images/test-banner.jpg"
              alt="banner01"
              className="h-full object-cover w-full"
            />
          </div>
        </section>
        <section className="c-banner-form-section">
          <div className="container">
            <div className="form-holder bg-white shadow-lg z-10 relative -mt-7 py-4 px-2 md:px-4  rounded-lg">
              <form action="#" className="flex flex-wrap md:space-x-3">
                <div className="form-row w-1/2 px-1 mb-1 md:w-auto md:flex-1 md:mb-0">
                  <SelectWithoutCreate
                    label="Brand"
                    placeholder="Select Brand"
                    value={selectedBrand}
                    onChange={(selectedOption) => {
                      setSelectedBrand(selectedOption.value);
                    }}
                    loading={isLoadingBrandData}
                    options={brandData?.map((b) => ({
                      value: b.brandName,
                      label: b.brandName,
                    }))}
                  />
                  {/* <div className="form-group three-col-sm w-full">
                    <label>Brand</label>
                    <SearchWithSelect
                      // loading={fetchingBrands}
                      defaultValue={[{ id: 'brand', value: 'Tvs' }]}
                      placeholder="Select Brand"
                      // value={selectedBrands || null}
                      // isMulti={false}
                      onChange={(selectedValue) => {
                        // setSelectedBrands({
                        //   id: 'brand',
                        //   value: selectedValue.value,
                        // });

                        setSelectedBrands({
                          id: 'brand',
                          value: arrayToString(selectedValue),
                        });
                      }}
                      options={brandData?.map((brand) => ({
                        value: brand.brandName,
                        label: brand.brandName,
                      }))}
                    />
                  </div> */}
                </div>
                <div className="form-row w-1/2 px-1 mb-1  md:w-auto md:flex-1 md:mb-0">
                  <SelectWithoutCreate
                    label="Budget"
                    placeholder="Select Budget"
                    value={selectedBudgetRange}
                    onChange={(selectedOption) => {
                      setSelectedBudgetRange(selectedOption.value);
                    }}
                    options={homeFilterBudgetOptions.map((b) => ({
                      value: b,
                      label: 'NRs. ' + b,
                    }))}
                  />
                  {/* <RangeSlider
                    min={0}
                    max={1000000}
                    value={selectedBudgetRange}
                    onChange={(value) => {
                      const minMaxStr = value.join('-');
                      setSelectedBudgetRange(value);
                      setSelectedBudget({
                        id: 'expectedPrice',
                        value: minMaxStr,
                      });
                    }}
                    postFix=" NRs"
                    sliderName="NRs. "
                  /> */}
                </div>
                <div className="form-row w-1/2 px-1 mb-1  md:w-auto md:flex-1 md:mb-0">
                  <label>Make Year</label>
                  <DatePicker
                    selected={selectedMakeYear || null}
                    onChange={(date) => {
                      setSelectedMakeYear(date);
                    }}
                    showYearPicker
                    dateFormat="yyyy"
                    yearItemNumber={9}
                    placeholderText="Make Year"
                    maxDate={new Date()}
                  />
                </div>
                <div className="form-row w-1/2 px-1 mb-1  md:w-auto md:flex-1 md:mb-0">
                  <SelectWithoutCreate
                    label="District"
                    placeholder="Select District"
                    value={selectedDistrict}
                    onChange={(selectedOption) => {
                      setSelectedDistrict(selectedOption.value);
                    }}
                    loading={isLoadingDistrictData}
                    options={districtData?.map((option) => ({
                      value: option?.district,
                      label: option?.district,
                    }))}
                  />
                  {/* <SearchWithSelect
                    onChange={(selectedValue) => {
                      setSelectedDistricts({
                        id: 'district',
                        value: arrayToString(selectedValue),
                      });
                    }}
                    loading={isLoadingDistrictData}
                    options={districtData?.map((option) => ({
                      // value: firstLetterCapital(option.district),
                      value: option.district,
                      label: option.district,
                    }))}
                    placeholder="Select District"
                  /> */}
                </div>
                <div className="flex flex-col justify-center md:mt-[24px] ml-2 md:ml-0">
                  <Button
                    type="button"
                    className="btn btn-sm "
                    variant="primary"
                    disabled={
                      !selectedBrand &&
                      !selectedBudgetRange &&
                      !selectedMakeYear &&
                      !selectedDistrict
                    }
                    onClick={handleFindBike}
                  >
                    Find Bike
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </section>
        <section className="py-[50px] lg:pt-[55px] lg:pb-[75px]  featured-section">
          <div className="container">
            <div className="c-heading flex justify-between items-end ">
              <h1 className="h3">
                Featured <span>Items</span>
              </h1>
              <Link href="/vehicle-listing?browseBy=featured">
                <a className="px-3 py-1 text-sm bg-textColor text-white rounded-lg hover:bg-primary-dark hover:text-white">
                  View All
                  <span className="icon-arrow-right2 rotate-180 inline-block align-middle mt-[-3px] ml-[4px]"></span>
                </a>
              </Link>
            </div>
            <div className="flex flex-wrap">
              <div className="featured-carousel">
                {isLoadingFeaturedVehicle ? (
                  <div className="row">
                    <LoadingCard cardColsSize="three" />
                  </div>
                ) : !featItems.length ? (
                  <p className="text-sm text-center">Data Unavailable</p>
                ) : (
                  <AliceCarousel
                    mouseTracking
                    items={featItems}
                    responsive={responsive}
                    controlsStrategy="alternate"
                  />
                )}
              </div>
              <div className="post-ad justify-center w-full md:w-[100px] flex items-center">
                <a
                  onClick={() => {
                    handleSellBike('/sell', token);
                  }}
                  className="  text-textColor text-center w-full hover:text-primary hover:cursor-pointer"
                >
                  <div className=" hidden md:flex w-[45px] h-[45px] mx-auto  text-[24px] bg-gradient-to-r from-[#8DC63FD9] to-[#75A535DE]  items-center justify-center rounded-full text-white hover:text-white">
                    {' '}
                    +
                  </div>
                  <span className="font-semibold text-sm hover:text-primary hidden md:block">
                    Post Your Ads
                  </span>
                  <span className="mt-3 md:mt-0 inline-block md:hidden text-sm md:hover:text-primary py-1.5 px-5 bg-gradient-to-r from-primary  to-primary-dark text-white hover:text-white rounded-md rounded-md hover:from-primary-dark  hover:to-primary ">
                    Post Your Ads
                  </span>
                </a>
              </div>
            </div>
          </div>
        </section>
        <section
          className="py-10 relative before:absolute  before:bg-gradient-to-r before:from-[#d49653] before:to-[#8DC63FD9] before:inset-0"
          style={{ backgroundImage: "url('images/bike-banner.jpg')" }}
        >
          <div className="container z-20 relative">
            <div className="hidden opacity-60 lg:block bike-pattern absolute top-1/2 translate-y-[-55%] left-[-80px] z-[-1] w-[300px]">
              <img src="images/bike-pattern02.png" alt="bike pattern" />
            </div>
            <div className="text-center max-w-[850px] mx-auto">
              <p className="text-white">
                DuiPangra. Nepali two-wheeler e-commerce portal. Buy and sell
                2nd hand motorbikes + branded bike gear & accessories. Time
                Efficient Hassle Free Easy to buy/sell and exchange User
                Friendly Interface Well trained technician for any problems or
                issues Trustworthy
              </p>
              <div className="btn-holder mt-2">
                <Link href="/vehicle-listing">
                  <a className="btn bg-white text-textColor hover:text-textColor hover:bg-gray-100">
                    Find your dream bike
                  </a>
                </Link>

                <Link href="">
                  <a
                    onClick={() => {
                      handleSellBike('/sell', token);
                    }}
                    className="btn bg-transparent border !border-white hover:bg-white hover:text-textColor"
                  >
                    sell Bike
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </section>
        <section className="py-[55px]">
          <div className="container">
            <div className="c-heading flex justify-between items-end ">
              <h1 className="h3">
                Recently <span>Added</span>
              </h1>
              <Link
                href={`/vehicle-listing?browseBy=recentlyAdded&vehicleType=${recentVehTypeQuery}&`}
              >
                <a className="px-3 py-1 text-sm bg-textColor text-white rounded-lg hover:bg-primary-dark hover:text-white">
                  View All
                  <span className="icon-arrow-right2 rotate-180 inline-block align-middle mt-[-3px] ml-[4px]"></span>
                </a>
              </Link>
            </div>
            <div className="mt-[-8px] mb-4">
              <ButtonTabs
                loading={isLoadingRecentVehicle}
                btnArray={[
                  {
                    name: 'Bike',
                    queryValue: 'BIKE',
                  },
                  {
                    name: 'Scooter',
                    queryValue: 'SCOOTER',
                  },
                ]}
                handleClick={handleRecentVehicleType}
              />
            </div>
            <div className="carousel-holder">
              {isLoadingRecentVehicle ? (
                <div className="row">
                  <LoadingCard />
                </div>
              ) : !recentItems.length ? (
                <p className="text-sm text-center">Data Unavailable</p>
              ) : (
                <AliceCarousel
                  mouseTracking
                  items={recentItems}
                  responsive={responsive2}
                  controlsStrategy="alternate"
                />
              )}
            </div>
          </div>
        </section>
        <section className="py-[35px] bg-gray-200">
          <div className="container">
            <div className="c-heading flex justify-between items-end  ">
              <h1 className="h3">
                Browse By <span>Brand</span>
              </h1>
              <Link href="/brand-listing">
                <a className="px-3 py-1 text-sm bg-textColor text-white rounded-lg hover:bg-primary-dark hover:text-white">
                  View All
                  <span className="icon-arrow-right2 rotate-180 inline-block align-middle mt-[-3px] ml-[4px]"></span>
                </a>
              </Link>
            </div>
            <div className="row justify-center mt-[60px]">
              {isLoadingFeatBrands
                ? null
                : featBrandsData
                ? featBrandsData.map((b, i) => {
                    return (
                      <div className="six-col" key={i}>
                        <div className="mb-8 rounded-xl h-[70px] bg-white">
                          <Link
                            // We put & at last because there can be spaces in the brandName after the last character which would be ignored otherwise.
                            href={`/vehicle-listing?browseBy=brand&brandName=${b?.brandName}&`}
                          >
                            <a className="w-full px-3 py-3 flex items-center h-full">
                              <img
                                src={b?.brandImage?.imageUrl}
                                alt="logo"
                                className="w-full h-full object-contain"
                                title={b?.brandName}
                              />
                            </a>
                          </Link>
                        </div>
                      </div>
                    );
                  })
                : null}
              {/* <div className="six-col">
                <div className="mb-8 rounded-xl h-[70px] bg-white">
                  <Link
                    href={'/vehicle-listing?browseBy=brand&brandName=Benelli'}
                  >
                    <a className="w-full px-3 py-3 flex items-center h-full">
                      <img src="images/benelli-logo.png" alt="logo" />
                    </a>
                  </Link>
                </div>
              </div>
              <div className="six-col">
                <div className="mb-8 rounded-xl h-[70px] bg-white">
                  <Link
                    href={'/vehicle-listing?browseBy=brand&brandName=Honda'}
                  >
                    <a className="w-full px-3 py-3 flex items-center h-full">
                      <img src="images/honda-logo.png" alt="logo" />
                    </a>
                  </Link>
                </div>
              </div> */}
            </div>
          </div>
        </section>
        <section className="py-[55px]">
          <div className="container">
            <div className="c-heading flex justify-between items-end ">
              <h1 className="h3">
                Browse By <span>Budget</span>
              </h1>
              <Link
                href={`/vehicle-listing?browseBy=budget&budgetRange=${budgetQuery}&`}
              >
                <a className="px-3 py-1 text-sm bg-textColor text-white rounded-lg hover:bg-primary-dark hover:text-white">
                  View All
                  <span className="icon-arrow-right2 rotate-180 inline-block align-middle mt-[-3px] ml-[4px]"></span>
                </a>
              </Link>
            </div>
            <div className="mt-[-8px] mb-4">
              <ButtonTabs
                loading={isLoadingByBudget}
                btnArray={[
                  {
                    name: 'Below 1 Lakh',
                    queryValue: '0-99999',
                  },
                  {
                    name: '1 Lakh - 3 Lakhs',
                    queryValue: '100000-299999',
                  },
                  {
                    name: 'Above 3 Lakhs',
                    queryValue: '300000-10000000',
                  },
                ]}
                handleClick={handleBudgetRange}
              />
            </div>
            <div className="carousel-holder">
              {isLoadingByBudget ? (
                <div className="row">
                  <LoadingCard />
                </div>
              ) : !budgetItems.length ? (
                <p className="text-sm text-center">Data Unavailable</p>
              ) : (
                <AliceCarousel
                  mouseTracking
                  items={budgetItems}
                  responsive={responsive2}
                  controlsStrategy="alternate"
                />
              )}
              {/* <div className='text-center mt-8'>
                <Button loading={isLoadingByBudget} variant="outlined" onClick={() => router.push(`/vehicle-listing?browseBy=budget&budgetRange=${budgetQuery}&`)}>
                  View All
                </Button>
              </div> */}
            </div>
          </div>
        </section>
        <section className="py-[55px] relative">
          <div className="bg-pattern absolute top-0 bottom-0 left-0 right-[30%] z-[-1] opacity-10">
            <img src="images/street-outline.svg" alt="street outline" />
          </div>

          <div className="container relative">
            <div className="hidden opacity-60 lg:block bike-pattern absolute top-1/2 translate-y-[-50%] right-[-40px] z-[-1] w-[300px]">
              <img src="images/bike-pattern.png" alt="bike pattern" />
            </div>
            <div className="c-heading flex flex-wrap justify-between max-w-[1000px] mx-auto mb-[20px] lg:mb-[30px]">
              <h1 className="h3 h-full mb-6">
                How It <span>Works</span>
              </h1>
              <div className="w-full sm:max-w-[65%] sm:w-auto">
                <p>
                  Nepals finest two-wheeler buying and selling experience with
                  trust, selection, and best quality. Time Efficient Hassle Free
                  Easy to buy/sell and exchange User Friendly Interface Well
                  trained technician for any problems or issues Trustworthy
                </p>
              </div>
            </div>
            <div className="video-holder max-w-[750px] mx-auto relative">
              {/* <a href="#" className="hover:opacity-95">
                <span className="absolute top-1/2 translate-x-[-50%] translate-y-[-50%] left-1/2 w-[85px] sm:w-auto">
                  <img src="images/play-btn.svg" alt="play button" />
                </span>
                <img src="images/video-img.jpg" alt="image description" />
              </a> */}
              <ModalVideo
                id={(new Date() * 1).toString()}
                source={
                  'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4'
                }
                poster={
                  'https://raw.githubusercontent.com/waskito/react-modal-videojs/master/example/public/preview.png'
                }
                show={showVideos}
                showModal={() => {
                  setShowVideos(true);
                }}
                handleClose={() => {
                  setShowVideos(false);
                }}
              />
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </div>
  );
};

export default Homepage;
