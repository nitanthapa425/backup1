import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import Link from 'next/link';

import AdminLayout from 'layouts/Admin';
import Button from 'components/Button';
import Tabs from 'components/Tabs';
import ImgGallery from 'components/ImgGallery';
import { useReadVehicleDetailsQuery } from 'services/api/vehicle';
import BreadCrumb from 'components/BreadCrumb/breadCrumb';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';

function VehicleDetailContainer() {
  const router = useRouter();
  const [accordionData, setAccordionData] = useState([]);
  const [imageData, setImageData] = useState([]);

  const {
    data: vehicleDetails,
    isError: isVehicleError,
    isLoading: isLoadingVehicle,
  } = useReadVehicleDetailsQuery(router.query.id, {
    skip: !router.query.id,
  });

  const tabHeadings = [
    'Basics & Features',
    'Technical Specification',
    'Dimensions & Wheels',
  ];

  useEffect(() => {
    if (vehicleDetails) {
      const images = vehicleDetails?.vehicleImgPath.map((img) => ({
        original: img.imageUrl,
        thumbnail: img.imageUrl,
      }));
      setImageData(images || []);
      setAccordionData([
        {
          heading: 'Basic Factor',
          tabNumber: 1,
          list: [
            {
              label: 'Vehicle Type',
              data: vehicleDetails.vehicleType, // need to place in brand details
            },
            {
              label: 'Market Price',
              data: thousandNumberSeparator(vehicleDetails.marketPrice),
            },
            {
              label: 'Fuel Type',
              data: vehicleDetails.fuelType, // need to place in brand details
            },
            {
              label: 'Transmission Type',
              data: vehicleDetails.basicFactor?.transmissionType,
            },
            {
              label: 'Body Type',
              data: vehicleDetails.basicFactor?.bodyType,
            },
            {
              label: 'Seating Capacity',
              data: vehicleDetails.basicFactor?.seatingCapacity,
            },
            {
              label: 'Fuel Capacity (l)',
              data: vehicleDetails.basicFactor?.fuelCapacity,
            },
          ],
        },
        {
          heading: 'Option Features',
          tabNumber: 1,
          list: [
            {
              label: 'Instrumentation',
              data: vehicleDetails.optionFeature?.instrumentation.join(' , '),
            },
            {
              label: 'Safety',
              data: vehicleDetails.optionFeature?.safety.join(' , '),
            },
            {
              label: 'Features',
              data: vehicleDetails.optionFeature?.features.join(' , '),
            },
            {
              label: 'Ignition Type',
              data: vehicleDetails.optionFeature?.ignitionType,
            },
            {
              label: 'Charging Point',
              data: vehicleDetails.optionFeature?.chargingPoint
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Speedometer',
              data:
                vehicleDetails.optionFeature?.speedometer === 'DIGITAL'
                  ? 'Digital'
                  : vehicleDetails.optionFeature?.speedometer === 'ANALOG'
                  ? 'Analog'
                  : 'Not Available',
            },
            {
              label: 'Trip Meter',
              data:
                vehicleDetails.optionFeature?.tripMeter === 'DIGITAL'
                  ? 'Digital'
                  : vehicleDetails.optionFeature?.tripMeter === 'ANALOG'
                  ? 'Analog'
                  : 'Not Available',
            },
            {
              label: 'Pass Switch',
              data: vehicleDetails.optionFeature?.passSwitch
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Clock',
              data: vehicleDetails.optionFeature?.clock
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Riding Mode',
              data: vehicleDetails.optionFeature?.ridingModes
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Charging At Home',
              data: vehicleDetails.optionFeature?.chargingAtHome
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Charging At Charging Station ',
              data: vehicleDetails.optionFeature?.chargingAtChargingStation
                ? 'Available'
                : 'Not Available',
            },
            {
              label: 'Navigation',
              data: vehicleDetails.optionFeature?.navigation
                ? 'Available'
                : 'Not Available',
            },
          ],
        },
        {
          heading: 'Technical Factor',
          tabNumber: 2,
          list: [
            {
              label: 'Front Brake System',
              data: vehicleDetails.technicalFactor?.frontBrakeSystem,
            },
            {
              label: 'Back Brake System',
              data: vehicleDetails.technicalFactor?.backBrakeSystem,
            },
            {
              label: 'Front Suspension',
              data: vehicleDetails.technicalFactor?.frontSuspension,
            },
            {
              label: 'Rear Suspension',
              data: vehicleDetails.technicalFactor?.rearSuspension,
            },
            {
              label: 'No of Gears',
              data: vehicleDetails.technicalFactor?.noOfGears
                ? vehicleDetails.technicalFactor?.noOfGears
                : 'Not Available',
            },
            {
              label: 'Drive Type',
              data: vehicleDetails.technicalFactor?.driveType
                ? vehicleDetails.technicalFactor?.driveType
                : 'Not Available',
            },
            {
              label: 'Clutch Type',
              data: vehicleDetails.technicalFactor?.clutchType
                ? vehicleDetails.technicalFactor?.clutchType
                : 'Not Available',
            },
            {
              label: 'Gear Pattern',
              data: vehicleDetails.technicalFactor?.gearPattern
                ? vehicleDetails.technicalFactor?.gearPattern
                : 'Not Available',
            },
            {
              label: 'Headlight',
              data: vehicleDetails.technicalFactor?.headlight,
            },
            {
              label: 'Taillight',
              data: vehicleDetails.technicalFactor?.taillight,
            },
            {
              label: 'Starter',
              data: vehicleDetails.technicalFactor?.starter.join(' , '),
            },
            {
              label: 'Battery (V)',
              data: vehicleDetails.technicalFactor?.battery,
            },
            {
              label: 'Low Battery Indicator',
              data: vehicleDetails.technicalFactor?.lowBatteryIndicator
                ? 'Available'
                : 'Not Available',
            },
          ],
        },
        {
          heading: 'Engine Factor',
          tabNumber: 2,
          list: [
            {
              label: 'Displacement (cc)',
              data: vehicleDetails.engineFactor?.displacement,
            },
            // {
            //   label: 'Fuel Efficiency',
            //   data: vehicleDetails.engineFactor?.fuelEfficiency,
            // },
            {
              label: 'Engine Type',
              data: vehicleDetails.engineFactor?.engineType.join(' , '),
            },
            {
              label: 'No of Cylinders',
              data: vehicleDetails.engineFactor?.noOfCylinder,
            },
            {
              label: 'Valves Per Cylinder',
              data: vehicleDetails.engineFactor?.valvesPerCylinder,
            },
            {
              label: 'Valve Configuration',
              data: vehicleDetails.engineFactor?.valveConfiguration,
            },
            {
              label: 'Fuel Supply System',
              data: vehicleDetails.engineFactor?.fuelSupplySystem,
            },
            {
              label: 'Maximum Power (RPM)',
              data: vehicleDetails.engineFactor?.maximumPower,
            },
            {
              label: 'Maximum Torque (RPM)',
              data: vehicleDetails.engineFactor?.maximumTorque,
            },
            {
              label: 'Lubrication',
              data: vehicleDetails.engineFactor?.lubrication,
            },
            {
              label: 'Engine Oil',
              data: vehicleDetails.engineFactor?.engineOil,
            },
            {
              label: 'Air Cleaner',
              data: vehicleDetails.engineFactor?.airCleaner,
            },
            {
              label: 'Bore X Stroke',
              data: vehicleDetails.engineFactor?.boreXStroke,
            },
            {
              label: 'Compression Ratio',
              data: vehicleDetails.engineFactor?.compressionRatio,
            },
            {
              label: 'Motor Power',
              data: vehicleDetails.engineFactor?.motorPower
                ? vehicleDetails.engineFactor?.motorPower
                : 'Not Available',
            },
          ],
        },
        {
          heading: 'Dimensional Factor',
          tabNumber: 3,
          list: [
            {
              label: 'Wheelbase (mm)',
              data: vehicleDetails.dimensionalFactor?.wheelBase,
            },
            {
              label: 'Overall Width (mm)',
              data: vehicleDetails.dimensionalFactor?.overallWidth,
            },
            {
              label: 'Overall Length (mm)',
              data: vehicleDetails.dimensionalFactor?.overallLength,
            },
            {
              label: 'Overall Height (mm)',
              data: vehicleDetails.dimensionalFactor?.overallHeight,
            },
            {
              label: 'Ground Clearance (mm)',
              data: vehicleDetails.dimensionalFactor?.groundClearance,
            },
            {
              label: 'Kerb Weight (kg)',
              data: vehicleDetails.dimensionalFactor?.kerbWeight,
            },
            {
              label: 'Seat Height (mm)',
              data: vehicleDetails.dimensionalFactor?.doddleHeight,
            },
            {
              label: 'Bootspace (l)',
              data: vehicleDetails.dimensionalFactor?.bootSpace
                ? vehicleDetails.dimensionalFactor?.bootSpace
                : 'Not Available',
            },
          ],
        },
        {
          heading: 'Wheel Tyre Factor',
          tabNumber: 3,
          list: [
            {
              label: ' Front Wheel Type',
              data: vehicleDetails.wheelTyreFactor?.rearWheelType,
            },
            {
              label: 'Rear Wheel Type',
              data: vehicleDetails.wheelTyreFactor?.rearWheelType,
            },
            {
              label: 'Front Wheel Size (mm)',
              data: vehicleDetails.wheelTyreFactor?.rearWheelSize,
            },
            {
              label: 'Rear Wheel Size (mm)',
              data: vehicleDetails.wheelTyreFactor?.rearWheelSize,
            },

            {
              label: 'Steel Rims',
              data: vehicleDetails.wheelTyreFactor?.steelRims,
            },
          ],
        },
      ]);
    }
  }, [vehicleDetails]);
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Vehicle List',
      route: '/vehicle/view',
    },
    {
      routeName: 'Vehicle Details',
      route: '',
    },
  ];

  return (
    <AdminLayout documentTitle="Vehicle Detail">
      <main className="container">
        <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        {/* <button
          className="mb-3 hover:text-primary"
          onClick={() => router.back()}
        >
          Go Back
        </button> */}
        {isVehicleError ? (
          <p>Error loading data</p>
        ) : isLoadingVehicle ? (
          <p>Loading Vehicle Details</p>
        ) : vehicleDetails ? (
          <>
            <div className="flex justify-between mb-3">
              <h1 className="h4">{vehicleDetails.vehicleName}</h1>
              <Link href={`/vehicle/edit/${vehicleDetails.id}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
            <div className="container">
              <div className="row">
                <div className="two-col mb-5 detail-image-gallery">
                  <ImgGallery images={imageData} />
                </div>
                <div className="two-col">
                  <div className="px-4 pt-6 border border-black">
                    <h4 className="h5 mb-1">{vehicleDetails.brandName}</h4>
                    <p className="font-bold mb-6">
                      {vehicleDetails.vehicleName}
                    </p>
                    {/* <p className="font-bold ">
                      Make Year (AD):{' '}
                      <span className="font-normal">
                        {vehicleDetails.makeYear.slice(0, 4)}
                      </span>
                    </p> */}
                    {vehicleDetails.otherDetail && (
                      <p className="font-bold mb-10 ">
                        Sub-Variant:{' '}
                        <span className="font-normal">
                          {vehicleDetails.otherDetail}
                        </span>
                      </p>
                    )}

                    <p className="font-bold mb-6">Basic Facts:</p>
                    <div className="row">
                      <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Maximum Torque</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-6 w-6"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="#EC1C24"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M13 10V3L4 14h7v7l9-11h-7z"
                              />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.engineFactor?.maximumTorque} Nm
                          </p>
                        </div>
                      </div>
                      <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Mileage</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="#EC1C24"
                              className="bi bi-speedometer"
                              viewBox="0 0 16 16"
                            >
                              <path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z" />
                              <path
                                fillRule="evenodd"
                                d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"
                              />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.basicFactor?.mileage
                              ? vehicleDetails.basicFactor?.mileage
                              : '-'}{' '}
                            KMPL
                          </p>
                        </div>
                      </div>
                      {/* <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Make Year</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-6 w-6"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="#EC1C24"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"
                              />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.makeYear.slice(0, 4)}
                          </p>
                        </div>
                      </div> */}
                      <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Transmission</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="#EC1C24"
                              className="bi bi-joystick"
                              viewBox="0 0 16 16"
                            >
                              <path d="M10 2a2 2 0 0 1-1.5 1.937v5.087c.863.083 1.5.377 1.5.726 0 .414-.895.75-2 .75s-2-.336-2-.75c0-.35.637-.643 1.5-.726V3.937A2 2 0 1 1 10 2z" />
                              <path d="M0 9.665v1.717a1 1 0 0 0 .553.894l6.553 3.277a2 2 0 0 0 1.788 0l6.553-3.277a1 1 0 0 0 .553-.894V9.665c0-.1-.06-.19-.152-.23L9.5 6.715v.993l5.227 2.178a.125.125 0 0 1 .001.23l-5.94 2.546a2 2 0 0 1-1.576 0l-5.94-2.546a.125.125 0 0 1 .001-.23L6.5 7.708l-.013-.988L.152 9.435a.25.25 0 0 0-.152.23z" />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.basicFactor?.transmissionType}
                          </p>
                        </div>
                      </div>
                      <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Battery</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="#EC1C24"
                              className="bi bi-battery-half"
                              viewBox="0 0 16 16"
                            >
                              <path d="M2 6h5v4H2V6z" />
                              <path d="M2 4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2H2zm10 1a1 1 0 0 1 1 1v4a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V6a1 1 0 0 1 1-1h10zm4 3a1.5 1.5 0 0 1-1.5 1.5v-3A1.5 1.5 0 0 1 16 8z" />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.technicalFactor?.battery}
                          </p>
                        </div>
                      </div>
                      <div className="three-col w-1/2 md:w-1/3">
                        <div className="icon-holder tooltip-parent group">
                          <span className="tooltip">Fuel Capacity</span>
                          <div className="w-10 h-10 my-0 mx-auto border border-primary rounded-full flex justify-center items-center">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="#EC1C24"
                              className="bi bi-droplet-half"
                              viewBox="0 0 16 16"
                            >
                              <path
                                fillRule="evenodd"
                                d="M7.21.8C7.69.295 8 0 8 0c.109.363.234.708.371 1.038.812 1.946 2.073 3.35 3.197 4.6C12.878 7.096 14 8.345 14 10a6 6 0 0 1-12 0C2 6.668 5.58 2.517 7.21.8zm.413 1.021A31.25 31.25 0 0 0 5.794 3.99c-.726.95-1.436 2.008-1.96 3.07C3.304 8.133 3 9.138 3 10c0 0 2.5 1.5 5 .5s5-.5 5-.5c0-1.201-.796-2.157-2.181-3.7l-.03-.032C9.75 5.11 8.5 3.72 7.623 1.82z"
                              />
                              <path
                                fillRule="evenodd"
                                d="M4.553 7.776c.82-1.641 1.717-2.753 2.093-3.13l.708.708c-.29.29-1.128 1.311-1.907 2.87l-.894-.448z"
                              />
                            </svg>
                          </div>
                          <p className="text-center">
                            {vehicleDetails.basicFactor?.fuelCapacity} litres
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <Tabs
                  tabHeadings={tabHeadings}
                  hasAccordion={true}
                  accordionData={accordionData}
                />
              </div>
            </div>
          </>
        ) : null}
      </main>
    </AdminLayout>
  );
}

export default VehicleDetailContainer;
