import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';

import {
  useDeleteDraftVehicleMutation,
  useReadDraftVehicleQuery,
} from 'services/api/vehicleDraft';

function ViewDraftContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [vehicleDataQuery, setVehicleDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: vehicleData,
    isError: vehicleError,
    isFetching: isLoadingVehicle,
  } = useReadDraftVehicleQuery(vehicleDataQuery);

  // const deleteInfo = useDeleteDraftVehicleMutation();
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteDraftVehicleMutation();
  const {
    data: draftDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadDraftVehicleQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (draftDataAll) {
      setTableDataAll(
        draftDataAll.docs.map((vehicle) => {
          return {
            id: vehicle.id,
            vehicleName: vehicle.vehicleName,
            brandName: vehicle.brandName,
            modelName: vehicle.modelName,
            brandModelName: vehicle.brandModelName,
            displacement: vehicle.engineFactor?.displacement,
            /*
                    vehicleStatus: "Sold",
                    */
            frontBrake: vehicle.technicalFactor?.frontBrakeSystem,
            backBrake: vehicle.technicalFactor?.backBrakeSystem,
            marketPrice: vehicle.marketPrice,
            mileage: vehicle.basicFactor?.mileage,
            makeYear: !vehicle.makeYear
              ? undefined
              : vehicle.makeYear.slice(0, 10),
          };
        })
      );
    }
  }, [draftDataAll]);

  const columns = useMemo(
    () => [
      {
        // NOTE: If we don't put id, the accessor is what will be sent into sortBy object.
        // NOTE: When we put id, it will be sent into sortBy object instead of accessor. This id is gonna be sent into sortBy object. And we'll use that as api endpoint as our api sorts nested data like: '?sortBy=engineFactor.displacement&sortOrder=1'.
        // id should be the same level format as our api response. Eg: If displacement is inside engineFactor in api response, then id should be engineFactor.displacement.
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        // canBeSorted is the column property not given by react-table itself and we assigned it ourselves
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'brandName',
        Header: 'Brand Name',
        accessor: 'brandName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'modelName',
        Header: 'Model Name',
        accessor: 'modelName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        // NOTE: This id is gonna be sent into sortBy object. And we'll use that as api endpoint as our api sorts nested data like: '?sortBy=engineFactor.displacement&sortOrder=1'.
        id: 'engineFactor.displacement',
        Header: 'Displacement (cc)',
        accessor: 'displacement',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      /*
            {
                Header: 'Vehicle Status',
                accessor: 'vehicleStatus',
                Cell: ({ cell: { value } }) => <VehicleStatus value={value} />
            },
            */
      {
        id: 'technicalFactor.frontBrakeSystem',
        Header: 'Front Brake',
        accessor: 'frontBrake',
        Cell: ({ cell: { value } }) => value || '-',
        canBeFiltered: true,
      },
      {
        id: 'technicalFactor.backBrakeSystem',
        Header: 'Back Brake',
        accessor: 'backBrake',
        Cell: ({ cell: { value } }) => value || '-',
        canBeFiltered: true,
      },
      {
        id: 'marketPrice',
        Header: 'Market Price',
        accessor: 'marketPrice',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'basicFactor.mileage',
        Header: 'Mileage',
        accessor: 'mileage',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'makeYear',
        Header: 'Make Year',
        accessor: 'makeYear',
        Cell: ({ cell: { value } }) => value || '-',
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    // If Sort is clicked/exists in that column
    /*
        if (sortBy.length) {
            // setVehicleDataQuery(`?limit=${pageSize}&page=${pageIndex + 1}&sortBy=${sortBy[0].id}&sortOrder=${sortBy[0].desc ? "-1" : "1"}`);
            setVehicleDataQuery(query);
        } else {
            var searchString = '';
            if (filters && filters.length) {
                searchString =
                    '&search=' +
                    filters.map(v => {
                        return `"${v.id}":"${v.value}"`;
                    })
                        .join(',');
            }
            setVehicleDataQuery(`?limit=${pageSize}&page=${pageIndex + 1}&sortBy=createdAt&sortOrder=-1${searchString}`);
        }
        */
    setVehicleDataQuery(query);
  };

  useEffect(() => {
    if (vehicleData) {
      setPageCount(vehicleData.totalPages);
      setTotalData(vehicleData.totalDocs);
      // var vehicleDataArr = vehicleData.docs;
      // Cloning the original data and reversing it
      //  var reversedArr = [...vehicleDataArr].reverse();
      setTableData(
        vehicleData.docs.map((vehicle) => {
          return {
            id: vehicle.id,
            vehicleName: vehicle.vehicleName,
            brandName: vehicle.brandName,
            modelName: vehicle.modelName,
            brandModelName: vehicle.brandModelName,
            displacement: vehicle.engineFactor?.displacement,
            /*
                    vehicleStatus: "Sold",
                    */
            frontBrake: vehicle.technicalFactor?.frontBrakeSystem,
            backBrake: vehicle.technicalFactor?.backBrakeSystem,
            marketPrice: vehicle.marketPrice,
            mileage: vehicle.basicFactor?.mileage,
            makeYear: !vehicle.makeYear
              ? undefined
              : vehicle.makeYear.slice(0, 10),
          };
        })
      );
    }
  }, [vehicleData]);

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Draft List',
      route: '/vehicle/draft/view',
    },
  ];

  return (
    <AdminLayout
      documentTitle="View VehicleDraft"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}
          <Table
            tableName="VehicleDraft/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={vehicleError}
            isLoadingData={isLoadingVehicle}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="vehicle/draft/edit"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewDraftContainer;
