import { useEffect, useState } from 'react';
import Button from 'components/Button';
import AdminLayout from 'layouts/Admin';
import {
  useDeleteDraftVehicleMutation,
  useGetDraftedVehiclesQuery,
} from 'services/api/vehicleDraft';
import Link from 'next/link';
import { useToasts } from 'react-toast-notifications';
import Popup from 'components/Popup';

const Draft = () => {
  const { addToast } = useToasts();

  const [showDelPopup, setShowDelPopup] = useState(false);
  const [currentVehicleId, setCurrentVehicleId] = useState(null);

  const { data } = useGetDraftedVehiclesQuery();

  const [deleteDraftVehicle, { isError, error, isSuccess, data: deleteData }] =
    useDeleteDraftVehicleMutation();

  useEffect(() => {
    if (isError) {
      addToast(
        error?.message ||
          'Error occured while deleting drafted vehicle. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  useEffect(() => {
    if (isSuccess) {
      addToast(deleteData?.message || 'Draft Delete successfully.', {
        appearance: 'success',
      });
    }
  }, [isSuccess]);

  return (
    <AdminLayout documentTitle="Draft Vehicles">
      <div className="ml-5">
        {data?.length === 0 && 'No records found.'}
        {data?.map((vehicle) => {
          return (
            <div
              key={vehicle?.id}
              className="border max-w-screen-md my-2 p-2 shadow rounded flex justify-between align-middle"
            >
              <div>
                <h2 className="text-lg">{vehicle?.vehicleName}</h2>
                <span className="text-sm text-gray-500 -mt-3">
                  Drafted at: {vehicle?.updatedAt?.split('T')[0]}
                </span>
              </div>
              <div className="">
                <Button
                  size="sm"
                  onClick={() => {
                    setCurrentVehicleId(vehicle?.id);
                    setShowDelPopup(true);
                  }}
                >
                  Delete
                </Button>
                <Link href={`/vehicle/draft/edit/${vehicle?.id}`}>
                  <a className="ml-2">
                    <Button size="sm" variant="outlined">
                      Edit
                    </Button>
                  </a>
                </Link>
              </div>
            </div>
          );
        })}
      </div>

      {showDelPopup && (
        <Popup
          title="Do you delete this draft?"
          description=""
          onOkClick={() => {
            deleteDraftVehicle(currentVehicleId);
            setShowDelPopup(false);
            setCurrentVehicleId(null);
          }}
          onCancelClick={() => setShowDelPopup(false)}
          okText="Delete"
          cancelText="Cancel"
        />
      )}
    </AdminLayout>
  );
};

export default Draft;
