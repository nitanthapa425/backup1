import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';
import {
  useDeleteVehicleMutation,
  useReadVehicleCustomQuery,
} from 'services/api/vehicle';

import { SelectColumnFilter } from 'components/Table/Filter';

function ViewBikeContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [vehicleDataQuery, setVehicleDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: vehicleData,
    isError: isVehicleError,
    isFetching: isLoadingVehicle,
  } = useReadVehicleCustomQuery(vehicleDataQuery);

  const {
    data: vehicleDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadVehicleCustomQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  // console.log(
  //   'orginal&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&',
  //   isLoadingAll
  // );
  useEffect(() => {
    if (vehicleDataAll) {
      setTableDataAll(
        vehicleDataAll.docs.map((vehicle) => {
          return {
            id: vehicle.id || '-',
            vehicleName: vehicle.vehicleName || '-',
            vehicleType: vehicle.vehicleType || '-',
            fuelType: vehicle.fuelType || '-',
            brandName: vehicle.brandName || '-',
            modelName: vehicle.modelName || '-',
            displacement: vehicle.engineFactor?.displacement || '-',
            frontBrake: vehicle.technicalFactor?.frontBrakeSystem || '-',
            backBrake: vehicle.technicalFactor?.backBrakeSystem || '-',
            marketPrice: vehicle.marketPrice || '-',
            mileage: vehicle.basicFactor?.mileage || '-',
          };
        })
      );
    }
  }, [vehicleDataAll]);

  // console.log('tabledata******', tableDataAll);
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteVehicleMutation();

  const columns = useMemo(
    () => [
      {
        // NOTE: If we don't put id, the accessor is what will be sent into sortBy object.
        // NOTE: When we put id, it will be sent into sortBy object instead of accessor. This id is gonna be sent into sortBy object. And we'll use that as api endpoint as our api sorts nested data like: '?sortBy=engineFactor.displacement&sortOrder=1'.
        // id should be the same level format as our api response. Eg: If displacement is inside engineFactor in api response, then id should be engineFactor.displacement.
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        // canBeSorted is the column property not given by react-table itself and we assigned it ourselves. canSort is the react-table given property.
        canBeSorted: true,
        // canBeSorted is the column property not given by react-table itself and we assigned it ourselves. canFilter is the react-table given property.
        canBeFiltered: true,
      },
      {
        id: 'vehicleType',
        Header: 'Vehicle Type',
        accessor: 'vehicleType',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'BIKE',
            value: 'BIKE',
          },
          {
            label: 'SCOOTER',
            value: 'SCOOTER',
          },
        ],
      },
      {
        id: 'fuelType',
        Header: 'Fuel Type',
        accessor: 'fuelType',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'ELECTRIC',
            value: 'ELECTRIC',
          },
          {
            label: 'PETROL',
            value: 'PETROL',
          },
        ],
      },
      {
        id: 'brandName',
        Header: 'Brand Name',
        accessor: 'brandName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'modelName',
        Header: 'Model Name',
        accessor: 'modelName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        // NOTE: This id is gonna be sent into sortBy object. And we'll use that as api endpoint as our api sorts nested data like: '?sortBy=engineFactor.displacement&sortOrder=1'.
        id: 'engineFactor.displacement',
        Header: 'Displacement (CC)',
        accessor: 'displacement',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      /*
            {
                Header: 'Vehicle Status',
                accessor: 'vehicleStatus',
                Cell: ({ cell: { value } }) => <VehicleStatus value={value} />
            },
            */
      {
        id: 'technicalFactor.frontBrakeSystem',
        Header: 'Front Brake',
        accessor: 'frontBrake',
        Cell: ({ cell: { value } }) => value || '-',
        canBeFiltered: true,
      },
      {
        id: 'technicalFactor.backBrakeSystem',
        Header: 'Back Brake',
        accessor: 'backBrake',
        Cell: ({ cell: { value } }) => value || '-',
        canBeFiltered: true,
      },
      {
        id: 'marketPrice',
        Header: 'Market Price (NRs)',
        accessor: 'marketPrice',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'basicFactor.mileage',
        Header: 'Mileage (KMPL)',
        accessor: 'mileage',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    // If Sort is clicked/exists in that column
    /*
        if (sortBy.length) {
            // setVehicleDataQuery(`?limit=${pageSize}&page=${pageIndex + 1}&sortBy=${sortBy[0].id}&sortOrder=${sortBy[0].desc ? "-1" : "1"}`);
            setVehicleDataQuery(query);
        } else {
            var searchString = '';
            if (filters && filters.length) {
                searchString =
                    '&search=' +
                    filters.map(v => {
                        return `"${v.id}":"${v.value}"`;
                    })
                        .join(',');
            }
            setVehicleDataQuery(`?limit=${pageSize}&page=${pageIndex + 1}&sortBy=createdAt&sortOrder=-1${searchString}`);
        }
        */
    setVehicleDataQuery(query);
  };

  useEffect(() => {
    if (vehicleData) {
      setPageCount(vehicleData.totalPages);
      setTotalData(vehicleData.totalDocs);
      // var vehicleDataArr = vehicleData.docs;
      // Cloning the original data and reversing it
      //  var reversedArr = [...vehicleDataArr].reverse();
      setTableData(
        vehicleData.docs.map((vehicle) => {
          return {
            // id is required
            id: vehicle.id,
            vehicleName: vehicle.vehicleName,
            vehicleType: vehicle.vehicleType,
            fuelType: vehicle.fuelType,
            brandName: vehicle.brandName,
            modelName: vehicle.modelName,
            displacement: vehicle.engineFactor?.displacement,
            /*
                    vehicleStatus: "Sold",
                    */
            frontBrake: vehicle.technicalFactor?.frontBrakeSystem,
            backBrake: vehicle.technicalFactor?.backBrakeSystem,
            marketPrice: vehicle.marketPrice,
            mileage: vehicle.basicFactor?.mileage,
          };
        })
      );
    }
  }, [vehicleData]);
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Vehicle List',
      route: '/vehicle/view',
    },
  ];

  return (
    <AdminLayout documentTitle="View Vehicles" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Vehicle/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={isVehicleError}
            isLoadingData={isLoadingVehicle}
            // isLoadingData={true}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="vehicle/edit"
            viewRoute="vehicle/vehicleDetail"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: 'Add Vehicle', route: '/vehicle/add' }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewBikeContainer;
