import BrandDetails from './steps/BrandDetails';
import BasicsFacts from './steps/BasicsFacts';
import OptionFeatures from './steps/OptionFeatures';
import TechnicalSpecifications from './steps/TechnicalSpecifications';
import vehicleDetails from 'container/Seller/Add/steps/VehicleDetails';
import vehicleImages from 'container/Seller/Add/steps/VehicleImages';
import VehicleLocations from 'container/Seller/Add/steps/VehicleLocation';
import GeneralInformation from 'container/Seller/Add/steps/OtherDetails';
import CustomerDetail from 'container/Seller/Add/steps/CustomerDetails';

export const genders = ['Male', 'Female', 'Other'];

export const stepsData = [
  {
    id: 0,
    key: 'BRAND_DETAILS',
    title: 'Brand Details',
    component: BrandDetails,
  },
  {
    id: 1,
    key: 'BASIC_FACTS',
    title: 'Basic Facts',
    component: BasicsFacts,
  },
  {
    id: 2,
    key: 'OPTIONS_AND_FEATURES',
    title: 'Option and Features',
    component: OptionFeatures,
  },
  {
    id: 3,
    key: 'TECHNICAL_SPECIFICATION',
    title: 'Technical Specification',
    component: TechnicalSpecifications,
  },
];
export const sellVehicleStepsData = [
  {
    id: 0,
    key: 'VEHICLE_DETAILS',
    title: 'Vehicle Details',
    component: vehicleDetails,
  },
  {
    id: 1,
    key: 'VEHICLE_IMAGES',
    title: 'Vehicle Images',
    component: vehicleImages,
  },
  {
    id: 2,
    key: 'OTHER_INFORMATION',
    title: 'Other Details',
    component: GeneralInformation,
  },
  {
    id: 3,
    key: 'VEHICLE_LOCATION',
    title: 'Vehicle Location',
    component: VehicleLocations,
  },
];

export const sellVehicleStepsDataAdmin = [
  {
    id: 0,
    key: 'VEHICLE_DETAILS',
    title: 'Vehicle Details',
    component: vehicleDetails,
  },
  {
    id: 1,
    key: 'VEHICLE_IMAGES',
    title: 'Vehicle Images',
    component: vehicleImages,
  },
  {
    id: 2,
    key: 'OTHER_INFORMATION',
    title: 'Other Details',
    component: GeneralInformation,
  },
  {
    id: 3,
    key: 'VEHICLE_LOCATION',
    title: 'Vehicle Location',
    component: VehicleLocations,
  },
  {
    id: 4,
    key: 'CUSTOMER_DETAILS',
    title: 'Customer Details',
    component: CustomerDetail,
  },
];

export const AddToFeature = [
  {
    id: 0,
    key: 'CONFIRMATION',
    title: 'Confirmation',
    component: vehicleDetails,
  },
  {
    id: 1,
    key: 'TIME',
    title: 'Time',
    component: vehicleImages,
  },
];
