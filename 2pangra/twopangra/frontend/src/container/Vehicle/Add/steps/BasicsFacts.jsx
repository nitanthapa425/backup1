import { useEffect } from 'react';
// import { useSelector } from 'react-redux';
import ReactSelect from 'components/Select/ReactSelect';
// import UnitDropdown from 'components/UnitDropdown';
// import { setFuelCapacityUnit } from 'store/features/selectedUnits/addVehicleUnitsSlice';
import {
  useCreateBodyTypeMutation,
  useCreateFuelCapacityMutation,
  // useCreateFuelTypeMutation,
  useCreateMileageMutation,
  useCreateSeatingCapacityMutation,
  useCreateTransmissionTypeMutation,
} from 'services/api/vehicle';
// import Unit from 'components/Unit';
// import { volumeConverter } from 'utils/unitConverter';

const BasicsFacts = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  allVehicleDetails,
  isLoadingAllVehicleDetails,
  setFieldTouched,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  // const { fuelCapacityUnit } = useSelector((state) => state.addVehicleUnits);

  // const [
  //   createFuelType,
  //   {
  //     isLoading: isLoadingCreateFuelType,
  //     // isError: isErrorCreateFuelType,
  //     // isSuccess: isSuccessCreateFuelType,
  //   },
  // ] = useCreateFuelTypeMutation();

  const [
    createTransmissionType,
    {
      isLoading: isLoadingCreateTransmissionType,
      // isError: isErrorCreateTransmissionType,
      // isSuccess: isSuccessCreateTransmissionType,
    },
  ] = useCreateTransmissionTypeMutation();

  const [
    createBodyType,
    {
      isLoading: isLoadingCreateBodyType,
      // isError: isErrorCreateBodyType,
      // isSuccess: isSuccessCreateBodyType,
    },
  ] = useCreateBodyTypeMutation();

  const [
    createSeatingCapacity,
    {
      isLoading: isLoadingCreateSeatingCapacity,
      // isError: isErrorCreateSeatingCapacity,
      // isSuccess: isSuccessCreateSeatingCapacity,
    },
  ] = useCreateSeatingCapacityMutation();

  const [
    createFuelCapacity,
    {
      isLoading: isLoadingCreateFuelCapacity,
      // isError: isErrorCreateFuelCapacity,
      // isSuccess: isSuccessCreateFuelCapacity,
    },
  ] = useCreateFuelCapacityMutation();

  const [
    createMileage,
    {
      isLoading: isLoadingCreateMileage,
      // isError: isErrorCreateMileage,
      // isSuccess: isSuccessCreateMileage,
    },
  ] = useCreateMileageMutation();

  return (
    <>
      <div className="row-sm ">
        {/* <div className="three-col-sm">
          <ReactSelect
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateFuelType}
            label="Fuel Type"
            placeholder=" E.g: Petrol"
            error={
              touched?.basicFactor?.fuelType
                ? errors?.basicFactor?.fuelType
                : ''
            }
            value={values?.basicFactor?.fuelType}
            onChange={(selectedFuelType, actionMeta) => {
              setFieldValue('basicFactor.fuelType', selectedFuelType.value);
              setFieldValue(
                'engineFactor.engineFactorFuelType',
                selectedFuelType.value
              );
              setFieldTouched('basicFactor.fuelType');

              if (
                selectedFuelType.value &&
                actionMeta.action === 'create-option'
              ) {
                createFuelType({
                  fuelType: selectedFuelType.value,
                });
              }
            }}
            options={allVehicleDetails?.fuelType?.map((fuelType) => ({
              value: fuelType.fuelType,
              label: fuelType.fuelType,
            }))}
            onBlur={() => {
              setFieldTouched('basicFactor.fuelType');
            }}
          />
        </div>
       */}
        <div className="three-col-sm">
          <ReactSelect
            required
            loading={
              isLoadingAllVehicleDetails || isLoadingCreateTransmissionType
            }
            label="Transmission Type"
            placeholder="E.g: Manual Transmission"
            error={
              touched?.basicFactor?.transmissionType
                ? errors?.basicFactor?.transmissionType
                : ''
            }
            value={values?.basicFactor?.transmissionType}
            onChange={(selectedTransmissionType, actionMeta) => {
              setFieldTouched('basicFactor.transmissionType');

              setFieldValue(
                'basicFactor.transmissionType',
                selectedTransmissionType.value
              );

              if (
                selectedTransmissionType.value &&
                actionMeta.action === 'create-option'
              ) {
                createTransmissionType({
                  transmissionType: selectedTransmissionType.value,
                });
              }
            }}
            options={allVehicleDetails?.transmissionType?.map(
              (transmissionType) => ({
                value: transmissionType.transmissionType,
                label: transmissionType.transmissionType,
              })
            )}
            onBlur={() => {
              setFieldTouched('basicFactor.transmissionType');
            }}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateBodyType}
            label="Body Type"
            placeholder="E.g: Touring"
            error={
              touched?.basicFactor?.bodyType
                ? errors?.basicFactor?.bodyType
                : ''
            }
            value={values?.basicFactor?.bodyType}
            onChange={(selectedBodyType, actionMeta) => {
              setFieldTouched('basicFactor.bodyType');

              setFieldValue('basicFactor.bodyType', selectedBodyType.value);

              if (
                selectedBodyType.value &&
                actionMeta.action === 'create-option'
              ) {
                createBodyType({
                  bodyType: selectedBodyType.value,
                });
              }
            }}
            options={allVehicleDetails?.bodyType?.map((bodyType) => ({
              value: bodyType.bodyType,
              label: bodyType.bodyType,
            }))}
            onBlur={() => {
              setFieldTouched('basicFactor.bodyType');
            }}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            menuPlacement="top"
            required
            loading={
              isLoadingAllVehicleDetails || isLoadingCreateSeatingCapacity
            }
            label="Seating Capacity"
            placeholder="E.g: 2"
            error={
              touched?.basicFactor?.seatingCapacity
                ? errors?.basicFactor?.seatingCapacity
                : ''
            }
            value={Number(values?.basicFactor?.seatingCapacity)}
            onChange={(selectedSeatingCapacity, actionMeta) => {
              setFieldTouched('basicFactor.seatingCapacity');

              setFieldValue(
                'basicFactor.seatingCapacity',
                selectedSeatingCapacity.value
              );

              if (
                selectedSeatingCapacity.value &&
                actionMeta.action === 'create-option'
              ) {
                createSeatingCapacity({
                  seatingCapacity: selectedSeatingCapacity.value,
                });
              }
            }}
            options={allVehicleDetails?.seatingCapacity?.map(
              (seatingCapacity) => ({
                value: seatingCapacity.seatingCapacity,
                label: seatingCapacity.seatingCapacity,
              })
            )}
            onBlur={() => {
              setFieldTouched('basicFactor.seatingCapacity');
            }}
          />
        </div>
        <div className="three-col-sm unit-converter-container">
          <ReactSelect
            menuPlacement="top"
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateFuelCapacity}
            label="Fuel Tank Capacity"
            placeholder="E.g: 15"
            error={
              touched?.basicFactor?.fuelCapacity
                ? errors?.basicFactor?.fuelCapacity
                : ''
            }
            value={Number(values?.basicFactor?.fuelCapacity)}
            onChange={(selectedFuelCapacity, actionMeta) => {
              setFieldTouched('basicFactor.fuelCapacity');
              setFieldValue(
                'basicFactor.fuelCapacity',
                selectedFuelCapacity.value
              );

              // setFieldValue('fuelCapacityUnit', 'l');
              if (
                selectedFuelCapacity.value &&
                actionMeta.action === 'create-option'
              ) {
                createFuelCapacity({
                  // selectedFuelCapacity.value
                  fuelCapacity: selectedFuelCapacity.value,
                });
              }
            }}
            options={allVehicleDetails?.fuelCapacity?.map((fuelCapacity) => ({
              value: fuelCapacity.fuelCapacity,
              label: fuelCapacity.fuelCapacity,
            }))}
            onBlur={() => {
              setFieldTouched('basicFactor.fuelCapacity');
            }}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            menuPlacement="top"
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateMileage}
            label="Mileage (KMPL)"
            placeholder="E.g: 30"
            error={
              touched?.basicFactor?.mileage ? errors?.basicFactor?.mileage : ''
            }
            value={Number(values?.basicFactor?.mileage)}
            onChange={(selectedMileage, actionMeta) => {
              setFieldTouched('basicFactor.mileage');
              setFieldValue('basicFactor.mileage', selectedMileage.value);

              if (
                selectedMileage.value &&
                actionMeta.action === 'create-option'
              ) {
                createMileage({
                  mileage: selectedMileage.value,
                });
              }
            }}
            options={allVehicleDetails?.mileage?.map((mileage) => ({
              value: mileage.mileage,
              label: mileage.mileage,
            }))}
            onBlur={() => {
              setFieldTouched('basicFactor.mileage');
            }}
          />
        </div>
      </div>
    </>
  );
};

export default BasicsFacts;
