import { useState, useEffect } from 'react';
// import DatePicker from 'react-datepicker';

import DropZone from 'components/DropZone';

import ReactSelect from 'components/Select/ReactSelect';
// import Input from 'components/Input';

import { useReadBrandQuery } from 'services/api/brand';
import { useGetModelFromBrandQuery } from 'services/api/model';
import { useGetSubModelFromModelQuery } from 'services/api/varient';
import {
  useCreateFuelTypeMutation,
  useCreateMarketPriceMutation,
  // useCreateOtherDetailMutation,
  useCreateVehicleTypeMutation,
} from 'services/api/vehicle';

import 'react-datepicker/dist/react-datepicker.css';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
// import Textarea from 'components/Input/textarea';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import Popup from 'components/Popup';
import Input from 'components/Input';
// import RadioBox from 'components/RadioBox/RadioBox';

const BrandDetails = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  allVehicleDetails,
  isLoadingAllVehicleDetails,
  setFieldTouched,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const [brandModelData, setModelData] = useState([]);
  const [variantData, setVariantData] = useState([]);
  const [vehicleType, setVehicleType] = useState([]);
  const [fuelType, setFuelType] = useState([]);

  const [selectedBrandId, setBrandId] = useState(values.brandId);
  const [selectedModelId, setModelId] = useState(values.brandModelId);

  const { data: brandData, isLoading: fetchingBrands } = useReadBrandQuery();

  // const [createOtherDetail, { isLoading: isLoadingCreateOtherDetail }] =
  //   useCreateOtherDetailMutation();

  const [createMarketPrice, { isLoading: isLoadingCreateMarketPrice }] =
    useCreateMarketPriceMutation();

  const [createVehicleType, { isLoading: isLoadingCreateVehicleType }] =
    useCreateVehicleTypeMutation();

  const {
    data: modelData,
    isLoading: fetchingModel,
    isError: modelError,
  } = useGetModelFromBrandQuery(selectedBrandId, {
    skip: !selectedBrandId,
  });

  const {
    data: subModelData,
    isLoading: fetchingSubModel,
    isError: subModelError,
  } = useGetSubModelFromModelQuery(selectedModelId, {
    skip: !selectedModelId,
  });

  // success
  useEffect(() => {
    modelData && setModelData(modelData);
  }, [JSON.stringify(modelData)]);
  useEffect(() => {
    subModelData && setVariantData(subModelData);
  }, [JSON.stringify(subModelData)]);
  // error
  useEffect(() => {
    modelError && setModelData([]);
  }, [modelError]);
  useEffect(() => {
    subModelError && setVariantData([]);
  }, [subModelError]);

  const setNewFiles = (newFiles) => {
    const allFiles = [...values.vehicleImgPath, ...newFiles];
    setFieldValue('vehicleImgPath', allFiles);
  };

  const handleRemoveFile = (idx) => {
    const currentFiles = [...values.vehicleImgPath];
    currentFiles.splice(idx, 1);
    setFieldValue('vehicleImgPath', currentFiles);
  };

  useEffect(() => {
    setBrandId(values.brandId);
    setModelId(values.modelId);
  }, [values?.brandId, values?.modelId]);

  const [
    createFuelType,
    {
      isLoading: isLoadingCreateFuelType,
      // isError: isErrorCreateFuelType,
      // isSuccess: isSuccessCreateFuelType,
    },
  ] = useCreateFuelTypeMutation();

  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [openModalFuelType, setOpenModalFuelType] = useState(false);

  return (
    <>
      <div className="row-sm">
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateVehicleType}
            label="Vehicle Type"
            placeholder="E.g: Bike"
            error={touched?.vehicleType ? errors?.vehicleType : ''}
            value={values.vehicleType}
            onChange={(selectedVehicleType, actionMeta) => {
              setFieldTouched('vehicleType');
              setVehicleType(selectedVehicleType.value);
              setOpenModalVehicleType(true);

              if (
                selectedVehicleType.value &&
                actionMeta.action === 'create-option'
              ) {
                createVehicleType({
                  vehicleType: selectedVehicleType.value,
                });
              }
            }}
            options={allVehicleDetails?.vehicleType?.map((vehicleType) => ({
              value: vehicleType.vehicleType,
              label: vehicleType.vehicleType,
            }))}
            onBlur={() => {
              setFieldTouched('vehicleType');
            }}
          />
        </div>
        {openModalVehicleType && (
          <Popup
            title="Are you sure to change Vehicle Type?"
            description="If you change Vehicle Type, the data related to the Vehicle Type will be removed."
            onOkClick={() => {
              setFieldValue('vehicleType', vehicleType);
              setFieldValue(
                'technicalFactor.technicalVehicleType',
                vehicleType
              );
              setFieldValue(
                'dimensionalFactor.dimensionalVehicleType',
                vehicleType
              );
              setOpenModalVehicleType(false);
            }}
            onCancelClick={() => setOpenModalVehicleType(false)}
            okText="Change"
            cancelText="Cancel"
          />
        )}
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateFuelType}
            label="Fuel Type"
            placeholder=" E.g: Petrol"
            error={touched?.fuelType ? errors?.fuelType : ''}
            value={values?.fuelType}
            onChange={(selectedFuelType, actionMeta) => {
              setOpenModalFuelType(true);
              setFieldTouched('fuelType');
              setFuelType(selectedFuelType.value);
              if (
                selectedFuelType.value &&
                actionMeta.action === 'create-option'
              ) {
                createFuelType({
                  fuelType: selectedFuelType.value,
                });
              }
            }}
            options={allVehicleDetails?.fuelType?.map((fuelType) => ({
              value: fuelType.fuelType,
              label: fuelType.fuelType,
            }))}
            onBlur={() => {
              setFieldTouched('fuelType');
            }}
          />
        </div>
        {openModalFuelType && (
          <Popup
            title="Are you sure to change Fuel Type?"
            description="If you change Fuel Type, the data related to the Fuel Type will be removed."
            onOkClick={() => {
              setFieldValue('fuelType', fuelType);
              setFieldValue('engineFactor.engineFactorFuelType', fuelType);
              setOpenModalFuelType(false);
            }}
            onCancelClick={() => setOpenModalFuelType(false)}
            okText="Change"
            cancelText="Cancel"
          />
        )}
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={fetchingBrands}
            label="Select Brand"
            placeholder="E.g: Bajaj"
            error={touched?.brandId ? errors?.brandId : ''}
            value={values.brandId || null}
            onChange={(selectedBrand) => {
              // setFieldTouched('brandId');
              setBrandId(selectedBrand.value);
              setFieldValue('brandId', selectedBrand.value);
              // reseting the model and variant value
              setFieldValue('modelId', '');
              setFieldValue('varientId', '');
              setFieldValue('otherDetail', '');
              setFieldTouched('modelId', false);
              setFieldTouched('varientId', false);
            }}
            options={brandData?.docs?.map((brand) => ({
              value: brand.id,
              label: brand.brandVehicleName,
            }))}
            onBlur={() => {
              setFieldTouched('brandId');
            }}
          />
        </div>
        <div
          className="three-col-sm"
          title={!selectedBrandId ? 'Select brand before selecting model' : ''}
        >
          <SelectWithoutCreate
            required
            loading={fetchingModel}
            label="Select Model"
            placeholder="E.g: Pulsar"
            error={touched?.modelId ? errors?.modelId : ''}
            value={values.modelId}
            onChange={(selectedBrandModel) => {
              setModelId(selectedBrandModel.value);
              setFieldValue('modelId', selectedBrandModel.value);
              // setFieldTouched('modelId');
              // reseting the variant value
              setFieldValue('varientId', '');
              setFieldValue('otherDetail', '');
              setFieldTouched('varientId', false);
            }}
            options={brandModelData?.map((brandModel) => ({
              value: brandModel.id,
              label: brandModel.modelName,
            }))}
            isDisabled={!selectedBrandId}
            onBlur={() => {
              setFieldTouched('modelId');
            }}
          />
        </div>
        <div
          className="three-col-sm"
          title={
            !selectedModelId ? 'Select model before selecting sub model' : ''
          }
        >
          <SelectWithoutCreate
            loading={fetchingSubModel}
            label="Select Variant"
            placeholder="E.g: Pulsar 150 Twin Disc"
            error={touched?.varientId ? errors?.varientId : ''}
            value={values.varientId}
            onChange={(selectedBrandSubModel) => {
              setFieldTouched('varientId');
              setFieldValue('varientId', selectedBrandSubModel.value);
              setFieldValue('otherDetail', '');
            }}
            options={variantData?.map((variant) => ({
              value: variant.id,
              label: variant.varientName,
            }))}
            isDisabled={!selectedModelId}
            onBlur={() => {
              setFieldTouched('varientId');
            }}
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Sub-Variant"
            name="otherDetail"
            type="text"
            placeholder="Sub-Variant"
            required={false}
            disabled={!values.varientId}
          />

          {/* <input disabled></input> */}
          {/* <Textarea
            label=""
            name="otherDetail"
            type="text"
            placeholder="Other Detail"
            required={false}
          /> */}
          {/* <ReactSelect
            // required
            loading={isLoadingAllVehicleDetails || isLoadingCreateOtherDetail}
            label="Model Description"
            placeholder="E.g: Model Description"
            error={touched?.otherDetail ? errors?.otherDetail : ''}
            value={values?.otherDetail}
            onChange={(selectedOtherDetail, actionMeta) => {
              setFieldValue('otherDetail', selectedOtherDetail.value);
              setFieldTouched('otherDetail');

              if (
                selectedOtherDetail.value &&
                actionMeta.action === 'create-option'
              ) {
                createOtherDetail({
                  otherDetail: selectedOtherDetail.value,
                });
              }
            }}
            options={allVehicleDetails?.otherDetail?.map((otherDetail) => ({
              value: otherDetail.otherDetail,
              label: otherDetail.otherDetail,
            }))}
            onBlur={() => {
              setFieldTouched('otherDetail');
            }}
          /> */}
        </div>
        {/* <div className="three-col-sm">
          <label htmlFor="">
            Make Year(AD)
            <span className="required">*</span>
          </label>
          <DatePicker
            selected={values?.makeYear ? new Date(values?.makeYear) : null}
            onChange={(date) => {
              setFieldValue('makeYear', date || '');
            }}
            showYearPicker
            dateFormat="yyyy"
            yearItemNumber={9}
            placeholderText="E.g: 2021"
            maxDate={new Date()}
          />
          {touched.makeYear && errors.makeYear && (
            <div className="text-primary-dark">{errors.makeYear}</div>
          )}
        </div> */}
        <div className="three-col-sm">
          <DropZone
            label="Upload Vehicle Image/s"
            required
            currentFiles={values.vehicleImgPath}
            setNewFiles={setNewFiles}
            handleRemoveFile={handleRemoveFile}
            error={touched?.vehicleImgPath ? errors?.vehicleImgPath : ''}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            required
            loading={isLoadingAllVehicleDetails || isLoadingCreateMarketPrice}
            label="Market Price(NRs)"
            placeholder="E.g: 2,60,900"
            error={touched?.marketPrice ? errors?.marketPrice : ''}
            // value={values?.marketPrice}
            value={Number(values?.marketPrice)}
            onChange={(selectedMarketPrice, actionMeta) => {
              setFieldTouched('marketPrice');

              setFieldValue('marketPrice', selectedMarketPrice.value);
              if (
                selectedMarketPrice.value &&
                actionMeta.action === 'create-option'
              ) {
                createMarketPrice({
                  marketPrice: selectedMarketPrice.value,
                });
              }
            }}
            options={allVehicleDetails?.market?.map((marketPrice) => ({
              value: +marketPrice.marketPrice || marketPrice.marketPrice,
              label: thousandNumberSeparator(marketPrice.marketPrice),
            }))}
            onBlur={() => {
              setFieldTouched('marketPrice');
            }}
          />
        </div>{' '}
        {/* <div className="three-col-sm">
          <RadioBox
            title="title1"
            name="nitan"
            values={[
              { label: 'Digital', value: 'Digital' },
              { label: 'Analog', value: 'Analog' },
            ]}
            onChange={(e) => {
              setFieldValue('nitan', e.target.value);
            }}
          ></RadioBox>
        </div> */}
        {/* <div
          onChange={(e) => {
            setFieldValue('nitan', e.target.value);
          }}
        >
          <input
            type="radio"
            name="nitan"
            value="v1"
            checked={values.nitan === 'v1'}
          />{' '}
          Name
          <input
            type="radio"
            name="nitan"
            value="v2"
            checked={values.nitan === 'v2'}
          />{' '}
          Name1
        </div> */}
      </div>
    </>
  );
};

export default BrandDetails;
