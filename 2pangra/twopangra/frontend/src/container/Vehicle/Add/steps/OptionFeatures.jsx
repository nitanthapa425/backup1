import { useEffect } from 'react';

import {
  useCreateFeatureMutation,
  useCreateIgnitionMutation,
  useCreateInstrumentationMutation,
  useCreateSafetyMutation,
} from 'services/api/vehicle';
import ReactSelect from 'components/Select/ReactSelect';
import RadioBox from 'components/RadioBox/RadioBox';

const OptionFeatures = ({
  values,
  errors,
  setFieldValue,
  touched,
  setChanged,
  dirty,
  allVehicleDetails,
  isLoadingAllVehicleDetails,
  setFieldTouched,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const [
    createInstrumentation,
    {
      isLoading: isLoadingInstrumentation,
      // isError: isErrorInstrumentation,
      // isSuccess: isSuccessInstrumentation,
    },
  ] = useCreateInstrumentationMutation();
  const [
    createSafeties,
    {
      isLoading: isLoadingCreateSafety,
      // isError: isErrorCreateSafety,
      // isSuccess: isSuccessCreateSafety,
    },
  ] = useCreateSafetyMutation();
  const [
    createFeatures,
    {
      isLoading: isLoadingCreateFeature,
      // isError: isErrorCreateFeature,
      // isSuccess: isSuccessCreateFeature,
    },
  ] = useCreateFeatureMutation();
  const [
    createIgnition,
    {
      isLoading: isLoadingIgnition,
      // isError: isErrorIgnition,
      // isSuccess: isSuccessIgnition,
    },
  ] = useCreateIgnitionMutation();

  return (
    <>
      <div className="row-sm">
        <div className="three-col-sm">
          <ReactSelect
            isMulti={true}
            required
            loading={isLoadingInstrumentation || isLoadingAllVehicleDetails}
            label="Instrumentation/s"
            placeholder="E.g: Fully Digital Clusters"
            error={
              touched?.optionFeature?.instrumentation
                ? errors?.optionFeature?.instrumentation
                : ''
            }
            value={values?.optionFeature?.instrumentation || []}
            onChange={(newTags, actionMeta) => {
              setFieldTouched('optionFeature.instrumentation');
              setFieldValue(
                'optionFeature.instrumentation',
                newTags.map((netTag) => netTag.value)
              );
              if (actionMeta.action === 'create-option') {
                createInstrumentation({
                  instrumentation: newTags[newTags.length - 1]?.value,
                });
              }
            }}
            options={allVehicleDetails?.instrumentation?.map(
              (instrumentation, i) => ({
                value: instrumentation.instrumentation,
                label: instrumentation.instrumentation,
              })
            )}
            onBlur={() => {
              setFieldTouched('optionFeature.instrumentation');
            }}
          />
          <span className="text-sm inline-block mt-2">
            Note: Please press enter to add Instrumentations{' '}
          </span>
        </div>
        <div className="three-col-sm">
          <ReactSelect
            isMulti={true}
            required
            loading={isLoadingCreateSafety || isLoadingAllVehicleDetails}
            label="Safety/s"
            placeholder="E.g: Anti-lock Brake Systems (ABS)"
            error={
              touched?.optionFeature?.safety
                ? errors?.optionFeature?.safety
                : ''
            }
            value={values?.optionFeature?.safety || []}
            onChange={(newTags, actionMeta) => {
              setFieldTouched('optionFeature.safety');
              setFieldValue(
                'optionFeature.safety',
                newTags.map((netTag) => netTag.value)
              );
              if (actionMeta.action === 'create-option') {
                createSafeties({
                  safety: newTags[newTags.length - 1]?.value,
                });
              }
            }}
            options={allVehicleDetails?.safety?.map((safety, i) => ({
              value: safety.safety,
              label: safety.safety,
            }))}
            onBlur={() => {
              setFieldTouched('optionFeature.safety');
            }}
          />
          <span className="text-sm inline-block mt-2">
            Note: Please press enter to add safety{' '}
          </span>
        </div>
        <div className="three-col-sm">
          <ReactSelect
            required
            loading={isLoadingAllVehicleDetails || isLoadingIgnition}
            label="Ignition Type"
            placeholder="E.g: Distributor-Based"
            error={
              touched?.optionFeature?.ignitionType
                ? errors?.optionFeature?.ignitionType
                : ''
            }
            value={values.optionFeature.ignitionType}
            onChange={(ignition, actionMeta) => {
              setFieldTouched('optionFeature.ignitionType');
              setFieldValue('optionFeature.ignitionType', ignition.value);
              if (ignition.value && actionMeta.action === 'create-option') {
                createIgnition({
                  ignitionType: ignition.value,
                });
              }
            }}
            options={allVehicleDetails?.ignition?.map((data) => ({
              value: data.ignitionType,
              label: data.ignitionType,
            }))}
            onBlur={() => {
              setFieldTouched('optionFeature.ignitionType');
            }}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            menuPlacement="top"
            isMulti={true}
            required
            loading={isLoadingCreateFeature || isLoadingAllVehicleDetails}
            label="Feature/s"
            placeholder="E.g: Stability Control"
            error={
              touched?.optionFeature?.features
                ? errors?.optionFeature?.features
                : ''
            }
            value={values?.optionFeature?.features || []}
            onChange={(newTags, actionMeta) => {
              setFieldTouched('optionFeature.features');
              setFieldValue(
                'optionFeature.features',
                newTags.map((netTag) => netTag.value)
              );
              if (actionMeta.action === 'create-option') {
                createFeatures({
                  feature: newTags[newTags.length - 1]?.value,
                });
              }
            }}
            options={allVehicleDetails?.feature?.map((feature, i) => ({
              value: feature.feature,
              label: feature.feature,
            }))}
            onBlur={() => {
              setFieldTouched('optionFeature.features');
            }}
          />
          {/* <TagInput
            required
            label="Features"
            placeholder="Features"
            value={values?.optionFeature?.features || []}
            handleChange={(newTags) => {
              setFieldValue('optionFeature.features', newTags);
            }}
            error={
              touched?.optionFeature?.features
                ? errors?.optionFeature?.features
                : ''
            }
          /> */}
          <span className="text-sm inline-block mt-2">
            Note: Please press enter to add features{' '}
          </span>
        </div>

        {values.fuelType === 'ELECTRIC' && (
          <>
            <div className="three-col-sm">
              <RadioBox
                title="Speedometer"
                name="optionFeature.speedometer"
                values={[
                  { label: 'ANALOG', value: 'ANALOG' },
                  { label: 'DIGITAL', value: 'DIGITAL' },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.speedometer', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Charging point"
                name="optionFeature.chargingPoint"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.chargingPoint', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Trip meter"
                name="optionFeature.tripMeter"
                values={[
                  { label: 'ANALOG', value: 'ANALOG' },
                  { label: 'DIGITAL', value: 'DIGITAL' },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.tripMeter', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Pass switch"
                name="optionFeature.passSwitch"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.passSwitch', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Clock"
                name="optionFeature.clock"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.clock', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Riding modes"
                name="optionFeature.ridingModes"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.ridingModes', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Navigation"
                name="optionFeature.navigation"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.navigation', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Charging at home"
                name="optionFeature.chargingAtHome"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue('optionFeature.chargingAtHome', e.target.value);
                }}
              ></RadioBox>
            </div>

            <div className="three-col-sm">
              <RadioBox
                title="Charging at chargingStation"
                name="optionFeature.chargingAtChargingStation"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue(
                    'optionFeature.chargingAtChargingStation',
                    e.target.value
                  );
                }}
              ></RadioBox>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default OptionFeatures;
