import { useEffect } from 'react';
// import { useSelector } from 'react-redux';

import ReactSelect from 'components/Select/ReactSelect';
// import UnitDropdown from 'components/UnitDropdown';
// import { setBootSpaceUnit } from 'store/features/selectedUnits/addVehicleUnitsSlice';

import {
  useCreateAirCleanerMutation,
  useCreateBatteryMutation,
  useCreateBootSpaceMutation,
  useCreateBoreXStrokeMutation,
  useCreateClutchTypeMutation,
  useCreateCompressionRatioMutation,
  useCreateDisplacementMutation,
  useCreateDoddleHeightMutation,
  useCreateDriveTypeMutation,
  useCreateEngineOilMutation,
  useCreateEngineTypeMutation,
  useCreateFrontBrakeSystemMutation,
  useCreateFrontSuspensionMutation,
  useCreateFrontWheelSizeMutation,
  // useCreateFrontTyreMutation,
  useCreateFrontWheelTypeMutation,
  // useCreateFrontWheelDriveMutation,
  // useCreateFuelEfficiencyMutation,
  useCreateFuelSupplySystemMutation,
  useCreateGearPatternMutation,
  useCreateGroundClearanceMutation,
  useCreateHeadLightMutation,
  useCreateKerbWeightMutation,
  useCreateLubricationMutation,
  useCreateMaximumPowerMutation,
  useCreateMaximumTorqueMutation,
  useCreateMotorPowerMutation,
  useCreateNoOfCylinderMutation,
  useCreateNoOfGearMutation,
  useCreateOverallHeightMutation,
  useCreateOverallLengthMutation,
  useCreateOverallWidthMutation,
  useCreateRearBrakeSystemMutation,
  useCreateRearSuspensionMutation,
  useCreateRearWheelSizeMutation,
  useCreateRearWheelTypeMutation,
  useCreateStarterMutation,
  useCreateSteelRimsMutation,
  useCreateTailLightMutation,
  useCreateValveConfigurationMutation,
  useCreateValvesPerCylinderMutation,
  useCreateWheelBaseMutation,
  // useCreateWheelSizeMutation,
  // useCreateWheelTypeMutation,
} from 'services/api/vehicle';
import RadioBox from 'components/RadioBox/RadioBox';
// import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import {
  lengthConverter,
  massConverter,
  volumeConverter,
} from 'utils/unitConverter';
import Unit from 'components/Unit';

const TechnicalSpecifications = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  allVehicleDetails,
  isLoadingAllVehicleDetails,
  setFieldTouched,
  type,
}) => {
  // useEffect(() => {
  //   setChanged(dirty);
  // }, [dirty]);

  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  // const { bootSpaceUnit } = useSelector((state) => state.addVehicleUnits);

  const [
    createRearBrakeSystem,
    {
      isLoading: isLoadingCreateRearBrakeSystem,
      // isError: isErrorCreateRearBrakeSystem,
      // isSuccess: isSuccessCreateRearBrakeSystem,
    },
  ] = useCreateRearBrakeSystemMutation();

  const [
    createFrontBrakeSystem,
    {
      isLoading: isLoadingCreateFrontBrakeSystem,
      // isError: isErrorCreateFrontBrakeSystem,
      // isSuccess: isSuccessCreateFrontBrakeSystem,
    },
  ] = useCreateFrontBrakeSystemMutation();

  const [
    createFrontSuspension,
    {
      isLoading: isLoadingCreateFrontSuspension,
      // isError: isErrorCreateFrontSuspension,
      // isSuccess: isSuccessCreateFrontSuspension,
    },
  ] = useCreateFrontSuspensionMutation();

  const [
    createRearSuspension,
    {
      isLoading: isLoadingCreateRearSuspension,
      // isError: isErrorCreateRearSuspension,
      // isSuccess: isSuccessCreateRearSuspension,
    },
  ] = useCreateRearSuspensionMutation();

  const [
    createNoOfGear,
    {
      isLoading: isLoadingCreateNoOfGear,
      // isError: isErrorCreateNoOfGear,
      // isSuccess: isSuccessCreateNoOfGear,
    },
  ] = useCreateNoOfGearMutation();

  const [
    createDriveType,
    {
      isLoading: isLoadingCreateDriveType,
      // isError: isErrorCreateDriveType,
      // isSuccess: isSuccessCreateDriveType,
    },
  ] = useCreateDriveTypeMutation();

  const [
    createClutchType,
    {
      isLoading: isLoadingCreateClutchType,
      // isError: isErrorCreateClutchType,
      // isSuccess: isSuccessCreateClutchType,
    },
  ] = useCreateClutchTypeMutation();

  const [
    createGearPattern,
    {
      isLoading: isLoadingCreateGearPattern,
      // isError: isErrorCreateGearPattern,
      // isSuccess: isSuccessCreateGearPattern,
    },
  ] = useCreateGearPatternMutation();

  const [
    createHeadLight,
    {
      isLoading: isLoadingCreateHeadLight,
      // isError: isErrorCreateHeadLight,
      // isSuccess: isSuccessCreateHeadLight,
    },
  ] = useCreateHeadLightMutation();

  const [
    createTailLight,
    {
      isLoading: isLoadingCreateTailLight,
      // isError: isErrorCreateTailLight,
      // isSuccess: isSuccessCreateTailLight,
    },
  ] = useCreateTailLightMutation();

  const [
    createStarter,
    {
      isLoading: isLoadingCreateStarter,
      // isError: isErrorCreateStarter,
      // isSuccess: isSuccessCreateStarter,
    },
  ] = useCreateStarterMutation();

  const [
    createBattery,
    {
      isLoading: isLoadingCreateBattery,
      // isError: isErrorCreateBattery,
      // isSuccess: isSuccessCreateBattery,
    },
  ] = useCreateBatteryMutation();

  const [
    createWheelBase,
    {
      isLoading: isLoadingCreateWheelBase,
      // isError: isErrorCreateWheelBase,
      // isSuccess: isSuccessCreateWheelBase,
    },
  ] = useCreateWheelBaseMutation();

  const [
    createOverallWidth,
    {
      isLoading: isLoadingCreateOverallWidth,
      // isError: isErrorCreateOverallWidth,
      // isSuccess: isSuccessCreateOverallWidth,
    },
  ] = useCreateOverallWidthMutation();

  const [
    createOverallLength,
    {
      isLoading: isLoadingCreateOverallLength,
      // isError: isErrorCreateOverallLength,
      // isSuccess: isSuccessCreateOverallLength,
    },
  ] = useCreateOverallLengthMutation();

  const [
    createOverallHeight,
    {
      isLoading: isLoadingCreateOverallHeight,
      // isError: isErrorCreateOverallHeight,
      // isSuccess: isSuccessCreateOverallHeight,
    },
  ] = useCreateOverallHeightMutation();

  const [
    createGroundClearance,
    {
      isLoading: isLoadingCreateGroundClearance,
      // isError: isErrorCreateGroundClearance,
      // isSuccess: isSuccessCreateGroundClearance,
    },
  ] = useCreateGroundClearanceMutation();

  const [
    createBootSpace,
    {
      isLoading: isLoadingCreateBootSpace,
      // isError: isErrorCreateBootSpace,
      // isSuccess: isSuccessCreateBootSpace,
    },
  ] = useCreateBootSpaceMutation();

  const [
    createKerbWeight,
    {
      isLoading: isLoadingCreateKerbWeight,
      // isError: isErrorCreateKerbWeight,
      // isSuccess: isSuccessCreateKerbWeight,
    },
  ] = useCreateKerbWeightMutation();

  const [
    createDoddleHeight,
    {
      isLoading: isLoadingCreateDoddleHeight,
      // isError: isErrorCreateDoddleHeight,
      // isSuccess: isSuccessCreateDoddleHeight,
    },
  ] = useCreateDoddleHeightMutation();

  // const [
  //   createWheelType,
  //   {
  //     isLoading: isLoadingCreateWheelType,
  //     // isError: isErrorCreateWheelType,
  //     // isSuccess: isSuccessCreateWheelType,
  //   },
  // ] = useCreateWheelTypeMutation();

  const [
    createFrontWheelSize,
    {
      isLoading: isLoadingCreateFrontWheelSize,
      // isError: isErrorCreateWheelSize,
      // isSuccess: isSuccessCreateWheelSize,
    },
  ] = useCreateFrontWheelSizeMutation();
  const [
    createRearWheelSize,
    {
      isLoading: isLoadingCreateRearWheelSize,
      // isError: isErrorCreateWheelSize,
      // isSuccess: isSuccessCreateWheelSize,
    },
  ] = useCreateRearWheelSizeMutation();

  const [
    createRearWheelType,
    {
      isLoading: isLoadingCreateRearWheelType,
      // isError: isErrorCreateRearTyre,
      // isSuccess: isSuccessCreateRearTyre,
    },
  ] = useCreateRearWheelTypeMutation();

  const [
    createFrontWheelType,
    {
      isLoading: isLoadingCreateFrontWheelType,
      // isError: isErrorCreateFrontTyre,
      // isSuccess: isSuccessCreateFrontTyre,
    },
  ] = useCreateFrontWheelTypeMutation();

  // const [
  //   createFrontWheelDrive,
  //   {
  //     isLoading: isLoadingCreateFrontWheelDrive,
  //     // isError: isErrorCreateFrontWheelDrive,
  //     // isSuccess: isSuccessCreateFrontWheelDrive,
  //   },
  // ] = useCreateFrontWheelDriveMutation();

  const [
    createSteelRims,
    {
      isLoading: isLoadingCreateSteelRims,
      // isError: isErrorCreateSteelRims,
      // isSuccess: isSuccessCreateSteelRims,
    },
  ] = useCreateSteelRimsMutation();

  const [
    createDisplacement,
    {
      isLoading: isLoadingCreateDisplacement,
      // isError: isErrorCreateDisplacement,
      // isSuccess: isSuccessCreateDisplacement,
    },
  ] = useCreateDisplacementMutation();

  // const [
  //   createFuelEfficiency,
  //   {
  //     isLoading: isLoadingCreateFuelEfficiency,
  //     // isError: isErrorCreateFuelEfficiency,
  //     // isSuccess: isSuccessCreateFuelEfficiency,
  //   },
  // ] = useCreateFuelEfficiencyMutation();

  const [
    createEngineType,
    {
      isLoading: isLoadingCreateEngineType,
      // isError: isErrorCreateEngineType,
      // isSuccess: isSuccessCreateEngineType,
    },
  ] = useCreateEngineTypeMutation();

  const [
    createNoOfCylinder,
    {
      isLoading: isLoadingCreateNoOfCylinder,
      // isError: isErrorCreateNoOfCylinder,
      // isSuccess: isSuccessCreateNoOfCylinder,
    },
  ] = useCreateNoOfCylinderMutation();

  const [
    createValvesPerCylinder,
    {
      isLoading: isLoadingCreateValvesPerCylinder,
      // isError: isErrorCreateValvesPerCylinder,
      // isSuccess: isSuccessCreateValvesPerCylinder,
    },
  ] = useCreateValvesPerCylinderMutation();

  const [
    createValveConfiguration,
    {
      isLoading: isLoadingCreateValveConfiguration,
      // isError: isErrorCreateValveConfiguration,
      // isSuccess: isSuccessCreateValveConfiguration,
    },
  ] = useCreateValveConfigurationMutation();

  const [
    createFuelSupplySystem,
    {
      isLoading: isLoadingCreateFuelSupplySystem,
      // isError: isErrorCreateFuelSupplySystem,
      // isSuccess: isSuccessCreateFuelSupplySystem,
    },
  ] = useCreateFuelSupplySystemMutation();

  const [
    createMaximumTorque,
    {
      isLoading: isLoadingCreateMaximumTorque,
      // isError: isErrorCreateMaximumTorque,
      // isSuccess: isSuccessCreateMaximumTorque,
    },
  ] = useCreateMaximumTorqueMutation();

  const [
    createMaximumPower,
    {
      isLoading: isLoadingCreateMaximumPower,
      // isError: isErrorCreateMaximumPower,
      // isSuccess: isSuccessCreateMaximumPower,
    },
  ] = useCreateMaximumPowerMutation();

  const [
    createLubrication,
    {
      isLoading: isLoadingCreateLubrication,
      // isError: isErrorCreateLubrication,
      // isSuccess: isSuccessCreateLubrication,
    },
  ] = useCreateLubricationMutation();

  const [
    createEngineOil,
    {
      isLoading: isLoadingCreateEngineOil,
      // isError: isErrorCreateEngineOil,
      // isSuccess: isSuccessCreateEngineOil,
    },
  ] = useCreateEngineOilMutation();

  const [
    createAirCleaner,
    {
      isLoading: isLoadingCreateAirCleaner,
      // isError: isErrorCreateAirCleaner,
      // isSuccess: isSuccessCreateAirCleaner,
    },
  ] = useCreateAirCleanerMutation();

  const [
    createBoreXStroke,
    {
      isLoading: isLoadingCreateBoreXStroke,
      // isError: isErrorCreateBoreXStroke,
      // isSuccess: isSuccessCreateBoreXStroke,
    },
  ] = useCreateBoreXStrokeMutation();

  const [
    createMotorPower,
    {
      isLoading: isLoadingCreateMotorPower,
      // isError: isErrorCreateBoreXStroke,
      // isSuccess: isSuccessCreateBoreXStroke,
    },
  ] = useCreateMotorPowerMutation();

  const [
    createCompressionRatio,
    {
      isLoading: isLoadingCreateCompressionRatio,
      // isError: isErrorCreateCompressionRatio,
      // isSuccess: isSuccessCreateCompressionRatio,
    },
  ] = useCreateCompressionRatioMutation();

  return (
    <div>
      <div className="form-group">
        <h2 className="h6">Brake System</h2>
        <div className="row-sm">
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateRearBrakeSystem
              }
              label="Back Brake System"
              placeholder="E.g: Disc Brake"
              error={
                touched?.technicalFactor?.backBrakeSystem
                  ? errors?.technicalFactor?.backBrakeSystem
                  : ''
              }
              value={values?.technicalFactor?.backBrakeSystem}
              onChange={(selectedBackBrakeSystem, actionMeta) => {
                setFieldTouched('technicalFactor.backBrakeSystem');

                setFieldValue(
                  'technicalFactor.backBrakeSystem',
                  selectedBackBrakeSystem.value
                    .toLowerCase()
                    .split('')
                    .map((letter, i) => {
                      if (i === 0) return letter.toUpperCase();
                      else return letter;
                    })
                    .join('')
                );
                if (
                  selectedBackBrakeSystem.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createRearBrakeSystem({
                    backBrakeSystem: selectedBackBrakeSystem.value,
                  });
                }
              }}
              options={allVehicleDetails?.backBrake?.map((backBrakeSystem) => ({
                value: backBrakeSystem.backBrakeSystem,
                label: backBrakeSystem.backBrakeSystem,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.backBrakeSystem');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontBrakeSystem
              }
              label="Front Brake System"
              placeholder="E.g: Disc Brake"
              error={
                touched?.technicalFactor?.frontBrakeSystem
                  ? errors?.technicalFactor?.frontBrakeSystem
                  : ''
              }
              value={values?.technicalFactor?.frontBrakeSystem}
              onChange={(selectedFrontBrakeSystem, actionMeta) => {
                setFieldTouched('technicalFactor.frontBrakeSystem');

                setFieldValue(
                  'technicalFactor.frontBrakeSystem',
                  selectedFrontBrakeSystem.value
                    .toLowerCase()
                    .split('')
                    .map((letter, i) => {
                      if (i === 0) return letter.toUpperCase();
                      else return letter;
                    })
                    .join('')
                );

                if (
                  selectedFrontBrakeSystem.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontBrakeSystem({
                    frontBrakeSystem: selectedFrontBrakeSystem.value,
                  });
                }
              }}
              options={allVehicleDetails?.frontBrake?.map(
                (frontBrakeSystem) => ({
                  value: frontBrakeSystem.frontBrakeSystem,
                  label: frontBrakeSystem.frontBrakeSystem,
                })
              )}
              onBlur={() => {
                setFieldTouched('technicalFactor.frontBrakeSystem');
              }}
            />
          </div>
        </div>
      </div>
      <div className="form-group">
        <h2 className="h6">Suspension</h2>
        <div className="row-sm">
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontSuspension
              }
              label="Front Suspension"
              placeholder="E.g: Torsion Bars"
              error={
                touched?.technicalFactor?.frontSuspension
                  ? errors?.technicalFactor?.frontSuspension
                  : ''
              }
              value={values?.technicalFactor?.frontSuspension}
              onChange={(selectedFrontSuspension, actionMeta) => {
                setFieldTouched('technicalFactor.frontSuspension');
                setFieldValue(
                  'technicalFactor.frontSuspension',
                  selectedFrontSuspension.value
                );

                if (
                  selectedFrontSuspension.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontSuspension({
                    frontSuspension: selectedFrontSuspension.value,
                  });
                }
              }}
              options={allVehicleDetails?.frontSuspension?.map(
                (frontSuspension) => ({
                  value: frontSuspension.frontSuspension,
                  label: frontSuspension.frontSuspension,
                })
              )}
              onBlur={() => {
                setFieldTouched('technicalFactor.frontSuspension');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateRearSuspension
              }
              label="Rear Suspension"
              placeholder="E.g: Leaf Springs"
              error={
                touched?.technicalFactor?.rearSuspension
                  ? errors?.technicalFactor?.rearSuspension
                  : ''
              }
              value={values?.technicalFactor?.rearSuspension}
              onChange={(selectedRearSuspension, actionMeta) => {
                setFieldTouched('technicalFactor.rearSuspension');
                setFieldValue(
                  'technicalFactor.rearSuspension',
                  selectedRearSuspension.value
                );
                if (
                  selectedRearSuspension.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createRearSuspension({
                    rearSuspension: selectedRearSuspension.value,
                  });
                }
              }}
              options={allVehicleDetails?.rearSuspension?.map(
                (rearSuspension) => ({
                  value: rearSuspension.rearSuspension,
                  label: rearSuspension.rearSuspension,
                })
              )}
              onBlur={() => {
                setFieldTouched('technicalFactor.rearSuspension');
              }}
            />
          </div>
        </div>
      </div>
      <div className="form-group">
        <h2 className="h6">Transmission</h2>
        <div className="row-sm">
          {values.vehicleType === 'BIKE' && (
            <div className="three-col-sm">
              <ReactSelect
                required
                loading={isLoadingAllVehicleDetails || isLoadingCreateNoOfGear}
                label="Number of Gears"
                placeholder="E.g: 6"
                error={
                  touched?.technicalFactor?.noOfGears
                    ? errors?.technicalFactor?.noOfGears
                    : ''
                }
                value={Number(values?.technicalFactor?.noOfGears)}
                onChange={(selectedNoOfGear, actionMeta) => {
                  setFieldTouched('technicalFactor.noOfGears');

                  setFieldValue(
                    'technicalFactor.noOfGears',
                    selectedNoOfGear.value
                  );
                  if (
                    selectedNoOfGear.value &&
                    actionMeta.action === 'create-option'
                  ) {
                    createNoOfGear({
                      noOfGears: selectedNoOfGear.value,
                    });
                  }
                }}
                options={allVehicleDetails?.noOfGear?.map((noOfGear) => ({
                  value: noOfGear.noOfGears,
                  label: noOfGear.noOfGears,
                }))}
                onBlur={() => {
                  setFieldTouched('technicalFactor.noOfGears');
                }}
              />
            </div>
          )}
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateDriveType}
              label="Drive Type"
              placeholder="E.g: Chain Drive"
              error={
                touched?.technicalFactor?.driveType
                  ? errors?.technicalFactor?.driveType
                  : ''
              }
              value={values?.technicalFactor?.driveType}
              onChange={(selectedDriveType, actionMeta) => {
                setFieldTouched('technicalFactor.driveType');
                setFieldValue(
                  'technicalFactor.driveType',
                  selectedDriveType.value
                );
                if (
                  selectedDriveType.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createDriveType({
                    driveType: selectedDriveType.value,
                  });
                }
              }}
              options={allVehicleDetails?.driveType?.map((driveType) => ({
                value: driveType.driveType,
                label: driveType.driveType,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.driveType');
              }}
            />
          </div>
          {values?.vehicleType === 'BIKE' && (
            <div className="three-col-sm">
              <ReactSelect
                required
                loading={
                  isLoadingAllVehicleDetails || isLoadingCreateClutchType
                }
                label="Clutch Type"
                placeholder="E.g: Single Plate Clutch"
                error={
                  touched?.technicalFactor?.clutchType
                    ? errors?.technicalFactor?.clutchType
                    : ''
                }
                value={values?.technicalFactor?.clutchType}
                onChange={(selectedClutchType, actionMeta) => {
                  setFieldTouched('technicalFactor.clutchType');
                  setFieldValue(
                    'technicalFactor.clutchType',
                    selectedClutchType.value
                  );
                  if (
                    selectedClutchType.value &&
                    actionMeta.action === 'create-option'
                  ) {
                    createClutchType({
                      clutchType: selectedClutchType.value,
                    });
                  }
                }}
                options={allVehicleDetails?.clutchType?.map((clutchType) => ({
                  value: clutchType.clutchType,
                  label: clutchType.clutchType,
                }))}
                onBlur={() => {
                  setFieldTouched('technicalFactor.clutchType');
                }}
              />
            </div>
          )}
          {values?.vehicleType === 'BIKE' && (
            <div className="three-col-sm">
              <ReactSelect
                required
                loading={
                  isLoadingAllVehicleDetails || isLoadingCreateGearPattern
                }
                label="Gear Pattern"
                placeholder="E.g: 1-N-2-3-4-5"
                error={
                  touched?.technicalFactor?.gearPattern
                    ? errors?.technicalFactor?.gearPattern
                    : ''
                }
                value={values?.technicalFactor?.gearPattern}
                onChange={(selectedGearPattern, actionMeta) => {
                  setFieldTouched('technicalFactor.gearPattern');
                  setFieldValue(
                    'technicalFactor.gearPattern',
                    selectedGearPattern.value
                  );
                  if (
                    selectedGearPattern.value &&
                    actionMeta.action === 'create-option'
                  ) {
                    createGearPattern({
                      gearPattern: selectedGearPattern.value,
                    });
                  }
                }}
                options={allVehicleDetails?.gearPattern?.map((gearPattern) => ({
                  value: gearPattern.gearPattern,
                  label: gearPattern.gearPattern,
                }))}
                onBlur={() => {
                  setFieldTouched('technicalFactor.gearPattern');
                }}
              />
            </div>
          )}
        </div>
      </div>
      <div className="form-group">
        <h2 className="h6">Electricals</h2>
        <div className="row-sm">
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateHeadLight}
              label="HeadLight/HeadLamp"
              placeholder="E.g: LED"
              error={
                touched?.technicalFactor?.headlight
                  ? errors?.technicalFactor?.headlight
                  : ''
              }
              value={values?.technicalFactor?.headlight}
              onChange={(selectedHeadLight, actionMeta) => {
                setFieldTouched('technicalFactor.headlight');
                setFieldValue(
                  'technicalFactor.headlight',
                  selectedHeadLight.value
                );
                if (
                  selectedHeadLight.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createHeadLight({
                    headlight: selectedHeadLight.value,
                  });
                }
              }}
              options={allVehicleDetails?.headlight?.map((headlight) => ({
                value: headlight.headlight,
                label: headlight.headlight,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.headlight');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateTailLight}
              label="TailLight/TailLamp"
              placeholder="E.g: Halogen"
              error={
                touched?.technicalFactor?.taillight
                  ? errors?.technicalFactor?.taillight
                  : ''
              }
              value={values?.technicalFactor?.taillight}
              onChange={(selectedTailLight, actionMeta) => {
                setFieldTouched('technicalFactor.taillight');
                setFieldValue(
                  'technicalFactor.taillight',
                  selectedTailLight.value
                );
                if (
                  selectedTailLight.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createTailLight({
                    taillight: selectedTailLight.value,
                  });
                }
              }}
              options={allVehicleDetails?.taillight?.map((taillight) => ({
                value: taillight.taillight,
                label: taillight.taillight,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.taillight');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              isMulti={true}
              required
              loading={isLoadingCreateStarter || isLoadingAllVehicleDetails}
              label="Starter/s"
              placeholder="E.g: Self Start"
              error={
                touched?.technicalFactor?.starter
                  ? errors?.technicalFactor?.starter
                  : ''
              }
              value={values?.technicalFactor?.starter || []}
              onChange={(newTags, actionMeta) => {
                setFieldTouched('technicalFactor.starter');
                setFieldValue(
                  'technicalFactor.starter',
                  newTags.map((netTag) => netTag.value)
                );
                if (actionMeta.action === 'create-option') {
                  createStarter({
                    starter: newTags[newTags.length - 1]?.value,
                  });
                }
              }}
              options={allVehicleDetails?.starter?.map((starter, i) => ({
                value: starter.starter,
                label: starter.starter,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.starter');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateBattery}
              label="Battery (V)"
              placeholder="E.g: Deep Cycle Battery"
              error={
                touched?.technicalFactor?.battery
                  ? errors?.technicalFactor?.battery
                  : ''
              }
              value={values?.technicalFactor?.battery}
              onChange={(selectedBattery, actionMeta) => {
                setFieldTouched('technicalFactor.battery');
                setFieldValue('technicalFactor.battery', selectedBattery.value);
                if (
                  selectedBattery.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createBattery({
                    battery: selectedBattery.value,
                  });
                }
              }}
              options={allVehicleDetails?.battery?.map((battery) => ({
                value: battery.battery,
                label: battery.battery,
              }))}
              onBlur={() => {
                setFieldTouched('technicalFactor.battery');
              }}
            />
          </div>

          {values.fuelType === 'ELECTRIC' && (
            <div className="three-col-sm">
              <RadioBox
                title="Low battery indicator"
                name="technicalFactor.lowBatteryIndicator"
                values={[
                  { label: 'Available', value: true },
                  { label: 'Not available', value: false },
                ]}
                onChange={(e) => {
                  setFieldValue(
                    'technicalFactor.lowBatteryIndicator',
                    e.target.value
                  );
                }}
              ></RadioBox>
            </div>
          )}
        </div>
      </div>
      <div className="form-group">
        <h2 className="h6">Dimension</h2>
        <div className="row-sm">
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateWheelBase}
              label="Wheel Base"
              placeholder="E.g:	1,335"
              error={
                touched?.dimensionalFactor?.wheelBase
                  ? errors?.dimensionalFactor?.wheelBase
                  : ''
              }
              value={
                values?.dimensionalFactor?.wheelBase !== ''
                  ? Number(values?.dimensionalFactor?.wheelBase)
                  : ''
              }
              onChange={(selectedWheelBase, actionMeta) => {
                setFieldTouched('dimensionalFactor.wheelBase');
                const convertedWheelBase = lengthConverter(
                  Number(selectedWheelBase.value),
                  values.wheelBaseUnit || 'mm',
                  'mm'
                ).toString();
                setFieldValue('wheelBaseUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.wheelBase',
                  convertedWheelBase
                );

                if (
                  selectedWheelBase.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createWheelBase({
                    wheelBase: lengthConverter(
                      Number(selectedWheelBase.value),
                      values.wheelBaseUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.wheelBase?.map((wheelBase) => ({
                value: wheelBase.wheelBase,
                label: wheelBase.wheelBase,
              }))}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.wheelBase');
              }}
            />
            <Unit
              name="wheelBaseUnit"
              onChange={(e) => {
                setFieldValue('wheelBaseUnit', e.target.value);
                createWheelBase({
                  wheelBase: lengthConverter(
                    Number(values?.dimensionalFactor?.wheelBase),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateOverallWidth
              }
              label="Overall Width(mm)"
              placeholder="E.g: 800"
              error={
                touched?.dimensionalFactor?.overallWidth
                  ? errors?.dimensionalFactor?.overallWidth
                  : ''
              }
              value={
                values?.dimensionalFactor?.overallWidth !== ''
                  ? Number(values?.dimensionalFactor?.overallWidth)
                  : ''
              }
              onChange={(selectedOverallWidth, actionMeta) => {
                setFieldTouched('dimensionalFactor.overallWidth');
                const convertedOverallWidth = lengthConverter(
                  Number(selectedOverallWidth.value),
                  values.overalWidthUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('overalWidthUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.overallWidth',
                  convertedOverallWidth
                );
                if (
                  selectedOverallWidth.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createOverallWidth({
                    overallWidth: lengthConverter(
                      Number(selectedOverallWidth.value),
                      values.overalWidthUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.overallWidth?.map((overallWidth) => ({
                value: overallWidth.overallWidth,
                label: overallWidth.overallWidth,
              }))}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.overallWidth');
              }}
            />
            <Unit
              name="overalWidthUnit"
              onChange={(e) => {
                setFieldValue('overalWidthUnit', e.target.value);
                createOverallWidth({
                  overallWidth: lengthConverter(
                    Number(values?.dimensionalFactor?.overallWidth),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateOverallLength
              }
              label="Overall Length(mm)"
              placeholder="E.g: 1,070"
              error={
                touched?.dimensionalFactor?.overallLength
                  ? errors?.dimensionalFactor?.overallLength
                  : ''
              }
              value={
                values?.dimensionalFactor?.overallLength !== ''
                  ? Number(values?.dimensionalFactor?.overallLength)
                  : ''
              }
              onChange={(selectedOverallLength, actionMeta) => {
                setFieldTouched('dimensionalFactor.overallLength');
                const convertedOverallLength = lengthConverter(
                  Number(selectedOverallLength.value),
                  values.overalLengthUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('overalLengthUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.overallLength',
                  convertedOverallLength
                );
                if (
                  selectedOverallLength.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createOverallLength({
                    overallLength: lengthConverter(
                      Number(selectedOverallLength.value),
                      values.overalLengthUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.overallLength?.map(
                (overallLength) => ({
                  value: overallLength.overallLength,
                  label: overallLength.overallLength,
                })
              )}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.overallLength');
              }}
            />
            <Unit
              name="overalLengthUnit"
              onChange={(e) => {
                setFieldValue('overalLengthUnit', e.target.value);
                createOverallLength({
                  overallLength: lengthConverter(
                    Number(values?.dimensionalFactor?.overallLength),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateOverallHeight
              }
              label="Overall Height(mm)"
              placeholder="E.g: 2,020 "
              error={
                touched?.dimensionalFactor?.overallHeight
                  ? errors?.dimensionalFactor?.overallHeight
                  : ''
              }
              value={
                values?.dimensionalFactor?.overallHeight !== ''
                  ? Number(values?.dimensionalFactor?.overallHeight)
                  : ''
              }
              onChange={(selectedOverallHeight, actionMeta) => {
                setFieldTouched('dimensionalFactor.overallHeight');
                const convertedOverallHeight = lengthConverter(
                  Number(selectedOverallHeight.value),
                  values.overalHeightUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('overalHeightUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.overallHeight',
                  convertedOverallHeight
                );
                if (
                  selectedOverallHeight.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createOverallHeight({
                    overallHeight: lengthConverter(
                      Number(selectedOverallHeight.value),
                      values.overalHeightUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.overallHeight?.map(
                (overallHeight) => ({
                  value: overallHeight.overallHeight,
                  label: overallHeight.overallHeight,
                })
              )}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.overallHeight');
              }}
            />
            <Unit
              name="overalHeightUnit"
              onChange={(e) => {
                setFieldValue('overalHeightUnit', e.target.value);
                createOverallHeight({
                  overallHeight: lengthConverter(
                    Number(values?.dimensionalFactor?.overallHeight),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateGroundClearance
              }
              label="Ground Clearance(mm)"
              placeholder="E.g: 170"
              error={
                touched?.dimensionalFactor?.groundClearance
                  ? errors?.dimensionalFactor?.groundClearance
                  : ''
              }
              value={
                values?.dimensionalFactor?.groundClearance !== ''
                  ? Number(values?.dimensionalFactor?.groundClearance)
                  : ''
              }
              onChange={(selectedGroundClearance, actionMeta) => {
                setFieldTouched('dimensionalFactor.groundClearance');
                const convertedGroundClearance = lengthConverter(
                  Number(selectedGroundClearance.value),
                  values.groundClearanceUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('groundClearanceUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.groundClearance',
                  convertedGroundClearance
                );
                if (
                  selectedGroundClearance.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createGroundClearance({
                    groundClearance: lengthConverter(
                      Number(selectedGroundClearance.value),
                      values.groundClearanceUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.groundClearance?.map(
                (groundClearance) => ({
                  value: groundClearance.groundClearance,
                  label: groundClearance.groundClearance,
                })
              )}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.groundClearance');
              }}
            />
            <Unit
              name="groundClearanceUnit"
              onChange={(e) => {
                setFieldValue('groundClearanceUnit', e.target.value);
                createGroundClearance({
                  groundClearance: lengthConverter(
                    Number(values?.dimensionalFactor?.groundClearance),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>

          {values.vehicleType === 'SCOOTER' && (
            <div className="three-col-sm unit-converter-container">
              <ReactSelect
                required
                loading={isLoadingAllVehicleDetails || isLoadingCreateBootSpace}
                label="Boot Space(liters)"
                placeholder="E.g: 375"
                error={
                  touched?.dimensionalFactor?.bootSpace
                    ? errors?.dimensionalFactor?.bootSpace
                    : ''
                }
                value={
                  Number(values?.dimensionalFactor?.bootSpace)
                    ? Number(values?.dimensionalFactor?.bootSpace)
                    : ''
                }
                onChange={(selectedBootSpace, actionMeta) => {
                  setFieldTouched('dimensionalFactor.bootSpace');
                  const convertedBootSpace = volumeConverter(
                    Number(selectedBootSpace.value),
                    values.bootSpaceUnit || 'l',
                    'l'
                  ).toString();

                  setFieldValue('bootSpaceUnit', 'l');
                  setFieldValue(
                    'dimensionalFactor.bootSpace',
                    convertedBootSpace
                  );
                  if (
                    selectedBootSpace.value &&
                    actionMeta.action === 'create-option'
                  ) {
                    createBootSpace({
                      bootSpace: volumeConverter(
                        Number(selectedBootSpace.value),
                        values.bootSpaceUnit || 'l',
                        'l'
                      ).toString(),
                    });
                  }
                }}
                options={allVehicleDetails?.bootSpace?.map((bootSpace) => ({
                  value: bootSpace.bootSpace,
                  label: bootSpace.bootSpace,
                }))}
                onBlur={() => {
                  setFieldTouched('dimensionalFactor.bootSpace');
                }}
              />
              <Unit
                name="bootSpaceUnit"
                onChange={(e) => {
                  setFieldValue('bootSpaceUnit', e.target.value);
                  createBootSpace({
                    bootSpace: volumeConverter(
                      Number(values?.dimensionalFactor?.bootSpace),
                      e.target.value || 'l',
                      'l'
                    ).toString(),
                  });
                }}
              >
                {['l', 'gal'].map((accessLevel, i) => (
                  <option key={i} value={accessLevel}>
                    {accessLevel}
                  </option>
                ))}
              </Unit>
            </div>
          )}

          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateKerbWeight}
              label="Kerb Weight(Kgs)"
              placeholder="E.g: 138 "
              error={
                touched?.dimensionalFactor?.kerbWeight
                  ? errors?.dimensionalFactor?.kerbWeight
                  : ''
              }
              value={
                values?.dimensionalFactor?.kerbWeight !== ''
                  ? Number(values?.dimensionalFactor?.kerbWeight)
                  : ''
              }
              onChange={(selectedKerbWeight, actionMeta) => {
                setFieldTouched('dimensionalFactor.kerbWeight');
                const ConvertedKerbWeight = massConverter(
                  Number(selectedKerbWeight.value),
                  values.kerbWeightUnit || 'kg',
                  'kg'
                ).toString();

                setFieldValue('kerbWeightUnit', 'kg');
                setFieldValue(
                  'dimensionalFactor.kerbWeight',
                  ConvertedKerbWeight
                );
                if (
                  selectedKerbWeight.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createKerbWeight({
                    kerbWeight: massConverter(
                      Number(selectedKerbWeight.value),
                      values.kerbWeightUnit || 'kg',
                      'kg'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.kerbWeight?.map((kerbWeight) => ({
                value: kerbWeight.kerbWeight,
                label: kerbWeight.kerbWeight,
              }))}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.kerbWeight');
              }}
            />

            <Unit
              name="kerbWeightUnit"
              onChange={(e) => {
                setFieldValue('kerbWeightUnit', e.target.value);
                createKerbWeight({
                  kerbWeight: massConverter(
                    Number(values?.dimensionalFactor?.kerbWeight),
                    e.target.value || 'kg',
                    'kg'
                  ).toString(),
                });
              }}
            >
              {['kg', 'g'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateDoddleHeight
              }
              label="Seat Height(mm)" // ***note Seat Height is changed to Seat Height
              placeholder="E.g: 180"
              error={
                touched?.dimensionalFactor?.doddleHeight
                  ? errors?.dimensionalFactor?.doddleHeight
                  : ''
              }
              value={
                values?.dimensionalFactor?.doddleHeight !== ''
                  ? Number(values?.dimensionalFactor?.doddleHeight)
                  : ''
              }
              onChange={(selectedDoddleHeight, actionMeta) => {
                setFieldTouched('dimensionalFactor.doddleHeight');
                const convertedDoodleHeight = lengthConverter(
                  Number(selectedDoddleHeight.value),
                  values.doddleHeightUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('doddleHeightUnit', 'mm');
                setFieldValue(
                  'dimensionalFactor.doddleHeight',
                  convertedDoodleHeight
                );
                if (
                  selectedDoddleHeight.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createDoddleHeight({
                    doddleHeight: lengthConverter(
                      Number(selectedDoddleHeight.value),
                      values.doddleHeightUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.doddleHeight?.map((doddleHeight) => ({
                value: doddleHeight.doddleHeight,
                label: doddleHeight.doddleHeight,
              }))}
              onBlur={() => {
                setFieldTouched('dimensionalFactor.doddleHeight');
              }}
            />
            <Unit
              name="doddleHeightUnit"
              onChange={(e) => {
                setFieldValue('doddleHeightUnit', e.target.value);
                createDoddleHeight({
                  doddleHeight: lengthConverter(
                    Number(values?.dimensionalFactor?.doddleHeight),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>
        </div>
      </div>

      <div className="form-group">
        <h2 className="h6">Engine</h2>
        <div className="row-sm">
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateDisplacement
              }
              label="Displacement"
              placeholder="E.g: 150"
              error={
                touched?.engineFactor?.displacement
                  ? errors?.engineFactor?.displacement
                  : ''
              }
              value={
                Number(values?.engineFactor?.displacement)
                  ? Number(values?.engineFactor?.displacement)
                  : ''
              }
              onChange={(selecteDDisplacement, actionMeta) => {
                setFieldTouched('engineFactor.displacement');

                setFieldValue(
                  'engineFactor.displacement',
                  selecteDDisplacement.value
                );
                if (
                  selecteDDisplacement.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createDisplacement({
                    displacement: selecteDDisplacement.value,
                  });
                }
              }}
              options={allVehicleDetails?.displacement?.map((displacement) => ({
                value: displacement.displacement,
                label: displacement.displacement,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.displacement');
              }}
            />
          </div>
          {/* <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFuelEfficiency
              }
              label="Fuel Efficiency(mpg)"
              placeholder="E.g: 47 "
              error={
                touched?.engineFactor?.fuelEfficiency
                  ? errors?.engineFactor?.fuelEfficiency
                  : ''
              }
              value={
                Number(values?.engineFactor?.fuelEfficiency)
                  ? Number(values?.engineFactor?.fuelEfficiency)
                  : ''
              }
              onChange={(selecteDFuelEfficiency, actionMeta) => {
                setFieldTouched('engineFactor.fuelEfficiency');

                setFieldValue(
                  'engineFactor.fuelEfficiency',
                  selecteDFuelEfficiency.value
                );
                if (
                  selecteDFuelEfficiency.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFuelEfficiency({
                    fuelEfficiency: selecteDFuelEfficiency.value,
                  });
                }
              }}
              options={allVehicleDetails?.fuelEfficiency?.map(
                (fuelEfficiency) => ({
                  value: fuelEfficiency.fuelEfficiency,
                  label: fuelEfficiency.fuelEfficiency,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.fuelEfficiency');
              }}
            />
          </div> */}
          <div className="three-col-sm">
            <ReactSelect
              isMulti={true}
              required
              loading={isLoadingCreateEngineType || isLoadingAllVehicleDetails}
              label="Engine Type"
              placeholder="E.g: 4-Stroke"
              error={
                touched?.engineFactor?.engineType
                  ? errors?.engineFactor?.engineType
                  : ''
              }
              value={values?.engineFactor?.engineType || []}
              onChange={(newTags, actionMeta) => {
                setFieldTouched('engineFactor.engineType');
                setFieldValue(
                  'engineFactor.engineType',
                  newTags.map((netTag) => netTag.value)
                );
                if (actionMeta.action === 'create-option') {
                  createEngineType({
                    engineType: newTags[newTags.length - 1]?.value,
                  });
                }
              }}
              options={allVehicleDetails?.engineType?.map((engineType, i) => ({
                value: engineType.engineType,
                label: engineType.engineType,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.engineType');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateNoOfCylinder
              }
              label="No of Cylinders"
              placeholder="E.g: 4"
              error={
                touched?.engineFactor?.noOfCylinder
                  ? errors?.engineFactor?.noOfCylinder
                  : ''
              }
              value={Number(values?.engineFactor?.noOfCylinder)}
              onChange={(selectedNoOfCylinder, actionMeta) => {
                setFieldTouched('engineFactor.noOfCylinder');

                setFieldValue(
                  'engineFactor.noOfCylinder',
                  selectedNoOfCylinder.value
                );
                if (
                  selectedNoOfCylinder.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createNoOfCylinder({
                    noOfCylinder: selectedNoOfCylinder.value,
                  });
                }
              }}
              options={allVehicleDetails?.noOfCylender?.map((noOfCylinder) => ({
                value: noOfCylinder.noOfCylinder,
                label: noOfCylinder.noOfCylinder,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.noOfCylinder');
              }}
            />
          </div>

          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateValvesPerCylinder
              }
              label="Valves Per Cylinder"
              placeholder="E.g: 4 "
              error={
                touched?.engineFactor?.valvesPerCylinder
                  ? errors?.engineFactor?.valvesPerCylinder
                  : ''
              }
              value={Number(values?.engineFactor?.valvesPerCylinder)}
              onChange={(selecteDValvesPerCylinder, actionMeta) => {
                setFieldTouched('engineFactor.valvesPerCylinder');

                setFieldValue(
                  'engineFactor.valvesPerCylinder',
                  selecteDValvesPerCylinder.value
                );
                if (
                  selecteDValvesPerCylinder.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createValvesPerCylinder({
                    valvesPerCylinder: selecteDValvesPerCylinder.value,
                  });
                }
              }}
              options={allVehicleDetails?.valvesPerCylinder?.map(
                (valvesPerCylinder) => ({
                  value: valvesPerCylinder.valvesPerCylinder,
                  label: valvesPerCylinder.valvesPerCylinder,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.valvesPerCylinder');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateValveConfiguration
              }
              label="Valve configuration/Valve Train"
              placeholder="E.g: Direct Acting"
              error={
                touched?.engineFactor?.valveConfiguration
                  ? errors?.engineFactor?.valveConfiguration
                  : ''
              }
              value={values?.engineFactor?.valveConfiguration}
              onChange={(selecteDValveConfiguration, actionMeta) => {
                setFieldTouched('engineFactor.valveConfiguration');
                setFieldValue(
                  'engineFactor.valveConfiguration',
                  selecteDValveConfiguration.value
                );
                if (
                  selecteDValveConfiguration.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createValveConfiguration({
                    valveConfiguration: selecteDValveConfiguration.value,
                  });
                }
              }}
              options={allVehicleDetails?.valvesConfiguration?.map(
                (valveConfiguration) => ({
                  value: valveConfiguration.valveConfiguration,
                  label: valveConfiguration.valveConfiguration,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.valveConfiguration');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFuelSupplySystem
              }
              label="Fuel Supply System"
              placeholder="E.g: Carburetor"
              error={
                touched?.engineFactor?.fuelSupplySystem
                  ? errors?.engineFactor?.fuelSupplySystem
                  : ''
              }
              value={values?.engineFactor?.fuelSupplySystem}
              onChange={(selecteDFuelSupplySystem, actionMeta) => {
                setFieldTouched('engineFactor.fuelSupplySystem');
                setFieldValue(
                  'engineFactor.fuelSupplySystem',
                  selecteDFuelSupplySystem.value
                );
                if (
                  selecteDFuelSupplySystem.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFuelSupplySystem({
                    fuelSupplySystem: selecteDFuelSupplySystem.value,
                  });
                }
              }}
              options={allVehicleDetails?.fuelSupply?.map(
                (fuelSupplySystem) => ({
                  value: fuelSupplySystem.fuelSupplySystem,
                  label: fuelSupplySystem.fuelSupplySystem,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.fuelSupplySystem');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateMaximumPower
              }
              label="Maximum Power(RPM)"
              placeholder="E.g: 13.6kW(18.5PS)/10000RPM"
              error={
                touched?.engineFactor?.maximumPower
                  ? errors?.engineFactor?.maximumPower
                  : ''
              }
              value={values?.engineFactor?.maximumPower}
              onChange={(selecteDMaximumPower, actionMeta) => {
                setFieldTouched('engineFactor.maximumPower');

                setFieldValue(
                  'engineFactor.maximumPower',
                  selecteDMaximumPower.value
                );
                if (
                  selecteDMaximumPower.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createMaximumPower({
                    maximumPower: selecteDMaximumPower.value,
                  });
                }
              }}
              options={allVehicleDetails?.maximumPower?.map((maximumPower) => ({
                value: maximumPower.maximumPower,
                label: maximumPower.maximumPower,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.maximumPower');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateMaximumTorque
              }
              label="Maximum Torque(RPM)"
              placeholder="E.g: 13.9N.m(1.4kgf.m)/8500RPM"
              error={
                touched?.engineFactor?.maximumTorque
                  ? errors?.engineFactor?.maximumTorque
                  : ''
              }
              value={values?.engineFactor?.maximumTorque}
              onChange={(selecteDMaximumTorque, actionMeta) => {
                setFieldTouched('engineFactor.maximumTorque');

                setFieldValue(
                  'engineFactor.maximumTorque',
                  selecteDMaximumTorque.value
                );
                if (
                  selecteDMaximumTorque.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createMaximumTorque({
                    maximumTorque: selecteDMaximumTorque.value,
                  });
                }
              }}
              options={allVehicleDetails?.maximumTorque?.map(
                (maximumTorque) => ({
                  value: maximumTorque.maximumTorque,
                  label: maximumTorque.maximumTorque,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.maximumTorque');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateLubrication}
              label="Lubrication"
              placeholder="E.g: Grease"
              error={
                touched?.engineFactor?.lubrication
                  ? errors?.engineFactor?.lubrication
                  : ''
              }
              value={values?.engineFactor?.lubrication}
              onChange={(selecteDLubrication, actionMeta) => {
                setFieldTouched('engineFactor.lubrication');

                setFieldValue(
                  'engineFactor.lubrication',
                  selecteDLubrication.value
                );
                if (
                  selecteDLubrication.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createLubrication({
                    lubrication: selecteDLubrication.value,
                  });
                }
              }}
              options={allVehicleDetails?.lubrication?.map((lubrication) => ({
                value: lubrication.lubrication,
                label: lubrication.lubrication,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.lubrication');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateEngineOil}
              label="Engine Oil"
              placeholder="E.g: Synthetic Oil"
              error={
                touched?.engineFactor?.engineOil
                  ? errors?.engineFactor?.engineOil
                  : ''
              }
              value={values?.engineFactor?.engineOil}
              onChange={(selecteDEngineOil, actionMeta) => {
                setFieldTouched('engineFactor.engineOil');

                setFieldValue(
                  'engineFactor.engineOil',
                  selecteDEngineOil.value
                );
                if (
                  selecteDEngineOil.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createEngineOil({
                    engineOil: selecteDEngineOil.value,
                  });
                }
              }}
              options={allVehicleDetails?.engineOil?.map((engineOil) => ({
                value: engineOil.engineOil,
                label: engineOil.engineOil,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.engineOil');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateAirCleaner}
              label="Air Cleaner"
              placeholder="E.g: Mechanical Filters"
              error={
                touched?.engineFactor?.airCleaner
                  ? errors?.engineFactor?.airCleaner
                  : ''
              }
              value={values?.engineFactor?.airCleaner}
              onChange={(selecteDAirCleaner, actionMeta) => {
                setFieldTouched('engineFactor.airCleaner');
                setFieldValue(
                  'engineFactor.airCleaner',
                  selecteDAirCleaner.value
                );
                if (
                  selecteDAirCleaner.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createAirCleaner({
                    airCleaner: selecteDAirCleaner.value,
                  });
                }
              }}
              options={allVehicleDetails?.airCleaner?.map((airCleaner) => ({
                value: airCleaner.airCleaner,
                label: airCleaner.airCleaner,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.airCleaner');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateBoreXStroke}
              label="Bore X Stroke"
              placeholder="E.g: 58 X 47"
              error={
                touched?.engineFactor?.boreXStroke
                  ? errors?.engineFactor?.boreXStroke
                  : ''
              }
              value={values?.engineFactor?.boreXStroke}
              onChange={(selecteDBoreXStroke, actionMeta) => {
                setFieldTouched('engineFactor.boreXStroke');

                setFieldValue(
                  'engineFactor.boreXStroke',
                  selecteDBoreXStroke.value
                );
                if (
                  selecteDBoreXStroke.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createBoreXStroke({
                    boreXStroke: selecteDBoreXStroke.value,
                  });
                }
              }}
              options={allVehicleDetails?.boreXStoke?.map((boreXStroke) => ({
                value: boreXStroke.boreXStroke,
                label: boreXStroke.boreXStroke,
              }))}
              onBlur={() => {
                setFieldTouched('engineFactor.boreXStroke');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateCompressionRatio
              }
              label="Compression Ratio"
              placeholder="E.g: 9.5:1"
              error={
                touched?.engineFactor?.compressionRatio
                  ? errors?.engineFactor?.compressionRatio
                  : ''
              }
              value={values?.engineFactor?.compressionRatio}
              onChange={(selecteDCompressionRatio, actionMeta) => {
                setFieldTouched('engineFactor.compressionRatio');

                setFieldValue(
                  'engineFactor.compressionRatio',
                  selecteDCompressionRatio.value
                );
                if (
                  selecteDCompressionRatio.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createCompressionRatio({
                    compressionRatio: selecteDCompressionRatio.value,
                  });
                }
              }}
              options={allVehicleDetails?.compressionRatio?.map(
                (compressionRatio) => ({
                  value: compressionRatio.compressionRatio,
                  label: compressionRatio.compressionRatio,
                })
              )}
              onBlur={() => {
                setFieldTouched('engineFactor.compressionRatio');
              }}
            />
          </div>

          {values.fuelType === 'ELECTRIC' && (
            <div className="three-col-sm">
              <ReactSelect
                required
                loading={
                  isLoadingAllVehicleDetails || isLoadingCreateMotorPower
                }
                label="Motor Power"
                placeholder="E.g: Motor Power" // Example
                error={
                  touched?.engineFactor?.motorPower
                    ? errors?.engineFactor?.motorPower
                    : ''
                }
                // converted to string because the option required string
                value={`${values.engineFactor.motorPower}`}
                onChange={(motorPower, actionMeta) => {
                  setFieldTouched('engineFactor.motorPower');

                  setFieldValue('engineFactor.motorPower', motorPower.value);
                  if (
                    motorPower.value &&
                    actionMeta.action === 'create-option'
                  ) {
                    createMotorPower({
                      motorPower: motorPower.value,
                    });
                  }
                }}
                options={allVehicleDetails?.motorPower?.map((motorPower) => ({
                  value: `${motorPower.motorPower}`,
                  label: `${motorPower.motorPower}`,
                }))}
                onBlur={() => {
                  setFieldTouched('engineFactor.motorPower');
                }}
              />
            </div>
          )}
        </div>
      </div>
      <div className="form-group">
        <h2 className="h6">Wheels and Tyres</h2>
        <div className="row-sm">
          {/* <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateWheelType}
              label="Wheel Type"
              placeholder="E.g: Tubeless "
              error={
                touched?.wheelTyreFactor?.wheelType
                  ? errors?.wheelTyreFactor?.wheelType
                  : ''
              }
              value={values?.wheelTyreFactor?.wheelType}
              onChange={(selectedWheelType, actionMeta) => {
                setFieldTouched('wheelTyreFactor.wheelType');

                setFieldValue(
                  'wheelTyreFactor.wheelType',
                  selectedWheelType.value
                );
                if (
                  selectedWheelType.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createWheelType({
                    wheelType: selectedWheelType.value,
                  });
                }
              }}
              options={allVehicleDetails?.wheelType?.map((wheelType) => ({
                value: wheelType.wheelType,
                label: wheelType.wheelType,
              }))}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.wheelType');
              }}
            />
          </div> */}
          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontWheelSize
              }
              label="Front Wheel Size(mm)"
              placeholder="E.g: 19 "
              error={
                touched?.wheelTyreFactor?.frontWheelSize
                  ? errors?.wheelTyreFactor?.frontWheelSize
                  : ''
              }
              value={
                values?.wheelTyreFactor?.frontWheelSize !== ''
                  ? Number(values?.wheelTyreFactor?.frontWheelSize)
                  : ''
              }
              onChange={(selectedFrontWheelSize, actionMeta) => {
                setFieldTouched('wheelTyreFactor.frontWheelSize');
                const convertedFrontWheelSize = lengthConverter(
                  Number(selectedFrontWheelSize.value),
                  values.frontWheelSizeUnit || 'mm',
                  'mm'
                ).toString();
                setFieldValue('frontWheelSizeUnit', 'mm');

                setFieldValue(
                  'wheelTyreFactor.frontWheelSize',
                  convertedFrontWheelSize
                );

                if (
                  selectedFrontWheelSize.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontWheelSize({
                    frontWheelSize: lengthConverter(
                      Number(selectedFrontWheelSize.value),
                      values.frontWheelSizeUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.frontWheelSize?.map(
                (frontWheelSize) => ({
                  value: frontWheelSize.frontWheelSize,
                  label: frontWheelSize.frontWheelSize,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.frontWheelSize');
              }}
            />
            <Unit
              name="frontWheelSizeUnit"
              onChange={(e) => {
                setFieldValue('frontWheelSizeUnit', e.target.value);
                createFrontWheelSize({
                  frontWheelSize: lengthConverter(
                    Number(values?.wheelTyreFactor?.frontWheelSize),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
            {/* <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontWheelSize
              }
              label="Front Wheel Size"
              placeholder="E.g:	19"
              error={
                touched?.wheelTyreFactor?.frontWheelSize
                  ? errors?.wheelTyreFactor?.frontWheelSize
                  : ''
              }
              value={values?.wheelTyreFactor?.frontWheelSize}
              onChange={(selectedWheelBase, actionMeta) => {
                setFieldTouched('wheelTyreFactor.frontWheelSize');
                setFieldValue(
                  'wheelTyreFactor.frontWheelSize',
                  lengthConverter(
                    Number(selectedWheelBase.value),
                    values.frontWheelSizeUnit || 'mm',
                    'mm'
                  ).toString()
                );

                setFieldValue('frontWheelSizeUnit', 'mm');
                if (
                  selectedWheelBase.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontWheelSize({
                    frontWheelSize: lengthConverter(
                      Number(selectedWheelBase.value),
                      values.frontWheelSizeUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.frontWheelSize?.map(
                (frontWheelSize) => ({
                  value: frontWheelSize.frontWheelSize,
                  label: frontWheelSize.frontWheelSize,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.frontWheelSize');
              }}
            />
            <Unit
              name="frontWheelSizeUnit"
              onChange={(e) => {
                setFieldValue('frontWheelSizeUnit', e.target.value);
                createFrontWheelSize({
                  frontWheelSize: lengthConverter(
                    Number(values?.wheelTyreFactor?.frontWheelSize),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit> */}
          </div>

          <div className="three-col-sm unit-converter-container">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateRearWheelSize
              }
              label="Rear Wheel Size(mm)"
              placeholder="E.g: 19 "
              error={
                touched?.wheelTyreFactor?.rearWheelSize
                  ? errors?.wheelTyreFactor?.rearWheelSize
                  : ''
              }
              value={
                values?.wheelTyreFactor?.rearWheelSize !== ''
                  ? Number(values?.wheelTyreFactor?.rearWheelSize)
                  : ''
              }
              onChange={(selectedRearWheelSize, actionMeta) => {
                setFieldTouched('wheelTyreFactor.rearWheelSize');
                const convertedRearWheelSize = lengthConverter(
                  Number(selectedRearWheelSize.value),
                  values.rearWheelSizeUnit || 'mm',
                  'mm'
                ).toString();

                setFieldValue('rearWheelSizeUnit', 'mm');
                setFieldValue(
                  'wheelTyreFactor.rearWheelSize',
                  convertedRearWheelSize
                );
                if (
                  selectedRearWheelSize.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createRearWheelSize({
                    rearWheelSize: lengthConverter(
                      Number(selectedRearWheelSize.value),
                      values.rearWheelSizeUnit || 'mm',
                      'mm'
                    ).toString(),
                  });
                }
              }}
              options={allVehicleDetails?.rearWheelSize?.map(
                (rearWheelSize) => ({
                  value: rearWheelSize.rearWheelSize,
                  label: rearWheelSize.rearWheelSize,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.rearWheelSize');
              }}
            />
            <Unit
              name="rearWheelSizeUnit"
              onChange={(e) => {
                setFieldValue('rearWheelSizeUnit', e.target.value);
                createRearWheelSize({
                  rearWheelSize: lengthConverter(
                    Number(values?.wheelTyreFactor?.rearWheelSize),
                    e.target.value || 'mm',
                    'mm'
                  ).toString(),
                });
              }}
            >
              {['mm', 'cm'].map((accessLevel, i) => (
                <option key={i} value={accessLevel}>
                  {accessLevel}
                </option>
              ))}
            </Unit>
          </div>

          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateRearWheelType
              }
              label="Rear Wheel Type"
              placeholder="E.g: Tubeless Tyre"
              error={
                touched?.wheelTyreFactor?.rearWheelType
                  ? errors?.wheelTyreFactor?.rearWheelType
                  : ''
              }
              value={values?.wheelTyreFactor?.rearWheelType}
              onChange={(selectedRearWheelType, actionMeta) => {
                setFieldTouched('wheelTyreFactor.rearWheelType');

                setFieldValue(
                  'wheelTyreFactor.rearWheelType',
                  selectedRearWheelType.value
                );
                if (
                  selectedRearWheelType.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createRearWheelType({
                    rearWheelType: selectedRearWheelType.value,
                  });
                }
              }}
              options={allVehicleDetails?.rearWheelType?.map(
                (rearWheelType) => ({
                  value: rearWheelType.rearWheelType,
                  label: rearWheelType.rearWheelType,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.rearWheelType');
              }}
            />
          </div>
          <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontWheelType
              }
              label="Front Wheel Type "
              placeholder="E.g: Tubeless Tyre"
              error={
                touched?.wheelTyreFactor?.frontWheelType
                  ? errors?.wheelTyreFactor?.frontWheelType
                  : ''
              }
              value={values?.wheelTyreFactor?.frontWheelType}
              onChange={(selectedFrontWheelType, actionMeta) => {
                setFieldTouched('wheelTyreFactor.frontWheelType');

                setFieldValue(
                  'wheelTyreFactor.frontWheelType',
                  selectedFrontWheelType.value
                );
                if (
                  selectedFrontWheelType.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontWheelType({
                    frontWheelType: selectedFrontWheelType.value,
                  });
                }
              }}
              options={allVehicleDetails?.frontWheelType?.map(
                (frontWheelType) => ({
                  value: frontWheelType.frontWheelType,
                  label: frontWheelType.frontWheelType,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.frontWheelType');
              }}
            />
          </div>
          {/* <div className="three-col-sm">
            <ReactSelect
              required
              loading={
                isLoadingAllVehicleDetails || isLoadingCreateFrontWheelDrive
              }
              label="Front-wheel Drive"
              placeholder="E.g: NO"
              error={
                touched?.wheelTyreFactor?.frontWheelDrive
                  ? errors?.wheelTyreFactor?.frontWheelDrive
                  : ''
              }
              value={values?.wheelTyreFactor?.frontWheelDrive}
              onChange={(selectedFrontWheelDrive, actionMeta) => {
                setFieldTouched('wheelTyreFactor.frontWheelDrive');

                setFieldValue(
                  'wheelTyreFactor.frontWheelDrive',
                  selectedFrontWheelDrive.value
                );
                if (
                  selectedFrontWheelDrive.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createFrontWheelDrive({
                    frontWheelDrive: selectedFrontWheelDrive.value,
                  });
                }
              }}
              options={allVehicleDetails?.frontWheelDrive?.map(
                (frontWheelDrive) => ({
                  value: frontWheelDrive.frontWheelDrive,
                  label: frontWheelDrive.frontWheelDrive,
                })
              )}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.frontWheelDrive');
              }}
            />
          </div> */}
          {/* <div className="three-col-sm">
            <ReactSelect
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateAlloyWheel}
              label="Alloy Wheel"
              placeholder="E.g: 3 Spoke Wheels"
              error={
                touched?.wheelTyreFactor?.alloyWheel
                  ? errors?.wheelTyreFactor?.alloyWheel
                  : ''
              }
              value={values?.wheelTyreFactor?.alloyWheel}
              onChange={(selectedAlloyWheel, actionMeta) => {
                setFieldTouched('wheelTyreFactor.alloyWheel');

                setFieldValue(
                  'wheelTyreFactor.alloyWheel',
                  selectedAlloyWheel.value
                );
                if (
                  selectedAlloyWheel.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createAlloyWheel({
                    alloyWheel: selectedAlloyWheel.value,
                  });
                }
              }}
              options={allVehicleDetails?.alloyWheel?.map((alloyWheel) => ({
                value: alloyWheel.alloyWheel,
                label: alloyWheel.alloyWheel,
              }))}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.alloyWheel');
              }}
            />
          </div> */}

          <div className="three-col-sm">
            <ReactSelect
              menuPlacement="top"
              required
              loading={isLoadingAllVehicleDetails || isLoadingCreateSteelRims}
              label="Steel Rims"
              placeholder="E.g: Alloy Rims"
              error={
                touched?.wheelTyreFactor?.steelRims
                  ? errors?.wheelTyreFactor?.steelRims
                  : ''
              }
              value={values?.wheelTyreFactor?.steelRims}
              onChange={(selectedSteelRims, actionMeta) => {
                setFieldTouched('wheelTyreFactor.steelRims');

                setFieldValue(
                  'wheelTyreFactor.steelRims',
                  selectedSteelRims.value
                );
                if (
                  selectedSteelRims.value &&
                  actionMeta.action === 'create-option'
                ) {
                  createSteelRims({
                    steelRims: selectedSteelRims.value,
                  });
                }
              }}
              options={allVehicleDetails?.steelRims?.map((steelRims) => ({
                value: steelRims.steelRims,
                label: steelRims.steelRims,
              }))}
              onBlur={() => {
                setFieldTouched('wheelTyreFactor.steelRims');
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TechnicalSpecifications;
