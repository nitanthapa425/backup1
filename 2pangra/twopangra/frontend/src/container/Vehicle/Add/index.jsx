import { useState, useCallback, useRef, useEffect } from 'react';
// import { useSelector } from 'react-redux';
import { Formik, Form, setNestedObjectValues } from 'formik';
import Steps, { Step } from 'rc-steps';
import { ConnectedFocusError } from 'focus-formik-error';
import { useToasts } from 'react-toast-notifications';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';

import AdminLayout from 'layouts/Admin';

import Popup from 'components/Popup';
import Button from 'components/Button';

import { vechileCreateValidation } from 'validation/vehicleCreate.validation';
import {
  useCreateVehicleMutation,
  useReadAllVehicleDetailsQuery,
  useReadVehicleDetailsQuery,
  useSaveDraftToSourceMutation,
  useUpdateVehicleMutation,
} from 'services/api/vehicle';
import 'rc-steps/assets/index.css';
import 'rc-steps/assets/iconfont.css';

import { stepsData } from './data';
import {
  vehicleForminitialValues,
  vehicleFormUpdateInitialValues,
} from './initialValues';
import {
  useGetDraftedVehicleQuery,
  useVehicleSaveAsDraftMutation,
  useUpdateDraftVehicleMutation,
} from 'services/api/vehicleDraft';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
import {
  lengthConverter,
  massConverter,
  // massConverter,
  volumeConverter,
} from 'utils/unitConverter';
import Loading from 'components/Loading';

const FormContainer = ({ type }) => {
  const router = useRouter();
  // const { fuelCapacityUnit } = useSelector((state) => state.addVehicleUnits);
  const formikBag = useRef(null);
  const { addToast } = useToasts();

  const { data: allVehicleDetails, isLoading: isLoadingAllVehicleDetails } =
    useReadAllVehicleDetailsQuery();

  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  // for edit/update
  const [currentVehicleDetails, setCurrentVehicleDetails] = useState({});

  const [currentDraftVehicleDetails, setCurrentDraftVehicleDetails] = useState(
    {}
  );

  const [currentStep, setCurrentStep] = useState(0);
  const isLastStep = currentStep === stepsData.length - 1;
  const [openModal, setOpenModal] = useState(false);

  const handleBackClick = useCallback(
    () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null),
    [currentStep]
  );

  const [createVehicle, { isLoading, isError, error, isSuccess }] =
    useCreateVehicleMutation();
  const [
    updateVehicle,
    {
      isLoading: updating,
      isError: isUpdateError,
      // error: updateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateVehicleMutation();

  const {
    data: vehicleDetails,
    error: vehicleFetchError,
    isLoading: isLoadingVehicle,
  } = useReadVehicleDetailsQuery(router.query.id, {
    skip: type !== 'edit' || !router.query.id,
  });

  const [
    vehicleSaveAsDraft,
    {
      isLoading: savingAsDraft,
      isSuccess: isVehicleDraftSuccess,
      data: vechileDraftData,
      isError: isVehicleDraftError,
      error: vehicleDraftError,
    },
  ] = useVehicleSaveAsDraftMutation();
  const [
    updateSaveAsDraft,
    {
      isLoading: updatingAsDraft,
      isSuccess: isUpdateDraftSuccess,
      data: updateVechileDraftData,
      isError: isUpdateDraftError,
      error: updateDraftError,
    },
  ] = useUpdateDraftVehicleMutation();
  const [
    saveDraftToSource,
    {
      isLoading: savingDraftToSource,
      isSuccess: isSaveDraftSuccess,
      isError: isSaveDraftError,
      error: saveDraftError,
    },
  ] = useSaveDraftToSourceMutation();

  const {
    data: draftVehicleDetails,
    error: draftVehicleFetchError,
    isLoading: loadingDraft,
  } = useGetDraftedVehicleQuery(router.query.id, {
    skip: type !== 'draft' || !router.query.id,
  });

  useEffect(() => {
    if (vehicleDetails) {
      // const newVehicleDetails = { ...vehicleDetails };
      // delete newVehicleDetails?.createdBy;
      // delete newVehicleDetails?.updatedBy;
      // delete newVehicleDetails?.brandName;
      // delete newVehicleDetails?.brandModelName;
      // delete newVehicleDetails?.brandSubModelName;
      // delete newVehicleDetails?.vehicleName;
      // delete newVehicleDetails?.createdAt;
      // delete newVehicleDetails?.updatedAt;
      // delete newVehicleDetails?.id;
      const newVehicleDetails = vehicleFormUpdateInitialValues(vehicleDetails);
      setCurrentVehicleDetails(newVehicleDetails);
    }
  }, [JSON.stringify(vehicleDetails)]);

  useEffect(() => {
    if (draftVehicleDetails) {
      // const newVehicleDetails = { ...draftVehicleDetails };
      // delete newVehicleDetails?.createdBy;
      // delete newVehicleDetails?.updatedBy;
      // delete newVehicleDetails?.brandName;
      // delete newVehicleDetails?.brandModelName;
      // delete newVehicleDetails?.brandSubModelName;
      // delete newVehicleDetails?.vehicleName;
      // delete newVehicleDetails?.createdAt;
      // delete newVehicleDetails?.updatedAt;
      // delete newVehicleDetails?.id;
      const newVehicleDetails =
        vehicleFormUpdateInitialValues(draftVehicleDetails);
      setCurrentDraftVehicleDetails(newVehicleDetails);
    }
  }, [JSON.stringify(draftVehicleDetails)]);

  const _submitForm = (values, actions) => {
    values.dimensionalFactor.wheelBase = lengthConverter(
      Number(values.dimensionalFactor.wheelBase),
      values.wheelBaseUnit || 'mm',
      'mm'
    ).toString();

    values.dimensionalFactor.overallWidth = lengthConverter(
      Number(values.dimensionalFactor.overallWidth),
      values.overalWidthUnit || 'mm',
      'mm'
    ).toString();

    values.dimensionalFactor.overallLength = lengthConverter(
      Number(values.dimensionalFactor.overallLength),
      values.overalLengthUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.overallHeight = lengthConverter(
      Number(values.dimensionalFactor.overallHeight),
      values.overalHeightUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.groundClearance = lengthConverter(
      Number(values.dimensionalFactor.groundClearance),
      values.groundClearanceUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.doddleHeight = lengthConverter(
      Number(values.dimensionalFactor.doddleHeight),
      values.doddleHeightUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.bootSpace = volumeConverter(
      Number(values.dimensionalFactor.bootSpace),
      values.bootSpaceUnit || 'l',
      'l'
    ).toString();
    values.dimensionalFactor.kerbWeight = massConverter(
      Number(values.dimensionalFactor.kerbWeight),
      values.kerbWeightUnit || 'kg',
      'kg'
    ).toString();
    values.wheelTyreFactor.frontWheelSize = lengthConverter(
      Number(values.wheelTyreFactor.frontWheelSize),
      values.frontWheelSizeUnit || 'mm',
      'mm'
    ).toString();
    values.wheelTyreFactor.rearWheelSize = lengthConverter(
      Number(values.wheelTyreFactor.rearWheelSize),
      values.rearWheelSizeUnit || 'mm',
      'mm'
    ).toString();

    values.marketPrice = values.marketPrice.toString();
    values.basicFactor.mileage = values.basicFactor.mileage.toString();
    values.basicFactor.seatingCapacity =
      values.basicFactor.seatingCapacity.toString();
    values.basicFactor.fuelCapacity =
      values.basicFactor.fuelCapacity.toString();
    values.technicalFactor.noOfGears =
      values.technicalFactor.noOfGears.toString();
    values.engineFactor.displacement =
      values.engineFactor.displacement.toString();
    // values.engineFactor.fuelEfficiency =
    //   values.engineFactor.fuelEfficiency.toString();
    values.engineFactor.noOfCylinder =
      values.engineFactor.noOfCylinder.toString();
    values.engineFactor.valvesPerCylinder =
      values.engineFactor.valvesPerCylinder.toString();

    // values.dimensionalFactor.overallWidth = '3';
    delete values.technicalFactor.technicalVehicleType;
    delete values.dimensionalFactor.dimensionalVehicleType;
    delete values.engineFactor.engineFactorFuelType;
    delete values.wheelBaseUnit;
    delete values.overalWidthUnit;
    delete values.overalLengthUnit;
    delete values.overalHeightUnit;
    delete values.groundClearanceUnit;
    delete values.doddleHeightUnit;

    delete values.kerbWeightUnit;
    delete values.bootSpaceUnit;
    delete values.frontWheelSizeUnit;
    delete values.rearWheelSizeUnit;

    if (values.fuelType === 'PETROL') {
      delete values.optionFeature.chargingPoint;
      delete values.optionFeature.speedometer;
      delete values.optionFeature.tripMeter;
      delete values.optionFeature.passSwitch;
      delete values.optionFeature.clock;
      delete values.optionFeature.ridingModes;
      delete values.optionFeature.navigation;
      delete values.optionFeature.chargingAtHome;
      delete values.optionFeature.chargingAtChargingStation;
      delete values.technicalFactor.lowBatteryIndicator;
      values = {
        ...values,
        engineFactor: {
          ...values.engineFactor,
          motorPower: '',
        },
      };
    }

    if (values.vehicleType === 'SCOOTER') {
      values = {
        ...values,
        technicalFactor: {
          ...values.technicalFactor,
          noOfGears: '',
          clutchType: '',
          gearPattern: '',
        },
      };
    }

    if (values.vehicleType === 'BIKE') {
      values = {
        ...values,
        dimensionalFactor: {
          ...values.dimensionalFactor,
          bootSpace: '',
        },
      };
    }

    // values.dimensionalFactor.overallWidth = lengthConverter(
    //   Number(values.dimensionalFactor.overallWidth),
    //   values.overalWidthUnit || 'mm',
    //   'mm'
    // ).toString();

    if (type === 'add') {
      // values.basicFactor.fuelCapacity = volumeConverter(
      //   Number(values.basicFactor?.fuelCapacity),
      //   fuelCapacityUnit,
      //   'l'
      // ).toString();

      createVehicle(values);
    }
    if (type === 'edit') {
      updateVehicle({ ...values, id: router.query.id });
    }
    if (type === 'draft') {
      saveDraftToSource({ ...values, id: router.query.id });
    }
    actions.setSubmitting(false);
  };

  const handleSubmit = (values, actions) => {
    if (isLastStep) {
      _submitForm(values, actions);
    } else {
      setCurrentStep((prev) => prev + 1);
      actions.setTouched({});
      actions.setSubmitting(false);
    }
  };

  const handleSaveAsDraft = (values) => {
    values.dimensionalFactor.wheelBase = lengthConverter(
      Number(values.dimensionalFactor.wheelBase),
      values.wheelBaseUnit || 'mm',
      'mm'
    ).toString();

    values.dimensionalFactor.overallWidth = lengthConverter(
      Number(values.dimensionalFactor.overallWidth),
      values.overalWidthUnit || 'mm',
      'mm'
    ).toString();

    values.dimensionalFactor.overallLength = lengthConverter(
      Number(values.dimensionalFactor.overallLength),
      values.overalLengthUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.overallHeight = lengthConverter(
      Number(values.dimensionalFactor.overallHeight),
      values.overalHeightUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.groundClearance = lengthConverter(
      Number(values.dimensionalFactor.groundClearance),
      values.groundClearanceUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.doddleHeight = lengthConverter(
      Number(values.dimensionalFactor.doddleHeight),
      values.doddleHeightUnit || 'mm',
      'mm'
    ).toString();
    values.dimensionalFactor.bootSpace = volumeConverter(
      Number(values.dimensionalFactor.bootSpace),
      values.bootSpaceUnit || 'l',
      'l'
    ).toString();
    values.basicFactor.fuelCapacity = volumeConverter(
      Number(values.basicFactor.fuelCapacity),
      values.fuelCapacityUnit || 'l',
      'l'
    ).toString();
    values.dimensionalFactor.kerbWeight = massConverter(
      Number(values.dimensionalFactor.kerbWeight),
      values.kerbWeightUnit || 'kg',
      'kg'
    ).toString();
    values.wheelTyreFactor.frontWheelSize = lengthConverter(
      Number(values.wheelTyreFactor.frontWheelSize),
      values.frontWheelSizeUnit || 'mm',
      'mm'
    ).toString();
    values.wheelTyreFactor.rearWheelSize = lengthConverter(
      Number(values.wheelTyreFactor.rearWheelSize),
      values.rearWheelSizeUnit || 'mm',
      'mm'
    ).toString();

    values.marketPrice = values.marketPrice.toString();
    values.basicFactor.mileage = values.basicFactor.mileage.toString();
    values.basicFactor.seatingCapacity =
      values.basicFactor.seatingCapacity.toString();
    values.basicFactor.fuelCapacity =
      values.basicFactor.fuelCapacity.toString();
    values.technicalFactor.noOfGears =
      values.technicalFactor.noOfGears.toString();
    values.engineFactor.displacement =
      values.engineFactor.displacement.toString();
    // values.engineFactor.fuelEfficiency =
    //   values.engineFactor.fuelEfficiency.toString();
    values.engineFactor.noOfCylinder =
      values.engineFactor.noOfCylinder.toString();
    values.engineFactor.valvesPerCylinder =
      values.engineFactor.valvesPerCylinder.toString();

    delete values.technicalFactor.technicalVehicleType;
    delete values.dimensionalFactor.dimensionalVehicleType;
    delete values.engineFactor.engineFactorFuelType;
    delete values.wheelBaseUnit;
    delete values.overalWidthUnit;
    delete values.overalLengthUnit;
    delete values.overalHeightUnit;
    delete values.groundClearanceUnit;
    delete values.doddleHeightUnit;

    delete values.kerbWeightUnit;
    delete values.bootSpaceUnit;
    delete values.frontWheelSizeUnit;
    delete values.rearWheelSizeUnit;

    if (values.fuelType === 'PETROL') {
      delete values.optionFeature.chargingPoint;
      delete values.optionFeature.speedometer;
      delete values.optionFeature.tripMeter;
      delete values.optionFeature.passSwitch;
      delete values.optionFeature.clock;
      delete values.optionFeature.ridingModes;
      delete values.optionFeature.navigation;
      delete values.optionFeature.chargingAtHome;
      delete values.optionFeature.chargingAtChargingStation;
      delete values.technicalFactor.lowBatteryIndicator;
      values = {
        ...values,
        engineFactor: {
          ...values.engineFactor,
          motorPower: '',
        },
      };
    }

    if (values.vehicleType === 'SCOOTER') {
      values = {
        ...values,
        technicalFactor: {
          ...values.technicalFactor,
          noOfGears: '',
          clutchType: '',
          gearPattern: '',
        },
      };
    }

    if (values.vehicleType === 'BIKE') {
      values = {
        ...values,
        dimensionalFactor: {
          ...values.dimensionalFactor,
          bootSpace: '',
        },
      };
    }
    if (type === 'add') {
      vehicleSaveAsDraft(values);
    } else if (type === 'draft') {
      const val = {
        id: router?.query?.id,
        ...values,
      };
      updateSaveAsDraft(val);
    }
  };

  const _sucessAction = (message) => {
    formikBag.current?.resetForm();

    setCurrentStep(0);
    addToast(message, {
      appearance: 'success',
    });
  };

  useEffect(() => {
    if (isSuccess) {
      _sucessAction('New vehicle added successfully.');
    }
    if (isSaveDraftSuccess) {
      _sucessAction('Draft value has been  submitted successfully.');
      setChanged(false);
      router.push(`/vehicle/add`);
    }
  }, [isSuccess, isSaveDraftSuccess]);

  useEffect(() => {
    if (isError || isSaveDraftError) {
      error?.data?.message ||
        saveDraftError?.data?.message ||
        addToast(
          'Error occured while adding vehicle. Please try again later.',
          {
            appearance: 'error',
          }
        );
    }
  }, [isError, isSaveDraftError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      _sucessAction(
        updateSuccessData?.message || 'vehicle details updated successfully.'
      );
      setChanged(false);
      // redirect to vehicle page
      router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  useEffect(() => {
    if (isUpdateError) {
      addToast(
        'Error occurred while updating vehicle details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (vehicleFetchError) {
      addToast(
        'Error occured while fetching vehicle details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [vehicleFetchError]);

  useEffect(() => {
    if (draftVehicleFetchError) {
      addToast(
        'Error occurred while fetching drafted vehicle details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [draftVehicleFetchError]);

  useEffect(() => {
    if (isVehicleDraftSuccess) {
      _sucessAction(
        vechileDraftData?.message ||
          'Draft value submitted to vehicle list successfully'
      );
      setChanged(false);
      router.push('/vehicle/draft/view');
    }
    if (isUpdateDraftSuccess) {
      _sucessAction(
        updateVechileDraftData?.message ||
          'vehicle details updated successfully.'
      );
      setChanged(false);
      router.push('/vehicle/draft/view');
    }
  }, [isVehicleDraftSuccess, isUpdateDraftSuccess]);

  useEffect(() => {
    if (isVehicleDraftError) {
      addToast(
        vehicleDraftError?.message ||
          'Error occured while adding  vehicle draft details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
    if (isUpdateDraftError) {
      addToast(
        updateDraftError?.message ||
          'Error occured while updating vehicle draft details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isVehicleDraftError, isUpdateDraftError]);

  const _getTitle = () => {
    let title = '';
    if (type === 'add') {
      title = 'Add New Vehicle';
    } else if (type === 'edit') {
      title = 'Edit Vehicle Details';
    } else if (type === 'draft') {
      title = 'Edit Drafted Vehicle Details';
    }
    return title;
  };

  const _getInitialValues = () => {
    let initialValues = vehicleForminitialValues;
    if (type === 'edit') initialValues = currentVehicleDetails;
    else if (type === 'draft') initialValues = currentDraftVehicleDetails;
    return initialValues;
  };

  let BreadCrumbList = [];

  if (type === 'edit') {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Vehicle List',
        route: '/vehicle/view',
      },
      {
        routeName: 'Edit Vehicle',
        route: '',
      },
    ];
  }

  return (
    <AdminLayout documentTitle={_getTitle()} BreadCrumbList={BreadCrumbList}>
      {(loadingDraft || isLoadingVehicle) && <Loading />}
      <section className="stepper-section-pt-4 mt-5">
        <div className="container lg:px-10">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <h4 className="my-3 ">{_getTitle()}</h4>
          <Formik
            initialValues={_getInitialValues()}
            validationSchema={vechileCreateValidation[currentStep]}
            onSubmit={handleSubmit}
            innerRef={formikBag}
            enableReinitialize
          >
            {({
              setFieldValue,
              values,
              errors,
              touched,
              resetForm,
              validateForm,
              setTouched,
              dirty,
              setFieldTouched,
              isValid,
            }) => {
              return (
                <Form>
                  <ConnectedFocusError />
                  <Steps current={currentStep} direction="vertical">
                    {stepsData.map(
                      ({ id, title, component: StepContent, key }, index) => (
                        <Step
                          key={id + key}
                          title={
                            <span
                              className="cursor-pointer"
                              onClick={() => {
                                if (id < currentStep) {
                                  setCurrentStep(id);
                                }
                              }}
                            >
                              {title}
                            </span>
                          }
                          description={
                            <>
                              {currentStep === index && (
                                <>
                                  <StepContent
                                    values={values}
                                    setFieldValue={setFieldValue}
                                    errors={errors}
                                    touched={touched}
                                    setChanged={setChanged}
                                    dirty={dirty}
                                    allVehicleDetails={allVehicleDetails}
                                    isLoadingAllVehicleDetails={
                                      isLoadingAllVehicleDetails
                                    }
                                    setFieldTouched={setFieldTouched}
                                    isValid={isValid}
                                    type={type}
                                  />
                                  {/* <Button
                                    loading={isLoading || updating}
                                    type="submit"
                                    variant={dirty ?
                                      isValid ?
                                        'primary' :
                                        'disabled' :
                                        'disabled'
                                    }
                                  >
                                    {isLastStep ? 'Finish' : 'Next'}
                                  </Button> */}
                                  <Button
                                    loading={
                                      isLoading ||
                                      updating ||
                                      savingDraftToSource
                                    }
                                    type="submit"
                                    variant={
                                      isLastStep ? 'primary' : 'secondary'
                                    }
                                  >
                                    {isLastStep && type === 'add'
                                      ? 'Finish'
                                      : isLastStep && type === 'edit'
                                      ? 'Update'
                                      : isLastStep && type === 'draft'
                                      ? 'Finish'
                                      : 'Next'}
                                  </Button>
                                  <Button
                                    onClick={() => {
                                      setOpenModal(true);
                                    }}
                                    variant="outlined-error"
                                    type="button"
                                  >
                                    Clear
                                  </Button>
                                  {openModal && (
                                    <Popup
                                      title="Are you sure to clear all fields?"
                                      description="If you clear all fields, the data will not be saved."
                                      onOkClick={() => {
                                        resetForm();
                                        setCurrentStep(0);
                                        setOpenModal(false);
                                      }}
                                      onCancelClick={() => setOpenModal(false)}
                                      okText="Clear All"
                                      cancelText="Cancel"
                                    />
                                  )}

                                  {(type === 'add' || type === 'draft') &&
                                    values.brandId && (
                                      <Button
                                        loading={
                                          savingAsDraft || updatingAsDraft
                                        }
                                        type="button"
                                        variant="tertiary"
                                        onClick={() => {
                                          validateForm().then((errs) => {
                                            if (
                                              Object.keys(errs).length === 0
                                            ) {
                                              handleSaveAsDraft(values);
                                            } else {
                                              setTouched(
                                                setNestedObjectValues(
                                                  errs,
                                                  true
                                                )
                                              );
                                            }
                                          });
                                        }}
                                      >
                                        {type === 'draft'
                                          ? 'Update Draft'
                                          : 'Save as draft'}
                                      </Button>
                                    )}
                                  {currentStep !== 0 && (
                                    <Button
                                      type="button"
                                      disabled={isLoading}
                                      onClick={handleBackClick}
                                      variant="link"
                                    >
                                      Back
                                    </Button>
                                  )}
                                </>
                              )}
                            </>
                          }
                        />
                      )
                    )}
                  </Steps>
                </Form>
              );
            }}
          </Formik>
        </div>
      </section>
    </AdminLayout>
  );
};

FormContainer.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default FormContainer;
