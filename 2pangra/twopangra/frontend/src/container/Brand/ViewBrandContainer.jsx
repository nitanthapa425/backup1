import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';
import { SelectColumnFilter } from 'components/Table/Filter';
import {
  useDeleteBrandMutation,
  useReadBrandListQuery,
  useAddFeaturedBrandMutation,
  useRemoveFeaturedBrandMutation,
} from 'services/api/brand';
import { useToasts } from 'react-toast-notifications';

function ViewBrandContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [vehicleDataQuery, setVehicleDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteBrandMutation();
  const {
    data: vehicleData,
    isError: vehicleError,
    isFetching: isLoadingVehicle,
  } = useReadBrandListQuery(vehicleDataQuery);

  const {
    data: brandDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadBrandListQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  // Service for adding to featured brands
  const [
    addFeaturedBrand,
    {
      error: errorAddFeaturedBrand,
      isLoading: isAddingFeaturedBrand,
      isSuccess: isSuccessAddFeaturedBrand,
    },
  ] = useAddFeaturedBrandMutation();

  // Service for removing from featured brands
  const [
    removeFeaturedBrand,
    {
      error: errorRemoveFeaturedBrand,
      isLoading: isRemovingFeaturedBrand,
      isSuccess: isSuccessRemoveFeaturedBrand,
    },
  ] = useRemoveFeaturedBrandMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: 'success',
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: 'error',
    });
  };

  useEffect(() => {
    if (brandDataAll) {
      setTableDataAll(
        brandDataAll.docs.map((brand) => {
          return {
            id: brand.id,
            brandVehicleName: brand.brandVehicleName,
            brandVehicleDescription: brand.brandVehicleDescription,
          };
        })
      );
    }
  }, [brandDataAll]);

  useEffect(() => {
    if (errorAddFeaturedBrand) {
      showFailureToast(
        errorAddFeaturedBrand?.data?.message ||
          'Failed to add to featured brands'
      );
    }
    if (isSuccessAddFeaturedBrand) {
      showSuccessToast('Successfully added to featured brands');
    }
  }, [errorAddFeaturedBrand, isSuccessAddFeaturedBrand]);

  useEffect(() => {
    if (errorRemoveFeaturedBrand) {
      showFailureToast(
        errorRemoveFeaturedBrand?.data?.message ||
          'Failed to remove from featured brands'
      );
    }
    if (isSuccessRemoveFeaturedBrand) {
      showSuccessToast('Successfully removed from featured brands');
    }
  }, [errorRemoveFeaturedBrand, isSuccessRemoveFeaturedBrand]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteBrandMutation();

  const columns = useMemo(
    () => [
      {
        id: 'brandVehicleName',
        Header: 'Brand Name',
        accessor: 'brandVehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'brandVehicleDescription',
        Header: 'Brand Description',
        accessor: 'brandVehicleDescription',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'hasInHomePage',
        Header: 'Is Featured',
        accessor: 'hasInHomePage',
        Cell: ({ cell: { value } }) => (value ? 'Featured' : 'Not Featured'),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'Featured',
            value: true,
          },
          {
            label: 'Not Featured',
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setVehicleDataQuery(query + '&noRegex=hasInHomePage');
  };

  useEffect(() => {
    if (vehicleData) {
      setPageCount(vehicleData.totalPages);
      setTotalData(vehicleData.totalDocs);
      setTableData(
        vehicleData.docs.map((vehicle) => {
          return {
            id: vehicle.id,
            brandVehicleName: vehicle.brandVehicleName,
            brandVehicleDescription: vehicle.brandVehicleDescription,
            hasInHomePage: vehicle.hasInHomePage,
          };
        })
      );
    }
  }, [vehicleData]);

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Brand List',
      route: '/vehicle/brand/view',
    },
  ];

  return (
    <AdminLayout documentTitle="View Brand" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Brand/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={vehicleError}
            isLoadingData={isLoadingVehicle}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="vehicle/brand/edit"
            viewRoute="vehicle/brand/get"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: 'Add Brand', route: '/vehicle/brand/add' }}
            hasDropdownBtn={true}
            dropdownBtnName="More"
            dropdownBtnOptions={[
              {
                name: 'Add to Featured Brands',
                onClick: addFeaturedBrand,
                isLoading: isAddingFeaturedBrand,
                loadingText: 'Adding...',
                // conditionalShow: true,
                // showFor: 'hasInHomePage',
                // dontShowWhen: false,
              },
              {
                name: 'Remove from Featured Brands',
                onClick: removeFeaturedBrand,
                isLoading: isRemovingFeaturedBrand,
                loadingText: 'Removing...',
                // conditionalShow: true,
                // showFor: 'hasInHomePage',
                // dontShowWhen: false,
              },
            ]}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewBrandContainer;
