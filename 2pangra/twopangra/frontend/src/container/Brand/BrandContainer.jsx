import React, { useRef, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import { useToasts } from 'react-toast-notifications';
import PropTypes from 'prop-types';
import Link from 'next/link';

import Button from 'components/Button';
import DropZone from 'components/DropZone';
import Input from 'components/Input';
import AdminLayout from 'layouts/Admin';

import {
  useCreateBrandMutation,
  useReadBrandDetailsQuery,
  useUpdateBrandMutation,
} from 'services/api/brand';
import { brandvalidationSchema } from 'validation/brand.validation';
import Popup from 'components/Popup';
import { useRouter } from 'next/router';
import Textarea from 'components/Input/textarea';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';

const BrandContainer = ({ type }) => {
  const router = useRouter();
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [editBrandInitialValue, setEditBrandInitialValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();
  const [createBrand, { isError, isLoading, isSuccess, data }] =
    useCreateBrandMutation();

  const handleBrandLogo = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('uploadBrandImage', newFiles[0]);
    }
  };

  const handleRemoveBrandImage = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('uploadBrandImage', '');
    }
  };
  const handleCompanyImage = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('companyImage', newFiles[0]);
    }
  };

  const handleRemoveCompanyImage = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('companyImage', '');
    }
  };

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(
        <>
          New Brand added successfully.{' '}
          <Link href={`/vehicle/brand/get/${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Brand
            </a>
          </Link>
          <Link href={`/vehicle/model/add?brand=${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700">
              Add Model
            </a>
          </Link>
        </>,
        {
          appearance: 'success',
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast('Brand Name already exist.', {
        appearance: 'error',
        autoDismiss: false,
      });
    }
  }, [isError]);
  const addBrandInitialValue = {
    brandVehicleName: '',
    brandVehicleDescription: '',
    uploadBrandImage: '',
    companyImage: '',
  };
  const {
    data: brandDetails,
    error: brandFetchError,
    // isLoading: isLoadingBrand,
  } = useReadBrandDetailsQuery(router.query.id, {
    skip: type !== 'edit' || !router.query.id,
  });

  useEffect(() => {
    if (brandFetchError) {
      addToast(
        'Error occured while fetching Brand details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [brandFetchError]);

  useEffect(() => {
    if (brandDetails) {
      const newBrandDetails = {
        brandVehicleName: brandDetails?.brandVehicleName,
        brandVehicleDescription: brandDetails?.brandVehicleDescription,
        uploadBrandImage: brandDetails?.uploadBrandImage,
        companyImage: brandDetails?.companyImage,
      };

      setEditBrandInitialValue(newBrandDetails);
    }
  }, [brandDetails]);

  const [
    updateBrand,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateBrandMutation();
  useEffect(() => {
    if (isUpdateError) {
      addToast('Brand already exists.', {
        appearance: 'error',
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || 'Brand details updated successfully.',
        {
          appearance: 'success',
        }
      );
      setChanged(false);
      // redirect to model page
      // ..........redirect is left
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
      router.push(`/vehicle/brand/get/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === 'add') {
      createBrand(values, resetForm);
    }
    if (type === 'edit') {
      updateBrand({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === 'add') {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Add Brand',
        route: '/vehicle/brand/add',
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Brand List',
        route: '/vehicle/brand/view',
      },
      {
        routeName: 'Edit Brand',
        route: '',
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={type === 'add' ? 'Add New Brand' : 'Edit Brand Details'}
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === 'edit' ? editBrandInitialValue : addBrandInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={brandvalidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              {/* <button
                className="mb-3 hover:text-primary"
                onClick={() => router.back()}
              >
                Go Back
              </button> */}

              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">
                    {type === 'add' ? 'Add New Brand' : 'Edit Brand Details'}
                  </h3>
                  <div className="mb-2">
                    <Input
                      label="Brand Name"
                      name="brandVehicleName"
                      type="text"
                      placeholder="E.g: Bajaj"
                    />
                  </div>

                  <div className="mb-2">
                    <Textarea
                      label="Brand Description"
                      name="brandVehicleDescription"
                      type="text"
                      placeholder="E.g: Brand Description"
                      required={false}
                    />
                  </div>

                  <div className="mb-2">
                    <DropZone
                      label="Upload Brand Image"
                      required
                      currentFiles={
                        values.uploadBrandImage ? [values.uploadBrandImage] : []
                      }
                      setNewFiles={handleBrandLogo}
                      handleRemoveFile={handleRemoveBrandImage}
                      error={
                        touched?.uploadBrandImage
                          ? errors?.uploadBrandImage
                          : ''
                      }
                    />
                  </div>
                  <div className="mb-2">
                    <DropZone
                      label="Upload Company Image (Vehicle)"
                      required
                      currentFiles={
                        values.companyImage ? [values.companyImage] : []
                      }
                      setNewFiles={handleCompanyImage}
                      handleRemoveFile={handleRemoveCompanyImage}
                      error={touched?.companyImage ? errors?.companyImage : ''}
                    />
                  </div>
                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === 'add' ? 'Submit' : 'Update'}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

BrandContainer.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default BrandContainer;
