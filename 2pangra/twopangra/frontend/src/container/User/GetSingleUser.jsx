import React from 'react';
import Image from 'next/image';
import AdminLayout from 'layouts/Admin';
import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';
import { useReadUserProfileDetailsQuery } from 'services/api/signup';
import Link from 'next/link';
import Button from 'components/Button';
import { firstLetterCapital } from 'utils/firstLetterCapita.';

const GetSingleProfile = ({
  canEdit = true,
  list = 'User List',
  profile = 'User Profile',
}) => {
  const router = useRouter();
  // const { addToast } = useToasts();

  const {
    data: userProfile,
    // error: userProfileFetchError,
    isLoading: isLoadinguserProfile,
  } = useReadUserProfileDetailsQuery(router.query.id, {
    skip: !router.query.id,
  });

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: `${list}`,
      route: '/signup/view',
    },
    {
      routeName: `${profile}`,
      route: '',
    },
  ];
  return (
    // <AdminLayout

    // >

    // </AdminLayout>
    <AdminLayout
      documentTitle="Get User Profile"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          {/* <div className="flex justify-end">
            <Link href={`/signup/edit/${router.query.id}`}>Edit</Link>
          </div> */}

          {canEdit && (
            <div className="flex justify-end">
              <Link href={`/signup/edit/${router.query.id}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
          )}

          {isLoadinguserProfile ? (
            'Loading...'
          ) : (
            <>
              <div className="row">
                <div className="three-col">
                  <div
                  // border border-gray-200 pt-3 pb-2 px-4 rounded-md
                  // className="relative"
                  >
                    <strong>{userProfile?.userName}&nbsp;</strong>
                    {userProfile?.profileImagePath?.imageUrl ? (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${userProfile?.profileImagePath?.imageUrl}`}
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    ) : (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src="/user.JPG"
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                    rounded
                    cursor-pointer
                    transition duration-200 ease-in-out
                    transform  hover:scale-125"
                        />
                      </div>
                    )}
                    <p>{userProfile?.accessLevel}</p>
                  </div>
                </div>
                <div className="mx-3">
                  <p>
                    <strong>Email:&nbsp;</strong>
                    {/* {brandDetails?.brandVehicleDescription
                      ? brandDetails.brandVehicleDescription
                      : 'N/A'} */}
                    {userProfile?.email}
                  </p>
                  <p>
                    <strong>Full Name:</strong> &nbsp;
                    {userProfile?.fullName}
                  </p>

                  <p>
                    <strong>Gender:</strong> &nbsp;
                    {userProfile?.gender &&
                      firstLetterCapital(userProfile?.gender)}
                  </p>
                  <p>
                    <strong>Mobile Number:</strong> &nbsp;
                    {userProfile?.mobile}
                  </p>

                  <p>
                    <strong>Date of Birth:</strong> &nbsp;
                    {userProfile?.dob
                      ? new Date(userProfile?.dob).toLocaleDateString()
                      : null}
                  </p>
                  <p>
                    <strong>User Active:</strong> &nbsp;
                    {userProfile?.isActive ? 'Yes' : 'No'}
                  </p>

                  <p>
                    <strong>User Verified:</strong> &nbsp;
                    {userProfile?.verified ? 'Yes' : 'No'}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetSingleProfile;
