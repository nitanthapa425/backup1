import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
// import { Form, Formik } from 'formik';
import Head from 'next/head';

import Button from 'components/Button';
import { useToasts } from 'react-toast-notifications';
// import { useResetPasswordPostMutation } from 'services/api/resetPassword';
import { useExpirePostMutation } from 'services/api/seller';

// import { PasswordUpdateSchema } from 'validation/RestPasswordValidation';
// import PasswordInput from 'components/Input/PasswordInput';
// import Popup from 'components/Popup';

const ExpirePostContainer = () => {
  //   const [openModal, setOpenModal] = useState(false);

  const router = useRouter();
  const id = router?.query?.id;

  // const formikBag = useRef();
  const { addToast } = useToasts();
  const [createExpirePost, { isError, error, isSuccess }] =
    useExpirePostMutation();

  useEffect(() => {
    if (isSuccess) {
      // formikBag.current?.resetForm();

      addToast('Your bike has been successfully removed from the ads .', {
        appearance: 'success',
      });
      router.push('/sell/view');
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'Error occurred while removing from ads. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  return (
    <>
      <Head>
        <title>2Pangra | Expire Post</title>
      </Head>
      <div className="container">
        <div className="flex justify-center items-center min-h-screen">
          <div
            className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
            style={{ position: 'relative' }}
          >
            <h3 className="mb-3">Expire Post</h3>

            {/* <div className="row-sm">
              <PasswordInput
                label="New Password"
                name="newPassword"
                placeholder="New Password"
              ></PasswordInput>
              <PasswordInput
                label="Confirm Password"
                name="confirmPassword"
                placeholder="Confirm Password"
              ></PasswordInput>
            </div> */}
            <p>Are you sure you want to remove your bike from selling list ?</p>

            <div className="btn-holder mt-3">
              <Button
                type="button"
                onClick={() => {
                  createExpirePost(id);
                }}
              >
                {' '}
                Expire Post
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  router.push(`/vehicle-listing/${id}`);
                  // setOpenModal(true);
                }}
                // disabled={isSubmitting || !dirty}
              >
                Cancel
              </Button>

              {/* {openModal && (
                <Popup
                  title="Are you sure to clear all fields?"
                  description="If you clear all fields, the data will not be saved."
                  // onOkClick={() => {
                  //   resetForm();
                  //   setOpenModal(false);
                  // }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )} */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ExpirePostContainer;
