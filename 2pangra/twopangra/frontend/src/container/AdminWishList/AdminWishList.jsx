// import Table from 'components/Table/table';
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import {
  //   getQueryStringForTable,
  getQueryStringForWishList,
} from 'utils/getQueryStringForTable';
import //   useDeleteVehicleMutation,
//   useReadVehicleCustomQuery,
'services/api/vehicle';
import { MultiSelectFilter, SelectColumnFilter } from 'components/Table/Filter';
import {
  useDeleteWishListMutation,
  useReadAllWishListQuery,
} from 'services/api/adminCartList';
import Table from 'components/Table/table';
import { color } from 'constant/constant';
function AdminWishList() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [wishListQuery, setWishListQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataWishList,
    isError: isErrorWishList,
    isFetching: isFetchingWishList,
  } = useReadAllWishListQuery(wishListQuery);

  // console.log(wishListQuery);

  const {
    data: dataAllWishList,
    // isError: isVehicleErrorAll,
    isFetching: isFetchingAllWishList,
  } = useReadAllWishListQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (dataAllWishList) {
      setTableDataAll(
        dataAllWishList.docs.map((value) => {
          return {
            id: value.id || '-',
            bikeDriven: value.bikeDriven || '-',
            color: value.color || '-',
            isVerified: value.isVerified || '-',
            numViews: value.numViews || '-',
            price: value.price || '-',
            vehicleName: value.vehicleName || '-',
          };
        })
      );
    }
  }, [dataAllWishList]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteWishListMutation();

  const columns = useMemo(
    () => [
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeDriven',
        Header: 'Bike Driven',
        accessor: 'bikeDriven',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'color',
        Header: 'Color',
        accessor: 'color',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isVerified',
        Header: 'Verified',
        accessor: 'isVerified',
        Cell: ({ cell: { value } }) => (value ? 'Verified' : 'Unverified'),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'verified',
            value: true,
          },
          {
            label: 'unverified',
            value: false,
          },
        ],
      },
      {
        id: 'numViews',
        Header: 'Views',
        accessor: 'numViews',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'price',
        Header: 'Price (NRs)',
        accessor: 'price',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForWishList(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    setWishListQuery(query);
  };

  //   console.log(dataWishList);

  useEffect(() => {
    if (dataWishList) {
      setPageCount(dataWishList.totalPages);
      setTotalData(dataWishList.totalDocs);
      // var vehicleDataArr = dataWishList.docs;
      // Cloning the original data and reversing it
      //  var reversedArr = [...vehicleDataArr].reverse();
      setTableData(
        dataWishList.docs.map((value) => {
          return {
            // id is required

            id: value.id,
            bikeDriven: value.bikeDriven,
            color: value.color,
            isVerified: value.isVerified,
            numViews: value.numViews,
            price: value.price,
            vehicleName: value.vehicleName,
          };
        })
      );
    }
  }, [dataWishList]);
  //   const BreadCrumbList = [
  //     {
  //       routeName: 'Add Vehicle',
  //       route: '/vehicle/add',
  //     },
  //     {
  //       routeName: 'Vehicle List',
  //       route: '/vehicle/view',
  //     },
  //   ];

  return (
    <>
      <h3 className="mb-3">Wish List</h3>
      <Table
        tableName="WishList/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllWishList}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorWishList}
        isLoadingData={isFetchingWishList}
        // isLoadingData={true}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        // editRoute="vehicle/edit"
        // viewRoute="/vehicle-listing"
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
        // addPage={{ page: 'Add Vehicle', route: '/vehicle/add' }}
      />
    </>
  );
}

export default AdminWishList;
