import React, { useEffect, useState } from 'react';
import '@heroicons/react/outline';
import { useGetHotDealsQuery } from 'services/api/seller';
import { getQueryStringForVehicle } from 'utils/getQueryStringForTable';

import Button from 'components/Button';

import ListingCard from 'components/Card/ListingCard';
import LoadingCard from 'components/LoadingCard';

const HotDeals = ({ isSold, isVerified }) => {
  const [hotDealList, setHotDealsList] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [hotDealsDataQuery, setHotDealsDataQuery] = useState('');

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setHotDealsDataQuery(query);
  };

  useEffect(() => {
    getData({
      pageIndex: 0,
      pageSize: pageSize,
      sortBy: [],
    });
  }, [getData, pageSize]);

  const { data: hotDealsData, isLoading: isFetchingHotDeals } =
    useGetHotDealsQuery(hotDealsDataQuery);

  useEffect(() => {
    if (hotDealsData) {
      setHasNextPage(hotDealsData.hasNextPage);

      setHotDealsList(hotDealsData);
    }
  }, [hotDealsData]);

  return (
    <div>
      <main id="main">
        <section className="vehicle-listing-section pt-10 pb-20">
          <div className="container">
            <h1 className="h3 mb-4">Hot-Deals</h1>
            <div className="row">
              {hotDealList?.hotDeals?.length ? (
                hotDealList?.hotDeals?.map((vehicle, i) => {
                  return (
                    <ListingCard
                      key={i}
                      cardColsSize="three"
                      route={`/vehicle-listing/${vehicle?.id}`}
                      imgUrl={vehicle?.bikeImagePath[0].imageUrl}
                      totalImages={vehicle?.bikeImagePath.length - 1}
                      vehicleName={vehicle?.vehicleName}
                      price={vehicle?.expectedPrice}
                      bikeDriven={vehicle?.bikeDriven}
                      bikeNumber={vehicle?.bikeNumber}
                      ownershipCount={vehicle?.ownershipCount}
                      id={vehicle.id}
                      condition={vehicle?.condition}
                      isSold={vehicle?.isSold}
                      isVerified={vehicle?.isVerified}
                      location={vehicle?.location?.combineLocation}
                    />
                  );
                })
              ) : (
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: '100vw',
                  }}
                ></div>
              )}
            </div>

            {isFetchingHotDeals && (
              <div className="row">
                <LoadingCard cardColsSize="three" />
                <LoadingCard cardColsSize="three" />
              </div>
            )}

            <div className="w-full flex justify-center items-center">
              {hotDealList?.clientVehicleSellHotDeals?.length > 0 && (
                <div>
                  {isFetchingHotDeals && <div>...Loading</div>}

                  {hasNextPage && (
                    <Button
                      type="button"
                      disabled={!hasNextPage}
                      onClick={() => {
                        setPageSize((pageSize) => pageSize + 10);
                      }}
                      loading={isFetchingHotDeals}
                    >
                      Load More
                    </Button>
                  )}
                </div>
              )}
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default HotDeals;
