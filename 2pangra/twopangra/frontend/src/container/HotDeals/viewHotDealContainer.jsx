// import { DateColumnFilter } from 'components/Table/Filter';
import {
  DateColumnFilter,
  MultiSelectFilter,
  YearColumnFilter,
} from 'components/Table/Filter';
import Table from 'components/Table/table';
import { color, condition, ownershipCount, usedFor } from 'constant/constant';
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
// import { useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import {
  useReadAllHotDealsQuery,
  useRemoveHotDealMutation,
} from 'services/api/hotdeals';

import { getQueryStringForHotDeals } from 'utils/getQueryStringForTable';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';

// import { SelectColumnFilter } from 'components/Table/Filter';

function ViewHotDealsContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [hotDealDataQuery, setHotDealDataQuery] = useState('');
  // console.log(hotDealDataQuery);

  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  const level = useSelector((state) => state.levelReducer.level);

  const {
    data: hotDealsData,
    isError: hotDealError,
    isFetching: isLoadingHotDeal,
  } = useReadAllHotDealsQuery(
    // '?search="hasInHotDeal":"true","isApproved":"true"&noRegex=isApproved,hasInHotDeal',
    hotDealDataQuery
  );

  const {
    data: hotDealDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllHotDealsQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });
  const [
    removeHotDeals,
    {
      error: errorRemoveHotDeal,
      isLoading: isRemovingHotDeal,
      isSuccess: isSuccessRemoveaHotDeal,
    },
  ] = useRemoveHotDealMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: 'success',
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: 'error',
    });
  };
  useEffect(() => {
    if (errorRemoveHotDeal) {
      showFailureToast(
        errorRemoveHotDeal?.data?.message || 'Failed to remove from hot deals'
      );
    }
    if (isSuccessRemoveaHotDeal) {
      showSuccessToast('Successfully removed from hot deals');
    }
  }, [errorRemoveHotDeal, isSuccessRemoveaHotDeal]);

  useEffect(() => {
    if (hotDealDataAll) {
      setTableDataAll(
        hotDealDataAll?.hotDeals?.map((hotDeal) => {
          return {
            vehicleName: hotDeal?.vehicleName,
            bikeDriven: hotDeal?.bikeDriven,
            expectedPrice: thousandNumberSeparator(hotDeal?.expectedPrice),

            bikeNumber: hotDeal?.bikeNumber,
            ownershipCount: hotDeal?.ownershipCount,
            color: hotDeal?.color,
            condition: hotDeal?.condition,
            postExpiryDate: hotDeal?.postExpiryDate,
            id: hotDeal?.id,
            makeYear: !hotDeal.makeYear
              ? undefined
              : hotDeal.makeYear.slice(0, 4),
            usedFor: hotDeal?.usedFor,
          };
        })
      );
    }
  }, [hotDealDataAll]);

  const columns = useMemo(
    () => [
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeDriven',
        Header: 'Bike Driven',
        accessor: 'bikeDriven',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'expectedPrice',
        Header: 'Expected Price',
        accessor: 'expectedPrice',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeNumber',
        Header: 'Bike Number',
        accessor: 'bikeNumber',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'lotNumber',
        Header: 'Lot Number',
        accessor: 'lotNumber',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'ownershipCount',
        Header: 'Ownership Count',
        accessor: 'ownershipCount',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: ownershipCount.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'color',
        Header: 'Color',
        accessor: 'color',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'condition',
        Header: 'Condition',
        accessor: 'condition',
        Cell: ({ cell: { value } }) => value.toLowerCase() || '-',
        Filter: MultiSelectFilter,
        possibleFilters: condition.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'postExpiryDate',
        Header: 'Post Expiry Date',
        accessor: 'postExpiryDate',
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || '-',
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'makeYear',
        Header: 'Make Year (AD)',
        accessor: 'makeYear',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: YearColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'usedFor',
        Header: 'Used For',
        accessor: 'usedFor',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: usedFor.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForHotDeals(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setHotDealDataQuery(query);
  };

  useEffect(() => {
    if (hotDealsData) {
      setPageCount(hotDealsData?.totalPages);
      setTotalData(hotDealsData?.totalDocs);

      setTableData(
        hotDealsData?.docs.map((hotDeal) => {
          return {
            vehicleName: hotDeal?.vehicleName,
            bikeDriven: hotDeal?.bikeDriven,
            expectedPrice: thousandNumberSeparator(hotDeal?.expectedPrice),
            bikeNumber: hotDeal?.bikeNumber,
            lotNumber: hotDeal?.lotNumber,
            ownershipCount: hotDeal?.ownershipCount,
            color: hotDeal?.color,
            condition: hotDeal?.condition,
            postExpiryDate: hotDeal?.postExpiryDate,
            id: hotDeal?.id,
            makeYear: !hotDeal.makeYear
              ? undefined
              : hotDeal.makeYear.slice(0, 4),
            usedFor: hotDeal?.usedFor,
          };
        })
      );
    }
  }, [hotDealsData]);
  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add Vehicle',
  //     route: '/vehicle/add',
  //   },
  //   {
  //     routeName: 'Selling Vehicle List',
  //     route: '',
  //   },
  // ];

  return (
    // <AdminLayout documentTitle="View Seller" BreadCrumbList={BreadCrumbList}>
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="HotDeal/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={hotDealError}
          isLoadingData={isLoadingHotDeal}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          hasExport={true}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          editRoute={
            level === 'superAdmin'
              ? 'seller/edit'
              : level === 'customer'
              ? 'sell/edit'
              : ''
          }
          viewRoute={
            level === 'superAdmin'
              ? 'seller/get'
              : level === 'customer'
              ? 'sell/get'
              : ''
          }
          hasDropdownBtn={true}
          dropdownBtnName="Remove"
          dropdownBtnOptions={[
            {
              name: 'Remove from Hot Deals',
              onClick: removeHotDeals,
              isLoading: isRemovingHotDeal,
              loadingText: 'Removing...',
              // conditionalShow: true,
              // showFor: 'hasInHomePage',
              // dontShowWhen: false,
            },
          ]}
        />
      </div>
    </section>
    // </AdminLayout>
  );
}

export default ViewHotDealsContainer;
