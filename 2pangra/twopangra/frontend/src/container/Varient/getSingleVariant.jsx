import React, { useEffect } from 'react';
import AdminLayout from 'layouts/Admin';
import { useReadVariantDetailsQuery } from 'services/api/varient';
import { useRouter } from 'next/router';
import { useToasts } from 'react-toast-notifications';
import Button from 'components/Button';
import Link from 'next/link';
const GetSingleVariant = () => {
  const { addToast } = useToasts();

  const router = useRouter();
  const {
    data: variantDetails,
    error: variantFetchError,
    isLoading: isLoadingVariant,
  } = useReadVariantDetailsQuery(router.query.id);

  useEffect(() => {
    if (variantFetchError) {
      addToast(
        'Error occured while fetching Variant details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [variantFetchError]);

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Variant List',
      route: '/vehicle/submodel/view',
    },
    {
      routeName: 'Variant Details',
      route: '',
    },
  ];

  return (
    <AdminLayout documentTitle="Get Sub Model" BreadCrumbList={BreadCrumbList}>
      <section className="brand-detail-section">
        <div className="flex justify-end">
          <Link href={`/vehicle/submodel/edit/${router.query.id}`}>
            <a>
              <Button variant="outlined" type="button">
                Edit
              </Button>
            </a>
          </Link>
        </div>
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}
          <h1 className="h3 mb-5">Variant Details</h1>
          <div className="row ">
            {isLoadingVariant ? (
              'Loading...'
            ) : (
              <div className="three-col">
                <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                  <p>
                    <span className="font-bold">Variant Name :</span>&nbsp;{' '}
                    {variantDetails?.varientName}
                  </p>

                  {/* <p>
                    <span className="font-bold">Variant Description :</span>
                    &nbsp;
                    {variantDetails?.varientDescription
                      ? variantDetails.varientDescription
                      : 'Not available'}
                  </p> */}
                  <p>
                    <span className="font-bold">Model Name :</span>&nbsp;
                    {variantDetails?.modelId?.modelName}
                  </p>
                  <p>
                    <span className="font-bold">Brand Name :</span>&nbsp;
                    {variantDetails?.brandName}
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetSingleVariant;
