import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';

import {
  useReadSubModelQuery,
  useDeleteSubModelMutation,
} from 'services/api/varient';

function ViewVariantContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [subModelDataQuery, setSubModelDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  const {
    data: subModelData,
    isError: subModelError,
    isFetching: isLoadingSubModel,
  } = useReadSubModelQuery(subModelDataQuery);

  const {
    data: varientDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadSubModelQuery('', {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (varientDataAll) {
      setTableDataAll(
        varientDataAll.docs.map((subModel) => {
          return {
            varientName: subModel.varientName,
            id: subModel.id,

            // varientDescription: subModel.varientDescription,
            modelName: subModel?.modelName,
            brandName: subModel?.brandName,
          };
        })
      );
    }
  }, [varientDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteSubModelMutation();

  const columns = useMemo(
    () => [
      {
        id: 'varientName',
        Header: 'Variant Name',
        accessor: 'varientName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      // {
      //   id: 'varientDescription',
      //   Header: 'Variant Description',
      //   accessor: 'varientDescription',
      //   Cell: ({ cell: { value } }) => value || '-',
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: 'modelName',
        Header: 'Model Name',
        accessor: 'modelName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'brandName',
        Header: 'Brand Name',
        accessor: 'brandName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);

    setSubModelDataQuery(query);
  };

  useEffect(() => {
    if (subModelData) {
      setPageCount(subModelData.totalPages);
      setTotalData(subModelData.totalDocs);

      setTableData(
        subModelData.docs.map((subModel) => {
          return {
            varientName: subModel.varientName,
            id: subModel.id,

            // varientDescription: subModel.varientDescription,
            modelName: subModel?.modelName,
            brandName: subModel?.brandName,
          };
        })
      );
    }
  }, [subModelData]);
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Variant List',
      route: '/vehicle/submodel/view',
    },
  ];

  return (
    <AdminLayout documentTitle="View Variant" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb> */}

          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Variant/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={subModelError}
            isLoadingData={isLoadingSubModel}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="vehicle/submodel/edit"
            viewRoute="vehicle/submodel/get"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: 'Add Variant', route: '/vehicle/submodel/add' }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewVariantContainer;
