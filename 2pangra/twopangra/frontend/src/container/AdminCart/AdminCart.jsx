// import Table from 'components/Table/table';
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import {
  getQueryStringForCart,
  //   getQueryStringForTable,
} from 'utils/getQueryStringForTable';
import //   useDeleteVehicleMutation,
//   useReadVehicleCustomQuery,
'services/api/vehicle';
import { MultiSelectFilter, SelectColumnFilter } from 'components/Table/Filter';
import {
  useDeleteCartMutation,
  useReadAllCartQuery,
} from 'services/api/adminCartList';
import Table from 'components/Table/table';
import { color } from 'constant/constant';
function AdminCart() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [cartQuery, setCartQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataCart,
    isError: isErrorCart,
    isFetching: isFetchingCart,
  } = useReadAllCartQuery(cartQuery);

  const {
    data: dataAllCart,
    // isError: isVehicleErrorAll,
    isFetching: isFetchingAllCart,
  } = useReadAllCartQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  //   console.log()

  useEffect(() => {
    if (dataAllCart) {
      setTableDataAll(
        dataAllCart.docs.map((value) => {
          return {
            id: value.id || '-',
            bikeDriven: value.bikeDriven || '-',
            color: value.color || '-',
            isVerified: value.isVerified || '-',
            numViews: value.numViews || '-',
            price: value.price || '-',
            vehicleName: value.vehicleName || '-',
          };
        })
      );
    }
  }, [dataAllCart]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteCartMutation();

  const columns = useMemo(
    () => [
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeDriven',
        Header: 'Bike Driven',
        accessor: 'bikeDriven',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'color',
        Header: 'Color',
        accessor: 'color',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isVerified',
        Header: 'Verified',
        accessor: 'isVerified',
        Cell: ({ cell: { value } }) => (value ? 'Verified' : 'Unverified'),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'verified',
            value: true,
          },
          {
            label: 'unverified',
            value: false,
          },
        ],
      },
      {
        id: 'numViews',
        Header: 'Views',
        accessor: 'numViews',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'price',
        Header: 'Price (NRs)',
        accessor: 'price',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForCart(pageIndex, pageSize, sortBy, filters);
    setCartQuery(query);
  };

  //   console.log(dataCart);

  useEffect(() => {
    if (dataCart) {
      setPageCount(dataCart.totalPages);
      setTotalData(dataCart.totalDocs);
      // var vehicleDataArr = dataCart.docs;
      // Cloning the original data and reversing it
      //  var reversedArr = [...vehicleDataArr].reverse();
      setTableData(
        dataCart.docs.map((value) => {
          return {
            // id is required

            id: value.id,
            bikeDriven: value.bikeDriven,
            color: value.color,
            isVerified: value.isVerified,
            numViews: value.numViews,
            price: value.price,
            vehicleName: value.vehicleName,
          };
        })
      );
    }
  }, [dataCart]);
  //   const BreadCrumbList = [
  //     {
  //       routeName: 'Add Vehicle',
  //       route: '/vehicle/add',
  //     },
  //     {
  //       routeName: 'Vehicle List',
  //       route: '/vehicle/view',
  //     },
  //   ];

  return (
    <>
      <h3 className="mb-3">Cart List</h3>
      <Table
        tableName="Cart/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllCart}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorCart}
        isLoadingData={isFetchingCart}
        // isLoadingData={true}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        // editRoute="vehicle/edit"
        // viewRoute="/vehicle-listing"
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
        // addPage={{ page: 'Add Vehicle', route: '/vehicle/add' }}
      />
    </>
  );
}

export default AdminCart;
