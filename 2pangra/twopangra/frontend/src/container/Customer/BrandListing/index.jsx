import Link from 'next/link';
import { useState, useEffect } from 'react';
import {
    // useReadBrandNoAuthQuery,
    useReadBrandNoAuthWithLimitQuery,
} from 'services/api/brand';
import Button from 'components/Button';
// import SearchWithSelect from 'components/Search/SearchSingeData';

function BrandListing() {
    const [pageSize, setPageSize] = useState(12);
    const [hasNextPage, setHasNextPage] = useState(false);
    const {
        data: allBrandDataWithLimit,
        error: errorAllBrandDataWithLimit,
        isLoading: isLoadingAllBrandDataWithLimit,
        isFetching: isFetchingAllBrandDataWithLimit
    } = useReadBrandNoAuthWithLimitQuery(`?select=id,brandVehicleName,uploadBrandImage&limit=${pageSize}&noRegex=hasInHomePage`);

    // const {
    //     data: allBrandData,
    //     error: errorAllBrandData,
    //     isFetching: isFetchingAllBrandData
    // } = useReadBrandNoAuthQuery();

    useEffect(() => {
        if (allBrandDataWithLimit) {
            setHasNextPage(allBrandDataWithLimit?.hasNextPage);
        }
    }, [allBrandDataWithLimit]);

    return (
        <main id="main">
            <section className="vehicle-listing-section pb-20">
                <div className="container">
                    <div className="row mb-5 px-3 border-b border-primary">
                        <h5 className="font-md">Our Available Brands</h5>
                    </div>
                    {/* <div className="row">
                        <form action="#" className="filter-form">
                            <div className="form-row row-sm">
                                <div className="form-group three-col-sm">
                                    <SearchWithSelect
                                        // loading={fetchingBrands}
                                        placeholder="Search By Brand"
                                        value={brandId || null}
                                        onChange={(selectedBrand) => {
                                            setBrandId({
                                                id: 'brandName',
                                                value: arrayToString(selectedBrand),
                                            });
                                            setBrandSelectClear(false);
                                        }}
                                        options={brandData?.map((brand) => ({
                                            value: brand.brandName,
                                            label: brand.brandName,
                                        }))}
                                        customClear={brandSelectClear}
                                    />
                                </div>
                            </div>
                        </form>
                    </div> */}
                    <div className="row justify-center mt-[60px]">
                        {errorAllBrandDataWithLimit ?
                            <p className="f-sm">Error loading brands</p> :
                            // We want to show "loading..." only on first time. After we click on Load More, only load more will have the spinner and "loading..." will not be shown.
                            isLoadingAllBrandDataWithLimit ?
                                <p className='f-sm'>Loading...</p> :
                                allBrandDataWithLimit?.docs ?
                                    allBrandDataWithLimit?.docs.map((brand, i) => {
                                        return (
                                            <div className="six-col" key={i}>
                                                <div className="mb-8 rounded-xl h-[70px] bg-white">
                                                    <Link
                                                        href={`/vehicle-listing?browseBy=brand&brandName=${brand?.brandVehicleName}&`}
                                                        key={i}
                                                    >
                                                        <a className="w-full px-3 py-3 flex items-center h-full">
                                                            {brand?.uploadBrandImage?.imageUrl ?
                                                                <img src={brand?.uploadBrandImage?.imageUrl} alt="logo" className='w-full h-full object-cover' title={brand?.brandVehicleName} /> :
                                                                <p className="f-sm">{brand?.brandVehicleName}</p>
                                                            }
                                                        </a>
                                                    </Link>
                                                </div>
                                            </div>
                                        );
                                    }) :
                                    null
                        }
                    </div>
                    {hasNextPage ?
                        <div className="row justify-center">
                            <Button
                                type="button"
                                variant="secondary"
                                disabled={!hasNextPage}
                                onClick={() => {
                                    setPageSize(pageSize + 12);
                                }}
                                loading={isFetchingAllBrandDataWithLimit}
                            >
                                Load More
                            </Button>
                        </div> :
                        null
                    }
                </div>
            </section>
        </main>
    )
}

export default BrandListing;