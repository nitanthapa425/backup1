import React from 'react';
import Image from 'next/image';
// import AdminLayout from 'layouts/Admin';
// import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';

import {
  PencilIcon,
  PhoneIcon,
  MailIcon,
  LocationMarkerIcon,
  UserIcon,
  CalendarIcon,
} from '@heroicons/react/outline';
import { BadgeCheckIcon } from '@heroicons/react/solid';
import Link from 'next/link';

import { useCustomerMyProfileQuery } from 'services/api/customerSelf';
import { CityLastWord } from 'utils/LastWord';
// import UserLayout from 'layouts/User';

const CustomerMyProfileDetails = ({
  canEdit = true,
  list = 'Customer List',
  profile = 'Customer Profile',
}) => {
  // const router = useRouter();
  // const { addToast } = useToasts();

  const {
    data: customerProfile,
    // error: customerProfileFetchError,
    isLoading: isLoadingCustomerProfile,
  } = useCustomerMyProfileQuery();

  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add Vehicle',
  //     route: '/vehicle/add',
  //   },
  //   {
  //     routeName: ` My Profile`,
  //     route: '',
  //   },
  // ];
  return (
    // <UserLayout
    //   documentTitle="Get Customer Profile"
    //   BreadCrumbList={BreadCrumbList}
    // >
    <section className="my-profile-section py-[40px] md:py-[60px]">
      <div className="container">
        {isLoadingCustomerProfile ? (
          <div className="container flex justify-center">
            {/* <p className="f-lg">Loading...</p> */}
            <svg
              className={`animate-spin h-10 w-10 text-primary`}
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
            >
              <circle
                className="opacity-25"
                cx="12"
                cy="12"
                r="10"
                stroke="currentColor"
                strokeWidth="4"
              ></circle>
              <path
                className="opacity-75"
                fill="currentColor"
                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
              ></path>
            </svg>
          </div>
        ) : (
          <>
            <div className="max-w-[650px] mx-auto flex flex-wrap justify-center md:justify-start pl-7 md:pr-[80px] relative mb-[20px] md:mb-0 text-center md:text-left">
              <div className="w-[150px] h-[143px] object-cover relative rounded-xl overflow-hidden ">
                <strong>{customerProfile?.userName}&nbsp;</strong>
                {customerProfile?.profileImagePath?.imageUrl ? (
                  <Image
                    src={`${customerProfile?.profileImagePath?.imageUrl}`}
                    alt="Profile Image"
                    layout="fill"
                  />
                ) : (
                  <div className="w-[160px] h-[153px] object-cover mx-auto relative">
                    <Image src="/user.JPG" alt="Profile Image" layout="fill" />
                  </div>
                )}
              </div>
              <div className="w-full md:w-auto md:flex-1 md:pl-[25px] pt-[15px] md:pt-[25px]">
                <h1 className="h4 block"> {customerProfile?.fullName}</h1>
                <span className="block mb-1">
                  {customerProfile?.location[0]?.combineLocation}
                </span>
              </div>
              <div className="edit w-[30px] h-[30px] rounded-full flex items-center justify-center border border-primary-dark absolute right-2 top-[40px]">
                {canEdit && (
                  <Link href={`/customer-profile/edit`}>
                    <a>
                      <PencilIcon className="w-4 h-4" />
                    </a>
                  </Link>
                )}
              </div>
            </div>

            <div className="max-w-[650px] mx-auto pt-[30px] md:pt-[70px] rounded-xl bg-gray-100 p-4 md:p-8 border border-gray-300 md:mt-[-45px]">
              <div className="row">
                <div className="two-col">
                  <div className="flex  items-center justify-center flex-wrap mb-2 rounded-[30px] bg-white shadow-sm border border-gray-200 text-center py-2 px-6">
                    <CalendarIcon className="w-4 h-4 mr-2 mt-[-2px]" />
                    {customerProfile?.dob
                      ? new Date(customerProfile?.dob).toLocaleDateString()
                      : null}
                  </div>
                </div>
                <div className="two-col">
                  <div className="flex relative items-center justify-center flex-wrap  mb-2 rounded-[30px] bg-white shadow-sm border border-gray-200 text-center py-2 px-6">
                    <PhoneIcon className="w-4 h-4 mr-2 mt-[-2px]" />
                    <a
                      href="tel:{customerProfile?.mobile}"
                      className="text-textColor"
                    >
                      {customerProfile?.mobile}
                    </a>
                    <a href="#">
                      <BadgeCheckIcon className="h-5 w-5 absolute right-2 -top-2" />
                    </a>
                  </div>
                </div>
                <div className="two-col">
                  <div className=" flex relative  items-center justify-center flex-wrap mb-2 rounded-[30px] bg-white shadow-sm border border-gray-200 text-center py-2 px-6">
                    <MailIcon className="w-4 h-4 mr-1" />
                    <a
                      href="mailto:{customerProfile?.email}"
                      className="text-textColor"
                    >
                      {customerProfile?.email}
                    </a>
                    <a href="#" className="text-textColor">
                      <BadgeCheckIcon className="h-5 w-5 absolute right-2 -top-2 opacity-40" />
                    </a>
                  </div>
                </div>
                <div className="two-col">
                  <div className=" flex  items-center justify-center flex-wrap mb-2 rounded-[30px] bg-white shadow-sm border border-gray-200 text-center py-2 px-6">
                    <UserIcon className="w-4 h-4 mr-2 mt-[-2px]" />
                    {customerProfile?.gender}
                  </div>
                </div>
                <div className="two-col w-auto">
                  <div className="flex  items-center justify-center flex-wrap  mb-0 rounded-[30px] bg-white shadow-sm border border-gray-200 text-center py-2 px-6">
                    <LocationMarkerIcon className="w-4 h-4 mr-2 mt-[-2px]" />

                    {CityLastWord(
                      customerProfile?.location[0]?.combineLocation
                    ) ? (
                      <span className="name-icon">
                        {CityLastWord(
                          customerProfile?.location[0]?.combineLocation
                        )}
                      </span>
                    ) : (
                      <span className="name-icon">Not Available</span>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </section>
  );
};

export default CustomerMyProfileDetails;
