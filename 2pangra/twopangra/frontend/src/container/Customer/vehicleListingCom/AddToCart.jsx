/* eslint-disable no-empty-pattern */
// import UserHeader from 'components/Header/UserHeader';
import React, { useEffect, useState } from 'react';
// import Link from 'next/link';
// import UserLayout from 'layouts/User';

import {
  // DownloadIcon,
  EyeIcon,
  TrashIcon,
  // HeartIcon,
  XIcon,
  CheckCircleIcon,
  XCircleIcon,
} from '@heroicons/react/outline';
import {
  // useAddToBookMutation,
  useDeleteMyCartMutation,
  useGetMyCartQuery,
} from 'services/api/seller';
import { useToasts } from 'react-toast-notifications';
import { sameArrayIrrespectiveToPosition } from 'utils/sameArray';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import Popup from 'components/Popup';
import { useRouter } from 'next/router';
import Link from 'next/link';
import ReactModal from 'components/ReactModal/ReactModal';
import Address from 'components/Location';

const AddToCart = () => {
  const router = useRouter();
  const { addToast } = useToasts();
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [deleteSingleCart, setDeleteSingleCart] = useState(null);
  const [openModalDeleteSingleCart, setOpenModalDeleteSingleCart] =
    useState(false);
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  const {
    data: dataMyCart,
    // isError: isErrorMyCart,
    // error: errorMyCart,
    // isFetching: isFetchingMyCart,
  } = useGetMyCartQuery();

  const [bookList, setBookList] = useState([]);

  // console.log('mycart', dataMyCart);

  // const [
  //   addToBook,
  //   {
  //     isLoading: isLoadingAddToBook,
  //     isError: isErrorAddToBook,
  //     isSuccess: isSuccessAddToBook,
  //     // data: addToBookData,
  //     error: addToBookError,
  //   },
  // ] = useAddToBookMutation();
  const [
    deleteCart,
    {
      isLoading: isLoadingDeleteCart,
      isError: isErrorDeleteCart,
      isSuccess: isSuccessDeleteCart,
      error: errorDeleteCart,
    },
  ] = useDeleteMyCartMutation();

  useEffect(() => {
    if (isSuccessDeleteCart) {
      addToast('You just delete item/s form cart ', {
        appearance: 'success',
      });
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessDeleteCart]);
  useEffect(() => {
    if (isErrorDeleteCart) {
      addToast(errorDeleteCart?.data?.message, {
        appearance: 'error',
      });
    }
  }, [isErrorDeleteCart, errorDeleteCart]);

  // useEffect(() => {
  //   if (isSuccessAddToBook) {
  //     addToast(
  //       'You just booked vehicle, please set your location for further inquiry',
  //       {
  //         appearance: 'success',
  //       }
  //     );
  //     // router.push('/book');
  //     router.push('/location');

  //     // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
  //   }
  // }, [isSuccessAddToBook]);
  // useEffect(() => {
  //   if (isErrorAddToBook) {
  //     addToast(addToBookError?.data?.message, {
  //       appearance: 'error',
  //     });
  //   }
  // }, [isErrorAddToBook, addToBookError]);

  // console.log(bookList);

  const totalPrice = (bookList = []) => {
    const array1 = dataMyCart?.cllientCartDetail.filter((data, i) =>
      bookList.includes(data.sellerVehicleId)
    );

    // console.log(array1[0]?.price);

    const arrayPrice = array1?.map((data, i) => data.price);

    const total = arrayPrice?.reduce((cur, pre) => cur + pre, 0);
    return total;
  };

  return (
    <div className="pb-[80px]">
      {/* <UserLayout title="My Cart"> */}
      {/* <UserHeader></UserHeader> */}

      {dataMyCart?.cllientCartDetail?.length ? (
        <div className="container">
          {openModalVehicleType && (
            <Popup
              title="Are you sure to remove item/s form cart?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                bookList.forEach((id, i) => {
                  deleteCart(id)
                    .then(() => {
                      setOpenModalVehicleType(false);
                    })
                    .catch(() => {
                      setOpenModalVehicleType(false);
                    });
                });
              }}
              onCancelClick={() => setOpenModalVehicleType(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteCart}
            />
          )}

          {openModalDeleteSingleCart && (
            <Popup
              title="Are you sure to remove item form wish list?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                deleteCart(deleteSingleCart)
                  .then(() => {
                    setOpenModalDeleteSingleCart(false);
                  })
                  .catch(() => {
                    setOpenModalDeleteSingleCart(false);
                  });
              }}
              onCancelClick={() => setOpenModalDeleteSingleCart(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteCart}
            />
          )}

          <div className="flex flex-wrap items-start">
            <div className="flex-1 md:mr-[25px] ">
              <div className="flex justify-between items-center py-2 mb-2">
                <div className="flex items-center space-x-2">
                  <input
                    type="checkbox"
                    // checked if two array are same
                    checked={sameArrayIrrespectiveToPosition(
                      bookList,
                      dataMyCart?.cllientCartDetail?.map(
                        (data, i) => data.sellerVehicleId
                      )
                    )}
                    className="checkbox w-4 h-4 border border-gray-500 inline-block"
                    onClick={(e) => {
                      // console.log(e.target.checked);
                      if (e.target.checked) {
                        // setBookList(dataMyCart?.cllientCartDetail);
                        setBookList((bookList) => {
                          bookList = [];
                          return dataMyCart?.cllientCartDetail?.map(
                            (data, i) => {
                              return data.sellerVehicleId;
                            }
                          );
                        });
                      } else {
                        setBookList([]);
                      }
                    }}
                  ></input>
                  <span className="inline-block ml-2">
                    Select all items{' '}
                    {bookList.length ? `(${bookList.length} item/s)` : ''}
                  </span>
                </div>
                <div className="flex items-center ">
                  <button
                    onClick={() => {
                      setOpenModalVehicleType(true);
                    }}
                  >
                    <TrashIcon className="w-5 h-5 inline-block mr-2" />
                  </button>
                </div>
              </div>
              {dataMyCart?.cllientCartDetail?.map((data, i) => {
                return (
                  <div
                    key={i}
                    className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5  pr-3 lg:pr-0"
                  >
                    {/* <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span> */}
                    <input
                      type="checkbox"
                      checked={bookList.includes(data.sellerVehicleId)}
                      className="checkbox w-4 h-4 border border-gray-400 inline-block mr-3"
                      onClick={(e) => {
                        // console.log(e.target.checked);
                        if (e.target.checked) {
                          const allId = [...bookList, data.sellerVehicleId];
                          const uniqueList = [...new Set(allId)];
                          setBookList(uniqueList);
                        } else {
                          setBookList(
                            bookList.filter((v, j) => {
                              return v !== data.sellerVehicleId;
                            })
                          );
                        }
                      }}
                    ></input>
                    <div
                      className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0"
                      onClick={() => {
                        router.push(`/vehicle-listing/${data.sellerVehicleId}`);
                      }}
                    >
                      <img
                        src={data?.vehicleImage?.[0]?.imageUrl}
                        alt="image description"
                        className="w-full h-[110px] object-contain"
                      />
                    </div>
                    <div
                      className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[130px]"
                      onClick={() => {
                        router.push(`/vehicle-listing/${data.sellerVehicleId}`);
                      }}
                    >
                      <h1 className="h5 mb-1">{data.vehicleName}</h1>
                      <div className=" flex justify-between lg:block lg:w-[100px] lg:absolute lg:text-right lg:right-[15px] lg:top-1/2 lg:translate-Y-[-50%]">
                        <span className="price font-bold">
                          NRs {thousandNumberSeparator(data.price)}
                        </span>
                        {/* <div className="icon-holder flex space-x-2">
                          <HeartIcon className="w-5 h-5" />
                          <TrashIcon className="w-5 h-5" />
                        </div> */}
                      </div>
                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">Color :</span>{' '}
                          {data.color} ,
                          <span className="font-semibold">location :</span>{' '}
                          {data?.location?.combineLocation} ,
                          <span className="font-semibold"> Owner Count: </span>{' '}
                          {data?.ownershipCount} ,
                          <span className="font-semibold">
                            {' '}
                            Kilometer Driven:{' '}
                          </span>
                          {data?.bikeDriven}Km
                        </p>
                      </div>
                      <div className="flex flex-wrap space-x-3">
                        {/* <span className="flex text-sm items-center font-semibold">
                          <CheckCircleIcon className="w-4 h-4 mr-1 mt-[-2px] text-primary-dark" />
                          {data?.isVerified ? 'Verified' : 'Unverified'}
                        </span> */}
                        {data?.isVerified ? (
                          <span>
                            <CheckCircleIcon className="w-4 text-secondary-dark inline-block mr-1" />
                            Verified
                          </span>
                        ) : (
                          <span>
                            <XCircleIcon className="w-4 text-error-dark inline-block mr-1" />
                            Unverified
                          </span>
                        )}
                        <span className="flex text-sm items-center font-semibold">
                          <EyeIcon className="w-4 h-4 mr-1 mt-[-2px] text-secondary-dark" />
                          {data.numViews} Views
                        </span>
                      </div>
                    </div>
                    <span
                      className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
                      onClick={() => {
                        setOpenModalDeleteSingleCart(true);
                        setDeleteSingleCart(data.sellerVehicleId);
                      }}
                    >
                      <XIcon className="w-5 h-5"></XIcon>
                    </span>
                  </div>
                );
              })}
              {/* <div className="flex flex-wrap items-center space-x-3 mb-4 border border-gray-600 py-6 pl-5">
                <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span>
                <input
                  type="checkbox"
                  className="checkbox w-5 h-5 border border-gray-500 inline-block"
                ></input>
                <div className="image-holder max-w-[300px] w-full lg:w-[220px]">
                  <img
                    src="images/card-img.jpg"
                    alt="image description"
                    className="w-full"
                  />
                </div>
                <div className="detail-holder flex-1 relative md:pr-[110px]">
                  <h1 className="h4">Name Goes Here</h1>
                  <div className=" flex justify-between lg:block lg:w-[85px] lg:absolute lg:right-[-40px] lg:top-1/2 lg:translate-x-[-50%]">
                    <span className="price">1,40,000</span>
                    <div className="icon-holder flex space-x-2">
                      <HeartIcon className="w-5 h-5" />
                      <TrashIcon className="w-5 h-5" />
                    </div>
                  </div>
                  <div className="other-details">
                    <p className="text-sm">
                      <span className="font-semibold">Color :</span> Black ,
                      <span className="font-semibold">location :</span>{' '}
                      Kathmandu ,
                      <span className="font-semibold"> Owner Count: </span> 2 ,
                      <span className="font-semibold"> Kilometer Driven: </span>
                      1000Km
                    </p>
                  </div>
                  <div className="flex flex-wrap space-x-3">
                    <span className="flex">
                      <CheckCircleIcon className="w-5 h-5" />
                      Verified
                    </span>
                    <span className="flex">
                      <EyeIcon className="w-5 h-5" />1 views
                    </span>
                  </div>
                </div>
              </div> */}
            </div>
            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-5">
              <h1 className="h5 mb-3">Order Summary</h1>
              <div className="border-b-2 border-gray-200 mb-2">
                <div className="flex flex-wrap justify-between ">
                  <p className="mb-1">
                    SubTotal{' '}
                    {bookList.length ? `(${bookList.length} item/s)` : ''}
                  </p>
                  <p className="mb-1">
                    NRs {thousandNumberSeparator(totalPrice(bookList))}
                  </p>
                </div>
                <div className="flex flex-wrap justify-between ">
                  <p>Shipping Charge</p>
                  <p>NRs 0</p>
                </div>
              </div>
              <div className="flex flex-wrap justify-between mb-3">
                <p className="mb-1">Total Amount</p>
                <p className="mb-1">
                  NRs {thousandNumberSeparator(totalPrice(bookList))}
                </p>
              </div>
              {/* <div className="text-center">
                <Button
                  type="button"
                  // onClick={() => {
                  //   c.forEach((value, i) => {
                  //     addToBook(value);
                  //   });
                  // }}
                  // disabled={bookList.length === 0}
                  // loading={isLoadingAddToBook}

                  onClick={() => {
                    router.push(`/location`);
                  }}
                >
                  Book Now
                </Button>
              </div> */}

              <ReactModal
                name="Book Now"
                disabled={bookList.length === 0}
                modal={modal}
              >
                <Address
                  bookList={bookList}
                  setModalFunction={setModalFunction}
                  modal={modal}
                ></Address>
              </ReactModal>
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your cart is empty</h1>
            <p>
              Looks like you havent made your <br />
              choice yet
            </p>

            <Link href={'/vehicle-listing'}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}

      {/* <div className="container">
          <div className="row-sm">
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
          </div>
        </div> */}
      {/* </UserLayout> */}
    </div>
  );
};

export default AddToCart;
