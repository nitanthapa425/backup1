/* eslint-disable no-empty-pattern */
// import UserHeader from 'components/Header/UserHeader';
import React, { useEffect, useState } from 'react';
// import Link from 'next/link';
// import UserLayout from 'layouts/User';

import {
  // DownloadIcon,
  EyeIcon,
  TrashIcon,
  // HeartIcon,
  XIcon,
  CheckCircleIcon,
  // XCircleIcon,
} from '@heroicons/react/outline';
// import Button from 'components/Button';
import {
  //   useAddToBookMutation,
  //   useAddToCartMutation,
  useDeleteMyBookMutation,
  //   useDeleteMyCartMutationWishList,
  //   useDeleteMyWishListMutation,
  useGetMyBookQuery,
  //   useGetMyWishListQuery,
} from 'services/api/seller';
import { useToasts } from 'react-toast-notifications';
import { sameArrayIrrespectiveToPosition } from 'utils/sameArray';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import Popup from 'components/Popup';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { CityLastWord } from 'utils/LastWord';

const AddToBigList = () => {
  // const router = useRouter();
  const { addToast } = useToasts();
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [deleteSingleBookList, setDeleteSingleBookList] = useState(null);
  const [openModalDeleteSingleBookList, setOpenModalDeleteSingleBookList] =
    useState(false);
  const router = useRouter();

  const {
    data: dataMyBookList,
    // isError: isErrorMyCart,
    // error: errorMyCart,
    isFetching: isFetchingBookList,
  } = useGetMyBookQuery();

  const [bookList, setBookList] = useState([]);

  //   const [
  //     addToCart,
  //     {
  //       isLoading: isLoadingAddToBook,
  //       isError: isErrorAddToBook,
  //       isSuccess: isSuccessAddToBook,
  //       // data: addToCartData,
  //       error: addToBookError,
  //     },
  //   ] = useAddToCartMutation();
  const [
    deleteBookList,
    {
      isLoading: isLoadingDeleteBookList,
      isError: isErrorDeleteBookList,
      isSuccess: isSuccessDeleteBookList,
      error: errorDeleteBookList,
    },
  ] = useDeleteMyBookMutation();

  useEffect(() => {
    if (isSuccessDeleteBookList) {
      addToast('You just cancel booked item/s', {
        appearance: 'success',
      });
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessDeleteBookList]);
  useEffect(() => {
    if (isErrorDeleteBookList) {
      addToast(errorDeleteBookList?.data?.message, {
        appearance: 'error',
      });
    }
  }, [isErrorDeleteBookList, errorDeleteBookList]);

  //   useEffect(() => {
  //     if (isSuccessAddToBook) {
  //       addToast('You just added item/s in cart.', {
  //         appearance: 'success',
  //       });
  //       // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
  //     }
  //   }, [isSuccessAddToBook]);
  //   useEffect(() => {
  //     if (isErrorAddToBook) {
  //       addToast(addToBookError?.data?.message, {
  //         appearance: 'error',
  //       });
  //     }
  //   }, [isErrorAddToBook, addToBookError]);

  // console.log(bookList);

  const totalPrice = (bookList = []) => {
    const arrayPrice = dataMyBookList?.map((data, i) => data.price);
    const total = arrayPrice?.reduce((cur, pre) => cur + pre, 0);
    return total;
  };

  return (
    <div className="pb-[80px]">
      {/* <UserLayout title="My Cart"> */}
      {/* <UserHeader></UserHeader> */}
      {isFetchingBookList ? (
        <div className="container flex justify-center">
          {/* <p className="f-lg">Loading...</p> */}
          <svg
            className={`animate-spin h-10 w-10 text-primary`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      ) : dataMyBookList?.length ? (
        <div className="container">
          {openModalVehicleType && (
            <Popup
              title="Are you sure to cancel booked item/s?"
              description="Please note that,
              once canceled, this cannot be undone."
              onOkClick={() => {
                bookList.forEach((id, i) => {
                  deleteBookList(id)
                    .then(() => {
                      setOpenModalVehicleType(false);
                    })
                    .catch(() => {
                      setOpenModalVehicleType(false);
                    });
                });
              }}
              onCancelClick={() => setOpenModalVehicleType(false)}
              okText="Yes"
              cancelText="No"
              loading={isLoadingDeleteBookList}
            />
          )}

          {openModalDeleteSingleBookList && (
            <Popup
              title="Are you sure to remove item form booked list?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                deleteBookList(deleteSingleBookList)
                  .then(() => {
                    setOpenModalDeleteSingleBookList(false);
                  })
                  .catch(() => {
                    setOpenModalDeleteSingleBookList(false);
                  });
              }}
              onCancelClick={() => setOpenModalDeleteSingleBookList(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteBookList}
            />
          )}

          <div className="flex flex-wrap items-start">
            <div className="flex-1 mr-[25px] ">
              <div className="h6 mb-3">
                Note: We will contact you immediately within 1 weeks
              </div>
              <div className="flex justify-between items-center py-2 mb-2">
                <div className="flex items-center space-x-2">
                  <input
                    type="checkbox"
                    // checked if two array are same
                    checked={sameArrayIrrespectiveToPosition(
                      bookList,
                      dataMyBookList?.map((data, i) => data.vehicleId)
                    )}
                    className="checkbox w-4 h-4 border border-gray-500 inline-block"
                    onClick={(e) => {
                      // console.log(e.target.checked);
                      if (e.target.checked) {
                        // setBookList(dataMyBookList?.docs);
                        setBookList((bookList) => {
                          bookList = [];
                          return dataMyBookList?.map((data, i) => {
                            return data.vehicleId;
                          });
                        });
                      } else {
                        setBookList([]);
                      }
                    }}
                  ></input>

                  <span className="inline-block ml-2">
                    Select all items{' '}
                    {bookList.length ? `(${bookList.length} item/s)` : ''}
                  </span>
                </div>
                {bookList.length !== 0 && (
                  <div className="flex items-center ">
                    <button
                      onClick={() => {
                        setOpenModalVehicleType(true);
                      }}
                    >
                      <TrashIcon className="w-5 h-5 inline-block mr-2" />
                    </button>
                  </div>
                )}
              </div>
              {dataMyBookList?.map((data, i) => {
                return (
                  <div
                    key={i}
                    className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5"
                  >
                    {/* <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span> */}
                    <input
                      type="checkbox"
                      checked={bookList.includes(data.vehicleId)}
                      className="checkbox w-4 h-4 border border-gray-400 inline-block"
                      onClick={(e) => {
                        // console.log(e.target.checked);
                        if (e.target.checked) {
                          const allId = [...bookList, data.vehicleId];
                          const uniqueList = [...new Set(allId)];
                          setBookList(uniqueList);
                        } else {
                          setBookList(
                            bookList.filter((v, j) => {
                              return v !== data.vehicleId;
                            })
                          );
                        }
                      }}
                    ></input>
                    <div className="image-holder max-w-[300px] px-2 !mx-auto  mb-4 w-full lg:w-[180px] lg:mb-0">
                      <img
                        src={data?.vehicleImage?.[0]?.imageUrl}
                        alt="image description"
                        className="w-full h-[110px] object-contain"
                        onClick={() => {
                          router.push(`/vehicle-listing/${data.vehicleId}`);
                        }}
                      />
                    </div>
                    <div
                      className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[130px]"
                      onClick={() => {
                        router.push(`/vehicle-listing/${data.vehicleId}`);
                      }}
                    >
                      <h1 className="h5 mb-1">{data.vehicleName}</h1>
                      <div className=" flex justify-between lg:block lg:w-[100px] lg:absolute lg:text-right lg:right-[15px] lg:top-1/2 lg:translate-Y-[-50%]">
                        <span className="price font-bold">
                          NRs {thousandNumberSeparator(data.price)}
                        </span>
                        {/* <div className="icon-holder flex space-x-2">
                          <HeartIcon className="w-5 h-5" />
                          <TrashIcon className="w-5 h-5" />
                        </div> */}
                      </div>
                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">Color :</span>{' '}
                          {data.color} ,
                          <span className="font-semibold">location :</span>{' '}
                          {/* {data?.location?.combineLocation} , */}
                          {CityLastWord(data?.combineLocation) ? (
                            <span className="text-sm">
                              {CityLastWord(data?.combineLocation)}
                            </span>
                          ) : (
                            <span className="text-sm">Not Available</span>
                          )}
                          <span className="font-semibold"> Owner Count: </span>{' '}
                          {data?.ownershipCount} ,
                          <span className="font-semibold">
                            {' '}
                            Kilometer Driven:{' '}
                          </span>
                          {data?.bikeDriven}Km
                        </p>
                      </div>
                      <div className="flex flex-wrap space-x-3">
                        {/* <span className="flex text-sm items-center font-semibold">
                          <CheckCircleIcon className="w-4 h-4 mr-1 mt-[-2px] text-primary-dark" />
                          {data?.isVerified ? 'Verified' : 'Unverified'}
                        </span> */}
                        {/* {data?.isVerified ? (
                          <span>
                            <CheckCircleIcon className="w-4 text-secondary-dark inline-block mr-1" />
                            Verified
                          </span>
                        ) : (
                          <span>
                            <XCircleIcon className="w-4 text-error-dark inline-block mr-1" />
                            Unverified
                          </span>
                        )} */}
                        {
                          data?.isVerified ? (
                            <span>
                              <CheckCircleIcon className="w-4 text-secondary-dark mt-[-3px] inline-block mr-1" />
                              Verified
                            </span>
                          ) : null
                          // <span>
                          //   <XCircleIcon className="w-4 text-error mt-[-3px] inline-block mr-1" />
                          //   Unverified
                          // </span>
                        }
                        <span className="flex text-sm items-center font-semibold">
                          <EyeIcon className="w-4 h-4 mr-1 mt-[-2px] text-secondary-dark" />
                          {data.numViews} Views
                        </span>
                      </div>
                    </div>
                    <span
                      className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
                      onClick={() => {
                        setOpenModalDeleteSingleBookList(true);
                        setDeleteSingleBookList(data.vehicleId);
                      }}
                    >
                      <XIcon className="w-5 h-5"></XIcon>
                    </span>
                  </div>
                );
              })}
              {/* <div className="flex flex-wrap items-center space-x-3 mb-4 border border-gray-600 py-6 pl-5">
                <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span>
                <input
                  type="checkbox"
                  className="checkbox w-5 h-5 border border-gray-500 inline-block"
                ></input>
                <div className="image-holder max-w-[300px] w-full lg:w-[220px]">
                  <img
                    src="images/card-img.jpg"
                    alt="image description"
                    className="w-full"
                  />
                </div>
                <div className="detail-holder flex-1 relative md:pr-[110px]">
                  <h1 className="h4">Name Goes Here</h1>
                  <div className=" flex justify-between lg:block lg:w-[85px] lg:absolute lg:right-[-40px] lg:top-1/2 lg:translate-x-[-50%]">
                    <span className="price">1,40,000</span>
                    <div className="icon-holder flex space-x-2">
                      <HeartIcon className="w-5 h-5" />
                      <TrashIcon className="w-5 h-5" />
                    </div>
                  </div>
                  <div className="other-details">
                    <p className="text-sm">
                      <span className="font-semibold">Color :</span> Black ,
                      <span className="font-semibold">location :</span>{' '}
                      Kathmandu ,
                      <span className="font-semibold"> Owner Count: </span> 2 ,
                      <span className="font-semibold"> Kilometer Driven: </span>
                      1000Km
                    </p>
                  </div>
                  <div className="flex flex-wrap space-x-3">
                    <span className="flex">
                      <CheckCircleIcon className="w-5 h-5" />
                      Verified
                    </span>
                    <span className="flex">
                      <EyeIcon className="w-5 h-5" />1 views
                    </span>
                  </div>
                </div>
              </div> */}
            </div>
            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-5">
              <h1 className="h5 mb-3">Order Summary</h1>
              <div className="flex flex-wrap justify-between ">
                <p>
                  Product Amount{' '}
                  {bookList.length ? `(${bookList.length} item/s)` : ''}
                </p>
                <p>NRs {thousandNumberSeparator(totalPrice(bookList))}</p>
              </div>
              {/* <Button
                  type="button"
                  onClick={() => {
                    bookList.forEach((value, i) => {
                      addToCart(value);
                    });
                  }}
                  disabled={bookList.length === 0}
                  loading={isLoadingAddToBook}
                >
                  Add To Cart
                </Button> */}
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your book list is empty</h1>
            <p>
              Looks like you havent made your <br />
              choice yet
            </p>
            <Link href={'/vehicle-listing'}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}

      {/* <div className="container">
          <div className="row-sm">
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
          </div>
        </div> */}
      {/* </UserLayout> */}
    </div>
  );
};

export default AddToBigList;
