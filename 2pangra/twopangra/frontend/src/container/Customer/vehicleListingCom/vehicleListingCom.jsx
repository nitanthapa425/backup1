// import UserHeader from 'components/Header/UserHeader';
import { useEffect, useState, useRef } from 'react';
import { SearchIcon } from '@heroicons/react/outline';
import { useReadSellerQuery } from 'services/api/seller';
import {
  // getQueryStringForTable,
  getQueryStringForVehicle,
} from 'utils/getQueryStringForTable';
import { useRouter } from 'next/router';
import Button from 'components/Button';
import useDebounce from 'hooks/useDebounce';
import { useReadBrandNoAuthQuery } from 'services/api/brand';
import SearchWithSelect from 'components/Search/SearchSingeData';
import LoadingCard from 'components/LoadingCard';
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import ReactAccordions from 'components/Accordions/ReactAccordions';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-accessible-accordion/dist/fancy-example.css';
import RangeSlider from 'components/RangeSlider/RangeSlider';
import ListingCard from 'components/Card/ListingCard';
import FilterTags from 'components/FilterTags';
import { arrayToString } from 'utils/arrtoString';
import { color } from 'constant/constant';
import { useReadAllDistrictQuery } from 'services/api/location';
// import { firstLetterCapital } from 'utils/firstLetterCapita.';

// {{URL}}/seller/vehicleSell?search="condition":"Good,Fair"&type=vehicleSell
// {{URL}}/seller/vehicleSell?search="bikeDriven":"1000-4500"&type=vehicleSell
// {{URL}}/seller/vehicleSell?search="expectedPrice":"10000-450000"&type=vehicleSell

const VehicleListingCom = () => {
  const router = useRouter();
  // Use refs for clearing the form
  const vehNameEl = useRef(null);
  const [mainTag, setMainTag] = useState({});
  const [tags, setTags] = useState([]);
  const [colorSelectClear, setColorSelectClear] = useState(false);
  // const [brandSelectClear, setBrandSelectClear] = useState(false);
  const [conditionSelectClear, setConditionSelectClear] = useState(false);
  const [fuelTypeSelectClear, setFuelTypeSelectClear] = useState(false);
  // const [vehTypeSelectClear, setVehTypeSelectClear] = useState(false);
  // const [districtSelectClear, setDistrictSelectClear] = useState(false);

  /*
   Some Supported routes: 
   /vehicle-listing?browseBy=featured or recentlyAdded or budget,
   /vehicle-listing?browseBy=brand&brandName={{brandname}},
   /vehicle-listing?find=true&brand={{Honda,Tvs}}&budget={{0-100000}}&makeYear={{2021-01-01}}&district={{Kathmandu,Jhapa}}
   and many more
  */
  const {
    browseBy: browseByQuery,
    brandName: brandNameQuery,
    vehicleType: vehicleTypeQuery,
    budgetRange: budgetRangeQuery,
    // All the query below are for Find Bike
    find: findQuery,
    brand: brandQuery,
    budget: budgetQuery,
    makeYear: makeYearQuery,
    district: districtQuery,
    // For global searching
    search: searchQuery,
  } = router.query;

  const brandSelectRef = useRef(null);
  const districtSelectRef = useRef(null);
  const vehTypeSelectRef = useRef(null);
  const [brandNameQueryState, setBrandNameQueryState] = useState('');
  const [budgetRangeQueryState, setBudgetRangeQueryState] = useState('');
  const [districtQueryState, setDistrictQueryState] = useState('');
  const [makeYearQueryState, setMakeYearQueryState] = useState('');
  const [vehicleTypeQueryState, setVehicleTypeQueryState] = useState('');
  const [globalSearchQueryState, setGlobalSearchQueryState] = useState('');

  // When we refresh the page, our router.query objects get populated after rendering.
  // So to give the selected fields their default values according to the query params in the url route, we used router.ready
  useEffect(() => {
    if (router.isReady) {
      if (findQuery === 'true') {
        setBrandNameQueryState(brandQuery);
        setBudgetRangeQueryState(budgetQuery);
        setDistrictQueryState(districtQuery);
        setMakeYearQueryState(makeYearQuery);
      } else if (searchQuery) {
        setGlobalSearchQueryState(searchQuery);
      } else {
        setBrandNameQueryState(brandNameQuery);
        setBudgetRangeQueryState(budgetRangeQuery);
        setVehicleTypeQueryState(vehicleTypeQuery);
      }
    }
  }, [router.isReady]);

  useEffect(() => {
    const handleRouteChange = (url) => {
      setSelectedSortBy('latest');
      if (url === '/vehicle-listing') {
        // We want to clear all filters if route is just '/vehicle-listing'
        handleReset();
      } else if (url.includes('/vehicle-listing?search=')) {
        handleReset();
        // This is for when we are in the /vehicle-listing page and we search using the global search in the navbar
        // decodeURI converts the spaces given by %20 in URI into actual spaces and many more
        vehNameEl.current.value = decodeURI(url.split('=')[1]);
      } else if (url.includes('/vehicle-listing?browseBy=brand&brandName=')) {
        handleReset();
        const brandNameFromUrl = decodeURI(url.split('&brandName=')[1]);
        const bNameWithoutAnd = brandNameFromUrl.replace('&', '');
        brandSelectRef.current.setValue([
          { value: bNameWithoutAnd, label: bNameWithoutAnd },
        ]);
      }
    };
    router.events.on('routeChangeStart', handleRouteChange);
    return () => {
      router.events.off('routeChangeStart', handleRouteChange);
    };
  }, []);

  useEffect(() => {
    if (vehicleTypeQuery) {
      vehTypeSelectRef.current.setValue([
        { value: vehicleTypeQueryState, label: vehicleTypeQueryState },
      ]);
    } else {
      vehTypeSelectRef.current.setValue([]);
    }
  }, [vehicleTypeQueryState]);

  useEffect(() => {
    if (brandNameQuery) {
      brandSelectRef.current.setValue([
        { value: brandNameQueryState, label: brandNameQueryState },
      ]);
    } else if (brandQuery) {
      // When findQuery === 'true'
      brandSelectRef.current.setValue([
        { value: brandNameQueryState, label: brandNameQueryState },
      ]);
    } else {
      brandSelectRef.current.setValue([]);
    }
  }, [brandNameQueryState]);

  useEffect(() => {
    if (districtQuery) {
      // We selected more than one district in Find Bike
      if (districtQuery.includes(',')) {
        const dQArr = districtQuery.split(',');
        const dSetValue = dQArr.map((d) => ({ value: d, label: d }));
        districtSelectRef.current.setValue(dSetValue);
      } else {
        districtSelectRef.current.setValue([
          { value: districtQueryState, label: districtQueryState },
        ]);
      }
    } else {
      districtSelectRef.current.setValue([]);
    }
  }, [districtQueryState]);

  useEffect(() => {
    if (makeYearQuery) {
      setSelectedMakeYear(new Date(`${makeYearQueryState}-01-01`));
    } else {
      setSelectedMakeYear(null);
    }
  }, [makeYearQueryState]);

  useEffect(() => {
    if (budgetRangeQuery) {
      const minMaxArr = budgetRangeQuery.split('-');
      setExpectedPriceRange(minMaxArr);
      // Also need to set this to fetch from api
      setExpectedPrice({
        id: 'expectedPrice',
        value: budgetRangeQuery,
      });
    } else if (budgetQuery) {
      // When findQuery === 'true'
      const minMaxArr = budgetQuery.split('-');
      setExpectedPriceRange(minMaxArr);
      // Also need to set this to fetch from api
      setExpectedPrice({
        id: 'expectedPrice',
        value: budgetQuery,
      });
    }
  }, [budgetRangeQueryState]);

  useEffect(() => {
    if (searchQuery) {
      vehNameEl.current.value = searchQuery;
    }
  }, [globalSearchQueryState]);

  const [bikeDrivenRange, setBikeDrivenRange] = useState([0, 1000000]);
  const [expectedPriceRange, setExpectedPriceRange] = useState([0, 10000000]);
  const [mileageRange, setMileageRange] = useState([0, 100]);
  // const [vehicleList, setVehicleList] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [sortBy, setSortBy] = useState([]);
  const [selectedSortBy, setSelectedSortBy] = useState('latest');
  const [vehicleFilter, setVehicleFilter] = useState({
    id: 'vehicleName',
    value: '',
  });
  const conditionOptions = [
    'Brand New',
    'Like New',
    'Excellent',
    'Good/Fair',
    'Not Working',
  ];
  // const verifiedUnverified = [
  //   { value: 'verified', label: 'Verified' },
  //   { value: 'unverified', label: 'Unverified' },
  // ];
  const vehicleFuelType = [
    { value: 'PETROL', label: 'Petrol' },
    { value: 'ELECTRIC', label: 'Electric' },
  ];
  const vehTypes = [
    { value: 'BIKE', label: 'Bike' },
    { value: 'SCOOTER', label: 'Scooter' },
  ];
  // const districtAll = ['KATHMANDU', 'LALITPUR', 'BHAKTAPUR'];
  const [districtAll, setDistrictAll] = useState([]);
  // const [colorFilter, setColorFilter] = useState({ id: 'color', value: '' });
  // const [verified, setVerified] = useState({ id: 'verified', value: '' });
  const [vehType, setVehType] = useState({ id: 'vehicleType', value: '' });
  const [fuelType, setFuelType] = useState({ id: 'fuelType', value: '' });
  const [district, setDistrict] = useState({
    id: 'district',
    value: '',
  });
  const [overallFilter, setOverallFilter] = useState([]);

  const [condition, setCondition] = useState({ id: 'condition', value: '' });

  const [bikeDriven, setBikeDriven] = useState({ id: 'bikeDriven', value: '' });
  const [expectedPrice, setExpectedPrice] = useState({
    id: 'expectedPrice',
    value: '',
  });
  const [mileage, setMileage] = useState({
    id: 'mileage',
    value: '',
  });
  const [selectedMakeYear, setSelectedMakeYear] = useState(null);
  const [viewAllPageQuery, setViewAllPageQuery] = useState('');
  const debounceVehicleFilter = useDebounce(vehicleFilter, 1000);
  // const debounceColorFilter = useDebounce(colorFilter, 1000);
  const debounceBikeDriven = useDebounce(bikeDriven, 1000);
  const debounceExpectedPrice = useDebounce(expectedPrice, 1000);
  const debounceMileage = useDebounce(mileage, 1000);
  const { data: brandData } = useReadBrandNoAuthQuery();
  const [brandId, setBrandId] = useState({ id: 'brandName', value: '' });
  const [colored, setColored] = useState({ id: 'color', value: '' });
  const [isApproved, setIsApproved] = useState({
    id: 'isApproved',
    value: true,
  });

  // Listing Title
  const [listingTitle, setListingTitle] = useState('');

  // For setting viewAllPageQuery using query params from client/browser
  useEffect(() => {
    if (browseByQuery === 'featured') {
      setMainTag({ id: 'main', value: 'Featured' });
    } else if (browseByQuery === 'recentlyAdded') {
      setMainTag({ id: 'main', value: 'Recently Added' });
      setViewAllPageQuery(
        `?search="isApproved":true,"vehicleType":"${vehicleTypeQuery}"&noRegex=isApproved&sortBy="createdAt"&sortOrder=-1&limit=10`
      );
    } else if (browseByQuery === 'budget') {
      setViewAllPageQuery(
        `?search="expectedPrice":"${budgetRangeQuery}"&type=vehicleSell&noRegex=expectedPrice&limit=10`
      );
    } else if (browseByQuery === 'brand') {
      setViewAllPageQuery(
        `?search="brandName":"${brandNameQuery}","isApproved":true&noRegex=isApproved`
      );
    } else if (findQuery === 'true') {
      setViewAllPageQuery(
        `?search=${brandQuery === '' ? '' : `"brandName":"${brandQuery}",`}${
          budgetQuery === '' ? '' : `"expectedPrice":"${budgetQuery}",`
        }${makeYearQuery === '' ? '' : `"makeYear":"${makeYearQuery}-01-01",`}${
          districtQuery === '' ? '' : `"district":"${districtQuery}"`
        }&type=vehicleSell&noRegex=brandName,expectedPrice,makeYear,district`.replace(
          ',&type=vehicleSell',
          '&type=vehicleSell'
        )
      );
    } else {
      setMainTag({});
      setViewAllPageQuery('?null');
    }
  }, [browseByQuery, findQuery]);

  useEffect(() => {
    setIsApproved({ id: 'isApproved', value: true });
  }, []);

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    if (browseByQuery === 'featured') {
      const queryForFeatured = query.replace(
        '&search=',
        `&search="hasInFeature":true,`
      );
      setViewAllPageQuery(queryForFeatured);
    }
    // else if (browseByQuery === 'recentlyAdded' && vehicleTypeQuery) {
    //   const queryForRecentlyAdded = query.replace(
    //     `&search=`,
    //     `&search="vehicleType":"${vehicleTypeQuery}",`
    //   );
    //   setViewAllPageQuery(queryForRecentlyAdded);
    // }
    else if (browseByQuery === 'budget' && budgetRangeQuery) {
      setViewAllPageQuery(query);
    } else if (browseByQuery === 'brand' && brandNameQuery) {
      setViewAllPageQuery(query);
    } else if (findQuery === 'true') {
      if (selectedMakeYear) {
        const queryForFind = query.replace(
          `&search=`,
          `&search="makeYear":"${selectedMakeYear
            .getFullYear()
            .toString()}-01-01",`
        );
        const withMakeYearRegex = queryForFind.replace(
          `&noRegex=`,
          `&noRegex=makeYear,`
        );
        setViewAllPageQuery(withMakeYearRegex);
      } else {
        setViewAllPageQuery(query);
      }
    } else if (searchQuery) {
      const queryForGlobalSearch = query.replace(
        '&search=',
        `&search="vehicleName":"${searchQuery}",`
      );
      setViewAllPageQuery(queryForGlobalSearch);
    } else {
      // setSellerDataQuery(query);
      setViewAllPageQuery(query);
    }
  };

  useEffect(() => {
    const search = [
      debounceVehicleFilter,
      // debounceColorFilter,
      // verified,
      vehType,
      fuelType,
      district,
      brandId,
      colored,
      condition,
      debounceBikeDriven,
      debounceExpectedPrice,
      debounceMileage,
      isApproved,
    ].filter((obj) => obj?.value !== '');

    search.forEach((s) => {
      if (s.id === 'location.district') {
        s.id = 'district';
      }
    });
    setOverallFilter(search);
    setTags([...search, mainTag]);
    // search.map(s => {
    //   // Excluding isApproved from getting inside our tags
    //   if (s.id !== 'isApproved') {
    //     const tagKey = s.id;
    //     console.log(search);
    //     // console.log({ ...tags, [tagKey]: s.value });
    //     setTags({ ...tags, [tagKey]: s.value });
    //     setTags1(search);
    //   }
    // })
  }, [
    debounceVehicleFilter,
    // debounceColorFilter,
    // verified,
    vehType,
    fuelType,
    district,
    brandId,
    colored,
    condition,
    debounceBikeDriven,
    debounceExpectedPrice,
    debounceMileage,
    isApproved,
  ]);

  useEffect(() => {
    // sortBy looks like: [{id: 'vehicleName', desc: false}]
    getData({
      pageIndex: 0,
      pageSize: pageSize,
      sortBy: sortBy,
      filters: overallFilter,
    });
  }, [getData, pageSize, sortBy]);

  useEffect(() => {
    if (selectedSortBy === 'priceDesc') {
      setSortBy([{ id: 'expectedPrice', desc: true }]);
    } else if (selectedSortBy === 'priceAsc') {
      setSortBy([{ id: 'expectedPrice', desc: false }]);
    } else {
      setSortBy([]);
    }
  }, [selectedSortBy]);

  // For fetching data according to View All clicked
  const {
    data: viewAllPageData,
    // isLoading: isLoadingViewAllPage,
    isFetching: isFetchingViewAllPage,
  } = useReadSellerQuery(viewAllPageQuery);

  const {
    data: allDistrict,
    // isError: sellerError,
    // isFetching: isFetchingAllDistrict,
  } = useReadAllDistrictQuery();

  useEffect(() => {
    setDistrictAll(allDistrict);
  }, [allDistrict]);

  useEffect(() => {
    if (viewAllPageData) {
      setHasNextPage(viewAllPageData.hasNextPage);
      setListingTitle(`${viewAllPageData.totalDocs} vehicles found`);
    }
  }, [viewAllPageData]);

  const handleReset = () => {
    // Local states and refs
    setBikeDrivenRange([0, 1000000]);
    // if (budgetRangeQuery) {
    //   setExpectedPriceRange(budgetRangeQuery.split("-"));
    // } else {
    //   setExpectedPriceRange([0, 10000000]);
    // }
    setExpectedPriceRange([0, 10000000]);
    setMileageRange([0, 100]);
    vehNameEl.current.value = '';
    // For clearing react-select component
    setColorSelectClear(true);
    // setBrandSelectClear(true);
    // if (brandNameQuery) {
    //   // If brandNameQuery exists then we clear all the values and then set the value from our query param again.
    //   brandSelectRef.current.clearValue();
    //   brandSelectRef.current.setValue([{ value: brandNameQueryState, label: brandNameQueryState }]);
    // } else {
    //   brandSelectRef.current.clearValue();
    // }
    brandSelectRef.current.clearValue();
    districtSelectRef.current.clearValue();
    vehTypeSelectRef.current.clearValue();
    setConditionSelectClear(true);
    setFuelTypeSelectClear(true);
    // setDistrictSelectClear(true);
    setSelectedMakeYear(null);

    // For getData function
    setBikeDriven({
      id: 'bikeDriven',
      value: '',
    });
    setExpectedPrice({
      id: 'expectedPrice',
      value: '',
    });
    setMileage({
      id: 'mileage',
      value: '',
    });
    setVehicleFilter({
      id: 'vehicleName',
      value: '',
    });
    setColored({
      id: 'color',
      value: '',
    });
    setBrandId({
      id: 'brandName',
      value: '',
    });
    setCondition({
      id: 'condition',
      value: '',
    });
    setFuelType({
      id: 'fuelType',
      value: '',
    });
    setDistrict({
      id: 'location.district',
      value: '',
    });
  };

  const accordionItems = [
    {
      uuid: 1,
      heading: 'Name',
      content: (
        <div className="search relative">
          <SearchIcon className="h-5 w-5 absolute top-1/2 translate-y-[-60%] left-3" />
          <input
            type="search"
            placeholder=" Search By Vehicle Name"
            className="pl-[36px]"
            ref={vehNameEl}
            onChange={(e) => {
              setVehicleFilter({
                id: 'vehicleName',
                value: e.target.value,
              });
            }}
          />
        </div>
      ),
    },
    {
      uuid: 2,
      heading: 'Vehicle Type',
      content: (
        <Select
          isMulti={true}
          placeholder="Search By Vehicle Type"
          onChange={(selectedValue) => {
            setVehType({
              id: 'vehicleType',
              value: arrayToString(selectedValue),
            });
          }}
          options={vehTypes.map((option) => ({
            value: option.value,
            label: option.label,
          }))}
          ref={vehTypeSelectRef}
        />
        // <SearchWithSelect
        //   onChange={(selectedValue) => {
        //     setVehType({
        //       id: 'vehType',
        //       value: arrayToString(selectedValue),
        //     });
        //     setVehTypeSelectClear(false);
        //   }}
        //   options={vehTypes.map((option) => ({
        //     value: option.value,
        //     label: option.label,
        //   }))}
        //   placeholder="Vehicle Type"
        //   customClear={vehTypeSelectClear}
        // />
      ),
    },
    {
      uuid: 3,
      heading: 'Color',
      content: (
        <SearchWithSelect
          // loading={fetchingBrands}
          placeholder="Search By Color"
          value={[colored || null]}
          onChange={(selectedValue) => {
            // setFieldValue('brandId', selectedBrand.value);
            setColored({
              id: 'color',
              value: arrayToString(selectedValue),
            });
            // Don't forget to make it false
            setColorSelectClear(false);
          }}
          options={color?.map((option) => ({
            value: option,
            label: option,
          }))}
          customClear={colorSelectClear}
        />
      ),
    },
    {
      uuid: 4,
      heading: 'Brand',
      content: (
        <Select
          isMulti={true}
          placeholder="Search By Brand"
          onChange={(selectedBrand) => {
            setBrandId({
              id: 'brandName',
              value: arrayToString(selectedBrand),
            });
          }}
          options={brandData?.map((brand) => ({
            value: brand?.brandName,
            label: brand?.brandName,
          }))}
          ref={brandSelectRef}
        />
      ),
    },
    {
      uuid: 5,
      heading: 'Condition',
      content: (
        <SearchWithSelect
          onChange={(selectedValue) => {
            setCondition({
              id: 'condition',
              value: arrayToString(selectedValue),
            });
            setConditionSelectClear(false);
          }}
          options={conditionOptions.map((option) => ({
            value: option,
            label: option,
          }))}
          placeholder=" Search By Condition"
          customClear={conditionSelectClear}
        />
      ),
    },
    {
      uuid: 6,
      heading: 'Vehicle Driven',
      content: (
        <RangeSlider
          min={0}
          max={1000000}
          value={bikeDrivenRange}
          onChange={(value) => {
            const minMaxStr = value.join('-');
            setBikeDrivenRange(value);
            setBikeDriven({
              id: 'bikeDriven',
              value: minMaxStr,
            });
          }}
          postFix="Km"
          sliderName="Bike Driven (Km)"
        />
      ),
    },
    {
      uuid: 7,
      heading: 'Price',
      content: (
        <RangeSlider
          min={0}
          max={10000000}
          value={expectedPriceRange}
          onChange={(value) => {
            const minMaxStr = value.join('-');
            setExpectedPriceRange(value);
            setExpectedPrice({
              id: 'expectedPrice',
              value: minMaxStr,
            });
          }}
          postFix="NRs"
          sliderName="Expected Price (NRs)"
        />
      ),
    },
    {
      uuid: 8,
      heading: 'Mileage',
      content: (
        <RangeSlider
          min={0}
          max={100}
          value={mileageRange}
          onChange={(value) => {
            const minMaxStr = value.join('-');
            setMileageRange(value);
            setMileage({
              id: 'mileage',
              value: minMaxStr,
            });
          }}
          postFix="Kmpl"
          sliderName="Mileage (Kmpl)"
        />
      ),
    },
    {
      uuid: 9,
      heading: 'Fuel Type',
      content: (
        <SearchWithSelect
          onChange={(selectedValue) => {
            setFuelType({
              id: 'fuelType',
              value: arrayToString(selectedValue),
            });
            setFuelTypeSelectClear(false);
          }}
          options={vehicleFuelType.map((option) => ({
            value: option.value,
            label: option.label,
          }))}
          placeholder="Search By Fuel Type"
          customClear={fuelTypeSelectClear}
        />
      ),
    },
    {
      uuid: 10,
      heading: 'District',
      content: (
        <Select
          isMulti={true}
          placeholder="Search By District"
          onChange={(selectedDistrict) => {
            setDistrict({
              id: 'location.district',
              value: arrayToString(selectedDistrict),
            });
          }}
          options={districtAll?.map((district) => ({
            value: district?.district,
            label: district?.district,
          }))}
          ref={districtSelectRef}
        />
      ),
    },
  ];

  return (
    <div>
      {/* <UserLayout> */}
      {/* <UserHeader /> */}
      <main id="main">
        <section className="filter-section py-8">
          <div className="container">
            {/* {console.log('View All Page Query: ' + viewAllPageQuery)} */}
            {/* {console.log('Seller Data Query: ' + sellerDataQuery)} */}
          </div>
        </section>

        <section className="vehicle-listing-section pb-20">
          <div className="container">
            <div className="row">
              <div className="w-full sm:w-1/3 px-2 mb-3">
                <div className="toolbar-accordion-holder">
                  <ReactAccordions
                    AccordionItems={
                      findQuery === 'true'
                        ? accordionItems.concat({
                            uuid: 10,
                            heading: 'Make Year',
                            content: (
                              <DatePicker
                                selected={selectedMakeYear || null}
                                onChange={(date) => {
                                  setSelectedMakeYear(date);
                                }}
                                showYearPicker
                                dateFormat="yyyy"
                                yearItemNumber={9}
                                placeholderText="Search Make Year"
                                maxDate={new Date()}
                                isClearable
                              />
                            ),
                          })
                        : accordionItems
                    }
                    preExp={
                      browseByQuery === 'budget' && budgetRangeQuery
                        ? [7]
                        : browseByQuery === 'brand' && brandNameQuery
                        ? [4]
                        : browseByQuery === 'recentlyAdded' && vehicleTypeQuery
                        ? [2]
                        : findQuery === 'true'
                        ? [4, 7, 10, 11]
                        : searchQuery !== ''
                        ? [1]
                        : [1]
                    }
                    allowMulExp={true}
                  />
                </div>
                <div className="form-group pl-[35px] text-center">
                  <Button type="button" variant="error" onClick={handleReset}>
                    Reset
                  </Button>
                </div>
              </div>
              <div className="w-full sm:w-2/3 px-2 mb-3">
                {listingTitle ? (
                  <div className="row mb-5 px-3 border-b border-primary">
                    <div className="w-full mb-2">
                      <h4 className="text-xl">{listingTitle}</h4>
                    </div>
                    <div className="mb-1">
                      <span className="text-sm mr-2">Filtered By: </span>
                      <FilterTags
                        tags={tags}
                        vehTypeSelectRef={vehTypeSelectRef}
                        setVehicleFilter={setVehicleFilter}
                        setColored={setColored}
                        setColorSelectClear={setColorSelectClear}
                        vehNameEl={vehNameEl}
                        brandSelectRef={brandSelectRef}
                        setBrandId={setBrandId}
                        setConditionSelectClear={setConditionSelectClear}
                        setCondition={setCondition}
                        setBikeDrivenRange={setBikeDrivenRange}
                        setBikeDriven={setBikeDriven}
                        setExpectedPriceRange={setExpectedPriceRange}
                        setExpectedPrice={setExpectedPrice}
                        setMileageRange={setMileageRange}
                        setMileage={setMileage}
                        districtSelectRef={districtSelectRef}
                        setDistrict={setDistrict}
                        setFuelTypeSelectClear={setFuelTypeSelectClear}
                        setFuelType={setFuelType}
                        setSelectedMakeYear={setSelectedMakeYear}
                      />
                      {tags.length >= 3 ? (
                        <span
                          className="text-sm ml-2 text-red-400 capitalize hover:cursor-pointer hover:text-red-600"
                          onClick={handleReset}
                        >
                          Clear filters
                        </span>
                      ) : null}
                    </div>
                  </div>
                ) : null}
                <div className="row d-flex justify-end mb-3">
                  <div>
                    <label htmlFor="sortBy">Sort By:</label>
                    <select
                      name="sortBy"
                      onChange={(e) => {
                        setSelectedSortBy(e.target.value);
                      }}
                      value={selectedSortBy}
                    >
                      <option value="latest">Latest</option>
                      <option value="priceDesc">Price (High to Low)</option>
                      <option value="priceAsc">Price (Low to High)</option>
                    </select>
                  </div>
                </div>
                <div className="row">
                  {isFetchingViewAllPage ? (
                    <LoadingCard cardColsSize="two" />
                  ) : (browseByQuery || findQuery === 'true') &&
                    viewAllPageData?.docs.length ? (
                    viewAllPageData?.docs.map((data, i) => {
                      return (
                        <ListingCard
                          key={i}
                          route={`/vehicle-listing/${data?.id}`}
                          imgUrl={data?.bikeImagePath?.[0]?.imageUrl}
                          totalImages={data?.bikeImagePath.length - 1}
                          vehicleName={data?.vehicleName}
                          price={data?.expectedPrice}
                          bikeDriven={data?.bikeDriven}
                          bikeNumber={data?.bikeNumber}
                          ownershipCount={data?.ownershipCount}
                          id={data.id}
                          condition={data?.condition}
                          isSold={data?.isSold}
                          isVerified={data?.isVerified}
                          location={data?.location?.combineLocation}
                        />
                      );
                    })
                  ) : browseByQuery || findQuery === 'true' ? (
                    <div className="w-full">
                      <p className="text-sm text-center">No Data Found</p>
                    </div>
                  ) : null}
                  {(browseByQuery || findQuery === 'true') &&
                  viewAllPageData?.docs.length ? (
                    <div className="w-full flex justify-center items-center">
                      <div>
                        {hasNextPage && (
                          <Button
                            type="button"
                            variant="secondary"
                            disabled={!hasNextPage}
                            onClick={() => {
                              setPageSize((pageSize) => pageSize + 10);
                            }}
                            loading={isFetchingViewAllPage}
                          >
                            Load More
                          </Button>
                        )}
                      </div>
                    </div>
                  ) : null}
                  {isFetchingViewAllPage ? (
                    <LoadingCard cardColsSize="two" />
                  ) : !browseByQuery &&
                    findQuery !== 'true' &&
                    viewAllPageData?.docs.length ? (
                    viewAllPageData?.docs.map((data, i) => {
                      return (
                        <ListingCard
                          key={i}
                          route={`/vehicle-listing/${data?.id}`}
                          imgUrl={data?.bikeImagePath[0]?.imageUrl}
                          totalImages={data?.bikeImagePath?.length - 1}
                          vehicleName={data?.vehicleName}
                          price={data?.expectedPrice}
                          bikeDriven={data?.bikeDriven}
                          bikeNumber={data?.bikeNumber}
                          ownershipCount={data?.ownershipCount}
                          id={data.id}
                          condition={data?.condition}
                          isSold={data?.isSold}
                          isVerified={data?.isVerified}
                          location={data?.location?.combineLocation}
                        />
                      );
                    })
                  ) : !browseByQuery && findQuery !== 'true' ? (
                    <div className="w-full">
                      <p className="text-sm text-center">No Data Found</p>
                    </div>
                  ) : null}
                  {!browseByQuery &&
                  findQuery !== 'true' &&
                  viewAllPageData?.docs.length ? (
                    <div className="w-full flex justify-center items-center">
                      <div>
                        {hasNextPage && (
                          <Button
                            type="button"
                            variant="secondary"
                            disabled={!hasNextPage}
                            onClick={() => {
                              setPageSize((pageSize) => pageSize + 10);
                            }}
                            loading={isFetchingViewAllPage}
                          >
                            Load More
                          </Button>
                        )}
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
      {/* </UserLayout> */}
    </div>
  );
};

export default VehicleListingCom;
