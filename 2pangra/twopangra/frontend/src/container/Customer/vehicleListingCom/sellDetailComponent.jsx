import Link from 'next/link';
import 'react-accessible-accordion/dist/fancy-example.css';
import {
  // StarIcon,
  CheckCircleIcon,
  EyeIcon,
  DownloadIcon,
  LocationMarkerIcon,
  UsersIcon,
  CalendarIcon,
  TruckIcon,
  ArchiveIcon,
  // XCircleIcon,
  BadgeCheckIcon,
  ColorSwatchIcon,
} from '@heroicons/react/outline';
import ImgGallery from 'components/ImgGallery';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {
  useGetOfferStatusQuery,
  useReadSellerQuery,
  useReadSingleSellerQuery,
} from 'services/api/seller';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';

import CustomTabs from 'components/Tabs';
import ReactAccordions from 'components/Accordions/ReactAccordions';
import { getQueryStringForVehicle } from 'utils/getQueryStringForTable';
import Button from 'components/Button';
import { useSelector } from 'react-redux';
import ReactModal from 'components/ReactModal/ReactModal';
import Address from 'components/Location';
import Comment from 'components/Comment';
import { CityLastWord } from 'utils/LastWord';
import OfferPricePopUp from 'container/OfferPrices';

function SellDetailComponent() {
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };
  const [tab1, setTab1] = useState([]);
  const [tab2, setTab2] = useState([]);
  const [tab3, setTab3] = useState([]);
  const [overallFilter, setOverallFilter] = useState([]);
  const [sellerDataQuery, setSellerDataQuery] = useState('');
  const [combineLocation, setCombineLocation] = useState({
    id: 'location.combineLocation',
    value: '',
  });

  const AccordionItems = [
    {
      uuid: 1,
      heading: 'Basic & Features',
      content: (
        <div>
          <CustomTabs tabData={tab1} hasAccordion={false} />
        </div>
      ),
    },
    {
      uuid: 2,
      heading: 'Technical Specification',
      content: (
        <div>
          <CustomTabs tabData={tab2} hasAccordion={false} />
        </div>
      ),
    },
    {
      uuid: 3,
      heading: 'Dimensions & Wheels',
      content: (
        <div>
          <CustomTabs tabData={tab3} hasAccordion={false} />
        </div>
      ),
    },
  ];

  const [imageData, setImageData] = useState([]);

  const { data: vehicleDetails } = useReadSingleSellerQuery(router.query.id, {
    skip: !router.query.id,
  });
  const sellerId = router.query.id;

  const { data: offerStatus } = useGetOfferStatusQuery(router.query.id, {
    skip: !router.query.id,
  });

  const { data: sellerData } = useReadSellerQuery(sellerDataQuery);

  const sameLocationFilter = sellerData?.docs?.filter(
    (res) => res.id !== sellerId
  );
  // console.log('name', sellerData);

  useEffect(() => {
    const search = [combineLocation].filter((obj) => obj.value !== '');

    setOverallFilter(search);
  }, [combineLocation]);

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setSellerDataQuery(query);
  };

  useEffect(() => {
    getData({
      pageIndex: 0,
      pageSize: 5,
      sortBy: [],
      filters: overallFilter,
    });
  }, [getData, overallFilter]);

  useEffect(() => {
    if (vehicleDetails) {
      const images = vehicleDetails?.sellerValue?.bikeImagePath.map((img) => ({
        original: img.imageUrl,
        thumbnail: img.imageUrl,
      }));
      setImageData(images || []);
      setCombineLocation({
        id: 'location.combineLocation',
        value: vehicleDetails?.sellerValue?.location?.combineLocation,
      });
      setTab1([
        {
          heading: 'Basic Factors',
          list: [
            {
              label: 'Vehicle Type',
              data: vehicleDetails?.vehicleDetailValue?.vehicleType,
            },
            {
              label: 'Market Price',
              data: vehicleDetails?.vehicleDetailValue?.marketPrice,
            },
            {
              label: 'Fuel Type',
              data: vehicleDetails?.vehicleDetailValue?.fuelType,
            },
            {
              label: 'Transmission Type',
              data: vehicleDetails?.vehicleDetailValue?.basicFactor
                ?.transmissionType,
            },
            {
              label: 'Body Type',
              data: vehicleDetails?.vehicleDetailValue?.basicFactor?.bodyType,
            },
            {
              label: 'Seating Capacity',
              data: vehicleDetails?.vehicleDetailValue?.basicFactor
                ?.seatingCapacity,
            },
            {
              label: 'Fuel Capacity (l)',
              data: vehicleDetails?.vehicleDetailValue?.basicFactor
                ?.fuelCapacity,
            },
          ],
        },
        {
          heading: 'Option Features',
          list: [
            {
              label: 'Instrumentation',
              data: vehicleDetails?.vehicleDetailValue?.optionFeature?.instrumentation.join(
                ' , '
              ),
            },
            {
              label: 'Safety',
              data: vehicleDetails?.vehicleDetailValue?.optionFeature?.safety.join(
                ' , '
              ),
            },
            {
              label: 'Features',
              data: vehicleDetails?.vehicleDetailValue?.optionFeature?.features.join(
                ' , '
              ),
            },
            {
              label: 'Ignition Type',
              data: vehicleDetails?.vehicleDetailValue.optionFeature
                ?.ignitionType,
            },
            {
              label: 'Charging Point',
              data: vehicleDetails?.vehicleDetailValue.optionFeature
                ?.chargingPoint
                ? 'Available'
                : 'Not available',
            },
            {
              label: 'Speedometer',
              data:
                vehicleDetails?.vehicleDetailValue.optionFeature
                  ?.speedometer === 'DIGITAL'
                  ? 'Digital'
                  : vehicleDetails?.vehicleDetailValue.optionFeature
                      ?.speedometer === 'ANALOG'
                  ? 'Analog'
                  : 'Not available',
            },
            {
              label: 'Trip Meter',
              data:
                vehicleDetails?.vehicleDetailValue.optionFeature?.tripMeter ===
                'DIGITAL'
                  ? 'Digital'
                  : vehicleDetails?.vehicleDetailValue.optionFeature
                      ?.tripMeter === 'ANALOG'
                  ? 'Analog'
                  : 'Not available',
            },
            {
              label: 'Pass Switch',
              data: vehicleDetails?.vehicleDetailValue.optionFeature?.passSwitch
                ? 'Available'
                : 'Not available',
            },
            {
              label: 'Clock',
              data: vehicleDetails?.vehicleDetailValue.optionFeature?.clock
                ? 'Available'
                : 'Not available',
            },
            {
              label: 'Riding Mode',
              data: vehicleDetails?.vehicleDetailValue.optionFeature
                ?.ridingModes
                ? 'Available'
                : 'Not available',
            },
            {
              label: 'Charging At Home',
              data: vehicleDetails?.vehicleDetailValue.optionFeature
                ?.chargingAtHome
                ? 'Available'
                : 'Not available',
            },
            {
              label: 'Charging At Charging Station ',
              data: vehicleDetails?.vehicleDetailValue.optionFeature
                ?.chargingAtChargingStation
                ? 'Available'
                : 'Not available',
            },
          ],
        },
      ]);

      setTab2([
        {
          heading: 'Technical Factors',
          list: [
            {
              label: 'Front Brake System',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.frontBrakeSystem,
            },
            {
              label: 'Back Brake System',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.backBrakeSystem,
            },
            {
              label: 'Front Suspension',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.frontSuspension,
            },
            {
              label: 'Rear Suspension',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.rearSuspension,
            },
            {
              label: 'No of Gears',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.noOfGears
                ? vehicleDetails?.vehicleDetailValue.technicalFactor?.noOfGears
                : 'Not available',
            },
            {
              label: 'Drive Type',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.driveType
                ? vehicleDetails?.vehicleDetailValue.technicalFactor?.driveType
                : 'Not available',
            },
            {
              label: 'Clutch Type',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.clutchType
                ? vehicleDetails?.vehicleDetailValue.technicalFactor?.clutchType
                : 'Not available',
            },
            {
              label: 'Gear Pattern',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.clutchType
                ? vehicleDetails?.vehicleDetailValue.technicalFactor
                    ?.gearPattern
                : 'Not available',
            },

            {
              label: 'Headlight',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.headlight,
            },
            {
              label: 'Taillight',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.taillight,
            },
            {
              label: 'Starter',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor?.starter.join(
                ' , '
              ),
            },
            {
              label: 'Battery (V)',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor?.battery,
            },
            {
              label: 'Low Battery Indicator',
              data: vehicleDetails?.vehicleDetailValue.technicalFactor
                ?.lowBatteryIndicator
                ? 'Available'
                : 'Not available',
            },
          ],
        },
        {
          heading: 'Engine Factors',
          list: [
            {
              label: 'Displacement (cc)',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.displacement,
            },
            {
              label: 'Engine Type',
              data: vehicleDetails?.vehicleDetailValue.engineFactor?.engineType,
            },
            {
              label: 'No of Cylinders',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.noOfCylinder,
            },
            {
              label: 'Valves Per Cylinder',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.valvesPerCylinder,
            },
            {
              label: 'Valve Configuration',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.valveConfiguration,
            },
            {
              label: 'Fuel Supply System',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.fuelSupplySystem,
            },
            {
              label: 'Maximum Power (watt)',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.maximumPower,
            },
            {
              label: 'Maximum Torque (Nm)',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.maximumTorque,
            },
            {
              label: 'Lubrication',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.lubrication,
            },
            {
              label: 'Engine Oil',
              data: vehicleDetails?.vehicleDetailValue.engineFactor?.engineOil,
            },
            {
              label: 'Air Cleaner',
              data: vehicleDetails?.vehicleDetailValue.engineFactor?.airCleaner,
            },
            {
              label: 'Bore X Stroke',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.boreXStroke,
            },
            {
              label: 'Compression Ratio',
              data: vehicleDetails?.vehicleDetailValue.engineFactor
                ?.compressionRatio,
            },
            {
              label: 'Motor Power',
              data: vehicleDetails?.vehicleDetailValue.engineFactor?.motorPower
                ? vehicleDetails?.vehicleDetailValue.engineFactor?.motorPower
                : 'Not available',
            },
          ],
        },
      ]);

      setTab3([
        {
          heading: 'Dimensional Factors',
          list: [
            {
              label: 'Wheelbase (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.wheelBase,
            },
            {
              label: 'Overall Width (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.overallWidth,
            },
            {
              label: 'Overall Length (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.overallLength,
            },
            {
              label: 'Overall Height (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.overallHeight,
            },
            {
              label: 'Ground Clearance (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.groundClearance,
            },
            {
              label: 'Kerb Weight (kg)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.kerbWeight,
            },
            {
              label: 'Seat Height (mm)',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.doddleHeight,
            },
            {
              label: 'Bootspace',
              data: vehicleDetails?.vehicleDetailValue.dimensionalFactor
                ?.bootSpace
                ? vehicleDetails?.vehicleDetailValue.dimensionalFactor
                    ?.bootSpace
                : 'Not available',
            },
          ],
        },
        {
          heading: 'Wheel Tyre Factors',
          list: [
            {
              label: 'Front Wheel Size (mm)',
              data: vehicleDetails?.vehicleDetailValue.wheelTyreFactor
                ?.frontWheelSize,
            },
            {
              label: 'Rear Wheel Size (mm)',
              data: vehicleDetails?.vehicleDetailValue.wheelTyreFactor
                ?.rearWheelSize,
            },
            {
              label: 'Front Wheel Type',
              data: vehicleDetails?.vehicleDetailValue.wheelTyreFactor
                ?.frontWheelType,
            },
            {
              label: 'Rear Wheel Type',
              data: vehicleDetails?.vehicleDetailValue.wheelTyreFactor
                ?.rearWheelType,
            },

            {
              label: 'Steel Rims',
              data: vehicleDetails?.vehicleDetailValue.wheelTyreFactor
                ?.steelRims,
            },
          ],
        },
      ]);
    }
  }, [vehicleDetails]);

  return (
    <div>
      <main id="main">
        <section className="vehicle-detail-section py-8">
          <div className="container">
            <h1 className="h4 mb-1">
              {vehicleDetails?.sellerValue?.vehicleName}
            </h1>
            <div className="md:flex flex-wrap items-center justify-end mb-5  ">
              {/* *** dont delete */}
              {/* <div className="flex flex-wrap  mr-2 md:max-w-[75%] space-x-3 sm:space-x-6 items-center">
                <div className="rating flex space-x-2 ">
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-dark" />
                </div>
                <span>
                  <a
                    href="#"
                    className="text-sm text-textColor hover:text-primary"
                  >
                    39 Reviews
                  </a>
                </span>
                <span>
                  <a
                    href="#"
                    className="text-sm text-textColor hover:text-primary"
                  >
                    Write a review
                  </a>
                </span>
              </div> */}
              <div className="flex space-x-6 max-w-[290px] text-sm">
                {
                  vehicleDetails?.sellerValue?.isVerified ? (
                    <span>
                      <CheckCircleIcon className="w-4 text-secondary-dark mt-[-3px] inline-block mr-1" />
                      Verified
                    </span>
                  ) : null
                  // <span>
                  //   <XCircleIcon className="w-4 text-error mt-[-3px] inline-block mr-1" />
                  //   Unverified
                  // </span>
                }

                {vehicleDetails?.sellerValue?.updatedAt && (
                  <span className="tooltip-parent group">
                    <span className="tooltip">Created At</span>
                    <CalendarIcon className="w-4 text-secondary-dark mt-[-3px] inline-block mr-1 " />
                    {new Date(
                      vehicleDetails?.sellerValue?.updatedAt
                    ).toLocaleDateString()}
                    {/* ***note updatedAt is placed instead of created at */}
                  </span>
                )}
              </div>
            </div>
            <div className="row-sm">
              <div className="w-full lg:w-[55%] two-col-sm mb-5">
                <ImgGallery images={imageData} />
                {vehicleDetails?.sellerValue?.note && (
                  <div className="border border-gray-300 rounded-md p-5 mb-5 shadow-md bg-white bg-opacity-75">
                    <h3 className="h5 mb-2">Description of Vehicle</h3>
                    <p className="text-sm">
                      {vehicleDetails?.sellerValue?.note}
                    </p>
                  </div>
                )}
                <div className=" lg:hidden bg-white border border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                  <div className="flex flex-wrap justify-between items-center pb-4 mb-5 border border-t-0 border-l-0 border-r-0 border-b-gray-300">
                    <div>
                      <span className="text-xl mb-1 inline-block">Price</span>
                      <h2 className="text-[18px] sm:h4 text-primary-dark">
                        NRs{' '}
                        {vehicleDetails?.sellerValue?.expectedPrice &&
                          thousandNumberSeparator(
                            vehicleDetails?.sellerValue?.expectedPrice
                          )}
                      </h2>
                    </div>
                    <div className="space-y-2 text-right">
                      <span className="inline-block px-2 py-1 text-[13px] rounded-md bg-gray-300">
                        Used For {vehicleDetails?.sellerValue?.usedFor}
                        {/* ***********need to check usedFor */}
                      </span>
                      <ul className="text-sm font-semibold flex space-x-3">
                        <li>
                          <a href="#" className="text-textColor">
                            <DownloadIcon className="w-4 h-4 inline-block" />{' '}
                            Broucher
                          </a>
                        </li>
                        <li className=" text-textColor">
                          {' '}
                          <EyeIcon className="w-4 h-4 inline-block" />{' '}
                          {vehicleDetails?.sellerValue?.numViews} Views
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div>
                    <h3 className="h5 text-textColor mb-5">Vehicle Summary</h3>
                    <div className="row-sm">
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <span className="tooltip">Location</span>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <LocationMarkerIcon className="w-5 h-5 text-secondary-dark" />
                        </div>
                        {CityLastWord(
                          vehicleDetails?.sellerValue?.location?.combineLocation
                        ) ? (
                          <span className="name-icon">
                            {CityLastWord(
                              vehicleDetails?.sellerValue?.location
                                ?.combineLocation
                            )}
                          </span>
                        ) : (
                          <span className="name-icon">Not Available</span>
                        )}
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Mileage (Kmpl)</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-speedometer"
                            viewBox="0 0 16 16"
                          >
                            <path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z" />
                            <path
                              fillRule="evenodd"
                              d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"
                            />
                          </svg>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.mileage} kmpl
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Owner Count</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <UsersIcon className="w-5 h-5 text-secondary-dark" />
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.ownershipCount}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Fuel Type</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-droplet-half"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M7.21.8C7.69.295 8 0 8 0c.109.363.234.708.371 1.038.812 1.946 2.073 3.35 3.197 4.6C12.878 7.096 14 8.345 14 10a6 6 0 0 1-12 0C2 6.668 5.58 2.517 7.21.8zm.413 1.021A31.25 31.25 0 0 0 5.794 3.99c-.726.95-1.436 2.008-1.96 3.07C3.304 8.133 3 9.138 3 10c0 0 2.5 1.5 5 .5s5-.5 5-.5c0-1.201-.796-2.157-2.181-3.7l-.03-.032C9.75 5.11 8.5 3.72 7.623 1.82z"
                            />
                            <path
                              fillRule="evenodd"
                              d="M4.553 7.776c.82-1.641 1.717-2.753 2.093-3.13l.708.708c-.29.29-1.128 1.311-1.907 2.87l-.894-.448z"
                            />
                          </svg>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.fuelType}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Bike Driven (Km)</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <TruckIcon className="w-5 h-5 text-secondary-dark"></TruckIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.bikeDriven}Km
                        </span>
                      </div>

                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Color</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          {/* <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-droplet-half"
                            viewBox="0 0 16 16"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"
                            />
                          </svg> */}
                          <ColorSwatchIcon className="w-4 h-4 text-secondary-dark"></ColorSwatchIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.color}
                        </span>
                      </div>

                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Had Accident</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <ArchiveIcon className="w-4 h-4 text-secondary-dark"></ArchiveIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.hasAccident
                            ? 'Yes'
                            : 'No'}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Is Negotiable</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <span className="w-4 h-4 text-secondary-dark icon-handshake"></span>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.isNegotiable
                            ? 'Yes'
                            : 'No'}
                        </span>
                      </div>

                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Condition</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <BadgeCheckIcon className="w-4 h-4 text-secondary-dark"></BadgeCheckIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.condition}
                        </span>
                      </div>
                    </div>
                    <div className="button-holder text-center mt-4 ">
                      {vehicleDetails?.sellerValue?.isSold ? (
                        <Button type="button" disabled>
                          Sold Out
                        </Button>
                      ) : (
                        <>
                          <ReactModal
                            name="Book Now"
                            redirect={token ? '' : '/customer/login'}
                            modal={modal}
                          >
                            <Address
                              bookList={[vehicleDetails?.sellerValue?.id]}
                              setModalFunction={setModalFunction}
                              modal={modal}
                            ></Address>
                          </ReactModal>
                          {vehicleDetails?.sellerValue?.isNegotiable ===
                          true ? (
                            <ReactModal
                              variant="outlined"
                              name="Offer Price"
                              redirect={token ? '' : '/customer/login'}
                              modal={modal}
                            >
                              <OfferPricePopUp
                                setModalFunction={setModalFunction}
                                modal={modal}
                              ></OfferPricePopUp>
                            </ReactModal>
                          ) : null}
                        </>
                      )}
                    </div>
                  </div>
                </div>

                <div className="detail-page-accordion-holder">
                  <ReactAccordions
                    AccordionItems={AccordionItems}
                  ></ReactAccordions>
                </div>
              </div>

              <div className="w-full lg:w-[45%] two-col-sm mb-5">
                <div className="hidden bg-white bg-opacity-75 lg:block border border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                  <div className="flex flex-wrap justify-between items-center pb-4 mb-5 border border-t-0 border-l-0 border-r-0 border-b-gray-300">
                    <div>
                      <span className="text-xl mb-1 inline-block">Price</span>
                      <h2 className="text-lg sm:h4 text-primary-dark">
                        NRs{' '}
                        {vehicleDetails?.sellerValue?.expectedPrice &&
                          thousandNumberSeparator(
                            vehicleDetails?.sellerValue?.expectedPrice
                          )}
                      </h2>
                    </div>
                    <div className="space-y-2 text-right">
                      <span className="inline-block px-2 py-1 text-[13px] rounded-md bg-gray-300">
                        Used For {vehicleDetails?.sellerValue?.usedFor}
                      </span>
                      <ul className="text-sm font-semibold flex space-x-3">
                        <li>
                          <a href="#" className="text-textColor">
                            <DownloadIcon className="w-4 h-4 inline-block" />{' '}
                            Brochure
                          </a>
                        </li>
                        <li className=" text-textColor">
                          {' '}
                          <EyeIcon className="w-4 h-4 inline-block" />{' '}
                          {vehicleDetails?.sellerValue?.numViews} Views
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div>
                    <h3 className="h5 text-textColor mb-5">Vehicle Summary</h3>
                    <div className="row-sm">
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <span className="tooltip">Location</span>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <LocationMarkerIcon className="w-5 h-5 text-secondary-dark" />
                        </div>
                        {CityLastWord(
                          vehicleDetails?.sellerValue?.location?.combineLocation
                        ) ? (
                          <span className="name-icon">
                            {CityLastWord(
                              vehicleDetails?.sellerValue?.location
                                ?.combineLocation
                            )}
                          </span>
                        ) : (
                          <span className="name-icon">Not Available</span>
                        )}
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Mileage (Kmpl)</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-speedometer"
                            viewBox="0 0 16 16"
                          >
                            <path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z" />
                            <path
                              fillRule="evenodd"
                              d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"
                            />
                          </svg>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.mileage} kmpl
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Owner Count</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <UsersIcon className="w-5 h-5 text-secondary-dark" />
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.ownershipCount}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Fuel Type</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-droplet-half"
                            viewBox="0 0 16 16"
                          >
                            <path
                              fillRule="evenodd"
                              d="M7.21.8C7.69.295 8 0 8 0c.109.363.234.708.371 1.038.812 1.946 2.073 3.35 3.197 4.6C12.878 7.096 14 8.345 14 10a6 6 0 0 1-12 0C2 6.668 5.58 2.517 7.21.8zm.413 1.021A31.25 31.25 0 0 0 5.794 3.99c-.726.95-1.436 2.008-1.96 3.07C3.304 8.133 3 9.138 3 10c0 0 2.5 1.5 5 .5s5-.5 5-.5c0-1.201-.796-2.157-2.181-3.7l-.03-.032C9.75 5.11 8.5 3.72 7.623 1.82z"
                            />
                            <path
                              fillRule="evenodd"
                              d="M4.553 7.776c.82-1.641 1.717-2.753 2.093-3.13l.708.708c-.29.29-1.128 1.311-1.907 2.87l-.894-.448z"
                            />
                          </svg>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.fuelType}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Bike Driven (Km)</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <TruckIcon className="w-5 h-5 text-secondary-dark"></TruckIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.bikeDriven}Km
                        </span>
                      </div>

                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Color</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          {/* <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="18"
                            height="18"
                            fill="#ca7314"
                            className="bi bi-droplet-half"
                            viewBox="0 0 16 16"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="2"
                              d="M7 21a4 4 0 01-4-4V5a2 2 0 012-2h4a2 2 0 012 2v12a4 4 0 01-4 4zm0 0h12a2 2 0 002-2v-4a2 2 0 00-2-2h-2.343M11 7.343l1.657-1.657a2 2 0 012.828 0l2.829 2.829a2 2 0 010 2.828l-8.486 8.485M7 17h.01"
                            />
                          </svg> */}
                          <ColorSwatchIcon className="w-4 h-4 text-secondary-dark"></ColorSwatchIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.color}
                        </span>
                      </div>

                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Had Accident</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <ArchiveIcon className="w-4 h-4 text-secondary-dark"></ArchiveIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.hasAccident
                            ? 'Yes'
                            : 'No'}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Is Negotiable</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <span className="w-4 h-4 text-secondary-dark icon-handshake"></span>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.isNegotiable
                            ? 'Yes'
                            : 'No'}
                        </span>
                      </div>
                      <div className="w-[33.33%]  sm:w-[25%] four-col-sm text-center tooltip-parent group mb-5">
                        <div className="tooltip">Condition</div>
                        <div className="border border-primary-dark w-10 h-10 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                          <BadgeCheckIcon className="w-4 h-4 text-secondary-dark"></BadgeCheckIcon>
                        </div>
                        <span className="text-sm">
                          {vehicleDetails?.sellerValue?.condition}
                        </span>
                      </div>
                    </div>
                    <div className="button-holder text-center mt-4 ">
                      {vehicleDetails?.sellerValue?.isSold ? (
                        <Button type="button" disabled>
                          Sold Out
                        </Button>
                      ) : (
                        <>
                          <ReactModal
                            name="Book Now"
                            redirect={token ? '' : '/customer/login'}
                            modal={modal}
                          >
                            <Address
                              bookList={[vehicleDetails?.sellerValue?.id]}
                              setModalFunction={setModalFunction}
                              modal={modal}
                            ></Address>
                          </ReactModal>
                          {/* {console.log(
                            vehicleDetails?.sellerValue?.isNegotiable
                          )} */}

                          {vehicleDetails?.sellerValue?.isNegotiable ===
                          true ? (
                            <ReactModal
                              variant="outlined"
                              name="Offer Price"
                              redirect={token ? '' : '/customer/login'}
                              modal={modal}
                            >
                              <OfferPricePopUp
                                setModalFunction={setModalFunction}
                                modal={modal}
                              ></OfferPricePopUp>
                            </ReactModal>
                          ) : null}
                        </>
                      )}
                    </div>
                  </div>
                </div>

                {/* )} */}

                {offerStatus?.filter((offerr) => {
                  return loginInfo.customer?._id === offerr.createdBy;
                })?.length ? (
                  <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                    <h3 className="h5 mb-2">Offer Status</h3>

                    {offerStatus
                      ?.filter((offerr) => {
                        return loginInfo.customer?._id === offerr.createdBy;
                      })
                      ?.map((offer, i) => {
                        return (
                          <div
                            key={i}
                            className="list  border-b-2 border-gray-300 "
                          >
                            <span className="font-semibold">
                              {' '}
                              Offer Price:{' '}
                            </span>
                            {offer?.offerPrice}
                            <br />
                            <span className="font-semibold">
                              {' '}
                              Offer Status:{' '}
                            </span>
                            {offer?.status}
                            <br />
                            {offer?.finalOffer === undefined ? null : (
                              <div>
                                <span className="font-semibold">
                                  {' '}
                                  Final Offer:{' '}
                                </span>
                                {offer?.finalOffer}
                              </div>
                            )}

                            {/* <h8>Offer Price: {offer?.offerPrice} </h8>
                            <br />
                            <h8>OfferStatus: {offer?.status}</h8>
                            <br />
                            {offer?.finalOffer === undefined ? null : (
                              <h8>Final Offer: {offer?.finalOffer} </h8>
                            )} */}
                          </div>
                        );
                      })}
                  </div>
                ) : (
                  ''
                )}

                <div>
                  <Comment vehicleId={vehicleDetails?.sellerValue?.id} />
                </div>
                <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                  <h3 className="h5 mb-2">Why Buy From Dui Pangra</h3>
                  <p className="text-sm">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Alias officia obcaecati laborum veniam sequi harum incidunt
                    impedit nisi minima libero vero dignissimos, ex aliquid, qui
                    perspiciatis beatae voluptates ullam similique?
                  </p>
                </div>
                {/* {console.log('locaaaa', sameLocationFilter)} */}
                {sameLocationFilter?.length === 0 ? null : (
                  <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                    <h3 className="h5 mb-3">Listing in same location</h3>
                    <div className="vechile-listing-horizontal space-y-6 md:space-y-3">
                      {sameLocationFilter?.map((sellInfo, i) => {
                        return (
                          <div
                            key={i}
                            className="list flex flex-wrap border-b-2 border-gray-300 pb-1  max-w-[365px] sm:max-w-[600px] mx-auto last-of-type:border-0"
                          >
                            <figure className="w-full sm:w-[190px] mb-2 sm:mb-0 ">
                              <Link href={`/vehicle-listing/${sellInfo.id}`}>
                                <a className="w-full">
                                  <img
                                    src={sellInfo.bikeImagePath[0]?.imageUrl}
                                    alt="image description"
                                    className="h-[220px] sm:h-[140px] w-full object-cover"
                                  />
                                </a>
                              </Link>
                            </figure>
                            <div className="detail-holder flex-1 pl-3">
                              <div className="flex flex-wrap justify-between items-center pb-4 mb-2">
                                <div>
                                  <span className="inline-block">
                                    {sellInfo.vehicleName}
                                  </span>
                                  <h4 className="text-lg sm:h5 text-primary-dark">
                                    NRs{' '}
                                    {thousandNumberSeparator(
                                      sellInfo.expectedPrice
                                    )}
                                  </h4>
                                </div>
                              </div>
                              <div className="row-sm">
                                <div className=" w-[33.33%] three-col-sm text-center tooltip-parent group mb-5">
                                  <span className="tooltip">Location</span>
                                  <div className="border border-primary-dark w-8 h-8 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                                    <LocationMarkerIcon className="w-4 h-4 text-secondary-dark" />
                                  </div>

                                  {CityLastWord(
                                    vehicleDetails?.sellerValue?.location
                                      ?.combineLocation
                                  ) ? (
                                    <span className="text-sm">
                                      {CityLastWord(
                                        vehicleDetails?.sellerValue?.location
                                          ?.combineLocation
                                      )}
                                    </span>
                                  ) : (
                                    <span className="text-sm">
                                      Not Available
                                    </span>
                                  )}
                                </div>
                                <div className=" w-[33.33%] three-col-sm text-center tooltip-parent group mb-5">
                                  <div className="tooltip">Mileage (Kmpl)</div>
                                  <div className="border border-primary-dark w-8 h-8 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="16"
                                      height="16"
                                      fill="#ca7314"
                                      className="bi bi-speedometer"
                                      viewBox="0 0 16 16"
                                    >
                                      <path d="M8 2a.5.5 0 0 1 .5.5V4a.5.5 0 0 1-1 0V2.5A.5.5 0 0 1 8 2zM3.732 3.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707zM2 8a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 8zm9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5zm.754-4.246a.389.389 0 0 0-.527-.02L7.547 7.31A.91.91 0 1 0 8.85 8.569l3.434-4.297a.389.389 0 0 0-.029-.518z" />
                                      <path
                                        fillRule="evenodd"
                                        d="M6.664 15.889A8 8 0 1 1 9.336.11a8 8 0 0 1-2.672 15.78zm-4.665-4.283A11.945 11.945 0 0 1 8 10c2.186 0 4.236.585 6.001 1.606a7 7 0 1 0-12.002 0z"
                                      />
                                    </svg>
                                  </div>
                                  <span className="text-sm">
                                    {sellInfo.mileage}
                                  </span>
                                </div>
                                <div className=" w-[33.33%] three-col-sm text-center tooltip-parent group mb-5">
                                  <div className="tooltip">Owner Count</div>
                                  <div className="border border-primary-dark w-8 h-8 flex items-center justify-center rounded-[50%] mx-auto mb-1">
                                    <UsersIcon className="w-4 h-4 text-secondary-dark" />
                                  </div>
                                  <span className="text-sm">
                                    {sellInfo.ownershipCount}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}

export default SellDetailComponent;
