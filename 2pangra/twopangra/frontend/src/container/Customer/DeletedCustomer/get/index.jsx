import React from 'react';
import Image from 'next/image';
import AdminLayout from 'layouts/Admin';
import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';

// import Link from 'next/link';
// import Button from 'components/Button';

import { useReadDeletedCustomerQuery } from 'services/api/customer';

const DeletedCustomerDetails = ({
  canEdit = true,
  list = 'User List',
  profile = 'User Profile',
}) => {
  const router = useRouter();
  // const { addToast } = useToasts();

  const {
    data: deletedCustomerProfile,
    // error: customerProfileFetchError,
    isLoading: isLoadingCustomerProfile,
  } = useReadDeletedCustomerQuery(router.query.id, {
    skip: !router.query.id,
  });

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: `Deleted Customer List`,
      route: '/customer/view',
    },
    {
      routeName: ` Deleted Customer Profile`,
      route: '',
    },
  ];
  return (
    <AdminLayout
      documentTitle="Get Deleted Customer Profile"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Deleted Customer Profile</h1>

          {/* {canEdit && (
            <div className="flex justify-end">
              <Link href={`/customer/edit/${router.query.id}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
          )} */}

          {isLoadingCustomerProfile ? (
            'Loading...'
          ) : (
            <>
              <div className="row">
                <div className="three-col">
                  <div
                  // border border-gray-200 pt-3 pb-2 px-4 rounded-md
                  // className="relative"
                  >
                    <strong>{deletedCustomerProfile?.userName}&nbsp;</strong>
                    {deletedCustomerProfile?.profileImagePath?.imageUrl && (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${deletedCustomerProfile?.profileImagePath?.imageUrl}`}
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="mx-3">
                  <p>
                    <strong>Full Name:</strong> &nbsp;
                    {deletedCustomerProfile?.fullName}
                  </p>
                  <p>
                    <strong>Email:&nbsp;</strong>
                    {/* {brandDetails?.brandVehicleDescription
                      ? brandDetails.brandVehicleDescription
                      : 'N/A'} */}
                    {deletedCustomerProfile?.email}
                  </p>

                  <p>
                    <strong>Gender:</strong> &nbsp;
                    {deletedCustomerProfile?.gender &&
                      deletedCustomerProfile?.gender}
                  </p>
                  <p>
                    <strong>Mobile Number:</strong> &nbsp;
                    {deletedCustomerProfile?.mobile}
                  </p>

                  <p>
                    <strong>Date of Birth(AD):</strong> &nbsp;
                    {deletedCustomerProfile?.dob
                      ? new Date(
                          deletedCustomerProfile?.dob
                        ).toLocaleDateString()
                      : null}
                  </p>

                  <p>
                    <strong>Mobile verified:</strong> &nbsp;
                    {deletedCustomerProfile?.mobileVerified
                      ? 'Verified'
                      : 'Unverified'}
                  </p>
                  <p>
                    <strong>Email verified:</strong> &nbsp;
                    {deletedCustomerProfile?.emailVerified
                      ? 'Verified'
                      : 'Unverified'}
                  </p>
                  <p>
                    <strong>Receive Newsletter:</strong> &nbsp;
                    {deletedCustomerProfile?.receiveNewsletter ? 'Yes' : 'No'}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default DeletedCustomerDetails;
