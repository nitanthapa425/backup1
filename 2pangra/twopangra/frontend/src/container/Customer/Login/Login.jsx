import Head from 'next/head';
import CustomerLoginForm from '.';

const CustomerLogin = () => {
  return (
    <>
      <Head>
        <title>Customer Login</title>
      </Head>

      <main>
        <div className="flex flex-wrap min-h-[460px] h-screen">
          <div
            className="flex items-center h-[250px] w-full  md:h-auto md:w-[56%] lg:w-[50%] px-[15px] py-10 text-white relative md:rounded-br-[80px] lg:rounded-br-[150px]  overflow-hidden before:absolute before:inset-0 before:bg-primary-dark before:bg-opacity-60 bg-cover"
            style={{ backgroundImage: "url('/images/bike-racing.jpg')" }}
          >
            <div className="max-w-[500px] mx-auto z-20 relative">
              <div className="logo-holder text-center w-[200px] lg:w-[260px] mx-auto mb-3 md:mb-4">
                <a href="#">
                  <img src="../images/logo-white.svg" alt="2Pangra" />
                </a>
              </div>
              <p className="text-center mb-5 md:mb-10 lg:text-[18px] font-semibold md:tracking-wide">
                Nepal&apos;s finest two-wheeler buying and selling experience
                with trust, selection, and best quality.
              </p>
              <ul className="hidden md:block list-disc pl-5 lg:text-[18px]  space-y-2 mb-10">
                <li className="leading-loose">Time Efficient</li>
                <li className="leading-loose">Hassle Free</li>
                <li className="leading-loose">Easy to buy/sell and exchange</li>
                <li className="leading-loose">User Friendly Interface</li>
                <li className="leading-loose">
                  Well trained technician for any problems or issues
                </li>
                <li className="leading-loose">Trustworthy</li>
              </ul>
              <p className="text-left hidden md:block">
                For support :{' '}
                <a href="mailto:support@pangra.com" className="text-white ">
                  support@pangra.com
                </a>
              </p>
            </div>
          </div>
          <div className="h-full md:h-auto w-full bg-white z-20 rounded-t-[30px]  py-10 px-[25px] mt-[-25px] md:mt-0 md:rounded-none md:w-[44%] lg:w-[50%] ">
            <div className="max-w-[500px] mx-auto h-full relative flex md:items-center">
              <div className="mx-auto ">
                <h2 className="uppercase font-semibold text-gray-600 mb-2">
                  Login
                </h2>
                <CustomerLoginForm />
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default CustomerLogin;
