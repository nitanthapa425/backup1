import React, { useRef, useEffect } from 'react';
import { Form, Formik } from 'formik';

import Button from 'components/Button';

import Input from 'components/Input';
import { useToasts } from 'react-toast-notifications';

import Link from 'next/link';
import { customerForgotValidationSchema } from 'validation/customer.validation';
import { useCustomerForgotMutation } from 'services/api/customer';
const CustomerForgotPasswordContainer = () => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [
    createCustomerForgotPassword,
    { isError, error, isLoading, isSuccess },
  ] = useCustomerForgotMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('The OTP Code has been sent to your Mobile Number.', {
        appearance: 'success',
      });
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'you might have entered wrong Phone Number . Please try again later.',
        {
          appearance: 'error',
          autoDismiss: false,
        }
      );
    }
  }, [isError]);
  return (
    <>
      <Formik
        initialValues={{
          mobile: '',
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          createCustomerForgotPassword(values);
          setSubmitting(false);
        }}
        validationSchema={customerForgotValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">Forgot Password</h3>
                  <p className="text-lg">
                    Enter your Mobile Number and we will send OTP Code to reset
                    your password
                  </p>
                  <Input
                    label="Mobile Number"
                    name="mobile"
                    type="text"
                    placeholder="Mobile Number"
                  />

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Send
                    </Button>
                    <Button
                      type="button"
                      variant="outlined-error"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Clear
                    </Button>
                    <Link href="/customer/login">
                      <Button type="button" variant="link">
                        Back
                      </Button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CustomerForgotPasswordContainer;
