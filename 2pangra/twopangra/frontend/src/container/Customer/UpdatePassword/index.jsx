import React, { useRef, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';

import Button from 'components/Button';
import { PasswordUpdateSchema } from 'validation/passwordUpdate.validation';

// import AdminLayout from 'layouts/Admin';
import { useToasts } from 'react-toast-notifications';

import { useRouter } from 'next/router';
import PasswordInput from 'components/Input/PasswordInput';
import { useCustomerUpdateMutation } from 'services/api/customer';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
import { customerActions } from 'store/features/customerAuth/customerAuthSlice';
import { useDispatch } from 'react-redux';
const CustomerPasswordUpdateContainer = () => {
  const dispatch = useDispatch();
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);
  const handleCustomerLogout = () => {
    dispatch(customerActions.removeCustomerToken());
    router.replace('/customer/login');
  };

  const [createPasswordUpdate, { isError, error, isSuccess, isLoading }] =
    useCustomerUpdateMutation();

  const router = useRouter();
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'Error occurred while changing password. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('New Password is Updated successfully.', {
        appearance: 'success',
      });
      setChanged(false);
      handleCustomerLogout();
      // router.push('/customer/login');
    }
  }, [isSuccess]);

  return (
    // <AdminLayout documentTitle="Update Password">
    <Formik
      initialValues={{
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
      }}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        const payloadData = {
          newPassword: values.newPassword,
          oldPassword: values.currentPassword,
        };
        createPasswordUpdate(payloadData);
        // console.log(payloadData);
        setSubmitting(false);
      }}
      validationSchema={PasswordUpdateSchema}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <Form onChange={() => setChanged(true)}>
          <div className="container mt-4">
            {/* <button
                className="mb-3 hover:text-primary"
                onClick={() => router.back()}
              >
                Go Back
              </button> */}

            <div className="flex justify-center items-center ">
              <div
                className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
                style={{ position: 'relative' }}
              >
                <h3 className="mb-3">Update Password</h3>
                <div className="row-sm">
                  <PasswordInput
                    label="Current Password"
                    name="currentPassword"
                    placeholder="Current Password"
                  ></PasswordInput>

                  <PasswordInput
                    label="New Password"
                    name="newPassword"
                    placeholder="New Password"
                  ></PasswordInput>
                  <PasswordInput
                    label="Confirm Password"
                    name="confirmPassword"
                    placeholder="Confirm Password"
                  ></PasswordInput>
                </div>
                <div className="btn-holder mt-3">
                  <Button type="submit" disabled={!dirty || isLoading}>
                    Update
                  </Button>
                  <Button
                    variant="outlined-error"
                    type="button"
                    onClick={() => {
                      resetForm();
                      setChanged(false);
                    }}
                    disabled={!dirty || isLoading}
                  >
                    Clear
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
    // </AdminLayout>
  );
};

export default CustomerPasswordUpdateContainer;
