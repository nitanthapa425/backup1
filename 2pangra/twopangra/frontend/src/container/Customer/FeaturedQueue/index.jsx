import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Button from 'components/Button';
import { useToasts } from 'react-toast-notifications';
import { useAddToFeatureQueueMutation } from 'services/api/seller';
const AddToFeaturedBikeContainer = () => {
  const router = useRouter();
  const id = router?.query?.id;

  const { addToast } = useToasts();
  const [addToFeaturedBike, { isError, error, isSuccess }] =
    useAddToFeatureQueueMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast('Your bike has been successfully Added to featured bike.', {
        appearance: 'success',
      });
      router.push('/sell/view');
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'Error occurred while adding to featured bike. Please try again later.',
        {
          appearance: 'error',
          autoDismiss: false,
        }
      );
    }
  }, [isError]);

  return (
    <>
      <Head>
        <title>2Pangra | Add to Feature Bike</title>
      </Head>
      <div className="container">
        <div className="flex justify-center items-center min-h-screen">
          <div
            className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
            style={{ position: 'relative' }}
          >
            <h3 className="mb-3">Add To Featured Bike</h3>

            <p>Are you sure you want to Featured Bike ?</p>

            <div className="btn-holder mt-3">
              <Button
                type="button"
                onClick={() => {
                  addToFeaturedBike(id);
                }}
              >
                {' '}
                Featured Bike
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  //   router.push(`/vehicle-listing/${id}`);
                  router.push(`/`);
                }}
              >
                Cancel
              </Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddToFeaturedBikeContainer;
