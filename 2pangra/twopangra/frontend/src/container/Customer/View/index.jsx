import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import {
  useDeleteCustomerMutation,
  useReadAllCustomerQuery,
} from 'services/api/customer';
import {
  getQueryStringForCustomerTable,
  // getQueryStringForTable,
  // getQueryStringForTable,
} from 'utils/getQueryStringForTable';

import {
  DateColumnFilter,
  MultiSelectFilter,
  SelectColumnFilter,
} from 'components/Table/Filter';

function ViewCustomerContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [CustomerDataQuery, setCustomerDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();

  const {
    data: customerData,
    isError: customerError,
    isFetching: isLoadingCustomer,
  } = useReadAllCustomerQuery(CustomerDataQuery);
  const {
    data: customerDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllCustomerQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (customerDataAll) {
      setTableDataAll(
        customerDataAll.docs.map((customers) => {
          return {
            fullName: customers?.fullName,
            mobile: customers?.mobile,
            email: customers?.email,
            condition: customers?.condition,
            gender: customers?.gender,
            dob: customers?.dob,
            status: customers?.status,
            phoneVerified: customers?.phoneVerified ? 'Verified' : 'Unverified',
            emailVerified: customers?.emailVerified ? 'Verified' : 'Unverified',
            receiveNewsletter: customers?.receiveNewsletter ? 'Yes' : 'No',
            id: customers?.id,
            addedByAdmin: customers?.addedByAdmin ? 'Yes' : 'No',
          };
        })
      );
    }
  }, [customerDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteCustomerMutation();

  const columns = useMemo(
    () => [
      {
        id: 'fullName',
        Header: 'Full Name',
        accessor: 'fullName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      // {
      //   id: 'middleName',
      //   Header: 'Middle Name',
      //   accessor: 'middleName',
      //   Cell: ({ cell: { value } }) => value || '-',
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      // {
      //   id: 'lastName',
      //   Header: 'Last Name',
      //   accessor: 'lastName',
      //   Cell: ({ cell: { value } }) => value || '-',
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: 'mobile',
        Header: 'Mobile Number',
        accessor: 'mobile',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'email',
        Header: 'Email',
        accessor: 'email',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'dob',
        Header: 'Date of Birth',
        accessor: 'dob',
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || '-',
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'gender',
        Header: 'Gender',
        accessor: 'gender',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        // Filter: SelectColumnFilter,
        Filter: MultiSelectFilter,
        possibleFilters: [
          {
            label: 'Male',
            value: 'Male',
          },
          {
            label: 'Female',
            value: 'Female',
          },
          {
            label: 'Other',
            value: 'Other',
          },
        ],
      },
      {
        id: 'phoneVerified',
        Header: 'Phone Verified',
        accessor: 'phoneVerified',
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'verified',
            value: true,
          },
          {
            label: 'unverified',
            value: false,
          },
        ],
      },
      {
        id: 'emailVerified',
        Header: 'Email Verified',
        accessor: 'emailVerified',
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'verified',
            value: true,
          },
          {
            label: 'unverified',
            value: false,
          },
        ],
      },
      // {
      //   id: 'status',
      //   Header: 'Status',
      //   accessor: 'status',
      //   // Cell: ({ cell: { value } }) => {
      //   //   if (value) {
      //   //     return 'verified';
      //   //   } else {
      //   //     return 'unverified';
      //   //   }
      //   // },
      //   Cell: ({ cell: { value } }) => {
      //     if (value === null || value === undefined) return '-';
      //     else if (value === 'active')
      //       return <span className="badge badge-green">Active</span>;
      //     else return <span className="badge badge-red">Inactive</span>;
      //   },

      //   canBeSorted: true,
      //   canBeFiltered: true,
      //   Filter: SelectColumnFilter,
      //   possibleFilters: [
      //     {
      //       label: 'active',
      //       value: 'active',
      //     },
      //     {
      //       label: 'inactive',
      //       value: 'inactive',
      //     },
      //   ],
      // },

      {
        id: 'receiveNewsletter',
        Header: 'Receive Newsletter',
        accessor: 'receiveNewsletter',
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'yes',
            value: true,
          },
          {
            label: 'no',
            value: false,
          },
        ],
      },

      // {
      //   id: 'addedByAdmin',
      //   Header: 'Added By Admin',
      //   accessor: 'addedByAdmin',
      //   Cell: ({ cell: { value } }) => value || '-',

      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
    ],

    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForCustomerTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setCustomerDataQuery(query);
  };

  useEffect(() => {
    if (customerData) {
      setPageCount(customerData.totalPages);
      setTotalData(customerData.totalDocs);

      setTableData(
        customerData.docs.map((customers) => {
          return {
            fullName: customers?.fullName,
            mobile: customers?.mobile,
            email: customers?.email,
            condition: customers?.condition,
            gender: customers?.gender,
            dob: customers?.dob,
            status: customers?.status,
            phoneVerified: customers?.phoneVerified ? 'Verified' : 'Unverified',
            emailVerified: customers?.emailVerified ? 'Verified' : 'Unverified',
            receiveNewsletter: customers?.receiveNewsletter ? 'Yes' : 'No',
            id: customers?.id,
          };
        })
      );
    }
  }, [customerData]);
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Customer List',
      route: '',
    },
  ];

  return (
    <AdminLayout documentTitle="View Customer" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Table
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={customerError}
            isLoadingData={isLoadingCustomer}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="customer/edit"
            viewRoute="customer/details"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            // addPage={{
            //   page: 'Add Customer',
            //   route: '/customer/add',
            // }}
            tableName="Customer/s"
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewCustomerContainer;
