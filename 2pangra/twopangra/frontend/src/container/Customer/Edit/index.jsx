import React, { useRef, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import { useToasts } from 'react-toast-notifications';

import Button from 'components/Button';

import Input from 'components/Input';
// import AdminLayout from 'layouts/Admin';

import {
  useReadCustomerQuery,
  useUpdateCustomerMutation,
} from 'services/api/customer';

import { customerUpdateValidationSchema } from 'validation/customer.validation';
import Popup from 'components/Popup';
// import { useRouter } from 'next/router';
// import PasswordInput from 'components/Input/PasswordInput';
import Select from 'components/Select';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { beforeAfterDate } from 'utils/beforeAfterdate';
import UserLayout from 'layouts/User';
import { useRouter } from 'next/router';
import DropZone from 'components/DropZone';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';

const CustomerEdit = () => {
  const router = useRouter();

  const customerId = router?.query?.id;

  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const genders = ['Male', 'Female', 'Other'];

  const formikBag = useRef();
  const {
    data: customerProfile,
    // error: customerProfileFetchError,
    // isLoading: isLoadingCustomerProfile,
  } = useReadCustomerQuery(customerId, {
    skip: !customerId,
  });

  const [updateCustomer, { isError, isSuccess, error }] =
    useUpdateCustomerMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('Customer details has been updated successfully.', {
        appearance: 'success',
      });
      setChanged(false);

      router.push(`/customer/details/${customerId}`);
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message,
        // || 'Error occurred while adding customer. Please try again later.',
        {
          appearance: 'error',
          autoDismiss: false,
        }
      );
    }
  }, [isError]);
  const initialValue = {
    firstName: customerProfile?.firstName,
    middleName: customerProfile?.middleName,
    lastName: customerProfile?.lastName,
    mobile: customerProfile?.mobile,
    email: customerProfile?.email,
    gender: customerProfile?.gender,
    dob: customerProfile?.dob,
    profileImagePath: customerProfile?.profileImagePath,
  };

  let BreadCrumbList = [];

  BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Edit Customer',
      route: '/customer/edit',
    },
  ];

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('profileImagePath', newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('profileImagePath', {});
    }
  };

  return (
    <UserLayout
      documentTitle={'Add New Customer'}
      BreadCrumbList={BreadCrumbList}
    >
      <Formik
        initialValues={initialValue}
        onSubmit={(values, { resetForm }) => {
          const payloadData = {
            id: values.id,
            firstName: values.firstName,
            middleName: values.middleName,
            lastName: values.lastName,
            email: values.email,
            mobile: values.mobile,
            gender: values.gender,
            dob: values.dob,
            profileImagePath: values?.profileImagePath,
          };
          updateCustomer({ ...payloadData, id: customerId });
        }}
        validationSchema={customerUpdateValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div className="container mt-4">
            <h3 className="mb-3">Edit Customer Details</h3>

            <Form onChange={() => setChanged(true)}>
              <div className="row-sm  pt-5 pb-2">
                <div className="three-col-sm">
                  <Input
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder="E.g: John"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    label="Middle Name"
                    name="middleName"
                    type="text"
                    placeholder="E.g: Jung"
                    required={false}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder="E.g: Deo"
                  />
                </div>
                {/* <div className="three-col-sm">
                  <PasswordInput
                    label="Password"
                    name="password"
                    placeholder="Password"
                  ></PasswordInput>
                </div> */}
                {/* <div className="three-col-sm">
                  <PasswordInput
                    label="Confirm Password"
                    name="confirmPassword"
                    placeholder="Confirm Password"
                  ></PasswordInput>
                </div> */}
                <div className="three-col-sm">
                  <Input
                    label="Mobile Number"
                    name="mobile"
                    type="text"
                    placeholder="E.g: 98XXXXXXXX"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="E.g: john320@gmail.com"
                  />
                </div>
                <div className="three-col-sm">
                  <label htmlFor="">
                    Date Of Birth(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={values?.dob ? new Date(values?.dob) : null}
                    onChange={(date) => {
                      setFieldValue('dob', date?.toLocaleDateString());
                      setFieldTouched('dob');
                    }}
                    onBlur={() => {
                      setFieldTouched('dob');
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    // minDate={beforeAfterDate(new Date(), 0, 0, 0)}
                    maxDate={beforeAfterDate(new Date(), 0, 0, -16)}
                  />

                  {touched.dob && errors.dob && (
                    <div className="text-primary-dark">{errors.dob}</div>
                  )}
                </div>
                <div className="three-col-sm">
                  <Select label="Gender" name="gender">
                    <option value="">Select Gender</option>

                    {genders.map((gender, i) => (
                      <option key={i} value={gender}>
                        {gender}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  <div className="">
                    <div>
                      <DropZone
                        label="Upload Profile Image"
                        currentFiles={
                          Object.keys(values.profileImagePath || {}).length
                            ? [values.profileImagePath]
                            : []
                        }
                        setNewFiles={handleProfileImagePath}
                        handleRemoveFile={handleRemoveProfileImagePath}
                        error={
                          touched?.profileImagePath
                            ? errors?.profileImagePath
                            : ''
                        }
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="btn-holder mt-2">
                <Button type="submit" disabled={!dirty}>
                  Update
                </Button>
                <Button
                  variant="outlined"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </UserLayout>
  );
};

export default CustomerEdit;
