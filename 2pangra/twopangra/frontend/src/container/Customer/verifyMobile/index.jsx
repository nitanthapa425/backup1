import { useRouter } from 'next/router';
import React, { useRef, useEffect } from 'react';

import { useToasts } from 'react-toast-notifications';
import Button from 'components/Button';
import { Form, Formik } from 'formik';

import { PhoneVerificationValidationSchema } from 'validation/customer.validation';
import Input from 'components/Input';
import {
  useVerifyPhoneMutation,
  useResendOtpMutation,
} from 'services/api/customer';
import { useDispatch, useSelector } from 'react-redux';
import { customerActions } from 'store/features/customerAuth/customerAuthSlice';

const VerifyPhone = () => {
  const { addToast } = useToasts();
  const customerInfo = useSelector((state) => state.customerAuth);
  // console.log(customerInfo);
  const formikBag = useRef();
  const router = useRouter();
  const dispatch = useDispatch();
  const [
    verifyPhone,
    {
      isLoading: isLoadingVerifyPhone,
      isSuccess: isSuccessVerifyPhone,
      isError: isErrorVerifyPhone,
      error: errorVerifyPhone,
    },
  ] = useVerifyPhoneMutation();
  const [
    resendToken,
    {
      // isLoading: isLoadingResendToken,
      isSuccess: isSuccessResendToken,
      isError: isErrorResendToken,
      // error: errorResendToken,
      data: dataResendToken,
    },
  ] = useResendOtpMutation();

  const ReSend = () => {
    resendToken({
      mobile: customerInfo.phone,
    });
  };

  useEffect(() => {
    if (isSuccessResendToken) {
      addToast('Congratulation Otp has been resent successfully.', {
        appearance: 'success',
      });
      dispatch(customerActions.setOtpId(dataResendToken.sendOtp.id));
    }
  }, [isSuccessResendToken]);

  useEffect(() => {
    if (isErrorResendToken) {
      addToast('Sorry we could not send Otp', {
        appearance: 'error',
      });
      // dispatch(customerActions.setPhone(dataResendToken.mobile));
    }
  }, [isSuccessResendToken]);

  useEffect(() => {
    if (isSuccessVerifyPhone) {
      addToast('Congratulation your mobile has been verified successfully.', {
        appearance: 'success',
      });
      router.push('/customer/login');
    }
  }, [isSuccessVerifyPhone]);

  useEffect(() => {
    if (isErrorVerifyPhone) {
      addToast(errorVerifyPhone?.data?.message, {
        appearance: 'error',
      });
    }
  }, [isErrorVerifyPhone]);

  if (isLoadingVerifyPhone) {
    return <div>Loading...</div>;
  }

  if (isErrorVerifyPhone) {
    return <div>Sorry your Phone could not be verified at this time</div>;
  }
  // const phoneNumber = localStorage.getItem('phone');
  // console.log(localStorage.getItem('otpId'));
  return (
    <>
      <Formik
        initialValues={{
          token: '',
        }}
        onSubmit={(values, { resetForm }) => {
          const payloadData = {
            // id: localStorage.getItem('otpId'),
            id: customerInfo.otpId,

            token: values.token,
          };

          // console.log(payloadData);

          verifyPhone(payloadData);
        }}
        validationSchema={PhoneVerificationValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div
                  className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
                  style={{ position: 'relative' }}
                >
                  <h1 className="mb-1 h4">Verify your phone Number.</h1>
                  <span className="mb-3 block ">
                    Please enter the 6 digit code sent to your mobile phone
                  </span>
                  {/* <strong>
                    Mobile Number: {localStorage.getItem('phone')}
                  </strong> */}

                  <div className="row-sm mb-3">
                    <Input
                      label="Verification Code"
                      name="token"
                      type="text"
                      placeholder=""
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      disabled={
                        isSubmitting ||
                        !dirty ||
                        isLoadingVerifyPhone ||
                        isLoadingVerifyPhone
                      }
                    >
                      Verify
                    </Button>
                    <Button
                      variant="secondary"
                      onClick={() => {
                        ReSend();
                      }}
                    >
                      Resend
                    </Button>
                    <Button
                      variant="outlined-error"
                      type="button"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={
                        isSubmitting ||
                        !dirty ||
                        isLoadingVerifyPhone ||
                        isLoadingVerifyPhone
                      }
                    >
                      Clear
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default VerifyPhone;
