import React from 'react';
import Image from 'next/image';
import AdminLayout from 'layouts/Admin';
import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';

import Link from 'next/link';
import Button from 'components/Button';

import { useReadCustomerQuery } from 'services/api/customer';

const CustomerDetails = ({
  canEdit = true,
  list = 'User List',
  profile = 'User Profile',
}) => {
  const router = useRouter();
  // const { addToast } = useToasts();

  const {
    data: customerProfile,
    // error: customerProfileFetchError,
    isLoading: isLoadingCustomerProfile,
  } = useReadCustomerQuery(router.query.id, {
    skip: !router.query.id,
  });

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: `Customer List`,
      route: '/customer/view',
    },
    {
      routeName: ` Customer Profile`,
      route: '',
    },
  ];
  return (
    <AdminLayout
      documentTitle="Get Customer Profile"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          {canEdit && (
            <div className="flex justify-end">
              <Link href={`/customer/edit/${router.query.id}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
          )}

          {isLoadingCustomerProfile ? (
            'Loading...'
          ) : (
            <>
              <div className="row">
                <div className="three-col">
                  <div
                  // border border-gray-200 pt-3 pb-2 px-4 rounded-md
                  // className="relative"
                  >
                    <strong>{customerProfile?.userName}&nbsp;</strong>
                    {customerProfile?.profileImagePath?.imageUrl ? (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${customerProfile?.profileImagePath?.imageUrl}`}
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    ) : (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src="/user.JPG"
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                    rounded
                    cursor-pointer
                    transition duration-200 ease-in-out
                    transform  hover:scale-125"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="mx-3">
                  <p>
                    <strong>Full Name:</strong> &nbsp;
                    {customerProfile?.fullName}
                  </p>
                  <p>
                    <strong>Email:&nbsp;</strong>
                    {/* {brandDetails?.brandVehicleDescription
                      ? brandDetails.brandVehicleDescription
                      : 'N/A'} */}
                    {customerProfile?.email || 'Not Available'}
                  </p>

                  <p>
                    <strong>Gender:</strong> &nbsp;
                    {customerProfile?.gender && customerProfile?.gender}
                  </p>
                  <p>
                    <strong>Mobile Number:</strong> &nbsp;
                    {customerProfile?.mobile}
                  </p>

                  <p>
                    <strong>Date of Birth(AD):</strong> &nbsp;
                    {customerProfile?.dob
                      ? new Date(customerProfile?.dob).toLocaleDateString()
                      : null}
                  </p>
                  {/* <p>
                    <strong>Status:</strong> &nbsp;
                    {customerProfile?.status}
                  </p> */}

                  <p>
                    <strong>Mobile verified:</strong> &nbsp;
                    {customerProfile?.phoneVerified ? 'Verified' : 'Unverified'}
                  </p>
                  <p>
                    <strong>Email verified:</strong> &nbsp;
                    {customerProfile?.emailVerified ? 'Verified' : 'Unverified'}
                  </p>
                  <p>
                    <strong>Receive Newsletter:</strong> &nbsp;
                    {customerProfile?.receiveNewsletter ? 'Yes' : 'No'}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default CustomerDetails;
