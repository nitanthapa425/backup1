import { DateColumnFilter } from 'components/Table/Filter';
import Table from 'components/Table/table';
import { useState, useEffect, useMemo } from 'react';
import { useReadAllLogQuery, useReadLogQuery } from 'services/api/log';
import { getQueryStringForLog } from 'utils/getQueryStringForTable';
function ViewLog() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [dataQuery, setDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataNotAll,
    isError: isErrorDataNotAll,
    isFetching: isLoadingDataNotAll,
  } = useReadLogQuery(dataQuery, {});

  const { data: dataAll, isFetching: isLoadingAll } = useReadAllLogQuery(
    '?&sortBy=createdAt&sortOrder=-1',
    {
      skip: skipTableDataAll,
    }
  );

  useEffect(() => {
    if (dataAll) {
      setTableDataAll(
        dataAll?.docs?.map((sellers) => {
          return {
            category: sellers?.category,
            logType: sellers?.logType,
            logDescription: sellers?.logDescription,
            createdAt: sellers?.createdAt,
            createdByName: sellers?.createdByName,
            // status: sellers?.status,
            id: sellers?.id,
          };
        })
      );
    }
  }, [dataAll]);

  const columns = useMemo(
    () => [
      {
        id: 'createdAt',
        Header: 'Date',
        accessor: 'createdAt',
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || '-',
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'category',
        Header: 'Category',
        accessor: 'category',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'logType',
        Header: 'Log Type',
        accessor: 'logType',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'logDescription',
        Header: 'Message',
        accessor: 'logDescription',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'createdByName',
        Header: 'Done By',
        accessor: 'createdByName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForLog(pageIndex, pageSize, sortBy, filters);

    setDataQuery(query);
  };

  useEffect(() => {
    if (dataNotAll) {
      setPageCount(dataNotAll.totalPages);
      setTotalData(dataNotAll.totalDocs);

      setTableData(
        dataNotAll?.docs?.map((sellers) => {
          return {
            category: sellers?.category,
            logType: sellers?.logType,
            logDescription: sellers?.logDescription,
            createdAt: sellers?.createdAt,
            // createdBy: sellers?.createdBy,
            createdByName: sellers?.createdByName,
            // updatedAt: sellers?.updatedAt,
            // status: sellers?.status,
            id: sellers?.id,
          };
        })
      );
    }
  }, [dataNotAll]);

  return (
    <section className="mt-3">
      <div className="container">
        <h3 className="mb-3">Log List</h3>
        <Table
          tableName="Log/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={isErrorDataNotAll}
          isLoadingData={isLoadingDataNotAll}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          hasExport={true}
        />
      </div>
    </section>
  );
}

export default ViewLog;
