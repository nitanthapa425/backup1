// import Table from 'components/Table/table';
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import //   useDeleteVehicleMutation,
//   useReadVehicleCustomQuery,
'services/api/vehicle';
import { MultiSelectFilter, SelectColumnFilter } from 'components/Table/Filter';
import Table from 'components/Table/table';
// import {
//   useDeleteBookedListMutation,
//   useReadAllBookQuery,
// } from 'services/api/adminCartList';
import {
  useDeleteBookedListMutation,
  useReadAllBookInvQuery,
} from 'services/api/seller';
import { getQueryStringForBook } from 'utils/getQueryStringForTable';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import MeetingSchedule from 'container/MeetingSchedual/MeetingSchedual';
import { color } from 'constant/constant';
import ReactModal from 'components/ReactModal/ReactModal';
// import ReactModal from 'components/ReactModal/ReactModal';
function AdminBook() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [bookQuery, setBookQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  // const {
  //   data: dataBook,
  //   isError: isErrorBook,
  //   isFetching: isFetchingBook,
  // } = useReadAllBookQuery(bookQuery);

  const {
    data: dataBook,
    isError: isErrorBook,
    isFetching: isFetchingBook,
  } = useReadAllBookInvQuery(bookQuery);

  // const {
  //   data: dataAllBook,
  //   // isError: isVehicleErrorAll,
  //   isFetching: isFetchingAllBook,
  // } = useReadAllBookQuery('?&sortBy=createdAt&sortOrder=-1', {
  //   skip: skipTableDataAll,
  // });

  const {
    data: dataAllBook,
    // isError: isVehicleErrorAll,
    isFetching: isFetchingAllBook,
  } = useReadAllBookInvQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  //   console.log()

  useEffect(() => {
    if (dataAllBook) {
      setTableDataAll(
        dataAllBook.docs.map((value) => {
          return {
            id: value.id || '-',
            clientMobile: value.clientMobile || '-',
            clientName: value.clientName || '-',
            bikeDriven: value.bikeDriven || '-',
            color: value.color || '-',
            isVerified: value.isVerified || '-',
            numViews: value.numViews || '-',
            price: value.price || '-',
            vehicleName: value.vehicleName || '-',
            meetingTime: value?.meetingSchedule?.meetingDateTime,
          };
        })
      );
    }
  }, [dataAllBook]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteBookedListMutation();

  const columns = useMemo(
    () => [
      {
        id: 'id',
        Header: 'id',
        accessor: 'id',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'clientName',
        Header: 'Client Name',
        accessor: 'clientName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'clientMobile',
        Header: 'Client Mobile',
        accessor: 'clientMobile',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeDriven',
        Header: 'Bike Driven',
        accessor: 'bikeDriven',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'color',
        Header: 'Color',
        accessor: 'color',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isVerified',
        Header: 'Verified',
        accessor: 'isVerified',
        Cell: ({ cell: { value } }) => (value ? 'Verified' : 'Unverified'),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'verified',
            value: true,
          },
          {
            label: 'unverified',
            value: false,
          },
        ],
      },
      {
        id: 'numViews',
        Header: 'Views',
        accessor: 'numViews',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'price',
        Header: 'Price (NRs)',
        accessor: 'price',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'meetingTime',
        Header: 'Meeting Time',
        accessor: 'meetingTime',
        Cell: ({ row: { original }, cell: { value } }) => {
          if (value) {
            return <div>{new Date(value).toLocaleString()}</div>;
          } else {
            return (
              <div>
                <ReactModal name="Schedule" modal={modal}>
                  {
                    <MeetingSchedule
                      bookId={original.id}
                      setModalFunction={setModalFunction}
                      modal={modal}
                    >
                      {' '}
                    </MeetingSchedule>
                  }
                </ReactModal>
              </div>
            );
          }
        },
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForBook(pageIndex, pageSize, sortBy, filters);
    setBookQuery(query);
  };

  //   console.log(dataBook);

  useEffect(() => {
    if (dataBook) {
      setPageCount(dataBook.totalPages);
      setTotalData(dataBook.totalDocs);
      // var vehicleDataArr = dataBook.docs;
      // Cloning the original data and reversing it
      //  var reversedArr = [...vehicleDataArr].reverse();
      setTableData(
        dataBook.docs.map((value) => {
          return {
            // id is required

            id: value.id,
            clientMobile: value.clientMobile,
            clientName: value.clientName,
            bikeDriven: value.bikeDriven,
            color: value.color,
            isVerified: value.isVerified,
            numViews: value.numViews,
            price: thousandNumberSeparator(value.price),
            vehicleName: value.vehicleName,
            meetingTime: value?.meetingSchedule?.meetingDateTime,
          };
        })
      );
    }
  }, [dataBook]);
  //   const BreadCrumbList = [
  //     {
  //       routeName: 'Add Vehicle',
  //       route: '/vehicle/add',
  //     },
  //     {
  //       routeName: 'Vehicle List',
  //       route: '/vehicle/view',
  //     },
  //   ];

  return (
    <>
      <h3 className="mb-3">Booked List</h3>
      <Table
        tableName="Booked/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllBook}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorBook}
        isLoadingData={isFetchingBook}
        // isLoadingData={true}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        // editRoute="vehicle/edit"
        viewRoute="book"
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
        // meetingModalAction={MeetingSchedule}
        hiddenColumns={['id', 'verify', 'approved', 'sold']}
        MeetingSchedule={MeetingSchedule}
      />
      {/* <ReactModal>
        <MeetingSchedule></MeetingSchedule>
      </ReactModal> */}
    </>
  );
}

export default AdminBook;
