import { useState, useEffect } from 'react';
import {
  useDeleteSingleMeetingScheduleMutation,
  useReadSingleBookDetailsQuery,
} from 'services/api/seller';
import { useRouter } from 'next/router';
import Button from 'components/Button';
import Popup from 'components/Popup';
import ReactModal from 'components/ReactModal/ReactModal';
import MeetingSchedule from 'container/MeetingSchedual/MeetingSchedual';
import { useToasts } from 'react-toast-notifications';
import Link from 'next/link';

const AdminBookDetail = () => {
  const router = useRouter();

  const [openCancelPopup, setOpenCancelPopup] = useState(false);

  const { data: dataBookDetails, isFetching: isFetchingBookDetails } =
    useReadSingleBookDetailsQuery(router.query.id);
  const { addToast } = useToasts();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: 'success',
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: 'error',
    });
  };

  const [
    deleteMeetingSchedule,
    {
      isLoading: isLoadingDeleteMeetingSchedule,
      isSuccess: isSuccessDeleteMeetingSchedule,
      isError: isErrorDeleteMeetingSchedule,
      error: errorDeleteMeetingSchedule,
    },
  ] = useDeleteSingleMeetingScheduleMutation();

  useEffect(() => {
    if (isSuccessDeleteMeetingSchedule) {
      showSuccessToast(`Meeting schedule successfully cancel.`);
    }
    if (isErrorDeleteMeetingSchedule) {
      showFailureToast(
        errorDeleteMeetingSchedule?.data?.message ||
          `An error occurred when canceling meeting schedule`
      );
    }
  }, [
    isSuccessDeleteMeetingSchedule,
    isErrorDeleteMeetingSchedule,
    errorDeleteMeetingSchedule,
  ]);

  return (
    <section className="brand-detail-section">
      {openCancelPopup ? (
        <Popup
          title="Are you sure you want to cancel this schedule?"
          description=""
          onOkClick={() => {
            deleteMeetingSchedule(dataBookDetails?.id);
            setOpenCancelPopup(false);
          }}
          onCancelClick={() => {
            setOpenCancelPopup(false);
          }}
          okText="Yes"
          cancelText="Cancel"
        />
      ) : null}
      <div className="flex justify-end"></div>
      <div className="container">
        <h1 className="h3 mb-5">Book Details</h1>
        <div className="row ">
          {isFetchingBookDetails ? (
            'Loading...'
          ) : (
            <div className="three-col">
              <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                <p>
                  <span className="font-bold">Client Name :</span>&nbsp;{' '}
                  {dataBookDetails?.clientName}
                </p>
                <p>
                  <span className="font-bold">Client Mobile:</span>&nbsp;
                  {dataBookDetails?.clientMobile}
                </p>
                <p>
                  <span className="font-bold">Color:</span>&nbsp;
                  {dataBookDetails?.color}
                </p>
                <p>
                  <span className="font-bold">Number of Views :</span>&nbsp;
                  {dataBookDetails?.numViews}
                </p>
                <Link
                  disabled
                  href={`/seller/get/${dataBookDetails?.vehicleId}`}
                >
                  <a className="text-blue-600  hover:text-primary">
                    Vehicle Details
                  </a>
                </Link>
                {dataBookDetails?.meetingSchedule?.location && (
                  <>
                    <hr></hr>
                    <p>
                      <span className="font-bold">Meeting location:</span>&nbsp;
                      {dataBookDetails?.meetingSchedule?.location}
                    </p>
                    <p>
                      <span className="font-bold">Meeting Date Time :</span>
                      &nbsp;
                      {new Date(
                        dataBookDetails?.meetingSchedule?.meetingDateTime
                      ).toLocaleString()}
                    </p>
                  </>
                )}

                {/* {console.log(dataBookDetails?.meetingSchedule?.combineLocation)} */}

                {!dataBookDetails?.meetingSchedule?.combineLocation ? (
                  <ReactModal name="Create Schedule">
                    {
                      <MeetingSchedule
                        type="add"
                        bookId={dataBookDetails?.id}
                      ></MeetingSchedule>
                    }
                  </ReactModal>
                ) : (
                  <>
                    <ReactModal name="Update Schedule">
                      {
                        <MeetingSchedule
                          type="edit"
                          bookId={dataBookDetails?.id}
                        ></MeetingSchedule>
                      }
                    </ReactModal>
                    <Button
                      type="button"
                      isLoading={isLoadingDeleteMeetingSchedule}
                      onClick={() => {
                        // deleteMeetingSchedule(dataBookDetails?.id);
                        setOpenCancelPopup(true);
                      }}
                    >
                      Cancel Schedule
                    </Button>
                  </>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default AdminBookDetail;
