import React, { useEffect, useState } from 'react';
// import Image from 'next/image';
// import AdminLayout from 'layouts/Admin';
import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';

import Link from 'next/link';
import { useReadSingleSellerQuery } from 'services/api/seller';
import Button from 'components/Button';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import { useSelector } from 'react-redux';
import { useReadCustomerQuery } from 'services/api/customer';

// import UserLayout from 'layouts/UserLayout';

const getSingleSeller = () => {
  const router = useRouter();
  // const { addToast } = useToasts();
  const sellerId = router?.query?.id;
  const level = useSelector((state) => state.levelReducer.level);

  const [customerProfileId, setCustomerProfileId] = useState(null);

  const {
    data: customerProfile,
    // error: customerProfileFetchError,
    // isLoading: isLoadingCustomerProfile,
    // isSuccess: customerProfileIsSuccess,
  } = useReadCustomerQuery(customerProfileId, {
    skip: !customerProfileId,
  });

  const {
    data: sellerProfile,

    // error: sellerProfileFetchError,
    isLoading: isLoadingSellerProfile,
    isSuccess: sellerProfileSuccess,
  } = useReadSingleSellerQuery(sellerId, {
    skip: !sellerId,
  });

  useEffect(() => {
    if (sellerProfileSuccess) {
      setCustomerProfileId(sellerProfile?.sellerValue?.clientId);
    }
    // console.log('data', );
  }, [sellerProfileSuccess]);

  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add Vehicle',
  //     route: '/vehicle/add',
  //   },
  //   {
  //     routeName: 'Selling Vehicle List',
  //     route: '/seller/view',
  //   },
  //   {
  //     routeName: 'Selling Vehicle Information',
  //     route: '',
  //   },
  // ];
  return (
    // <AdminLayout

    // >

    // </AdminLayout>
    // <UserLayout
    //   documentTitle="Get Selling Vehicle Information"
    //   BreadCrumbList={BreadCrumbList}
    // >
    <section className="brand-detail-section mt-2">
      <div className="container">
        <div className="flex items-center mb-5">
          <h1 className="h4 font-semibold flex-1">
            Selling Vehicle Information
          </h1>

          <div className=" w-[100px] ml-5 seller-detail-edit-btn">
            <Link
              href={
                level === 'superAdmin'
                  ? `/seller/edit/${sellerId}`
                  : level === 'customer'
                  ? `/sell/edit/${sellerId}`
                  : ''
              }
            >
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
        </div>

        {isLoadingSellerProfile ? (
          'Loading...'
        ) : (
          <>
            <div className="row">
              <div className="two-col  sm:w-[75%] lg:w-[40%]">
                <h2 className="h5 font-semibold mb-3">Bike Images</h2>
                <div>
                  {sellerProfile?.sellerValue?.bikeImagePath.map(
                    (imageUrl, i) => (
                      <div key={i}>
                        {imageUrl.imageUrl && (
                          <img
                            src={`${imageUrl.imageUrl}`}
                            alt="Bike Image"
                            className="w-full rounded-sm"
                          />
                        )}
                      </div>
                    )
                  )}
                </div>
              </div>

              <div className="two-col w-[100%] lg:w-[60%]">
                <h2 className="h5 font-semibold mb-3">Bike Details</h2>
                <div className="detail-holder px-6 py-4 border border-gray-300 rounded-sm">
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold  mr-2">
                      Vehicle Name :
                    </span>

                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.vehicleName}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Bike Driven (Km) :
                    </span>

                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.bikeDriven}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Fuel Type:
                    </span>

                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.fuelType}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Expected Price :
                    </span>
                    <span className="inline-block flex-1">
                      {/* {sellerProfile?.sellerValue?.expectedPrice &&
                        thousandNumberSeparator(
                          sellerProfile?.sellerValue?.expectedPrice
                        )} */}
                      NRs
                      {thousandNumberSeparator(
                        sellerProfile?.sellerValue?.expectedPrice
                      )}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Bike Number :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.bikeNumber}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Lot Number :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.lotNumber}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Ownership Count :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.ownershipCount}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Color :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.color}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Post Expiry Date :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.postExpiryDate
                        ? new Date(
                            sellerProfile?.sellerValue?.postExpiryDate
                          ).toLocaleDateString()
                        : null}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Vehicle Used For :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.usedFor}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Condition :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.condition}
                    </span>
                  </div>

                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Had Accident :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.hasAccident ? 'Yes' : 'No'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Is Negotiable :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.isNegotiable ? 'Yes' : 'No'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Is Approved :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.isApproved ? 'Yes' : 'No'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Is Verified :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.isVerified ? 'Yes' : 'No'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Is Sold :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.isSold ? 'Yes' : 'No'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Note :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.note
                        ? sellerProfile?.sellerValue?.note
                        : 'Not Available'}
                    </span>
                  </div>

                  <div className="w-full p-4 mt-4">
                    <p className="text-lg mb-2 text-gray-800 font-bold">
                      Location
                    </p>
                    <hr className="border-t border-gray-400" />
                  </div>

                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Location :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.location?.combineLocation
                        ? sellerProfile?.sellerValue?.location?.combineLocation
                        : 'Not Available'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Near By Location :
                    </span>
                    <span className="inline-block flex-1">
                      {sellerProfile?.sellerValue?.location?.nearByLocation
                        ? sellerProfile?.sellerValue?.location?.nearByLocation
                        : 'Not Available'}
                    </span>
                  </div>
                  <div className="w-full p-4 mt-4">
                    <p
                      className="text-lg mb-2 font-bold text-blue-400 hover:text-primary"
                      onClick={() => {
                        router.push(`/customer/details/${customerProfileId}`);
                      }}
                    >
                      Customer Details
                    </p>
                    <hr className="border-t border-gray-400" />
                  </div>

                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Customer Name:
                    </span>

                    <span className="inline-block flex-1">
                      {customerProfile?.fullName}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Customer Email:
                    </span>

                    <span className="inline-block flex-1">
                      {customerProfile?.email ? customerProfile?.email : 'N/A'}
                    </span>
                  </div>
                  <div className="flex mb-3">
                    <span className="inline-block sm:w-[200px] font-bold mr-2">
                      Customer Phone:
                    </span>

                    <span className="inline-block flex-1">
                      {customerProfile?.mobile
                        ? customerProfile?.mobile
                        : 'N/A'}
                    </span>
                  </div>
                  {level === 'superAdmin' ? (
                    <div className="two-col  sm:w-[75%] lg:w-[40%]">
                      <h2 className="h5 font-semibold mb-3">
                        Blue-Book Images
                      </h2>
                      <div>
                        {sellerProfile?.sellerValue?.bikeImagePath.map(
                          (imageUrl, i) => (
                            <div key={i}>
                              {imageUrl.imageUrl && (
                                <img
                                  src={`${imageUrl.imageUrl}`}
                                  alt="Bike Image"
                                  className="w-full rounded-sm"
                                />
                              )}
                            </div>
                          )
                        )}
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </section>
    // </AdminLayout>
  );
};

export default getSingleSeller;
