import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { featureDate } from 'constant/constant';
import { useEffect, useState } from 'react';

const FeaturePrice = ({
  setFieldValue,
  values,
  errors,
  touched,
  setChanged,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  isValid,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const [price, setPrice] = useState('');

  const setPriceAccordingToDate = (value) => {
    if (value === '3 Days') setPrice(3000);
    else if (value === '7 Days') setPrice(7000);
    else if (value === '15 Days') setPrice(15000);
  };

  return (
    <div className="mb-8">
      <div className="mb-2 select-price">
        <h6 className="mb-2">Some message will go here</h6>
        <SelectWithoutCreate
          required
          label="Feature Time"
          placeholder="Feature Time"
          error={touched?.featureTime ? errors?.featureTime : ''}
          value={values?.featureTime || null}
          onChange={(selectedValue) => {
            setFieldValue('featureTime', selectedValue.value);
            setPriceAccordingToDate(selectedValue.label);
          }}
          options={featureDate?.map((value) => ({
            label: value.label,
            value: value.value,
          }))}
          onBlur={() => {
            setFieldTouched('featureTime');
          }}
        />
      </div>

      {price && (
        <div className="text-sm">
          For this you will be charged
          <span className="line-through text-error"> NRS {price}</span>
          <span> &nbsp;NRS 0</span>
        </div>
      )}
    </div>
  );
};

export default FeaturePrice;
