import Input from 'components/Input';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { useEffect } from 'react';
import { useGetDetailLocationQuery } from 'services/api/getFullLocation';

const VehicleLocations = ({
  setFieldValue,
  values,
  errors,
  touched,
  setChanged,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  isValid,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);
  const { data: combineLocations, isFetching: isFetchingCombineLocation } =
    useGetDetailLocationQuery();

  return (
    <div className="min-h-[415px]">
      <div className="row-sm  pt-5 pb-2">
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={isFetchingCombineLocation}
            label="Select Location"
            placeholder="Location"
            error={
              touched?.location?.combineLocation
                ? errors?.location?.combineLocation
                : ''
            }
            value={values?.location?.combineLocation || null}
            onChange={(selectedValue) => {
              setFieldValue('location.combineLocation', selectedValue.value);
            }}
            options={combineLocations?.map((value) => ({
              label: value.fullAddress,
              // value: `${value.id}`,
              value: value.fullAddress,
            }))}
            onBlur={() => {
              setFieldTouched('location.combineLocation');
            }}
          />
        </div>

        <div className="three-col-sm">
          <Input
            required={true}
            name="location.nearByLocation"
            placeholder="Thamel"
            label="Near By Location"
            type="text"
          />
        </div>
      </div>
    </div>
  );
};

export default VehicleLocations;
