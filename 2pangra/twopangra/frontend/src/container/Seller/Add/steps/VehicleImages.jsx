import DropZone from 'components/DropZone';
import { useEffect } from 'react';

const vehicleImages = ({
  setFieldValue,
  values,
  errors,
  touched,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  isValid,
  setChanged,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const setNewFiles = (newFiles) => {
    // if (!formikBag.current) return;
    // const { values, setFieldValue } = formikBag?.current;
    const allFiles = [...values?.bikeImagePath, ...newFiles];
    setFieldValue('bikeImagePath', allFiles);
  };

  const handleRemoveFile = (idx) => {
    // if (!formikBag.current) return;
    // const { values, setFieldValue } = formikBag?.current;
    const currentFiles = [...values.bikeImagePath];
    currentFiles.splice(idx, 1);
    setFieldValue('bikeImagePath', currentFiles);
  };

  const setBillBoookFile = (newFiles) => {
    // if (!formikBag.current) return;
    // const { values, setFieldValue } = formikBag?.current;
    const allFiles = [...values?.billBookImagePath, ...newFiles];
    setFieldValue('billBookImagePath', allFiles);
  };

  const handleRemoveBillBookFile = (idx) => {
    // if (!formikBag.current) return;
    // const { values, setFieldValue } = formikBag?.current;
    const currentFiles = [...values.billBookImagePath];
    currentFiles.splice(idx, 1);
    setFieldValue('billBookImagePath', currentFiles);
  };

  return (
    <div className="min-h-[415px]">
      <div className="row-sm  pt-5 pb-2">
        <div className="three-col-sm">
          <DropZone
            label="Vehicle"
            required
            currentFiles={values.bikeImagePath}
            // values.bikeImagePath ? [values.bikeImagePath] : []
            setNewFiles={setNewFiles}
            handleRemoveFile={handleRemoveFile}
            error={touched?.bikeImagePath ? errors?.bikeImagePath : ''}
          />
        </div>
        <div className="three-col-sm">
          <DropZone
            label="Blue-Book"
            required={false}
            currentFiles={values.billBookImagePath}
            setNewFiles={setBillBoookFile}
            handleRemoveFile={handleRemoveBillBookFile}
            error={touched?.billBookImagePath ? errors?.billBookImagePath : ''}
          />
        </div>
      </div>
    </div>
  );
};

export default vehicleImages;
