import Input from 'components/Input';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { color } from 'constant/constant';
import { useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { useSelector } from 'react-redux';

import { useReadVehicleCustomQuery } from 'services/api/vehicle';

const vehicleDetails = ({
  setFieldValue,
  values,
  errors,
  touched,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  isValid,
  setChanged,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const { data: vehicleData, isFetching: fetchingVehicles } =
    useReadVehicleCustomQuery('');

  const level = useSelector((state) => state.levelReducer.level);

  return (
    <div className="min-h-[415px]">
      <div className="row-sm  pt-5 pb-2">
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={fetchingVehicles}
            label="Select Vehicle Name"
            placeholder="E.g: Honda"
            error={touched?.vehicleDetailId ? errors?.vehicleDetailId : ''}
            value={values.vehicleDetailId || null}
            onChange={(selectdVehicleDetailId) => {
              setFieldTouched('vehicleDetailId');
              setFieldValue('vehicleDetailId', selectdVehicleDetailId.value);
            }}
            options={vehicleData?.docs?.map((vehicle) => ({
              value: vehicle.id,
              label: vehicle.vehicleName,
            }))}
            onBlur={() => {
              setFieldTouched('vehicleDetailId');
            }}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="Bike Driven (Km)"
            name="bikeDriven"
            type="text"
            placeholder="E.g: 1000"
            required={level !== 'superAdmin'}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="Bike Number"
            name="bikeNumber"
            type="text"
            placeholder="E.g: Ba6pa:2045"
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Lot Number"
            name="lotNumber"
            type="text"
            placeholder="E.g: 66"
          />
        </div>

        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            // loading={fetchingVehicles}
            label="Color"
            placeholder="Red"
            error={touched?.color ? errors?.color : ''}
            value={values.color || []}
            onChange={(color) => {
              setFieldTouched('color');
              // setFieldValue('color', color.value);
              setFieldValue(
                'color',
                color.map((netTag) => netTag.value)
              );
            }}
            options={color.map((value) => ({
              value: value,
              label: value,
            }))}
            onBlur={() => {
              setFieldTouched('color');
            }}
            isMulti={true}
          />
        </div>
        {/* 
        <ReactSelect
            isMulti={true}
            required
            loading={isLoadingInstrumentation || isLoadingAllVehicleDetails}
            label="Instrumentation/s"
            placeholder="E.g: Fully Digital Clusters"
            error={
              touched?.optionFeature?.instrumentation
                ? errors?.optionFeature?.instrumentation
                : ''
            }
            value={values?.optionFeature?.instrumentation || []}
            onChange={(newTags, actionMeta) => {
              setFieldTouched('optionFeature.instrumentation');
              setFieldValue(
                'optionFeature.instrumentation',
                newTags.map((netTag) => netTag.value)
              );
              if (actionMeta.action === 'create-option') {
                createInstrumentation({
                  instrumentation: newTags[newTags.length - 1]?.value,
                });
              }
            }}
            options={allVehicleDetails?.instrumentation?.map(
              (instrumentation, i) => ({
                value: instrumentation.instrumentation,
                label: instrumentation.instrumentation,
              })
            )}
            onBlur={() => {
              setFieldTouched('optionFeature.instrumentation');
            }}
          /> */}

        <div className="three-col-sm">
          <Input
            label="Mileage"
            name="mileage"
            type="text"
            placeholder="E.g: 35"
            required={level !== 'superAdmin'}
          />
        </div>
        {/* {console.log(values?.expectedPrice)} */}

        <div className="three-col-sm">
          <label htmlFor="">
            Make Year(AD)
            {/* <span className="required">*</span> */}
          </label>
          <DatePicker
            selected={values?.makeYear ? new Date(values?.makeYear) : null}
            onChange={(date) => {
              setFieldValue('makeYear', date || '');
            }}
            showYearPicker
            dateFormat="yyyy"
            yearItemNumber={9}
            placeholderText="E.g: 2021"
            maxDate={new Date()}
          />
          {touched.makeYear && errors.makeYear && (
            <div className="text-error">{errors.makeYear}</div>
          )}
        </div>
      </div>
    </div>
  );
};

export default vehicleDetails;
