import InputNumberFormat from 'components/Input/InputNumberFormat';
import Textarea from 'components/Input/textarea';
import RadioBox from 'components/RadioBox/RadioBox';
import Select from 'components/Select';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import {
  condition,
  ownershipCount,
  postExpiryDate,
  usedFor,
} from 'constant/constant';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { removePrefixComma } from 'utils/removePrefixComma';

const GeneralInformation = ({
  setFieldValue,
  values,
  errors,
  touched,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  setChanged,
  type,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);
  const level = useSelector((state) => state.levelReducer.level);

  return (
    <div className="min-h-[415px]">
      <div className="row-sm  pt-5 pb-2">
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            label="Ownership Count"
            placeholder="1"
            error={touched?.ownershipCount ? errors?.ownershipCount : ''}
            value={values.ownershipCount}
            onChange={(ownershipCount) => {
              setFieldTouched('ownershipCount');
              setFieldValue('ownershipCount', ownershipCount.value);
            }}
            options={ownershipCount.map((value) => ({
              value: value,
              label: value,
            }))}
            onBlur={() => {
              setFieldTouched('ownershipCount');
            }}
          />
        </div>
        {type === 'add' && (
          <div className="three-col-sm">
            <SelectWithoutCreate
              required
              label="Post Expiry Date"
              placeholder="1 Month"
              error={touched?.postExpiryDate ? errors?.postExpiryDate : ''}
              value={values.postExpiryDate}
              onChange={(selectedValue) => {
                setFieldValue('postExpiryDate', selectedValue?.value);
                setFieldTouched('postExpiryDate');
              }}
              options={postExpiryDate.map((d) => ({
                value: d?.value,
                label: d?.label,
              }))}
              onBlur={() => {
                setFieldTouched('postExpiryDate');
              }}
            />
          </div>
        )}
        <div className="three-col-sm">
          <Select label="Condition" name="condition">
            {condition.map((value, i) => (
              <option key={i} value={value}>
                {value}
              </option>
            ))}
          </Select>
        </div>
        <div className="three-col-sm">
          <InputNumberFormat
            label="Expected Price(NRs)"
            name="expectedPrice"
            value={values?.expectedPrice || null}
            thousandSeparator={true}
            prefix="NRs "
            onChange={(e) => {
              setFieldValue(
                'expectedPrice',
                removePrefixComma('NRs ', e.target.value)
              );
            }}
          ></InputNumberFormat>
        </div>
        {level === 'superAdmin' && (
          <>
            <div className="three-col-sm">
              <RadioBox
                title="Is Negotiable"
                name="isNegotiable"
                values={[
                  { label: 'Yes', value: 'Yes' },
                  { label: 'No', value: 'No' },
                  { label: 'N/A', value: 'N/A' },
                ]}
                onChange={(e) => {
                  setFieldValue('isNegotiable', e.target.value);
                }}
              ></RadioBox>
            </div>
            <div className="three-col-sm">
              <RadioBox
                title="Had Accident"
                name="hasAccident"
                values={[
                  { label: 'Yes', value: 'Yes' },
                  { label: 'No', value: 'No' },
                  { label: 'N/A', value: 'N/A' },
                ]}
                onChange={(e) => {
                  setFieldValue('hasAccident', e.target.value);
                }}
              ></RadioBox>
            </div>
          </>
        )}
        {level === 'customer' && (
          <>
            <div className="three-col-sm">
              <RadioBox
                title="Is Negotiable"
                name="isNegotiable"
                values={[
                  { label: 'Yes', value: 'Yes' },
                  { label: 'No', value: 'No' },
                  { label: 'N/A', value: 'N/A' },
                ]}
                onChange={(e) => {
                  setFieldValue('isNegotiable', e.target.value);
                }}
              ></RadioBox>
            </div>
            <div className="three-col-sm">
              <RadioBox
                title="Had Accident"
                name="hasAccident"
                values={[
                  { label: 'Yes', value: 'Yes' },
                  { label: 'No', value: 'No' },
                  { label: 'N/A', value: 'N/A' },
                ]}
                onChange={(e) => {
                  setFieldValue('hasAccident', e.target.value);
                }}
              ></RadioBox>
            </div>
          </>
        )}

        <div className="three-col-sm">
          <Textarea
            label="Note"
            name="note"
            placeholder="E.g: Description note"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <SelectWithoutCreate
            required={level !== 'superAdmin'}
            label="Vehicle Used Time"
            placeholder="1 Month"
            error={touched?.usedFor ? errors?.usedFor : ''}
            value={values.usedFor}
            onChange={(usedFor) => {
              setFieldTouched('usedFor');
              setFieldValue('usedFor', usedFor.value);
            }}
            options={usedFor.map((value) => ({
              value: value,
              label: value,
            }))}
            onBlur={() => {
              setFieldTouched('usedFor');
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default GeneralInformation;
