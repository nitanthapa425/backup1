// import { DateColumnFilter } from 'components/Table/Filter';
import {
  DateColumnFilter,
  MultiSelectFilter,
  SelectColumnFilter,
  YearColumnFilter,
} from 'components/Table/Filter';
import Table from 'components/Table/table';
import {
  approved,
  color,
  condition,
  ownershipCount,
  sold,
  usedFor,
  verified,
} from 'constant/constant';
import MeetingSchedule from 'container/MeetingSchedual/MeetingSchedual';
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
// import {
//   useAddFeaturedBikeMutation,
//   useRemoveFeaturedBikeMutation,
// } from 'services/api/featuredBike';
// import { useAddHotDealMutation } from 'services/api/hotDeals';

import {
  useAddFeaturedBikeMutation,
  useRemoveFeaturedBikeMutation,
  useAddHotDealMutation,
  useDeleteSellerMutation,
  // useReadSellerQuery,
  useReadVehicleSellQuery,
  useUnVerifiedMutation,
  useVerifiedMutation,
  useApprovedMutation,
  useUnApprovedMutation,
  useSoldMutation,
  useUnSoldMutation,
} from 'services/api/seller';
import {
  getQueryStringForSellingVehicle,
  // getQueryStringForTable,
  // getQueryStringForTable,
} from 'utils/getQueryStringForTable';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';

// import { SelectColumnFilter } from 'components/Table/Filter';

function ViewSellerContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [sellerDataQuery, setSellerDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  const level = useSelector((state) => state.levelReducer.level);

  const {
    data: sellerData,
    isError: sellerError,
    isFetching: isLoadingSeller,
  } = useReadVehicleSellQuery(sellerDataQuery);
  const [
    addFeaturedBikes,
    {
      error: errorAddFeaturedBike,
      isLoading: isAddingFeaturedBike,
      isSuccess: isSuccessAddFeaturedBikes,
    },
  ] = useAddFeaturedBikeMutation();
  const [
    addHotDeals,
    {
      error: errorAddHotDeals,
      isLoading: isAddingHotDeals,
      isSuccess: isSuccessAddHotDeals,
    },
  ] = useAddHotDealMutation();
  const [
    verifySell,
    {
      error: errorVerifySell,
      isLoading: isAddingVerifySell,
      isSuccess: isSuccessAddVerifySell,
    },
  ] = useVerifiedMutation();
  const [
    unVerifySell,
    {
      error: errorUnVerifySell,
      isLoading: isAddingUnVerifySell,
      isSuccess: isSuccessAddUnVerifySell,
    },
  ] = useUnVerifiedMutation();
  const [
    soldSell,
    {
      error: errorSoldSell,
      isLoading: isAddingSoldSell,
      isSuccess: isSuccessAddSoldSell,
      data: dataAddSoldSell,
    },
  ] = useSoldMutation();

  const [
    unSoldSell,
    {
      error: errorUnSoldSell,
      isLoading: isAddingUnSoldSell,
      isSuccess: isSuccessAddUnSoldSell,
      data: dataAddUnSoldSell,
    },
  ] = useUnSoldMutation();

  const [
    approvedSell,
    {
      error: errorApprovedSell,
      isLoading: isAddingApprovedSell,
      isSuccess: isSuccessAddApprovedSell,
    },
  ] = useApprovedMutation();

  const [
    unApprovedSell,
    {
      error: errorUnApprovedSell,
      isLoading: isAddingUnApprovedSell,
      isSuccess: isSuccessAddUnApprovedSell,
    },
  ] = useUnApprovedMutation();
  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: 'success',
    });
  };
  const [
    removeFeaturedBike,
    {
      error: errorRemoveFeaturedBike,
      isLoading: isRemovingFeaturedBike,
      isSuccess: isSuccessRemoveFeaturedBike,
    },
  ] = useRemoveFeaturedBikeMutation();

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: 'error',
    });
  };
  useEffect(() => {
    if (errorAddFeaturedBike) {
      showFailureToast(
        errorAddFeaturedBike?.data?.message || 'Failed to add to featured bike'
      );
    }
    if (isSuccessAddFeaturedBikes) {
      showSuccessToast('Successfully added to featured bike');
    }
  }, [errorAddFeaturedBike, isSuccessAddFeaturedBikes]);
  useEffect(() => {
    if (errorAddHotDeals) {
      showFailureToast(
        errorAddHotDeals?.data?.message || 'Failed to add to hot deals'
      );
    }
    if (isSuccessAddHotDeals) {
      showSuccessToast('Successfully added to hot deals');
    }
  }, [errorAddHotDeals, isSuccessAddHotDeals]);
  useEffect(() => {
    if (errorVerifySell) {
      showFailureToast(errorVerifySell?.data?.message || 'Failed to verified');
    }
    if (isSuccessAddVerifySell) {
      showSuccessToast('Successfully verified');
    }
  }, [errorVerifySell, isSuccessAddVerifySell]);
  useEffect(() => {
    if (errorUnVerifySell) {
      showFailureToast(
        errorUnVerifySell?.data?.message || 'Failed to unverified'
      );
    }
    if (isSuccessAddUnVerifySell) {
      showSuccessToast('Successfully unverified');
    }
  }, [errorUnVerifySell, isSuccessAddUnVerifySell]);

  useEffect(() => {
    if (errorSoldSell) {
      showFailureToast(errorSoldSell?.data?.message || 'Failed to sold');
    }
    if (isSuccessAddSoldSell) {
      showSuccessToast(dataAddSoldSell?.message);
    }
  }, [errorSoldSell, isSuccessAddSoldSell]);
  useEffect(() => {
    if (errorUnSoldSell) {
      showFailureToast(errorUnSoldSell?.data?.message || 'Failed to unsold');
    }
    if (isSuccessAddUnSoldSell) {
      showSuccessToast(dataAddUnSoldSell?.message);
    }
  }, [errorUnSoldSell, isSuccessAddUnSoldSell]);

  useEffect(() => {
    if (errorApprovedSell) {
      showFailureToast(
        errorApprovedSell?.data?.message || 'Failed to approved'
      );
    }
    if (isSuccessAddApprovedSell) {
      showSuccessToast('Successfully approved');
    }
  }, [errorApprovedSell, isSuccessAddApprovedSell]);
  useEffect(() => {
    if (errorUnApprovedSell) {
      showFailureToast(
        errorUnApprovedSell?.data?.message || 'Failed to unapproved'
      );
    }
    if (isSuccessAddUnApprovedSell) {
      showSuccessToast('Successfully unapproved');
    }
  }, [errorUnApprovedSell, isSuccessAddUnApprovedSell]);

  useEffect(() => {
    if (errorRemoveFeaturedBike) {
      showFailureToast(
        errorRemoveFeaturedBike?.data?.message ||
          'Failed to remove from featured bikes'
      );
    }
    if (isSuccessRemoveFeaturedBike) {
      showSuccessToast('Successfully removed from featured bikes');
    }
  }, [errorRemoveFeaturedBike, isSuccessRemoveFeaturedBike]);

  const {
    data: sellerDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadVehicleSellQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (sellerDataAll) {
      setTableDataAll(
        sellerDataAll?.docs?.map((sellers) => {
          return {
            vehicleName: sellers?.vehicleName,
            bikeDriven: sellers?.bikeDriven,
            expectedPrice: thousandNumberSeparator(sellers?.expectedPrice),

            bikeNumber: sellers?.bikeNumber,
            ownershipCount: sellers?.ownershipCount,
            color: sellers?.color,
            condition: sellers?.condition,
            postExpiryDate: sellers?.postExpiryDate,
            id: sellers?.id,
            makeYear: !sellers.makeYear
              ? undefined
              : sellers.makeYear.slice(0, 4),
            usedFor: sellers?.usedFor,
          };
        })
      );
    }
  }, [sellerDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteSellerMutation();

  const columns = useMemo(
    () => [
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'id',
        Header: 'id',
        accessor: 'id',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeDriven',
        Header: 'Bike Driven',
        accessor: 'bikeDriven',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'expectedPrice',
        Header: 'Expected Price(NRs)',
        accessor: 'expectedPrice',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'bikeNumber',
        Header: 'Bike Number',
        accessor: 'bikeNumber',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'lotNumber',
        Header: 'Lot Number',
        accessor: 'lotNumber',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'ownershipCount',
        Header: 'Ownership Count',
        accessor: 'ownershipCount',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: ownershipCount.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'color',
        Header: 'Color',
        accessor: 'color',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'condition',
        Header: 'Condition',
        accessor: 'condition',
        Cell: ({ cell: { value } }) => value.toLowerCase() || '-',
        Filter: MultiSelectFilter,
        possibleFilters: condition.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'postExpiryDate',
        Header: 'Post Expiry Date',
        accessor: 'postExpiryDate',
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || '-',
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'makeYear',
        Header: 'Make Year (AD)',
        accessor: 'makeYear',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: YearColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'usedFor',
        Header: 'Used For',
        accessor: 'usedFor',
        Cell: ({ cell: { value } }) => value || '-',
        Filter: MultiSelectFilter,
        possibleFilters: usedFor.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isVerified',
        Header: 'Verified',
        accessor: 'isVerified',
        Cell: ({ cell: { value } }) => (value ? 'Yes' : 'No'),
        Filter: MultiSelectFilter,
        possibleFilters: verified.map((data) => ({
          label: data.label,
          value: data.value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isApproved',
        Header: 'Approved',
        accessor: 'isApproved',
        Cell: ({ cell: { value } }) => (value ? 'Yes' : 'No'),
        Filter: MultiSelectFilter,
        possibleFilters: approved.map((data) => ({
          label: data.label,
          value: data.value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'isSold',
        Header: 'Sold',
        accessor: 'isSold',
        Cell: ({ cell: { value } }) => (value ? 'Yes' : 'No'),
        Filter: MultiSelectFilter,
        possibleFilters: sold.map((data) => ({
          label: data.label,
          value: data.value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'hasInHotDeal',
        Header: 'Is Hot Deal',
        accessor: 'hasInHotDeal',
        Cell: ({ cell: { value } }) => (value ? ' Hot Deal' : 'Not Hot Deal'),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'Hot Deal',
            value: true,
          },
          {
            label: 'Not Hot Deal',
            value: false,
          },
        ],
      },
      {
        id: 'hasInFeature',
        Header: 'Is Featured Bike',
        accessor: 'hasInFeature',
        Cell: ({ cell: { value } }) =>
          value ? 'Featured Bike' : 'Not Featured Bike',
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: 'Featured Bike',
            value: true,
          },
          {
            label: 'Not Feature Bike',
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForSellingVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setSellerDataQuery(query);
  };

  useEffect(() => {
    if (sellerData) {
      setPageCount(sellerData.totalPages);
      setTotalData(sellerData.totalDocs);

      setTableData(
        sellerData?.docs?.map((sellers) => {
          return {
            vehicleName: sellers?.vehicleName,
            bikeDriven: sellers?.bikeDriven,
            expectedPrice: thousandNumberSeparator(sellers?.expectedPrice),
            bikeNumber: sellers?.bikeNumber,
            lotNumber: sellers?.lotNumber,
            ownershipCount: sellers?.ownershipCount,
            color: sellers?.color,
            condition: sellers?.condition,
            postExpiryDate: sellers?.postExpiryDate,
            id: sellers?.id,
            makeYear: !sellers.makeYear
              ? undefined
              : sellers.makeYear.slice(0, 4),
            usedFor: sellers?.usedFor,
            isVerified: sellers?.isVerified,
            isApproved: sellers?.isApproved,
            isSold: sellers?.isSold,
            hasInHotDeal: sellers?.hasInHotDeal,
            hasInFeature: sellers?.hasInFeature,
          };
        })
      );
    }
  }, [sellerData]);
  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add Vehicle',
  //     route: '/vehicle/add',
  //   },
  //   {
  //     routeName: 'Selling Vehicle List',
  //     route: '',
  //   },
  // ];

  return (
    // <AdminLayout documentTitle="View Seller" BreadCrumbList={BreadCrumbList}>
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="SellingVehicle/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={sellerError}
          isLoadingData={isLoadingSeller}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          editRoute={
            level === 'superAdmin'
              ? 'seller/edit'
              : level === 'customer'
              ? 'sell/edit'
              : ''
          }
          viewRoute={
            level === 'superAdmin'
              ? 'seller/get'
              : level === 'customer'
              ? 'sell/get'
              : ''
          }
          deleteQuery={deleteItems}
          isDeleting={isDeleting}
          isDeleteError={isDeleteError}
          isDeleteSuccess={isDeleteSuccess}
          DeletedError={DeletedError}
          hasExport={true}
          addPage={
            level === 'superAdmin'
              ? {}
              : level === 'customer'
              ? {
                  page: 'Add Selling Vehicle',
                  route: '/sell',
                }
              : {
                  page: 'Add Selling Vehicle',
                  route: '/sell',
                }
          }
          hasDropdownBtn={true}
          dropdownBtnName={
            level === 'superAdmin' ? 'Add to' : level === 'customer' ? '' : ''
          }
          dropdownBtnOptions={
            level === 'superAdmin'
              ? [
                  {
                    name: 'Featured Bikes',
                    onClick: addFeaturedBikes,
                    isLoading: isAddingFeaturedBike,
                    loadingText: 'Adding...',
                  },
                  {
                    name: 'Hot Deals',
                    onClick: addHotDeals,
                    isLoading: isAddingHotDeals,
                    loadingText: 'Adding...',
                  },
                  {
                    name: 'Remove from Featured Bikes',
                    onClick: removeFeaturedBike,
                    isLoading: isRemovingFeaturedBike,
                    loadingText: 'Removing...',
                    // conditionalShow: true,
                    // showFor: 'hasInHomePage',
                    // dontShowWhen: false,
                  },
                ]
              : level === 'customer'
              ? []
              : []
          }
          verifiedAction={
            level === 'superAdmin'
              ? [
                  {
                    name: 'Verify',
                    onClick: verifySell,
                    isLoading: isAddingVerifySell,
                    loadingText: 'Adding...',
                    isVerifiedValue: true,
                  },
                  {
                    name: 'Unverified',
                    onClick: unVerifySell,
                    isLoading: isAddingUnVerifySell,
                    loadingText: 'Adding...',
                    isVerifiedValue: false,
                  },
                ]
              : level === 'customer'
              ? []
              : []
          }
          approvedAction={
            level === 'superAdmin'
              ? [
                  {
                    name: 'Approved',
                    onClick: approvedSell,
                    isLoading: isAddingApprovedSell,
                    loadingText: 'Adding...',
                    isApprovedValue: true,
                  },
                  {
                    name: 'UnApproved',
                    onClick: unApprovedSell,
                    isLoading: isAddingUnApprovedSell,
                    loadingText: 'Adding...',
                    isApprovedValue: false,
                  },
                ]
              : level === 'customer'
              ? []
              : []
          }
          soldAction={
            level === 'superAdmin'
              ? [
                  {
                    name: 'Sold',
                    onClick: soldSell,
                    isLoading: isAddingSoldSell,
                    loadingText: 'Adding...',
                    isSoldValue: true,
                  },
                  {
                    name: 'UnSold',
                    onClick: unSoldSell,
                    isLoading: isAddingUnSoldSell,
                    loadingText: 'Adding...',
                    isSoldValue: false,
                  },
                ]
              : level === 'customer'
              ? []
              : []
          }
          hiddenColumns={
            level === 'superAdmin'
              ? ['id', 'meetingSchedule']
              : level === 'customer'
              ? ['id', 'verify', 'approved', 'sold', 'meetingSchedule']
              : ['id', 'verify', 'approved', 'sold', 'meetingSchedule']
          }
          MeetingSchedule={MeetingSchedule}
        />
      </div>
    </section>
    // </AdminLayout>
  );
}

export default ViewSellerContainer;
