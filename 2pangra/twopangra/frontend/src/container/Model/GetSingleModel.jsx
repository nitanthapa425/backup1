import React, { useEffect } from 'react';
import AdminLayout from 'layouts/Admin';
import { useRouter } from 'next/router';
import { useToasts } from 'react-toast-notifications';
import { useReadModelDetailsQuery } from 'services/api/model';
import { useGetSubModelFromModelQuery } from 'services/api/varient';
import Link from 'next/link';
import Button from 'components/Button';

const GetSingleModel = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: modelDetails,
    error: modelFetchError,
    isLoading: isLoadingModel,
  } = useReadModelDetailsQuery(router.query.id, {});

  const {
    data: subModelData,
    // isLoading: fetchingSubModel,
    // isError: subModelError,
  } = useGetSubModelFromModelQuery(router.query.id);

  useEffect(() => {
    if (modelFetchError) {
      addToast(
        'Error occured while fetching model details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [modelFetchError]);

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Model List',
      route: '/vehicle/model/view',
    },
    {
      routeName: 'Model Details',
      route: '',
    },
  ];
  return (
    <AdminLayout
      documentTitle="Get Single Model"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          {/* <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb> */}
          {/* 
          <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}
          <h1 className="h3 mb-5">Model Details</h1>
          <div className="flex justify-end">
            <Link href={`/vehicle/model/edit/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          <div className="row ">
            {isLoadingModel ? (
              'Loading...'
            ) : (
              <div className="flex items-start">
                <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md ">
                  <p>Model Name :&nbsp;{modelDetails?.modelName}</p>
                  {/* <p>
                    Model Description :&nbsp;
                    {modelDetails?.modelDescription
                      ? modelDetails?.modelDescription
                      : 'Not available'}
                  </p> */}
                  <p>
                    Brand Name:&nbsp;
                    {modelDetails?.brandId?.brandVehicleName}
                  </p>
                </div>
                {subModelData?.length && (
                  <div className="list-max-height">
                    <h5>Variant List</h5>
                    {subModelData?.map((variant, i) => {
                      return (
                        <div key={i}>
                          <Link href={`/vehicle/submodel/get/${variant?.id}`}>
                            <a>{variant?.varientName}</a>
                          </Link>
                          <br></br>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetSingleModel;
