import React from 'react';
import AdminLayout from 'layouts/Admin';
import { useReadModelQuery } from 'services/api/model';

const GetModelDetails = () => {
  const { data: modelData } = useReadModelQuery();

  return (
    <AdminLayout documentTitle="Get Model">
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Brand Model</h1>
          <div className="row ">
            {modelData?.docs?.map?.((brand, i) => {
              return (
                <div key={i} className="three-col">
                  <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                    <p className="ellipsis">
                      Model Name :&nbsp;{brand?.modelName}
                    </p>
                    {/* <p className="ellipsis">
                      Model Description :&nbsp;
                      {brand?.modelDescription}
                    </p> */}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetModelDetails;
