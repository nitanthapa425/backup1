import React, { useRef, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';
import Link from 'next/link';

import Button from 'components/Button';
import Input from 'components/Input';
import AdminLayout from 'layouts/Admin';

import { useReadBrandQuery } from 'services/api/brand';
import { modelvalidationSchema } from 'validation/model.validation';
import {
  useCreateModelMutation,
  useReadModelDetailsQuery,
  useUpdateModelMutation,
} from 'services/api/model';
import { useToasts } from 'react-toast-notifications';
import Popup from 'components/Popup';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
// import Textarea from 'components/Input/textarea';

const ModelContainer = ({ type }) => {
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const [currentModelDetails, setCurrentModelDetails] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const { addToast, removeAllToasts } = useToasts();

  const { data: brandData, isLoading: fetchingBrands } = useReadBrandQuery();
  const [BrandData, setBrandData] = useState([]);
  useEffect(() => {
    if (brandData) {
      setBrandData(brandData);
    }
  }, [brandData]);
  const formikModelBag = useRef();
  const [createModel, { isError, isLoading, isSuccess, data }] =
    useCreateModelMutation();

  const [
    updateModel,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateModelMutation();

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    formikModelBag.current?.resetForm();
    if (isSuccess) {
      formikModelBag.current?.resetForm();
      addToast(
        <>
          New Model added successfully.{' '}
          <Link href={`/vehicle/model/get/${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Model
            </a>
          </Link>
          <Link href={`/vehicle/submodel/add?model=${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700">
              Add Variant
            </a>
          </Link>
        </>,
        {
          appearance: 'success',
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikModelBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || 'Model details updated successfully.',
        {
          appearance: 'success',
        }
      );
      setChanged(false);
      router.push(`/vehicle/model/get/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  useEffect(() => {
    if (isUpdateError) {
      addToast('Model already exists.', {
        appearance: 'error',
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isError) {
      addToast('Model Name already exist.', {
        appearance: 'error',
      });
    }
  }, [isError]);

  const addModelInitialValue = {
    modelName: '',
    // modelDescription: '',
    brandId: router?.query?.brand || '',
  };
  const {
    data: modelDetails,
    error: modelFetchError,
    // isLoading: isLoadingModel,
  } = useReadModelDetailsQuery(router.query.id, {
    skip: type !== 'edit' || !router.query.id,
  });

  useEffect(() => {
    if (modelDetails) {
      const newModelDetails = {
        modelName: modelDetails?.modelName,
        // modelDescription: modelDetails?.modelDescription,
        brandId: modelDetails?.brandId?.id,
      };

      setCurrentModelDetails(newModelDetails);
    }
  }, [modelDetails]);

  useEffect(() => {
    if (modelFetchError) {
      addToast(
        'Error occured while fetching model details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [modelFetchError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === 'add') {
      createModel(values, resetForm);
    }
    if (type === 'edit') {
      updateModel({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === 'add') {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Add Model',
        route: '/vehicle/model/add',
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Model List',
        route: '/vehicle/model/view',
      },
      {
        routeName: 'Edit Model',
        route: '',
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={type === 'add' ? 'Add New Model' : 'Edit Model Details'}
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === 'edit' ? currentModelDetails : addModelInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={modelvalidationSchema}
          enableReinitialize
          innerRef={formikModelBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            setFieldTouched,
            dirty,
          }) => {
            return (
              <Form onChange={() => setChanged(true)}>
                {/* <button
                  className="mb-3 hover:text-primary"
                  onClick={() => router.back()}
                >
                  Go Back
                </button> */}
                <div className="flex justify-center pt-3 pb-10">
                  <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-xl mx-auto">
                    <h3 className="mb-3">
                      {type === 'add' ? 'Add New Model' : 'Edit Model Details'}
                    </h3>
                    <div className="mb-2">
                      <SelectWithoutCreate
                        required
                        loading={fetchingBrands}
                        label="Select Brand"
                        placeholder="E.g: Bajaj"
                        error={touched?.brandId ? errors?.brandId : ''}
                        value={values.brandId}
                        onChange={(selectedBrand) => {
                          setFieldTouched('brandId');
                          setFieldValue('brandId', selectedBrand.value);
                        }}
                        options={BrandData?.docs?.map((brand) => ({
                          value: brand.id,
                          label: brand.brandVehicleName,
                        }))}
                        onBlur={() => {
                          setFieldTouched('brandId');
                        }}
                      />
                    </div>
                    <div className="mb-2">
                      <Input
                        label="Model Name"
                        name="modelName"
                        type="text"
                        placeholder="E.g: Pulsar"
                      />
                    </div>

                    {/* <div className="mb-2">
                      <Textarea
                        label="Model Description"
                        name="modelDescription"
                        type="text"
                        placeholder="E.g: Model Description"
                        required={false}
                      />
                    </div> */}

                    <div className="btn-holder mt-3">
                      <Button
                        type="submit"
                        loading={isLoading || updating}
                        disabled={
                          isSubmitting || !dirty || isLoading || updating
                        }
                      >
                        {type === 'add' ? 'Submit' : 'Update'}
                      </Button>

                      <Button
                        onClick={() => {
                          setOpenModal(true);
                        }}
                        variant="outlined-error"
                        type="button"
                        disabled={
                          isSubmitting || !dirty || isLoading || updating
                        }
                      >
                        Clear
                      </Button>

                      {openModal && (
                        <Popup
                          title="Are you sure to clear all fields?"
                          description="If you clear all fields, the data will not be saved."
                          onOkClick={() => {
                            setFieldValue('brandId', null);
                            setFieldValue('brandId', null);
                            resetForm();
                            setChanged(false);
                            setOpenModal(false);
                          }}
                          onCancelClick={() => setOpenModal(false)}
                          okText="Clear All"
                          cancelText="Cancel"
                        />
                      )}
                    </div>
                  </div>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </AdminLayout>
  );
};

ModelContainer.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default ModelContainer;
