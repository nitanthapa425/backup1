import { useEffect } from 'react';
import { Form, Formik } from 'formik';
// import Input from 'components/Input';
import Button from 'components/Button';
import { useToasts } from 'react-toast-notifications';
import { OfferPriceValidationSchema } from 'validation/seller.validation';
import { useOfferPriceMutation } from 'services/api/seller';
import { useRouter } from 'next/router';
import InputNumberFormat from 'components/Input/InputNumberFormat';
import { removePrefixComma } from 'utils/removePrefixComma';
// import Input from 'components/Input';

const initialValues = {
  offerPrice: '',
};

const OfferPricePopUp = (props) => {
  const { addToast } = useToasts();
  const router = useRouter();

  const [
    createOffer,
    { isSuccess, isError, isLoading: isLoadingOfferPrice, error },
  ] = useOfferPriceMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast('yours Offer Price is set successfully', {
        appearance: 'success',
      });
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'error occurred while setting yours offer price',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        createOffer({ ...values, id: router.query.id });
        setSubmitting(false);
      }}
      validationSchema={OfferPriceValidationSchema}
      enableReinitialize
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">Please enter your offer Price !</h1>

          <Form>
            <div className="w-full mb-2">
              {/* <Input
                label="Offer Price(NRs)"
                name="offerPrice"
                type="text"
                placeholder="E.g: NRs 20000"
              /> */}
              <InputNumberFormat
                label="Offer Price(NRs)"
                name="offerPrice"
                value={values?.offerPrice || null}
                // displayType="text"
                thousandSeparator={true}
                prefix="NRs "
                onChange={(e) => {
                  setFieldValue(
                    'offerPrice',
                    removePrefixComma('NRs ', e.target.value)
                  );
                }}
              />
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingOfferPrice}
                disabled={isLoadingOfferPrice || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              {/* <Button
                // disabled={isLoadingOfferPrice || !dirty}
                type="reset"
                variant="outlined-error"
                onClick={() => {
                  setFieldValue('offerPrice', removePrefixComma('NRs ', ''));
                }}
              >
                Clear
              </Button> */}

              {/* <div className="mt-3"></div> */}
            </div>
            {/* {isError && (
          <div className="max-w-xs mt-3">
            <Alert
              message={error?.data?.message || 'Error logging in.'}
              type="error"
            />
          </div>
        )} */}
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default OfferPricePopUp;
