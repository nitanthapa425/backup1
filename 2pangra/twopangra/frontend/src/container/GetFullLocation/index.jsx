import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Formik } from 'formik';
import { useToasts } from 'react-toast-notifications';
import Input from 'components/Input';
import Button from 'components/Button';
import Popup from 'components/Popup';
import 'react-datepicker/dist/react-datepicker.css';
// import { wardNumber } from 'constant/constant';
import {
  usePostFullLocationMutation,
  useReadSingleLocationQuery,
  useUpdateFullLocationMutation,
} from 'services/api/getFullLocation';
import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { useRouter } from 'next/router';
import {
  useCreateMunicipalityVdcByDistrictMutation,
  useReadMunicipalityVdcByDistrictQuery,
  useReadAllDistrictQuery,
} from 'services/api/location';
// import { useSelector } from 'react-redux';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
import Select from 'components/Select';
import { FullLocationValidationSchema } from 'validation/getFullLocation';
import ReactSelect from 'components/Select/ReactSelect';
import { nNumberArray } from 'utils/nNumberOfArray';

const FullLocation = ({ type }) => {
  const router = useRouter();
  const formikBag = useRef();
  const locationId = router?.query?.id;

  // console.log('location id', locationId);
  const [districtValue, setDistrictValue] = useState(null);
  // const level = useSelector((state) => state.levelReducer.level);

  const { addToast } = useToasts();
  const [
    createFullLocation,
    { isError, error, isSuccess, isLoading, data: dataAddLocation },
  ] = usePostFullLocationMutation();
  const [createMunicipalityVdcByDistrict] =
    useCreateMunicipalityVdcByDistrictMutation();

  const [openModal, setOpenModal] = useState(false);
  const [editFullLocationInitialValue, setEditFullLocationInitialValue] =
    useState({});

  const { data: district, isFetching: isFetchingDistrict } =
    useReadAllDistrictQuery();

  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataAddLocation?.message ||
          'Location details has been added successfully.',
        {
          appearance: 'success',
        }
      );
      setChanged(false);

      router.push('/location/view');
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'Error occured while adding location details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  const addFullLocationInitialValue = {
    districtName: '',
    municipalityId: '',
    wardNumber: '',
    toleName: '',
  };

  const {
    data: locationDetails,
    error: singleLocationError,
    // isLoading: isLoadingLocation,
  } = useReadSingleLocationQuery(locationId, {
    skip: !locationId,
  });

  useEffect(() => {
    if (singleLocationError) {
      addToast(
        singleLocationError?.data?.message ||
          'Error occured while fetching location details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [singleLocationError]);

  useEffect(() => {
    if (locationDetails) {
      const newLOcationDetails = {
        districtName: locationDetails?.districtName,
        municipalityId: locationDetails?.municipalityId,
        wardNumber: locationDetails?.wardNumber,
        toleName: locationDetails?.toleName,
      };

      setEditFullLocationInitialValue(newLOcationDetails);

      setDistrictValue(newLOcationDetails.districtName);
    }
  }, [locationDetails]);

  // useEffect(() => {
  //   setDistrictValue(formikBag?.current?.values?.district);
  // }, [formikBag?.current?.values?.district]);

  const [
    updateFullLocation,
    {
      isLoading: updating,
      error: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateFullLocationMutation();
  useEffect(() => {
    if (isUpdateError) {
      addToast(
        isUpdateError?.data?.message ||
          'error occurred while updating location detail.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || 'Location Details updated successfully.',
        {
          appearance: 'success',
        }
      );
      setChanged(false);

      router.push('/location/view');
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (locationId) {
      updateFullLocation({ ...values, id: locationId });
    } else {
      createFullLocation(values, resetForm);
    }
    setSubmitting(false);
  };

  const {
    data: municipalityVdc,
    isFetching: isFetchingMunicipalityVdcByDistrict,
  } = useReadMunicipalityVdcByDistrictQuery(districtValue, {
    skip: !districtValue,
    // keepUnusedDataFor: 0,
  });

  return (
    <Formik
      initialValues={
        type === 'edit'
          ? editFullLocationInitialValue
          : addFullLocationInitialValue
      }
      onSubmit={onSubmit}
      validationSchema={FullLocationValidationSchema}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        setFieldTouched,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <Form className="pb-[80px]" onChange={() => setChanged(true)}>
            <div className="row-sm  pt-5 pb-2">
              <div className="three-col-sm">
                <SelectWithoutCreate
                  required
                  loading={isFetchingDistrict}
                  label="Select District"
                  placeholder="District"
                  error={touched?.districtName ? errors?.districtName : ''}
                  value={values?.districtName || null}
                  onChange={(selectedValue) => {
                    // setFieldTouched('district');
                    setFieldValue('districtName', selectedValue.value);
                    setDistrictValue(selectedValue.value);
                    setFieldValue('municipalityId', '');
                    setFieldTouched('municipalityId', false);
                  }}
                  options={district?.map((value) => ({
                    label: value.district,
                    value: value.district,
                  }))}
                  onBlur={() => {
                    setFieldTouched('districtName');
                  }}
                />
              </div>

              <div className="three-col-sm">
                <ReactSelect
                  required
                  isDisabled={!values?.districtName}
                  loading={isFetchingMunicipalityVdcByDistrict}
                  label="Select Municipality/VDC"
                  placeholder="Select Municipality/VDC"
                  error={touched?.municipalityId ? errors?.municipalityId : ''}
                  value={values?.municipalityId || ''}
                  onChange={(selectedValue, actionMeta) => {
                    setFieldTouched('municipalityId');
                    if (
                      selectedValue.value &&
                      actionMeta.action === 'create-option'
                    ) {
                      createMunicipalityVdcByDistrict({
                        // provinceName: values?.province,
                        districtName: values?.districtName,
                        municipalityName: selectedValue.value,
                      }).then((res, i) => {
                        setFieldValue('municipalityId', res.data.id);
                      });
                    } else {
                      setFieldValue('municipalityId', selectedValue.value);
                    }
                  }}
                  options={municipalityVdc?.map((value) => ({
                    label: value.municipalityName,
                    value: value.id,
                  }))}
                  onBlur={() => {
                    setFieldTouched('municipalityId');
                  }}
                />
              </div>

              <div className="three-col-sm">
                <Select label="Ward Number" name="wardNumber" required={true}>
                  <option value="">Select Ward Number</option>

                  {nNumberArray(35).map((value, i) => (
                    <option key={i} value={value}>
                      {value}
                    </option>
                  ))}
                </Select>
              </div>
              <div className="three-col-sm">
                <Input
                  required={true}
                  name="toleName"
                  label="Tole/Marg"
                  type="text"
                />
              </div>
            </div>
            <div className="c-fixed btn-holder">
              <Button
                type="submit"
                disabled={isSubmitting || !dirty}
                loading={isLoading || updating}
              >
                {!locationId ? 'Submit' : 'Update'}
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  setOpenModal(true);
                }}
                disabled={isSubmitting || !dirty || isLoading || updating}
              >
                Clear
              </Button>
              {openModal && (
                <Popup
                  title="Do you want to clear all fields?"
                  description="if you clear all the filed will be removed"
                  onOkClick={() => {
                    resetForm();
                    setChanged(false);

                    setOpenModal(false);
                  }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )}
            </div>
          </Form>
        </div>
      )}
    </Formik>
    // <>
  );
};

FullLocation.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default FullLocation;
