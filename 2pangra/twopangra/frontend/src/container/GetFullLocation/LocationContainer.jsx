import Table from 'components/Table/table';

import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';

import {
  useReadAllLocationQuery,
  useDeleteLocationMutation,
} from 'services/api/getFullLocation';

function ViewLocationContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [locationDataQuery, setLocationDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: locationData,
    isError: locationError,
    isFetching: isLoadingLocation,
  } = useReadAllLocationQuery(locationDataQuery);

  const {
    data: LocationDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllLocationQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });
  useEffect(() => {
    if (LocationDataAll) {
      setTableDataAll(
        LocationDataAll.docs.map((location) => {
          return {
            id: location.id,
            districtName: location.districtName,
            municipalityId: location?.municipalityId,
            wardNumber: location?.wardNumber,
            toleName: location?.toleName,
          };
        })
      );
    }
  }, [LocationDataAll]);
  // const {deleteInfo = useDeleteLocationMutation();
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteLocationMutation();

  const columns = useMemo(
    () => [
      {
        id: 'fullAddress',
        Header: 'Location',
        accessor: 'fullAddress',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: 'districtName',
        Header: 'District Name',
        accessor: 'districtName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'municipalityName',
        Header: 'Municipality Name',
        accessor: 'municipalityName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'wardNumber',
        Header: 'Ward Number',
        accessor: 'wardNumber',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'toleName',
        Header: 'Tole Name',
        accessor: 'toleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setLocationDataQuery(query);
  };

  useEffect(() => {
    if (locationData) {
      setPageCount(locationData.totalPages);
      setTotalData(locationData.totalDocs);
      setTableData(
        locationData.docs.map((location) => {
          return {
            id: location.id,
            districtName: location.districtName,
            municipalityName: location?.municipalityName,
            wardNumber: location?.wardNumber,
            toleName: location?.toleName,
            fullAddress: location?.fullAddress,
          };
        })
      );
    }
  }, [locationData]);

  return (
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="Location/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={locationError}
          isLoadingData={isLoadingLocation}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          editRoute="location"
          // viewRoute="location/get"
          deleteQuery={deleteItems}
          isDeleting={isDeleting}
          isDeleteError={isDeleteError}
          isDeleteSuccess={isDeleteSuccess}
          DeletedError={DeletedError}
          hasExport={true}
          addPage={{ page: 'Add Location', route: '/location' }}
        />
      </div>
    </section>
  );
}

export default ViewLocationContainer;
