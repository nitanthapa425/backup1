import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Form, Formik } from 'formik';
import UserLayout from 'layouts/User';

import { useToasts } from 'react-toast-notifications';

import Input from 'components/Input';
// import Select from '../Select/index';

import Button from 'components/Button';

import Popup from 'components/Popup';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import DropZone from 'components/DropZone';
import { SellerValidationSchema } from 'validation/seller.validation';
import {
  useSellerPostMutation,
  useUpdateSellerMutation,
  useReadSingleSellerQuery,
} from 'services/api/seller';
import { useReadVehicleCustomQuery } from 'services/api/vehicle';

import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { useRouter } from 'next/router';
import RadioBox from 'components/RadioBox/RadioBox';
import Textarea from 'components/Input/textarea';
import { beforeAfterDate } from 'utils/beforeAfterdate';
import Select from 'components/Select';
import { usedFor, condition, color, ownershipCount } from 'constant/constant';
import InputNumberFormat from 'components/Input/InputNumberFormat';
import { removePrefixComma } from 'utils/removePrefixComma';
import {
  // useCreateMunicipalityVdcByDistrictMutation,
  useReadDistrictsByProvincesQuery,
  useReadMunicipalityVdcByDistrictQuery,
  useReadProvincesQuery,
} from 'services/api/location';
// import ReactSelect from 'components/Select/ReactSelect';
// import NumberFormat from 'react-number-format';
// import NumberFormatInput from 'components/Input/NumberFormatInput';
const PostADS = ({ type }) => {
  const router = useRouter();
  const formikBag = useRef();
  const sellerId = router?.query?.id;
  const [provinceValue, setProvinceValue] = useState('');
  const [districtValue, setDistrictValue] = useState('');
  // const [municipalityValue, setMunicipalityValue] = useState('');
  const { addToast } = useToasts();
  const [createSellerInfo, { isError, error, isSuccess, isLoading }] =
    useSellerPostMutation();

  const [openModal, setOpenModal] = useState(false);
  const [editVehicleSellInitialValue, setEditVehicleSellInitialValue] =
    useState({});

  const { data: vehicleData, isFetching: fetchingVehicles } =
    useReadVehicleCustomQuery('');

  const { data: provinces, isFetching: isFetchingProvinces } =
    useReadProvincesQuery();
  const { data: district, isFetching: isFetchingDistrictByProvince } =
    useReadDistrictsByProvincesQuery(provinceValue, {
      skip: !provinceValue,
    });
  const {
    data: municipalityVdc,
    isFetching: isFetchingMunicipalityVdcByDistrict,
  } = useReadMunicipalityVdcByDistrictQuery(districtValue, {
    skip: !districtValue,
  });

  // createMarketPrice, { isLoading: isLoadingCreateMarketPrice }]
  // const [
  //   createMunicipalityVdcByDistrict,
  //   {
  //     isFetching: isFetchingCreateMunicipalityVdcByDistrict,
  //     isError: isErrorCreateMunicipalityVdcByDistrict,
  //     error: errorCreateMunicipalityVdcByDistrict,
  //   },
  // ] = useCreateMunicipalityVdcByDistrictMutation();

  const setNewFiles = (newFiles) => {
    if (!formikBag.current) return;
    const { values, setFieldValue } = formikBag?.current;
    const allFiles = [...values?.bikeImagePath, ...newFiles];
    setFieldValue('bikeImagePath', allFiles);
  };

  const handleRemoveFile = (idx) => {
    if (!formikBag.current) return;
    const { values, setFieldValue } = formikBag?.current;
    const currentFiles = [...values.bikeImagePath];
    currentFiles.splice(idx, 1);
    setFieldValue('bikeImagePath', currentFiles);
  };

  const setBillBoookFile = (newFiles) => {
    if (!formikBag.current) return;
    const { values, setFieldValue } = formikBag?.current;
    const allFiles = [...values?.billBookImagePath, ...newFiles];
    setFieldValue('billBookImagePath', allFiles);
  };

  const handleRemoveBillBookFile = (idx) => {
    if (!formikBag.current) return;
    const { values, setFieldValue } = formikBag?.current;
    const currentFiles = [...values.billBookImagePath];
    currentFiles.splice(idx, 1);
    setFieldValue('billBookImagePath', currentFiles);
  };

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('Selling Vehicle Details has been added successfully.', {
        appearance: 'success',
      });
      router.push('/post-ads/details');
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'Error occured while creating selling vehicle details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  const addVehicleSellInitialValue = {
    vehicleDetailId: '',
    bikeImagePath: [],
    billBookImagePath: [],
    bikeDriven: '',
    expectedPrice: '',
    bikeNumber: '',
    isNegotiable: true,
    color: [],
    condition: 'Brand New',
    mileage: '',
    postExpiryDate: beforeAfterDate(new Date(), 0, 2, 0),
    hasAccident: false,
    ownershipCount: '',
    note: '',
    usedFor: '1 Year',
    makeYear: '',
    location: {
      province: '',
      district: '',
      municipalityVdc: '',
      wardNo: '',
      toleMarg: '',
      streetRoadName: '',
      nearByLocation: '',
      // geoLocation: {
      //   type: 'Point',
      //   coordinates: [78899.89, 89897.89],
      // },
    },
  };

  const {
    data: singleSellerDetails,
    error: singleSellerFetchError,
    // isLoading: isLoadingBrand,
  } = useReadSingleSellerQuery(sellerId, {
    skip: !sellerId,
  });

  useEffect(() => {
    if (singleSellerFetchError) {
      addToast(
        'Error occured while fetching Vehicle Sell details. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [singleSellerFetchError]);

  let BreadCrumbList = [];

  if (type === 'add') {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Add Selling Vehicle Information',
        route: '/seller',
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: 'Add Vehicle',
        route: '/vehicle/add',
      },
      {
        routeName: 'Selling Vehicle List',
        route: '/seller/view',
      },
      {
        routeName: 'Edit Selling Vehicle Information',
        route: 'seller/edit',
      },
    ];
  }

  useEffect(() => {
    if (singleSellerDetails) {
      const newsingleSellerDetails = {
        vehicleDetailId: singleSellerDetails?.sellerValue?.vehicleDetailId,
        bikeImagePath: singleSellerDetails?.sellerValue?.bikeImagePath,
        billBookImagePath: singleSellerDetails?.sellerValue?.billBookImagePath,
        bikeDriven: singleSellerDetails?.sellerValue?.bikeDriven,
        expectedPrice: singleSellerDetails?.sellerValue?.expectedPrice,
        bikeNumber: singleSellerDetails?.sellerValue?.bikeNumber,
        isNegotiable: singleSellerDetails?.sellerValue?.isNegotiable,
        color: singleSellerDetails?.sellerValue?.color || [],
        condition: singleSellerDetails?.sellerValue?.condition,
        postExpiryDate: singleSellerDetails?.sellerValue?.postExpiryDate,
        hasAccident: singleSellerDetails?.sellerValue?.hasAccident,
        ownershipCount: singleSellerDetails?.sellerValue?.ownershipCount,
        note: singleSellerDetails?.sellerValue?.note,
        mileage: singleSellerDetails?.sellerValue?.mileage,
        usedFor: singleSellerDetails?.sellerValue?.usedFor,
        makeYear: singleSellerDetails?.sellerValue?.makeYear,
        location: {
          province: singleSellerDetails?.sellerValue?.location?.province,
          district: singleSellerDetails?.sellerValue?.location?.district,
          municipalityVdc:
            singleSellerDetails?.sellerValue?.location?.municipalityVdc,
          wardNo: singleSellerDetails?.sellerValue?.location?.wardNo,
          toleMarg: singleSellerDetails?.sellerValue?.location?.toleMarg,
          streetRoadName:
            singleSellerDetails?.sellerValue?.location?.streetRoadName,
          nearByLocation:
            singleSellerDetails?.sellerValue?.location?.nearByLocation,
          // geoLocation: {
          //   type: 'Point',
          //   coordinates: [78899.89, 89897.89],
          // },
        },
      };

      setEditVehicleSellInitialValue(newsingleSellerDetails);
    }
  }, [singleSellerDetails]);

  const [
    updateSeller,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateSellerMutation();
  useEffect(() => {
    if (isUpdateError) {
      addToast('error occured while updating vehicle sell.', {
        appearance: 'error',
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        updateSuccessData?.message ||
          'Selling Vehicle Details updated successfully.',
        {
          appearance: 'success',
        }
      );

      router.push(`/seller/get/${sellerId}`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    values.location.country = 'Nepal';
    if (sellerId) {
      updateSeller({ ...values, id: sellerId });
    } else {
      createSellerInfo(values, resetForm);
    }
    setSubmitting(false);
  };

  return (
    <UserLayout
      documentTitle={
        !sellerId ? 'Add New Vehicle Sell' : 'Edit Vehicle sell Details'
      }
      BreadCrumbList={BreadCrumbList}
    >
      <Formik
        initialValues={
          sellerId ? editVehicleSellInitialValue : addVehicleSellInitialValue
        }
        onSubmit={onSubmit}
        validationSchema={SellerValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div className="container mt-4">
            <h3 className="mb-3">Selling Vehicle Information </h3>

            <Form>
              <div className="row-sm  pt-5 pb-2">
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={fetchingVehicles}
                    label="Select Vehicle Name"
                    placeholder="E.g: Honda"
                    error={
                      touched?.vehicleDetailId ? errors?.vehicleDetailId : ''
                    }
                    value={values.vehicleDetailId || null}
                    onChange={(selectdVehicleDetailId) => {
                      setFieldTouched('vehicleDetailId');
                      setFieldValue(
                        'vehicleDetailId',
                        selectdVehicleDetailId.value
                      );
                    }}
                    options={vehicleData?.docs?.map((vehicle) => ({
                      value: vehicle.id,
                      label: vehicle.vehicleName,
                    }))}
                    onBlur={() => {
                      setFieldTouched('vehicleDetailId');
                    }}
                  />
                </div>
                <div className="three-col-sm">
                  <DropZone
                    label="Bike Images"
                    required
                    currentFiles={values.bikeImagePath}
                    // values.bikeImagePath ? [values.bikeImagePath] : []
                    setNewFiles={setNewFiles}
                    handleRemoveFile={handleRemoveFile}
                    error={touched?.bikeImagePath ? errors?.bikeImagePath : ''}
                  />
                </div>
                <div className="three-col-sm">
                  <DropZone
                    label="Blue-Book Images"
                    required
                    currentFiles={values.billBookImagePath}
                    setNewFiles={setBillBoookFile}
                    handleRemoveFile={handleRemoveBillBookFile}
                    error={
                      touched?.billBookImagePath
                        ? errors?.billBookImagePath
                        : ''
                    }
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    label="Bike Driven"
                    name="bikeDriven"
                    type="text"
                    placeholder="E.g: 1000"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    label="Bike Number"
                    name="bikeNumber"
                    type="text"
                    placeholder="E.g: Ba6pa:2045"
                  />
                </div>
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    // loading={fetchingVehicles}
                    label="Ownership Count"
                    placeholder="1"
                    error={
                      touched?.ownershipCount ? errors?.ownershipCount : ''
                    }
                    value={values.ownershipCount}
                    onChange={(ownershipCount) => {
                      setFieldTouched('ownershipCount');
                      setFieldValue('ownershipCount', ownershipCount.value);
                    }}
                    options={ownershipCount.map((value) => ({
                      value: value,
                      label: value,
                    }))}
                    onBlur={() => {
                      setFieldTouched('ownershipCount');
                    }}
                  />
                </div>

                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    // loading={fetchingVehicles}
                    label="Color"
                    placeholder="Red"
                    error={touched?.color ? errors?.color : ''}
                    value={values.color}
                    onChange={(color) => {
                      setFieldTouched('color');
                      setFieldValue(
                        'color',
                        color.map((netTag) => netTag.value)
                      );
                    }}
                    options={color.map((value) => ({
                      value: value,
                      label: value,
                    }))}
                    onBlur={() => {
                      setFieldTouched('color');
                    }}
                    isMulti={true}
                  />
                </div>

                <div className="three-col-sm">
                  <label htmlFor="">
                    Post Expiry Date(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={
                      values?.postExpiryDate
                        ? new Date(values?.postExpiryDate)
                        : null
                    }
                    onChange={(date) => {
                      setFieldValue(
                        'postExpiryDate',
                        date?.toLocaleDateString()
                      );
                      setFieldTouched('postExpiryDate');
                    }}
                    onBlur={() => {
                      setFieldTouched('postExpiryDate');
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    minDate={beforeAfterDate(new Date(), 0, 2, 0)}
                    maxDate={beforeAfterDate(new Date(), 0, 6, 0)}
                  />

                  {touched.postExpiryDate && errors.postExpiryDate && (
                    <div className="text-primary-dark">
                      {errors.postExpiryDate}
                    </div>
                  )}
                </div>

                <div className="three-col-sm">
                  <Select label="Condition" name="condition">
                    <option value="">Select Condition</option>

                    {condition.map((value, i) => (
                      <option key={i} value={value}>
                        {value}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Mileage"
                    name="mileage"
                    type="text"
                    placeholder="E.g: 35"
                  />
                </div>
                {/* {console.log(values?.expectedPrice)} */}
                <div className="three-col-sm">
                  <InputNumberFormat
                    label="Expected Price(NRs)"
                    name="expectedPrice"
                    value={values?.expectedPrice || null}
                    // displayType="text"
                    thousandSeparator={true}
                    prefix="NRs "
                    onChange={(e) => {
                      setFieldValue(
                        'expectedPrice',
                        removePrefixComma('NRs ', e.target.value)
                      );
                    }}
                  ></InputNumberFormat>
                </div>
                <div className="three-col-sm">
                  <RadioBox
                    title="Is Negotiable"
                    name="isNegotiable"
                    values={[
                      { label: 'Yes', value: true },
                      { label: 'No', value: false },
                    ]}
                    onChange={(e) => {
                      setFieldValue('isNegotiable', e.target.value);
                    }}
                  ></RadioBox>
                </div>

                <div className="three-col-sm">
                  <RadioBox
                    title="Had Accident"
                    name="hasAccident"
                    values={[
                      { label: 'Yes', value: true },
                      { label: 'No', value: false },
                    ]}
                    onChange={(e) => {
                      setFieldValue('hasAccident', e.target.value);
                    }}
                  ></RadioBox>
                </div>

                {/* <div className="three-col-sm">
                  <Input
                    label="Note"
                    name="note"
                    type="text"
                    placeholder="E.g: Description note"
                    required={false}
                  />
                </div> */}

                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    // loading={fetchingVehicles}
                    label="vehicle used time"
                    placeholder="1 Month"
                    error={touched?.usedFor ? errors?.usedFor : ''}
                    value={values.usedFor}
                    onChange={(usedFor) => {
                      setFieldTouched('usedFor');
                      setFieldValue('usedFor', usedFor.value);
                    }}
                    options={usedFor.map((value) => ({
                      value: value,
                      label: value,
                    }))}
                    onBlur={() => {
                      setFieldTouched('usedFor');
                    }}
                  />
                </div>

                <div className="three-col-sm">
                  <label> Make Year</label>
                  <DatePicker
                    selected={
                      values?.makeYear ? new Date(values?.makeYear) : null
                    }
                    onChange={(date) => {
                      setFieldValue('makeYear', date || '');
                    }}
                    showYearPicker
                    dateFormat="yyyy"
                    yearItemNumber={9}
                    placeholderText="E.g: 2021"
                    maxDate={new Date()}
                  />
                  {touched.makeYear && errors.makeYear && (
                    <div className="text-primary-dark">{errors.makeYear}</div>
                  )}
                </div>

                <div className="three-col-sm">
                  <Textarea
                    label="Note"
                    name="note"
                    placeholder="E.g: Description note"
                    required={false}
                  />
                </div>

                <div className="w-full p-4 mt-4">
                  <p className="text-lg mb-2 text-gray-800">Location</p>
                  <hr className="border-t border-gray-400" />
                </div>
                {/* <div className="three-col-sm">
                <Input name="location.province" label="Province" type="text" />
              </div> */}
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={isFetchingProvinces}
                    label="Select Province"
                    placeholder="Province"
                    error={
                      touched?.location?.province
                        ? errors?.location?.province
                        : ''
                    }
                    value={values?.location?.province || null}
                    onChange={(selectedValue) => {
                      // setFieldTouched('location.province');
                      setFieldValue('location.province', selectedValue.value);
                      setProvinceValue(selectedValue.value);
                      setFieldValue('location.district', '');
                      setFieldValue('location.municipalityVdc', '');
                      setFieldTouched('location.district', false);
                      setFieldTouched('location.municipalityVdc', false);
                    }}
                    options={provinces?.map((value) => ({
                      label: value.name,
                      value: `${value.id}`,
                    }))}
                    onBlur={() => {
                      setFieldTouched('location.province');
                    }}
                  />
                </div>
                {/* <div className="three-col-sm">
                <Input name="location.district" label="District" type="text" />
              </div> */}
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    isDisabled={!values?.location?.province}
                    loading={isFetchingDistrictByProvince}
                    label="Select District"
                    placeholder="District"
                    error={
                      touched?.location?.district
                        ? errors?.location?.district
                        : ''
                    }
                    value={values?.location?.district || null}
                    onChange={(selectedValue) => {
                      // setFieldTouched('location.district');
                      setFieldValue('location.district', selectedValue.value);
                      setDistrictValue(selectedValue.value);
                      setFieldValue('location.municipalityVdc', '');
                      setFieldTouched('location.municipalityVdc', false);
                    }}
                    options={district?.map((value) => ({
                      label: value.district,
                      value: value.district,
                    }))}
                    onBlur={() => {
                      setFieldTouched('location.district');
                    }}
                  />
                </div>

                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    isDisabled={!values?.location?.district}
                    loading={isFetchingMunicipalityVdcByDistrict}
                    label="Select Municipality/VDC"
                    placeholder="Select Municipality/VDC"
                    error={
                      touched?.location?.municipalityVdc
                        ? errors?.location?.municipalityVdc
                        : ''
                    }
                    value={values?.location?.municipalityVdc || ''}
                    onChange={(selectedValue, actionMeta) => {
                      setFieldTouched('location.municipalityVdc');
                      setFieldValue(
                        'location.municipalityVdc',
                        selectedValue.value
                      );
                    }}
                    options={municipalityVdc?.map((value) => ({
                      label: value.municipalityName,
                      value: value.municipalityName,
                    }))}
                    onBlur={() => {
                      setFieldTouched('location.municipalityVdc');
                    }}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name="location.wardNo"
                    label="Ward Number"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={false}
                    name="location.toleMarg"
                    label="Tole/Marg"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={false}
                    name="location.streetRoadName"
                    label="Street/Road Name"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={false}
                    name="location.nearByLocation"
                    label="Near By Location"
                    type="text"
                  />
                </div>
              </div>
              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading || updating}
                >
                  {!sellerId ? 'Submit' : 'Update'}
                </Button>
                <Button
                  variant="outlined"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={isSubmitting || !dirty || isLoading || updating}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </UserLayout>
  );
};

PostADS.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default PostADS;
