import React from 'react';
// import Image from 'next/image';
import UserLayout from 'layouts/User';
import { useRouter } from 'next/router';
// import { useToasts } from 'react-toast-notifications';

import Link from 'next/link';
import { useReadSingleSellerQuery } from 'services/api/seller';
import Button from 'components/Button';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';

const GetSellerDetails = () => {
  const router = useRouter();
  // const { addToast } = useToasts();
  const sellerId = router?.query?.id;

  const {
    data: sellerProfile,

    // error: sellerProfileFetchError,
    isLoading: isLoadingSellerProfile,
  } = useReadSingleSellerQuery(sellerId, {
    skip: !sellerId,
  });

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Selling Vehicle List',
      route: '/seller/view',
    },
    {
      routeName: 'Selling Vehicle Information',
      route: '',
    },
  ];
  return (
    // <AdminLayout

    // >

    // </AdminLayout>
    <UserLayout
      documentTitle="Get Selling Vehicle Information"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section mt-2">
        <div className="container">
          <div className="flex items-center mb-5">
            <h1 className="h4 font-semibold flex-1">
              Selling Vehicle Information
            </h1>

            <div className=" w-[100px] ml-5 seller-detail-edit-btn">
              <Link href={`/seller/edit/${sellerId}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
          </div>

          {isLoadingSellerProfile ? (
            'Loading...'
          ) : (
            <>
              <div className="row">
                <div className="two-col  sm:w-[75%] lg:w-[40%]">
                  <h2 className="h5 font-semibold mb-3">Bike Images</h2>
                  <div>
                    {sellerProfile?.sellerValue?.bikeImagePath.map(
                      (imageUrl, i) => (
                        <div key={i}>
                          {imageUrl.imageUrl && (
                            <img
                              src={`${imageUrl.imageUrl}`}
                              alt="Bike Image"
                              className="w-full rounded-sm"
                            />
                          )}
                        </div>
                      )
                    )}
                  </div>
                </div>
                <div className="two-col w-[100%] lg:w-[60%]">
                  <h2 className="h5 font-semibold mb-3">Bike Detail</h2>
                  <div className="detail-holder px-6 py-4 border border-gray-300 rounded-sm">
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold  mr-2">
                        Vehicle Name :
                      </span>

                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.vehicleName}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Bike Driven :
                      </span>

                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.bikeDriven}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Expected Price :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.expectedPrice &&
                          thousandNumberSeparator(
                            sellerProfile?.sellerValue?.expectedPrice
                          )}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Bike Number :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.bikeNumber}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Ownership Count :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.ownershipCount}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Color :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.color}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Post Expiry Date :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.postExpiryDate
                          ? new Date(
                              sellerProfile?.sellerValue?.postExpiryDate
                            ).toLocaleDateString()
                          : null}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Vehicle Used For :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.usedFor}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Condition :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.condition}
                      </span>
                    </div>

                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Had Accident :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.hasAccident ? 'Yes' : 'No'}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Is Negotiable :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.isNegotiable
                          ? 'Yes'
                          : 'No'}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Note :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.note
                          ? sellerProfile?.sellerValue?.note
                          : 'Not Available'}
                      </span>
                    </div>

                    <div className="w-full p-4 mt-4">
                      <p className="text-lg mb-2 text-gray-800 font-bold">
                        Location
                      </p>
                      <hr className="border-t border-gray-400" />
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Province :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.province}
                      </span>
                    </div>

                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        District :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.district}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Municipality/VDC :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.municipalityVdc}
                      </span>
                    </div>
                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Ward Number :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.wardNo
                          ? sellerProfile?.sellerValue?.location?.wardNo
                          : 'Not Available'}
                      </span>
                    </div>

                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Tole/Marg :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.toleMarg
                          ? sellerProfile?.sellerValue?.location?.toleMarg
                          : 'Not Available'}
                      </span>
                    </div>

                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Street/Road Name :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.streetRoadName
                          ? sellerProfile?.sellerValue?.location?.streetRoadName
                          : 'Not Available'}
                      </span>
                    </div>

                    <div className="flex mb-3">
                      <span className="inline-block sm:w-[200px] font-bold mr-2">
                        Near By Location :
                      </span>
                      <span className="inline-block flex-1">
                        {sellerProfile?.sellerValue?.location?.nearByLocation
                          ? sellerProfile?.sellerValue?.location?.nearByLocation
                          : 'Not Available'}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </UserLayout>
  );
};

export default GetSellerDetails;
