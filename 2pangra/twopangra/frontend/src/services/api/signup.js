import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';
export const signup = createApi({
  reducerPath: 'signup',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    SignUpPost: builder.mutation({
      query: (data) => {
        return {
          url: `/signUp`,
          method: 'POST',
          body: data,
        };
      },
    }),
    readUser: builder.query({
      query: (id) => {
        return {
          url: `/user/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllUsers'],
    }),
    deleteUser: builder.mutation({
      query: (id) => {
        return {
          url: `/user/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllUsers'],
    }),
    updateUser: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/user/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllUsers'],
    }),
    readDeletedUser: builder.query({
      query: (id) => {
        return {
          url: `/deletedUser/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readDeletedUser'],
    }),
    deleteDeletedUser: builder.mutation({
      query: (id) => {
        return {
          url: `/deletedUser/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllUsers', 'readDeletedUser'],
    }),
    activeDeletedUser: builder.mutation({
      query: (id) => {
        return {
          url: `/revertUser/${id}`,
          method: 'POST',
        };
      },
      invalidatesTags: ['readAllUsers', 'readDeletedUser'],
    }),
    updateDeletedUser: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/deletedUser/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllUsers', 'readSingleUserDetails'],
    }),
    readUserProfileDetails: builder.query({
      query: (id) => {
        return {
          url: `/user/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readSingleUserDetails'],
    }),

    updateUserProfileDetails: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/user/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllUsers', 'readSingleUserDetails'],
    }),

    readMyProfileDetails: builder.query({
      query: () => {
        return {
          url: `/me/profile`,
          method: 'GET',
        };
      },
      providesTags: ['readMyProfile'],
    }),

    updateMyProfileDetails: builder.mutation({
      query: (data) => {
        // const { id, ...data } = updatePostData;
        return {
          url: `/me/profile`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readMyProfile'],
    }),

    verifyEmail: builder.mutation({
      query: (data) => {
        return {
          url: `/confirmEmail`,
          method: 'POST',
          body: data,
        };
      },
    }),
    invalidatesTags: ['readAllUsers', 'readSingleUserDetails'],
  }),
});

export const {
  useSignUpPostMutation,
  useReadUserQuery,
  useDeleteUserMutation,
  useUpdateUserMutation,
  useReadDeletedUserQuery,
  useDeleteDeletedUserMutation,
  useUpdateDeletedUserMutation,
  useReadUserProfileDetailsQuery,
  useUpdateUserProfileDetailsMutation,
  useReadMyProfileDetailsQuery,
  useUpdateMyProfileDetailsMutation,
  useVerifyEmailMutation,
  useActiveDeletedUserMutation,
} = signup;
