import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const getFullLocation = createApi({
  reducerPath: 'getFullLocation',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readAllLocation'],

  endpoints: (builder) => ({
    readAllLocation: builder.query({
      query: (query) => {
        return {
          // get full Location
          url: `/geo-location/full-location${query}`,

          method: 'GET',
        };
      },
      providesTags: ['readAllLocation'],
    }),
    readSingleLocation: builder.query({
      query: (locationId) => {
        return {
          // get single Location
          url: `/geo-location/full-location/${locationId}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllLocation'],
    }),
    getDetailLocation: builder.query({
      query: () => {
        return {
          // get detail Location
          url: `/geo-location/full-location-no-auth`,
          method: 'GET',
        };
      },
      providesTags: ['readAllLocation'],
    }),

    postFullLocation: builder.mutation({
      query: (data) => {
        return {
          url: `/geo-location/full-location`,
          // Post Full Location
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['readAllLocation'],
    }),
    updateFullLocation: builder.mutation({
      query: ({ id, ...data }) => {
        return {
          url: `/geo-location/full-location/${id}`,

          method: 'PATCH',
          body: data,
        };
      },
      invalidatesTags: ['readAllLocation'],
    }),
    deleteLocation: builder.mutation({
      query: (id) => {
        return {
          url: `/geo-location/full-location/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllLocation'],
    }),
  }),
});

export const {
  useReadAllLocationQuery,
  useGetDetailLocationQuery,
  usePostFullLocationMutation,
  useDeleteLocationMutation,
  useReadSingleLocationQuery,
  useUpdateFullLocationMutation,
} = getFullLocation;
