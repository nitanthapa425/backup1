import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';
import { store } from 'store/store';

export const seller = createApi({
  reducerPath: 'seller',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      // const token = getState().customerAuth.token;
      const token =
        getState().levelReducer.level === 'superAdmin'
          ? getState().adminAuth.token
          : getState().levelReducer.level === 'customer'
          ? getState().customerAuth.token
          : '';
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: [
    'readSellerDetail',
    'readAllSellers',
    'readVehicleSell',
    'getMyBook',
    'readSingleBookDetails',
    'getComment',
    'getOffer',
  ],

  endpoints: (builder) => ({
    SellerPost: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/vehicleSell`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    adminSellerPost: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/vehicleSellByAdmin`,
          method: 'POST',
          body: data,
        };
      },
    }),

    readSeller: builder.query({
      query: (query) => {
        return {
          url: `/seller/vehicleSell${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllSellers'],
    }),
    readVehicleSell: builder.query({
      query: (query) => {
        let url = '';
        if (store.getState().levelReducer.level === 'superAdmin') {
          url = `/seller/vehicleSell/${query}`;
        } else if (store.getState().levelReducer.level === 'customer') {
          url = `/seller/getSellerVehicleDetail/${query}`;
        }
        return {
          url: url,
          method: 'GET',
        };
      },
      providesTags: ['readVehicleSell'],
    }),

    getHotDeals: builder.query({
      query: () => {
        return {
          url: `/seller/getHotDeals`,
          method: 'GET',
        };
      },
    }),

    readSingleSeller: builder.query({
      query: (id) => {
        return {
          url: `/seller/vehicleSell/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readSellerDetail'],
    }),

    deleteSeller: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/vehicleSell/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
        'readVehicleSell',
      ],
    }),

    updateSeller: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;

        return {
          url: `/seller/vehicleSell/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
      ],
    }),

    addToWishList: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/addTowishlist/${vehicleId}`,

          method: 'POST',
        };
      },
      invalidatesTags: ['getMyWishList'],
    }),
    getMyWishList: builder.query({
      query: (userId) => {
        return {
          url: `/seller/getClientWishList`,
          method: 'GET',
        };
      },
      providesTags: ['getMyWishList'],
    }),
    deleteMyWishList: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/deleteWishList/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['getMyWishList'],
    }),

    addToCart: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/addToCart/${vehicleId}`,
          method: 'POST',
        };
      },
      invalidatesTags: ['getMyCart', 'getMyWishList'],
    }),

    addToCartFromWishList: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/cartNowWishList/${vehicleId}`,
          method: 'POST',
        };
      },
      invalidatesTags: ['getMyCart', 'getMyWishList'],
    }),
    getMyCart: builder.query({
      query: (userId) => {
        return {
          url: `/seller/getClientAddToCart`,
          method: 'GET',
        };
      },
      providesTags: ['getMyCart'],
    }),
    deleteMyCart: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/deleteCart/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['getMyCart'],
    }),

    addToBook: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/bookNow/${data.id}`,
          method: 'POST',
          body: data.values,
        };
      },
      invalidatesTags: ['getMyBook', 'getMyCart'],
    }),
    getMyBook: builder.query({
      query: (userId) => {
        return {
          url: `/seller/getClientBookedVehicle`,
          method: 'GET',
        };
      },
      providesTags: ['getMyBook'],
    }),
    deleteMyBook: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/deleteBookedVehicle/${vehicleId}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['getMyBook'],
    }),
    addFeaturedBike: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/add-feature-vehicle/${id}`,
          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),
    verified: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/verifyVehicle/${vehicleId}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),
    removeFeaturedBike: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/remove-feature-vehicle/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),
    unVerified: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/unverifySellerVerify/${vehicleId}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),
    expirePost: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/removeAd/${id}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),
    addHotDeal: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/addInHotDeals/${id}`,
          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    approved: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/approveSellerVehicle/${vehicleId}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    unApproved: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/unApproveSellerVehicle/${vehicleId}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    sold: builder.mutation({
      query: (body) => {
        return {
          url: `/seller/mark-as-sold`,
          method: 'PATCH',
          body: body,
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    unSold: builder.mutation({
      query: (body) => {
        return {
          url: `/seller/mark-as-unsold`,

          method: 'PATCH',
          body: body,
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
      ],
    }),

    meetingSchedule: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/meetingSchedule/${data.bookId}`,

          method: 'POST',
          body: data.values,
        };
      },

      invalidatesTags: ['getMyBook', 'readSingleBookDetails'],
    }),

    updateSingleMeetingSchedule: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/meetingSchedule/${data.bookId}`,
          method: 'POST',
          body: data.values,
        };
      },
      invalidatesTags: [
        'getMyBook',
        'readMeetingSchedule',
        'readSingleBookDetails',
      ],
    }),

    readSingleMeetingSchedule: builder.query({
      query: (bookId) => {
        return {
          url: `/seller/meetingSchedule/${bookId}`,
          method: 'GET',
        };
      },
      keepUnusedDataFor: 0,
      providesTags: ['readMeetingSchedule'],
    }),

    deleteSingleMeetingSchedule: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/book/${id}/meetingSchedule`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        'readMeetingSchedule',
        'readSingleBookDetails',
        'getMyBook',
      ],
    }),

    readSingleBookDetails: builder.query({
      query: (id) => {
        return {
          url: `/seller/getBookedVehicleById/${id}`,
          method: 'GET',
        };
      },
      // keepUnusedDataFor: 0,
      providesTags: ['readSingleBookDetails'],
    }),

    // For Invalidation in book list.
    readAllBookInv: builder.query({
      query: (query) => {
        return {
          url: `/seller/bookNow/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['getMyBook'],
    }),

    deleteBookedList: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/bookedVehicleByAdmin/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['getMyBook'],
    }),
    AddVehicleInFeatureCustomer: builder.mutation({
      query: (data) => {
        return {
          url: `seller/addFeatureByClient/${data.id}`,
          method: 'POST',
          body: data.values,
        };
      },
    }),
    AddToFeatureQueue: builder.mutation({
      query: (id) => {
        return {
          url: `seller/addFeatureByClient/${id}`,
          method: 'POST',
          // body: data.values,
        };
      },
    }),
    // comment
    comment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/${id}`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: [
        'getComment',
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
      ],
    }),
    getComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['getComment'],
    }),
    getAllComment: builder.query({
      query: () => {
        return {
          url: `/comment`,
          method: 'GET',
        };
      },
      providesTags: ['getComment'],
    }),
    deleteComment: builder.mutation({
      query: (id) => {
        return {
          url: `/comment/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        'getComment',
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
      ],
    }),
    updateComment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: [
        'getComment',
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
        'getSingleComment',
      ],
    }),
    getSingleComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/single/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['getSingleComment'],
    }),
    replyComment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/createReplyComment/${id}`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: [
        'getComment',
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
        'getReplyOfComment',
      ],
    }),
    getReply: builder.query({
      query: (id) => {
        return {
          url: `/reply/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['getComment'],
    }),
    getSingleReply: builder.query({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/createReplyComment${replyId}`,
          method: 'GET',
        };
      },
      providesTags: ['getSingleComment'],
    }),
    allReplyComment: builder.query({
      query: (id) => {
        // const { id, ...data } = vehicleDate;
        return {
          url: `/comment/createReplyComment/${id}`,
          method: 'GET',
          // body: data,
        };
      },
    }),

    getReplyOfComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/createReplyComment/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['getReplyOfComment'],
    }),
    deleteReplyComment: builder.mutation({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['getReplyOfComment', 'getSingleReplyOfComment'],
    }),
    editReplyComment: builder.mutation({
      query: ({ commentId, replyId, body }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: 'PUT',
          body: body,
        };
      },
      invalidatesTags: ['getReplyOfComment', 'getSingleReplyOfComment'],
    }),

    getSingleReplyOfComment: builder.query({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: 'GET',
        };
      },
      providesTags: ['getSingleReplyOfComment'],
    }),
    offerPrice: builder.mutation({
      query: (vehicleData) => {
        const { id, ...data } = vehicleData;
        return {
          url: `seller/offerPrice/${id}`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: [
        'getOffer',
        'readSellerDetail',
        'readAllSellers',
        'readVehicleSell',
      ],
    }),

    getOfferDetails: builder.query({
      query: (query) => {
        return {
          url: `/seller/getOfferDetails/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllSellers', 'getOffer'],
    }),
    rejectOfferPrice: builder.mutation({
      query: ({ values, id }) => {
        return {
          url: `/seller/rejectOfferPrice/${id}`,
          body: values,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
        'getOffer',
      ],
    }),
    approveOfferPrice: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/approveOfferPrice/${id}`,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
        'getOffer',
      ],
    }),
    finalOffer: builder.mutation({
      query: ({ values, id }) => {
        return {
          url: `/seller/finalOffer/${id}`,
          body: values,

          method: 'POST',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
        'getOffer',
      ],
    }),
    deleteOfferPrice: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/offer/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        'readSellerDetail',
        'readAllSellers',
        'readSellerAllVehicle',
        'readVehicleSell',
        'getOffer',
      ],
    }),
    getOfferStatus: builder.query({
      query: (id) => {
        return {
          url: `/seller/getClientOffer/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllSellers', 'getOffer'],
    }),

    editVehicleSellByAdmin: builder.mutation({
      query: ({ id, body }) => {
        return {
          url: `/seller/vehicleSellByAdmin/${id}`,
          method: 'PUT',
          body: body,
        };
      },
      invalidatesTags: ['readSellerDetail'],
      // invalidatesTags: ['getReplyOfComment', 'getSingleReplyOfComment'],
    }),
  }),
});

export const {
  useSellerPostMutation,
  useReadSellerQuery,
  useReadSingleSellerQuery,
  useDeleteSellerMutation,
  useUpdateSellerMutation,
  useGetHotDealsQuery,
  useAddToCartMutation,
  useGetMyCartQuery,
  useAddToBookMutation,
  useGetMyBookQuery,
  useAddToWishListMutation,
  useGetMyWishListQuery,
  useDeleteMyWishListMutation,
  useDeleteMyCartMutation,
  useDeleteMyBookMutation,
  useAddToCartFromWishListMutation,
  useReadVehicleSellQuery,
  useAddFeaturedBikeMutation,
  useRemoveFeaturedBikeMutation,
  useAddHotDealMutation,
  useVerifiedMutation,
  useUnVerifiedMutation,
  useApprovedMutation,
  useUnApprovedMutation,
  useSoldMutation,
  useUnSoldMutation,
  useMeetingScheduleMutation,
  useReadSingleMeetingScheduleQuery,
  useReadSingleBookDetailsQuery,
  useUpdateSingleMeetingScheduleMutation,
  useDeleteSingleMeetingScheduleMutation,
  useExpirePostMutation,
  useReadAllBookInvQuery,
  useDeleteBookedListMutation,
  useAddVehicleInFeatureCustomerMutation,
  useAddToFeatureQueueMutation,
  useCommentMutation,
  useReplyCommentMutation,
  useGetCommentQuery,
  useGetAllCommentQuery,
  useDeleteCommentMutation,
  useUpdateCommentMutation,
  useGetSingleCommentQuery,
  useAdminSellerPostMutation,
  useGetReplyQuery,
  useGetSingleReplyQuery,
  useAllReplyCommentQuery,
  useGetReplyOfCommentQuery,
  useDeleteReplyCommentMutation,
  useEditReplyCommentMutation,
  useGetSingleReplyOfCommentQuery,
  useOfferPriceMutation,
  useGetOfferDetailsQuery,
  useRejectOfferPriceMutation,
  useApproveOfferPriceMutation,
  useFinalOfferMutation,
  useDeleteOfferPriceMutation,
  useGetOfferStatusQuery,
  useEditVehicleSellByAdminMutation,
} = seller;
