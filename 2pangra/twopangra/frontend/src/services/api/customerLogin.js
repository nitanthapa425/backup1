import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const customerLogin = createApi({
  reducerPath: 'customerLogin',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    loginCustomer: builder.mutation({
      query: (data) => {
        return {
          url: `/client/clientLogin`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { useLoginCustomerMutation } = customerLogin;
