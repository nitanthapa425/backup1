import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const resetPassword = createApi({
  reducerPath: 'resetPassword',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    resetPasswordPost: builder.mutation({
      query: (data) => {
        return {
          url: `/resetPassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { useResetPasswordPostMutation } = resetPassword;
