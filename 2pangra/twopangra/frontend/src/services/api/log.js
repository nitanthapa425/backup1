import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const log = createApi({
  reducerPath: 'log',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readLog'],

  endpoints: (builder) => ({
    readLog: builder.query({
      query: (query) => {
        return {
          url: `/log-management/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readLog'],
      keepUnusedDataFor: 0,
    }),
    readAllLog: builder.query({
      query: (query) => {
        return {
          url: `/log-management/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllLog'],
    }),
  }),
});

export const { useReadAllLogQuery, useReadLogQuery } = log;
