import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const subModel = createApi({
  reducerPath: 'subModel',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readVarientDetail,readAllVarient'],

  endpoints: (builder) => ({
    readSubModel: builder.query({
      query: (customQuery) => {
        return {
          url: `/varient/${customQuery}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllVarient'],
    }),

    deleteSubModel: builder.mutation({
      query: (id) => {
        return {
          url: `/varient/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readVarientDetail', 'readAllVarient', 'readLog'],
    }),

    updateVariant: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/varient/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readVarientDetail', 'readAllVarient'],
    }),

    createSubModel: builder.mutation({
      query: (data) => {
        return {
          url: `/varient`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['readVarientDetail', 'readAllVarient'],
    }),
    getSubModelFromModel: builder.query({
      query: (modelId) => {
        return {
          url: `/varientByModelId/${modelId}`,
          method: 'GET',
        };
      },
      // transformResponse: (res) => res.result,
    }),

    readVariantDetails: builder.query({
      query: (variantId) => {
        return {
          url: `/varient/${variantId}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllVarient'],
    }),
  }),
});

export const {
  useReadSubModelQuery,
  useDeleteSubModelMutation,
  useUpdateVariantMutation,
  useCreateSubModelMutation,
  useGetSubModelFromModelQuery,
  useReadVariantDetailsQuery,
} = subModel;
