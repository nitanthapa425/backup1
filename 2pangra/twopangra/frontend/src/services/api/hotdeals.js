import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const hotDeal = createApi({
  reducerPath: 'hotDeal',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readAllHotDeals'],

  endpoints: (builder) => ({
    readAllHotDeals: builder.query({
      query: (query) => {
        return {
          // url: `/seller/getAdminHotDeals?search="isApproved":"true","hasInHotDeal":"true"/${query}`,
          url: `/seller/getAdminHotDeals${query}`,

          method: 'GET',
        };
      },
      providesTags: ['readAllHotDeals'],
    }),

    removeHotDeal: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/removeHotDeals/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: 'POST',
        };
      },
      invalidatesTags: ['readAllHotDeals'],
    }),
  }),
});

export const { useReadAllHotDealsQuery, useRemoveHotDealMutation } = hotDeal;
