import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const vehicle = createApi({
  reducerPath: 'vehicle',
  keepUnusedDataFor: 0,
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['AllVehicles', 'AllVehicleDetails', 'Vehicle'],
  endpoints: (builder) => ({
    readVehicle: builder.query({
      query: () => {
        return {
          url: `/vehicleDetail`,
          method: 'GET',
        };
      },
      providesTags: ['AllVehicles'],
    }),

    readVehicleCustom: builder.query({
      query: (customQuery) => {
        return {
          url: `/vehicleDetail${customQuery}`,
          method: 'GET',
        };
      },
      providesTags: ['AllVehicles'],
    }),

    deleteVehicle: builder.mutation({
      query: (id) => {
        return {
          url: `/vehicleDetail/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['AllVehicles'],
    }),

    updateVehicle: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/vehicleDetail/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: (result, err, arg) => {
        return [{ type: 'Vehicle', id: arg.id }];
      },
    }),

    createVehicle: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleDetail`,
          method: 'POST',
          body: data,
        };
      },

      invalidatesTags: ['AllVehicleDetails'],
    }),
    saveDraftToSource: builder.mutation({
      query: (data) => {
        const { id, ...rest } = data;
        return {
          url: `/saveDraftToSource/${id}`,
          method: 'POST',
          body: rest,
        };
      },

      invalidatesTags: ['AllVehicleDetails'],
    }),

    readVehicleDetails: builder.query({
      query: (id) => {
        return {
          url: `/vehicleDetail/${id}`,
          method: 'GET',
        };
      },
      providesTags: (result, err, arg) => [{ type: 'Vehicle', id: arg }],
    }),

    createInstrumentation: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/instrumentation`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readInstrumentation: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/instrumentation`,
          method: 'GET',
        };
      },
      providesTags: ['instrumentation'],
    }),
    createIgnition: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/ignition`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readIgnition: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/ignition`,
          method: 'GET',
        };
      },
      providesTags: ['Ignition'],
    }),

    createVehicleType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/vehicleType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readVehicleType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/vehicleType`,
          method: 'GET',
        };
      },
      providesTags: ['VehicleType'],
    }),
    createFuelType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/fuelType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFuelType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/fuelType`,
          method: 'GET',
        };
      },
      providesTags: ['fuelType'],
    }),

    createTransmissionType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/transmissionType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readTransmissionType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/transmissionType`,
          method: 'GET',
        };
      },
      providesTags: ['transmissionType'],
    }),

    createBodyType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/bodyType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readBodyType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/bodyType`,
          method: 'GET',
        };
      },
      providesTags: ['bodyType'],
    }),

    createSeatingCapacity: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/seatingCapacity`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readSeatingCapacity: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/seatingCapacity`,
          method: 'GET',
        };
      },
      providesTags: ['seatingCapacity'],
    }),

    createFuelCapacity: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/fuelCapacity`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFuelCapacity: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/fuelCapacity`,
          method: 'GET',
        };
      },
      providesTags: ['fuelCapacity'],
    }),

    createMileage: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/mileage`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readMileage: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/mileage`,
          method: 'GET',
        };
      },
      providesTags: ['mileage'],
    }),
    createRearBrakeSystem: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/backBrake`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readRearBrakeSystem: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/backBrake`,
          method: 'GET',
        };
      },
      providesTags: ['backBrake'],
    }),

    createFrontBrakeSystem: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/frontBrake`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFrontBrakeSystem: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/frontBrake`,
          method: 'GET',
        };
      },
      providesTags: ['frontBrake'],
    }),
    createFrontSuspension: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/frontSuspension`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFrontSuspension: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/frontSuspension`,
          method: 'GET',
        };
      },
      providesTags: ['frontSuspension'],
    }),

    createRearSuspension: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/rearSuspension`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readRearSuspension: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/rearSuspension`,
          method: 'GET',
        };
      },
      providesTags: ['rearSuspension'],
    }),

    createNoOfGear: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/noOfGears`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readNoOfGear: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/noOfGears`,
          method: 'GET',
        };
      },
      providesTags: ['noOfGear'],
    }),

    createDriveType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/driveType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readDriveType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/driveType`,
          method: 'GET',
        };
      },
      providesTags: ['driveType'],
    }),

    createClutchType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/clutchType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readClutchType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/clutchType`,
          method: 'GET',
        };
      },
      providesTags: ['clutchType'],
    }),

    createGearPattern: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/gearPattern`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readGearPattern: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/gearPattern`,
          method: 'GET',
        };
      },
      providesTags: ['gearPattern'],
    }),

    createHeadLight: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/headlight`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readHeadLight: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/headlight`,
          method: 'GET',
        };
      },
      providesTags: ['headlight'],
    }),

    createTailLight: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/taillight`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readTailLight: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/taillight`,
          method: 'GET',
        };
      },
      providesTags: ['taillight'],
    }),

    createStarter: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/starter`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readStarter: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/starter`,
          method: 'GET',
        };
      },
      providesTags: ['starter'],
    }),

    createBattery: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/battery`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readBattery: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/battery`,
          method: 'GET',
        };
      },
      providesTags: ['battery'],
    }),
    createFeature: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/feature`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFeature: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/feature`,
          method: 'GET',
        };
      },
      providesTags: ['feature'],
    }),
    createSafety: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/safety`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readSafety: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/safety`,
          method: 'GET',
        };
      },
      providesTags: ['safety'],
    }),

    createWheelBase: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/wheelBase`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readWheelBase: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/wheelBase`,
          method: 'GET',
        };
      },
      providesTags: ['wheelBase'],
    }),

    createOverallWidth: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/overallWidth`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readOverallWidth: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/overallWidth`,
          method: 'GET',
        };
      },
      providesTags: ['overallWidth'],
    }),

    createOverallLength: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/overallLength`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readOverallLength: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/overallLength`,
          method: 'GET',
        };
      },
      providesTags: ['overallLength'],
    }),

    createOverallHeight: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/overallHeight`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readOverallHeight: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/overallHeight`,
          method: 'GET',
        };
      },
      providesTags: ['overallHeight'],
    }),

    createGroundClearance: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/groundClearance`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readGroundClearance: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/groundClearance`,
          method: 'GET',
        };
      },
      providesTags: ['groundClearance'],
    }),

    createBootSpace: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/bootSpace`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readBootSpace: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/bootSpace`,
          method: 'GET',
        };
      },
      providesTags: ['bootSpace'],
    }),

    createKerbWeight: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/kerbWeight`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readKerbWeight: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/kerbWeight`,
          method: 'GET',
        };
      },
      providesTags: ['kerbWeight'],
    }),

    createDoddleHeight: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/doddleHeight`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readDoddleHeight: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/doddleHeight`,
          method: 'GET',
        };
      },
      providesTags: ['doddleHeight'],
    }),
    createDisplacement: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/displacement`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readDisplacement: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/displacement`,
          method: 'GET',
        };
      },
      providesTags: ['displacement'],
    }),

    // createFuelEfficiency: builder.mutation({
    //   query: (data) => {
    //     return {
    //       url: `/vehicleAutoValue/fuelEfficiency`,
    //       method: 'POST',
    //       body: data,
    //     };
    //   },
    //   invalidatesTags: ['AllVehicleDetails'],
    // }),

    // readFuelEfficiency: builder.query({
    //   query: () => {
    //     return {
    //       url: `/vehicleAutoValue/fuelEfficiency`,
    //       method: 'GET',
    //     };
    //   },
    //   providesTags: ['fuelEfficiency'],
    // }),

    createEngineType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/engineType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readEngineType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/engineType`,
          method: 'GET',
        };
      },
      providesTags: ['engineType'],
    }),

    createNoOfCylinder: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/noOfCylinder`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readNoOfCylinder: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/noOfCylinder`,
          method: 'GET',
        };
      },
      providesTags: ['noOfCylinder'],
    }),

    createValvesPerCylinder: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/valvesPerCylinder`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readValvesPerCylinder: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/valvesPerCylinder`,
          method: 'GET',
        };
      },
      providesTags: ['valvesPerCylinder'],
    }),

    createValveConfiguration: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/valveConfiguration`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readValveConfiguration: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/valveConfiguration`,
          method: 'GET',
        };
      },
      providesTags: ['valveConfiguration'],
    }),

    createFuelSupplySystem: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/fuelSupplySystem`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFuelSupplySystem: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/fuelSupplySystem`,
          method: 'GET',
        };
      },
      providesTags: ['fuelSupplySystem'],
    }),

    createMaximumPower: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/maximumPower`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readMaximumPower: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/maximumPower`,
          method: 'GET',
        };
      },
      providesTags: ['maximumPower'],
    }),

    createMaximumTorque: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/maximumTorque`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readMaximumTorque: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/maximumTorque`,
          method: 'GET',
        };
      },
      providesTags: ['maximumTorque'],
    }),

    createLubrication: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/lubrication`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readLubrication: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/lubrication`,
          method: 'GET',
        };
      },
      providesTags: ['lubrication'],
    }),

    createEngineOil: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/engineOil`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readEngineOil: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/engineOil`,
          method: 'GET',
        };
      },
      providesTags: ['engineOil'],
    }),

    createAirCleaner: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/airCleaner`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readAirCleaner: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/airCleaner`,
          method: 'GET',
        };
      },
      providesTags: ['airCleaner'],
    }),

    createBoreXStroke: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/boreXStroke`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    createMotorPower: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/motorPower`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readBoreXStroke: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/boreXStroke`,
          method: 'GET',
        };
      },
      providesTags: ['boreXStroke'],
    }),

    createCompressionRatio: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/compressionRatio`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readCompressionRatio: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/compressionRatio`,
          method: 'GET',
        };
      },
      providesTags: ['compressionRatio'],
    }),

    createWheelType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/wheelType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readWheelType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/wheelType`,
          method: 'GET',
        };
      },
      providesTags: ['wheelType'],
    }),

    createFrontWheelSize: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/frontWheelSize`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFrontWheelSize: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/frontWheelSize`,
          method: 'GET',
        };
      },
      providesTags: ['frontWheelSize'],
    }),
    createRearWheelSize: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/rearWheelSize`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readRearWheelSize: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/rearWheelSize`,
          method: 'GET',
        };
      },
      providesTags: ['rearWheelSize'],
    }),
    createRearWheelType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/rearWheelType`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readRearWheelType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/rearWheelType`,
          method: 'GET',
        };
      },
      providesTags: ['rearWheelType'],
    }),

    createFrontWheelType: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/frontWheelType `,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readFrontWheelType: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/frontWheelType `,
          method: 'GET',
        };
      },
      providesTags: ['frontWheelType'],
    }),

    // createFrontWheelDrive: builder.mutation({
    //   query: (data) => {
    //     return {
    //       url: `/vehicleAutoValue/frontWheelDrive`,
    //       method: 'POST',
    //       body: data,
    //     };
    //   },
    //   invalidatesTags: ['AllVehicleDetails'],
    // }),

    // readFrontWheelDrive: builder.query({
    //   query: () => {
    //     return {
    //       url: `/vehicleAutoValue/frontWheelDrive`,
    //       method: 'GET',
    //     };
    //   },
    //   providesTags: ['frontWheelDrive'],
    // }),

    createAlloyWheel: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/alloyWheel`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readAlloyWheel: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/alloyWheel`,
          method: 'GET',
        };
      },
      providesTags: ['alloyWheel'],
    }),

    createSteelRims: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/steelRims`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readSteelRims: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/steelRims`,
          method: 'GET',
        };
      },
      providesTags: ['steelRims'],
    }),

    createOtherDetail: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/otherDetail`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readOtherDetail: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/otherDetail`,
          method: 'GET',
        };
      },
      providesTags: ['otherDetail'],
    }),

    createMarketPrice: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleAutoValue/marketPrice`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['AllVehicleDetails'],
    }),

    readMarketPrice: builder.query({
      query: () => {
        return {
          url: `/vehicleAutoValue/marketPrice`,
          method: 'GET',
        };
      },
      providesTags: ['marketPrice'],
    }),

    readAllVehicleDetails: builder.query({
      query: () => {
        return {
          url: `/getAllVehicleValue`,
          method: 'GET',
        };
      },
      providesTags: ['AllVehicleDetails'],
    }),
  }),
});

export const {
  useReadVehicleQuery,
  useReadVehicleCustomQuery,
  useDeleteVehicleMutation,
  useUpdateVehicleMutation,
  useCreateVehicleMutation,
  useReadVehicleDetailsQuery,
  useSaveDraftToSourceMutation,
  useCreateInstrumentationMutation,
  useReadInstrumentationQuery,
  useCreateIgnitionMutation,
  useReadIgnitionQuery,
  useCreateVehicleTypeMutation,
  useReadVehicleTypeQuery,
  useCreateFuelTypeMutation,
  useReadFuelTypeQuery,
  useCreateTransmissionTypeMutation,
  useReadTransmissionTypeQuery,
  useCreateBodyTypeMutation,
  useReadBodyTypeQuery,
  useCreateSeatingCapacityMutation,
  useReadSeatingCapacityQuery,
  useCreateFuelCapacityMutation,
  useReadFuelCapacityQuery,
  useCreateMileageMutation,
  useReadMileageQuery,
  useCreateRearBrakeSystemMutation,
  useReadRearBrakeSystemQuery,
  useCreateFrontBrakeSystemMutation,
  useReadFrontBrakeSystemQuery,
  useCreateFrontSuspensionMutation,
  useReadFrontSuspensionQuery,
  useCreateRearSuspensionMutation,
  useReadRearSuspensionQuery,
  useCreateNoOfGearMutation,
  useReadNoOfGearQuery,
  useCreateDriveTypeMutation,
  useReadDriveTypeQuery,
  useCreateClutchTypeMutation,
  useReadClutchTypeQuery,
  useCreateGearPatternMutation,
  useReadGearPatternQuery,
  useCreateHeadLightMutation,
  useReadHeadLightQuery,
  useCreateTailLightMutation,
  useReadTailLightQuery,
  useCreateStarterMutation,
  useReadStarterQuery,
  useCreateBatteryMutation,
  useReadBatteryQuery,
  useCreateSafetyMutation,
  useReadSafetyQuery,
  useCreateFeatureMutation,
  useReadFeatureQuery,
  useCreateWheelBaseMutation,
  useReadWheelBaseQuery,
  useCreateOverallWidthMutation,
  useReadOverallWidthQuery,
  useCreateOverallLengthMutation,
  useReadOverallLengthQuery,
  useCreateOverallHeightMutation,
  useReadOverallHeightQuery,
  useCreateGroundClearanceMutation,
  useReadGroundClearanceQuery,
  useCreateBootSpaceMutation,
  useReadBootSpaceQuery,
  useCreateKerbWeightMutation,
  useReadKerbWeightQuery,
  useCreateDoddleHeightMutation,
  useReadDoddleHeightQuery,
  useCreateWheelTypeMutation,
  useReadWheelTypeQuery,
  useCreateFrontWheelSizeMutation,
  useReadFrontWheelSizeQuery,
  useCreateRearWheelSizeMutation,
  useReadRearWheelSizeQuery,
  useCreateRearWheelTypeMutation,
  useReadRearWheelTypeQuery,
  useCreateFrontWheelTypeMutation,
  useReadFrontWheelTypeQuery,
  // useCreateFrontWheelDriveMutation,
  // useReadFrontWheelDriveQuery,
  useCreateAlloyWheelMutation,
  useReadAlloyWheelQuery,
  useCreateSteelRimsMutation,
  useReadSteelRimsQuery,
  useCreateDisplacementMutation,
  useReadDisplacementQuery,
  // useCreateFuelEfficiencyMutation,
  // useReadFuelEfficiencyQuery,
  useCreateEngineTypeMutation,
  useReadEngineTypeQuery,
  useCreateNoOfCylinderMutation,
  useReadNoOfCylinderQuery,
  useReadValvesPerCylinderQuery,
  useCreateValvesPerCylinderMutation,
  useCreateValveConfigurationMutation,
  useReadValveConfigurationQuery,
  useReadFuelSupplySystemQuery,
  useCreateFuelSupplySystemMutation,
  useReadMaximumPowerQuery,
  useCreateMaximumPowerMutation,
  useCreateMaximumTorqueMutation,
  useReadMaximumTorqueQuery,
  useCreateLubricationMutation,
  useReadLubricationQuery,
  useReadEngineOilQuery,
  useCreateEngineOilMutation,
  useReadAirCleanerQuery,
  useCreateAirCleanerMutation,
  useCreateBoreXStrokeMutation,
  useReadBoreXStrokeQuery,
  useReadCompressionRatioQuery,
  useCreateCompressionRatioMutation,
  useReadOtherDetailQuery,
  useCreateOtherDetailMutation,
  useReadMarketPriceQuery,
  useCreateMarketPriceMutation,
  useReadAllVehicleDetailsQuery,
  useCreateMotorPowerMutation,
} = vehicle;
