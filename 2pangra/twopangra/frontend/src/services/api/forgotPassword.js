import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const forgotPassword = createApi({
  reducerPath: 'forgotPassword',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    forgotPassword: builder.mutation({
      query: (data) => {
        return {
          url: `/forgotPassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { useForgotPasswordMutation } = forgotPassword;
