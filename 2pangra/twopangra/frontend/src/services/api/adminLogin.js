import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const adminLogin = createApi({
  reducerPath: 'adminLogin',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    loginAdmin: builder.mutation({
      query: (data) => {
        return {
          url: `/login`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { useLoginAdminMutation } = adminLogin;
