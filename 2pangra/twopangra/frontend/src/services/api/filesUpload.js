import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const filesUpload = createApi({
  reducerPath: 'filesUpload',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === 'superAdmin'
          ? getState().adminAuth.token
          : getState().levelReducer.level === 'customer'
          ? getState().customerAuth.token
          : '';
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    uploadFiles: builder.mutation({
      query: (data) => {
        return {
          url: `/uploadImages`,
          method: 'POST',
          body: data,
        };
      },
    }),
    deleteFile: builder.mutation({
      query: (fileUrl) => {
        return {
          url: `/deleteImage/${fileUrl}`,
          method: 'DELETE',
        };
      },
    }),
  }),
});

export const { useUploadFilesMutation, useDeleteFileMutation } = filesUpload;
