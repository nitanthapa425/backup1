import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const adminCart = createApi({
  reducerPath: 'adminCart',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),

  endpoints: (builder) => ({
    readAllWishList: builder.query({
      query: (query) => {
        return {
          url: `/seller/getWishlist/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllWishList'],
    }),

    readAllCart: builder.query({
      query: (query) => {
        return {
          url: `/seller/addToCart/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllCart'],
    }),

    readAllBook: builder.query({
      query: (query) => {
        return {
          url: `/seller/bookNow/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllBook'],
    }),

    deleteWishList: builder.mutation({
      query: (id) => {
        return {
          url: `seller/wishlistByAdmin/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllWishList'],
    }),

    deleteCart: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/cartByAdmin/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllCart'],
    }),

    deleteBookedList: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/bookedVehicleByAdmin/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllBook'],
    }),
  }),
});

export const {
  useReadAllWishListQuery,
  useReadAllCartQuery,
  useReadAllBookQuery,
  useDeleteWishListMutation,
  useDeleteCartMutation,
  useDeleteBookedListMutation,
} = adminCart;
