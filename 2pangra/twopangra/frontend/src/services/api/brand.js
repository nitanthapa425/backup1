import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const brand = createApi({
  reducerPath: 'brand',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readBrandDetail', 'readAllBrand', 'readAllBrands'],

  endpoints: (builder) => ({
    readBrand: builder.query({
      query: () => {
        return {
          url: `/brandVehicle?sortBy=createdAt&sortOrder=-1`,
          method: 'GET',
        };
      },
      providesTags: ['readAllBrands'],
    }),
    readBrandNoAuth: builder.query({
      query: () => {
        return {
          url: `/brandVehicleNoAuth`,
          method: 'GET',
        };
      },
      providesTags: ['readAllBrandsNoAuth'],
    }),

    deleteBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/brandVehicle/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: [
        'readBrandDetail',
        'readAllBrand',
        'readAllBrands',
        'readAllBrandsNoAuth',
      ],
    }),

    updateBrand: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/brandVehicle/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: [
        'readBrandDetail',
        'readAllBrand',
        'readAllBrands',
        'readAllBrandsNoAuth',
      ],
    }),

    createBrand: builder.mutation({
      query: (data) => {
        return {
          url: `/brandVehicle`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: [
        'readBrandDetail',
        'readAllBrand',
        'readAllBrands',
        'readAllBrandsNoAuth',
      ],
    }),

    readBrandDetails: builder.query({
      query: (BrandId) => {
        return {
          url: `/brandVehicle/${BrandId}`,
          method: 'GET',
        };
      },
      providesTags: ['readBrandDetail'],
    }),

    readBrandList: builder.query({
      query: (query) => {
        return {
          url: `/brandVehicle/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllBrand'],
    }),

    // Adding brand to homepage/featured brands
    addFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/addBrandInHomePage/${id}`,
          method: 'POST',
        };
      },
      invalidatesTags: ['readAllBrand'],
    }),

    readFeaturedBrandsNoAuth: builder.query({
      query: () => {
        return {
          url: `/homeBrandVehicleNoAuth`,
          method: 'GET',
        };
      },
    }),

    removeFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/removeBrandInHomePage/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: 'POST',
        };
      },
      invalidatesTags: ['readAllBrand'],
    }),

    readBrandNoAuthWithLimit: builder.query({
      query: (query) => {
        return {
          url: `/brandVehicleNoAuthLimit${query}`,
          method: 'GET',
        };
      },
    }),
  }),
});

export const {
  useReadBrandQuery,
  useReadBrandNoAuthQuery,
  useDeleteBrandMutation,
  useUpdateBrandMutation,
  useCreateBrandMutation,
  useReadBrandDetailsQuery,
  useReadBrandListQuery,
  useAddFeaturedBrandMutation,
  useReadFeaturedBrandsNoAuthQuery,
  useRemoveFeaturedBrandMutation,
  useReadBrandNoAuthWithLimitQuery,
} = brand;
