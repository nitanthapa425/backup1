import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';
export const customer = createApi({
  reducerPath: 'customer',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === 'superAdmin'
          ? getState().adminAuth.token
          : getState().levelReducer.level === 'customer'
          ? getState().customerAuth.token
          : '';
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readCustomerDetail', 'readAllCustomers'],
  endpoints: (builder) => ({
    customerLogin: builder.mutation({
      query: (data) => {
        return {
          url: `/client/clientLogin`,
          method: 'POST',
          body: data,
        };
      },
    }),
    customerSignUp: builder.mutation({
      query: (data) => {
        return {
          url: `/client`,
          method: 'POST',
          body: data,
        };
      },
    }),
    customerForgot: builder.mutation({
      query: (data) => {
        return {
          url: `/client/forgotClientPassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
    customerReset: builder.mutation({
      query: (data) => {
        return {
          url: `/client/resetClientPassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
    customerUpdate: builder.mutation({
      query: (data) => {
        return {
          url: `/client/changeClientPassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
    verifyPhone: builder.mutation({
      query: (data) => {
        return {
          url: `/client/verifyOTP`,
          method: 'POST',
          body: data,
        };
      },
    }),
    ResendOtp: builder.mutation({
      query: (data) => {
        return {
          url: `/client/sendOTP`,
          method: 'POST',
          body: data,
        };
      },
    }),
    readCustomer: builder.query({
      query: (id) => {
        return {
          url: `/client/${id}`,
          method: 'GET',
        };
      },
      keepUnusedDataFor: 0,
      providesTags: ['readCustomerDetail'],
    }),
    readAllCustomer: builder.query({
      query: (query) => {
        return {
          url: `/client/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllCustomers'],
    }),

    readAllDeletedCustomer: builder.query({
      query: (query) => {
        return {
          url: `/client/deletedClients/${query}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllCustomers'],
    }),
    readDeletedCustomer: builder.query({
      query: (id) => {
        return {
          url: `client/deletedClients/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readCustomerDetail'],
    }),

    deleteCustomer: builder.mutation({
      query: (id) => {
        return {
          url: `/client/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllCustomers', 'readCustomerDetail'],
    }),
    updateCustomer: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/client/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllCustomers', 'readCustomerDetail'],
    }),
    revertDeletedCustomer: builder.mutation({
      query: (id) => {
        return {
          url: `/client/revertClient/${id}`,
          method: 'POST',
        };
      },
      invalidatesTags: ['readAllCustomers', 'readCustomerDetail'],
    }),
    customerMyProfile: builder.query({
      query: () => {
        return {
          url: `/client/myProfile`,
          method: 'GET',
        };
      },
      providesTags: ['readCustomerDetail'],
    }),
    editCustomerMyProfile: builder.mutation({
      query: () => {
        return {
          url: `/client/myProfile`,
          method: 'PUT',
        };
      },
      invalidateTags: ['readCustomerDetail'],
    }),
    validateUser: builder.query({
      query: () => {
        return {
          url: `/validate-user`,
          method: 'GET',
          // this method check weather the given token is expired or not
        };
      },
    }),
  }),
});

export const {
  useCustomerSignUpMutation,
  useCustomerForgotMutation,
  useCustomerResetMutation,
  useCustomerUpdateMutation,
  useVerifyPhoneMutation,
  useResendOtpMutation,
  useCustomerLoginMutation,
  useUpdateCustomerMutation,
  useDeleteCustomerMutation,
  useRevertDeletedCustomerMutation,
  useReadCustomerQuery,
  useReadDeletedCustomerQuery,
  useReadAllCustomerQuery,
  useReadAllDeletedCustomerQuery,
  useCustomerMyProfileQuery,
  useEditCustomerMyProfileMutation,
  useValidateUserQuery,
} = customer;
