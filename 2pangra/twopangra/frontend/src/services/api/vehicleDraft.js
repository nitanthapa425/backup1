import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const vehicleDraft = createApi({
  reducerPath: 'vehicleDraft',
  keepUnusedDataFor: 0,
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readDraftedVehicles,readAllDraftVehicles'],
  endpoints: (builder) => ({
    vehicleSaveAsDraft: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleDraftDetail`,
          method: 'POST',
          body: data,
        };
      },
    }),
    getDraftedVehicles: builder.query({
      query: () => {
        return {
          url: `/getDraftByUserId`,
          method: 'GET',
        };
      },
      providesTags: ['draftedVehicles'],
    }),

    getDraftedVehicle: builder.query({
      query: (id) => {
        return {
          url: `/vehicleDraftDetail/${id}`,
          method: 'GET',
        };
      },
    }),
    deleteDraftVehicle: builder.mutation({
      query: (id) => {
        return {
          url: `/vehicleDraftDetail/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readDraftedVehicles,readAllDraftVehicles'],
    }),
    updateDraftVehicle: builder.mutation({
      query: ({ id, ...data }) => {
        return {
          url: `/vehicleDraftDetail/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readDraftedVehicles,readAllDraftVehicles'],
    }),
    readDraftVehicle: builder.query({
      query: (customQuery) => {
        return {
          url: `/vehicleDraftDetail/${customQuery}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllDraftVehicles'],
    }),
  }),
});

export const {
  useGetDraftedVehiclesQuery,
  useVehicleSaveAsDraftMutation,
  useGetDraftedVehicleQuery,
  useDeleteDraftVehicleMutation,
  useUpdateDraftVehicleMutation,
  useReadDraftVehicleQuery,
} = vehicleDraft;
