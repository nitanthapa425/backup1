// import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
// import { baseUrl } from 'config';

// export const featuredBike = createApi({
//   reducerPath: 'featuredBike',
//   baseQuery: fetchBaseQuery({
//     baseUrl,
//     prepareHeaders: (headers, { getState }) => {
//       const token = getState().adminAuth.token;
//       if (token) {
//         headers.set('authorization', `Bearer ${token}`);
//       }
//       return headers;
//     },
//     tagTypes: ['readSellerDetail', 'readAllSellers', 'readVehicleSell'],
//   }),

//   endpoints: (builder) => ({
//     addFeaturedBike: builder.mutation({
//       query: (id) => {
//         return {
//           url: `/seller/add-feature-vehicle/${id}`,
//           method: 'POST',
//         };
//       },
//       invalidatesTags: ['readAllSellers'],
//     }),
//     removeFeaturedBike: builder.mutation({
//       query: (id) => {
//         return {
//           url: `/seller/remove-feature-vehicle/${id}`,
//           // Method is still POST not DELETE. Made by our Backend Team.
//           method: 'POST',
//         };
//       },
//       invalidatesTags: ['readAllSellers'],
//     }),
//   }),
// });

// export const { useAddFeaturedBikeMutation, useRemoveFeaturedBikeMutation } =
//   featuredBike;
