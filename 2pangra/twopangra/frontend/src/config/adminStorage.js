export const getAdminToken = () => {
  return localStorage.getItem('adminTwoPangraToken');
};
export const setAdminToken = (token) => {
  if (token) {
    localStorage.setItem('adminTwoPangraToken', token);
  }
};
export const getUser = () => {
  return localStorage.getItem('adminTwoPangra');
};
export const setUser = (user) => {
  if (user) {
    localStorage.setItem('adminTwoPangra', JSON.stringify(user));
  }
};
export const removeAdminToken = () => {
  localStorage.removeItem('adminTwoPangraToken');
  localStorage.removeItem('adminTwoPangra');
};
