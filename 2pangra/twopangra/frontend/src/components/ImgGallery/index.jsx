import ImageGallery from 'react-image-gallery';
import PropTypes from 'prop-types';
import 'react-image-gallery/styles/css/image-gallery.css';

function ImgGallery({ images }) {
  return (
    <>
      {images.length === 0 ? (
        <img className="w-full " src="/images/placeholder.jpg"></img>
      ) : (
        <ImageGallery
          items={images}
          showThumbnails={true}
          lazyLoad={true}
          showPlayButton={false}
        />
      )}
    </>
  );
}

ImgGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ImgGallery;
