import { useState, useCallback } from 'react';
import PropTypes from 'prop-types';

const Alert = ({ type, message }) => {
  const [show, setShow] = useState(true);
  const hide = useCallback(() => setShow(false), [show]);

  const getClasses = () => {
    let classes = '';
    let iconClass = '';
    if (type === 'success') {
      classes += ' border-green-400 text-green-700 bg-green-100 ';
      iconClass = 'text-green-500';
    }
    if (type === 'error') {
      classes += ' border-primary-light text-primary-dark bg-red-100';
      iconClass = 'text-primary-dark';
    } else {
      classes += 'border-gray-400 text-black bg-white';
      iconClass = 'text-black';
    }

    return [classes, iconClass];
  };

  if (show)
    return (
      <div
        className={`flex gap-1 px-4 py-3 shadow rounded relative border  ${
          getClasses()[0]
        }`}
        role="alert"
      >
        <span className="block flex-wrap">{message}</span>
        <div
          className="absolute top-0 bottom-0 right-0 px-4 py-3"
          onClick={hide}
        >
          <svg
            className={`fill-current h-6 w-6 ${getClasses()[1]} `}
            role="button"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <title>Close</title>
            <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
          </svg>
        </div>
      </div>
    );
  else return <></>;
};

Alert.propTypes = {
  type: PropTypes.oneOf(['error', 'success']),
  message: PropTypes.string.isRequired,
};

export default Alert;
