// import Accordion from 'components/Accordion';
import React from 'react';
import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel,
} from 'react-accessible-accordion';

const ReactAccordions = ({ AccordionItems, preExp = [1], allowMulExp = false }) => {
  return (
    <Accordion allowZeroExpanded allowMultipleExpanded={allowMulExp} preExpanded={preExp} className="w-full">
      {AccordionItems.map((item) => (
        <AccordionItem uuid={item.uuid} key={item.uuid}>
          <AccordionItemHeading>
            <AccordionItemButton>{item.heading}</AccordionItemButton>
          </AccordionItemHeading>
          <AccordionItemPanel>{item.content}</AccordionItemPanel>
        </AccordionItem>
      ))}
    </Accordion>
  );
};

export default ReactAccordions;
