import { useState } from 'react';
import PropTypes from 'prop-types';
import { Collapse } from 'react-collapse';

function Accordion({ heading, data }) {
  const [open, setOpen] = useState(false);
  return (
    <>
      <div
        className="flex justify-between cursor-pointer pb-2"
        onClick={() => setOpen(!open)}
      >
        <p className="font-bold text-base">{heading}</p>
        {open ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
            stroke="gainsboro"
          >
            <path
              fillRule="evenodd"
              d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
              clipRule="evenodd"
            />
          </svg>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
            stroke="gainsboro"
          >
            <path
              fillRule="evenodd"
              d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        )}
      </div>
      <Collapse isOpened={open}>
        <div className="pb-2 pl-4">
          {data.map((item, index) => (
            <p className="text-sm" key={index}>
              <span className="font-bold">{item.label}:&emsp;</span>
              {
                /* If Data is undefined, then return a - */ !item.data
                  ? '-'
                  : item.data
              }
            </p>
          ))}
        </div>
      </Collapse>
    </>
  );
}

Accordion.propTypes = {
  heading: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Accordion;
