import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import EmailNewsLetter from 'components/NewsLetter';
import ReactModal from 'components/ReactModal/ReactModal';
import CustomerLoginPopUp from 'components/LoginPopUp';
import { useState } from 'react';
function Footer() {
  const loginInfo = useSelector((state) => state.customerAuth);
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const handleSellBike = (route, token) => {
    if (token) {
      router.push(route);
    } else {
      setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  const token = loginInfo?.token;
  return (
    <footer id="footer" className="rounded-t-[25px] overflow-hidden">
      {openModal ? (
        <ReactModal
          directlyOpen={true}
          closeFunc={handleCloseModal}
          modal={modal}
        >
          <CustomerLoginPopUp
            setModalFunction={setModalFunction}
            modal={modal}
          ></CustomerLoginPopUp>
        </ReactModal>
      ) : null}
      <div className="footer-top py-10 bg-gray-200 ">
        <div className="container">
          <div className="row">
            <div className=" px-4  w-[100%] md:w-[35%] ">
              <Link href="/">
                <a className="mb-2">
                  <img
                    src="images/logo.svg"
                    alt="2 Pangra"
                    className="w-[180px]"
                  />
                </a>
              </Link>
              <p className="text-sm">
                Nepals finest two-wheeler buying and selling experience with
                trust, selection, and best quality. Time Efficient Hassle Free
                Easy to buy/sell and exchange User Friendly Interface Well
                trained technician for any problems or issues Trustworthy
              </p>
              <ul className="flex space-x-6 text-[24px]">
                <li>
                  <a
                    href="https://business.facebook.com/2Pangra/"
                    className="text-textColor hover:text-primary-dark"
                  >
                    {/* <img src="images/facebook.svg" alt="Facebook" /> */}
                    <span className="icon-facebook"></span>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/2pangra/"
                    className="text-textColor hover:text-primary-dark"
                  >
                    {/* <img src="images/insta.svg" alt="Instagram" /> */}

                    <span className="icon-instagram"></span>
                  </a>
                </li>
                <li>
                  <a
                    href="https://np.linkedin.com/company/2pangra-pvt-ltd?trk=public_profile_topcard-current-company"
                    className="text-textColor hover:text-primary-dark"
                  >
                    <span className="icon-linkedin"></span>
                  </a>
                </li>
              </ul>
            </div>
            <div className=" px-4 w-[50%] md:w-[17%] ">
              <ul className="space-y-[8px] text-sm">
                <li className="font-semibold text-[16px]">Quick Links</li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    About Us
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Blog
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Career
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Terms & Condition
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Privacy Policy
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Sitemap
                  </a>
                </li>
              </ul>
            </div>
            <div className=" px-4 w-[50%] md:w-[17%] ">
              <ul className="space-y-[8px] text-sm">
                <li className="font-semibold text-[16px]">Customer Care</li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Help Center
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Contact Us
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    How To Buy
                  </a>
                </li>
                <li>
                  <a
                    href="#"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Return & Refunds
                  </a>
                </li>
                <li>
                  <Link href="#">
                    <a
                      className="text-textColor hover:text-primary-dark cursor-pointer"
                      onClick={() => {
                        handleSellBike('/sell', token);
                      }}
                    >
                      Sell Bike
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className=" px-4 mt-6 w-[100%] md:w-[31%]  md:mt-0">
              <h2 className="h5 leading-none mb-4">
                Enter Your Email For Latest Offers
              </h2>
              <EmailNewsLetter />
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom py-10">
        <div className="container">
          <div className="trending mb-4 pb-5 border-b-[1px] border-gray-200 ">
            <h3 className="h5"> Trending In Dui Pangra </h3>
            <div className="space-y-2">
              <a href="#">Honda</a>
              <a href="#">Tvs</a>
              <a href="#">Yamaha</a>
              <a href="#">Hero</a>
              <a href="#">Suzuki</a>
              <a href="#">Duke</a>
            </div>
          </div>
          <div className="must-searched mb-4 pb-5 border-b-[1px] border-gray-200 ">
            <h3 className="h5"> must Searched </h3>
            <div className="space-y-2">
              <a href="#">Honda</a>
              <a href="#">Tvs</a>
              <a href="#">Yamaha</a>
              <a href="#">Hero</a>
              <a href="#">Suzuki</a>
              <a href="#">Duke</a>
            </div>
          </div>
          <div className="pb-5">
            <h3 className="h5">
              Experience Hassle-Free Online Shopping in Nepal.
            </h3>
            <p className="text-sm">
              Nepals finest two-wheeler buying and selling experience with
              trust, selection, and best quality. Time Efficient Hassle Free
              Easy to buy/sell and exchange User Friendly Interface Well trained
              technician for any problems or issues Trustworthy
            </p>
            <p className="text-sm">
              DuiPangra. Nepali two-wheeler e-commerce portal. Buy and sell 2nd
              hand motorbikes + branded bike gear & accessories. Time Efficient
              Hassle Free Easy to buy/sell and exchange User Friendly Interface
              Well trained technician for any problems or issues Trustworthy
            </p>
          </div>
          <div className="Top Brands">
            <h3 className="h5 mb-2"> Top Brands </h3>
            <div className=" mb-3 pb-3 border-b-[1px] border-gray-200 pl-2">
              <h4 className="h5 ">Bikes</h4>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Bajaj : </h6>
                <a href="#">Pulsar-150</a>
                <a href="#">Pulsar-220</a>
                <a href="#">Ns-200</a>
                <a href="#">Pulsar NS 125</a>
                <a href="#">Bajaj Pulsar NS160</a>
              </div>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Yamaha : </h6>
                <a href="#">R15</a>
                <a href="#">MT 15 </a>
                <a href="#">FZS 25</a>
                <a href="#">FZ 25</a>
                <a href="#">FZ-X</a>
                <a href="#">FZ-FI </a>
              </div>
            </div>
            <div className="pl-2">
              <h4 className="h5 ">Scooters</h4>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Bajaj : </h6>
                <a href="#">Bajaj Chetak</a>
                <a href="#">Bajaj Chetak</a>
                <a href="#">Bajaj Chetak</a>
                <a href="#">Bajaj Chetak</a>
                <a href="#">Bajaj Chetak</a>
                <a href="#">Bajaj Chetak</a>
              </div>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Yamaha : </h6>
                <a href="#"> NMax 155</a>
                <a href="#">Ray ZR 125 FI</a>
                <a href="#">Fascino 125</a>
                <a href="#">Ray ZR 110 Street Rally</a>
                <a href="#">Fascino 110</a>
                <a href="#">Ray Z</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
