import { useField } from 'formik';

const Unit = ({ label, onChange, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div className="unit-converter-dropdown">
      <select
        {...field}
        {...props}
        onChange={onChange}
        className={
          meta.touched && meta.error
            ? 'border-primary-dark placeholder-primary-dark'
            : ''
        }
      />
      {meta.touched && meta.error ? (
        <div className="text-primary-dark">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Unit;
