import { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

function UnitDropdown({ defaultUnit, restUnits, dispatchFunc, globalState }) {
  // const { fuelCapacityUnit } = useSelector(state => state.addVehicleUnits);
  const [selectedUnit, setSelectedUnit] = useState(defaultUnit);

  // console.log('global/////////////', globalState);
  const dispatch = useDispatch();
  // Using useRef to control running some useEffects' inside block only when dependencies change and not on component mount.
  const didMount = useRef(false);

  // This useEffect runs only at first
  useEffect(() => {
    // If the desired state in the rtk slice exists then we set our local state.
    if (globalState.length) {
      setSelectedUnit(globalState);
    } else {
      // If the desired state in the rtk slice doesn't exist then we dispatch our local state which would be the defaultUnit from our props.
      dispatch(dispatchFunc(selectedUnit));
    }
  }, []);

  // Technically, the whole useEffect func runs at first and also when our dependency changes.
  // But the block inside didMount.current condition will run only when dependency changes. And the block inside the else statement has very minimal code so it won't make a difference even if it runs multiple times.
  // We have to control useEffects as much as possible because this component might be reused a lot.
  useEffect(() => {
    if (didMount.current) {
      // console.log('I run only when dependencies change and not on component mount');
      // If we have selected a unit from the dropdown, we will call handleChange which will set our local state. Then, this useEffect will get called which will dispatch our local state to rtk slice.
      dispatch(dispatchFunc(selectedUnit));
    } else {
      didMount.current = true;
    }
  }, [selectedUnit]);

  const handleChange = (e) => {
    setSelectedUnit(e.target.value);
  };

  return (
    <div className="unit-converter-dropdown">
      <select
        name="unit"
        id="unit"
        value={selectedUnit}
        onChange={handleChange}
      >
        <option value={defaultUnit}>{defaultUnit}</option>
        {restUnits.map((u, index) => (
          <option value={u} key={index}>
            {u}
          </option>
        ))}
      </select>
    </div>
  );
}

UnitDropdown.propTypes = {
  defaultUnit: PropTypes.string.isRequired,
  restUnits: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default UnitDropdown;
