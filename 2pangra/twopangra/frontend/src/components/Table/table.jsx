import Link from 'next/link';
import { useState, useEffect, useRef, forwardRef } from 'react';
import {
  useTable,
  usePagination,
  useSortBy,
  useFilters,
  useRowSelect,
} from 'react-table';
import PropTypes from 'prop-types';
import { useToasts } from 'react-toast-notifications';
import Button from 'components/Button';
import Pagination from 'components/Pagination';
import Popup from 'components/Popup';
import DropdownBtn from 'components/DropdownBtn';
import { DefaultColumnFilter } from './Filter';
import useDebounce from 'hooks/useDebounce';
import { Export } from './Export';
import { useSelector } from 'react-redux';
import { CheckCircleIcon, XCircleIcon } from '@heroicons/react/outline';
// import ReactModal from 'components/ReactModal/ReactModal';
// import MeetingSchedule from 'container/MeetingSchedual/MeetingSchedual';

const IndeterminateCheckbox = forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = useRef();
  const resolvedRef = ref || defaultRef;

  useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate;
  }, [resolvedRef, indeterminate]);

  return (
    <>
      <input type="checkbox" ref={resolvedRef} {...rest} />
    </>
  );
});

IndeterminateCheckbox.displayName = 'IndeterminateCheckbox';

function Table({
  tableDataAll = [],
  setSkipTableDataAll,
  isLoadingAll,
  columns,
  data,
  fetchData,
  isFetchError,
  isLoadingData,
  pageCount: controlledPageCount,
  defaultPageSize,
  totalData,
  rowOptions,
  viewRoute,
  editRoute,
  deleteQuery,
  isDeleting,
  isDeleteError,
  DeletedError,
  isDeleteSuccess,
  hasExport,
  addPage,
  // tableName = 'item/s',
  tableName = '',
  activeDeleteItems,
  isActivating,
  isActivatedError,
  isActivatedSuccess,
  activatedError,
  revertButtonName,
  revertMultipleButtonName,
  hasDropdownBtn,
  hideMultiple = false,
  dropdownBtnName,
  dropdownBtnOptions,
  verifiedAction = [],
  approvedAction = [],
  soldAction = [],
  // showVerifiedAction={true}
  hiddenColumns = ['id', 'verify', 'approved', 'sold', 'meetingSchedule'],
  MeetingSchedule,
}) {
  // console.log('soldAction', soldAction);
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageCount,
    gotoPage,
    setPageSize,
    selectedFlatRows,
    state: { pageIndex, pageSize, sortBy, filters },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageIndex: 0,
        pageSize: defaultPageSize,
        hiddenColumns: hiddenColumns,
      },
      manualPagination: true,
      manualSortBy: true,
      manualFilters: true,
      autoResetSortBy: false,
      defaultColumn: { Filter: DefaultColumnFilter },
      pageCount: controlledPageCount,
    },
    useFilters,
    useSortBy,
    usePagination,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => {
        return [
          {
            id: 'selection',
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            ),
            Cell: ({ row }) => (
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            ),
          },
          ...columns,
        ];
      });
    },
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        ...columns,
        {
          id: 'verify',

          Header: 'Verify',

          Cell: ({ row }) => {
            return verifiedAction.map((value, i) => {
              return (
                <div key={i}>
                  {row.values.isVerified !== value.isVerifiedValue &&
                    value.isVerifiedValue === true && (
                      <span title="Click to verified">
                        <XCircleIcon
                          className="h-6 w-6 hover:text-error"
                          variant="tab"
                          onClick={() => {
                            value.onClick(row.values.id);
                          }}
                          disabled={
                            row.values.isVerified === value.isVerifiedValue
                          }
                          loading={value.isLoading}
                        ></XCircleIcon>
                      </span>
                    )}
                  {row.values.isVerified !== value.isVerifiedValue &&
                    value.isVerifiedValue === false && (
                      <span title="Click to unverified">
                        <CheckCircleIcon
                          className="h-6 w-6 hover:text-primary"
                          variant="tab"
                          onClick={() => {
                            value.onClick(row.values.id);
                          }}
                          disabled={
                            row.values.isVerified === value.isVerifiedValue
                          }
                          loading={value.isLoading}
                        ></CheckCircleIcon>
                      </span>
                    )}
                </div>
              );
            });
          },
        },
      ]);
    },

    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        ...columns,
        {
          id: 'sold',

          Header: 'Sold',

          Cell: ({ row }) => {
            return soldAction.map((value, i) => {
              return (
                <div key={i}>
                  {row.values.isSold !== value.isSoldValue &&
                    value.isSoldValue === true && (
                      <span title="Click to sold">
                        <XCircleIcon
                          className="h-6 w-6 hover:text-error"
                          variant="tab"
                          onClick={() => {
                            setOpenModalForButtonAction({
                              isOpen: true,
                              type: 'unsold',
                              action: value.onClick,
                              id: row.values.id,
                            });
                          }}
                          disabled={row.values.isSold === value.isSoldValue}
                          loading={value.isLoading}
                        ></XCircleIcon>
                      </span>
                    )}
                  {row.values.isSold !== value.isSoldValue &&
                    value.isSoldValue === false && (
                      <span title="Click to unsold">
                        <CheckCircleIcon
                          className="h-6 w-6 hover:text-primary"
                          variant="tab"
                          onClick={() => {
                            setOpenModalForButtonAction({
                              isOpen: true,
                              type: 'sold',
                              action: value.onClick,
                              id: row.values.id,
                            });
                          }}
                          disabled={row.values.isSold === value.isSoldValue}
                          loading={value.isLoading}
                        ></CheckCircleIcon>
                      </span>
                    )}
                </div>
              );
            });
          },
        },
      ]);
    },

    // (hooks) => {
    //   hooks.visibleColumns.push((columns) => [
    //     ...columns,
    //     {
    //       id: 'meetingSchedule',

    //       Header: 'Meeting Schedule',

    //       Cell: ({ row }) => {
    //         console.log('row.values', row.values);
    //         return (
    //           <div>
    //             <ReactModal name="Schedule">
    //               {<MeetingSchedule bookId={row.values.id}></MeetingSchedule>}
    //             </ReactModal>
    //           </div>
    //         );
    //       },
    //     },
    //   ]);
    // },

    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        ...columns,
        {
          id: 'approved',

          Header: 'Approve',

          Cell: ({ row }) => {
            return approvedAction.map((value, i) => {
              return (
                <div key={i}>
                  {row.values.isApproved !== value.isApprovedValue &&
                    value.isApprovedValue === true && (
                      <span title="Click to approved">
                        <XCircleIcon
                          className="h-6 w-6 hover:text-error"
                          variant="tab"
                          onClick={() => {
                            value.onClick(row.values.id);
                          }}
                          disabled={
                            row.values.isApproved === value.isApprovedValue
                          }
                          loading={value.isLoading}
                        ></XCircleIcon>
                      </span>
                    )}
                  {row.values.isApproved !== value.isApprovedValue &&
                    value.isApprovedValue === false && (
                      <span title="Click to unapproved">
                        <CheckCircleIcon
                          className="h-6 w-6 hover:text-primary"
                          variant="tab"
                          onClick={() => {
                            value.onClick(row.values.id);
                          }}
                          disabled={
                            row.values.isApproved === value.isApprovedValue
                          }
                          loading={value.isLoading}
                        ></CheckCircleIcon>
                      </span>
                    )}
                </div>
              );
            });
          },
        },
      ]);
    }
  );

  const [selectedItemId, setSelectedItemId] = useState('');
  const debounceFilter = useDebounce(filters, 1000);
  const [openModalForRowSelect, setOpenModalForRowSelect] = useState(false);
  const [openModalForDelete, setOpenModalForDelete] = useState(false);
  const [openModalForActive, setOpenModalForActive] = useState(false);
  const [openModalForButtonAction, setOpenModalForButtonAction] = useState({
    isOpen: false,
  });
  const rowSizeSelectEl = useRef(null);
  const { addToast } = useToasts();

  useEffect(() => {
    if (selectedFlatRows.length !== 0) {
      setSelectedItemId(selectedFlatRows[0].original.id);
    }
  }, [selectedFlatRows]);

  useEffect(() => {
    // sortBy looks like: [{id: 'vehicleName', desc: false}]
    // console.log(sortBy);
    // console.log(debounceFilter);
    fetchData({ pageIndex, pageSize, sortBy, filters: debounceFilter });
  }, [fetchData, pageIndex, pageSize, sortBy, debounceFilter]);

  useEffect(() => {
    if (isDeleteSuccess) {
      showSuccessToast(`Selected ${tableName} successfully deleted.`);
    }
    if (isDeleteError) {
      showFailureToast(
        DeletedError?.data?.message ||
          `An error occurred when deleting selected ${tableName}.`
      );
    }
    if (isFetchError) {
      showFailureToast('An error occurred when fetching data.');
    }
  }, [isDeleteSuccess, isDeleteError, isFetchError, DeletedError]);
  useEffect(() => {
    if (isActivatedSuccess) {
      showSuccessToast(`Selected ${tableName} successfully Activated.`);
    }
    if (isActivatedError) {
      showFailureToast(
        DeletedError?.data?.message ||
          `An error occurred when Activating selected ${tableName}.`
      );
    }
    if (isFetchError) {
      showFailureToast('An error occurred when fetching data.');
    }
  }, [isActivatedSuccess, isActivatedError, isFetchError, activatedError]);

  const handleDelete = () => {
    setOpenModalForDelete(true);
  };
  const handleActive = () => {
    setOpenModalForActive(true);
  };
  // const handleButtonAction = () => {
  //   openModalForButtonAction(true);
  // };

  // openModalForButtonAction;

  const handlePageChange = (pNo) => {
    // Page Index works like 0, 1, 2.
    gotoPage(pNo);
  };

  const handleSelectRowSize = (e) => {
    if (Number(e.target.value) === totalData) {
      setOpenModalForRowSelect(true);
    } else {
      setPageSize(Number(e.target.value));
    }
  };

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: 'success',
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: 'error',
    });
  };

  // For Row Highlighting when selected
  const isRowSelected = (id) => {
    if (selectedFlatRows.find((row) => row.original.id === id)) {
      return true;
    } else {
      return false;
    }
  };

  const loginInfo = useSelector((state) => state.adminAuth);
  const loginInfoCustomer = useSelector((state) => state.customerAuth.customer);

  // console.log(loginInfoCustomer.accessLevel);

  return (
    <div className="flex flex-col relative">
      <div className="flex justify-between items-center mb-3">
        {openModalForRowSelect ? (
          <Popup
            title="This might take some time. Do you want to get all data?"
            description=""
            onOkClick={() => {
              setPageSize(totalData);
              setOpenModalForRowSelect(false);
            }}
            onCancelClick={() => {
              setPageSize(defaultPageSize);
              // Also change the value of select element in row size to defaultPageSize
              rowSizeSelectEl.current.value = defaultPageSize;
              setOpenModalForRowSelect(false);
            }}
            okText="Yes"
            cancelText="Cancel"
          />
        ) : null}
        {openModalForDelete ? (
          <Popup
            title={
              selectedFlatRows.length > 1
                ? 'Are you sure you want to delete this records?'
                : 'Are you sure you want to delete this record?'
            }
            description={
              selectedFlatRows.length > 1
                ? 'Please note that,once deleted, this cannot be undone and the records will be permanently removed.'
                : 'Please note that,once deleted, this cannot be undone and the record will be permanently removed.'
            }
            onOkClick={() => {
              // For multiple delete.
              if (selectedFlatRows.length > 1) {
                selectedFlatRows.forEach((item) => {
                  deleteQuery(item.original.id);
                });
              } else {
                deleteQuery(selectedFlatRows[0].original.id);
              }
              setOpenModalForDelete(false);
            }}
            onCancelClick={() => {
              setOpenModalForDelete(false);
            }}
            okText="Yes"
            cancelText="Cancel"
          />
        ) : null}
        {openModalForActive ? (
          <Popup
            title="Are you sure you want to Activate?"
            description="Please note that,
    once activated, this cannot be undone."
            onOkClick={() => {
              // For multiple delete.
              if (selectedFlatRows.length > 1) {
                selectedFlatRows.forEach((item) => {
                  activeDeleteItems(item.original.id);
                });
              } else {
                activeDeleteItems(selectedFlatRows[0].original.id);
              }
              setOpenModalForActive(false);
            }}
            onCancelClick={() => {
              setOpenModalForActive(false);
            }}
            okText="Yes"
            cancelText="Cancel"
          />
        ) : null}
        {openModalForButtonAction.isOpen ? (
          <Popup
            title={
              openModalForButtonAction.type === 'sold'
                ? 'Are you sure you want to mark as unsold?'
                : openModalForButtonAction.type === 'unsold'
                ? 'Are you sure you want to mark as sold?'
                : null
            }
            description=""
            onOkClick={() => {
              openModalForButtonAction.action({
                vehicleSellId: openModalForButtonAction.id,
              });
              setOpenModalForButtonAction({ isOpen: false });
            }}
            onCancelClick={() => {
              setOpenModalForButtonAction({ isOpen: false });
            }}
            okText="Yes"
            cancelText="Cancel"
          />
        ) : null}
        <span>
          <span className="bg-gray-500 text-white px-2 py-0 rounded-md">
            {selectedFlatRows.length}
          </span>{' '}
          Items selected
        </span>
        <div className="flex items-center table-right-action-buttons">
          {addPage && selectedFlatRows.length === 0 && (
            <Link href={`${addPage?.route}`}>
              <a className="mr-5 text-gray-500 hover:text-primary-light">
                {addPage?.page}
              </a>
            </Link>
          )}
          {selectedFlatRows.length === 1 ? (
            <>
              {viewRoute && loginInfo?.user?.accessLevel !== 'USER' ? (
                <Link href={`/${viewRoute}/${selectedItemId}`}>
                  <a className="mr-5 text-gray-500 hover:text-primary-light">
                    View Detail
                  </a>
                </Link>
              ) : null}
              {editRoute && loginInfo?.user?.accessLevel !== 'USER' ? (
                <Link href={`/${editRoute}/${selectedItemId}`}>
                  <a className="mr-5 text-gray-500 hover:text-primary-light">
                    Edit
                  </a>
                </Link>
              ) : null}
              {deleteQuery && loginInfo?.user?.accessLevel === 'SUPER_ADMIN' ? (
                <Button
                  variant="error"
                  loading={isDeleting}
                  onClick={handleDelete}
                >
                  Delete
                </Button>
              ) : null}
              {deleteQuery && loginInfoCustomer?.accessLevel === 'CLIENT' ? (
                <Button
                  variant="error"
                  loading={isDeleting}
                  onClick={handleDelete}
                >
                  Delete
                </Button>
              ) : null}
              {activeDeleteItems &&
              loginInfo?.user?.accessLevel === 'SUPER_ADMIN' ? (
                <Button
                  variant="error"
                  loading={isActivating}
                  onClick={handleActive}
                >
                  {revertButtonName}
                </Button>
              ) : null}
            </>
          ) : selectedFlatRows.length > 1 ? (
            <>
              {deleteQuery && loginInfo?.user?.accessLevel === 'SUPER_ADMIN' ? (
                <Button
                  variant="error"
                  loading={isDeleting}
                  onClick={handleDelete}
                >
                  Delete Multiple
                </Button>
              ) : null}
              {deleteQuery && loginInfoCustomer?.accessLevel === 'CLIENT' ? (
                <Button
                  variant="error"
                  loading={isDeleting}
                  onClick={handleDelete}
                >
                  Delete Multiple
                </Button>
              ) : null}
              {activeDeleteItems &&
              loginInfo?.user?.accessLevel === 'SUPER_ADMIN' ? (
                <Button
                  variant="error"
                  loading={isActivating}
                  onClick={handleActive}
                >
                  {revertMultipleButtonName}
                </Button>
              ) : null}
            </>
          ) : null}

          {hasDropdownBtn && selectedFlatRows.length > 0 && dropdownBtnName ? (
            hideMultiple && selectedFlatRows.length > 1 ? null : (
              <DropdownBtn
                name={dropdownBtnName}
                options={dropdownBtnOptions}
                selectedRowsIds={selectedFlatRows.map(
                  (row) => row?.original?.id
                )}
              />
            )
          ) : null}

          {hasExport && selectedFlatRows.length === 0 ? (
            <Export
              setSkipTableDataAll={setSkipTableDataAll}
              isLoadingAll={isLoadingAll}
              headers={columns}
              data={data}
              tableDataAll={tableDataAll}
              pdfFileName={tableName || 'data_pdf'}
              csvFileName={tableName || 'data_csv'}
              className={isLoadingData ? 'pointer-events-none' : ''}
            />
          ) : null}
          {/* {hasExport && selectedFlatRows.length === 0 ? (
            <Export
              headers={columns}
              data={tableDataAll}
              pdfFileName="data_pdf"
              csvFileName="data_csv"
              className={isLoadingData ? 'pointer-events-none' : ''}
            />
          ) : null} */}
        </div>
      </div>
      <div className="shadow overflow-auto w-full border-b border-gray-200 sm:rounded-lg h-[calc(100vh-165px)] min-h-[400px]">
        <div>
          <div
            className={
              isLoadingData
                ? ' w-full bg-white py-3 opacity-90 flex flex-col justify-center items-center '
                : 'hidden'
            }
          >
            <div className="flex flex-wrap justify-center absolute top-1/2 translate-y-[-50%]">
              <div className="w-full flex justify-center">
                <svg
                  className={`animate-spin h-10 w-10 text-primary`}
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                >
                  <circle
                    className="opacity-25"
                    cx="12"
                    cy="12"
                    r="10"
                    stroke="currentColor"
                    strokeWidth="4"
                  ></circle>
                  <path
                    className="opacity-75"
                    fill="currentColor"
                    d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                  ></path>
                </svg>
              </div>
              <div className="text-center pt-2 text-textColor">
                <h4 className="h4">Loading...</h4>
                <p className="mt-2">Please wait. Data is loading.</p>
              </div>
            </div>
          </div>
          <table
            {...getTableProps()}
            className="min-w-full divide-y divide-gray-200 relative"
          >
            <thead className="bg-gray-100">
              {headerGroups.map((headerGroup) => {
                const { key, ...restHeaderGroupProps } =
                  headerGroup.getHeaderGroupProps();
                return (
                  <tr key={key} {...restHeaderGroupProps}>
                    {headerGroup.headers.map((column) => {
                      const { key, ...restColumn } = column.getHeaderProps();
                      // We are removing the title attribute that byDefault shows 'Toggle SortBy' when we hover over the div with getSortByToggleProps.
                      const { title, ...togglePropsExceptTitle } =
                        column.getSortByToggleProps();
                      return (
                        <th
                          key={key}
                          {...restColumn}
                          scope="col"
                          className="px-4 py-0 text-left text-md font-medium text-gray-600"
                        >
                          {
                            // Only use getSortByToggleProps when column.canBeSorted is true.
                            // Also, sorting is automatically ascending at first and at second descending and at last no-sort then so on. This type of behaviour is given by react-table out of the box.
                            column.canBeSorted ? (
                              <div className="flex" {...togglePropsExceptTitle}>
                                <h6
                                  className="text-[15px] font-normal overflow-ellipsis overflow-hidden"
                                  title={column.Header}
                                >
                                  {column.render('Header')}&nbsp;
                                </h6>
                                <span>
                                  {column.isSortedDesc === undefined ? (
                                    // No Sort Icon
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-5 w-5"
                                      viewBox="0 0 20 20"
                                      fill="currentColor"
                                    >
                                      <path d="M5 12a1 1 0 102 0V6.414l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 001.414 1.414L5 6.414V12zM15 8a1 1 0 10-2 0v5.586l-1.293-1.293a1 1 0 00-1.414 1.414l3 3a1 1 0 001.414 0l3-3a1 1 0 00-1.414-1.414L15 13.586V8z" />
                                      <title>No Sort</title>
                                    </svg>
                                  ) : column.isSortedDesc ? (
                                    // Descending Icon
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-5 w-5"
                                      viewBox="0 0 20 20"
                                      fill="currentColor"
                                    >
                                      <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h7a1 1 0 100-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM15 8a1 1 0 10-2 0v5.586l-1.293-1.293a1 1 0 00-1.414 1.414l3 3a1 1 0 001.414 0l3-3a1 1 0 00-1.414-1.414L15 13.586V8z" />
                                      <title>Sorted Descending</title>
                                    </svg>
                                  ) : (
                                    // Ascending Icon
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      className="h-5 w-5"
                                      viewBox="0 0 20 20"
                                      fill="currentColor"
                                    >
                                      <path d="M3 3a1 1 0 000 2h11a1 1 0 100-2H3zM3 7a1 1 0 000 2h5a1 1 0 000-2H3zM3 11a1 1 0 100 2h4a1 1 0 100-2H3zM13 16a1 1 0 102 0v-5.586l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 101.414 1.414L13 10.414V16z" />
                                      <title>Sorted Ascending</title>
                                    </svg>
                                  )}
                                </span>
                              </div>
                            ) : (
                              <h6 className=" text-[15px] font-normal">
                                {column.render('Header')}
                              </h6>
                            )
                          }
                          {/* <div>{column.canFilter ? column.render('Filter') : null}</div> */}
                          <div>
                            {column.canBeFiltered
                              ? column.render('Filter')
                              : null}
                          </div>
                        </th>
                      );
                    })}
                  </tr>
                );
              })}
            </thead>
            <tbody {...getTableBodyProps()} className="">
              {isFetchError ? (
                <tr>
                  <td className="px-6 py-4 whitespace-nowrap">
                    Whoops, Error loading data
                  </td>
                </tr>
              ) : /* To show no data when loading instead of showing previous data. */
              isLoadingData ? (
                <tr></tr>
              ) : data && data.length ? (
                <>
                  {page.map((row) => {
                    prepareRow(row);
                    const { key, ...restRowProps } = row.getRowProps();
                    return (
                      <tr
                        key={key}
                        {...restRowProps}
                        className={
                          isRowSelected(row.original.id)
                            ? 'selected'
                            : 'not-selected'
                        }
                      >
                        {row.cells.map((cell) => {
                          const { key, ...restCellProps } = cell.getCellProps();
                          return (
                            <td
                              key={key}
                              {...restCellProps}
                              className="px-4 py-3 whitespace-nowrap text-[13px]"
                              title={cell.value}
                            >
                              {cell.render('Cell')}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })}
                </>
              ) : (
                <tr>
                  <td className="px-6 py-4 whitespace-nowrap">
                    Sorry, no data found.
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
      <div className="mt-5 flex flex-wrap justify-between items-center">
        <div className="flex text-sm mb-2 lg:mb-0">
          <p className="mr-3">
            <span className="font-bold">Showing data:</span> {page.length} out
            of {totalData}
          </p>
          <p className="mr-3">
            <span className="font-bold">Total Pages:</span> {pageCount}
          </p>
        </div>
        <div className="flex flex-wrap text-sm items-center">
          <div className="flex mx-3 items-center w-[185px]">
            <label htmlFor="rowSize" className="w-[180px]">
              Row Size
            </label>
            <select
              name="rowSize"
              id="rowSize"
              onChange={handleSelectRowSize}
              ref={rowSizeSelectEl}
              className={isLoadingData ? 'pointer-events-none' : ''}
            >
              {rowOptions
                .filter((value) => value <= totalData)
                .map((pSize, i) => (
                  <option key={i} value={pSize}>
                    {pSize === totalData ? 'All' : pSize}
                  </option>
                ))}
            </select>
          </div>
          {/* <p className="pr-3">
            <span className="font-bold">Page No: </span>
            {pageIndex + 1}
          </p> */}
          <div className="custom-pagination">
            <Pagination
              pageCount={pageCount}
              pageIndex={pageIndex}
              handlePageChange={handlePageChange}
              isLoading={isLoadingData}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

Table.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetchData: PropTypes.func.isRequired,
  isFetchError: PropTypes.bool.isRequired,
  isLoadingData: PropTypes.bool.isRequired,
  pageCount: PropTypes.number.isRequired,
  defaultPageSize: PropTypes.number.isRequired,
  totalData: PropTypes.number.isRequired,
  rowOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
  viewRoute: PropTypes.string,
  editRoute: PropTypes.string,
  deleteQuery: PropTypes.func,
  isDeleting: PropTypes.bool,
  isDeleteError: PropTypes.bool,
  isDeleteSuccess: PropTypes.bool,
  activeDeleteItems: PropTypes.func,
  revertButtonName: PropTypes.string,
  revertMultipleButtonName: PropTypes.string,

  isActivating: PropTypes.bool,
  isActivatedError: PropTypes.bool,
  isActivatedSuccess: PropTypes.bool,
  hasExport: PropTypes.bool,
  addPage: PropTypes.shape({
    page: PropTypes.string.isRequired,
    route: PropTypes.string.isRequired,
  }),
};

export default Table;
