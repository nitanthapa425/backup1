import { CSVLink } from 'react-csv';

export const GenerateCSV = ({
  data,
  headers,
  fileName = 'file',
  className,
}) => {
  // data format of
  // const data = [{ firstname: 'Ahmed', lastname: 'Tomi' },{ firstname: 'Ahmed2', lastname: 'Tomi2' }];
  //   const headers = [ { label: 'First Name', key: 'firstname' },{ label: 'Last Name', key: 'lastname' }];
  return (
    <CSVLink
      data={data}
      headers={headers}
      filename={`${fileName} ${new Date(Date.now()).toLocaleDateString()}.csv`}
      className={`${className} text-black hover:text-primary`}
    >
      Export to CSV
    </CSVLink>
  );
};

// export const GenerateAllCSV = ({
//   data,
//   headers,
//   fileName = 'file',
//   className,
// }) => {
//   // data format of
//   // const data = [{ firstname: 'Ahmed', lastname: 'Tomi' },{ firstname: 'Ahmed2', lastname: 'Tomi2' }];
//   //   const headers = [ { label: 'First Name', key: 'firstname' },{ label: 'Last Name', key: 'lastname' }];
//   return (
//     <CSVLink
//       data={data}
//       headers={headers}
//       filename={`${fileName} ${new Date(Date.now()).toLocaleDateString()}.csv`}
//       className={`${className} text-black hover:text-primary`}
//     >
//       All Data to CSV
//     </CSVLink>
//   );
// };
