import {
  BellIcon,
  CheckIcon,
  TrashIcon,
  XIcon,
} from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import {
  useReadNotificationQuery,
  useReadParticularNotificationMutation,
  useUnreadParticularNotificationMutation,
  useDeleteParticularNotificationMutation,
  useReadAllNotificationMutation,
} from 'services/api/notification';

import LoadingCustom from 'components/LoadingCustom/LoadingCustom';
import { agodate } from 'utils/datedifference';

const Notification = ({ type = 'customer' }) => {
  const loginInfoCustomer = useSelector((state) => state.customerAuth);
  const loginInfoAdmin = useSelector((state) => state.adminAuth);
  const [myKey, setMyKey] = useState(null);
  const userId =
    type === 'customer'
      ? loginInfoCustomer?.customer?._id
      : type === 'admin'
      ? loginInfoAdmin?.user?._id
      : '';
  // const userId = loginInfo.user?._id;
  const { addToast } = useToasts();
  const router = useRouter();

  const { data: dataNotification } = useReadNotificationQuery(
    `?search="createdBy":"${userId}"&sortBy=createdAt&sortOrder=-1`,
    {
      // pollingInterval: 3000,
    }

    // `?search="createdBy":"${userId}&sortBy=createdAt&sortOrder=-1`
  );

  const totalUnReadNotificationFunctions = (totalNotification) => {
    if (totalNotification?.length) {
      const totalUnReadNotification = totalNotification.filter(
        (totalNotificationData, i) => {
          return totalNotificationData.isRead === false;
        }
      );
      return totalUnReadNotification.length;
    }
    return 0;
  };

  const [
    readParticularNotification,
    {
      isError: isErrorReadParticularNotification,
      error: errorReadParticularNotification,
      isSuccess: isSuccessReadParticularNotification,
      isLoading: isLoadingReadParticularNotification,
    },
  ] = useReadParticularNotificationMutation();

  useEffect(() => {
    if (isErrorReadParticularNotification) {
      addToast(errorReadParticularNotification?.data?.message, {
        appearance: 'error',
      });
    }
    if (isSuccessReadParticularNotification) {
      // router.push(level'/seller/view');
    }
  }, [isErrorReadParticularNotification, isSuccessReadParticularNotification]);

  const [
    unreadParticularNotification,
    {
      isError: isErrorUnreadParticularNotification,
      error: errorUnreadParticularNotification,
      isSuccess: isSuccessUnreadParticularNotification,
      isLoading: isLoadingUnreadParticularNotification,
    },
  ] = useUnreadParticularNotificationMutation();

  useEffect(() => {
    if (isErrorUnreadParticularNotification) {
      addToast(errorUnreadParticularNotification?.data?.message, {
        appearance: 'error',
      });
    }
    if (isSuccessUnreadParticularNotification) {
      // router.push(level'/seller/view');
    }
  }, [
    isErrorUnreadParticularNotification,
    isSuccessUnreadParticularNotification,
  ]);

  const [
    deleteParticularNotification,
    {
      isError: isErrorDeleteParticularNotification,
      error: errorDeleteParticularNotification,
      isSuccess: isSuccessDeleteParticularNotification,
      isLoading: isLoadingDeleteParticularNotification,
    },
  ] = useDeleteParticularNotificationMutation();

  useEffect(() => {
    if (isErrorDeleteParticularNotification) {
      addToast(errorDeleteParticularNotification?.data?.message, {
        appearance: 'error',
      });
    }
    if (isSuccessDeleteParticularNotification) {
      // router.push(level'/seller/view');
    }
  }, [
    isErrorDeleteParticularNotification,
    isSuccessDeleteParticularNotification,
  ]);

  const [
    readAllNotification,
    {
      isError: isErrorReadAllNotification,
      error: errorReadAllNotification,
      isSuccess: isSuccessReadAllNotification,
      //   isLoading: isLoadingReadAllNotification,
    },
  ] = useReadAllNotificationMutation();

  useEffect(() => {
    if (isErrorReadAllNotification) {
      addToast(errorReadAllNotification?.data?.message, {
        appearance: 'error',
      });
    }
    if (isSuccessReadAllNotification) {
      // router.push(level'/seller/view');
    }
  }, [isErrorReadAllNotification, isSuccessReadAllNotification]);

  const handleReadParticularNotification = (notificationId) => {
    readParticularNotification({ notificationId: notificationId });
  };
  const handleUnreadParticularNotification = (notificationId) => {
    unreadParticularNotification({ notificationId: notificationId });
  };
  const handleDeleteParticularNotification = (notificationId) => {
    deleteParticularNotification({ notificationId: notificationId });
  };
  const handleReadAllNotification = (userId) => {
    readAllNotification({ userId: userId });
  };

  return (
    <li className="flex items-center relative has-dropdown cursor-pointer">
      <a
        href="#"
        className="relative text-textColor hover:text-primary opacity-80 p-[12px] md:p-0"
      >
        <BellIcon className="w-6 h-6" />
        {/* <span className="bg-error  rounded-full w-[10px] h-[10px]  absolute bottom-[30px] right-2 md:bottom-[1px] md:right-[1px]">
          2
        </span> */}

        {totalUnReadNotificationFunctions(dataNotification?.docs) ? (
          <span className="bg-error  rounded-full w-[22px] h-[22px] flex justify-center items-center text-[13px] absolute text-white top-[-13px] right-[-11px]">
            {totalUnReadNotificationFunctions(dataNotification?.docs)}
          </span>
        ) : (
          ''
        )}
      </a>
      {true && (
        <ul
          className="py-2 Guest border dropdown border-gray-200 rounded-md opacity-0 w-[300px] sm:w-[350px] invisible absolute z-50 left-[-60px] bottom-[100%] md:-right-12 md:left-auto bg-white shadow-md mt-3
                      transition-all  min-h-[200px] max-h-[90vh]  overflow-y-auto  md:top-full md:bottom-auto"
        >
          <li className="px-[12px] py-[5px] flex justify-between items-center">
            <h5>Notifications</h5>
            <a
              onClick={() => handleReadAllNotification(userId)}
              className="text-textColor text-[12px] hover:text-primary-dark font-semibold"
            >
              Mark all as read
            </a>
          </li>

          {dataNotification?.docs?.map((notificationData, i) => {
            return (
              <div key={i} className="group relative mb-[2px]">
                <li
                  className={`px-[12px] py-[12px]  ${
                    notificationData.isRead
                      ? ''
                      : 'bg-secondary-light bg-opacity-10'
                  }`}
                  onClick={() => {
                    if (!notificationData.isRead) {
                      handleReadParticularNotification(notificationData.id);
                      if (notificationData.itemId) {
                        router.push(notificationData.itemId);
                      }
                    }
                  }}
                >
                  <a
                    href="#"
                    className="flex flex-wrap text-textColor hover:text-primary-dark group"
                  >
                    <div className="image-holder w-[80px] h-[60px]">
                      {notificationData.imageUrl ? (
                        <img
                          src={notificationData.imageUrl}
                          alt="image description"
                          className="rounded-lg group-hover:opacity-90 transition-all w-full h-full object-cover"
                        />
                      ) : (
                        <img
                          src="images/bike01.jpg"
                          alt="image description"
                          className="rounded-lg group-hover:opacity-90 transition-all"
                        />
                      )}
                    </div>
                    <div className="text-holder text-[13px] flex-1 pl-[15px]">
                      <span>
                        {notificationData.content}
                        <br></br>
                        {agodate(new Date(notificationData.createdAt))}
                      </span>
                    </div>
                  </a>
                </li>
                <div className="absolute bg-white bg-opacity-70 p-2 top-0 right-0 opacity-0 invisible group-hover:opacity-100 group-hover:visible transition-all">
                  {notificationData.isRead ? (
                    <span
                      onClick={() => {
                        handleUnreadParticularNotification(notificationData.id);
                        setMyKey(i);
                      }}
                    >
                      <XIcon className="w-4 h-4 inline-block hover:text-error" />
                    </span>
                  ) : (
                    <span
                      onClick={() => {
                        handleReadParticularNotification(notificationData.id);
                        setMyKey(i);
                      }}
                    >
                      <CheckIcon className="w-4 h-4 inline-block hover:text-primary-dark" />
                    </span>
                  )}

                  <span
                    onClick={() => {
                      handleDeleteParticularNotification(notificationData.id);
                      setMyKey(i);
                    }}
                    className="inline-block ml-3"
                  >
                    <TrashIcon className="w-4 h-4 inline-block hover:text-error"></TrashIcon>
                  </span>
                </div>

                {i === myKey && (
                  <LoadingCustom
                    show={[
                      isLoadingReadParticularNotification,
                      isLoadingUnreadParticularNotification,
                      isLoadingDeleteParticularNotification,
                    ]}
                  ></LoadingCustom>
                )}
              </div>
            );
          })}

          {/* <li className="px-[12px] py-[10px]">
              <a
                href="#"
                className="flex flex-wrap text-textColor hover:text-primary-dark group"
              >
                <div className="image-holder w-[80px] h-[60px]">
                  <img
                    src="images/bike01.jpg"
                    alt="image description"
                    className="rounded-lg group-hover:opacity-90 transition-all"
                  />
                </div>
                <div className="text-holder text-[13px] flex-1 pl-[15px]">
                  <span>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  </span>
                </div>
              </a>
            </li> */}
        </ul>
      )}
    </li>
  );
};

export default Notification;
