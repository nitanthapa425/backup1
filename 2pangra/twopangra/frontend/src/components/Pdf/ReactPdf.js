import JsPDF from 'jspdf';
import 'jspdf-autotable';
// body=[{id:"id1",name:"name1"},{{id:"id2",name:"name2"}}]
// column = [{ header: 'Id', dataKey: 'id' },{ header: 'Name', dataKey: 'name' }];
export const generatePDF = (body, columns, name) => {
  const doc = new JsPDF('p', 'mm', [600, 792]);
  // checking for default fonts
  // console.log(console.log(doc.getFontList()));
  doc.autoTable({
    body: body,
    columns: columns,
    styles: {
      cellPadding: 1,
      fontSize: 10,
      font: 'Times',
      overflow: 'linebreak',
    },
    columnStyles: {
      brandVehicleDescription: { cellWidth: 300 },
      // brandVehicleName: { cellWidth: 300 },
      // modelName: { cellWidth: 300 },
      modelDescription: { cellWidth: 300 },
      // brandName: { cellWidth: 300 },
      // varientName: { cellWidth: 300 },
      varientDescription: { cellWidth: 300 },
    },
  });

  doc.save(`${name} ${new Date(Date.now()).toLocaleDateString()}.pdf`);
};
