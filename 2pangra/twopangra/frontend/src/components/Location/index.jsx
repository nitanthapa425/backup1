import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Form, Formik } from 'formik';
// import AdminLayout from 'layouts/Admin';

import { useToasts } from 'react-toast-notifications';

import Input from 'components/Input';
// import Select from '../Select/index';

import Button from 'components/Button';

import Popup from 'components/Popup';

import 'react-datepicker/dist/react-datepicker.css';
// import UserLayout from 'layouts/User';

import {
  // useUpdateSellerMutation,
  useAddToBookMutation,
} from 'services/api/seller';

import SelectWithoutCreate from 'components/Select/SelectWithoutCreate';
import { useRouter } from 'next/router';

import {
  // useCreateLocationMutation,
  // useCreateMunicipalityVdcByDistrictMutation,
  // useReadDistrictsByProvincesQuery,
  // useReadMunicipalityVdcByDistrictQuery,
  // useReadProvincesQuery,
  useGetAllLocationQuery,
} from 'services/api/location';

// import { useSelector } from 'react-redux';
import { LocationValidationSchema } from 'validation/location';
import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
import { useGetDetailLocationQuery } from 'services/api/getFullLocation';
// import ReactSelect from 'components/Select/ReactSelect';
// import NumberFormat from 'react-number-format';
// import NumberFormatInput from 'components/Input/NumberFormatInput';
const Address = ({ bookList }) => {
  const router = useRouter();
  const formikBag = useRef();
  // const sellerId = router?.query?.id;
  // const [provinceValue, setProvinceValue] = useState('');
  // const [districtValue, setDistrictValue] = useState('');
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);
  // const [municipalityValue, setMunicipalityValue] = useState('');

  // const level = useSelector((state) => state.levelReducer.level);

  const { addToast } = useToasts();
  // const [createLocationInfo, { isError, error, isSuccess, isLoading }] =
  //   useCreateLocationMutation();

  const [openModal, setOpenModal] = useState(false);
  const [editLocationInitialValue, setEditLocationInitialValue] = useState({});

  // const { data: provinces, isFetching: isFetchingProvinces } =
  //   useReadProvincesQuery();
  // const { data: district, isFetching: isFetchingDistrictByProvince } =
  //   useReadDistrictsByProvincesQuery(provinceValue, {
  //     skip: !provinceValue,
  //   });
  // const {
  //   data: municipalityVdc,
  //   isFetching: isFetchingMunicipalityVdcByDistrict,
  // } = useReadMunicipalityVdcByDistrictQuery(districtValue, {
  //   skip: !districtValue,
  // });

  const { data: combineLocations, isFetching: isFetchingCombineLocation } =
    useGetDetailLocationQuery();

  // createMarketPrice, { isLoading: isLoadingCreateMarketPrice }]
  // const [
  //   createMunicipalityVdcByDistrict,
  //   {
  //     isFetching: isFetchingCreateMunicipalityVdcByDistrict,
  //     isError: isErrorCreateMunicipalityVdcByDistrict,
  //     error: errorCreateMunicipalityVdcByDistrict,
  //   },
  // ] = useCreateMunicipalityVdcByDistrictMutation();
  // useEffect(() => {
  //   setProvinceValue(formikBag?.current?.values?.province);
  // }, [formikBag?.current?.values?.province]);
  // useEffect(() => {
  //   setDistrictValue(formikBag?.current?.values?.district);
  // }, [formikBag?.current?.values?.district]);

  // useEffect(() => {
  //   if (isSuccess) {
  //     formikBag.current?.resetForm();

  //     addToast('Thank You location Details has been added successfully.', {
  //       appearance: 'success',
  //     });
  //     setChanged(false);
  //     router.push('/book');
  //     // router.push(
  //     //   level === 'superAdmin'
  //     //     ? '/seller/view'
  //     //     : level === 'customer'
  //     //     ? '/sell/view'
  //     //     : ''
  //     // );
  //   }
  // }, [isSuccess]);
  // useEffect(() => {
  //   if (isError) {
  //     addToast(
  //       error?.data?.message ||
  //         'Error occured while Location details. Please try again later.',
  //       {
  //         appearance: 'error',
  //       }
  //     );
  //   }
  // }, [isError]);

  // const addLocationInitialValue = {
  //   province: '',
  //   district: '',
  //   municipalityVdc: '',
  //   wardNo: '',
  //   toleMarg: '',

  //   streetRoadName: '',
  //   nearByLocation: '',
  //   // geoLocation: {
  //   //   type: 'Point',
  //   //   coordinates: [78899.89, 89897.89],
  //   // },
  // };

  const {
    data: locationDetails,
    error: locationFetchError,
    // isLoading: isLoadingBrand,
  } = useGetAllLocationQuery();
  // console.log(locationDetails);
  // const loc = locationDetails?.filter((res) => res.label === 'billing');
  // console.log(loc);
  // const locationId = loc[0]?._id;

  // console.log(locationId);

  useEffect(() => {
    if (locationFetchError) {
      addToast(
        'Error occured while booking the vehicle. Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [locationFetchError]);

  // let BreadCrumbList = [];

  // if (type === 'add') {
  //   BreadCrumbList = [
  //     {
  //       routeName: 'Add Vehicle',
  //       route: '/vehicle/add',
  //     },
  //     {
  //       routeName: 'Add Selling Vehicle Information',
  //       route: '/seller',
  //     },
  //   ];
  // } else {
  //   BreadCrumbList = [
  //     {
  //       routeName: 'Add Vehicle',
  //       route: '/vehicle/add',
  //     },
  //     {
  //       routeName: 'Selling Vehicle List',
  //       route: '/seller/view',
  //     },
  //     {
  //       routeName: 'Edit Selling Vehicle Information',
  //       route: 'seller/edit',
  //     },
  //   ];
  // }

  useEffect(() => {
    if (locationDetails) {
      // console.log(locationDetails);
      const loc = locationDetails?.filter((res) => res.label === 'billing');
      // console.log(loc);
      const newLocationDetails = {
        // province: loc[0]?.province || '',
        // district: loc[0]?.district || '',
        // municipalityVdc: loc[0]?.municipalityVdc || '',
        // wardNo: loc[0]?.wardNo || '',
        // streetRoadName: loc[0]?.streetRoadName || '',
        // location: {
        // console.log(loc)
        combineLocation: loc[loc.length - 1]?.combineLocation || '',
        nearByLocation: loc[loc.length - 1]?.nearByLocation || '',

        label: loc[0]?.label || 'billing',
        // },
        //
        // geoLocation: {
        //   type: 'Point',
        //   coordinates: [78899.89, 89897.89],
        // },
      };

      // console.log(newLocationDetails);

      setEditLocationInitialValue(newLocationDetails);
      // setProvinceValue(loc[0]?.province || '');
      // setDistrictValue(loc[0]?.district || '');
      // setMunicipalityValue(loc[0]?.municipalityVdc || '');
    }
  }, [locationDetails]);
  // console.log(editLocationInitialValue);

  // useEffect(() => {
  //   setProvinceValue(formikBag?.current?.values?.loc?.province);
  // }, [formikBag?.current?.values?.loc?.province]);
  // useEffect(() => {
  //   setDistrictValue(formikBag?.current?.values?.loc?.district);
  // }, [formikBag?.current?.values?.loc?.district]);

  // const [
  //   updateLocation,
  //   {
  //     isLoading: updating,
  //     isError: isUpdateError,
  //     isSuccess: isUpdateSuccess,
  //     data: updateSuccessData,
  //   },
  // ] = useUpdateSellerMutation();
  // useEffect(() => {
  //   if (isUpdateError) {
  //     addToast('error occurred while updating vehicle sell.', {
  //       appearance: 'error',
  //     });
  //   }
  // }, [isUpdateError]);

  const [
    addToBook,
    {
      isLoading: isLoadingAddToBook,
      isError: isErrorAddToBook,
      isSuccess: isSuccessAddToBook,
      // data: addToBookData,
      error: addToBookError,
    },
  ] = useAddToBookMutation();

  // useEffect(() => {
  //   if (isUpdateSuccess) {
  //     formikBag.current?.resetForm();
  //     addToast(
  //       updateSuccessData?.message ||
  //         'Selling Vehicle Details updated successfully.',
  //       {
  //         appearance: 'success',
  //       }
  //     );

  //     // router.push(`/seller/get/${sellerId}`);
  //     // router.push(
  //     //   level === 'superAdmin'
  //     //     ? `/seller/get/${sellerId}`
  //     //     : level === 'customer'
  //     //     ? `/sell/get/${sellerId}`
  //     //     : ''
  //     // );
  //   }
  // }, [isUpdateSuccess]);

  useEffect(() => {
    if (isSuccessAddToBook) {
      addToast('Congratulation, You have booked the vehicle! ', {
        appearance: 'success',
      });
      // router.push('/book');
      router.push('/book');

      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessAddToBook]);
  useEffect(() => {
    if (isErrorAddToBook) {
      addToast(addToBookError?.data?.message, {
        appearance: 'error',
      });
      if (addToBookError?.data?.message === 'This item is already booked.') {
        router.push('/book');
      }
    }
  }, [isErrorAddToBook, addToBookError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    // if (sellerId) {
    //   updateLocation({ ...values });
    // } else {
    //   createLocationInfo(values, resetForm);
    // }

    // values.sellerVehicleId = bookList;

    // console.log('hello');
    // console.log(bookList);

    bookList.forEach((id, i) => {
      addToBook({ id, values });
    });

    // addToBook(values, resetForm);

    setSubmitting(false);
  };

  return (
    // <AdminLayout
    //   documentTitle={
    //     !sellerId ? 'Add New Vehicle Sell' : 'Edit Vehicle sell Details'
    //   }
    //   BreadCrumbList={BreadCrumbList}
    // >
    // <UserLayout
    //   documentTitle={
    //     !sellerId ? 'Add New Vehicle Sell' : 'Edit Vehicle sell Details'
    //   }
    // >
    <Formik
      initialValues={
        // sellerId ? editLocationInitialValue :
        editLocationInitialValue
      }
      onSubmit={onSubmit}
      validationSchema={LocationValidationSchema}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        setFieldTouched,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <Form>
            <h3 className="h3"> Enter your Location</h3>
            <div className="row-sm  pt-5 pb-2">
              <div className="w-full mb-5">
                <SelectWithoutCreate
                  required
                  loading={isFetchingCombineLocation}
                  label="Select Location"
                  placeholder="Location"
                  error={
                    touched?.combineLocation ? errors?.combineLocation : ''
                  }
                  value={values?.combineLocation || null}
                  onChange={(selectedValue) => {
                    setFieldValue('combineLocation', selectedValue.value);
                  }}
                  options={combineLocations?.map((value) => ({
                    label: value.fullAddress,
                    // value: `${value.id}`,
                    value: value.fullAddress,
                  }))}
                  onBlur={() => {
                    setFieldTouched('combineLocation');
                  }}
                />
              </div>

              <div className="w-full mb-3">
                <Input
                  required={true}
                  name="nearByLocation"
                  label="Near By Location"
                  type="text"
                />
              </div>
            </div>
            <div className="c-fixed  btn-holder">
              <Button
                type="submit"
                // disabled={isSubmitting}
                // loading={isLoading || updating}
                // onClick={() => {
                //   bookList.forEach((value, i) => {
                //     addToBook(value);
                //   });
                // }}
                // disabled={bookList.length === 0}
                loading={isLoadingAddToBook}
              >
                {/* {!sellerId ? 'Submit' : 'Update'} */}
                Confirm
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  setOpenModal(true);
                }}
                // disabled={isSubmitting || !dirty || isLoading || updating}
                disabled={isSubmitting || !dirty}
              >
                Clear
              </Button>
              {openModal && (
                <Popup
                  title="Do you want to clear all fields?"
                  description="if you clear all the filed will be removed"
                  onOkClick={() => {
                    resetForm();
                    setChanged(false);
                    setOpenModal(false);
                  }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )}
            </div>
          </Form>
        </div>
      )}
    </Formik>
    // </UserLayout>
  );
};

Address.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default Address;
