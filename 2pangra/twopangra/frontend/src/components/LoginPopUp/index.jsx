import { useEffect } from 'react';
import Link from 'next/link';
// import { useRouter } from 'next/router';
import { Form, Formik } from 'formik';

import Input from 'components/Input';
import Button from 'components/Button';
// import Alert from 'components/Alert';

import PasswordInput from 'components/Input/PasswordInput';
import { useToasts } from 'react-toast-notifications';
import { customerLoginValidationSchema } from 'validation/customer.validation';

import { useLoginCustomerMutation } from 'services/api/customerLogin';

const initialValues = {
  mobile: '',
  password: '',
};

const CustomerLoginPopUp = ({
  setIsOpen = () => {},
  modal,
  setModalFunction,
}) => {
  // const router = useRouter();
  const { addToast } = useToasts();
  const [loginCustomer, { isSuccess, isError, isLoading, error }] =
    useLoginCustomerMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast('Login successfully.', {
        appearance: 'success',
      });
      setIsOpen(false);

      if (modal === true) {
        setModalFunction(false);
      }
      if (modal === false) {
        setModalFunction(true);
      }
      // router.replace('/');
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(error?.data?.message || 'Mobile Number or password incorrect', {
        appearance: 'error',
        autoDismiss: false,
      });
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={loginCustomer}
      validationSchema={customerLoginValidationSchema}
      enableReinitialize
    >
      <div className="container mt-4">
        <h1 className="h3 mb-3">Welcome! Please Login to continue.</h1>

        <Form>
          <div className="w-full mb-2">
            <Input
              label="Mobile Number"
              name="mobile"
              type="text"
              placeholder="E.g: 98XXXXXXXX"
            />
          </div>

          <PasswordInput
            label="Password"
            name="password"
            placeholder="Password"
          ></PasswordInput>
          <div className="mt-3">
            <Button loading={isLoading} type="submit" className="btn-lg">
              Login
            </Button>

            <Button disabled={isLoading} type="reset" variant="outlined-error">
              Clear
            </Button>
            <div className="mt-3">
              <Link href="/customer/forgot-password">
                <a className="text-textColor">Forgot Password?</a>
              </Link>
              <Link href="/customer/add">
                <a className="text-textColor ml-2">Sign Up</a>
              </Link>
            </div>
            {/* <div className="mt-3"></div> */}
          </div>
          {/* {isError && (
          <div className="max-w-xs mt-3">
            <Alert
              message={error?.data?.message || 'Error logging in.'}
              type="error"
            />
          </div>
        )} */}
        </Form>
      </div>
    </Formik>
  );
};
export default CustomerLoginPopUp;
