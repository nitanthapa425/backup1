import { useState, useEffect } from 'react';
import Link from 'next/link';
import {
  PlusIcon,
  SearchIcon,
  MenuAlt3Icon,
  ChevronDownIcon,
} from '@heroicons/react/outline';
import { customerActions } from 'store/features/customerAuth/customerAuthSlice';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';
import Button from 'components/Button';
import useDebounce from 'hooks/useDebounce';
import { useReadBrandNoAuthWithLimitQuery } from 'services/api/brand';
import Notification from 'components/Notification/Notification';
import ReactModal from 'components/ReactModal/ReactModal';
import CustomerPasswordUpdateContainer from 'container/Customer/UpdatePassword';
import useOnclickOutside from 'react-cool-onclickoutside';
import CustomerLoginPopUp from 'components/LoginPopUp';
import LoadingCustom from 'components/LoadingCustom/LoadingCustom';
const UserHeader = () => {
  const [scrollPosition, setScrollPosition] = useState(0);

  const handleScroll = () => {
    if (typeof window !== 'undefined') {
      const position = window.pageYOffset;
      setScrollPosition(position);
    }
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('scroll', handleScroll);

      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }
  }, []);

  const loginInfo = useSelector((state) => state.customerAuth);
  const [searchedVehicle, setSearchedVehicle] = useState('');
  const [searchedBrand, setSearchedBrand] = useState('');
  const debSearchedBrand = useDebounce(searchedBrand, 1000);
  const [pageSize, setPageSize] = useState(24);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  const [brandQuery, setBrandQuery] = useState(
    `?select=id,brandVehicleName&limit=${pageSize}&sortBy=brandVehicleName&sortOrder=1&noRegex=hasInHomePage`
  );

  const {
    data: brandData,
    isError: isErrorBrandData,
    isLoading: isLoadingBrandData,
    isFetching: isFetchingBrandData,
  } = useReadBrandNoAuthWithLimitQuery(brandQuery);

  const token = loginInfo?.token;
  const dispatch = useDispatch();
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const switchFunctions = () => {
    const body = document.querySelector('body');
    body.classList.toggle('c-opener-menu');
  };
  const removeClass = () => {
    const body = document.querySelector('body');
    body.classList.remove('c-opener-menu');
  };
  const handleCustomerLogout = () => {
    dispatch(customerActions.removeCustomerToken());
    router.replace('/customer/login');
  };
  const ref = useOnclickOutside(removeClass);

  useEffect(() => {
    setBrandQuery(
      `?select=id,brandVehicleName&search="brandVehicleName":"${debSearchedBrand}"&limit=${pageSize}&sortBy=brandVehicleName&sortOrder=1&noRegex=hasInHomePage`
    );
  }, [debSearchedBrand, pageSize]);

  useEffect(() => {
    if (brandData) {
      setHasNextPage(brandData?.hasNextPage);
    }
  }, [brandData]);

  const handleSellBike = (route, token) => {
    if (token) {
      router.push(route);
    } else {
      // router.push('/customer/login');
      setOpenModal(true);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const setModalFunction = (value) => {
    setModal(value);
  };

  return (
    <div>
      <header
        id="header"
        className={`c-header z-[99999] bg-white w-full transition delay-150 duration-300 ease-in-out ${
          scrollPosition > 10
            ? 'header-fixed fixed shadow-xl !py-[10px]'
            : '!py-[15px]'
        }`}
      >
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}

        <div className="container flex justify-between md:justify-start">
          <div className="logo mr-6 w-[135px] sm:w-[160px]">
            <Link href="/">
              <a className="text-lg md:text-2xl font-bold mb-0">
                <img src="/images/logo.svg" alt="2 Pangra" />
              </a>
            </Link>
          </div>
          <nav
            id="nav"
            className="bg-white rounded-t-[15px] z-50 md:rounded-none md:bg-transparent shadow-negative md:shadow-none flex-1 flex items-center fixed left-0 right-0 bottom-0  md:relative"
            ref={ref}
          >
            <div className="c-drop z-30 opacity-0 invisible flex-1 absolute bottom-12 right-[15px] bg-gray-600 rounded-md shadow-sm md:visible md:opacity-100 md:shadow-none md:rounded-none md:bg-transparent md:relative md:bottom-auto md:right-auto">
              <ul className="c-menu-center md:flex md:flex-wrap md:items-center md:space-x-7 md:justify-center">
                <li>
                  <Link href="/hot-deals">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Hot Deals
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/vehicle-listing">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Vehicle For Sale
                    </a>
                  </Link>
                </li>
                <li className="relative has-dropdown cursor-pointer">
                  <Link href="#">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Brands{' '}
                      <ChevronDownIcon className="w-3 h-3 text-textColor inline-block" />
                    </a>
                  </Link>
                  <div className="relative">
                    <ul
                      className="border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[230px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                    >
                      <div className="search relative px-4">
                        <input
                          type="search"
                          placeholder="Search brands"
                          className=" border-0 p-[5px] shadow-none border-b-[1px] border-gray-400 mb-3 rounded-none focus:outline-none focus:ring-0"
                          onChange={(e) => {
                            setSearchedBrand(e.target.value);
                          }}
                        />
                        <LoadingCustom
                          show={[isLoadingBrandData || isFetchingBrandData]}
                        ></LoadingCustom>
                      </div>
                      <>
                        {isErrorBrandData ? (
                          <li>
                            <Link href="#">
                              <a className="text-textColor hover:text-primary py-[4px] px-4">
                                Error loading data
                              </a>
                            </Link>
                          </li>
                        ) : isLoadingBrandData ? (
                          <p className="f-sm text-center">Loading...</p>
                        ) : brandData?.docs?.length ? (
                          brandData?.docs?.map((b, i) => (
                            <li key={i}>
                              <Link
                                href={`/vehicle-listing?browseBy=brand&brandName=${b?.brandVehicleName}&`}
                              >
                                <a className="text-textColor hover:text-primary py-[4px] px-4">
                                  {b?.brandVehicleName}
                                </a>
                              </Link>
                            </li>
                          ))
                        ) : null}
                      </>
                      {hasNextPage ? (
                        <div className="text-center">
                          <Button
                            type="button"
                            variant="secondary"
                            disabled={!hasNextPage}
                            onClick={() => {
                              setPageSize(pageSize + 12);
                            }}
                            loading={isFetchingBrandData}
                            size="sm"
                          >
                            Load More
                          </Button>
                        </div>
                      ) : null}
                    </ul>
                  </div>
                </li>
              </ul>
            </div>

            <ul className="menu-right flex items-center md:space-x-7 justify-between md:justify-items-center w-full md:w-auto">
              <li className="flex items-center relative has-dropdown">
                <a
                  href="#"
                  className="text-textColor hover:text-primary opacity-80 p-[12px] md:p-0"
                >
                  <SearchIcon className="w-6 h-6" />
                </a>
                <ul
                  className="Guest border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[250px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                              transition-all max-h-[400px] overflow-y-auto md:top-full md:bottom-auto"
                >
                  <li className="px-[12px] py-[5px]">
                    <span className="text-sm inline-block mb-3">
                      <div className="search relative">
                        <SearchIcon className="h-5 w-5 absolute top-1/2 translate-y-[-60%] left-3" />
                        <input
                          type="search"
                          placeholder="Search two wheelers.."
                          className="pl-[36px]"
                          onChange={(e) => {
                            setSearchedVehicle(e.target.value);
                          }}
                        />
                      </div>
                    </span>
                    <div className="text-center">
                      <Button
                        type="button"
                        variant={
                          searchedVehicle !== '' ? 'primary' : 'disabled'
                        }
                        onClick={() =>
                          router.push(
                            `/vehicle-listing?search=${searchedVehicle}`
                          )
                        }
                      >
                        Search
                      </Button>
                    </div>
                  </li>
                </ul>
              </li>
              {token ? <Notification /> : null}
              <li className="flex items-center relative has-dropdown cursor-pointer">
                {loginInfo?.customer?.profileImagePath?.imageUrl ? (
                  <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                    <img
                      width="100%"
                      height="100%"
                      src={`${loginInfo?.customer?.profileImagePath?.imageUrl}`}
                    ></img>
                  </div>
                ) : (
                  <a href="#">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-8 w-7"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="#231F20"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={1}
                        d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </a>
                )}
                {token ? (
                  <ul
                    className="Customer border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[180px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li>
                      {' '}
                      <Link href={`/wish-list`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Wish List
                        </a>
                      </Link>
                    </li>

                    <li>
                      {' '}
                      <Link href={`/book`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Booked List
                        </a>
                      </Link>
                    </li>
                    <li>
                      {' '}
                      <Link href={`/customer-profile/get`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          My Profile
                        </a>
                      </Link>
                    </li>
                    <li>
                      {/* <Link href="/customer/update-password">
                        <a className="text-textColor hover:text-primary px-4 py-[7px]">
                          Update Password
                        </a>
                      </Link> */}
                      <a className="text-textColor hover:text-primary px-4 py-[7px]">
                        <ReactModal link="Update Password">
                          <CustomerPasswordUpdateContainer />
                        </ReactModal>
                      </a>
                    </li>
                    <li>
                      <Link href="/sell/view">
                        <a className="text-textColor hover:text-primary px-4 py-[7px]">
                          Selling Vehicles
                        </a>
                      </Link>
                    </li>
                    <span
                      className="text-textColor hover:text-primary cursor-pointer px-4 py-[7px]"
                      onClick={handleCustomerLogout}
                    >
                      Log Out
                    </span>
                  </ul>
                ) : (
                  <ul
                    className="Guest border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[250px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                              transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li className="px-[12px] py-[5px]">
                      <h5 className="text">Hi Guest</h5>
                      <span className="text-sm inline-block mb-3">
                        Login to get your dream vehicle
                      </span>
                      <Link href="/customer/login">
                        <a className="btn btn-primary btn-sm w-full text-center px-4 py-[7px]">
                          Login
                        </a>
                      </Link>
                    </li>
                  </ul>
                )}
              </li>

              <li className="hidden md:inline-block">
                <a
                  className=" btn btn-primary btn-sm cursor-pointer mb-0 mr-0"
                  onClick={() => {
                    handleSellBike('/sell', token);
                  }}
                >
                  <PlusIcon className="h-4 w-4 inline-block mt-[-3px] mr-[6px]" />
                  Sell
                </a>
              </li>
              <a
                className="opener md:hidden p-[12px] md:p-0"
                href="#"
                onClick={switchFunctions}
              >
                {' '}
                <span>
                  {' '}
                  <MenuAlt3Icon className="w-6 h-6" />
                </span>
              </a>
            </ul>
          </nav>
          <ul className="md:hidden">
            <li>
              <a
                className=" btn btn-primary btn-sm cursor-pointer mb-0 mr-0"
                onClick={() => {
                  handleSellBike('/sell', token);
                }}
              >
                <PlusIcon className="h-4 w-4 inline-block mt-[-3px] mr-[6px]" />
                Sell
              </a>
            </li>
          </ul>
        </div>
      </header>
    </div>
  );
};
export default UserHeader;
