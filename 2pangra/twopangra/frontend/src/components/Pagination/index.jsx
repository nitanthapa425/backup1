import ReactPaginate from "react-paginate";

function Pagination({ pageCount, pageIndex, handlePageChange, isLoading }) {
    const handlePageClick = (e) => {
        handlePageChange(e.selected);
    };
    const nextSvg = () => {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clipRule="evenodd" />
            </svg>
        );
    };
    const prevSvg = () => {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clipRule="evenodd" />
            </svg>
        );
    };
    return (
        <ReactPaginate
            breakLabel="..."
            nextLabel={nextSvg()}
            onPageChange={handlePageClick}
            pageRangeDisplayed={2}
            pageCount={pageCount}
            previousLabel={prevSvg()}
            renderOnZeroPageCount={null}
            // When we are on let's say, page 10 and we sort a column, we get data for the 1st page with sorted data. So we have to move the pagination ui to page 1. So using forcePage prop.
            forcePage={pageIndex}
            className={isLoading ? "pointer-events-none" : ""}
        />
    );
}

export default Pagination;

