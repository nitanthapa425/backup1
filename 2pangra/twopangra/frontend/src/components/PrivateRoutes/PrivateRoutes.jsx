import React from 'react';
import { useSelector } from 'react-redux';
// import AddVehiclePage from 'container/Vehicle/Add';
// import { useRouter } from 'next/router';

const PrivateRoutes = ({ showFor, children }) => {
  // const router = useRouter();
  const { user } = useSelector((state) => state.adminAuth);
  //   console.log('accessLevel', accessLevel);
  if (user && user.accessLevel && showFor.includes(user.accessLevel)) {
    return <div>{children}</div>;
  } else {
    return <div>You can not access this page</div>;
  }
};

export default PrivateRoutes;
