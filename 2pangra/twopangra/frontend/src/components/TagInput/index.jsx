import ReactTagInput from '@pathofdev/react-tag-input';
import '@pathofdev/react-tag-input/build/index.css';

const TagInput = ({
  label,
  required,
  value,
  handleChange,
  placeholder,
  error,
}) => {
  return (
    <>
      <label htmlFor="">
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <div>
        <ReactTagInput
          tags={value}
          onChange={handleChange}
          placeholder={placeholder}
        />
      </div>
      {error && <div className="text-primary-dark text-sm mt-1">{error}</div>}
    </>
  );
};

export default TagInput;
