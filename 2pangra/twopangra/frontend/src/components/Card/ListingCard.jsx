import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import Link from 'next/link';
import {
  LocationMarkerIcon,
  ClockIcon,
  UserIcon,
  PhotographIcon,
  BadgeCheckIcon,
  HeartIcon,
  // ShareIcon,
  // CheckCircleIcon,
  // XCircleIcon,
} from '@heroicons/react/outline';
import {
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookIcon,
} from 'react-share';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import { useAddToWishListMutation } from 'services/api/seller';
import { useToasts } from 'react-toast-notifications';
import { useSelector } from 'react-redux';
import { CityLastWord } from 'utils/LastWord';
import ReactModal from 'components/ReactModal/ReactModal';
import CustomerLoginPopUp from 'components/LoginPopUp';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';

function ListingCard({
  key,
  cardColsSize = 'two',
  route,
  imgUrl,
  totalImages = 0,
  vehicleName,
  price,
  bikeDriven,
  id,
  ownershipCount,
  condition,
  isSold = false,
  isVerified,
  location,
}) {
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const router = useRouter();
  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);
  const shareUrl = ' https://duipangra-fd-uat.herokuapp.com/';
  const setModalFunction = (value) => {
    setModal(value);
  };
  const handleWishList = (query, token) => {
    if (token) {
      addToWishList(query);
    } else {
      // router.push('/customer/login');
      setOpenModal(true);
    }
    // console.log(token);
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const [
    addToWishList,
    {
      isLoading: isLoadingAddToWishList,
      isError: isErrorAddToWishList,
      isSuccess: isSuccessAddToWishList,
      // data: addToWishListData,
      error: addToWishListError,
    },
  ] = useAddToWishListMutation();
  useEffect(() => {
    if (isSuccessAddToWishList) {
      addToast('You just add vehicle in wish list.', {
        appearance: 'success',
      });
      router.push('/wish-list');
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessAddToWishList]);
  useEffect(() => {
    if (isErrorAddToWishList) {
      addToast(addToWishListError?.data?.message, {
        appearance: 'error',
        autoDismiss: false,
      });
    }
  }, [isErrorAddToWishList, addToWishListError]);

  return (
    <div key={key} className={`${cardColsSize}-col`}>
      <div className="card">
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}
        <div className="card-image-holder relative">
          <figure className="image-holder rounded-xl overflow-hidden group">
            <div className="absolute z-40 top-6 space-y-2 right-6 w-[30px] transition-all opacity-0 group-hover:opacity-100 ">
              <a>
                <Tooltip overlay="Click to add in wish list" placement="right">
                  <HeartIcon
                    onClick={() => {
                      handleWishList(id, token);
                    }}
                    loading={isLoadingAddToWishList}
                    className="w-6 h-6 text-white"
                  />
                </Tooltip>
              </a>
              {/* <Tooltip overlay="Click to share" placement="right">
                <ShareIcon className="w-6 h-6 text-white" />
              </Tooltip> */}
              <Tooltip overlay="Click to share" placement="right">
                <FacebookShareButton
                  url={shareUrl}
                  quote={'2 Pangra'}
                  hashtag={'#2Pangra'}
                >
                  <FacebookIcon size={25} round={true} />
                </FacebookShareButton>
              </Tooltip>
              <Tooltip overlay="Click to share" placement="right">
                <WhatsappShareButton
                  url={shareUrl}
                  quote={'2 Pangra'}
                  hashtag={'#2Pangra'}
                >
                  <WhatsappIcon size={25} round={true} />
                </WhatsappShareButton>
              </Tooltip>
              {/* {isVerified ? (
                <span>
                  <CheckCircleIcon className="w-6 h-6 text-primary " />
                </span>
              ) : (
                <span>
                  <XCircleIcon className="w-6 h-6 text-error " />
                </span>
              )} */}
            </div>

            <Link href={route}>
              <a className="relative w-[200px] h-[200px] rounded-xl overflow-hidden before:transition-all before:absolute before:z-10 before:inset-0 before:bg-textColor before:bg-opacity-0 group-hover:before:bg-opacity-30">
                {imgUrl?.substring(0, 4) === 'http' ? (
                  <Image src={imgUrl} alt="Img Desc" layout="fill" />
                ) : (
                  <img src={imgUrl} alt="image description" />
                )}
                {/* <img
                src={imgUrl}
                alt="image description"
              /> */}
              </a>
            </Link>
          </figure>
          {isSold ? (
            <div className="absolute flex justify-center items-center inset-0 bg-black bg-opacity-60 text-white font-semibold text-lg z-50 ">
              <span>{isSold ? 'SOLD OUT' : ''}</span>
            </div>
          ) : (
            ''
          )}{' '}
          {isVerified ? (
            <div className="verified inline-block text-[13px] px-3 py-[2px] w-auto bg-primary-dark text-white absolute top-2 left-2 rounded-r-md">
              <BadgeCheckIcon className="w-4 h-4 inline-block mt-[-2px] mr-1" />
              Verified
            </div>
          ) : null}
          <div className="no-of-image absolute right-3 bottom-3 bg-gray-200 flex  items-center px-2 rounded-md ">
            <PhotographIcon className="h-4 w-4" />
            <span className="number text-sm ml-1">
              {totalImages !== 0 && `${totalImages}+`}
              {/* {data?.bikeImagePath.length - 1 !== 0 &&
                                `${data?.bikeImagePath.length - 1}+`} */}
            </span>
          </div>
        </div>
        <div className="card-body">
          <Link href={route}>
            <a className="vehicle-name text-textColor">{vehicleName}</a>
          </Link>
          <div className="price-compare">
            <span className="price">
              {/* NRs {thousandNumberSeparator(data?.expectedPrice)} */}
              NRs. {thousandNumberSeparator(price)}
            </span>
            <div className="compare">Add to compare</div>
          </div>
          <div className="detail-wrapper">
            <div className="detail">
              <div className="icon-holder">
                <LocationMarkerIcon />
              </div>
              {CityLastWord(location) ? (
                <span className="name-icon">{CityLastWord(location)}</span>
              ) : (
                <span className="name-icon">Not Available</span>
              )}
            </div>
            <div className="detail">
              <div className="icon-holder">
                <ClockIcon />
              </div>
              <span className="name-icon">
                {/* {thousandNumberSeparator(data?.bikeDriven)} Km */}
                {thousandNumberSeparator(bikeDriven)} Km
              </span>
            </div>
            <div className="detail">
              <div className="icon-holder">
                <BadgeCheckIcon />
              </div>
              <span className="name-icon">
                {/* {data?.bikeNumber} */}
                {condition}
              </span>
              {/* {console.log(condition)} */}
            </div>
            <div className="detail">
              <div className="icon-holder">
                <UserIcon />
              </div>
              <span className="name-icon">
                {/* {data?.ownershipCount} Owner */}
                {ownershipCount} Owners
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListingCard;
