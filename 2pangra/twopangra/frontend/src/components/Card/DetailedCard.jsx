import { useRouter } from 'next/router';
import {
  LocationMarkerIcon,
  ClockIcon,
  UserIcon,
  PhotographIcon,
  HeartIcon,
  ShareIcon,
  CheckCircleIcon,
  XCircleIcon,
} from '@heroicons/react/outline';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
function DetailedCard({
  id,
  imgPathArray,
  name,
  price,
  bikeDriven,
  bikeNumber,
  ownershipCount,
  index,
  isSold = false,
  isVerified,
}) {
  const router = useRouter();

  return (
    <div
      key={index}
      className="three-col"
      onClick={() => {
        router.push(`/vehicle-listing/${id}`);
      }}
    >
      <div className="card">
        <div className="card-image-holder relative">
          <figure className="group">
            <div className="absolute z-20 top-6 space-y-2 right-6 w-[30px] transition-all opacity-0 group-hover:opacity-100 ">
              {/* <a href="#"> */}
              <HeartIcon
                // onClick={() => {
                //   handleWishList(id, token);
                // }}
                // loading={isLoadingAddToWishList}
                className="w-6 h-6 text-white"
              />
              {/* </a> */}
              <a href="#">
                <ShareIcon className="w-6 h-6 text-white" />
              </a>
              {isVerified ? (
                <span>
                  <CheckCircleIcon className="w-6 h-6 text-primary " />
                </span>
              ) : (
                <span>
                  <XCircleIcon className="w-6 h-6 text-error " />
                </span>
              )}
            </div>
            <a href="#">
              <img src={imgPathArray[0].imageUrl} alt="image description" />
            </a>
          </figure>
          {isSold ? (
            <div className="absolute flex justify-center items-center inset-0 bg-black bg-opacity-60 text-white font-semibold text-lg z-50 ">
              <span>{isSold ? 'SOLD OUT' : ''}</span>
            </div>
          ) : (
            ''
          )}{' '}
          <div className="no-of-image absolute right-3 bottom-3 bg-gray-200 flex  items-center px-2 rounded-md ">
            <PhotographIcon className="h-4 w-4" />
            <span className="number text-sm ml-1">
              {imgPathArray.length - 1 !== 0 && `${imgPathArray.length - 1}+`}
            </span>
          </div>
        </div>
        <div className="card-body">
          <span className="vehicle-name">{name}</span>
          <div className="price-compare">
            <span className="price">NRs {thousandNumberSeparator(price)}</span>
            <div className="compare">add to compare</div>
          </div>
          <div className="detail-wrapper">
            <div className="detail">
              <div className="icon-holder">
                <LocationMarkerIcon />
              </div>
              <span className="name-icon">Static Location </span>
            </div>
            <div className="detail">
              <div className="icon-holder">
                <ClockIcon />
              </div>
              <span className="name-icon">{bikeDriven} Km</span>
            </div>
            {/* <div className="detail">
              <div className="icon-holder">
                <ClockIcon />
              </div>
              <span className="name-icon">{bikeNumber}</span>
            </div> */}
            <div className="detail">
              <div className="icon-holder">
                <UserIcon />
              </div>
              <span className="name-icon">{ownershipCount} Owner</span>
            </div>
            {/* <div className="detail">
                            <div className="icon-holder">
                                <UserIcon />
                            </div>
                            <span className="name-icon">
                                {vehicle.condition}
                            </span>
                        </div>
                        <div className="detail">
                            <div className="icon-holder">
                                <UserIcon />
                            </div>
                            <span className="name-icon">{vehicle.color}</span>
                        </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailedCard;
