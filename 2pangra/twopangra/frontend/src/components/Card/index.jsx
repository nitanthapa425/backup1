import Link from 'next/link';
import Image from 'next/image';
import {
  // CheckCircleIcon,
  HeartIcon,
  // ShareIcon,
  // XCircleIcon,
  BadgeCheckIcon,
} from '@heroicons/react/outline';
import { thousandNumberSeparator } from 'utils/thousandNumberFormat';
import {
  useAddToWishListMutation,
  // useReadSingleSellerQuery,
} from 'services/api/seller';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useToasts } from 'react-toast-notifications';
import { useSelector } from 'react-redux';
import Tooltip from 'rc-tooltip';
import 'rc-tooltip/assets/bootstrap.css';
import ReactModal from 'components/ReactModal/ReactModal';
import CustomerLoginPopUp from 'components/LoginPopUp';
// import Button from 'components/Button';
import {
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookIcon,
} from 'react-share';

function Card({
  imgPath,
  name,
  subName,
  linkPath,
  index,
  id,
  isSold = false,
  isVerified,
  location,
  isInWishList = false,
}) {
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const router = useRouter();
  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);
  const shareUrl = ' https://duipangra-fd-uat.herokuapp.com/';
  const setModalFunction = (value) => {
    setModal(value);
  };
  const handleWishList = (query, token) => {
    if (token) {
      addToWishList(query);
    } else {
      // router.push('/customer/login');
      setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const [
    addToWishList,
    {
      isLoading: isLoadingAddToWishList,
      isError: isErrorAddToWishList,
      isSuccess: isSuccessAddToWishList,
      // data: addToWishListData,
      error: addToWishListError,
    },
  ] = useAddToWishListMutation();
  // const {
  //   data: vehicleDetails,
  //   // isError: isVehicleError,
  //   // isLoading: isLoadingVehicle,
  // } = useReadSingleSellerQuery(router.query.id, {
  //   skip: !router.query.id,
  // });

  useEffect(() => {
    if (isSuccessAddToWishList) {
      addToast('You just add vehicle in wish list.', {
        appearance: 'success',
      });
      router.push('/wish-list');
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessAddToWishList]);
  useEffect(() => {
    if (isErrorAddToWishList) {
      addToast(addToWishListError?.data?.message, {
        appearance: 'error',
        autoDismiss: false,
      });
    }
  }, [isErrorAddToWishList, addToWishListError]);
  return (
    <div className="c-card-holder px-2" data-value={++index} key={index}>
      {openModal ? (
        <ReactModal
          directlyOpen={true}
          closeFunc={handleCloseModal}
          modal={modal}
        >
          <CustomerLoginPopUp
            setModalFunction={setModalFunction}
            modal={modal}
          ></CustomerLoginPopUp>
        </ReactModal>
      ) : null}
      <div className="c-card-sm ">
        <figure className="image-holder rounded-xl overflow-hidden group">
          <div className="absolute z-20 top-6 space-y-2 right-6 w-[30px] transition-all opacity-0 group-hover:opacity-100 ">
            {isInWishList ? (
              <Tooltip overlay="Click to add in wish list" placement="right">
                <HeartIcon
                  onClick={() => {
                    handleWishList(id, token);
                  }}
                  loading={isLoadingAddToWishList}
                  className="w-6 h-6 text-red-600 "
                />
              </Tooltip>
            ) : (
              <Tooltip overlay="Click to add in wish list" placement="right">
                <HeartIcon
                  onClick={() => {
                    handleWishList(id, token);
                  }}
                  loading={isLoadingAddToWishList}
                  className="w-6 h-6 text-white"
                />
              </Tooltip>
            )}
            {/* <Tooltip overlay="Click to share" placement="right">
              <ShareIcon className="w-6 h-6 text-white flex items-center relative has-dropdown cursor-pointer"></ShareIcon>
            </Tooltip> */}

            <Tooltip overlay="Click to share" placement="right">
              <FacebookShareButton
                url={shareUrl}
                quote={'2 Pangra'}
                hashtag={'#2Pangra'}
              >
                <FacebookIcon size={25} round={true} />
              </FacebookShareButton>
            </Tooltip>
            <Tooltip overlay="Click to share" placement="right">
              <WhatsappShareButton
                url={shareUrl}
                quote={'2 Pangra'}
                hashtag={'#2Pangra'}
              >
                <WhatsappIcon size={25} round={true} />
              </WhatsappShareButton>
            </Tooltip>
          </div>
          <Link href={linkPath}>
            <a className="relative w-[200px] h-[200px] rounded-xl overflow-hidden before:transition-all before:absolute before:z-10 before:inset-0 before:bg-textColor before:bg-opacity-0 group-hover:before:bg-opacity-30">
              {imgPath?.substring(0, 4) === 'http' ? (
                <Image src={imgPath} alt="Img Desc" layout="fill" />
              ) : (
                <img src={imgPath} alt="image description" />
              )}
              {isSold ? (
                <div className="absolute flex justify-center items-center inset-0 bg-black bg-opacity-60 text-white font-semibold text-lg z-50 ">
                  <span>{isSold ? 'SOLD OUT' : ''}</span>
                </div>
              ) : (
                ''
              )}{' '}
            </a>
          </Link>
          {isVerified ? (
            <div className="verified inline-block text-[13px] px-3 py-[2px] w-auto bg-primary-dark text-white absolute top-2 left-2 rounded-r-md">
              <BadgeCheckIcon className="w-4 h-4 inline-block mt-[-2px] mr-1" />
              Verified
            </div>
          ) : null}
        </figure>
        <div className="detail-holder">
          <Link href={linkPath}>
            <a className="text-textColor hover:text-primary-dark inline-block !w-auto">
              <h5 className="inline-block">{name}</h5>
            </a>
          </Link>
          <div className="text-primary-dark font-semibold">
            <span>NRs{thousandNumberSeparator(subName)}</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Card;
