import { useState } from 'react';
import Button from 'components/Button';
import PropTypes from 'prop-types';

function ButtonTabs({ loading = false, btnArray, handleClick }) {
  const [activeBudgetBtn, setActiveBudgetBtn] = useState(0);

  return (
    <div>
      {btnArray.map((btn, index) => {
        return (
          <Button
            disabled={loading}
            type="button"
            variant={activeBudgetBtn === index ? 'tab-active' : 'tab'}
            onClick={() => {
              handleClick(btn?.queryValue);
              setActiveBudgetBtn(index);
            }}
            key={index}
          >
            {btn?.name}
          </Button>
        );
      })}
    </div>
  );
}

ButtonTabs.propTypes = {
  loading: PropTypes.bool,
  btnNameArray: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleClick: PropTypes.func.isRequired,
};

export default ButtonTabs;
