import React, { useRef, useEffect } from 'react';
import { Form, Formik } from 'formik';

import Button from 'components/Button';

import Input from 'components/Input';
import { useToasts } from 'react-toast-notifications';
import { useEmailSubscribeMutation } from 'services/api/emailSubscribe';
import { emailSchema } from 'validation/resetLink.validation';

const EmailNewsLetter = () => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [createEmailSubscribe, { isError, error, isSuccess, isLoading }] =
    useEmailSubscribeMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('Thank you for subscribing. We will be in touch with you.', {
        appearance: 'success',
      });
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          'You might have entered wrong email . Please try again later.',
        {
          appearance: 'error',
          autoDismiss: false,
        }
      );
    }
  }, [isError]);
  return (
    <>
      <Formik
        initialValues={{
          email: '',
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          createEmailSubscribe(values);
          setSubmitting(false);
        }}
        validationSchema={emailSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div>
              <div>
                <Input
                  required={false}
                  name="email"
                  type="text"
                  placeholder="Email"
                />
              </div>
              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty || isLoading}
                >
                  Submit
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EmailNewsLetter;
