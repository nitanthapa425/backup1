// import { MultiSelect } from 'react-multi-select-component';

// const CheckBoxMultiSelect = ({ onChange, value, options, }) => {
//   //   const colourStyles = {
//   //     placeholder: (defaultStyles) => {
//   //       return {
//   //         ...defaultStyles,
//   //         color: '#B9B9B9',
//   //       };
//   //     },
//   //   };
//   return (
//     <>
//       <MultiSelect
//         // key={Date.now()}
//         value={
//           options
//             ? options.filter((option) => value.includes(option.value))
//             : []
//         }
//         onChange={onChange}
//         options={options}
//         // styles={colourStyles}
//       />
//     </>
//   );
// };

// export default CheckBoxMultiSelect;

import SelectEl from 'react-select/creatable';
const CheckBoxMultiSelect = ({ onChange, value, options, placeholder }) => {
  return (
    <>
      <SelectEl
        options={options}
        value={
          options
            ? options.filter((option) => value?.includes(option.value))
            : []
        }
        onChange={onChange}
        // placeholder={placeholder}
        isMulti={true}
      />
    </>
  );
};

export default CheckBoxMultiSelect;
