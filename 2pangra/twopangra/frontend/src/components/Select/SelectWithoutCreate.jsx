import SelectEl from 'react-select';
import PropTypes from 'prop-types';
const SelectWithoutCreate = ({
  required,
  label,
  error,
  onChange,
  value,
  options,
  placeholder,
  loading,
  isDisabled,
  isMulti = false,
  menuPlacement = 'auto',
  onBlur,
}) => {
  const colourStyles = {
    placeholder: (defaultStyles) => {
      return {
        ...defaultStyles,
        color: '#B9B9B9',
      };
    },
  };
  return (
    <>
      <label htmlFor="">
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <SelectEl
        key={Date.now()}
        value={
          isMulti
            ? options
              ? options.filter((option) => value.includes(option.value))
              : []
            : options
            ? options.find((option) => option.value === value)
            : ''
        }
        onChange={onChange}
        options={options}
        placeholder={placeholder}
        isLoading={loading}
        isDisabled={isDisabled}
        isMulti={isMulti}
        className={error ? 'border-error placeholder-error' : ''}
        menuPlacement={menuPlacement}
        onBlur={onBlur}
        styles={colourStyles}
      />

      {error && <div className="text-error text-sm">{error}</div>}
    </>
  );
};

SelectWithoutCreate.prototype = {
  required: PropTypes.bool,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  options: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  placeholder: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  menuPlacement: PropTypes.string,
};

export default SelectWithoutCreate;
