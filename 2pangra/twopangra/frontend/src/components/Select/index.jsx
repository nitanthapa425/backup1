import { useField } from 'formik';

const Select = ({ label, required = false, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <label htmlFor={props.id || props.name}>
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <select
        {...field}
        {...props}
        className={
          meta.touched && meta.error ? 'border placeholder-primary-dark' : ''
        }
      />
      {meta.touched && meta.error ? (
        <div className=" text-error text-sm">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Select;
