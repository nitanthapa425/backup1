import { useField } from 'formik';

const Input = ({ label, required = true, disabled = false, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name}>
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <input
        {...field}
        {...props}
        className={
          meta.touched && meta.error
            ? 'border-primary-dark placeholder-primary-dark'
            : disabled
            ? 'disabled-input'
            : ''
        }
        disabled={disabled}
      />
      {meta.touched && meta.error && (
        <div className="text-error text-sm">{meta.error}</div>
      )}
    </>
  );
};

export default Input;
