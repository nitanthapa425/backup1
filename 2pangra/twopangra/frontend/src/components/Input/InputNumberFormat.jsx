import { useField } from 'formik';
import React from 'react';
import NumberFormat from 'react-number-format';

const InputNumberFormat = ({
  label,
  value,
  thousandSeparator,
  prefix,
  onChange,
  required = true,
  ...props
}) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <label>
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <NumberFormat
        {...field}
        {...props}
        value={value}
        // displayType="text"
        thousandSeparator={true}
        prefix={prefix}
        onChange={onChange}
      ></NumberFormat>

      {meta.touched && meta.error && (
        <div className="text-error text-sm">{meta.error}</div>
      )}
    </div>
  );
};

export default InputNumberFormat;
