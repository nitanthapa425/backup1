import { useField } from 'formik';

const Textarea = ({ label, required = true, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name}>
        {label}
        <span className="required">
          <span>{required && '*'}</span>
        </span>
      </label>
      <textarea
        {...field}
        {...props}
        className={
          meta.touched && meta.error ? 'border-error placeholder-error' : ''
        }
      />
      {meta.touched && meta.error && (
        <div className="text-error text-sm">{meta.error}</div>
      )}
    </>
  );
};

export default Textarea;
