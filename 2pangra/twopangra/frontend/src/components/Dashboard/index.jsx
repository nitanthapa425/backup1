import Image from 'next/image';
import Link from 'next/link';
import { useEffect } from 'react';
import {
  ViewGridIcon,
  TruckIcon,
  // CollectionIcon,
  UsersIcon,
  TagIcon,
  SupportIcon,
  CubeTransparentIcon,
  ChevronRightIcon,
  ShoppingCartIcon,
  ShoppingBagIcon,
  PresentationChartBarIcon,
  UserIcon,
  AtSymbolIcon,
  LocationMarkerIcon,
  CurrencyDollarIcon,
  GiftIcon,
  PencilAltIcon,
  FireIcon,
} from '@heroicons/react/outline';
import Header from 'components/Header/Header';
import BreadCrumb from 'components/BreadCrumb/breadCrumb';
import DashAccordion from './DashAccordion';
import { adminActions } from 'store/features/adminAuth/adminAuthSlice';
import { useToasts } from 'react-toast-notifications';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
// import { Scrollbars } from 'react-custom-scrollbars';
import { useLogoutSuperAdminMutation } from 'services/api/loginCheck';
import Notification from 'components/Notification/Notification';
import ReactModal from 'components/ReactModal/ReactModal';
import PasswordUpdateContainer from 'container/PasswordUpdate/PasswordUpdateContainer';

function Dashboard({ noHeader, children, noContainer, BreadCrumbList }) {
  const body = document.querySelector('body');
  const { addToast } = useToasts();
  const router = useRouter();
  const dispatch = useDispatch();
  const loginInfo = useSelector((state) => state.adminAuth);

  const hFunctions = () => {
    body.classList.toggle('sidebar-toggle');
  };

  const data = [
    {
      id: 1,
      exp: false,
      parentTitle: 'Vehicle',
      children: [
        {
          title: 'Add',
          link: '/vehicle/add',
        },
        {
          title: 'Vehicle List',
          link: '/vehicle/view',
        },
        {
          title: 'Draft List',
          link: '/vehicle/draft/view',
        },
        // {
        //   title: 'Selling List',
        //   link: '/seller/view',
        // },
        // {
        //   title: 'Hot Deals List',
        //   link: '/hot-deals/view',
        // },
      ],
    },

    {
      id: 2,
      exp: false,
      parentTitle: 'Brand',
      children: [
        {
          title: 'Add',
          link: '/vehicle/brand/add',
        },
        {
          title: 'List',
          link: '/vehicle/brand/view',
        },
      ],
    },
    {
      id: 3,
      exp: false,
      parentTitle: 'Model',
      children: [
        {
          title: 'Add',
          link: '/vehicle/model/add',
        },
        {
          title: 'List',
          link: '/vehicle/model/view',
        },
      ],
    },
    {
      id: 4,
      exp: false,
      parentTitle: 'Variant',
      children: [
        {
          title: 'Add',
          link: '/vehicle/submodel/add',
        },
        {
          title: 'List',
          link: '/vehicle/submodel/view',
        },
      ],
    },
    {
      id: 5,
      exp: false,
      parentTitle: 'Users',
      children: [
        {
          title: 'Add',
          link: '/signup',
        },
        {
          title: 'List',
          link: '/signup/view',
        },
        {
          title: 'Deleted Users',
          link: '/deleteduser/view',
        },
      ],
    },
    {
      id: 6,
      exp: false,
      parentTitle: 'Customers',
      children: [
        {
          title: 'List',
          link: '/customer/view',
        },
        {
          title: 'Deleted Customers',
          link: '/deletedCustomer/view',
        },
      ],
    },
    {
      id: 7,
      exp: false,
      parentTitle: 'Wish List',
      children: [
        {
          title: 'List',
          link: '/wish-list/view',
        },
      ],
    },
    {
      id: 8,
      exp: false,
      parentTitle: 'Cart',
      children: [
        {
          title: 'List',
          link: '/cart/view',
        },
      ],
    },
    {
      id: 9,
      exp: false,
      parentTitle: 'book List',
      children: [
        {
          title: 'List',
          link: '/book/view',
        },
      ],
    },
    {
      id: 10,
      exp: false,
      parentTitle: 'Log',
      children: [
        {
          title: 'List',
          link: '/log/view',
        },
      ],
    },
    {
      id: 11,
      exp: false,
      parentTitle: 'Location',
      children: [
        {
          title: 'Add',
          link: '/location',
        },
        {
          title: 'List',
          link: '/location/view',
        },
      ],
    },
    {
      id: 12,
      exp: false,
      parentTitle: 'Sell',
      children: [
        {
          title: 'Add',
          link: '/seller',
        },
        {
          title: 'List',
          link: '/seller/view',
        },
      ],
    },
    {
      id: 13,
      exp: false,
      parentTitle: 'Hot Deals',
      children: [
        {
          title: 'List',
          link: '/hot-deals/view',
        },
      ],
    },
    {
      id: 14,
      exp: false,
      parentTitle: 'Offers',
      children: [
        {
          title: 'List',
          link: '/offerPrice/view',
        },
      ],
    },
    {
      id: 15,
      exp: false,
      parentTitle: 'Comments',
      children: [
        {
          title: 'List',
          link: '/comment/view',
        },
      ],
    },
  ];

  const parentIcons = [
    <TruckIcon key={1} />,
    <TagIcon key={2} />,
    <SupportIcon key={3} />,
    <CubeTransparentIcon key={4} />,
    <UsersIcon key={5} />,
    <UserIcon key={6} />,
    <ShoppingBagIcon key={7} />,
    <ShoppingCartIcon key={8} />,
    <PresentationChartBarIcon key={9} />,
    <AtSymbolIcon key={10} />,
    <LocationMarkerIcon key={11} />,
    <CurrencyDollarIcon key={12} />,
    <FireIcon key={13} />,
    <GiftIcon key={14} />,
    <PencilAltIcon key={15} />,
  ];

  useEffect(() => {
    /*
        In the css class 'sidebar-toggle':
        In mobile, the class 'sidebar-toggle' in the body will open the sidebar.
        But in desktop, the class 'sidebar-toggle' in the body closes the sidebar.
        */
    if (window.innerWidth >= 768) {
      if (!body.classList.contains('sidebar-toggle')) {
        hFunctions();
      }
    } else {
      hFunctions();
    }
  }, []);

  // const handleLogout = () => {
  //   dispatch(adminActions.removeAdminToken());
  //   addToast('Logged Out successfully .', {
  //     appearance: 'success',
  //   });
  //   router.replace('/admin');
  // };
  const [LogoutSuperAdmin, { isError, error, isSuccess }] =
    useLogoutSuperAdminMutation();

  const handleLogout = () => {
    LogoutSuperAdmin();
  };

  useEffect(() => {
    if (isSuccess) {
      dispatch(adminActions.removeAdminToken());
      addToast('Logged Out successfully .', {
        appearance: 'success',
      });
      router.replace('/admin');
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      addToast(error.data.message || 'Cant Log Out.', {
        appearance: 'error',
      });
    }
  }, [isError]);

  return (
    <section className="wrapper-holder">
      <aside className="sidebar rounded-r-[10px]">
        <div className="logo-details">
          <Link href="#">
            <a className="menu opener move" onClick={() => hFunctions()}>
              <ChevronRightIcon className="db-sidebar-icon" />
            </a>
          </Link>
          <Link href="/vehicle/add">
            <a className="logo-holder">
              <span className="logo-icon">
                <Image
                  src="/images/logo-icon.svg"
                  alt="2Pangra"
                  width={35}
                  height={50}
                />
              </span>

              <span className="logo-text">
                <Image
                  src="/images/logo-text.svg"
                  alt="2Pangra"
                  width={110}
                  height={50}
                />
              </span>
            </a>
          </Link>
        </div>
        {/* <Scrollbars className="sidebar-scroll-wrap"> */}
        <ul className="sidebar-menu">
          <li>
            <Link href="/vehicle/add">
              <a>
                <ViewGridIcon className="db-sidebar-icon" />
                <span className="link-name">Dashboard</span>
              </a>
            </Link>
          </li>
          <DashAccordion data={data} parentIcons={parentIcons} />
        </ul>
        {/* </Scrollbars> */}
        <div className="a-profile">
          {/* {console.log(loginInfo?.user)} */}
          <div className="image-holder">
            {/* <img src="images/bike-racing.jpg" alt="" /> */}
            {loginInfo?.user?.profileImagePath?.imageUrl ? (
              <img
                width="100%"
                height="100%"
                src={`${loginInfo?.user?.profileImagePath?.imageUrl}`}
              ></img>
            ) : (
              // </div>
              <a href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-8 w-8"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="#231F20"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}
                    d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
              </a>
            )}
          </div>
          <div className="detail-holder">
            <div className="name-post">
              <span className="name">{loginInfo?.user?.userName}</span>
              <span className="post">{loginInfo?.user?.accessLevel}</span>
            </div>
          </div>
          <ul className="a-profile-dropdown py-[15px] px-[10px] ">
            <li>
              {' '}
              <Link href={`/my-profile/get`}>
                <a className="text-textColor hover:text-primary py-[4px] px-4">
                  My Profile
                </a>
              </Link>
            </li>

            <li>
              {/* <Link href="/password-update">
                <a className="text-textColor hover:text-primary px-4 py-[7px]">
                  Update Password
                </a>
              </Link> */}
              <ReactModal link="Update Password">
                <PasswordUpdateContainer />
              </ReactModal>
            </li>
            <li>
              <span
                className="text-textColor block hover:text-primary cursor-pointer px-[12px] py-[4px]"
                onClick={handleLogout}
              >
                Log Out
              </span>
            </li>
          </ul>
        </div>
      </aside>
      <div className="content-wrapper overflow-hidden">
        {!noHeader ? <Header /> : null}
        <div className="container">
          <ul className="admin-notification hidden md:inline-block absolute right-[60px] top-[18px] ">
            <Notification type="admin"></Notification>
          </ul>
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div>
        <div className={noContainer ? '' : 'container'}>{children}</div>
      </div>
    </section>
  );
}

export default Dashboard;
