import PropTypes from 'prop-types';
import { firstLetterCapital } from 'utils/firstLetterCapita.';

function FilterTags({
  tags,
  notShowIdsOf = ['main'],
  omitId = ['isApproved'],
  vehTypeSelectRef,
  setVehicleFilter,
  setColored,
  setColorSelectClear,
  vehNameEl,
  brandSelectRef,
  setBrandId,
  setConditionSelectClear,
  setCondition,
  setBikeDrivenRange,
  setBikeDriven,
  setExpectedPriceRange,
  setExpectedPrice,
  setMileageRange,
  setMileage,
  districtSelectRef,
  setDistrict,
  setFuelTypeSelectClear,
  setFuelType,
  setSelectedMakeYear,
}) {
  const tagId = (tagName) => {
    if (tagName === 'vehicleType') {
      return 'Vehicle Type';
    }
    if (tagName === 'vehicleName') {
      return 'Vehicle Name';
    }

    if (tagName === 'color') {
      return 'Color';
    }
    if (tagName === 'brandName') {
      return 'Brand Name';
    }
    if (tagName === 'condition') {
      return 'Condition';
    }
    if (tagName === 'bikeDriven') {
      return 'Bike Driven';
    }
    if (tagName === 'expectedPrice') {
      return 'Expected Price';
    }

    if (tagName === 'mileage') {
      return 'Mileage';
    }

    if (tagName === 'fuelType') {
      return 'Fuel Type';
    }
    if (tagName === 'district') {
      return 'District';
    }
    if (tagName === 'makeYear') {
      return 'Make Year';
    }
    return tagName;
  };
  const handleClear = (tagName) => {
    if (tagName === 'vehicleType') vehTypeSelectRef.current.clearValue();
    if (tagName === 'vehicleName') {
      setVehicleFilter({
        id: 'vehicleName',
        value: '',
      });

      vehNameEl.current.value = '';
    }

    if (tagName === 'color') {
      setColorSelectClear(true);
      setColored({
        id: 'color',
        value: '',
      });
    }
    if (tagName === 'brandName') {
      brandSelectRef.current.clearValue();
      setBrandId({
        id: 'brandName',
        value: '',
      });
    }
    if (tagName === 'condition') {
      setConditionSelectClear(true);

      setCondition({
        id: 'condition',
        value: '',
      });
    }
    if (tagName === 'bikeDriven') {
      setBikeDrivenRange([0, 1000000]);
      setBikeDriven({
        id: 'bikeDriven',
        value: '',
      });
    }
    if (tagName === 'expectedPrice') {
      setExpectedPriceRange([0, 10000000]);

      setExpectedPrice({
        id: 'expectedPrice',
        value: '',
      });
    }

    if (tagName === 'mileage') {
      setMileageRange([0, 100]);

      setMileage({
        id: 'mileage',
        value: '',
      });
    }

    if (tagName === 'fuelType') {
      setFuelTypeSelectClear(true);
      setFuelType({
        id: 'fuelType',
        value: '',
      });
    }
    if (tagName === 'district') {
      districtSelectRef.current.clearValue();
      setDistrict({
        id: 'location.district',
        value: '',
      });
    }
    if (tagName === 'makeYear') {
      setSelectedMakeYear(null);
    }
  };
  return (
    <>
      {tags.length
        ? // We will filter and not render those tags whose id matches with any of the values of omitId array.
          tags
            .filter((tag) => !omitId.includes(tag?.id))
            .filter((tag) => tag.id !== 'status')
            .map((tag, i) => {
              if (tag.id && tag.value) {
                return (
                  <div
                    key={i}
                    className="relative group inline-flex items-center justify-center mr-2 mb-1 px-2 py-1 text-sm leading-none text-gray-500 bg-white border border-primary-light rounded-full"
                  >
                    <span className="">
                      {/* If in tags prop we have any object that has either of the values of notShowIdsOf, then we will not show their id in render. */}
                      {notShowIdsOf.includes(tag?.id) ? (
                        <>{tag?.value}</>
                      ) : (
                        <>
                          {firstLetterCapital(tagId(tag?.id)) +
                            ': ' +
                            tag?.value}
                        </>
                      )}
                    </span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-5 w-5 absolute -top-3 right-0 opacity-0 text-error group-hover:opacity-100"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                      onClick={() => {
                        handleClear(tag.id);
                      }}
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                );
              } else {
                return null;
              }
            })
        : null}
    </>
  );
}

FilterTags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.object).isRequired,
  notShowIdsOf: PropTypes.arrayOf(PropTypes.string),
  omitId: PropTypes.arrayOf(PropTypes.string),
};

export default FilterTags;
