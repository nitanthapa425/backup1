import Table from 'components/Table/table';
import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from 'react';
import { getQueryStringForTable } from 'utils/getQueryStringForTable';
import { useDeleteModelMutation } from 'services/api/model';
import { useGetAllCommentQuery } from 'services/api/seller';

function ViewCommentContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [vehicleDataQuery, setVehicleDataQuery] = useState('');
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: vehicleData,
    isError: vehicleError,
    isFetching: isLoadingVehicle,
  } = useGetAllCommentQuery(vehicleDataQuery);

  const {
    data: modelDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useGetAllCommentQuery('?&sortBy=createdAt&sortOrder=-1', {
    skip: skipTableDataAll,
  });
  useEffect(() => {
    if (modelDataAll) {
      setTableDataAll(
        modelDataAll.docs.map((comment) => {
          return {
            id: comment.id,
            vehicleName: comment.vehicleName,
            name: comment.name,
            message: comment?.message,
            // replyComment: {
            //   fullName: comment?.replyComment?.fullName,
            //   id: comment?.replyComment?.id,

            //   replyMessage: comment?.replyComment?.replyMessage,
            // },
          };
        })
      );
    }
  }, [modelDataAll]);
  // const {deleteInfo = useDeleteModelMutation();
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteModelMutation();

  const columns = useMemo(
    () => [
      {
        id: 'vehicleName',
        Header: 'Vehicle Name',
        accessor: 'vehicleName',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'name',
        Header: 'CommentBy',
        accessor: 'name',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: 'message',
        Header: 'comment',
        accessor: 'message',
        Cell: ({ cell: { value } }) => value || '-',
        canBeSorted: true,
        canBeFiltered: true,
      },
      //   {
      //     id: 'fullName',
      //     Header: 'RepliedBy',
      //     accessor: 'fullName',
      //     Cell: ({ cell: { value } }) => value || '-',
      //     canBeSorted: true,
      //     canBeFiltered: true,
      //   },
      //   {
      //     id: 'replyMessage',
      //     Header: 'ReplyMessage',
      //     accessor: 'replyMessage',
      //     Cell: ({ cell: { value } }) => value || '-',
      //     canBeSorted: true,
      //     canBeFiltered: true,
      //   },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setVehicleDataQuery(query);
  };

  useEffect(() => {
    if (vehicleData) {
      setPageCount(vehicleData.totalPages);
      setTotalData(vehicleData.totalDocs);
      setTableData(
        vehicleData.docs.map((comment) => {
          return {
            id: comment.id,
            vehicleName: comment.vehicleName,
            name: comment.name,
            message: comment?.message,
            // replyComment: {
            //   fullName: comment?.replyComment?.fullName,
            //   id: comment?.replyComment?.id,

            //   replyMessage: comment?.replyComment?.replyMessage,
            // },
          };
        })
      );
    }
  }, [vehicleData]);

  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Model List',
      route: '/vehicle/model/view',
    },
  ];

  return (
    <AdminLayout documentTitle="View Comments" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb> */}

          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Comment/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={vehicleError}
            isLoadingData={isLoadingVehicle}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="vehicle/model/edit"
            // viewRoute="vehicle/model/get"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            // addPage={{ page: 'Add Model', route: '/vehicle/model/add' }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewCommentContainer;
