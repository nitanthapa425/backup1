import React, { useRef, useEffect, useState } from 'react';
import { Form, Formik } from 'formik';

import Button from 'components/Button';

// import Input from 'components/Input';
import { useToasts } from 'react-toast-notifications';
// import {
//   useUpdateCommentMutation,
//   useGetSingleCommentQuery,
// } from 'services/api/comment';
import { messageSchema } from 'validation/resetLink.validation';
// import UserLayout from 'layouts/User';
// import router from 'next/router';

import Textarea from 'components/Input/textarea';
import {
  useEditReplyCommentMutation,
  useGetSingleReplyOfCommentQuery,
} from 'services/api/seller';

const EditReplyComment = (props) => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [EditReplyCommentDetails, setEditReplyCommentDetails] = useState({});

  const [updateComment, { isError, error, isSuccess, isLoading }] =
    useEditReplyCommentMutation();

  const {
    data: singleCommentData,

    // isLoading: fetchingSubModel,
    // isError: subModelError,
  } = useGetSingleReplyOfCommentQuery({
    commentId: props.commentId,
    replyId: props.replyId,
  });

  useEffect(() => {
    if (singleCommentData) {
      const newCommentDetails = {
        replyMessage: singleCommentData.replyDetails[0].replyMessage,
      };

      setEditReplyCommentDetails(newCommentDetails);
    }
  }, [singleCommentData]);
  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast('You have successfully edited a comment.', {
        appearance: 'success',
      });

      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
    if (isError) {
      addToast(
        error?.data?.replyMessage ||
          'We are not able to edit your comment . Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <>
      {/* <div className="hidden bg-white bg-opacity-75 lg:block border border-gray-300 rounded-md p-5 mb-4 shadow-lg"> */}
      <h3 className="h5 mb-2">Update Reply Comment </h3>
      <Formik
        initialValues={EditReplyCommentDetails}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          updateComment({
            commentId: props.commentId,
            replyId: props.replyId,
            body: values,
          });
          setSubmitting(false);
        }}
        validationSchema={messageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div>
              <div>
                <Textarea
                  required={false}
                  name="replyMessage"
                  type="text"
                  placeholder="E.g. What is the price of bike?"
                />
              </div>

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Update
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
      {/* </div> */}
    </>
  );
};

export default EditReplyComment;
