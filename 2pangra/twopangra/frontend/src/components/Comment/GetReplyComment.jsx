import {
  ChevronDownIcon,
  ChevronUpIcon,
  DotsVerticalIcon,
} from '@heroicons/react/outline';
import Popup from 'components/Popup';
import ReactModal from 'components/ReactModal/ReactModal';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import {
  useDeleteReplyCommentMutation,
  useGetReplyOfCommentQuery,
} from 'services/api/seller';
import { agodate } from 'utils/datedifference';
import EditReplyComment from './editReplyComment';
import ReplyComment from './reply';

const ShowHideView = ({ allData = [], showReply, setShowReply }) => {
  if (allData.length) {
    // console.log('allData', allData);
    if (showReply) {
      return (
        <div className="text-[#0a62d5] font-bold hover:cursor-pointer">
          <span
            onClick={() => {
              setShowReply(false);
            }}
          >
            <ChevronUpIcon className="h-4 w-4 inline-block mr-2 mb-[3px] "></ChevronUpIcon>
            Hide {allData.length} replies
          </span>
        </div>
      );
    }
    return (
      <div className="text-[#0a62d5] font-bold hover:cursor-pointer">
        <span
          onClick={() => {
            setShowReply(true);
          }}
        >
          <ChevronDownIcon className="h-4 w-4 inline-block mr-2 mb-[3px]"></ChevronDownIcon>
          View {allData.length} replies
        </span>
      </div>
    );
  } else return <></>;
};

const GetReplyComment = ({ commentId }) => {
  const {
    data: getReplyOfComment,
    // isLoading: fetchingSubModel,
    // isError: subModelError,
  } = useGetReplyOfCommentQuery(commentId, {
    // skip: commentId,
  });

  const { addToast } = useToasts();

  const [editDeleteCommentId, setEditDeleteCommentId] = useState('');

  //   console.log('getReply', getReplyOfComment);

  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo.token;

  const [modal, setModal] = useState(false);
  const [showReply, setShowReply] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  const [openModel, setOpenModel] = useState(false);

  const [
    deleteComment,
    {
      isLoading: isLoadingDeleteComment,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteReplyCommentMutation();

  useEffect(() => {
    if (isDeleteSuccess) {
      //   formikBag.current?.resetForm();
      addToast('You have successfully deleted a comment.', {
        appearance: 'success',
      });
      setOpenModel(false);
    }
    if (isDeleteError) {
      addToast(
        DeletedError?.data?.message ||
          'We are not able to delete your comment . Please try again later.',
        {
          appearance: 'error',
        }
      );
    }
  }, [isDeleteSuccess, isDeleteError]);
  return (
    <div>
      {openModel && (
        <Popup
          title="Are you sure you want to delete comment?"
          description="Please note that, once deleted, this cannot be undone."
          onOkClick={() => {
            deleteComment({
              commentId: commentId,
              replyId: editDeleteCommentId,
            });
            //   deleteComment(editDeleteCommentId);
          }}
          onCancelClick={() => setOpenModel(false)}
          okText="Yes"
          cancelText="No"
          loading={isLoadingDeleteComment}
        />
      )}

      <ShowHideView
        allData={getReplyOfComment}
        showReply={showReply}
        setShowReply={setShowReply}
      ></ShowHideView>

      <div className="mt-4">
        {showReply &&
          getReplyOfComment.map((rep, replyIndex) => {
            return (
              <div key={replyIndex} className="relative">
                {loginInfo?.customer?._id === rep?.createdBy.id ? (
                  <div>
                    <div className="absolute right-0 top-[13px] group hover:cursor-pointer">
                      <DotsVerticalIcon className="h-4" />
                      <div className="px-4 py-3 rounded-lg bg-white border-[1px] border-gray-200 shadow-lg w-[110px] absolute top-5 right-0 opacity-0 z-10 invisible group-hover:opacity-100 group-hover:visible transition-all">
                        <div
                          className="mb-1 hover:text-primary-dark"
                          onClick={() => {
                            setEditDeleteCommentId(rep._id);
                            setOpenModel(true);
                          }}
                        >
                          <span>Delete</span>
                        </div>
                        <ReactModal
                          link="Edit"
                          className="comment-edit p-0"
                          modal={modal}
                        >
                          <EditReplyComment
                            commentId={commentId}
                            replyId={rep._id}
                            setModalFunction={setModalFunction}
                            modal={modal}
                          ></EditReplyComment>
                        </ReactModal>
                      </div>
                    </div>
                    <div className="flex">
                      <div className="profile-image-holder w-[60px]">
                        {/* <span className="w-[45px] h-[45px] rounded-full bg-gray-300 inline-block"></span> */}
                        {rep?.createdBy?.profileImagePath?.imageUrl ? (
                          <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                            <img
                              width="100%"
                              height="100%"
                              src={`${rep?.createdBy?.profileImagePath?.imageUrl}`}
                            ></img>
                          </div>
                        ) : (
                          <a>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              className="h-8 w-7"
                              fill="none"
                              viewBox="0 0 24 24"
                              stroke="#231F20"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={1}
                                d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                              />
                            </svg>
                          </a>
                        )}
                      </div>
                      <div className="flex-1">
                        <h6>
                          {rep?.fullName ? rep?.fullName : 'Not available'}{' '}
                        </h6>
                        <p className="mb-1">
                          {rep?.replyMessage
                            ? rep?.replyMessage
                            : 'Not available'}
                        </p>
                        <div className="flex space-x-3 items-center">
                          <span className="text-[12px] text-gray-500 font-normal inline-block   relative">
                            {agodate(new Date(rep?.createdAt))}
                          </span>
                          {/* <a
                                  className="text-[12px] text-gray-500 hover:text-primary-dark"
                                  href="#"
                                >
                                  Reply
                                </a> */}

                          {token && (
                            <ReactModal
                              link="reply"
                              className="comment-edit p-0"
                              modal={modal}
                            >
                              <ReplyComment
                                id={commentId}
                                setModalFunction={setModalFunction}
                                modal={modal}
                              ></ReplyComment>
                            </ReactModal>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default GetReplyComment;
