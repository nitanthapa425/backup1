import React from 'react';
import Head from 'next/head';

function layout({ children }) {
  return (
    <>
      <Head>
        {/* Importing Nunito font from Google Fonts */}
        {/* <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
                <link
                    href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&display=swap"
                    rel="stylesheet"
                /> */}
      </Head>
      <header>{/* Markup */}</header>
      <main>{children}</main>
      <footer>{/* Markup */}</footer>
    </>
  );
}

export default layout;
