import React, { useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Form, Formik } from 'formik';
import AdminLayout from 'layouts/Admin';

import { useToasts } from 'react-toast-notifications';
import {
  useReadMyProfileDetailsQuery,
  useReadUserProfileDetailsQuery,
  useSignUpPostMutation,
  useUpdateUserProfileDetailsMutation,
  useUpdateMyProfileDetailsMutation,
} from 'services/api/signup';

import Input from 'components/Input';
import Select from '../Select/index';

import Button from 'components/Button';
import {
  SignUpValidationSchema,
  UpdateMyValidationSchema,
  UpdateSignUpValidationSchema,
} from 'validation/signup.validation';

import Popup from 'components/Popup';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
// import PasswordInput from 'components/Input/PasswordInput';
import { useRouter } from 'next/router';
import DropZone from 'components/DropZone';
import { beforeAfterDate } from 'utils/beforeAfterdate';

import Link from 'next/link';
// import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
const SignUpComponent = ({ type, editUser }) => {
  const router = useRouter();
  const [editUserInitialValue, setEditUserInitialValue] = useState({});
  const [editMyInitialValue, setEditMyInitialValue] = useState({});
  // const [changed, setChanged] = useState(false);
  // useWarnIfUnsavedChanges(changed);

  const accessLevels = ['ADMIN', 'SUPER_ADMIN', 'USER'];

  const genders = ['Male', 'Female', 'Other'];
  const formikBag = useRef();
  const { addToast } = useToasts();
  // const [createSignUp, { isError, isSuccess, isLoading, error }] =
  const [createSignUp, { isError, error: signupError, isSuccess, isLoading }] =
    useSignUpPostMutation();
  useSignUpPostMutation();

  const [openModal, setOpenModal] = useState(false);

  const {
    data: userProfile,
    // error: userProfileFetchError,
  } = useReadUserProfileDetailsQuery(router.query.id, {
    skip: type !== 'edit' || !router.query.id,
  });

  const {
    data: myProfile,
    // error: myProfileFetchError,
  } = useReadMyProfileDetailsQuery();

  useEffect(() => {
    if (myProfile) {
      const newUserDetails = {
        // userName: myProfile?.userName,
        // accessLevel: myProfile?.accessLevel,
        firstName: myProfile?.firstName,
        middleName: myProfile?.middleName,
        lastName: myProfile?.lastName,
        // email: myProfile?.email,
        mobile: myProfile?.mobile,
        dob: myProfile?.dob,
        gender: myProfile?.gender,
        profileImagePath: myProfile?.profileImagePath,
      };

      setEditMyInitialValue(newUserDetails);
    }
  }, [myProfile]);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        'The profile has been successfully created. Please verify your e-mail for confirmation.',
        {
          appearance: 'success',
        }
      );
      // setChanged(false);
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      // addToast(error?.data?.message || 'User/Email already exists.', {
      addToast(
        <>
          {signupError?.data?.message ? (
            <>
              <span>{signupError?.data?.message}</span>
              <Link href={`/deleteduser/view`}>
                <a className="text-md pl-2 font-semibold underline text-blue-700">
                  Check User
                </a>
              </Link>
            </>
          ) : (
            <span>User/Email already exists.</span>
          )}
        </>,
        {
          appearance: 'error',
          autoDismiss: false,
        }
      );
    }
  }, [isError]);

  const [
    updateSignUp,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      // data: updateSuccessData,
      error: updateError,
    },
  ] = useUpdateUserProfileDetailsMutation();

  const [
    updateMyProfile,
    {
      isLoading: updatingMyProfile,
      isError: isUpdateErrorMyProfile,
      isSuccess: isUpdateSuccessMyProfile,
      // data: updateSuccessDataMyProfile,
      error: updateErrorMyProfile,
    },
  ] = useUpdateMyProfileDetailsMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast(updateError?.data?.message || 'User/Email already exists.', {
        appearance: 'error',
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast('User updated successfully.', {
        appearance: 'success',
      });
      // setChanged(false);
      router.push(`/signup/get/${router.query.id}`);
    }
  }, [isUpdateSuccess]);
  useEffect(() => {
    if (updateErrorMyProfile) {
      addToast(updateError?.data?.message || 'User/Email already exists.', {
        appearance: 'error',
      });
    }
  }, [isUpdateErrorMyProfile]);

  useEffect(() => {
    if (isUpdateSuccessMyProfile) {
      formikBag.current?.resetForm();
      addToast('User updated successfully.', {
        appearance: 'success',
      });
      // setChanged(false);
      // redirect to model page
      // ..........redirect is left
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
      // router.push(`/vehicle/brand/get/${router.query.id}`);
      router.push(`/my-profile/get`);
    }
  }, [isUpdateSuccessMyProfile]);

  useEffect(() => {
    if (userProfile) {
      const newUserDetails = {
        // userName: userProfile?.userName,
        accessLevel: userProfile?.accessLevel,
        firstName: userProfile?.firstName,
        middleName: userProfile?.middleName,
        lastName: userProfile?.lastName,
        // email: userProfile?.email,
        mobile: userProfile?.mobile,
        dob: userProfile?.dob,
        gender: userProfile?.gender,
        profileImagePath: userProfile?.profileImagePath,
      };

      setEditUserInitialValue(newUserDetails);
    }
  }, [userProfile]);

  const userInitialValue = {
    userName: '',
    accessLevel: '',
    // password: '',
    // confirmPassword: '',
    firstName: '',
    middleName: '',
    lastName: '',
    email: '',
    mobile: '',
    dob: beforeAfterDate(new Date(), 0, 0, -16),
    gender: '',
  };

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue('profileImagePath', newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      // console.log(formikBag.current.values);
      formikBag?.current?.setFieldValue('profileImagePath', {});
    }
  };

  const updateUser = (payloadData, id) => {
    // if (!payloadData.profileImagePath) {
    //   delete payloadData.profileImagePath;
    // }
    // console.log(payloadData);
    updateSignUp({ ...payloadData, id: router.query.id });
  };

  // updateSignUp({ ...payloadData, id: router.query.id });
  // console.log(beforeAfterDate(new Date(), 0, 0, 0));
  const BreadCrumbList = [
    {
      routeName: 'Add vehicle',
      route: '/vehicle/add',
    },

    {
      routeName: 'Add User',
      route: '',
    },
  ];

  return (
    <AdminLayout
      documentTitle={type === 'add' ? 'Sign Up' : 'Edit user'}
      BreadCrumbList={BreadCrumbList}
    >
      <Formik
        initialValues={
          type === 'add'
            ? userInitialValue
            : editUser === 'userProfile'
            ? editUserInitialValue
            : editMyInitialValue
        }
        onSubmit={(values, { resetForm, setSubmitting }) => {
          let payloadData = {};
          if (type === 'add') {
            payloadData = {
              userName: values.userName,
              accessLevel: values.accessLevel,
              // password: values.password,
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              email: values.email,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
            };
            createSignUp(payloadData);
          } else if (editUser === 'userProfile') {
            payloadData = {
              // userName: values.userName,
              accessLevel: values.accessLevel,
              // password: values.password,
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              // email: values.email,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
              profileImagePath: values?.profileImagePath,
            };

            // createSignUp(payloadData);
            // updateSignUp({ ...payloadData, id: router.query.id });
            updateUser(payloadData, router.query.id);
          } else {
            payloadData = {
              // userName: values.userName,
              // accessLevel: values.accessLevel,
              // password: values.password,
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              // email: values.email,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
              profileImagePath: values?.profileImagePath,
            };

            updateMyProfile(payloadData);
          }
          setSubmitting(false);
        }}
        validationSchema={
          type === 'add'
            ? SignUpValidationSchema
            : editUser === 'userProfile'
            ? UpdateSignUpValidationSchema
            : UpdateMyValidationSchema
        }
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          dirty,
        }) => (
          <div className="container">
            {/* <button
              className="mb-3 hover:text-primary"
              onClick={() => router.back()}
            >
              Go Back
            </button> */}
            <h3 className="mb-3">
              {type === 'add' ? 'Add User' : 'Edit User'}
            </h3>

            <Form>
              <div className="row-sm  pt-5 pb-2">
                {type === 'add' && (
                  <div className="three-col-sm">
                    <Input
                      label="User Name"
                      name="userName"
                      type="text"
                      placeholder="E.g: john1"
                    />
                  </div>
                )}
                <div className="three-col-sm">
                  <Input
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder="E.g: John"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Middle Name"
                    name="middleName"
                    type="text"
                    placeholder="E.g: Jung"
                    required={false}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder="E.g: Deo"
                  />
                </div>
                {editUser !== 'myProfile' && (
                  <div className="three-col-sm">
                    <Select label="Access Level" name="accessLevel">
                      <option value="">Select Access Level</option>

                      {accessLevels.map((accessLevel, i) => (
                        <option key={i} value={accessLevel}>
                          {accessLevel}
                        </option>
                      ))}
                    </Select>
                  </div>
                )}
                {type === 'add' && (
                  <div className="three-col-sm">
                    <Input
                      label="Email"
                      name="email"
                      type="email"
                      placeholder="E.g: john320@gmail.com"
                    />
                  </div>
                )}

                <div className="three-col-sm">
                  <Input
                    label="Phone Number"
                    name="mobile"
                    type="text"
                    placeholder="E.g: 98XXXXXXXX"
                  />
                </div>
                <div className="three-col-sm">
                  <label htmlFor="">
                    Date Of Birth(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={values?.dob ? new Date(values?.dob) : null}
                    onChange={(date) => {
                      setFieldValue('dob', date?.toLocaleDateString());
                      setFieldTouched('dob');
                    }}
                    onBlur={() => {
                      setFieldTouched('dob');
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    maxDate={beforeAfterDate(new Date(), 0, 0, -16)}
                  />
                  <h6 className="text-gray-500 text-xs font-thin">
                    age must be greater than 16
                  </h6>

                  {touched.dob && errors.dob && (
                    <div className="text-error">{errors.dob}</div>
                  )}
                </div>
                <div className="three-col-sm">
                  <Select label="Gender" name="gender">
                    <option value="">Select Gender</option>

                    {genders.map((gender, i) => (
                      <option key={i} value={gender}>
                        {gender}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  {type === 'edit' && (
                    <div className="">
                      <div>
                        <DropZone
                          label="Upload Profile Image"
                          currentFiles={
                            Object.keys(values.profileImagePath || {}).length
                              ? [values.profileImagePath]
                              : []
                          }
                          setNewFiles={handleProfileImagePath}
                          handleRemoveFile={handleRemoveProfileImagePath}
                          error={
                            touched?.profileImagePath
                              ? errors?.profileImagePath
                              : ''
                          }
                        />
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  loading={isLoading || updating || updatingMyProfile}
                  disabled={!dirty}
                >
                  {type === 'add' ? 'Create' : 'Update'}
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      // setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </AdminLayout>
  );
};

SignUpComponent.prototype = {
  type: PropTypes.oneOf(['edit', 'add']),
};

export default SignUpComponent;
