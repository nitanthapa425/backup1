import { createSlice, createSelector } from '@reduxjs/toolkit';
import {
  getCustomerToken,
  setCustomerToken,
  removeCustomerToken,
  setCustomer,
  getCustomer,
} from 'config/customerStorage';
import { customerLogin } from 'services/api/customerLogin';

const initialState = { otpId: null, phone: null, customer: null, token: null };

const slice = createSlice({
  name: 'customerAuth',
  initialState,
  reducers: {
    setOtpId: (state, action) => {
      state.otpId = action.payload;
    },
    setPhone: (state, action) => {
      state.phone = action.payload;
    },
    setCustomerToken: (state) => {
      const token = getCustomerToken() || null;
      state.token = token;
    },
    removeCustomerToken: (state) => {
      removeCustomerToken();
      state.token = null;
      state.customer = null;
    },
    setCustomer: (state) => {
      const customer = getCustomer() || null;
      state.customer = JSON.parse(customer);
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      customerLogin.endpoints.loginCustomer.matchFulfilled,
      (state, { payload }) => {
        // set Token
        // console.log('payload data', payload);
        setCustomerToken(payload?.client?.accessToken);
        setCustomer(payload?.client);
        state.token = payload.client.accessToken;
        state.customer = payload.client;
      }
    );
  },
});

export default slice.reducer;
export const { actions: customerActions } = slice;

const selectDomain = (state) => state.customerAuth || slice.initialState;

// SELECTORS
export const selectCustomerToken = createSelector(
  [selectDomain],
  (customerState) => customerState.token
);
export const selectCustomer = createSelector(
  [selectDomain],
  (customerState) => customerState.customer
);
export const selectCustomerOtp = createSelector(
  [selectDomain],
  (customerState) => customerState.otpId
);
export const selectCustomerPhone = createSelector(
  [selectDomain],
  (customerState) => customerState.phone
);
