import { configureStore } from '@reduxjs/toolkit';
import adminAuthReducer from './features/adminAuth/adminAuthSlice';
import customerAuthReducer from './features/customerAuth/customerAuthSlice';
import levelReducer from './features/adminCustomerAuth/adminCustomerAuth';

import addVehicleUnitsReducer from './features/selectedUnits/addVehicleUnitsSlice';
import { adminLogin } from 'services/api/adminLogin';
import { customerLogin } from 'services/api/customerLogin';
import { vehicle } from 'services/api/vehicle';
import { brand } from 'services/api/brand';
import { filesUpload } from 'services/api/filesUpload';
import { model } from 'services/api/model';
import { subModel } from 'services/api/varient';
import { vehicleDraft } from 'services/api/vehicleDraft';
import { forgotPassword } from 'services/api/forgotPassword';
import { resetPassword } from 'services/api/resetPassword';
import { passwordUpdate } from 'services/api/passwordUpdate';
import { signup } from 'services/api/signup';
import { seller } from 'services/api/seller';
import { customer } from 'services/api/customer';
import { location } from 'services/api/location';
import { customerSelf } from 'services/api/customerSelf';
import { adminCart } from 'services/api/adminCartList';
import { loginCheck } from 'services/api/loginCheck';
import { log } from 'services/api/log';
import { hotDeal } from 'services/api/hotdeals';
import { emailSubscribe } from 'services/api/emailSubscribe';
import { notification } from 'services/api/notification';
import { getFullLocation } from 'services/api/getFullLocation';
// import { offerPrice } from 'services/api/offerPrice';
// import { Comment } from 'services/api/comment';

export const store = configureStore({
  reducer: {
    adminAuth: adminAuthReducer,
    customerAuth: customerAuthReducer,
    addVehicleUnits: addVehicleUnitsReducer,
    levelReducer: levelReducer,
    [adminLogin.reducerPath]: adminLogin.reducer,
    [customerLogin.reducerPath]: customerLogin.reducer,
    [customerSelf.reducerPath]: customerSelf.reducer,

    [vehicle.reducerPath]: vehicle.reducer,
    [brand.reducerPath]: brand.reducer,
    [filesUpload.reducerPath]: filesUpload.reducer,
    [model.reducerPath]: model.reducer,
    [subModel.reducerPath]: subModel.reducer,
    [vehicleDraft.reducerPath]: vehicleDraft.reducer,
    [forgotPassword.reducerPath]: forgotPassword.reducer,
    [resetPassword.reducerPath]: resetPassword.reducer,
    [passwordUpdate.reducerPath]: passwordUpdate.reducer,
    [signup.reducerPath]: signup.reducer,
    [seller.reducerPath]: seller.reducer,
    [customer.reducerPath]: customer.reducer,
    [location.reducerPath]: location.reducer,
    [adminCart.reducerPath]: adminCart.reducer,
    [loginCheck.reducerPath]: loginCheck.reducer,
    [log.reducerPath]: log.reducer,
    // [featuredBrand.reducerPath]: featuredBrand.reducer,
    [hotDeal.reducerPath]: hotDeal.reducer,
    [emailSubscribe.reducerPath]: emailSubscribe.reducer,
    [notification.reducerPath]: notification.reducer,
    [getFullLocation.reducerPath]: getFullLocation.reducer,
    // [offerPrice.reducerPath]: offerPrice.reducer,

    // [Comment.reducerPath]: Comment.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      adminLogin.middleware,
      customerLogin.middleware,
      vehicle.middleware,
      filesUpload.middleware,
      brand.middleware,
      model.middleware,
      subModel.middleware,
      vehicleDraft.middleware,
      forgotPassword.middleware,
      resetPassword.middleware,
      passwordUpdate.middleware,
      signup.middleware,
      seller.middleware,
      customer.middleware,
      location.middleware,
      customerSelf.middleware,
      adminCart.middleware,
      loginCheck.middleware,
      log.middleware,
      // featuredBrand.middleware,
      hotDeal.middleware,
      emailSubscribe.middleware,
      notification.middleware,
      getFullLocation.middleware,
      // offerPrice.middleware,
      // Comment.middleware,
    ]),
});
