/* eslint-disable react/display-name */
import { useEffect, useState } from 'react';
// import { getAdminToken } from 'config/adminStorage';
import { getCustomerToken } from 'config/customerStorage';
import { useRouter } from 'next/router';

const withCustomerLogin = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== 'undefined') {
      const [show, setShow] = useState(false);
      const router = useRouter();

      useEffect(() => {
        const accessToken = getCustomerToken();
        // if token is available land to home page
        if (accessToken) {
          router.push('/');
        } else {
          setShow(true);
        }
      }, []);

      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    return <div>Loading...</div>;
  };
};

export default withCustomerLogin;
