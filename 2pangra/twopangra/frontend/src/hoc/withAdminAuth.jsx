/* eslint-disable react/display-name */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { adminActions } from 'store/features/adminAuth/adminAuthSlice';
import { getAdminToken } from 'config/adminStorage';
// import { useIsLoginQuery } from 'services/api/loginCheck';
import axios from 'axios';
import { levelAction } from 'store/features/adminCustomerAuth/adminCustomerAuth';
import { baseUrl } from 'config';
// import { , useSelector } from 'react-redux';

const withAdminAuth = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== 'undefined') {
      // const [skipFetching, setSkipFetching] = useState(true);
      const [isError, setIsError] = useState(false);
      const [isSuccess, setIsSuccess] = useState(false);
      const loginInfo = useSelector((state) => state?.adminAuth?.token);

      const Router = useRouter();
      const dispatch = useDispatch();
      // const { isSuccess, isError } = useIsLoginQuery();

      const [show, setShow] = useState(false);

      useEffect(() => {
        if (loginInfo) {
          // console.log('loginINfo', loginInfo);
          axios(`${baseUrl}/validate-user`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${loginInfo}`,
            },
          })
            .then((res) => {
              setIsSuccess(true);
              setIsError(false);
            })
            .catch((e) => {
              setIsError(true);
              setIsSuccess(false);
            });
        }
      }, [loginInfo]);

      useEffect(() => {
        const accessToken = getAdminToken();
        // If there is no access token we redirect to admin login page.
        if (!accessToken) {
          Router.replace('/admin');
          return null;
        } else {
          // load admin token to store (it must be done here (if page is refrest))
          // check weather the token is expired
          // setting levelAction to 'superadmin' to give token of superadmin
          dispatch(adminActions.setAdminToken());
          dispatch(adminActions.setUser());

          dispatch(levelAction.setSuperAdminLevel());
          // setSkipFetching(false);
        }
      }, []);

      useEffect(() => {
        if (isError === true) {
          Router.push('/admin');
          // removing token and user from store and local storage
          dispatch(adminActions.removeAdminToken());
        }
      }, [isError]);
      useEffect(() => {
        if (isSuccess === true) {
          setShow(true);
        }
      }, [isSuccess]);

      // If this is an accessToken we just render the component that was passed with all its props
      return show ? <WrappedComponent {...props} /> : <div></div>;
    }

    // If we are on server, return null
    // when return null it was giving error that server and client have different div
    return <div></div>;
  };
};

export default withAdminAuth;
