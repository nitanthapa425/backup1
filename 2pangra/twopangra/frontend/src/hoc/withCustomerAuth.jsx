/* eslint-disable react/display-name */
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
// import { adminActions } from 'store/features/adminAuth/adminAuthSlice';
// import { getAdminToken } from 'config/adminStorage';
import { getCustomerToken } from 'config/customerStorage';
import { customerActions } from 'store/features/customerAuth/customerAuthSlice';

const withCustomerAuth = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== 'undefined') {
      const Router = useRouter();
      const dispatch = useDispatch();

      const [show, setShow] = useState(false);

      useEffect(() => {
        const accessToken = getCustomerToken();
        // If there is no access token we redirect to admin login page.
        if (!accessToken) {
          Router.replace('/customer/login');
          return null;
        } else {
          // if (Router.pathname === '/admin') {
          //   Router.replace('/vehicle/add');
          // }
          // load admin token to store
          dispatch(customerActions.setCustomerToken());
          dispatch(customerActions.setCustomer());
          setShow(true);
        }
      }, []);

      // If this is an accessToken we just render the component that was passed with all its props
      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    // If we are on server, return null
    // when return null it was giving error that server and client have different div
    return <div>Loading...</div>;
  };
};

export default withCustomerAuth;
