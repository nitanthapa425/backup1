import Head from 'next/head';
import withAdminAuth from 'hoc/withAdminAuth';
// import Header from 'components/Header/Header';
// import BreadCrumb from 'components/BreadCrumb/breadCrumb';
import Dashboard from 'components/Dashboard';
import PrivateRoutes from 'components/PrivateRoutes/PrivateRoutes';
// import PrivateRoutes from 'components/PrivateRoutes/PrivateRoutes';

const AdminLayout = ({
  documentTitle,
  BreadCrumbList,
  children,
  noHeader = false,
  noContainer = false,
  showFor = ['ADMIN', 'SUPER_ADMIN'],
}) => {
  return (
    <>
      <PrivateRoutes showFor={showFor}>
        <Head>
          <title>2Pangra | {documentTitle}</title>
        </Head>

        {/* <Header /> */}
        <Dashboard
          noHeader={noHeader}
          noContainer={noContainer}
          BreadCrumbList={BreadCrumbList}
        >
          {children}
        </Dashboard>
        {/* <div className="container">
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div> */}
        {/* {children} */}
      </PrivateRoutes>
    </>
  );
};

export default withAdminAuth(AdminLayout);
