export function removePrefixComma(prefix, numStr) {
  // removing Prefix
  const toBeRemove1 = prefix;
  const reg1 = new RegExp(toBeRemove1, 'g'); // g indicate replace all
  const numStrWithoutPrefix = numStr.replace(reg1, '');

  // removing all comma
  const toBeRemove = ',';
  const reg2 = new RegExp(toBeRemove, 'g');
  const num = numStrWithoutPrefix.replace(reg2, '');

  return num;
}

// console.log(removePrefixComma('NRs', 'NRs 10000,2222'));
