export function firstLetterCapital(string) {
  const firstCapital = string
    .toLowerCase()
    .split('')
    .map((letter, i) => {
      if (i === 0) return letter.toUpperCase();
      else return letter;
    })
    .join('');

  return firstCapital;
}
