// import * as converter from 'units-converter';
import convert from 'convert';

// Our default length unit in backend is mm.
export const lengthConverter = (value, fromUnit, toUnit) => {
  return convert(value, fromUnit).to(toUnit);
};

// Our default mass unit in backend is kg.
export const massConverter = (value, fromUnit, toUnit) => {
  return convert(value, fromUnit).to(toUnit);
};

// Our default length unit in backend is l.
export const volumeConverter = (value, fromUnit, toUnit) => {
  return convert(value, fromUnit).to(toUnit);
};
