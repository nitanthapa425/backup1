export function beforeAfterDate(input, days = 0, months = 0, years = 0) {
  // this function gives the data which is before or after date and
  // input is date  eg new Date() or  new Date(...any)
  // +1 means one month or day or year ahead
  // -1 means before
  return new Date(
    input.getFullYear() + years,
    input.getMonth() + months,
    Math.min(
      input.getDate() + days,
      new Date(
        input.getFullYear() + years,
        input.getMonth() + months + 1,
        0
      ).getDate()
    )
  );
}
