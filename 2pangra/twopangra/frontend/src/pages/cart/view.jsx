import AdminCart from 'container/AdminCart/AdminCart';
import React from 'react';
import AdminLayout from 'layouts/Admin';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Cart List',
      route: '',
    },
  ];
  return (
    <AdminLayout BreadCrumbList={BreadCrumbList} documentTitle="Cart List">
      <AdminCart />
    </AdminLayout>
  );
};

export default index;
