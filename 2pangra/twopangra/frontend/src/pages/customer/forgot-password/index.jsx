import CustomerForgotPasswordContainer from 'container/Customer/ForgotPassword';
import React from 'react';

const ForgotPassword = () => <CustomerForgotPasswordContainer />;

export default ForgotPassword;
