// import CreateSellerForm from 'container/Seller/CreateSeller';
import React from 'react';
import UserLayout from 'layouts/User';
import Footer from 'components/Footer';
import AddSellBike from 'container/Seller/Add';

const EditSeller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Selling Vehicle List ',
      route: '/sell/view',
    },
    {
      routeName: 'Edit Vehicle Sell ',
      route: '',
    },
  ];
  return (
    <UserLayout
      documentTitle="Edit vehicle sell"
      BreadCrumbList={BreadCrumbList}
    >
      {/* <CreateSellerForm type="edit" /> */}
      <AddSellBike type="edit"></AddSellBike>
      <Footer />
    </UserLayout>
  );
};
export default EditSeller;
