import SignUpComponent from 'components/SignUp';
// import AdminLayout from 'layouts/Admin';

const SignUp = () => {
  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add vehicle',
  //     route: '/vehicle/add',
  //   },

  //   {
  //     routeName: 'Add User',
  //     route: '',
  //   },
  // ];
  return (
    // <AdminLayout documentTitle="Add user" BreadCrumbList={BreadCrumbList}>
    //   <SignUpComponent type="add" />
    // </AdminLayout>
    <SignUpComponent type="add" />
  );
};

export default SignUp;
