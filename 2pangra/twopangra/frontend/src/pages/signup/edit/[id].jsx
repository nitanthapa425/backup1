import SignUpComponent from 'components/SignUp';
import React from 'react';

const SignUp = () => {
  return (
    <div>
      <SignUpComponent type="edit" editUser="userProfile" />
    </div>
  );
};

export default SignUp;
