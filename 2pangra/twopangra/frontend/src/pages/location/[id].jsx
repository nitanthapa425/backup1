import FullLocation from 'container/GetFullLocation';
import AdminLayout from 'layouts/Admin';

const LocationFull = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },

    {
      routeName: 'Edit Location',
      route: '',
    },
  ];
  return (
    <AdminLayout documentTitle="Edit Location" BreadCrumbList={BreadCrumbList}>
      <FullLocation type="edit" />;
    </AdminLayout>
  );
};

export default LocationFull;
