import AdminBook from 'container/AdminBook/viewAdminBook';
import React from 'react';
import AdminLayout from 'layouts/Admin';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Book List',
      route: '',
    },
  ];

  return (
    <AdminLayout BreadCrumbList={BreadCrumbList} documentTitle="Book List">
      <AdminBook />
    </AdminLayout>
  );
};

export default index;
