import AddToBookList from 'container/Customer/vehicleListingCom/BookList';
import React from 'react';
import UserLayout from 'layouts/User';
import Footer from 'components/Footer';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Booked List',
      route: '',
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Book List">
      <AddToBookList />
      <Footer />
    </UserLayout>
  );
};

export default index;
