import React from 'react';
import AdminLayout from 'layouts/Admin';
import AdminBookDetail from 'container/AdminBook/adminBookDetail';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Book List',
      route: '/book/view',
    },
    {
      routeName: 'Book Details',
      route: '',
    },
  ];

  return (
    <AdminLayout BreadCrumbList={BreadCrumbList} documentTitle="Book Details">
      <AdminBookDetail />
    </AdminLayout>
  );
};

export default index;
