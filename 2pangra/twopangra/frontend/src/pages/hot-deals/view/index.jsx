import AdminLayout from 'layouts/Admin';
import ViewHotDealsContainer from 'container/HotDeals/viewHotDealContainer';

const Seller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/',
    },
    {
      routeName: 'Hot Deals List ',
      route: '',
    },
  ];
  return (
    <div>
      <AdminLayout documentTitle="Hot Deals" BreadCrumbList={BreadCrumbList}>
        <ViewHotDealsContainer />
      </AdminLayout>
    </div>
  );
};

export default Seller;
