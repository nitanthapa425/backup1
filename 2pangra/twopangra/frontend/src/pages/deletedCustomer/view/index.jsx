import ViewDeletedCustomerContainer from 'container/Customer/DeletedCustomer';

const DeletedCustomerList = () => <ViewDeletedCustomerContainer />;

export default DeletedCustomerList;
