// import CreateSellerForm from 'container/Seller/CreateSeller';
import React from 'react';
import AdminLayout from 'layouts/Admin';
import AddAdminSellVehicle from 'container/AdminSell';

const EditSeller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Selling List ',
      route: '/seller/view',
    },
    {
      routeName: 'Edit',
      route: '',
    },
  ];
  return (
    <AdminLayout
      documentTitle="Edit sell vehicle"
      BreadCrumbList={BreadCrumbList}
    >
      {/* <CreateSellerForm type="edit" /> */}
      <AddAdminSellVehicle type="edit"></AddAdminSellVehicle>
    </AdminLayout>
  );
};
export default EditSeller;
