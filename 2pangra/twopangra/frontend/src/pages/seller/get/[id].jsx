import GetSingleSeller from 'container/Seller/getSingleSeller';
import React from 'react';
import AdminLayout from 'layouts/Admin';

const viewSingleSeller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/vehicle/add',
    },
    {
      routeName: 'Selling List ',
      route: '/seller/view',
    },
    {
      routeName: 'Details',
      route: '',
    },
  ];
  return (
    <AdminLayout
      documentTitle="Sell vehicle details"
      BreadCrumbList={BreadCrumbList}
    >
      <GetSingleSeller />;
    </AdminLayout>
  );
};

export default viewSingleSeller;
