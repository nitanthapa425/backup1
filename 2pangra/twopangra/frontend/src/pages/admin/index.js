import AdminLoginContainer from 'container/AdminLogin';
import AdminLogins from 'layouts/AdminLogin';

function AdminLogin() {
  return (
    <AdminLogins>
      <AdminLoginContainer />
    </AdminLogins>
  );
}

export default AdminLogin;
