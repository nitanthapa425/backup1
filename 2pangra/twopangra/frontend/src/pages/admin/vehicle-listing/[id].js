// import { useRouter } from 'next/router';
import SellDetailComponent from 'container/Customer/vehicleListingCom/sellDetailComponent';
import React from 'react';
import AdminLayout from 'layouts/Admin';
import Header from 'components/Header/Header';

const AdminSellDetail = () => {
  return (
    <div>
      <AdminLayout documentTitle="Get Selling Vehicle Information">
        <Header></Header>
        <SellDetailComponent></SellDetailComponent>
      </AdminLayout>
    </div>
  );
};

export default AdminSellDetail;
