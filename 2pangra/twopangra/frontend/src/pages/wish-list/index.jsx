import AddToWishList from 'container/Customer/vehicleListingCom/WishList';
import React from 'react';
import UserLayout from 'layouts/User';
import Footer from 'components/Footer';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'wish-list',
      route: '',
    },
  ];
  return (
    <UserLayout documentTitle={'Wish List'} BreadCrumbList={BreadCrumbList}>
      <AddToWishList />
      <Footer />
    </UserLayout>
  );
};

export default index;
