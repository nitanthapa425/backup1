import BrandContainer from 'container/Brand/BrandContainer';

import React from 'react';

const addBrand = () => {
  return (
    <>
      <BrandContainer type="add"></BrandContainer>
    </>
  );
};

export default addBrand;
