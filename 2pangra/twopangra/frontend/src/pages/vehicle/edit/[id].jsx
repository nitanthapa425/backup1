import AddVehiclePage from 'container/Vehicle/Add';

const EditVehicle = () => <AddVehiclePage type="edit" />;

export default EditVehicle;
