import ViewModelContainer from 'container/Model/ViewModelContainer';

function ViewModel() {
  return (
    <>
      <ViewModelContainer />;
    </>
  );
}

export default ViewModel;
