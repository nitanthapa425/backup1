import GetSingleModel from 'container/Model/GetSingleModel';
import React from 'react';

const GetSingleModelPage = () => {
  return (
    <div>
      <GetSingleModel></GetSingleModel>
    </div>
  );
};

export default GetSingleModelPage;
