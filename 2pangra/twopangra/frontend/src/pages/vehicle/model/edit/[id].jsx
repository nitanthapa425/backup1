import ModelContainer from 'container/Model/ModelContainer';
import React from 'react';

const EditModelDetails = () => {
  return (
    <div>
      <ModelContainer type="edit"></ModelContainer>
    </div>
  );
};

export default EditModelDetails;
