import ModelContainer from 'container/Model/ModelContainer';
import React from 'react';

const ModelPage = () => {
  return (
    <div>
      <ModelContainer type="add"></ModelContainer>
    </div>
  );
};

export default ModelPage;
