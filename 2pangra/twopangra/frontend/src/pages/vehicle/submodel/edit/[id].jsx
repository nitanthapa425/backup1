import VariantContainer from 'container/Varient/createVarient';
import React from 'react';

const EditVariantDetails = () => {
  return (
    <div>
      <VariantContainer type="edit"></VariantContainer>
    </div>
  );
};

export default EditVariantDetails;
