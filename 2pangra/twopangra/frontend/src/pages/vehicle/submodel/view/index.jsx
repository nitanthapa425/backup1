import ViewVariantContainer from 'container/Varient/viewVariantContainer';
import React from 'react';

const ViewVariant = () => {
  return (
    <div>
      <ViewVariantContainer />
    </div>
  );
};

export default ViewVariant;
