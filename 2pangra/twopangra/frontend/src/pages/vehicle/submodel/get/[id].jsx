import GetSingleVariant from 'container/Varient/getSingleVariant';
import React from 'react';

const GetSingleVariantPage = () => {
  return (
    <div>
      <GetSingleVariant></GetSingleVariant>
    </div>
  );
};

export default GetSingleVariantPage;
