import ViewDraftContainer from 'container/Vehicle/Draft/viewDraftContainer';
import React from 'react';

const DraftList = () => {
  return (
    <div>
      <ViewDraftContainer />
    </div>
  );
};

export default DraftList;
