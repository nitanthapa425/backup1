import AddVehiclePage from 'container/Vehicle/Add';

const EditDraftVehicle = () => <AddVehiclePage type="draft" />;

export default EditDraftVehicle;
