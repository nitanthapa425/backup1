import GetSingleBrand from 'container/Brand/GetSingleBrand';
import React from 'react';

const GetSingleBrandPage = () => {
  return (
    <div>
      <GetSingleBrand></GetSingleBrand>
    </div>
  );
};

export default GetSingleBrandPage;
