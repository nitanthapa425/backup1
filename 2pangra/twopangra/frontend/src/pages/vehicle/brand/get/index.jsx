import GetBrandDetails from 'container/Brand/GetBrandDetails';
import React from 'react';

const GetBrand = () => {
  return (
    <div>
      <GetBrandDetails></GetBrandDetails>
    </div>
  );
};

export default GetBrand;
