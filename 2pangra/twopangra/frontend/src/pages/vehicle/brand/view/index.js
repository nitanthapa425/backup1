import ViewBrandContainer from 'container/Brand/ViewBrandContainer';

function ViewBrand() {
  return (
    <>
      <ViewBrandContainer />
    </>
  );
}

export default ViewBrand;
