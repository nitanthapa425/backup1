import BrandContainer from 'container/Brand/BrandContainer';
import React from 'react';

const EditBrandDetails = () => {
  return (
    <div>
      <BrandContainer type="edit"></BrandContainer>
    </div>
  );
};

export default EditBrandDetails;
