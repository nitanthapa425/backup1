import VehicleDetailContainer from "container/Vehicle/VehicleDetail";

function VehicleDetail() {
    return (
        <>
            <VehicleDetailContainer />
        </>
    );
}

export default VehicleDetail;
