import ViewBikeContainer from 'container/Vehicle/View';

function ViewBike() {
  return (
    <>
      <ViewBikeContainer />;
    </>
  );
}

export default ViewBike;
