import ResetPasswordContainer from 'container/ResetPassword/ResetPasswordContainer';
import React from 'react';

const ResetPassword = () => {
  return (
    <div>
      <ResetPasswordContainer />
    </div>
  );
};

export default ResetPassword;
