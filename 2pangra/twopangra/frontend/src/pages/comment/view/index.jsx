import AdminLayout from 'layouts/Admin';
import ViewCommentContainer from 'components/Comment/viewCommentContainer';

const Comments = () => {
  const BreadCrumbList = [
    {
      routeName: 'Add Vehicle',
      route: '/',
    },
    {
      routeName: 'Comment List ',
      route: '',
    },
  ];
  return (
    <div>
      <AdminLayout documentTitle="Hot Deals" BreadCrumbList={BreadCrumbList}>
        <ViewCommentContainer />
      </AdminLayout>
    </div>
  );
};

export default Comments;
