import React from 'react';
import { ToastProvider } from 'react-toast-notifications';
import { Provider } from 'react-redux';

import '../../styles/globals.css';
import '../../styles/custom.css';
import { store } from '../store/store';
// import io from 'socket.io-client';
// import { baseUrl } from 'config';

function MyApp({ Component, pageProps }) {
  // const [socket, setSocket] = useState(null);

  // useEffect(() => {
  //   setSocket(io('http://localhost:3100/'));
  // }, []);

  // // console.log('socket', socket);

  // useEffect(() => {
  //   // socket?.on('getnotification', (data) => {
  //   //   // console.log(data.name);
  //   //   // console.log(data.action)
  //   // });

  //   socket?.on('Welcome', (data) => {
  //     // console.log('hello0000000000');
  //   });
  // }, [socket]);
  return (
    <Provider store={store}>
      <ToastProvider autoDismiss placement="top-center">
        <Component {...pageProps} />
      </ToastProvider>
    </Provider>
  );
}

export default MyApp;
