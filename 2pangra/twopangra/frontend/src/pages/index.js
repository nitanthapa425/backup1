import Homepage from 'container/Home';
import WebsiteUserLayout from 'layouts/WebsiteUser';

export default function Home() {
  return (
    <div>
      <WebsiteUserLayout documentTitle="Home">
        <Homepage />
      </WebsiteUserLayout>
    </div>
  );
}
