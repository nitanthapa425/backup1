// import { useRouter } from 'next/router';
import SellDetailComponent from 'container/Customer/vehicleListingCom/sellDetailComponent';
import React from 'react';
// import UserLayout from 'layouts/User';
// import UserHeader from 'components/Header/UserHeader';
import WebsiteUserLayout from 'layouts/WebsiteUser/';
import Footer from 'components/Footer';

const SellDetail = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'vehicle-list',
      route: '/vehicle-listing',
    },
    {
      routeName: 'Details',
      route: '',
    },
  ];
  //   const router = useRouter();

  //   console.log(router.query.id);
  return (
    <div>
      {/* <UserLayout documentTitle="Vehicle Listing"> */}
      {/* <UserHeader></UserHeader> */}
      <WebsiteUserLayout
        documentTitle="Selling Vehicle Details"
        BreadCrumbList={BreadCrumbList}
      >
        <SellDetailComponent />
        <Footer />
      </WebsiteUserLayout>
      {/* </UserLayout> */}
    </div>
  );
};

export default SellDetail;
