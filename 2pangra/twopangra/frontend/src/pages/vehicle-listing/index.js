import VehicleListingCom from 'container/Customer/vehicleListingCom/vehicleListingCom';
import React from 'react';
import WebsiteUserLayout from 'layouts/WebsiteUser/';
import Footer from 'components/Footer';

const index = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'vehicle-list',
      route: '',
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Vehicle Listing"
      BreadCrumbList={BreadCrumbList}
    >
      <VehicleListingCom />
      <Footer />
    </WebsiteUserLayout>
  );
};

export default index;
