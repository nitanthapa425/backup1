import BrandListing from 'container/Customer/BrandListing';
import WebsiteUserLayout from 'layouts/WebsiteUser/';

const BrandListingPage = () => {
  return (
    <WebsiteUserLayout documentTitle="Available Brands">
      <BrandListing />
    </WebsiteUserLayout>
  );
};

export default BrandListingPage;
