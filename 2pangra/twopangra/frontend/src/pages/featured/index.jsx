import AddToFeaturedBikeContainer from 'container/Customer/FeaturedQueue';

const FeaturedBike = () => <AddToFeaturedBikeContainer />;

export default FeaturedBike;
