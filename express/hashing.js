//    Password@123   => "$2b$10$S8HVPgXVhAYtkC2GpAFK6Ob93CAyrOefhVE66GmVltN7iJKDMyPGu"
// same text  has different hash code
import bcrypt from "bcrypt";

//*************generate has code */
// let password = "Password@123";
// let hashedPassword = await bcrypt.hash(password, 10);
// console.log(hashedPassword);

//*******************compare has code */

// let loginPassword = "Password@123";
// let isValidPassword = await bcrypt.compare(
//   loginPassword,
//   "$2b$10$S8HVPgXVhAYtkC2GpAFK6Ob93CAyrOefhVE66GmVltN7iJKDMyPGu"
// );
// console.log(isValidPassword);
