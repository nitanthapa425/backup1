import express, { json } from "express";
import traineesRouter from "./src/router/traineesRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongodb.js";
import studentRouter from "./src/router/studentRouter.js";
import productRouter from "./src/router/productRouter.js";
import userRouter from "./src/router/userRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import fileRouter from "./src/router/fileRouter.js";

// make express application
let expressApp = express();
expressApp.use(express.static("./public"));
// localhost:8000/model.jpg
//localhost:8000/ram/profileImage.jpg
expressApp.use(json()); // this code makes our system  to take json data , always place express.use(json()) just below expressApp
// attach port to that
connectToMongoDb();

// expressApp.use(
//   (req, res, next) => {
//     console.log("i am application middleware1");
//     next();
//   },
//   (req, res, next) => {
//     console.log("i am application middleware2");
//     next();
//   }
// );

expressApp.use("/trainees", traineesRouter); //localhost:8000/bikes
expressApp.use("/bikes", bikeRouter); //localhost:8000/bikes
expressApp.use("/students", studentRouter); //localhost:8000/bikes
expressApp.use("/products", productRouter); //localhost:8000/bikes
expressApp.use("/users", userRouter); //localhost:8000/bikes
expressApp.use("/reviews", reviewRouter); //localhost:8000/bikes
expressApp.use("/files", fileRouter); //localhost:8000/bikes

expressApp.listen(8000, () => {
  console.log("express application is listening at port 8000");
});

/* 


api  localhost:8000/files/single method :post
pass image =form data
save image to static folder(public) generate link
send link to postman(frontend)

controller
router
index

*/
