/* 

Student =[
{name:"nitan",age:29, isMarried:false},
{name:"ram",age:30, isMarried:true},
{name:"hari",age:31, isMarried:false},
];

we have to define structure of data
define array (model)
  name object
define object (Schema)

*/

import { Schema } from "mongoose";

let studentSchema = Schema({
  name: {
    type: String,
    required: [true, "name field is required"],
  },
  age: {
    type: Number,
    required: [true, "age field is required"],
  },
  isMarried: {
    type: Boolean,
    required: [true, "isMarried field is required"],
  },
});

export default studentSchema;
