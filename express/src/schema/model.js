/* 

Students =[
{name:"nitan",age:29, isMarried:false},
{name:"ram",age:30, isMarried:true},
{name:"hari",age:31, isMarried:false},
];

we have to define structure of data
define array (model)
  name object
define object (Schema)

*/

import { model } from "mongoose";
import studentSchema from "./studentSchema.js";
import teacherSchema from "./teacherSchema.js";
import productSchema from "./productSchema.js";
import userSchema from "./userSchema.js";
import reviewSchema from "./reviewSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);

/* 
variable name must be same as model name
model name must be of first letter capital
model name must be singular

*/

/* 

localhost:8000/students, post


*/
