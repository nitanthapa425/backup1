import { Schema } from "mongoose";

/* 

manipulation
trim
lowercase
uppercase
default


validation
required

*********for string
minLength
maxLength
********* fro number
min
max


*/

let userSchema = Schema({
  profileImage: {
    type: String,
    required: [true, "name field is required"],
  },
  name: {
    type: String,
    trim: true,
    // lowercase: true,
    // uppercase:true,
    // default: "kapil",
    required: [true, "name field is required"],
    minLength: [3, "name must be at least 3 characters"],
    maxLength: [30, "name must be at most 5 characters"],
    validate: (value) => {
      let isValidName = /^[a-zA-Z]+$/.test(value);

      if (isValidName) {
      } else {
        let error = new Error("only alphabet is allowed for name field.");
        throw error;
      }
    },
  },
  email: {
    type: String,
    required: [true, "email field is required"],
    unique: true,
    validate: (value) => {
      let isValidEmail =
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value);

      if (isValidEmail) {
      } else {
        let error = new Error("invalid email");
        throw error;
      }
    },
  },
  password: {
    type: String,
    required: [true, "password field is required"],
  },
  phoneNumber: {
    type: Number,
    required: [true, "phoneNumber field is required"],

    validate: (value) => {
      //  9848468999   => "9848468999"   => length
      let strNumber = String(value);
      let length = strNumber.length;

      if (length !== 10) {
        let error = new Error("Phone number must be exact 10 characters long");
        throw error;
      }
    },
  },
  roll: {
    type: Number,
    min: [10, "roll must be at least 10"],
    max: [500, "roll must be at most 10"],
  },
  isMarried: {
    type: Boolean,
  },
  spouseName: {
    type: String,
  },

  gender: {
    type: String,
  },
  dob: {
    type: Date,
  },
  location: {
    country: {
      type: String,
    },
    exactLocation: {
      type: String,
    },
  },
  favTeacher: [
    {
      type: String,
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],
});

export default userSchema;

/* 
{
    "email":"abc@gmail.com"
    "location": {
        "country": {
          type:String
        },
        "exactLocation": {
          type:String
        }
    },
    "favTeacher": [
        "a",
        "b",
        "c",
        "nitan"
    ],
    favTeacher:[
      {
        type:String
      }

    ]




  favSubject:[
    {
      bookName:{
        type:String
      },
      bookAuthor:{
        type:String
      }

    }

  ]


    "favSubject": [
        {
            "bookName": "javascript",
            "bookAuthor": "nitan"
        },
        {
            "bookName": "b",
            "bookAuthor": "b"
        }
    ]
}



2 digit

10
99


3 digit

100
999


10 digit
1000000000
9999999999




*/
