import { Router } from "express";

let traineesRouter = Router();

// let createTrainees = (req, res, next) => {
//   console.log("trainees created successfully.");
// };

let createTrainees = (a, b) => {
  return (req, res, next) => {
    console.log("trainees created successfully.");
  };
};

traineesRouter
  .route("/") //localhost:8000/trainees
  // .post(createTrainees)// use this format if you don't need to pass values
  .post(createTrainees(1, 2)) // use this format if you need to pass value
  .get((req, res, next) => {
    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

export default traineesRouter;

/* 
      url=localhost:8000/bikes,post at response "bike post"
			url=localhost:8000/bikes,get at response "bike get"
			url=localhost:8000/bikes,patch at response "bike up"
			url=localhost:8000/bikes,delete at response "bike delete"

*/
