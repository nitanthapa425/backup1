import { Router } from "express";
import { Student } from "../schema/model.js";
import {
  createStudentController,
  deleteStudentController,
  readSpecificStudentController,
  readStudentController,
  updateStudentController,
} from "../controller/studentController.js";

let studentRouter = Router();

studentRouter
  .route("/") //localhost:8000/students
  .post(createStudentController)
  .get(readStudentController);

studentRouter
  .route("/:id") //localhost:8000/student/1232341
  .get(readSpecificStudentController)
  .patch(updateStudentController)
  .delete(deleteStudentController);

export default studentRouter;

// trainees
//book
//teacher

/* 
localhost:8000/students , method= post 
localhost:8000/students , method= get




localhost:8000/students/65fc2b2f91bed7a061c221df , method= get
localhost:8000/students/65fc2b2f91bed7a061c221df , method= patch
localhost:8000/students/65fc2b2f91bed7a061c221df , method= delete


error handling (try-catch)
async await



 let result = Student.create(data) 
 let result = Student.find({})//[{},{}] 
 let result = Student.findById(id) //{}
 let result = Student.findByIdAndUpdate(req.params.id, req.body) //{}
 let result = Student.findByIdAndDelete(req.params.id) //{}



*/
