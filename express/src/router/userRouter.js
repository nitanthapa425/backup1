import { Router } from "express";
import {
  createUserController,
  deleteUserController,
  readSpecificUserController,
  readUserController,
  updateUserController,
} from "../controller/userController.js";

let userRouter = Router();

userRouter
  .route("/") //localhost:8000/users
  .post(createUserController)
  .get(readUserController);

userRouter
  .route("/:id") //localhost:8000/user/1232341
  .get(readSpecificUserController)
  .patch(updateUserController)
  .delete(deleteUserController);

export default userRouter;

// trainees
//book
//teacher

/* 
localhost:8000/users , method= post 
localhost:8000/users , method= get




localhost:8000/users/65fc2b2f91bed7a061c221df , method= get
localhost:8000/users/65fc2b2f91bed7a061c221df , method= patch
localhost:8000/users/65fc2b2f91bed7a061c221df , method= delete


error handling (try-catch)
async await



 let result = User.create(data) 
 let result = User.find({})//[{},{}] 
 let result = User.findById(id) //{}
 let result = User.findByIdAndUpdate(req.params.id, req.body) //{}
 let result = User.findByIdAndDelete(req.params.id) //{}



*/
