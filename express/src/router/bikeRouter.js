import { Router } from "express";

let bikeRouter = Router();

bikeRouter
  .route("/") //localhost:8000/bikes
  .post((req, res, next) => {
    console.log(req.body);
    res.json("bike post");
  })
  .get((req, res, next) => {
    console.log(req.query);
    /* 
    req.query is object
    every data of query is a string
    {
        name="nitan",
        age="30"
    }
    
    */

    res.json("bike get");
  })
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete(
    (req, res, next) => {
      res.json("a");
      console.log("i am middleware 1");
      let err1 = new Error("i am error");
      next(err1);
    },
    (req, res, next) => {
      console.log("i am middleware 2");
      let err1 = new Error("i am error2");
      next();
    },
    (err, req, res, next) => {
      console.log("i am middleware 3");
      console.log(err.message);
    }
  );

bikeRouter
  .route("/:id") //localhost:8000/bikes/any
  .post((req, res, next) => {
    console.log(req.params);
    /* 


    req.params gives dynamic route parameter in the form of object
    {
        id:1234

    }
    
    */
    res.json("learn dynamic route params");
  })
  .get((req, res, next) => {
    res.json("get dynamic route");
  });

bikeRouter
  .route("/a/:id1/name/:id2") //localhost:8000/bikes/a/any/name/any
  .post((req, res, next) => {
    console.log(req.params);
    res.json("hello");
  });

export default bikeRouter;
