import { Router } from "express";
import {
  createReviewController,
  deleteReviewController,
  readReviewController,
  readSpecificReviewController,
  updateReviewController,
} from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter
  .route("/") //localhost:8000/reviews
  .post(createReviewController)
  .get(readReviewController);

reviewRouter
  .route("/:id") //localhost:8000/review/1232341
  .get(readSpecificReviewController)
  .patch(updateReviewController)
  .delete(deleteReviewController);

export default reviewRouter;

// trainees
//book
//teacher

/* 
localhost:8000/reviews , method= post 
localhost:8000/reviews , method= get




localhost:8000/reviews/65fc2b2f91bed7a061c221df , method= get
localhost:8000/reviews/65fc2b2f91bed7a061c221df , method= patch
localhost:8000/reviews/65fc2b2f91bed7a061c221df , method= delete


error handling (try-catch)
async await



 let result = Review.create(data) 
 let result = Review.find({})//[{},{}] 
 let result = Review.findById(id) //{}
 let result = Review.findByIdAndUpdate(req.params.id, req.body) //{}
 let result = Review.findByIdAndDelete(req.params.id) //{}



*/
