import { Router } from "express";
import {
  createProductController,
  deleteProductController,
  readProductController,
  readSpecificProductController,
  updateProductController,
} from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/") //localhost:8000/products
  .post(createProductController)
  .get(readProductController);

productRouter
  .route("/:id") //localhost:8000/product/1232341
  .get(readSpecificProductController)
  .patch(updateProductController)
  .delete(deleteProductController);

export default productRouter;

// trainees
//book
//teacher

/* 
localhost:8000/products , method= post 
localhost:8000/products , method= get




localhost:8000/products/65fc2b2f91bed7a061c221df , method= get
localhost:8000/products/65fc2b2f91bed7a061c221df , method= patch
localhost:8000/products/65fc2b2f91bed7a061c221df , method= delete


error handling (try-catch)
async await



 let result = Product.create(data) 
 let result = Product.find({})//[{},{}] 
 let result = Product.findById(id) //{}
 let result = Product.findByIdAndUpdate(req.params.id, req.body) //{}
 let result = Product.findByIdAndDelete(req.params.id) //{}



*/
