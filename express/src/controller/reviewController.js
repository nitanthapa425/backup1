// import { Review } from "../schema/model.js";

import { Review } from "../schema/model.js";

export let createReviewController = async (req, res, next) => {
  let data = req.body; //{,age:30, isMarried:false}
  try {
    let result = await Review.create(data);
    res.json({
      success: true,
      message: "Review created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readReviewController = async (req, res, next) => {
  // get reviews from database

  try {
    let result = await Review.find({})
      .populate("productId", "name price")
      .populate("userId");
    res.json({
      success: true,
      message: "Review read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }

  // give data to postman
};

export let readSpecificReviewController = async (req, res, next) => {
  try {
    let result = await Review.findById(req.params.id);
    res.json({
      success: true,
      message: "Review read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateReviewController = async (req, res, next) => {
  try {
    let result = await Review.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.json({
      success: true,
      message: "Review update successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteReviewController = async (req, res, next) => {
  try {
    let result = await Review.findByIdAndDelete(req.params.id);

    res.json({
      success: true,
      message: "Review deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
