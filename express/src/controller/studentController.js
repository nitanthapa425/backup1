//crud

import { Student } from "../schema/model.js";

export let createStudentController = async (req, res, next) => {
  let data = req.body; //{,age:30, isMarried:false}
  try {
    let result = await Student.create(data);
    res.json({
      success: true,
      message: "Student created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readStudentController = async (req, res, next) => {
  // get products from database

  try {
    let result = await Student.find({});
    res.json({
      success: true,
      message: "Student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }

  // give data to postman
};

export let readSpecificStudentController = async (req, res, next) => {
  try {
    let result = await Student.findById(req.params.id);
    res.json({
      success: true,
      message: "Student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateStudentController = async (req, res, next) => {
  try {
    let result = await Student.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.json({
      success: true,
      message: "Student update successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteStudentController = async (req, res, next) => {
  try {
    let result = await Student.findByIdAndDelete(req.params.id);

    res.json({
      success: true,
      message: "Student deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
