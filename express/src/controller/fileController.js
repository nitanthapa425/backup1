export let handleSingleFileController = (req, res, next) => {
  let link = `http://localhost:8000/${req.file.filename}`;
  res.json({
    success: true,
    message: `file save successfully.`,
    result: link,
  });
};
export let handleMultipleFileController = (req, res, next) => {
  let link = req.files.map((value, i) => {
    return `http://localhost:8000/${value.filename}`;
  });
  res.json({
    success: true,
    message: `file save successfully.`,
    result: link,
  });
};


