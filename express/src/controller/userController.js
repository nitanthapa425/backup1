import { User } from "../schema/model.js";
import bcrypt from "bcrypt";
import { sendEmail } from "../utils/sendMail.js";

export let createUserController = async (req, res, next) => {
  let data = req.body; //{,age:30, isMarried:false}

  let password = data.password;
  let hashPassword = await bcrypt.hash(password, 10);
  data.password = hashPassword;

  try {
    let result = await User.create(data);

    await sendEmail({
      from: "Unique hello<uniquekc425@gmail.com>",
      to: [req.body.email],
      subject: "Registration",
      html: `
      You have successfully registered.
      `,
    });

    res.json({
      success: true,
      message: "User created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let readUserController = async (req, res, next) => {
  // get users from database

  try {
    // let result = await User.find({});
    // find output are in [{},{},{}]
    // let result = await User.find({ name: "nitan" });
    // let result = await User.find({ age:{$gt:25}});
    // let result = await User.find({ age: { $gte: 25 } });
    // let result = await User.find({ age: { $lt: 25 } });
    // let result = await User.find({ age: { $lte: 25 } });
    // let result = await User.find({ age: { $ne: 25 } });

    //select

    // let result = await User.find({}).select("name -_id");
    // find has control over object where as select has control over object property

    // let result = await User.find({}).select("name email -password");//invalid
    // let result = await User.find({}).select("name email -_id");//valid
    // let result = await User.find({}).select("-name -password -email roll");// it is invalid

    //do use - and +  simultaneously   (except for _id)

    res.json({
      success: true,
      message: "User read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }

  // give data to postman
};

export let readSpecificUserController = async (req, res, next) => {
  try {
    let result = await User.findById(req.params.id);
    res.json({
      success: true,
      message: "User read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let updateUserController = async (req, res, next) => {
  try {
    let result = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.json({
      success: true,
      message: "User update successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};
export let deleteUserController = async (req, res, next) => {
  try {
    let result = await User.findByIdAndDelete(req.params.id);

    res.json({
      success: true,
      message: "User deleted successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

/* 
searching  =>  find, select
sort   =>
paginagion  => limit and skip



*/
