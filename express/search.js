
// for normal searching
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

find({})
find({name:"nitan"})
find({name:"nitan",age:29})
find({age:27})
find({age:"27"})
find({age:22, isMarried:"false"})
// in searching type does not matter

find({age:25})
find({age:{$gt:25}})
find({age:{$gte:25}})
find({age:{$lt:25}})
find({age:{$lte:25}})
find({name:{$ne:"nitan"}})

// task 1
// find age between 15 to 17
.find({age:{$gte:15,$lte:17}})

// finding those whose name is nitan , ram , hari

.find({name:"nitan"})
.find({name:{$in:["nitan","ram","hari"]}})
.find({name:{$nin:["nitan","ram","hari"]}})

//for  regex searchin
[
   {name:"ni1t",age:29, isMarried:false},
    {name:"sand2inip",age:25, isMarried:false},
    {name:"ni",age:26, isMarried:true},
    {name:"ris3hav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendran",age:27, isMarried:false},
    {name:"Nitan",age:16, isMarried:false},
    {name:"nitanthapa",age:22, isMarried:false},
]

/* 

[
   {name:"nitan",age:29, isMarried:true},

]


*/

find({name:/nitan/})
find({name:/nitan/i})
find({name:/^ni/})// starts with
find({name:/ni$/})// ends with
.find({name:/^\w{5,}$/})// name whose length is greater than 5


  //find it determine which object to show or not
  //select
  //sort
  //limit
  //skip


  //for sorting
  [
    {name:"ac",age:29, isMarried:false},
    {name:"b",age:40, isMarried:false},
    {name:"ab",age:50, isMarried:false},
    {name:"ab",age:60, isMarried:false},
    {name:"c",age:40, isMarried:false},
  
]

//output
[
  {name:"c",age:40, isMarried:false},
  {name:"b",age:40, isMarried:false},
  {name:"ac",age:29, isMarried:false},
  {name:"ab",age:60, isMarried:false},
  {name:"ab",age:50, isMarried:false},





]


.find({}).sort("name age")
.find({}).sort("-name")
.find({}).sort("-name age")
.find({}).sort("-name -age")


//number sorting work properly unlike javascript
// find({}).sort("name")
// find({}).sort("-name")
// find({}).sort("name age")
// find({}).sort("name -age")

// //ascending sort  descending sort
// 

// find({}).sort("-name age")

// find({}).sort("age -name")


//skip
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

.find({}).skip(2)//it skips first n no of values



//limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]




//skip and limit
[
    {name:"nitan",age:29, isMarried:false},
    {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
    {name:"rishav",age:20, isMarried:false},
    {name:"nitan",age:29, isMarried:true},
    {name:"chhimi",age:15, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false},
    {name:"kriston",age:22, isMarried:false},
]

//output
[
 
  {name:"sandip",age:25, isMarried:false},
    {name:"nitan",age:26, isMarried:true},
   {name:"nitan",age:29, isMarried:true},
    {name:"narendra",age:27, isMarried:false},
    {name:"shidhant",age:16, isMarried:false}
 

]
find({name:/n/i}).limit("5").skip("1")// .find({}).skip("2").limit("5")
//what ever the order, mongoose follows following sequence
//find, sort, select, skip , limit








//this order works
//find , sort, select, skipt, limit






/* 
c  => validation
r  => search, sorting, pagination



*/


