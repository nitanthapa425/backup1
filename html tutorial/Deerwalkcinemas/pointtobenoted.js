/* 
href="#" means remains in same page 
jpg is image with background
png is the image without background (background is transparent)
svg is also image without background but it will not pixelated
(broken image term)
In vscode White text are visible in browser
https://validator.w3.org/   this website is used to check weather our html code is write or not
flexboxfroggy.com   this website is for flex

<a style="background:red;">
<img></img>
</a>
here the style at a will not work
because it contain img (which is just like inline-block)
if a inline contain block or inline-block then there is a chance that the style property will not work on inline element


selector rule
it is good to have maximum selecter as 3  eg div .nav .ul    but dont do div .nav .ul .a  (it has 4 selecter)

always try to give 
    height to the button
    height to the input 
        for input use dispaly:block
    height to the icon 


avatar or icon give height and width 

img
display:block
height:100%;
width:100%


always wrap image with div 
and give image with properyt
    display:block
    width:100%
    height:auto

for relative and absolute
make one parent element for two element
<div class="position:absolute;">
<div>...........</div>
<div class="absolute">...........</div>
</div>


color
black #000000 or use #000
white #ffffff or use #ffffff
you can give opacity to any color but you have to use rgba
background: #000;  (you can not give opacity)
background-color: rgba(0, 0, 0, 0.65); 

z-index means place before or after 
opacity means increase invisibility.


when you use font-size always give line-height


if you increase number of lines 
    difficult to read 
    increase file size
      if file size increase it will takes more time to host
      may increase loading time

inline element
does not support (100%) margin and padding ()\


cta button means call to action button


if you use 
display:inline-block  (it takes unnecessary space ...  of 4px)
you must use vertical-align:top



for calculation use calc and  to calulate % use 100%/4  dont do like (100/4)%
width: calc((100% / 4));


To make a circle
give height , width etc


for a give box
assume inside box is same as outer box
+ margin can reduce box size
- margin can increase box size


ctr + shift +m  so see the effect on different devices4


try to play with padding instead of margin
margin property is not supported in animation (transition)
    

for list
margin :0
list-type: none
padding: 0


for paragraph


getting line height  = ling-heightin/font-size  1.5 means 1.5em




Dont use px in point


//for testimonial

use blockquotes for testimonia description
use cite tage for testimonial  which is cited


we can use inlineblock inside inline

for copy write &copy  site toptal.com

use address tag for address
at figma
press alt and move mouse you will get the height width ...infos


it is good not to use heading in footer section


if you use position relativei for pareent use z-index as 0


BEM naming convention fro class
(Block Element Modifier)



<section class="blog">  
blog is a Block all class only belong to blog are called Block element
modifier are use to modify the content of any class
<div class="blog__heading"></div>
<div class="blog__content">
<div class="blog__img"></div>//note not do blog__content__ima   because blog__content is  not a block we give __ seperator just after the block
<div class="blog__title blog__title--red"></div>
blog__title--red are the classes to modify blog__title so it is called modifier
<button class="btn bth--primary"></button>
here btn--primary is the class to modify btn so it is called modifier


instead of giving margin top and margin button try to give margin button


</div>
</section>



validat menans 
checking weather the data is in correct formet or not


 novalidate will stop in build in validation
<form novalidate>
</form>


padding is a clickable area 
where as margin is not a clickable area


we have to change h1 and p for cross browser compatibality

margin will not support by animation or transiton

to check weather the propety is supported or no
use caniuse


in textarea
dont play with row and col (try to play with height  and width)

button priority is   
right to left


inline element are
label
span
button

always wrap div
for image
for input
if your are using relative=> absoute and give parent as z-index as 0



*/

/* 

0--------------320px----------------------768--------------992px--------------1024--------------1200px----------------------------1920px
extra small       standard mobile device          tablet           tablet+           desktop            modern large screen(my pc)
mobile devices                                                     desktop



0-319=> min-width->320px        =>             scrolling
320-575px -> phones
576-767 -> phone(standard)  (my device)
768-991=> tablets
992-1024=> desktop
1025-1199=>wide screen
1200px -all (large screen) my laptop



how to write css
mobile first means write code from mobile (it is good approach) 
desktop first means write code from desktop  


some of  media query 
@media(min-width:768px)and (orientation:ladscape){

}



@media.screen(min-width:1024){

}
@media.print(min-width:1024){

}

here note media is same as media.screen
and media.print is for print




@media(min-width:320){} standard mobile device
@media(min-width:768){} tablet
@media(min-width:992){}desktop
@media(min-width:1200){} mordern large screen 



responsive 
mobile 
phablet
tablets
old desktop     960px
wide screen


note always give class from parent to children
it means first write   style for parent then for children


for newsletter (it means send mail to the email which is no newsletter section)
newsletter section email does not needs (login)


flex-item width:100%  does not works for this use flex grow


we do not decrease font -size below 16 px even in mobile


We can not use before and after in form element eg input,textarea select ...

<p>before Contentofp after</p>
in tag we have invisible tag in before and after which is inline by default
before and after only works if you given any content  either it is empty  content=""

to hide content you can use
font-size:0
line-height:0   
*/
