/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const validate = require('../middlewares/validate');
const { createReviewAndRatingValidate } = require('../validations/reviewAndRating.validation');
const { expressAuthentication } = require('../middlewares/auth');
const { ratingAndReviewController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');

const router = express.Router();

router
  .route('/review/:id')
  .post(
    expressAuthentication,
    validate(createReviewAndRatingValidate),
    ratingAndReviewController.createReviewAndRating
  );

router.route('/')
  .get(expressAuthentication, paginate, ratingAndReviewController.getAllRatingAndReview);

router.route('/review')
  .get(expressAuthentication, paginate, ratingAndReviewController.getReviewOfProductId);
router
  .route('/:id')
  .get(expressAuthentication, ratingAndReviewController.getRatingById)
  .delete(expressAuthentication, ratingAndReviewController.deleteRatingAndReview)
  .put(expressAuthentication, ratingAndReviewController.UpdateRating);

router.route('/productRatings/:id').get(ratingAndReviewController.getRatingOfProductById);

router.route('/approve/:id')
  .post(expressAuthentication, ratingAndReviewController.approveRating);

router.route('/disapprove/:id')
  .post(expressAuthentication, ratingAndReviewController.removeRating);
module.exports = router;
