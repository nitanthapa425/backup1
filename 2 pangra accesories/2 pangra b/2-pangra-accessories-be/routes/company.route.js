/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { companyController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');
const { createCompanyDetail } = require('../validations/company.validation');

const router = express.Router();

router
  .route('/')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    validate(paramsValidation.pagination),
    paginate,
    companyController.getCompanyDetails
  )
  .post(
    validate(createCompanyDetail),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN', 'ADMIN'),
    companyController.createCompanyDetail
  );

/* Get Update and delete Customer Location */
router
  .route('/:id')
  .get(companyController.getCompanyDetail)
  .put(
    // validate(paramsValidationSchema.commentValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    companyController.updateCompanyDetail
  )
  .delete(expressAuthentication, isAuthenticated, isAuthorized(['SUPER_ADMIN', 'ADMIN']), companyController.deleteCompanyDetail);

module.exports = router;
