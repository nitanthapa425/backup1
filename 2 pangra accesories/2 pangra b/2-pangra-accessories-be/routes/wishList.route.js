const express = require('express');
const { wishListController } = require('../controllers');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const { createWishListValidation } = require('../validations/wishlist.validation');

const router = express.Router();
router.route('/wishlist').get(expressAuthentication, wishListController.getAllWishList);
router.route('/wishlist/client').get(expressAuthentication, wishListController.getClientWishlist);
router
  .route('/wishlist/:id')
  .post(
    validate(createWishListValidation),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    wishListController.createWishList
  )
  .get(expressAuthentication, wishListController.getWishListById);

router
  .route('/wishList/:wishListId')
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    wishListController.deleteWishList
  );

module.exports = router;
