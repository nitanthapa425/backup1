/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { commentController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');
const paramsValidationSchema = require('../validations/validateSchema');

const router = express.Router();

router
  .route('/')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    validate(paramsValidation.pagination),
    paginate,
    commentController.getComments
  );

router
  .route('/:id')
  // .get(expressAuthentication, isAuthenticated, isAuthorized('CLIENT'), commentController.getComments)
  .post(
    validate(paramsValidationSchema.commentValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('CLIENT'),
    commentController.createComment
  );

/* Get Update and delete Customer Location */
router
  .route('/:id')
  .get(commentController.getSellerVehicleComment)
  .put(
    // validate(paramsValidationSchema.commentValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['CLIENT']),
    commentController.updateComment
  )
  .delete(expressAuthentication, isAuthenticated, isAuthorized(['CLIENT']), commentController.deleteComment);

router.route('/single/:id').get(commentController.getSingleComment);

router
  .route('/approveComment/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    commentController.approveCommentByAdmin
  );

router
  .route('/createReplyComment/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    commentController.createReplyComment
  )
  .get(
    // validate(paramsValidationSchema.locationValidate),
    // expressAuthentication,
    // isAuthenticated,
    // isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    commentController.getReplyComment
  );

/* Get Update and delete Customer Location */
router
  .route('/:id/replyComment/:replyId')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    commentController.getReplyCommentById
  )
  .put(
    // validate(paramsValidationSchema.locationValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    commentController.updateReplyCommentById
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    commentController.deleteReplyCommentById
  );
module.exports = router;
