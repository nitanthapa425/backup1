/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { accessoriesController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');
const accessoriesValidation = require('../validations/accessoriesItem.validation');

const router = express.Router();
router
  .route('/')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    paginate,
    // validate(accessoriesValidation.createAccessoriesItem),
    accessoriesController.createAccessoriesItem
  )
  .get(
    // expressAuthentication,
    // // isAuthenticated,
    // // isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    validate(paramsValidation.pagination),
    paginate,
    accessoriesController.getAccessoriesItems
  );

/* Get all feature */
router.route('/getAllFeaturedNoAuth').get(accessoriesController.getAllFeaturedAccessoriesNoAuth);

/* Get all hot deals */
router.route('/getAllHotDeals').get(accessoriesController.getAllHotDeals);

/* Get Hot deals accessories */
router
  .route('/getAllHotDealsByAdmin')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    accessoriesController.getHotDealAccessoriesItems
  );

/* Get Update and delete Customer Location */
router
  .route('/:id')
  .get(accessoriesController.getAccessoriesItem)
  .put(
    validate(accessoriesValidation),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    accessoriesController.updateAccessoriesItem
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    accessoriesController.deleteAccessoriesItem
  );

router
  .route('/addToFeatured/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    accessoriesController.addAccessoriesInFeature
  );

router
  .route('/removeFromFeatured/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    accessoriesController.removeAccessoriesFromFeature
  );

router
  .route('/addToHotDeals/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    accessoriesController.addAccessoriesInHotDeals
  );

router
  .route('/removeFromHotDeals/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    accessoriesController.removeAccessoriesInHotDeals
  );

router.route('/deleteInactive/:id').get(expressAuthentication, accessoriesController.deleteInActiveAccessoriesItem);

router
  .route('/user/recommendations')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    accessoriesController.getRecommendedAccessoriesItem
  );
router
  .route('/deletePermanently/:id')
  .delete(expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    accessoriesController.deleteAccesoriesItemPermanently);
module.exports = router;
