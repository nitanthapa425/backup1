/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { draftAccessoriesController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');
const createDraftAccessoriesItem = require('../validations/draftAccessoriesItem.validation');

const router = express.Router();

router
  .route('/')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    validate(createDraftAccessoriesItem),
    draftAccessoriesController.createDraftAccessoriesItem
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    validate(paramsValidation.pagination),
    paginate,
    draftAccessoriesController.getDraftAccessoriesItems
  );

/* Get Update and delete Customer Location */
router
  .route('/:id')
  .get(draftAccessoriesController.getDraftAccessoriesItem)
  .put(
    validate(createDraftAccessoriesItem),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftAccessoriesController.updateDraftAccessoriesItemById
  )
  .delete(expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftAccessoriesController.deleteDraftItem);

router
  .route('/finishDraft/:id')
  .post(
    validate(createDraftAccessoriesItem),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftAccessoriesController.finishDraftAccessoriesItemById
  );
module.exports = router;
