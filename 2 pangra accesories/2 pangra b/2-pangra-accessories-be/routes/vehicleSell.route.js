/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { vehicleSellController } = require('../controllers');
const validate = require('../middlewares/validate');
const { uploadMultipleFile } = require('../middlewares/uploadImageMiddleware');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');
const paramsValidationSchema = require('../validations/validateSchema');
// eslint-disable-next-line object-curly-newline
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const {
  createVehicleSell,
  // getAddToCart
} = require('../validations/validateSchema');

const router = express.Router();

router
  .route('/vehicleSell')
  .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getSellVehicleDetails);

// .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getSellVehicleDetails);
// router
//   .route('/vehicleSellInFeature')
//   .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getFeatureVehicleSellDetails)
//   .post(
//     validate(createVehicleSell, createVehicleDraftValidate),
//     expressAuthentication,
//     isAuthenticated,
//     uploadMultipleFile,
//     isAuthorized(['SUPER_ADMIN', 'ADMIN']),
//     vehicleSellController.createVehicleDetailSellInFeature
//   );
router
  .route('/add-feature-vehicle/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    vehicleSellController.addFeatureInHomePage
  );

router
  .route('/remove-feature-vehicle/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.removeFeatureInHomePage
  );

// router.route('/vehicleSellInFeature/:id').get(vehicleSellController.getFeatureVehicleSellDetail);

router
  .route('/vehicleSell/:id')
  .get(vehicleSellController.getVehicleSellDetail)
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    vehicleSellController.deleteVehicleSellDetail
  );

router
  .route('/uploadVehicleImage/vehicleSell/:vehicleDetail')
  .post(uploadMultipleFile, vehicleSellController.uploadVehicleSellImages);

router
  .route('/vehicleSellDraft')
  .get(
    validate(paramsValidation.pagination, createVehicleSell),
    paginate,
    vehicleSellController.getVehicleSellDraftDetailsController
  );

router
  .route('/vehicleSellDraft/:id')
  .get(vehicleSellController.getVehicleSellDraftDetailsByIdController)
  .delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteVehicleSellDraftDetailController);

router.route('/saveVehicleSellDraftToSource/:id').post(vehicleSellController.createVehicleSellDraftToVehicleController);

router
  .route('/getUserVehicleSellDraft')
  .get(expressAuthentication, isAuthenticated, vehicleSellController.getUserVehicleSellDraftController);

router
  .route('/getSellerVehicleDetail')
  .get(
    expressAuthentication,
    isAuthenticated,
    validate(paramsValidation.pagination),
    paginate,
    vehicleSellController.getUserVehicleSellController
  );

router.route('/addToCart/:id').post(expressAuthentication, isAuthenticated, vehicleSellController.addToCart);

router
  .route('/getClientAddToCart')
  .get(expressAuthentication, isAuthenticated, vehicleSellController.getClientAddToCart);

router
  .route('/addToCart')
  .get(
    validate(paramsValidation.pagination),
    paginate,
    expressAuthentication,
    isAuthenticated,
    vehicleSellController.getAddToCart
  );

router.route('/getCartById/:id').get(expressAuthentication, isAuthenticated, vehicleSellController.getCartById);

router.route('/deleteCart/:id').delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteCart);

router.route('/addTowishlist/:id').post(expressAuthentication, isAuthenticated, vehicleSellController.addTowishlist);

router.route('/getClientWishlist').get(expressAuthentication, isAuthenticated, vehicleSellController.getClientWishlist);

router
  .route('/getWishlist')
  .get(
    validate(paramsValidation.pagination),
    paginate,
    expressAuthentication,
    isAuthenticated,
    vehicleSellController.getWishlist
  );

router.route('/getWishlistById/:id').get(expressAuthentication, isAuthenticated, vehicleSellController.getWishlistById);

router
  .route('/deleteWishlist/:id')
  .delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteWishlist);

router
  .route('/cartNowWishlist/:id')
  .post(expressAuthentication, isAuthenticated, vehicleSellController.addToCartWishlist);

router
  .route('/wishlistByAdmin/:id')
  .get(expressAuthentication, isAuthenticated, vehicleSellController.getWishlistByAdmin)
  .delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteWishlistByAdmin);
router
  .route('/cartByAdmin/:id')
  .get(expressAuthentication, isAuthenticated, vehicleSellController.getCartByAdmin)
  .delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteCartByAdmin);

router
  .route('/getHotDeals')
  .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getVehicleSellHotDeals);

router
  .route('/getAdminHotDeals')
  .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getAdminVehicleSellHotDeals);

router
  .route('/addInHotDeals/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.addToHotDeals
  );

router
  .route('/removeHotDeals/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.removeFromHotDeal
  );

router
  .route('/verifyVehicle/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.verifySellerVehicle
  );
router
  .route('/unverifySellerVerify/:id')
  .post(expressAuthentication, isAuthenticated, vehicleSellController.unverifySellerVehicle);

router
  .route('/approveSellerVehicle/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.approveSellerVehicle
  );
router
  .route('/unApproveSellerVehicle/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.unApproveSellerVehicle
  );

// router
//   .route('/location')
//   .post(
//     validate(
//       paramsValidationSchema.updateLocation
//     ),
//     expressAuthentication,
//     isAuthenticated,
//     vehicleSellController.createLocationOfLocation
//   )
//   .get(
//     expressAuthentication,
//     isAuthenticated,
//     vehicleSellController.getCustomerLocation
//   );

router
  .route('/updateLocation')
  .put(
    validate(paramsValidationSchema.updateLocation),
    expressAuthentication,
    isAuthenticated,
    vehicleSellController.updateLocation
  );

router
  .route('/meetingSchedule')
  .get(validate(paramsValidation.pagination), paginate, vehicleSellController.getMeetingSchedules);

router
  .route('/meetingSchedule/:id')
  .post(
    validate(paramsValidationSchema.meetingSchedule),
    expressAuthentication,
    isAuthenticated,
    vehicleSellController.meetingSchedule
  )
  .get(expressAuthentication, isAuthenticated, vehicleSellController.getMeetingSchedule)
  .put(expressAuthentication, isAuthenticated, vehicleSellController.updateMeetingSchedule);
router
  .route('/book/:id/meetingSchedule')
  .delete(expressAuthentication, isAuthenticated, vehicleSellController.deleteMeetingSchedule);

router
  .route('/mark-as-sold')
  .patch(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.makeVehicleSellAsSold
  );

router
  .route('/mark-as-unsold')
  .patch(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.makeVehicleSellAsUnsold
  );

router
  .route('/removeAd/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    vehicleSellController.removeAdByVehicleSeller
  );
router
  .route('/addFeatureByClient/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['CLIENT']),
    vehicleSellController.addFeatureInHomePageByClient
  );

router
  .route('/offerPrice/:id')
  .post(expressAuthentication, isAuthenticated, isAuthorized(['CLIENT']), vehicleSellController.offerPriceToSeller);
router
  .route('/approveOfferPrice/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.aprroveBuyerOffer
  );
router
  .route('/rejectOfferPrice/:id')
  .post(
    validate(paramsValidationSchema.rejectOfferPrice),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.rejectBuyerOffer
  );

router
  .route('/getOfferDetails')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    validate(paramsValidation.pagination),
    paginate,
    vehicleSellController.getOfferDetails
  );

router
  .route('/finalOffer/:id') // Send vehicle sell id
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'CLIENT']),
    vehicleSellController.finalOfferFromSeller
  );

router
  .route('/offer/:id')
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    vehicleSellController.deleteOfferById
  );

router
  .route('/getClientOffer/:id')
  .get(expressAuthentication, isAuthenticated, isAuthorized('CLIENT'), vehicleSellController.getClientOfferDetail);
module.exports = router;
