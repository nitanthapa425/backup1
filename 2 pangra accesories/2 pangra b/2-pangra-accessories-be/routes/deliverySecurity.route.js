/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { deliveryPersonSecurityController } = require('../controllers');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const validate = require('../middlewares/validate');
const paramsValidation = require('../validations/validateSchema');
const { paginate } = require('../middlewares/pagination');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router.route('/login').post(deliveryPersonSecurityController.login);

router.route('/signUp').post(deliveryPersonSecurityController.signUp);

router.route('/me/profile').get(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.getMyProfile);

router
  .route('/me/profile')
  .put(
    validate(paramsValidation.userUpdate),
    expressAuthentication,
    isAuthenticated,
    deliveryPersonSecurityController.updateMyProfile
  );

router.route('/confirmEmail').post(deliveryPersonSecurityController.confirmEmail);

router.route('/confirmMobile').post(deliveryPersonSecurityController.confirmMobile);

router.route('/resendVerificationToken').patch(deliveryPersonSecurityController.resendVerificationToken);

router.route('/forgotPassword').post(deliveryPersonSecurityController.forgotPassword);

router.route('/resetPassword').post(deliveryPersonSecurityController.resetPassword);

router
  .route('/changePassword')
  .post(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.changePassword);

router.route('/logout').post(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.logout);

router.route('/validate-user').get(expressAuthentication, deliveryPersonSecurityController.validateUser);

router
  .route('/log-management')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    paginate,
    deliveryPersonSecurityController.getAllLogManagement
  );

router
  .route('/notification')
  .get(expressAuthentication, isAuthenticated, paginate, deliveryPersonSecurityController.getAllNotification);

router
  .route('/notification/mark-as-read')
  .patch(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.markNotificationAsRead);

router
  .route('/notification/mark-as-unread')
  .patch(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.markNotificationAsUnread);

router
  .route('/notification/delete')
  .delete(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.deleteNotification);

router
  .route('/notification/read-all')
  .patch(expressAuthentication, isAuthenticated, deliveryPersonSecurityController.readAllNotification);

router.route('/sendDeliveryToken').post(
  expressAuthentication,
  isAuthenticated,
  isAuthorized(['SUPER_ADMIN', 'ADMIN', 'DELIVERY']),
  deliveryPersonSecurityController.sendOTPCODEforDeliveryVerification
);

router.route('/verifyDeliveryToken').post(
  expressAuthentication,
  isAuthenticated,
  isAuthorized('DELIVERY'),
  deliveryPersonSecurityController.verifyDeliveryToken
);

router.route('/cancelOrderByDeliveryBoy/:id').post(
  expressAuthentication,
  isAuthenticated,
  isAuthorized(['SUPER_ADMIN', 'ADMIN', 'DELIVERY']),
  deliveryPersonSecurityController.cancelOrderByDeliveryBoy
);
module.exports = router;
