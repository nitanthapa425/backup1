/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { brandAccessoriesController } = require('../controllers');
const validate = require('../middlewares/validate');
const { fileUploadMiddleware } = require('../middlewares/uploadImageMiddleware');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');
const { createBrandAccessories } = require('../validations/brandAccessories.validation');

const router = express.Router();

router
  .route('/brandAccessories')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    brandAccessoriesController.getBrandAccessoriesDetails
  )
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    validate(createBrandAccessories),
    brandAccessoriesController.createBrandAccessories
  );

router.route('/brandAccessoriesNoAuth').get(brandAccessoriesController.getBrandWithoutAuth);

router.route('/brandAccessoriesNoAuthLimit').get(paginate, brandAccessoriesController.getBrandWithoutAuthLimit);

router.route('/homeBrandAccessoriesNoAuth').get(brandAccessoriesController.getHomePageBrandWithoutAuth);

router
  .route('/addBrandInHomePage/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.addBrandInHomePage
  );

router
  .route('/removeBrandInHomePage/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.removeBrandInHomePage
  );

router
  .route('/brandAccessories/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.getBrandAccessories
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.updateBrandAccessories
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.deleteBrandAccessories
  );

router
  .route('/accessoriesCategory')
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.createCategoryController
  )
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    paginate,
    brandAccessoriesController.getAccessoriesCategoryController
  );

router
  .route('/accessoriesCategory/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.getCategoryByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.updateCategoryController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.deleteCategoryByIdController
  );

router.route('/accessoriesCategoryNoAuth').get(brandAccessoriesController.getAccessoriesCategoryNoAuth);
router.route('/accessoriesCategoryNoAuthLimit').get(paginate, brandAccessoriesController.getCategoryWithoutAuthLimit);

router
  .route('/accessoriesSubCategory')
  .post(
    fileUploadMiddleware,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.createSubCategoryController
  )
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    paginate,
    brandAccessoriesController.getSubCategoryController
  );

router.route('/accessoriesSubCategoryNoAuth').get(brandAccessoriesController.getAccessoriesSubCategoryNoAuth);

router.route('/accessoriesSubCategoryNoAuthLimit').get(paginate, brandAccessoriesController.getAccessoriesSubCategoryNoAuthLimit);
router
  .route('/accessoriesSubCategory/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.getSubCategoryByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.updateSubCategoryByIdController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.deleteSubCategoryByIdController
  );

router
  .route('/subCategoryByCategoryId/:categoryId')
  .get(
    brandAccessoriesController.getAccessoriesSubCategoryByCategoryId
  );

router
  .route('/accessoriesCategoryByBrandId/:brandId')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    brandAccessoriesController.getCategoryByBrandIdController
  );

router
  .route('/accessoriesSubCategoryByName')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.creatVehicleNameValueController
  )
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    brandAccessoriesController.getVehicleNameValueController
  );

module.exports = router;
