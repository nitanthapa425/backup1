/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { tagController } = require('../controllers');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const { createTag } = require('../validations/accessoriesTag.validation');

const router = express.Router();

router
  .route('/')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    tagController.getAllTags
  )
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN', 'USER']),
    validate(createTag),
    tagController.createTagAccessories
  );

module.exports = router;
