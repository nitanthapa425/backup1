/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { buyNowController } = require('../controllers');
const validate = require('../middlewares/validate');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');
const { buyNow } = require('../validations/buyNow.validation');
// eslint-disable-next-line object-curly-newline
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
// const { buyNow} = require('../validations/validateSchema'); //isAuthorized

const router = express.Router();
router
  .route('/')
  .post(validate(buyNow), expressAuthentication, isAuthenticated, isAuthorized(['CLIENT']), buyNowController.buyNow);

router.route('/getCustomerBuyNow').get(expressAuthentication, isAuthenticated, buyNowController.getCustomerBuyNow);

router
  .route('/:id')
  .get(expressAuthentication, isAuthenticated, buyNowController.getBuyNowDetail)
  .delete(expressAuthentication, isAuthenticated, buyNowController.deleteBuyNow);

router.route('/adminDelete/:id').delete(expressAuthentication, isAuthenticated, buyNowController.deleteBuyNowByAdmin);

router
  .route('/')
  .get(
    validate(paramsValidation.pagination),
    paginate,
    expressAuthentication,
    isAuthenticated,
    buyNowController.getBuyNowDetails
  );
router
  .route('/customerBuyNowById/:id')
  .get(expressAuthentication, isAuthenticated, buyNowController.getCustomerBuyNowById)
  .delete(expressAuthentication, isAuthenticated, buyNowController.deleteBuyNowByCustomer);

router
  .route('/changeOrderStatus/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    buyNowController.changeOrderStatus
  );

module.exports = router;
