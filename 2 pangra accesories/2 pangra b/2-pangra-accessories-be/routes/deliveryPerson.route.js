/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { deliveryPersonController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router
  .route('/')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    deliveryPersonController.getUserProfileList
  );

router
  .route('/getDeliveryItemsList')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('DELIVERY'),
    paginate,
    // isAuthorized('DELIVERY'),
    deliveryPersonController.getDeliveryAccessoriesList
  );

router
  .route('/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    deliveryPersonController.getUserProfileByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    deliveryPersonController.updateUserProfileByIdController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    deliveryPersonController.deleteUserProfileByIdController
  );

router
  .route('/deletedUser')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    paginate,
    deliveryPersonController.getDeletedUserProfileList
  );

router
  .route('/deletedUser/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    deliveryPersonController.getDeletedUserById
  );

router
  .route('/revertUser/:id')
  .post(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    deliveryPersonController.revertDeletedUserById
  );

router
  .route('/companyPersonByCompanyId/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN', 'ADMIN'),
    deliveryPersonController.companyPersonByCompanyId
  );

router
  .route('/userDeliveryList/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('DELIVERY'),
    deliveryPersonController.getDeliveryDetailsOfUser
  );

module.exports = router;
