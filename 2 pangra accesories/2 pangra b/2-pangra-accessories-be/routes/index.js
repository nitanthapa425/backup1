/**
 * Copyright (C) Two Pangra
 */

/**
 * the route entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const SecurityRoutes = require('./security.routes');
const UserRoute = require('./user.route');
const ClientRoute = require('./client.route');
const deliveryRoute = require('./deliveryPerson.route');
const deliverySecurityRoute = require('./deliverySecurity.route');
const brandAccessoriesRoute = require('./brandAccessories.route');
const uploadImageRoute = require('./imageUpload.route');
const vehicleSellRoute = require('./vehicleSell.route');
const geoLocationRoute = require('./geolocation.route');
const emailSubscribeRoute = require('./emailSubscribe.route');
const commentRoute = require('./comment.route');
const accessoriesItemRoute = require('./accessoriesItem.route');
const ratingAndReviewRoute = require('./reviewAndRating.route');
const draftAccessoriesItemRoute = require('./draftAccessoriesItem.route');
const buyNowRoute = require('./buyNow.route');
const wishListRoute = require('./wishList.route');
const addToCartRoute = require('./addToCart.route');
const companyRoute = require('./company.route');
const accessoriesTagRoute = require('./accessoriesTag.route');

const apiRouter = express.Router();

const mainRoute = '/';
const seller = '/seller';
const client = '/client';
const delivery = '/delivery';
const deliverySecurity = '/deliverySecurity';
const geoLocation = '/geo-location';
const emailSubscribe = '/email';
const comment = '/comment';
const accessories = '/product';
const rating = '/rating';
const draftAccessories = '/draftProduct';
const buyNow = '/buyNow';
const wishlist = '/accessories';
const addToCart = '/cart';
const company = '/company';
const tags = '/tags';

const defaultRoutes = [
  {
    path: mainRoute,
    route: UserRoute
  },
  {
    path: client,
    route: ClientRoute
  },
  {
    path: delivery,
    route: deliveryRoute
  },
  {
    path: deliverySecurity,
    route: deliverySecurityRoute
  },
  {
    path: mainRoute,
    route: brandAccessoriesRoute
  },
  {
    path: mainRoute,
    route: uploadImageRoute
  },
  {
    path: mainRoute,
    route: SecurityRoutes
  },
  {
    path: seller,
    route: vehicleSellRoute
  },
  {
    path: geoLocation,
    route: geoLocationRoute
  },
  {
    path: emailSubscribe,
    route: emailSubscribeRoute
  },
  {
    path: comment,
    route: commentRoute
  },
  {
    path: accessories,
    route: accessoriesItemRoute
  },
  {
    path: rating,
    route: ratingAndReviewRoute
  },
  {
    path: draftAccessories,
    route: draftAccessoriesItemRoute
  },
  {
    path: buyNow,
    route: buyNowRoute
  },
  {
    path: wishlist,
    route: wishListRoute
  },
  {
    path: addToCart,
    route: addToCartRoute
  },
  {
    path: company,
    route: companyRoute
  },
  {
    path: tags,
    route: accessoriesTagRoute
  }
];

defaultRoutes.forEach((route) => {
  apiRouter.use(route.path, route.route);
});

module.exports = apiRouter;

// module.exports = userRouter;
