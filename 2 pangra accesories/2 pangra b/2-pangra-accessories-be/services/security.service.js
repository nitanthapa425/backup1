/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Two Pangra
 */

/**
 * the Security Service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const joi = require('joi');
const errors = require('common-errors');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const util = require('util');
const _ = require('lodash');
const config = require('config');
const models = require('../models');
const helper = require('../common/helper');
const { signUpSchema } = require('../validations/validateSchema');
const { capitalize } = require('../common/helper');
// eslint-disable-next-line object-curly-newline
const { LogManagementModel, NotificationModel, Client, User, DeliveryPerson } = require('../models');

/* ***************************** helpers ***************************** */

/**
 * gets user by email id
 * @param {String} email the email id
 * @returns {Object} the user information
 */
async function getUserByEmail(email) {
  const user = await models.User.findOne({ email });
  return user;
}

/**
 * gets user by user name
 * @param {String} userName the email id
 * @returns {Object} the user information
 */
async function getUserByUserName(userName) {
  const user = await models.User.findOne({ userName });
  return user;
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendVerificationEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.emailVerificationContent,
    email,
    `${frontendUrl}/confirmEmail?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/confirmEmail?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Verify Email Address on Dui Pangra',
    to: email,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await helper.sendEmail(emailEntity);
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendForgotPasswordEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.forgotPasswordContent,
    email,
    `${frontendUrl}/reset-password?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/reset-password?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Forgot Password',
    from: '"Dui-Pangra" <support@duipangra.com>',
    to: email,
    html: emailContent
  };
  await helper.sendEmail(emailEntity);
}

/**
 * checks user authentication using email and password
 * @param {Object} entity the entity
 * @returns {Object} the user information with token
 */
async function login(entity) {
  let user;
  if (typeof entity.userName === 'undefined' && entity.email !== '') {
    user = await helper.ensureEntityExists(
      models.User,
      { email: entity.email },
      `Sorry, we could not find any user with  ${entity.email} registered with us.`
    );
  } else {
    user = await helper.ensureEntityExists(
      models.User,
      { userName: entity.userName },
      `Sorry, we could not find any user with   ${entity.userName} registered with us.`
    );
  }
  if (user.isActive === true) {
    const matched = await helper.validateHash(entity.password, user.passwordHash);

    if (!matched) {
      throw new errors.HttpStatusError(401, 'Wrong email or password.');
    }

    helper.ensureUserVerified(user);

    // generate JWT token
    const token = jwt.sign(_.pick(user, ['id', 'userName', 'email', 'firstName', 'accessLevel']), config.JWT_SECRET, {
      expiresIn: config.JWT_EXPIRATION
    });
    user.accessToken = token;
    await user.save();
    user = _.omit(user.toObject(), 'passwordHash', 'verificationToken', 'forgotPasswordToken', '__v');
    const userData = await User.findOne({ userName: entity.userName });
    await helper.createLogManagement(userData._id, 'Add Data', 'Login User', 'User Login');
    return {
      user
    };
  }
  throw new errors.NotPermittedError('This account is no longer available.');
}

login.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      userName: joi.string().optional().allow(''),
      password: joi.string().required()
    })
    .required()
};

/**
 * does sign up process
 * @param {Object} entity the request body entity
 * @returns {Object} the sign user information
 */
async function signUp(entity) {
  let user = await getUserByEmail(entity.email);
  const username = await getUserByUserName(entity.userName);
  // const passwordHash = await helper.hashString(entity.password);

  if (username) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Username ${entity.userName} already exists`);
  }
  if (user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Email ${entity.email} already exists`);
  }
  const dateOfBirth = new Date(entity.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');

  const verificationToken = helper.getRandomString(25);
  user = _.extend(entity, {
    email: entity.email,
    userName: entity.userName,
    // passwordHash,
    verificationToken,
    firstName: capitalize(entity.firstName),
    middleName: capitalize(entity.middleName),
    lastName: capitalize(entity.lastName),
    fullName:
      entity.middleName !== ''
        ? capitalize(entity.firstName).concat(' ') +
          capitalize(entity.middleName).concat(' ') +
          capitalize(entity.lastName)
        : capitalize(entity.firstName).concat(' ') + capitalize(entity.lastName),
    role: entity.role,
    dob,
    verified: false
  });
  // generate JWT token
  const token = jwt.sign(_.pick(user, ['id', 'userName', 'email', 'firstName', 'accessLevel']), config.JWT_SECRET, {
    expiresIn: config.JWT_EXPIRATION
  });
  user.accessToken = token;

  user = new models.User(user);
  await user.save();
  // send an email
  try {
    await sendVerificationEmail(user.email, verificationToken, helper.getHostUrl());
  } catch (ex) {
    await user.remove();
    throw ex;
  }
  const userData = await User.findOne({ userName: entity.userName });
  await helper.createLogManagement(userData._id, 'Add Data', 'SignUp User', 'User SignUp');
  return user;
}

/* Validate Sign Up Schema Detail wit Joi */
signUp.schema = {
  entity: signUpSchema.entity
};

/**
 * get login user profile
 * @param {Object} entity the request query
 * @returns {String} the success or failure
 */
const getMyProfile = async (userId) => {
  let user = await helper.ensureEntityExists(models.User, { _id: userId });
  user = _.omit(
    user.toObject(),
    'verified',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken',
    'passwordHash',
    '_id'
  );
  return user;
};

/**
 * handles the confirm email
 * @param {Object} entity the request query
 * @returns {String} the success or failure
 */
async function confirmEmail(entity) {
  const passwordHash = await helper.hashString(entity.passwordHash);
  const user = await getUserByEmail(entity.email);
  if (!user) {
    throw new errors.NotFoundError(`Sorry we cannot find a user with this email ${entity.email}`);
  }
  if (user.verified) {
    throw new errors.HttpStatusError(
      httpStatus.BAD_REQUEST,
      `User with this email ${entity.email} has 
    already been  verified`
    );
  }

  if (user.verificationToken === entity.verificationToken) {
    user.verified = true;
    user.passwordHash = passwordHash;
    user.verificationToken = null;
    await user.save();
  } else {
    throw new errors.ValidationError("Sorry but the verification code doesn't match");
  }
  await helper.createLogManagement(user._id, 'Add Data', 'Confirm Email', 'Email Confirm');

  return { message: `Congratulations, ${entity.email} has been verified successfully` };
}

confirmEmail.schema = {
  entity: joi.object().keys({
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
    passwordHash: joi
      .string()
      /** Password must be minimum of eight characters, with at least */
      /** one uppercase letter, one lowercase letter, one number and one special character */
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      .required(),
    verificationToken: joi.string().required()
  })
};

async function confirmMobile(entity) {
  const client = await Client.findOne({
    mobile: entity.mobile
  });
  if (!client) {
    throw new errors.NotFoundError('Sorry, user does not exist');
  }
  if (client.phoneVerified === true) {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, 'User is already verified');
  }

  if (client.verificationToken === entity.verificationToken) {
    client.phoneVerified = true;
    client.isActive = true;
    client.verificationToken = null;
    await client.save();
  } else {
    throw new errors.ValidationError("Sorry, verification token doesn't match");
  }
  await helper.createLogManagement(client._id, 'Add Data', 'Confirm Mobile', 'Mobile Confirm');

  return { message: 'Congratulations, you account have been verified successfully' };
}

confirmMobile.schema = {
  entity: joi.object().keys({
    mobile: joi.string().required(),
    verificationToken: joi.string().required()
  })
};

async function resendVerificationToken(entity) {
  const client = await Client.findOne({
    mobile: entity.mobile
  });
  if (!client) {
    throw new errors.NotFoundError('Sorry, user does not exist');
  }
  if (client.phoneVerified === true) {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, 'User is already verified');
  }

  client.verificationToken = entity.verificationToken;

  await client.save();

  return { message: 'Verification token have been send in your mobile successfully.' };
}

/**
 * does forgot password process
 * @param {Object} entity the request body entity
 * @returns {Object} the user information
 */
async function forgotPassword(entity) {
  const user = await helper.ensureEntityExists(
    models.User,
    { email: entity.email },
    `${entity.email} could not be found`
  );
  // generate token
  const verificationToken = helper.getRandomString(25);
  // update user information in database
  user.forgotPasswordToken = verificationToken;
  await user.save();

  // send an email
  await sendForgotPasswordEmail(user.email, verificationToken, helper.getHostUrl());
  const userData = await User.findOne({ email: entity.email });
  await helper.createLogManagement(userData._id, 'Add Data', 'Forget Password', 'Password Forget');

  return {
    message: 'Please check your email for the next step for password reset.'
  };
}

forgotPassword.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required()
    })
    .required()
};

/**
 * handles the reset password
 * @param {Object} entity the request body
 * @returns {String} the success or failure
 */
async function resetPassword(entity) {
  const user = await getUserByEmail(entity.email);

  if (!user) {
    throw new errors.NotFoundError(`${entity.email} not found`);
  }

  const newMatched = await helper.validateHash(entity.newPassword, user.passwordHash);

  if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else if (user.forgotPasswordToken === entity.verificationToken) {
    user.passwordHash = await helper.hashString(entity.newPassword);
    user.forgotPasswordToken = null;
    user.accessToken = null;
    await user.save();
  } else {
    throw new errors.AuthenticationRequiredError('Sorry but your verification token is not valid.');
  }
  await helper.createLogManagement(user._id, 'Add Data', 'Reset Password', 'Password Reset');

  return { message: 'Your password has been reset successfully, please log in to continue!' };
}

resetPassword.schema = {
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        /** Password must be minimum of eight characters, with at least */
        /** one uppercase letter, one lowercase letter, one number and one special character */
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      // oldPassword: joi
      //   .string()
      //   /** Password must be minimum of eight characters, with at least */
      //   /** one uppercase letter, one lowercase letter, one number and one special character */
      //   .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      //   .required(),
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
      verificationToken: joi.string().required()
    })
    .required()
};

/**
 * handles the update password
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function changePassword(userId, entity) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId });
  const matched = await helper.validateHash(entity.oldPassword, user.passwordHash);
  const newMatched = await helper.validateHash(entity.newPassword, user.passwordHash);

  if (!matched) {
    throw new errors.HttpStatusError(401, 'Current Password is incorrect.');
  } else if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else {
    user.passwordHash = await helper.hashString(entity.newPassword);
    user.accessToken = null;
    await user.save();
  }
  await helper.createLogManagement(user._id, 'Add Data', 'Change Password', 'Password Change');
  return 'Your Password has been changed successfully.';
}

changePassword.schema = {
  userId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      oldPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required()
    })
    .required()
};

/**
 * handles logout
 * @param {Object} userId the user id
 */
async function logout(userId) {
  let user = {};
  if (user) {
    user = await helper.ensureEntityExists(models.User, { _id: userId });
    if (!user.accessToken) {
      throw new errors.NotPermittedError(`user ${userId} is already logged out`);
    }
    user.accessToken = null;
    await user.save();
  } else {
    user = await helper.ensureEntityExists(DeliveryPerson, { _id: userId });
    if (!user.accessToken) {
      throw new errors.NotPermittedError(`user ${userId} is already logged out`);
    }
    user.accessToken = null;
    await user.save();
  }
}

logout.schema = {
  userId: joi.string().required()
};

/* Update my Profile */
async function updateMyProfile(userId, entity) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId }, 'The User with Id does not exist.');

  if (!user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'User is not found. Try Refreshing the page');
  }

  _.assignIn(user, entity);

  await user.save();
  await helper.createLogManagement(user._id, 'Update Data', 'Update Profile', 'Profile');

  return {
    status: 200,
    message: 'User has been updated successfully'
  };
}

const validateUser = async (token) => {
  const user = await models.User.findOne({
    accessToken: token
  });

  if (user === null) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Please authenticate');
  }

  return user;
};

const getAllLogManagement = async (search, options) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const result = LogManagementModel.paginate(search, optionValue);
  return result;
};

const getAllNotification = async (search, options, io) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const result = await NotificationModel.paginate(search, optionValue);

  io.emit('notification', result);

  return result;
};

async function markNotificationAsRead(notificationId) {
  const notification = NotificationModel.findById(notificationId);
  if (notification.isRead === true) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This notification is already marked as read');
  }

  await NotificationModel.updateOne({ _id: notificationId }, { isRead: true });

  return {
    status: 200,
    message: 'Notification have been marked as read'
  };
}

async function markNotificationAsUnread(notificationId) {
  const notification = NotificationModel.findById(notificationId);

  if (notification.isRead === false) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This notification is already marked as unread');
  }

  const entity = { isRead: false };
  await NotificationModel.updateOne({ _id: notificationId }, entity);

  return {
    status: 200,
    message: 'Notification have been marked as unread'
  };
}

async function deleteNotification(notificationId) {
  const notification = await helper.ensureEntityExists(
    NotificationModel,
    { _id: notificationId },
    'The notification does not exist.'
  );

  notification.remove();

  return {
    status: 200,
    message: 'Notification is deleted Successfully'
  };
}
async function readAllNotification(createdBy) {
  await NotificationModel.updateMany({ createdBy }, { $set: { isRead: true } });

  return {
    status: 200,
    message: 'All Notification have been marked as read'
  };
}

module.exports = {
  login,
  signUp,
  logout,
  getMyProfile,
  confirmEmail,
  forgotPassword,
  resetPassword,
  changePassword,
  updateMyProfile,
  sendVerificationEmail,
  validateUser,
  getAllLogManagement,
  getAllNotification,
  markNotificationAsRead,
  markNotificationAsUnread,
  deleteNotification,
  readAllNotification,
  confirmMobile,
  resendVerificationToken
};
