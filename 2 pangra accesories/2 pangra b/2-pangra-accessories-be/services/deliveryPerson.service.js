/**
 * Copyright (C) Two Pangra
 */

/**
 * the Delivery Service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const _ = require('lodash');
const errors = require('common-errors');
const httpStatus = require('http-status');
const helper = require('../common/helper');
// eslint-disable-next-line object-curly-newline
const { DeliveryPerson, BuyNow, Client } = require('../models');

/* Get User data for authentication */
const getUserById = async (userId) => DeliveryPerson.find({ _id: userId });

/**
 * Get  User Data
 * @param userId
 */
const getUserProfileList = async (search, options) => {
  search.$or = [{ isActive: true }, { isActive: { $exists: false } }];
  const data = DeliveryPerson.paginate(search, options);
  return data;
};

/**
 * Get Deleted User
 * @param userId
 */

const getDeletedUserProfileList = async (search, options, userId) => {
  search.$or = [{ isActive: false }];

  const data = DeliveryPerson.paginate(search, options);
  await helper.createLogManagement(userId, 'View Data', 'View Deleted User Profile List', 'Deleted User Profile List');
  return data;
};

/**
 * Get User By id
 * @param userId
 */

const getUserProfileByIdService = async (userId, adminId) => {
  let user = await DeliveryPerson.findOne({
    _id: userId
  });
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  await helper.createLogManagement(adminId, 'View Data', 'View User Profile', 'User Profile');
  return user;
};

/* Update User Profile by Id */
async function updateUserProfileByIdService(userId, entity, updatedBy) {
  const user = await helper.ensureEntityExists(DeliveryPerson, { _id: userId }, 'The User with Id does not exist.');

  if (!user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'User is not found. Try Refreshing the page');
  }

  _.assignIn(user, { ...entity });

  await user.save();
  await helper.createLogManagement(updatedBy, 'Update Data', 'Update User Profile', 'User Profile');
  return {
    status: 200,
    message: 'User has been updated successfully'
  };
}

/* Delete User Super admin by id */
/**
 * Delete Brand Model By id
 * @returns {Object} Search result
 */
async function deleteUserProfileByIdService(userId, deleteId) {
  const user = await helper.ensureEntityExists(DeliveryPerson, { _id: deleteId }, 'The User does not exist.');
  if (user.isActive === false) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'User is already deleted.');
  } else {
    user.isActive = false;
    user.deletedBy = deleteId;
    user.updatedAt = Date.now();
    user.save();
  }
  await helper.createLogManagement(userId, 'Delete Data', 'Delete User Profile', 'User Profile');

  return {
    status: 200,
    message: 'User is deleted successfully.'
  };
}

/**
 * Get Deleted User By id
 * @param userId
 */

const getDeletedUserById = async (userId) => {
  const user = await DeliveryPerson.findOne({
    _id: userId
  });
  return user;
};

/**
 * Revert Deleted User By id
 * @param userId
 */

const revertDeletedUserById = async (userId, adminId) => {
  const user = await helper.ensureEntityExists(DeliveryPerson, { _id: userId }, 'The User does not exist.');
  if (user.isActive === true) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'User is already active.');
  } else {
    user.isActive = true;
    user.updatedBy = adminId;
    user.updatedAt = Date.now();
    user.save();
  }
  await helper.createLogManagement(adminId, 'Update Data', 'revert Deleted User', 'User Profile revert');
  return {
    status: 200,
    message: 'User is activated successfully.'
  };
};

/**
 * Get Delivery list to be delivered for delivery person
 * @param userId
 */

const getDeliveryAccessoriesList = async (search, options, userId) => {
  const data = await BuyNow.find({ 'onTheWayDetail.deliveryPersonId': userId, deliveryStatus: 'on the way' });
  const createdBy = await data.map((res) => res.createdBy);

  search.$and = [
    {
      _id: { $in: createdBy }
    }
  ];

  const values = Client.paginate(search, options);

  return values;
};

/**
 * Get delivery person detail by company Id
 * @param companyId
 */
const companyPersonByCompanyId = async (companyId) => {
  const deliverPersonDetailByCompanyId = await DeliveryPerson.find({ companyId });
  return deliverPersonDetailByCompanyId;
};

/**
 * Get user order details
 * @param companyId
 */
const getDeliveryDetailsOfUser = async (clientId, userId) => {
  const deliveryList = await BuyNow.find({
    deliveryStatus: 'on the way',
    'onTheWayDetail.deliveryPersonId': userId,
    createdBy: clientId
  });
  // eslint-disable-next-line eqeqeq
  const values = await deliveryList.filter((res) => res.createdBy == clientId);
  return values;
};

module.exports = {
  getUserProfileList,
  getUserProfileByIdService,
  getDeletedUserProfileList,
  getUserById,
  updateUserProfileByIdService,
  deleteUserProfileByIdService,
  getDeletedUserById,
  revertDeletedUserById,
  getDeliveryAccessoriesList,
  companyPersonByCompanyId,
  getDeliveryDetailsOfUser
};
