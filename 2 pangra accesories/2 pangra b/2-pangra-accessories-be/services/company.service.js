/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
const httpStatus = require('http-status');
// const errors = require('common-errors');
const { Company, DeliveryPerson } = require('../models');
const ApiError = require('../utils/ApiError');
const helper = require('../common/helper');

/**
 * Create a Company detail
 * @param {Object} categoryBody
 * @returns {Promise<Company detail>}
 */
const createCompanyDetail = async (categoryBody, userId) => {
  const service = Company.create(categoryBody);
  await helper.createLogManagement(userId, 'Add Data', 'Add Company Detail', 'Company Detail');
  return service;
};

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getCompanyDetails = async (filter, options, userId) => {
  const comments = await Company.paginate(filter, options);
  await helper.createLogManagement(userId, 'View Data', 'View Company Detail', 'Company Detail');
  return comments;
};

/**
 * Get Company detail by id
 * @param {ObjectId} id
 * @returns {Promise<Company detail>}
 */
const getCompanyDetailById = async (companyId) => {
  const company = Company.findOne({ _id: companyId });
  if (!company) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Company detail not found');
  }
  return company;
};

/**
 * Update Company detail by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<Company detail>}
 */
const updateCompanyDetailById = async (companyId, updateBody, userId) => {
  const company = await Company.findOne({ _id: companyId });
  if (!company) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Company detail not found');
  }
  await Company.updateOne({ _id: companyId }, updateBody);
  await helper.createLogManagement(userId, 'Update Data', 'Update Company Detail', 'Company Detail');
  return {
    code: 201,
    message: 'Company detail is updated successfully'
  };
};

/**
 * Delete Company detail by id
 * @param {ObjectId} companyId
 * @returns {Promise<Company detail>}
 */
const deleteCompanyDetailById = async (companyId, userId) => {
  const company = await Company.findOne({ _id: companyId });
  const ifDeliveryPersonExist = await DeliveryPerson.findOne({ companyId });
  if (!ifDeliveryPersonExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Delete delivery person of this company before deleting it');
  }
  if (!company) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Company detail is not found');
  }

  await company.deleteOne({ _id: companyId });
  await helper.createLogManagement(userId, 'Delete Data', 'Delete Company Detail', 'Company Detail');
  return company;
};

module.exports = {
  createCompanyDetail,
  getCompanyDetails,
  getCompanyDetailById,
  updateCompanyDetailById,
  deleteCompanyDetailById
};
