/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
const httpStatus = require('http-status');
// const errors = require('common-errors');
const _ = require('lodash');
const { Accessories, draftAccessories } = require('../models');
const ApiError = require('../utils/ApiError');
const helper = require('../common/helper');

/**
 * Create a Draft Accessories
 * @param {Object} accessoriesBody
 * @returns {Promise<draftAccessories>}
 */
const createDraftAccessoriesItem = async (categoryBody, userId) => {
  const value = await draftAccessories.create(categoryBody);
  if (value) {
    await helper.createLogManagement(userId, 'Add Data', 'Add Draft', 'Draft');
  }
  return value;
};

/**
 * Query for Draft Accessories
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getDraftAccessoriesItems = async (search, options, userId) => {
  const comments = await draftAccessories.paginate(options, search);
  await helper.createLogManagement(userId, 'View Data', 'Draft Data', 'Draft Accessories');
  return comments;
};

/**
 * Get draft Accessories by id
 * @param {ObjectId} id
 * @returns {Promise<Comment>}
 */
const getDraftAccessoriesItem = async (id) => {
  const result = draftAccessories.findOne({ _id: id, status: true });
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories not found');
  }
  return result;
};

/**
 * Update draftAccessories by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<draftAccessories>}
 */
const updateDraftAccessoriesItemById = async (userId, id, updateBody) => {
  const result = await draftAccessories.findOne({ _id: id });
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories not found');
  }
  await draftAccessories.updateOne({ _id: id }, updateBody);
  await helper.createLogManagement(userId, 'Update Data', 'Update Draft', 'Draft');

  return {
    code: 201,
    message: 'Draft accessories is updated successfully'
  };
};

// /**
//  * Approved comment by admin
//  * @param {ObjectId} userId
//  * @returns {Promise<Comment>}
//  */
// const approveCommentByAdmin = async (commentId) => {
//   const comment = await Comment.findOne({ _id: commentId });
//   if (!comment) {
//     throw new ApiError(httpStatus.NOT_FOUND, 'Comment is not found');
//   }
//   if (comment.isApproved === true) {
//     throw new ApiError(httpStatus.NOT_FOUND, 'Comment is already approved');
//   }
//   comment.isApproved = true;
//   comment.save();
//   return comment;
// };

/**
 * Finish draft accessories
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Comment>}
 */
const finishDraftAccessoriesItemById = async (userId, updateBody, id) => {
  const draft = await draftAccessories.findOne({ _id: id });
  if (!draft) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories is not found');
  }
  const value = _.omit(draft.toObject(), '_id');
  const _value = {
    ...value,
    ...updateBody,
    createdBy: userId
  };

  await Accessories.create(_value);
  draft.remove();
  return draft;
};

/**
 * Finish draft accessories
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Comment>}
 */
const deleteDraftItem = async (deleteId, userId) => {
  const value = await draftAccessories.findOne({ _id: deleteId });
  if (!value) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories is not found');
  }
  const deleteDraft = await draftAccessories.deleteOne({ _id: deleteId });
  await helper.createLogManagement(userId, 'Delete Data', 'Delete Draft', 'Draft');
  return deleteDraft;
};

module.exports = {
  createDraftAccessoriesItem,
  getDraftAccessoriesItems,
  getDraftAccessoriesItem,
  updateDraftAccessoriesItemById,
  // approveCommentByAdmin,
  finishDraftAccessoriesItemById,
  deleteDraftItem
};
