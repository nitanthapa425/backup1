const _ = require('lodash');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const { RatingAndReview, Accessories, BuyNow } = require('../models');

/* Create Rating And Review  */
// eslint-disable-next-line consistent-return
const createRating = async (userId, accessoriesId, reqObj) => {
  /* Check if product exist or not */
  await helper.ensureEntityExists(
    Accessories,
    {
      _id: accessoriesId
    },
    'Accessories is not found'
  );

  /* Check if the user has already given the rating */
  const existingRatingOfUsers = await RatingAndReview.findOne({ accessoriesId, createdBy: userId });

  if (existingRatingOfUsers) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'You cannot give the rating twice');
  }
  /* Check if the user has already given the rating */
  const checkUserHasBoughtSameProduct = await BuyNow.findOne({ accessoriesId, createdBy: userId });

  if (!checkUserHasBoughtSameProduct) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Buy this product to review it');
  }

  /* Add accessories id in rating and review */
  const values = {
    ...reqObj,
    accessoriesId
  };
  if (!existingRatingOfUsers) {
    const value = await RatingAndReview.create(values);
    await helper.ratingAndReviewLogManagement(userId, 'Add Data', 'Rating', 'Rating', 'CLIENT');
    return value;
  }
};

/* Get All Rating And Review */
const getAllRating = async (search, filter) => {
  const getAllRatingAndReview = await RatingAndReview.paginate(search, filter);
  return getAllRatingAndReview;
};
/* Get All Rating And Review Product Id */
const getReviewOfProductId = async (userId) => {
  const results = await RatingAndReview.find({ createdBy: userId });
  const value = await results.map((res) => res.accessoriesId);
  return value;
};

/* Delete Rating And Review ById */
const deleteRatingAndReview = async (deleteId, userId) => {
  const ratingAndReview = await RatingAndReview.findByIdAndDelete({ _id: deleteId, createdBy: userId });
  if (ratingAndReview) {
    await helper.ratingAndReviewLogManagement(userId, 'Delete Data', 'Rating', 'Rating', 'CLIENT');
  }
  return ratingAndReview;
};
/* Get Rating By Using Id */
const getRatingById = async (productId, userId) => {
  const ratingAndReview = await RatingAndReview.findOne({ accessoriesId: productId, createdBy: userId });
  if (!ratingAndReview) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Rating and review not found');
  }

  if (ratingAndReview) {
    await helper.ratingAndReviewLogManagement(userId, 'View Data', 'Rating', 'Rating', 'CLIENT');
  }
  return ratingAndReview;
};

/* Update Rating By Using Id */
const updateRatingById = async (productId, product, userId) => {
  const ratingDetailById = await helper.ensureEntityExists(
    RatingAndReview,
    {
      _id: productId
    },
    'Product is not found'
  );
  _.assignIn(ratingDetailById, { ...product });

  const updatedRating = await ratingDetailById.save();
  await helper.createLogManagement(userId, 'Update Data', 'Rating', 'Rating', 'CLIENT');
  return updatedRating;
};

/* Get Rating of product */
const getRatingOfProductById = async (productId) => {
  const ratingOfProducts = await RatingAndReview.find({ accessoriesId: productId });
  const ratingOfProduct = await RatingAndReview.find({ accessoriesId: productId, status: true });
  const initialValue = 0;
  const totalRatings = await ratingOfProduct
    .map((res) => res.ratingNumber)
    .reduce((previousValue, currentValue) => previousValue + Number(currentValue), initialValue);

  const totalPersonRating = await RatingAndReview.findOne({ accessoriesId: productId, status: true }).countDocuments();
  const value = totalRatings / totalPersonRating;
  return {
    averageRating: value.toFixed(1),
    numberOfRating: totalPersonRating,
    ratingOfProducts
  };
};
/* approve rating */
const approvedFeature = async (ratingId) => {
  const rating = await helper.ensureEntityExists(
    RatingAndReview,
    { _id: ratingId },
    'Rating And Review does not exist'
  );
  if (rating.status === true) {
    throw new errors.HttpStatusError(401, 'This item is already approved');
  }
  rating.status = true;
  const data = await rating.save();
  await helper.ratingAndReviewLogManagement(rating.createdBy, 'Add Data', 'Rating', 'Approve Rating', 'CLIENT');
  return data;
};

/* Dis-approve rating */
const removedFeature = async (ratingId) => {
  const rating = await helper.ensureEntityExists(
    RatingAndReview,
    { _id: ratingId },
    'Rating And Review does not exist'
  );
  if (rating.status === false) {
    throw new errors.HttpStatusError(401, 'This item is already disapproved');
  }
  rating.status = false;
  const data = await rating.save();
  await helper.ratingAndReviewLogManagement(rating.createdBy, 'Add Data', 'Rating', 'Dis-Approve Rating', 'CLIENT');
  return data;
};

module.exports = {
  createRating,
  getAllRating,
  deleteRatingAndReview,
  getRatingById,
  updateRatingById,
  approvedFeature,
  removedFeature,
  getRatingOfProductById,
  getReviewOfProductId
};
