/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Two Pangra Accessories
 */

/**
 * the delivery service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const joi = require('joi');
const errors = require('common-errors');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const util = require('util');
const _ = require('lodash');
const config = require('config');
const models = require('../models');
const helper = require('../common/helper');
const { signUpSchema } = require('../validations/validateSchema');
const { capitalize } = require('../common/helper');
// eslint-disable-next-line object-curly-newline
const { LogManagementModel, NotificationModel, DeliveryPerson, Company, Client, BuyNow } = require('../models');

/**
 * gets user by email id
 * @param {String} email the email id
 * @returns {Object} the user information
 */
async function getUserByEmail(email) {
  const user = await models.DeliveryPerson.findOne({ email });
  return user;
}

/**
 * gets user by user name
 * @param {String} userName the email id
 * @returns {Object} the user information
 */
async function getUserByUserName(userName) {
  const user = await models.DeliveryPerson.findOne({ userName });
  return user;
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendVerificationEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.emailVerificationContent,
    email,
    `${frontendUrl}/confirmDelivery?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/confirmDelivery?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Verify Email Address on Dui Pangra',
    to: email,
    from: '"Dui-Pangra" <support@duipangra.com>',
    html: emailContent
  };
  await helper.sendEmail(emailEntity);
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendForgotPasswordEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.forgotPasswordContent,
    email,
    `${frontendUrl}/delivery/reset-password?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/delivery/reset-password?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Forgot Password',
    from: '"Dui-Pangra-Accessories" <support@duipangra.com>',
    to: email,
    html: emailContent
  };
  await helper.sendEmail(emailEntity);
}

/**
 * checks user authentication using email and password
 * @param {Object} entity the entity
 * @returns {Object} the user information with token
 */
async function login(entity) {
  let delivery;
  if (typeof entity.userName === 'undefined' && entity.email !== '') {
    delivery = await helper.ensureEntityExists(
      DeliveryPerson,
      { email: entity.email },
      `Sorry, we could not find any user with  ${entity.email} registered with us.`
    );
  } else {
    delivery = await helper.ensureEntityExists(
      models.DeliveryPerson,
      { userName: entity.userName },
      `Sorry, we could not find any user with   ${entity.userName} registered with us.`
    );
  }
  if (delivery.isActive === true) {
    const matched = await helper.validateHash(entity.password, delivery.passwordHash);

    if (!matched) {
      throw new errors.HttpStatusError(401, 'Wrong email or password.');
    }

    helper.ensureUserVerified(delivery);
    const token = jwt.sign(
      _.pick(delivery, ['id', 'userName', 'email', 'firstName', 'accessLevel']),
      config.JWT_SECRET,
      {
        expiresIn: config.JWT_EXPIRATION
      }
    );

    delivery.accessToken = token;
    await delivery.save();
    delivery = _.omit(delivery.toObject(), 'passwordHash', 'verificationToken', 'forgotPasswordToken', '__v');
    return {
      delivery
    };
  }
  throw new errors.NotPermittedError('This account is no longer available.');
}

login.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      userName: joi.string().optional().allow(''),
      password: joi.string().required()
    })
    .required()
};

/**
 * does sign up process
 * @param {Object} entity the request body entity
 * @returns {Object} the sign user information
 */
async function signUp(entity) {
  let user = await getUserByEmail(entity.email);
  const username = await getUserByUserName(entity.userName);
  const companyDetail = await Company.findOne({ _id: entity.companyId });
  // const passwordHash = await helper.hashString(entity.password);

  if (username) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Username ${entity.userName} already exists`);
  }
  if (user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Email ${entity.email} already exists`);
  }
  const dateOfBirth = new Date(entity.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');

  const verificationToken = helper.getRandomString(25);
  user = _.extend(entity, {
    email: entity.email,
    userName: entity.userName,
    // passwordHash,
    verificationToken,
    firstName: capitalize(entity.firstName),
    middleName: capitalize(entity.middleName),
    lastName: capitalize(entity.lastName),
    fullName:
      entity.middleName !== ''
        ? capitalize(entity.firstName).concat(' ') +
          capitalize(entity.middleName).concat(' ') +
          capitalize(entity.lastName)
        : capitalize(entity.firstName).concat(' ') + capitalize(entity.lastName),
    dob,
    accessLevel: 'DELIVERY',
    verified: false,
    companyName: companyDetail.companyName
  });

  user = new models.DeliveryPerson(user);
  // generate JWT token
  const token = jwt.sign(_.pick(user, ['id', 'userName', 'email', 'firstName', 'accessLevel']), config.JWT_SECRET, {
    expiresIn: config.JWT_EXPIRATION
  });
  user.accessToken = token;
  await user.save();
  // send an email
  try {
    await sendVerificationEmail(user.email, verificationToken, helper.getHostUrl());
  } catch (ex) {
    await user.remove();
    throw ex;
  }
  return user;
}

/* Validate Sign Up Schema Detail wit Joi */
signUp.schema = {
  entity: signUpSchema.entity
};

/**
 * get login user profile
 * @param {Object} entity the request query
 * @returns {String} the success or failure
 */
const getMyProfile = async (userId) => {
  let user = await helper.ensureEntityExists(models.DeliveryPerson, { _id: userId });
  user = _.omit(
    user.toObject(),
    'verified',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken',
    'passwordHash',
    '_id'
  );
  return user;
};

/**
 * handles the confirm email
 * @param {Object} entity the request query
 * @returns {String} the success or failure
 */
async function confirmEmail(entity) {
  const passwordHash = await helper.hashString(entity.passwordHash);
  const user = await getUserByEmail(entity.email);
  if (!user) {
    throw new errors.NotFoundError(`Sorry we cannot find a user with this email ${entity.email}`);
  }
  if (user.verified) {
    throw new errors.HttpStatusError(
      httpStatus.BAD_REQUEST,
      `Delivery person with this email ${entity.email} has 
     already been  verified`
    );
  }

  if (user.verificationToken === entity.verificationToken) {
    user.verified = true;
    user.passwordHash = passwordHash;
    user.verificationToken = null;
    await user.save();
  } else {
    throw new errors.ValidationError("Sorry but the verification code doesn't match");
  }

  return { message: `Congratulations, ${entity.email} has been verified successfully` };
}

confirmEmail.schema = {
  entity: joi.object().keys({
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
    passwordHash: joi
      .string()
      /** Password must be minimum of eight characters, with at least */
      /** one uppercase letter, one lowercase letter, one number and one special character */
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      .required(),
    verificationToken: joi.string().required()
  })
};

/**
 * Confirm mobile number
 * @param {String} mobile mobile number
 * @returns {Object} the user information
 */
async function confirmMobile(entity) {
  const client = await DeliveryPerson.findOne({
    mobile: entity.mobile
  });
  if (!client) {
    throw new errors.NotFoundError('Sorry, user does not exist');
  }
  if (client.phoneVerified === true) {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, 'Delivery person is already verified');
  }

  if (client.verificationToken === entity.verificationToken) {
    client.phoneVerified = true;
    client.isActive = true;
    client.verificationToken = null;
    await client.save();
  } else {
    throw new errors.ValidationError("Sorry, verification token doesn't match");
  }

  return { message: 'Congratulations, you account have been verified successfully' };
}

confirmMobile.schema = {
  entity: joi.object().keys({
    mobile: joi.string().required(),
    verificationToken: joi.string().required()
  })
};

/**
 * Resend verification token through mobile
 * @param {String} mobile mobile number of client
 * @returns {Object} the user information
 */
async function resendVerificationToken(entity) {
  const client = await DeliveryPerson.findOne({
    mobile: entity.mobile
  });
  if (!client) {
    throw new errors.NotFoundError('Sorry, user does not exist');
  }
  if (client.phoneVerified === true) {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, 'Delivery person is already verified');
  }

  client.verificationToken = entity.verificationToken;

  await client.save();

  return { message: 'Verification token have been send in your mobile successfully.' };
}

/**
 * does forgot password process
 * @param {Object} entity the request body entity
 * @returns {Object} the user information
 */
async function forgotPassword(entity) {
  const user = await helper.ensureEntityExists(
    models.DeliveryPerson,
    { email: entity.email },
    `${entity.email} could not be found`
  );
  // generate token
  const verificationToken = helper.getRandomString(25);
  // update user information in database
  user.forgotPasswordToken = verificationToken;
  await user.save();

  // send an email
  await sendForgotPasswordEmail(user.email, verificationToken, helper.getHostUrl());

  return 'Please check your email for the next step for password reset.';
}

forgotPassword.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required()
    })
    .required()
};

/**
 * handles the reset password
 * @param {Object} entity the request body
 * @returns {String} the success or failure
 */
async function resetPassword(entity) {
  const user = await getUserByEmail(entity.email);

  if (!user) {
    throw new errors.NotFoundError(`${entity.email} not found`);
  }

  const newMatched = await helper.validateHash(entity.newPassword, user.passwordHash);

  if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else if (user.forgotPasswordToken === entity.verificationToken) {
    user.passwordHash = await helper.hashString(entity.newPassword);
    user.forgotPasswordToken = null;
    await user.save();
  } else {
    throw new errors.AuthenticationRequiredError('Sorry but your verification token is not valid.');
  }

  return { message: 'Your password has been reset successfully, please log in to continue!' };
}

resetPassword.schema = {
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        /** Password must be minimum of eight characters, with at least */
        /** one uppercase letter, one lowercase letter, one number and one special character */
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      // oldPassword: joi
      //   .string()
      //   /** Password must be minimum of eight characters, with at least */
      //   /** one uppercase letter, one lowercase letter, one number and one special character */
      //   .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      //   .required(),
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
      verificationToken: joi.string().required()
    })
    .required()
};

/**
 * handles the update password
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function changePassword(userId, entity) {
  const user = await helper.ensureEntityExists(models.DeliveryPerson, { _id: userId });
  const matched = await helper.validateHash(entity.oldPassword, user.passwordHash);
  const newMatched = await helper.validateHash(entity.newPassword, user.passwordHash);

  if (!matched) {
    throw new errors.HttpStatusError(401, 'Current Password is incorrect.');
  } else if (newMatched) {
    throw new errors.HttpStatusError(401, 'Your new password cannot be same as your current password.');
  } else {
    user.passwordHash = await helper.hashString(entity.newPassword);
    user.forgotPasswordToken = null;
    user.accessToken = null;
    await user.save();
  }
  return 'Your Password has been changed successfully.';
}

changePassword.schema = {
  userId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      oldPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required()
    })
    .required()
};

/**
 * handles logout
 * @param {Object} userId the user id
 */
async function logout(userId) {
  const user = await helper.ensureEntityExists(models.DeliveryPerson, { _id: userId });
  if (!user.accessToken) {
    throw new errors.NotPermittedError(`user ${userId} is already logged out`);
  }
  user.accessToken = null;
  await user.save();
}

logout.schema = {
  userId: joi.string().required()
};

/* Update my Profile */
async function updateMyProfile(userId, entity) {
  const user = await helper.ensureEntityExists(
    models.DeliveryPerson,
    { _id: userId },
    'The DeliveryPerson with Id does not exist.'
  );

  if (!user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Delivery person is not found. Try Refreshing the page');
  }

  _.assignIn(user, entity);

  await user.save();

  return {
    status: 200,
    message: 'Delivery person has been updated successfully'
  };
}

/* Validate user by access token */
const validateUser = async (token) => {
  let user = {};

  user = await DeliveryPerson.findOne({
    accessToken: token
  });

  if (typeof user === 'undefined' || user === {}) {
    user = await Client.findOne({
      accessToken: token
    });
  }

  return user;
};

const getAllLogManagement = async (search, options) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const result = LogManagementModel.paginate(search, optionValue);
  return result;
};

const getAllNotification = async (search, options, io) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const result = await NotificationModel.paginate(search, optionValue);

  io.emit('notification', result);

  return result;
};

async function markNotificationAsRead(notificationId) {
  const notification = NotificationModel.findById(notificationId);
  if (notification.isRead === true) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This notification is already marked as read');
  }

  await NotificationModel.updateOne({ _id: notificationId }, { isRead: true });

  return {
    status: 200,
    message: 'Notification have been marked as read'
  };
}

async function markNotificationAsUnread(notificationId) {
  const notification = NotificationModel.findById(notificationId);

  if (notification.isRead === false) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This notification is already marked as unread');
  }

  const entity = { isRead: false };
  await NotificationModel.updateOne({ _id: notificationId }, entity);

  return {
    status: 200,
    message: 'Notification have been marked as unread'
  };
}

async function deleteNotification(notificationId) {
  const notification = await helper.ensureEntityExists(
    NotificationModel,
    { _id: notificationId },
    'The notification does not exist.'
  );

  notification.remove();

  return {
    status: 200,
    message: 'Notification is deleted Successfully'
  };
}
async function readAllNotification(createdBy) {
  await NotificationModel.updateMany({ createdBy }, { $set: { isRead: true } });

  return {
    status: 200,
    message: 'All Notification have been marked as read'
  };
}
const getCompanyNameById = async (companyId) => {
  const company = Company.findOne({ _id: companyId });
  // console.log("companyName", company.companyName);
  return company;
};

/**
 * @param {Object} entity the mobile of client
 * @returns {Object} the entity
 */
async function sendOTPCODEforDeliveryVerification(mobile) {
  const clientDetail = await helper.ensureEntityExists(Client, { mobile: mobile.mobile }, 'Client does not exist');
  const randomNumber = Math.floor(10000 + Math.random() * 90000);
  const buyDetail = await BuyNow.findOne({ createdBy: clientDetail._id });
  if (!buyDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `The person with ${mobile} number has not placed any order`);
  }

  const value = await helper.sendOTPCodeFromSparrow(config.sparrowToken, mobile, clientDetail.fullName, randomNumber);
  if (value.status === 200) {
    clientDetail.deliveryToken = randomNumber;
    clientDetail.save();
    return { message: 'Verification token has been send successfully' };
  }
  throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Unable to send delivery verification token');
}

/**
 * Confirm mobile number
 * @param {String} mobile mobile number
 * @returns {Object} success message
 */
async function verifyDeliveryToken(userId, entity) {
  const { deliveryToken } = entity;
  const client = await helper.ensureEntityExists(Client, { deliveryToken }, 'Delivery token does not match');

  if (!client) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'Not able to find customer');
  }

  const buyDetail = await BuyNow.find({
    createdBy: client._id,
    'onTheWayDetail.deliveryPersonId': userId,
    deliveryStatus: 'on the way'
  });

  if (buyDetail.deliveryStatus === 'delivery problem') {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, 'Sorry, delivery problem occurred on the way');
  }
  if (!client) {
    throw new errors.HttpStatusError(httpStatus.BAD_REQUEST, "Sorry, delivery verification token doesn't match");
  }
  if (client.deliveryToken === deliveryToken) {
    buyDetail.map((res) => {
      res.deliveredDetail = res.onTheWayDetail;
      res.deliveryStatus = 'delivered';

      return res.save();
    });
    await client.save();
  }
  return { message: 'You have successfully delivered product. Thank you!' };
}

/* Cancel order by delivery person */
async function cancelOrderByDeliveryBoy(userId, buyId, entity) {
  const cancelList = await helper.ensureEntityExists(BuyNow, { _id: buyId }, 'Buy now detail not found');
  const deliveryPersonDetail = await DeliveryPerson.findOne({ _id: userId });
  cancelList.deliveryCancelDetail.cancelMessage = entity.deliveryCancelDetail.cancelMessage;
  cancelList.deliveryStatus = 'delivery problem';
  cancelList.deliveryCancelDetail.cancelledBy = userId;
  cancelList.deliveryCancelDetail.cancelName = deliveryPersonDetail.fullName;
  cancelList.deliveryCancelDetail.cancelEmail = deliveryPersonDetail.email;
  cancelList.deliveryCancelDetail.cancelledAt = new Date();
  cancelList.save();
  return cancelList;
}
module.exports = {
  login,
  signUp,
  logout,
  getMyProfile,
  confirmEmail,
  forgotPassword,
  resetPassword,
  changePassword,
  updateMyProfile,
  sendVerificationEmail,
  validateUser,
  getAllLogManagement,
  getAllNotification,
  markNotificationAsRead,
  markNotificationAsUnread,
  deleteNotification,
  readAllNotification,
  confirmMobile,
  resendVerificationToken,
  getCompanyNameById,
  sendOTPCODEforDeliveryVerification,
  verifyDeliveryToken,
  cancelOrderByDeliveryBoy
};
