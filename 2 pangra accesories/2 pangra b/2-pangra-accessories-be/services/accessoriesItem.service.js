/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
const httpStatus = require('http-status');
const errors = require('common-errors');
const { Accessories, CartModel, BuyNow } = require('../models');
const ApiError = require('../utils/ApiError');
const helper = require('../common/helper');

/**
 * Create a Accessories by admin
 * @param {Object} accessoriesBody
 * @returns {Promise<Accessories>}
 */
const createAccessoriesItem = async (categoryBody, userId) => {
  const item = Accessories.create(categoryBody);
  if (!item) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Accessories not found');
  }
  await helper.createLogManagement(userId, 'Add Data', 'Add Accessories', 'Accessories Item');
  return item;
};

/**
 * Query for Accessories
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAccessoriesItems = async (filter, options) => {
  // filter.$and = [{ status: true }];
  const values = await Accessories.paginate(filter, options);

  return values;
};

/**
 * Get Comment by id
 * @param {ObjectId} id
 * @returns {Promise<Comment>}
 */
const getAccessoriesItem = async (id) => {
  await Accessories.updateOne({ _id: id }, { $inc: { numViews: 1 } });
  await BuyNow.updateOne({ accessoriesId: id }, { $inc: { numViews: 1 } });
  const result = Accessories.findOne({ _id: id, status: true });
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Accessories not found');
  }
  return result;
};

/**
 * Update Accessories by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<Accessories>}
 */
const updateAccessoriesItemById = async (id, updateBody, userId) => {
  const result = await Accessories.findOne({ _id: id });
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Accessories not found');
  }
  await Accessories.updateOne({ _id: id }, updateBody);
  await helper.createLogManagement(userId, 'Update Data', 'Update Accessories', 'Accessories Item');

  return {
    code: 201,
    message: 'Accessories is updated successfully'
  };
};

/**
 * Delete Accessories by id
 * @param {ObjectId} commentId
 * @returns {Promise<Comment>}
 */
const deleteAccessoriesItemById = async (userId, id) => {
  const result = await Accessories.findOne({ _id: id });
  const ifAlreadyBooked = await BuyNow.findOne({ accessoriesId: id });
  const ifAlreadyExist = await CartModel.findOne({ accessoriesId: id });
  const ifAlreadyInWishList = await Accessories.find({ wishlist: { $exists: true, $ne: [] } });
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Accessories is not found');
  }
  if (ifAlreadyBooked) {
    throw new ApiError(httpStatus.NOT_FOUND, 'You cannot delete this accessories somebody have ordered this product');
  }
  if (ifAlreadyExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'You cannot delete this accessories somebody have added to cart');
  }
  if (ifAlreadyInWishList) {
    throw new errors.HttpStatusError(401, 'You cannot delete this product this product is on wishlist');
  }

  await Accessories.updateOne({ _id: id }, { status: false }, { deletedBy: userId });
  await helper.createLogManagement(userId, 'Remove Data', 'Remove Accessories', result.productTitle);
  return result;
};

/**
 * Add Accessories in feature home page
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Accessories>}
 */
const addAccessoriesInFeature = async (accessoriesId, userId) => {
  const accessories = await helper.ensureEntityExists(
    Accessories,
    { _id: accessoriesId },
    'Accessories does not exist'
  );
  const accessoriesFeatureDetail = await Accessories.count({ hasInFeatured: true });
  if (accessoriesFeatureDetail > 10) {
    // accessoriesFeatureDetail.hasInFeaturePending = true;
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Accessories feature has reached the limit of 10 post try again later.'
    );
  }
  if (accessories.hasInFeatured === true) {
    throw new errors.HttpStatusError(401, 'This item is already added to Feature');
  }
  accessories.hasInFeatured = true;
  await accessories.save();
  await helper.createLogManagement(userId, 'Add Data', 'Add Accessories in feature', accessories.productTitle);

  return {
    code: 201,
    message: 'Accessories is added to Feature'
  };
};

/**
 * Remove Accessories in feature home page
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Accessories>}
 */
const removeAccessoriesFromFeature = async (accessoriesId, userId) => {
  const accessoriesDetail = await helper.ensureEntityExists(
    Accessories,
    { _id: accessoriesId },
    'Accessories does not exist'
  );
  if (accessoriesDetail.hasInFeatured === false) {
    throw new errors.HttpStatusError(401, 'Seller Product is already removed from feature');
  }
  accessoriesDetail.hasInFeatured = false;

  await accessoriesDetail.save();
  await helper.createLogManagement(
    userId,
    'Remove Data',
    'Remove Accessories From Feature',
    'Accessories From Feature'
  );

  return {
    code: 201,
    message: 'Accessories is remove from feature'
  };
};
// /**
//  * Approved comment by admin
//  * @param {ObjectId} userId
//  * @returns {Promise<Comment>}
//  */
// const approveCommentByAdmin = async (commentId) => {
//   const comment = await Comment.findOne({ _id: commentId });
//   if (!comment) {
//     throw new ApiError(httpStatus.NOT_FOUND, 'Comment is not found');
//   }
//   if (comment.isApproved === true) {
//     throw new ApiError(httpStatus.NOT_FOUND, 'Comment is already approved');
//   }
//   comment.isApproved = true;
//   comment.save();
//   return comment;
// };

/**
 * Get All feature accessories in home page
 * @param {ObjectId} empty
 * @returns {Promise<Accessories>}
 */

const getAllFeaturedAccessoriesNoAuth = async () => {
  const values = await Accessories.find({ hasInFeatured: true }).sort({ createdAt: -1 });
  return values;
};

/**
 * Remove Accessories in hot deal by admin
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Accessories>}
 */
const addAccessoriesInHotDeals = async (accessoriesId, userId) => {
  const accessories = await helper.ensureEntityExists(
    Accessories,
    { _id: accessoriesId },
    'Accessories does not exist'
  );
  const accessoriesFeatureDetail = await Accessories.count({ hasInHotDeal: true, status: true });
  if (accessoriesFeatureDetail > 10) {
    // accessoriesFeatureDetail.hasInFeaturePending = true;
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Accessories in hot deals has reached the limit of 10 post try again later.'
    );
  }
  if (accessories.hasInHotDeal === true) {
    throw new errors.HttpStatusError(401, 'This item is already added to hot deals');
  }
  accessories.hasInHotDeal = true;
  await accessories.save();
  await helper.createLogManagement(
    userId,
    'Add Data',
    'Add Accessories in Hot Deals',
    'Accessories in Hot Deals',
    userId.createdBy
  );

  return {
    code: 201,
    message: 'Accessories is added to hot deals'
  };
};

/**
 * Add Accessories in hot deal by admin
 * @param {ObjectId} accessoriesId
 * @returns {Promise<Accessories>}
 */
const removeAccessoriesInHotDeals = async (accessoriesId, userId) => {
  const accessoriesDetail = await helper.ensureEntityExists(
    Accessories,
    { _id: accessoriesId },
    'Accessories does not exist'
  );
  if (accessoriesDetail.hasInHotDeal === false) {
    throw new errors.HttpStatusError(401, 'Seller Products is already removed from hot deals');
  }
  accessoriesDetail.hasInHotDeal = false;

  await accessoriesDetail.save();
  await helper.createLogManagement(
    userId,
    'Remove Data',
    'Remove Accessories In Hot Deals',
    'Accessories In Hot Deals'
  );

  return {
    code: 201,
    message: 'Accessories is remove from hot deals'
  };
};

/**
 * Get all hot deals
 * @param {ObjectId} empty
 * @returns {Promise<Accessories>} get hot deals
 */
const getAllHotDeals = async () => {
  const hotDealItem = await Accessories.find({ status: true, hasInHotDeal: true });
  const hotDealLength = hotDealItem.length;
  const nonHotDealItem = await Accessories.find({ status: true, hasInHotDeal: false })
    .sort({ numViews: -1 })
    .limit(12 - hotDealLength);

  const hotDeals = hotDealItem.concat(nonHotDealItem);

  return {
    hotDeals
  };
};

/**
 * Query for Hot Deal Accessories
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getHotDealAccessoriesItems = async (filter, options) => {
  filter.$and = [{ hasInHotDeal: true }, { status: true }];
  const values = await Accessories.paginate(filter, options);
  return values;
};

/**
 * Query for Product Recommendation
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getRecommendedAccessoriesItem = async (userId) => {
  const values = await CartModel.find({ createdBy: userId })
    .populate('accessoriesId', 'subCategoryId')
    .sort({ createdAt: -1 });
  const subCatId = values[0].accessoriesId.subCategoryId;
  const recommendedProducts = await Accessories.find({ subCategoryId: subCatId }).limit(10);
  return recommendedProducts;
};

/**
 * Delete InActive Accessories Item
 * @returns {Promise<QueryResult>}
 */
const deleteInActiveAccessoriesItem = async (accessId) => {
  const item = await Accessories.findOne({ _id: accessId, status: false });
  if (!item) {
    throw new errors.HttpStatusError(401, 'Active Product Cannot Be Deleted');
  }
  const value = await Accessories.deleteOne({ _id: accessId });
  return value;
};

const deleteAccessoriesItem = async (userId, deleteId) => {
  const value = Accessories.findOne({ _id: deleteId });
  if (!value) {
    throw new errors.HttpStatusError(401, 'Active Product Cannot Be Deleted');
  }
  const buyDetail = await BuyNow.findOne({ accessoriesId: deleteId });
  if (buyDetail) {
    throw new ApiError(httpStatus.NOT_FOUND, 'You cannot delete this product before deleting related order');
  }
  const cartDetail = await CartModel.findOne({ accessoriesId: deleteId });
  if (cartDetail) {
    throw new ApiError(httpStatus.NOT_FOUND, 'You cannot delete this product before deleting related cart');
  }

  const wishListDetail = await Accessories.find({ wishlist: { $exists: true, $ne: [] } });
  if (wishListDetail) {
    throw new errors.HttpStatusError(401, 'You cannot delete this product this product is on wishlist');
  }
  const item = await Accessories.deleteOne({ _id: deleteId });
  await helper.createLogManagement(userId, 'Delete Data', 'Delete Accessories', 'Product');
  return item;
};
module.exports = {
  createAccessoriesItem,
  getAccessoriesItems,
  getAccessoriesItem,
  updateAccessoriesItemById,
  deleteAccessoriesItemById,
  addAccessoriesInFeature,
  removeAccessoriesFromFeature,
  getAllFeaturedAccessoriesNoAuth,
  addAccessoriesInHotDeals,
  removeAccessoriesInHotDeals,
  getAllHotDeals,
  getHotDealAccessoriesItems,
  getRecommendedAccessoriesItem,
  deleteInActiveAccessoriesItem,
  deleteAccessoriesItem
  // approveCommentByAdmin
};
