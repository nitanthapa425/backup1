/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const _ = require('lodash');
const httpStatus = require('http-status');
// eslint-disable-next-line import/no-extraneous-dependencies
const temporal = require('temporal');
const errors = require('common-errors');
const cron = require('node-cron');
const helper = require('../common/helper');
// const { VehicleDetailService } = require('.');

const adminEmail = 'arjunsubedi360@gmail.com';
const superAdminEmail = 'susandhakal112@gmail.com';
const buyerEmail = 'arjunsubedi360@gmail.com';
const sellerEmail = 'susandhakal112@gmail.com';
const buyerName = 'Arjun User Subedi';
// eslint-disable-next-line object-curly-newline
const {
  VehicleSellDraftModel,
  VehicleSellModel,
  CartModel,
  BookModel,
  Client,
  WishlistModel,
  // MeetingSchedule,
  PostExpiryDateModel,
  OfferModel
} = require('../models');

/**
 * @param {Object} entity the new vehicleDetail
 * @returns {Object} the entity
 */
const createVehicleDetailSell = async (vehicleDetail) => {
  if (vehicleDetail.hasInFeature === true) {
    const vehicleSellFeatureDetail = await VehicleSellModel.count({ hasInFeature: true, isApproved: true });
    if (vehicleSellFeatureDetail > 10) {
      throw new errors.HttpStatusError(
        httpStatus.CONFLICT,
        'Feature Vehicle Sell has reached the limit of 10 post try again later'
      );
    }
  }

  const value = await VehicleSellModel.create(vehicleDetail);

  if (value) {
    await helper.createSellerLogManagement(
      vehicleDetail.createdBy,
      'Add Sellers Vehicle',
      'Sellers Vehicle',
      vehicleDetail.vehicleName,
      'CLIENT'
    );
    await PostExpiryDateModel.create({
      email: vehicleDetail.email,
      sellerName: vehicleDetail.fullName,
      sellerVehicleId: value._id,
      adminEmail,
      postExpiryDate: value.postExpiryDate
    });
  }
  return value;
};

/**
 * @param {Object} entity the new vehicleDetail
 * @returns {Object} the entity
 */
const createVehicleDetailSellByAdmin = async (vehicleDetail, accessLevel) => {
  const value = await VehicleSellModel.create(vehicleDetail);

  if (value) {
    await helper.createNotification(
      'Add',
      'Vehicle Sell',
      'SELLER',
      value._id,
      value.createdBy,
      value.createdBy,
      value.vehicleName,
      value.bikeImagePath
    );
    await helper.createSellerLogManagement(
      vehicleDetail.createdBy,
      'Add Sellers Vehicle',
      'Sellers Vehicle',
      vehicleDetail.vehicleName,
      accessLevel
    );
    // await PostExpiryDateModel.create({
    //   email: vehicleDetail.email,
    //   sellerName: vehicleDetail.fullName,
    //   sellerVehicleId: value._id,
    //   adminEmail,
    //   postExpiryDate: value.postExpiryDate
    // });
  }
  return value;
};

/**
 * @param {Object} entity the new vehicleDetail
 * @returns {Object} the entity
 */
//

/**
 * @returns {Object} get  all vehicleDetail
 */
const getSellVehicleDetails = async (search, options) => {
  search.$and = [{ isApproved: true }, { isApproved: { $exists: true } }];
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };

  const value = VehicleSellModel.paginate(search, optionValue);
  return value;
};

/**
 * @returns {Object} get  Specific Vehicle Detail
 */
const getVehicleSellDetail = async (tagId) => {
  await VehicleSellModel.updateOne({ _id: tagId }, { $inc: { numViews: 1 } });
  const vehicleDetailById = await VehicleSellModel.findOne({ _id: tagId });
  if (!vehicleDetailById) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Vehicle Seller in Feature is not found. Try refreshing page.'
    );
  }
  // const vehicleDetail = await VehicleDetailService.getVehicleDetail(vehicleDetailById.vehicleDetailId);
  // const value = _.omit(vehicleDetail.toObject(), '_id', 'vehicleName');

  const mergeValue = {
    sellerValue: vehicleDetailById,
    // vehicleDetailValue: value
  };
  return mergeValue;
};

/**
 * handles the update  Vehicle Details
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateVehicleSellDetail(vehicleId, tag) {
  const vehicleSellDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: vehicleId },
    'Vehicle does not exist.'
  );

  if (!vehicleSellDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle is not found. Try Refreshing the page');
  }

  _.assignIn(vehicleSellDetail, tag);

  await vehicleSellDetail.save();

  return {
    code: 200,
    message: 'Seller/s Vehicle Details updated successfully'
  };
}

/**
 * Delete vehicleDetail
 * @returns {Object} Search result
 */
/**
 * delete client profile by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteVehicleSellDetail(deleteId) {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: deleteId },
    'The Vehicle detail does not exist.'
  );
  vehicleDetail.isApproved = false;

  await vehicleDetail.save();

  if (vehicleDetail) {
    await CartModel.deleteOne({ sellerVehicleId: deleteId });
    await BookModel.deleteOne({ vehicleId: deleteId });
    await WishlistModel.deleteOne({ sellerVehicleId: deleteId });
  }
  return {
    status: 200,
    message: 'Vehicle detail is deleted Successfully'
  };
}

deleteVehicleSellDetail.schema = {
  deleteId: joi.string().required()
};

/* Add Feature in Home Page by customer */
const addFeatureInHomePage = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  const vehicleSellFeatureDetail = await VehicleSellModel.count({ hasInFeature: true, isApproved: true });
  if (vehicleSellFeatureDetail > 10) {
    vehicleSellFeatureDetail.hasInFeaturePending = true;
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Feature Vehicle Sell has reached the limit of 10 post try again later.'
    );
  }
  if (vehicleDetail.hasInFeature === true) {
    throw new errors.HttpStatusError(401, 'Seller Vehicle is already added to Feature');
  }
  vehicleDetail.hasInFeature = true;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller Vehicle is added to Feature'
  };
};

// /* Remove brand from Home Page */
// const removeFeatureInHomePage = async (sellerVehicleId) => {
//   const vehicleDetail = await helper.ensureEntityExists(
//     VehicleSellModel,
//     { _id: sellerVehicleId },
//     'Brand Vehicle does not exist'
//   );
//   if (vehicleDetail.hasInFeature === false) {
//     throw new errors.HttpStatusError(401, 'Seller Vehicle is already removed to Feature');
//   }
//   vehicleDetail.hasInFeature = false;
//   const totalVehicleDetailInFeature = await VehicleSellModel.count({ hasInFeature: true, isApproved: true });
//   // eslint-disable-next-line max-len
//   const value = await VehicleSellModel.find({ isApproved: true, hasInFeaturePending: true })
//     .sort({ createdAt: -1 })
//     .limit(10 - totalVehicleDetailInFeature);
//   value.forEach(async (res) => {
//     if (totalVehicleDetailInFeature <= 10) {
//       await VehicleSellModel.updateOne({ _id: res._id }, {});
//       const clientValue = await ClientService.getClientById(res.createdBy);
//       helper.adFeatureEmailToSeller(clientValue.email, clientValue.fullName, res._id, adminEmail, helper.getHostUrl());
//     }
//   });
//   await vehicleDetail.save();
//   return {
//     code: 201,
//     message: 'Seller Vehicle is remove from Feature'
//   };
// };

/* Upload Brand Image In vehicleDetail  */
const uploadVehicleSellImages = async (vehicleId, tag) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: vehicleId },
    `The Brand Vehicle ${vehicleId} does not exist.`
  );
  vehicleDetail.bikeImagePath.push(...tag.vehicleImgPath);

  await vehicleDetail.save();

  return vehicleDetail;
};

/**
 * @param {Object} entity the new vehicleDetail
 * @returns {Object} the entity
 */
const createVehicleSellDraftService = async (tag) => {
  const value = VehicleSellDraftModel.create(tag);
  return value;
};

/**
 * Search Vehicle Detail
 * @param {String} name the any in Vehicle
 */
async function getVehicleSellDraftDetailsService(search, options) {
  const value = await VehicleSellDraftModel.paginate(search, options);
  return value;
}

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const getVehicleSellDraftDetailsByIdService = async (tagId) => {
  const draftValue = await helper.ensureEntityExists(
    VehicleSellDraftModel,
    { _id: tagId },
    'The Draft Vehicle Sell does not exist.'
  );
  return draftValue;
};

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const getUserVehicleSellDraftService = async (userId) => {
  const userDraft = await VehicleSellDraftModel.find({ createdBy: userId });
  return userDraft;
};

/**
 * @returns {Object} get  User vehicle sell detail
 /** */

const getUserVehicleSellService = async (search, options, userId) => {
  search.$and = [{ createdBy: userId }, { isApproved: true }];
  const value = await VehicleSellModel.paginate(search, options);
  return value;
};

/**
 * @returns {Object} Create Draft and save to vehicle service
 */
const createVehicleSellDraftToVehicleService = async (tagId) => {
  // field and moving it to the destination collection
  const draftValue = await helper.ensureEntityExists(
    VehicleSellDraftModel,
    { _id: tagId },
    'The Draft Vehicle Sell does not exist.'
  );

  const value = _.omit(draftValue.toObject(), '_id');
  await VehicleSellModel.create(value);
  draftValue.remove();
  return { message: 'Vehicle Sell Draft value has been  submitted successfully.' };
};

/**
 * handles the update  Vehicle Sell Draft Details
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateVehicleSellDraftService(vehicleId, tag) {
  // const vehicleDetail = await VehicleDetailService.getVehicleDetail(tag.vehicleDetailId);
  const vehicleSellDraftDetail = await helper.ensureEntityExists(
    VehicleSellDraftModel,
    { _id: vehicleId },
    'Vehicle does not exist.'
  );

  if (!vehicleSellDraftDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle is not found. Try Refreshing the page');
  }

  _.assignIn(vehicleSellDraftDetail, tag);

  await vehicleSellDraftDetail.save();

  return {
    status: 200,
    message: 'Seller/s Vehicle Details updated successfully'
  };
}

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const deleteVehicleSellDraftService = async (tagId) => {
  const draftValue = await helper.ensureEntityExists(
    VehicleSellDraftModel,
    { _id: tagId },
    'The Draft Vehicle Sell does not exist.'
  );
  draftValue.remove();
  return 'Vehicle Sell Draft Detail has been deleted successfully';
};

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const addToCart = async (userId, tagId) => {
  const hasInBook = await BookModel.findOne({ vehicleId: tagId, createdBy: userId });
  if (hasInBook) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the book list.');
  }
  const hasInCart = await CartModel.findOne({ sellerVehicleId: tagId, createdBy: userId });
  if (hasInCart) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the cart list.');
  }
  const vehicleSellDetail = await VehicleSellModel.findOne({ _id: tagId, isApproved: true });
  const {
    id,
    vehicleName,
    bikeImagePath,
    color,
    expectedPrice,
    numViews,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified,
    createdBy: sellerId
  } = vehicleSellDetail;
  // eslint-disable-next-line no-sequences
  bikeImagePath.forEach((el) => (el.imageUrl, el.publicKey));
  const sellerVehicleDetail = {
    sellerVehicleId: id,
    vehicleName,
    vehicleImage: bikeImagePath,
    color,
    price: expectedPrice,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified,
    createdBy: userId,
    numViews
  };
  //  Find if this item is in cart
  const addedCart = await CartModel.findOne({ sellerVehicleId: tagId, createdBy: userId });
  if (addedCart) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the cart list.');
  }
  const value = await CartModel.create(sellerVehicleDetail);

  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof bikeImagePath !== 'undefined' && bikeImagePath.length > 0 ? bikeImagePath[0].imageUrl : '';

  if (value) {
    await helper.createNotification('Add', 'Cart', 'BUYER', '/cart', userId, sellerId, vehicleName, vehicleImage);
    await helper.createNotification('Add', 'Cart', 'SELLER', '/sell/view', userId, sellerId, vehicleName, vehicleImage);
    await helper.createAdminNotification(
      'Add',
      'Cart',
      'ADMIN',
      '/cart/view',
      userId,
      sellerId,
      vehicleName,
      vehicleImage
    );
    await helper.createAdminNotification(
      'Add',
      'Cart',
      'SUPER_ADMIN',
      '/cart/view',
      userId,
      sellerId,
      vehicleName,
      vehicleImage
    );
    await helper.createLogManagement(userId, 'Add Cart', 'Carts', vehicleName, 'CLIENT');
  }

  return {
    value
  };
};

const getClientAddToCart = async (userId) => {
  const cllientCartDetail = await CartModel.find({ createdBy: userId });
  //  console.log("cllientCartDetail",wish cllientCartDetail);
  let totalPrice = 0;
  cllientCartDetail.map((ele) => {
    totalPrice += Number(ele.price);
    return totalPrice;
  });
  return {
    cllientCartDetail,
    totalPrice
  };
};

/**
 * Get Cart by id
 * @param {ObjectId} id
 * @returns {Promise<Category>}
 */
const getCartById = async (id) => {
  const cart = CartModel.findOne({ _id: id });
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Cart is not found');
  }
  return cart;
};
/**
 * Delete Cart by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteCart = async (cartId) => {
  const cart = await CartModel.findOne({ sellerVehicleId: cartId });
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Cart is not found');
  }

  const vehicleSell = await VehicleSellModel.findById(cart.sellerVehicleId);

  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof vehicleSell.bikeImagePath !== 'undefined' && vehicleSell.bikeImagePath.length > 0
      ? vehicleSell.bikeImagePath[0].imageUrl
      : '';

  if (cart) {
    await helper.createNotification(
      'Delete',
      'Cart',
      'BUYER',
      '/cart',
      cart.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
    await helper.createNotification(
      'Delete',
      'Cart',
      'SELLER',
      '/sell/view',
      cart.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
    await helper.createAdminNotification(
      'Delete',
      'Cart',
      'ADMIN',
      '/cart/view',
      cart.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
    await helper.createAdminNotification(
      'Delete',
      'Cart',
      'SUPER_ADMIN',
      '/cart/view',
      cart.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
  }

  await CartModel.deleteOne({ sellerVehicleId: cartId });
  return cart;
};
/**
 * @returns {Object} Add seller's vehicle to wishlist
 */
const addTowishlist = async (userId, tagId) => {
  const hasInBook = await BookModel.findOne({ vehicleId: tagId, createdBy: userId });
  if (hasInBook) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the book list.');
  }
  const vehicleSellDetail = await VehicleSellModel.findOne({ _id: tagId, isApproved: true });
  const {
    id,
    vehicleName,
    bikeImagePath,
    color,
    expectedPrice,
    numViews,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified,
    createdBy: sellerId
  } = vehicleSellDetail;

  // eslint-disable-next-line no-sequences
  bikeImagePath.forEach((el) => (el.imageUrl, el.publicKey));

  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof bikeImagePath !== 'undefined' && bikeImagePath.length > 0 ? bikeImagePath[0].imageUrl : '';

  const sellerVehicleDetail = {
    sellerVehicleId: id,
    color,
    price: expectedPrice,
    vehicleName,
    vehicleImage: bikeImagePath,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified,
    createdBy: userId,
    numViews
  };
  //  Find if this item is in wishlist
  const wishlist = await WishlistModel.findOne({ sellerVehicleId: tagId, createdBy: userId });
  if (wishlist) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the wishlist.');
  }
  const value = await WishlistModel.create(sellerVehicleDetail);

  if (value) {
    await helper.createNotification(
      'Add',
      'Wishlist',
      'BUYER',
      '/wish-list',
      userId,
      sellerId,
      vehicleName,
      vehicleImage
    );
    await helper.createNotification(
      'Add',
      'Wishlist',
      'SELLER',
      '/sell/view',
      userId,
      sellerId,
      vehicleName,
      vehicleImage
    );
    // await helper.createAdminNotification(
    //   'Add',
    //   'Wishlist',
    //   'ADMIN',
    //   '/wish-list/view',
    //   userId,
    //   sellerId,
    //   vehicleName,
    //   vehicleImage
    // );
    // await helper.createAdminNotification(
    //   'Add',
    //   'Wishlist',
    //   'SUPER_ADMIN',
    //   '/wish-list/view',
    //   userId,
    //   sellerId,
    //   vehicleName,
    //   vehicleImage
    // );
  }
  return {
    value
  };
};

/* get clients wishlists */
const getClientWishlist = async (userId) => {
  const cllientCartDetail = await WishlistModel.find({ createdBy: userId });
  //  console.log("cllientCartDetail", cllientCartDetail);
  return {
    cllientCartDetail
  };
};

/**
 * Get wishlist by id
 * @param {ObjectId} id
 * @returns {Promise<WishList>}
 */
const getWishlistById = async (id) => {
  const wishlist = WishlistModel.findOne({ _id: id });
  if (!wishlist) {
    throw new httpStatus.NOT_FOUND('Wishlist is not found');
  }
  return wishlist;
};
/**
 * Delete wishlist by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteWishlist = async (wishlistId) => {
  const wishlist = await WishlistModel.findOne({ sellerVehicleId: wishlistId });
  if (!wishlist) {
    throw new httpStatus.NOT_FOUND('Wishlist is not found');
  }

  const vehicleSell = await VehicleSellModel.findById(wishlist.sellerVehicleId);

  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof vehicleSell.bikeImagePath !== 'undefined' && vehicleSell.bikeImagePath.length > 0
      ? vehicleSell.bikeImagePath[0].imageUrl
      : '';

  if (wishlist) {
    await helper.createNotification(
      'Delete',
      'Wishlist',
      'BUYER',
      '/wish-list',
      wishlist.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
    await helper.createNotification(
      'Delete',
      'Wishlist',
      'SELLER',
      '/sell/view',
      wishlist.createdBy,
      vehicleSell.createdBy,
      vehicleSell.vehicleName,
      vehicleImage
    );
  }

  await WishlistModel.deleteOne({ sellerVehicleId: wishlistId });
  return wishlist;
};

/**
 * @returns {Object} get  Specific Vehicle Draft Detail By Id
 */
const addToBookFromWishlist = async (userId, tagId) => {
  const wishlist = await WishlistModel.findOne({ sellerVehicleId: tagId, createdBy: userId });
  const hasInBook = await BookModel.findOne({ vehicleId: tagId, createdBy: userId });
  if (hasInBook) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already booked.');
  }
  // const cart = await CartModel.findOne({ sellerVehicleId: tagId, createdBy: userId });
  // if (cart) {
  //   throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already in the cart.');
  // }
  const clientDetail = await Client.findOne({ _id: userId });
  const { id: clientId, fullName, mobile } = clientDetail;
  // const value = _.omit(clientDetail.toObject(), '_id');
  const {
    sellerVehicleId,
    id,
    vehicleName,
    vehicleImage,
    color,
    price,
    numViews,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified
  } = wishlist;
  // eslint-disable-next-line no-sequences
  vehicleImage.forEach((el) => (el.imageUrl, el.publicKey));
  const bookDetail = {
    clientId,
    sellerVehicleId,
    clientName: fullName,
    clientMobile: mobile,
    cartId: id,
    vehicleName,
    vehicleImage,
    color,
    price,
    createdBy: userId,
    bikeDriven,
    combinelocation,
    ownershipCount,
    isVerified,
    numViews
  };
  //  Find if this item is already booked or not
  const ifBookedAlready = await CartModel.findOne({ sellerVehicleId: tagId, clientId: userId });
  if (!ifBookedAlready) {
    await CartModel.create(bookDetail);
    await WishlistModel.deleteOne({ sellerVehicleId: tagId, createdBy: userId });
  } else {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already added to cart.');
  }
  return {
    code: 200,
    message: 'This item is added to cart successfully.'
  };
};

/* Get Hot Deals */
const getVehicleSellHotDeals = async () => {
  const hotDealItem = await VehicleSellModel.find({ isApproved: true, hasInHotDeal: true });
  const hotDealLength = hotDealItem.length;
  const nonHotDealItem = await VehicleSellModel.find({ isApproved: true, hasInHotDeal: false })
    .sort({ numViews: -1 })
    .limit(12 - hotDealLength);

  const hotDeals = hotDealItem.concat(nonHotDealItem);

  return {
    hotDeals
  };
};

const getAdminVehicleSellHotDeals = async (search, options) => {
  const paginateResult = await VehicleSellModel.paginate(search, options);

  return paginateResult;
};

/* Add hot deals in Home Page */
const addToHotDeals = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  const vehicleSellFeatureDetail = await VehicleSellModel.count({ hasInHotDeal: true });
  if (vehicleSellFeatureDetail > 12) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Hot details Vehicle Sell has reached the limit of 12 vehicle'
    );
  }

  if (vehicleDetail.hasInHotDeal === true) {
    throw new errors.HttpStatusError(401, 'Seller Vehicle is already added to Hot Deals');
  }
  vehicleDetail.hasInHotDeal = true;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller Vehicle is added to Hot Deals'
  };
};

/* Remove hot deals from Home Page */
const removeFromHotDeal = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Brand Vehicle does not exist'
  );
  if (vehicleDetail.hasInHotDeal === false) {
    throw new errors.HttpStatusError(401, 'Seller Vehicle is already removed to Hot deals');
  }
  vehicleDetail.hasInHotDeal = false;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller Vehicle is remove from Hot deals'
  };
};

/**
 * @returns {Object} get  all Add to cart
 */
const getAddToCart = async (search, options) => {
  // search.$or = [{ isApproved: true }, { isApproved: { $exists: true } }];
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };
  const value = CartModel.paginate(search, optionValue);
  return value;
};

/**
 * @returns {Object} get  all Add to cart
 */
const getWishlist = async (search, options) => {
  // search.$or = [{ isApproved: true }, { isApproved: { $exists: true } }];
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };
  const value = WishlistModel.paginate(search, optionValue);
  return value;
};

/**
 * Delete Wishlist by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteWishlistByAdmin = async (bookId) => {
  const wishlist = await WishlistModel.findOne({ _id: bookId });
  if (!wishlist) {
    throw new httpStatus.NOT_FOUND('Wishlist is not found');
  }

  await WishlistModel.deleteOne({ _id: bookId });
  return wishlist;
};
/**
 * Delete Cart by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteCartByAdmin = async (bookId) => {
  const cart = await CartModel.findOne({ _id: bookId });
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Cart is not found');
  }

  await CartModel.deleteOne({ _id: bookId });
  return cart;
};

/**
 * get Book by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const getBookedVehicleByAdmin = async (bookId) => {
  const book = await BookModel.findOne({ _id: bookId });
  if (!book) {
    throw new httpStatus.NOT_FOUND('Booked vehicle is not found');
  }
  const { vehicleId } = book;
  const bookVehicleSell = await VehicleSellModel.findOne({ _id: vehicleId });
  if (!bookVehicleSell) {
    throw new httpStatus.NOT_FOUND("Seller's vehicle with this book detail is deleted.");
  }
  return bookVehicleSell;
};
/**
 * get Wishlist by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const getWishlistByAdmin = async (wishlistId) => {
  const wishlist = await WishlistModel.findOne({ _id: wishlistId });
  if (!wishlist) {
    throw new httpStatus.NOT_FOUND('Wishlist is not found');
  }
  const { sellerVehicleId } = wishlist;
  const wishlistVehicleSell = await VehicleSellModel.findOne({ _id: sellerVehicleId });
  if (!wishlistVehicleSell) {
    throw new httpStatus.NOT_FOUND("Seller's vehicle is not found.");
  }
  return wishlistVehicleSell;
};
/**
 * get Cart by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const getCartByAdmin = async (cartId) => {
  const cart = await CartModel.findOne({ _id: cartId });
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Cart is not found');
  }
  const { sellerVehicleId } = cart;
  const cartVehicleSell = await VehicleSellModel.findOne({ _id: sellerVehicleId });
  if (!cartVehicleSell) {
    throw new httpStatus.NOT_FOUND("Seller's vehicle is not found.");
  }
  return cartVehicleSell;
};

/* Verify Seller Vehicle by Two Pangra */
const verifySellerVehicle = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  if (vehicleDetail.isApproved === false) {
    throw new errors.HttpStatusError(401, 'The vehicle must be approved for verification');
  }
  if (vehicleDetail.isVerified === true) {
    throw new errors.HttpStatusError(401, 'Seller/s Vehicle is already verified.');
  }
  vehicleDetail.isVerified = true;
  await BookModel.update({ vehicleId: sellerVehicleId }, { isVerified: true });
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller/s Vehicle is verified successfully'
  };
};

/* Unverify Seller Vehicle by Two Pangra */
const unverifySellerVehicle = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  if (vehicleDetail.isApproved === false) {
    throw new errors.HttpStatusError(401, 'The vehicle must be approved to unverify');
  }

  if (vehicleDetail.isVerified === false) {
    throw new errors.HttpStatusError(401, 'Seller/s Vehicle is already unverified.');
  }
  vehicleDetail.isVerified = false;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller/s Vehicle is unverified successfully'
  };
};

/* Unverify Seller Vehicle by Two Pangra */
const updateLocation = async (sellerId, locationBody) => {
  const vehicleDetail = await helper.ensureEntityExists(Client, { _id: sellerId }, 'Seller  does not exist');
  if (vehicleDetail.isActive === true) {
    await Client.update({ _id: sellerId }, locationBody);
  }

  return {
    code: 201,
    message: 'Seller/s location updated successfully'
  };
};

/* Approve Seller Vehicle by Two Pangra */
const approveSellerVehicle = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  if (vehicleDetail.isApproved === true) {
    throw new errors.HttpStatusError(401, 'Seller/s Vehicle is already approved.');
  }
  vehicleDetail.isApproved = true;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller/s Vehicle is approved successfully'
  };
};

/* Unapproved Seller Vehicle by Two Pangra */
const unApproveSellerVehicle = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  if (vehicleDetail.isApproved === false) {
    throw new errors.HttpStatusError(401, 'Seller/s Vehicle is already unapproved.');
  }
  vehicleDetail.isApproved = false;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller/s Vehicle is unapproved successfully'
  };
};

/* Approve Seller Vehicle by Two Pangra */
const meetingSchedule = async (bookId, meetingBody) => {
  const bookDetail = await helper.ensureEntityExists(BookModel, { _id: bookId });
  if (!bookDetail) {
    throw new errors.HttpStatusError(401, 'Book detail does not exist');
  }
  // eslint-disable-next-line object-curly-newline
  const { _id, clientName, vehicleId, vehicleName } = bookDetail;
  // eslint-disable-next-line object-curly-newline
  const { combineLocation, nearByLocation, meetingPlace, meetingDateTime } = meetingBody;

  const values = await BookModel.updateOne(
    { _id: bookId },
    {
      $set: {
        'meetingSchedule.combineLocation': combineLocation,
        'meetingSchedule.nearByLocation': nearByLocation,
        'meetingSchedule.meetingPlace': meetingPlace,
        'meetingSchedule.meetingDateTime': meetingDateTime
      }
    }
  );
  await helper.adminEmailMeetingSchedule(
    adminEmail,
    buyerName,
    clientName,
    vehicleName,
    _id,
    meetingSchedule._id,
    helper.getHostUrl()
  );
  await helper.superAdminEmailMeetingSchedule(
    superAdminEmail,
    buyerName,
    clientName,
    vehicleName,
    meetingPlace,
    meetingDateTime,
    _id,
    meetingSchedule._id,
    helper.getHostUrl()
  );
  await helper.buyerEmailMeetingSchedule(buyerEmail, buyerName, vehicleId, adminEmail, helper.getHostUrl());
  await helper.sellerEmailMeetingSchedule(sellerEmail, clientName, vehicleId, adminEmail, helper.getHostUrl());
  return values;
};

// /* Get meeting Schedule */
// const getMeetingSchedules = async (search, options) => {
//   const optionValue = {
//     ...options,
//     collation: {
//       locale: 'en_US',
//       numericOrdering: true
//     }
//   };
//   const value = await MeetingSchedule.paginate(search, optionValue);

//   return value;
// };

/* Get Meeting shcedule  */
const getMeetingSchedule = async (bookId) => {
  const meetingScheduleDetail = await helper.ensureEntityExists(BookModel, { _id: bookId });
  if (!meetingScheduleDetail || meetingScheduleDetail.meetingSchedule === null) {
    throw new errors.HttpStatusError(401, 'Meeting Schedule does not exist');
  }
  return meetingScheduleDetail.meetingSchedule;
};

/* Update meeting schedule */
async function updateMeetingSchedule(bookId, scheduleBody) {
  const bookDetail = await helper.ensureEntityExists(BookModel, { _id: bookId });
  if (!bookDetail) {
    throw new errors.HttpStatusError(401, 'You have book any vehicle');
  }

  let loc = await bookDetail.meetingSchedule.id({ _id: bookId });
  // eslint-disable-next-line no-unused-vars
  loc = scheduleBody;
  bookDetail.save();

  return {
    status: 200,
    message: 'Scheduled meeting has been updated successfully.'
  };
}

/**
 * Delete vehicleDetail
 * @returns {Object} Search result
 */
/**
 * delete shceule meeting   by id
 * @param {String} clientProfileId the client profile id
 */
const deleteMeetingSchedule = async (bookId) => {
  const bookDetail = await helper.ensureEntityExists(BookModel, { _id: bookId });
  if (!bookDetail) {
    throw new errors.HttpStatusError(401, 'Book detail is not found');
  }
  if (bookDetail.meetingSchedule === {}) {
    throw new errors.HttpStatusError(401, 'Meeting detail is not found');
  }
  bookDetail.meetingSchedule = {};
  bookDetail.save();

  return {
    status: 201,
    message: 'Meeting Schedule has been canceled successfully.'
  };
};

deleteMeetingSchedule.schema = {
  bookId: joi.string().required()
};
/* mark vehicle sell as sold */
async function makeVehicleSellAsSold(vehicleSellId, userId) {
  const vehicle = VehicleSellModel.findById(vehicleSellId);
  if (vehicle.isSold === true) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This vehicle is already sold');
  }

  await VehicleSellModel.updateOne({ _id: vehicleSellId }, { isSold: true, isSoldMarkedBy: userId });
  await PostExpiryDateModel.updateOne({ sellerVehicleId: vehicleSellId }, { status: false });
  if (vehicle) {
    // eslint-disable-next-line operator-linebreak
    const vehicleImage =
      typeof vehicle.bikeImagePath !== 'undefined' && vehicle.bikeImagePath.length > 0
        ? vehicle.bikeImagePath[0].imageUrl
        : '';

    await helper.createAdminNotification(
      'Sold',
      'Vehicle Sell',
      'ADMIN',
      '/seller/view',
      userId,
      vehicle.createdBy,
      vehicle.vehicleName,
      vehicleImage
    );
    await helper.createAdminNotification(
      'Sold',
      'Vehicle Sell',
      'SUPER_ADMIN',
      '/seller/view',
      userId,
      vehicle.createdBy,
      vehicle.vehicleName,
      vehicleImage
    );
  }

  return {
    status: 200,
    message: 'Vehicle have been marked as sold'
  };
}

async function makeVehicleSellAsUnsold(vehicleSellId, userId) {
  const vehicle = await VehicleSellModel.findById(vehicleSellId);

  if (vehicle.isSold === false) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This vehicle is unsold');
  }

  const entity = { isSold: false, isSoldMarkedBy: userId };
  await VehicleSellModel.updateOne({ _id: vehicleSellId }, entity);

  if (vehicle) {
    // eslint-disable-next-line operator-linebreak
    const vehicleImage =
      typeof vehicle.bikeImagePath !== 'undefined' && vehicle.bikeImagePath.length > 0
        ? vehicle.bikeImagePath[0].imageUrl
        : '';

    await helper.createAdminNotification(
      'Unsold',
      'Vehicle Sell',
      'ADMIN',
      '/seller/view',
      userId,
      vehicle.createdBy,
      vehicle.vehicleName,
      vehicleImage
    );
    await helper.createAdminNotification(
      'Unsold',
      'Vehicle Sell',
      'SUPER_ADMIN',
      '/seller/view',
      userId,
      vehicle.createdBy,
      vehicle.vehicleName,
      vehicleImage
    );
  }

  return {
    status: 200,
    message: 'Vehicle have been marked as unsold'
  };
}

/* Notify Seller about post Expiry date */
// eslint-disable-next-line consistent-return
async function notifySellerExpiryDateOnEmail() {
  const postExpiryDetails = await PostExpiryDateModel.find({});
  // const oneDayTime = new Date().getTime() + 86400;
  try {
    postExpiryDetails.forEach(async (res) => {
      if (res.status === true) {
        postExpiryDetails.forEach(async (resp) => {
          cron.schedule('0 12 * * *', () => {
            const postDate = new temporal.PlainDateTime(`${resp.postExpiryDate}`);
            /* Subtract 5 days in post expiry date */
            const notifySellerFiveDaysBeforExpiration = postDate.subtract({ days: 5 }).toString();
            // 2022-04-25 //${new Date(resp.postExpiryDate).getTime() - 432000000}
            cron.schedule(`${notifySellerFiveDaysBeforExpiration} * * * * *`, () => {
              // const date = new Date(resp.postExpiryDate);
              helper.sendExpiryEmailToSeller(
                resp.email,
                resp.sellerName,
                resp.sellerVehicleId,
                resp.adminEmail,
                helper.getHostUrl()
              );
            });
            //
          });
        });
      }
    });
  } catch (err) {
    return err;
  }
  // let b = new Date(a-777600000); 43200000
  // console.log('time of 5 days', b);
}

/* Auto Renew postExpiry */
// eslint-disable-next-line consistent-return
const autoRenewPostExpiry = async () => {
  const postExpiryDetails = await PostExpiryDateModel.find({});
  try {
    postExpiryDetails.forEach(async (resp) => {
      cron.schedule('0 12 * * *', () => {
        helper.updateExpiryDate(resp._id, resp.postExpiryDate, resp.status);
      });
    });
  } catch (err) {
    return err;
  }
};

/* Remove from Feature queu if not respond to email in three days */

const removeFromFeatureIfNotRespondToEmail = async () => {
  try {
    const featureQueList = await VehicleSellModel.find({ hasInFeaturePending: true });
    featureQueList.forEach(async (resp) => {
      cron.schedule('0 12 * * *', async () => {
        if (resp.hasInFeaturePendingDate) {
          const featurePendingDate = new temporal.PlainDateTime(`${resp.hasInFeaturePendingDate}`);
          /* Add three days in featurePendingDate */
          const addedThreeDaysInFeaturePending = featurePendingDate.add({ days: 3 }).toString();
          // const featurePendingDate = resp.hasInFeaturePendingDate.getTime() / 1000 + 259200;
          const todayDate = (new Date().getTime() / 1000).toString();
          if (addedThreeDaysInFeaturePending >= todayDate) {
            await VehicleSellModel.updateOne({ _id: resp._id }, { hasInFeaturePending: false });
          }
        }
      });
    });
  } catch (err) {
    return err;
  }
  return { message: 'Featured vehicle has been removed from queu list because customer did not took action' };
};
notifySellerExpiryDateOnEmail();
autoRenewPostExpiry();
removeFromFeatureIfNotRespondToEmail();

/* Mark as vehicle sold and stop receiving emails from two pangra */
const removeAdByVehicleSeller = async (sellerVehicleId, userId) => {
  const sellerVehicle = await PostExpiryDateModel.findOne({ sellerVehicleId });
  if (sellerVehicle.status === false) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This vehicle is already removed from two pangra.');
  } else {
    await PostExpiryDateModel.updateOne({ sellerVehicleId }, { status: false });
    await VehicleSellModel.updateOne({ _id: sellerVehicleId }, { isSold: true, isSoldMarkedBy: userId });
  }

  return {
    message: 'You have removed your add successfully.'
  };
};

/* Add Feature in Home Page by customer */
const addFeatureInHomePageByClient = async (sellerVehicleId) => {
  const vehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );
  const vehicleSellFeatureDetail = await VehicleSellModel.count({ hasInFeature: true, isApprove: true });
  if (vehicleSellFeatureDetail > 10) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      `Feature Vehicle Sell has reached the limit of 10 post try again later.  We have listed your product 
      in pending list we will inform you on your email after feature has free space`
    );
  }
  if (vehicleDetail.hasInFeature === true) {
    throw new errors.HttpStatusError(401, 'Seller Vehicle is already added to Feature');
  }
  if (vehicleDetail.hasInFeature === false) {
    vehicleDetail.hasInFeaturePending = true;
    vehicleDetail.hasInFeaturePendingDate = new Date();
  }

  // vehicleDetail.hasInFeature = true;
  // vehicleDetail.clientAddToFeature = true;
  await vehicleDetail.save();
  return {
    code: 201,
    message: 'Seller Vehicle is added to Feature'
  };
};

/* Offer a price to seller */
const offerPriceToSeller = async (userId, sellerVehicleId, offerBody) => {
  const sellerVehicleDetail = await helper.ensureEntityExists(
    VehicleSellModel,
    { _id: sellerVehicleId },
    'Seller Vehicle does not exist'
  );

  const buyerDetail = await Client.findOne({ _id: userId });

  const { vehicleName, expectedPrice, createdBy } = sellerVehicleDetail;

  const sellerDetail = await Client.findOne({ _id: createdBy });

  const offerDetail = {
    offerPrice: offerBody.offerPrice,
    sellerVehicleId,
    createdBy: userId,
    vehicleName,
    buyerName: buyerDetail.fullName,
    sellerId: createdBy,
    sellerName: sellerDetail.fullName,
    realPrice: expectedPrice,
    status: 'pending',
    createAt: Date.now()
  };
  if (sellerVehicleDetail) {
    await OfferModel.create(offerDetail);
    helper.sendOfferPriceEmail(
      buyerDetail.fullName,
      userId,
      offerBody.offerPrice,
      adminEmail,
      sellerDetail.fullName,
      createdBy,
      sellerVehicleId,
      helper.getHostUrl()
    );
  }
  return {
    message: 'Price offer is placed successfully'
  };
};

/* Approve or reject the offer of buyer */
const aprroveBuyerOffer = async (userId, offerId) => {
  const offerDetail = await helper.ensureEntityExists(OfferModel, { _id: offerId }, 'Offer does not exist');
  if (offerDetail.status === 'approved') {
    throw new errors.HttpStatusError(401, 'Buyer offer is already approved');
  }
  if (offerDetail) {
    offerDetail.status = 'approved';
    offerDetail.approveBy = userId;
    offerDetail.save();
  }
  return {
    message: 'Buyer offer is approved successfully'
  };
};

/* Reject the offer of buyer */
const rejectBuyerOffer = async (userId, offerId, remarkBody) => {
  const offerDetail = await helper.ensureEntityExists(OfferModel, { _id: offerId }, 'Offer does not exist');

  if (offerDetail.status === 'rejected') {
    throw new errors.HttpStatusError(401, 'The price you have offer has already been rejected');
  }
  const { rejectRemarks } = remarkBody;
  if (offerDetail) {
    offerDetail.status = 'rejected';
    offerDetail.approveBy = userId;
    offerDetail.rejectRemarks = rejectRemarks;
    offerDetail.save();
  }

  return {
    message: 'The price you have offered for seller vehicle has been rejected'
  };
};

/* Final offer of buyer */
const finalOfferFromSeller = async (userId, offerId, finalOfferBody) => {
  const offerDetail = await helper.ensureEntityExists(OfferModel, { _id: offerId }, 'Offer does not exist');
  if (offerDetail.status === 'final offer') {
    throw new errors.HttpStatusError(401, 'Final offer is already placed');
  }
  if (offerDetail) {
    offerDetail.status = 'final offer';
    offerDetail.approveBy = userId;
    offerDetail.finalOffer = finalOfferBody.finalOffer;
    offerDetail.save();
  }

  return {
    message: 'Final offer is placed for buyer'
  };
};
/**
 * @returns {Object} Get all Offer details
 */
const getOfferDetails = async (search, options) => {
  const value = OfferModel.paginate(search, options);
  return value;
};

/**
 * @returns {Object} Get all Offer details of login user
 */

const getClientOfferDetail = async (createdBy, sellerVehicleId) => {
  const offerDetailOfClient = await OfferModel.find({ createdBy, sellerVehicleId });
  return offerDetailOfClient;
};
/**
 * @returns {Object} Delete offer by id
 */

const deleteOfferById = async (offerId) => {
  const offerDetails = await OfferModel.deleteOne({ _id: offerId });
  if (!offerDetails) {
    throw new errors.HttpStatusError(401, 'Final offer is already placed');
  }
  return { message: 'Offer is deleted successfully' };
};
module.exports = {
  createVehicleDetailSell,
  getSellVehicleDetails,
  getVehicleSellDetail,
  updateVehicleSellDetail,
  deleteVehicleSellDetail,
  uploadVehicleSellImages,
  addFeatureInHomePage,
  // removeFeatureInHomePage,
  createVehicleSellDraftService,
  updateVehicleSellDraftService,
  getVehicleSellDraftDetailsService,
  getVehicleSellDraftDetailsByIdService,
  getUserVehicleSellDraftService,
  getUserVehicleSellService,
  createVehicleSellDraftToVehicleService,
  deleteVehicleSellDraftService,
  addToCart,
  getClientAddToCart,
  deleteCart,
  getCartById,
  addToBookFromWishlist,
  getVehicleSellHotDeals,
  addToHotDeals,
  removeFromHotDeal,
  addTowishlist,
  getClientWishlist,
  getWishlistById,
  deleteWishlist,
  getAddToCart,
  getWishlist,
  deleteWishlistByAdmin,
  deleteCartByAdmin,
  getBookedVehicleByAdmin,
  getWishlistByAdmin,
  getCartByAdmin,
  verifySellerVehicle,
  unverifySellerVehicle,
  updateLocation,
  approveSellerVehicle,
  unApproveSellerVehicle,
  meetingSchedule,
  makeVehicleSellAsSold,
  makeVehicleSellAsUnsold,
  // getMeetingSchedules,
  getMeetingSchedule,
  deleteMeetingSchedule,
  updateMeetingSchedule,
  getAdminVehicleSellHotDeals,
  removeAdByVehicleSeller,
  addFeatureInHomePageByClient,
  createVehicleDetailSellByAdmin,
  offerPriceToSeller,
  aprroveBuyerOffer,
  rejectBuyerOffer,
  getOfferDetails,
  finalOfferFromSeller,
  deleteOfferById,
  getClientOfferDetail
};
