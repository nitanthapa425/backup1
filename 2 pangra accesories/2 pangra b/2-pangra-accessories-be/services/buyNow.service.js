/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const httpStatus = require('http-status');
// const errors = require('common-errors');
// const Temporal = require('temporal');
const { add } = require('date-fns');
const helper = require('../common/helper');
const ApiError = require('../utils/ApiError');

const adminEmail = 'arjunsubedi360@gmail.com';
const buyerEmail = 'arjunsubedi360@gmail.com';
// const sellerEmail = 'susandhakal112@gmail.com';
// eslint-disable-next-line object-curly-newline
const { Accessories, BuyNow, CartModel, Client, ShippingLocation } = require('../models');

/**
 * @returns {Object} Buy Accessories
 */
const buyNow = async (userId, buyBody) => {
  const clientDetail = await Client.findOne({ _id: userId });
  const deliveryStart = new Date();
  const deliveryEnd = add(new Date(), { days: 2 });
  const { fullName, mobile } = clientDetail;
  const { bookList, address } = buyBody;
  bookList.map(async (res) => {
    const accessoriesDetail = await Accessories.findOne({ _id: res.id });
    const shippingDetail = await ShippingLocation.findOne({ _id: address });

    const buyNowDetail = {
      ...res,
      accessoriesId: res.id,
      fullName,
      mobile,
      deliveryStart,
      deliveryEnd,
      createdBy: userId,
      productImage: accessoriesDetail.productImage,
      productName: accessoriesDetail.productTitle,
      price: accessoriesDetail.costPrice,
      numViews: accessoriesDetail.numViews,
      shippingAddress: address,
      shippingAddressLocation: {
        combineLocation: shippingDetail.combineLocation,
        exactLocation: shippingDetail.exactLocation,
        nearByLocation: shippingDetail.nearByLocation,
        mobile: shippingDetail.mobile
      },
      deliveryStatus: 'ordered'
    };
    await BuyNow.create(buyNowDetail);
    await CartModel.deleteOne({ accessoriesId: res.id, createdBy: userId });
    await helper.createLogManagement(userId, 'Buy Data', 'Buy Now', accessoriesDetail.productTitle, 'CLIENT');
    await helper.notifyBuyerBookedVehicleByEmail(buyerEmail, fullName, adminEmail, helper.getHostUrl());
  });
  return {
    code: 200,
    message: 'You have successfully ordered a product.'
  };
};

/* Get Client book vehicle list */
const getCustomerBuyNow = async (userId) => {
  const customerBuyNowDetail = await BuyNow.find({
    createdBy: userId,
    deliveryStatus: { $exists: true, $ne: 'delivered' }
  });
  await helper.createLogManagement(userId, 'View Data', 'Buy Now', 'Customer Buy Now Detail', 'CLIENT');
  return customerBuyNowDetail;
};

/**
 * @returns {Object} get  Buy Now Details
 */
const getBuyNowDetails = async (search, options, userId) => {
  // search.$or = [{ isApproved: true }, { isApproved: { $exists: true } }];
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };
  const value = BuyNow.paginate(search, optionValue);
  if (value) {
    await helper.createLogManagement(userId, 'View Data', 'Buy Now', 'Buy Now Details');
  }
  return value;
};

/**
 * Get Buy now by id
 * @param {ObjectId} id
 * @returns {Promise<Category>}
 */
const getBuyNowDetail = async (id) => {
  const book = BuyNow.findOne({ _id: id }).populate(
    'shippingAddress',
    'combineLocation nearByLocation exactLocation label mobile'
  );
  if (!book) {
    throw new httpStatus.NOT_FOUND('Buy now is not found');
  }
  return book;
};

/**
 * Delete Cart by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteBuyNowById = async (buyId, userId) => {
  const buyNowDetail = await BuyNow.findOne({ _id: buyId });
  if (!buyNowDetail) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Bought detail is not found');
  }
  const accessoriesDetail = await Accessories.findById(buyNowDetail.accessoriesId);
  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof accessoriesDetail.productImage !== 'undefined' && accessoriesDetail.productImage.length > 0
      ? accessoriesDetail.productImage[0].imageUrl
      : '';
  if (buyNowDetail) {
    await helper.createLogManagement(userId, 'Delete Data', 'Buy Now', accessoriesDetail.productTitle, 'CLIENT');
  }
  if (buyNowDetail) {
    await helper.createNotification(
      'Delete',
      'Book',
      'BUYER',
      '/book',
      buyNowDetail.createdBy,
      accessoriesDetail.createdBy,
      accessoriesDetail.productTitle,
      vehicleImage
    );
    await helper.createNotification(
      'Delete',
      'Book',
      'SELLER',
      '/sell/view',
      buyNowDetail.createdBy,
      accessoriesDetail.createdBy,
      accessoriesDetail.productTitle,
      vehicleImage
    );
  }
  await BuyNow.deleteOne({ _id: buyId });
  return buyNowDetail;
};

/**
 * Delete Buy Now by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteBuyNowByAdmin = async (bookId) => {
  const book = await BuyNow.findOne({ _id: bookId });
  if (!book) {
    throw new httpStatus.NOT_FOUND('Buy Now detail is not found');
  }

  await BuyNow.deleteOne({ _id: bookId });
  return book;
};

/**
 * Change Order Status
 * @param {ObjectId} buyId
 * @returns {Promise<Category>}
 */

const changeOrderStatus = async (productId, orderStatus, orderBody) => {
  const value = await helper.ensureEntityExists(BuyNow, { _id: productId }, 'Product is not ordered.');
  if (!value) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Customer has not ordered the product');
  }

  if (orderStatus === 'ordered') {
    if (value.deliveryStatus === 'ordered') {
      throw new ApiError(httpStatus.NOT_FOUND, 'Order is already on the ordered status');
    }
    value.deliveryStatus = 'ordered';
    value.save();
  } else if (orderStatus === 'processing') {
    if (value.deliveryStatus === 'on the way') {
      throw new ApiError(httpStatus.NOT_FOUND, 'Order is already on the process or on the way');
    }
    value.approveMessages.push(orderBody);
    value.deliveryStatus = 'processing';
    value.save();
  } else if (orderStatus === 'on the way') {
    if (value.deliveryStatus === 'cancelled') {
      throw new ApiError(httpStatus.NOT_FOUND, 'Order is cancelled');
    }
    value.onTheWayDetail = orderBody;
    value.deliveryStatus = 'on the way';
    value.save();
  } else if (orderStatus === 'delivered') {
    if (
      // eslint-disable-next-line operator-linebreak
      value.deliveryStatus === 'delivered' ||
      value.deliveryStatus === 'cancelled' ||
      value.deliveryStatus === 'delivery problem'
    ) {
      throw new ApiError(httpStatus.NOT_FOUND, 'Order is already  delivered or cancelled');
    }
    value.deliveredDetail = orderBody;
    value.deliveryStatus = 'delivered';
    value.save();
  } else if (orderStatus === 'cancelled') {
    if (value.deliveryStatus === 'cancelled') {
      throw new ApiError(httpStatus.NOT_FOUND, 'Order is already cancelled');
    }
    value.cancelMessages = orderBody;
    value.deliveryStatus = 'cancelled';
    value.save();
  }
  return value;
};
module.exports = {
  buyNow,
  getBuyNowDetails,
  getBuyNowDetail,
  getCustomerBuyNow,
  deleteBuyNowById,
  deleteBuyNowByAdmin,
  changeOrderStatus
};
