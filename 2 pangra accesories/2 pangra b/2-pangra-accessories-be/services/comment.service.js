/* eslint-disable array-callback-return */
/* eslint-disable consistent-return */
const httpStatus = require('http-status');
const errors = require('common-errors');
const { Comment, Client } = require('../models');
const ApiError = require('../utils/ApiError');
const helper = require('../common/helper');

/**
 * Create a Comment
 * @param {Object} categoryBody
 * @returns {Promise<Comment>}
 */
const createComment = async (categoryBody) => Comment.create(categoryBody);

/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getComments = async (filter, options, userId) => {
  const comments = await Comment.paginate(filter, options);
  if (comments) {
    await helper.createLogManagement(userId, 'Get Data', 'Get Comments', 'Comments');
  }
  return comments;
};

/**
 * Get Comment by id
 * @param {ObjectId} id
 * @returns {Promise<Comment>}
 */
const getCommentById = async (vehicleId) => {
  const comment = Comment.findOne({ _id: vehicleId });
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  return comment;
};

/**
 * Get Seller Vehicle Comment by  Seller id
 * @param {ObjectId} id
 * @returns {Promise<Comment>}
 */
const getSellerVehicleComment = async (vehicleId) => {
  const comment = await Comment.find({ vehicleId })
    .populate('clientId', 'profileImagePath fullName')
    .sort({ createdAt: -1 });

  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  return comment;
};

/**
 * Update Comment by id
 * @param {ObjectId} userId
 * @param {Object} updateBody
 * @returns {Promise<Comment>}
 */
const updateCommentById = async (userId, commentId, updateBody) => {
  const comment = await Comment.findOne({ _id: commentId, clientId: userId });
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  await Comment.updateOne({ _id: commentId }, updateBody);

  return {
    code: 201,
    message: 'Comment is updated successfully'
  };
};

/**
 * Delete Comment by id
 * @param {ObjectId} commentId
 * @returns {Promise<Comment>}
 */
const deleteCommentById = async (userId, commentId) => {
  const comment = await Comment.findOne({ _id: commentId, clientId: userId });
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment is not found');
  }

  await comment.deleteOne({ _id: commentId });
  return comment;
};
/**
 * Approved comment by admin
 * @param {ObjectId} userId
 * @returns {Promise<Comment>}
 */
const approveCommentByAdmin = async (commentId) => {
  const comment = await Comment.findOne({ _id: commentId });
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment is not found');
  }
  if (comment.isApproved === true) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment is already approved');
  }
  comment.isApproved = true;
  comment.save();
  return comment;
};

/* Create Reply Location */
const createReplyComment = async (userId, replyCommentId, replyCommentBody) => {
  const replyCommentDetail = await helper.ensureEntityExists(Comment, { _id: replyCommentId });
  const repliedCustomer = await Client.findOne({ _id: userId });
  if (!replyCommentDetail) {
    throw new errors.HttpStatusError.NOT_FOUND('Reply comment  does not exist');
  }
  const values = {
    replyMessage: replyCommentBody.replyMessage,
    fullName: repliedCustomer.fullName,
    createdBy: userId,
    createdAt: Date.now()
  };

  replyCommentDetail.replyComment.push(values);
  replyCommentDetail.save();
  return replyCommentDetail.replyComment;
};

/* Get comment of Customer by id */
const getReplyComment = async (replyCommentId) => {
  const replyComments = await helper.ensureEntityExists(Comment, { _id: replyCommentId });
  if (!replyComments) {
    throw new errors.HttpStatusError.NOT_FOUND('Replied comment does not exist');
  }

  const values = await Comment.find({ _id: replyCommentId }).populate({
    path: 'replyComment',
    options: { sort: { createdAt: -1 } },
    populate: {
      path: 'createdBy',
      model: 'Client',
      select: 'fullName profileImagePath'
    }
  });

  const replyCommentValues = values[0].replyComment;
  return replyCommentValues.reverse();
};

/* Get Location of Customer */
const getReplyCommentById = async (commentId, replyCommentId) => {
  const replyComment = await helper.ensureEntityExistsInNested(
    Comment,
    { _id: commentId },
    { replyComment: { $elemMatch: { _id: replyCommentId } } }
  );

  const customerDetail = await Client.find({ _id: replyComment.replyComment[0].updatedBy }).select({
    profileImagePath: 1,
    fullName: 1,
    _id: 0
  });

  const replyDetails = replyComment.replyComment.concat(customerDetail);
  return {
    replyDetails
  };
};

/* Get Location of Customer */
const deleteReplyCommentById = async (commentId, replyCommentId) => {
  await Comment.update(
    { _id: commentId },
    { $pull: { replyComment: { _id: replyCommentId } } },
    { safe: true, multi: true }
  );

  return {
    status: 201,
    message: 'Replied comment is delete successfully.'
  };
};

/* Update reply customer of comment */
const updateReplyCommentById = async (userId, commentId, repliedCommentId, commentBody) => {
  const arrayId = repliedCommentId;
  const { replyMessage } = commentBody;
  const toUpdateNote = await Comment.updateOne(
    {
      'replyComment._id': arrayId
    },
    {
      $set: {
        'replyComment.$.replyMessage': replyMessage,
        'replyComment.$.updatedBy': userId
      }
    }
  );

  if (toUpdateNote) {
    return toUpdateNote;
  }
  return 'unsuccesful';
};

module.exports = {
  createComment,
  getComments,
  getCommentById,
  updateCommentById,
  deleteCommentById,
  getSellerVehicleComment,
  approveCommentByAdmin,
  createReplyComment,
  getReplyComment,
  getReplyCommentById,
  deleteReplyCommentById,
  updateReplyCommentById
};
