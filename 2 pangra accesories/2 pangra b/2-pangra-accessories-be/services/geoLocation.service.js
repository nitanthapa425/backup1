const nepalGeojson = require('nepal-geojson');
const httpStatus = require('http-status');
const errors = require('common-errors');
const { GeoLocationSchemaModel, fullLocationModel, VehicleSellModel } = require('../models');
const ApiError = require('../utils/ApiError');
const helper = require('../common/helper');

const getAllProvince = async (userId) => {
  const provincesGeojson = nepalGeojson.listProvincesWithDistricts();
  const provinces = provincesGeojson.map((res) => res.details);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View All Province',
    'All Province',
  );
  return provinces;
};

const getAllDistrictByProvince = async (provinceId, userId) => {
  const provinceIdNum = parseInt(provinceId, 10);
  const districtsGeojson = nepalGeojson.listProvinceWithDistricts(provinceIdNum);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View All District By Province',
    'All District By Province',
  );
  return districtsGeojson.districts;
};

async function createMunicipality(entity, userId) {
  const districtExist = nepalGeojson.districtInfo(entity.districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.findOne({
    municipalityName: entity.municipalityName
  });

  if (municipality) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality already exist');
  }

  await helper.createLogManagement(
    userId,
    'Add Data',
    'Create Muncipality',
    'Muncipality',
  );
  return GeoLocationSchemaModel.create(entity);
}

async function createFullLocation(entity, userId) {
  const location = fullLocationModel.create(entity);
  await helper.createLogManagement(
    userId,
    'Add Data',
    'create FullLocation',
    'Full Location',
  );
  return location;
}

async function updateFullLocation(fullLocationId, entity, userId) {
  const fullLocation = await fullLocationModel.findById(fullLocationId);

  const value = await fullLocationModel.updateOne({ _id: fullLocationId }, entity);
  await VehicleSellModel.updateMany(
    { 'location.combineLocation': fullLocation.fullAddress },
    { $set: { 'location.combineLocation': entity.fullAddress } }
  );
  await helper.createLogManagement(
    userId,
    'Update Data',
    'update FullLocation',
    'Full Location',
  );

  return value;
}

const getMunicipalityByDistrict = async (districtName, userId) => {
  const districtExist = nepalGeojson.districtInfo(districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.find({
    districtName
  });
  await helper.createLogManagement(
    userId,
    'View Data',
    'View Muncipality District',
    'Muncipality District',
  );
  return municipality;
};

const getAllDistrict = async () => nepalGeojson.districtsInfo();

const getFullLocation = async (options, search, userId) => {
  const results = await fullLocationModel.paginate(search, options);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View FullLocation',
    'Full Location',
  );
  return results;
};

const getFullLocationWithoutAuth = async () => {
  const results = await fullLocationModel.find({});
  const fullLocations = results.map((res) => ({
    id: res._id,
    fullAddress: res.fullAddress
  }));
  return fullLocations.reverse();
};

const getMunicipalityById = async (municipalityId, userId) => {
  const results = await GeoLocationSchemaModel.findById(municipalityId);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View Municipality',
    'Municipality',
  );
  return results;
};

const getFullLocationById = async (fullLocationId, userId) => {
  const results = await fullLocationModel.findById(fullLocationId);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View FullLocation',
    'Full Location',
  );
  return results;
};

const getAllMunicipality = async (options, search, userId) => {
  const results = await GeoLocationSchemaModel.paginate(search, options);
  await helper.createLogManagement(
    userId,
    'View Data',
    'View Municipality',
    'Municipality',
  );
  return results;
};

async function updateMunicipality(municipalityId, entity, userId) {
  const districtExist = nepalGeojson.districtInfo(entity.districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.findById(municipalityId);

  if (!municipality) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality not found');
  }

  const municipalityOldName = municipality.municipalityName;

  const result = await GeoLocationSchemaModel.updateOne({ _id: municipalityId }, entity);

  await fullLocationModel.updateMany(
    { municipalityName: municipalityOldName },
    { $set: { municipalityName: entity.municipalityOldName } }
  );
  await helper.createLogManagement(
    userId,
    'Update Data',
    'Update Municipality',
    'Municipality',
  );

  return result;
}

const deleteFullLocationById = async (fullLocationId, userId) => {
  const location = await fullLocationModel.findOne({ _id: fullLocationId });
  if (!location) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Location not found');
  }

  const locationExist = await VehicleSellModel.findOne({
    combineLocation: location.fullAddress
  });

  if (locationExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'This location cannot be deleted since it is used in vehicle sell');
  }

  await location.deleteOne({ _id: fullLocationId });
  await helper.createLogManagement(
    userId,
    'Delete Data',
    'Delete FullLocation',
    'FullLocation',
  );
  return location;
};

module.exports = {
  getAllProvince,
  getAllDistrict,
  createMunicipality,
  getMunicipalityByDistrict,
  getAllDistrictByProvince,
  createFullLocation,
  getFullLocation,
  getMunicipalityById,
  getAllMunicipality,
  updateMunicipality,
  getFullLocationWithoutAuth,
  updateFullLocation,
  getFullLocationById,
  deleteFullLocationById
};
