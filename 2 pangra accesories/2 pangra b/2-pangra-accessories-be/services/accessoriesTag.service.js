/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const { Tag } = require('../models');
const helper = require('../common/helper');

/**
 * @param {Object} entity create accessories tag
 * @returns {Object} the create successful message
 */

async function createTagAccessories(entity, userId) {
  await Tag.create(entity);
  await helper.createLogManagement(
    userId,
    'Add Data',
    'Add Accessories Tag',
    'Accessories Tag',
  );
  return {
    status: 201,
    message: 'Tag created successfully'
  };
}

/**
 * @returns {Object} get  all tags
 */

const getAllTags = async (userId) => {
  const results = (await Tag.find({})).map((res) => ({
    id: res._id,
    tagName: res.tagName
  }));
  await helper.createLogManagement(
    userId,
    'View Data',
    'View Accessories Tag',
    'Accessories Tag',
  );

  return results;
};

module.exports = {
  createTagAccessories,
  getAllTags
};
