/* eslint-disable operator-linebreak */
/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
// eslint-disable-next-line object-curly-newline
const { Accessories, draftAccessories, BrandAccessories, Category, SubCategory } = require('../models');

/**
 * @returns {Object} get  Specific Sub Model By Id
 */
const getSubCategoryByIdService = async (tagId) => {
  const subCategory = await SubCategory.findOne({ _id: tagId }).populate('categoryId');
  if (!subCategory) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Sub Category is not found. Try Refreshing the page');
  }

  return subCategory;
};

/**
 * @returns {Object} get  Specific Brand Detail
 */
const getCategoryByIdService = async (tagId) => {
  const category = await Category.findOne({ _id: tagId });
  if (!category) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Category is not found. Try Refreshing the page');
  }

  return category;
};

/**
 * @param {Object} entity create brand accessories
 * @returns {Object} the create successful message
 */

async function createBrandAccessories(entity) {
  const value = await BrandAccessories.create(entity);

  if (value) {
    await helper.createLogManagementInBrand(value.createdBy, 'Add Data', 'Brands', value.brandName);
  }

  return value;
}

/**
 * @param {Object} entity get brand accessories
 * @returns {Object} result with object
 */
async function createCategoryService(entity) {
  const modelVehicle = await Category.findOne({ categoryName: entity.categoryName });

  if (modelVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Category already exist.');
  }

  const value = await Category.create(entity);

  if (value) {
    await helper.createLogManagementInBrand(value.createdBy, 'Add Data', 'Models', 'Model');
  }

  return value;
}

/**
 * @param {Object} entity Create sub category
 * @returns {Object} result with object
 */
async function createSubCategoryService(entity) {
  /* Check whether variant name exist or not */
  const subcategory = await SubCategory.findOne({
    subCategoryName: entity.subCategoryName,
    categoryId: entity.categoryId
  });

  if (subcategory) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Same category cannot have  multiple sub category with same name'
    );
  }
  const value = await SubCategory.create(entity);

  if (value) {
    await helper.createLogManagement(value.createdBy, 'Add Data', 'Variants', 'SubCategory');
  }

  return value;
}

/**
 * @returns {Object} get  categories by brand id
 */
const getCategoryByBrandIdService = async (brandId) => {
  const categoryDetail = await Category.findOne({ brandId });

  if (!categoryDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Brand Model is not found. Try Refreshing the page');
  }

  const value = await Category.find({ brandId });

  return value;
};

/**
 * @returns {Object} get  Specific Brand Sub Model
 */
const getSubCategoryByCategoryIdService = async (categoryId) => {
  const subCategory = await SubCategory.findOne({ categoryId });

  if (!subCategory) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Sub category is not found. Try Refreshing the page');
  }

  const value = await SubCategory.find({ categoryId });

  return value;
};

/**
 * @returns {Object} get  all brandVehicle
 */

const getBrandAccessoriesDetails = async (options, search) => {
  const results = await BrandAccessories.paginate(search, options);
  return results;
};

/* Get Brand without auth */
const getBrandWithoutAuth = async () => {
  const results = BrandAccessories.find({});

  const reverseResult = (await results).map((res) => ({
    brandId: res.id,
    brandName: res.brandName,
    uploadBrandImage: res.uploadBrandImage
  }));
  return reverseResult.reverse();
};

const getBrandWithoutAuthLimit = async (options, search) => {
  const results = await BrandAccessories.paginate(search, options);
  return results;
};

const getHomePageBrandWithoutAuth = async () => {
  const results = BrandAccessories.find({
    hasInHomePage: true
  });

  const reverseResult = (await results).map((res) => ({
    brandId: res.id,
    brandName: res.brandName,
    brandImage: res.uploadBrandImage
  }));
  return reverseResult;
};

/**
 * @returns {Object} get  Specific Brand Detail
 */
const getBrandAccessories = async (tagId) => {
  const brandVehicle = await BrandAccessories.findOne({ _id: tagId });
  if (!brandVehicle) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Brand Detail is not found. Try Refreshing the page');
  }

  return brandVehicle;
};

/**
 * handles the update  brand accessories
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateBrandAccessories(brandId, updateBody, userId) {
  const oldBrandValue = await BrandAccessories.findOne({ _id: brandId });
  const alreadyExist = await BrandAccessories.findOne({ brandName: updateBody.brandName });
  if (alreadyExist) {
    throw new errors.HttpStatusError(401, 'Brand name already exist');
  }
  const accessoriesBrandDetail = await Accessories.find({ brandName: oldBrandValue.brandName });
  // eslint-disable-next-line object-curly-newline
  accessoriesBrandDetail.map(async (res) => {
    // eslint-disable-next-line object-curly-newline
    const { brandName, tagLine, subCategoryName, productTitle } = res;
    const tagLineUpdated = typeof tagLine !== 'undefined' && tagLine !== '' ? tagLine.concat(' ') : '';
    const newProductTitle = `${updateBody.brandName.concat(' ') + tagLineUpdated.concat(' ') + subCategoryName}`;
    await Accessories.updateMany({ brandName }, { $set: { brandName: updateBody.brandName } });
    await Accessories.updateMany({ productTitle }, { $set: { productTitle: newProductTitle } });
    await draftAccessories.updateMany({ brandName }, { $set: { brandName: updateBody.brandName } });
    await draftAccessories.updateMany({ productTitle }, { $set: { productTitle: newProductTitle } });
  });
  const value = await BrandAccessories.updateOne({ _id: brandId }, updateBody);
  if (value) {
    await helper.createLogManagement(userId, 'Update Data', 'Brands', updateBody.brandName);
  }

  return value;
}

/**
 * Delete brandVehicle
 * @returns {Object} Search result
 */
async function deleteBrandAccessories(deleteId, userId) {
  const brandVehicle = await helper.ensureEntityExists(
    BrandAccessories,
    { _id: deleteId },
    'The Brand does not exist.'
  );
  const brandInModel = await Category.findOne({ brandId: deleteId });
  // eslint-disable-next-line eqeqeq
  if (brandInModel !== null && typeof brandInModel.brandId !== 'undefined' && brandInModel.brandId == deleteId) {
    throw new errors.HttpStatusError(401, "You cannot delete this brand before deleting it's category");
  }

  const usedInProducts = await Accessories.findOne({ brandId: deleteId });
  if (usedInProducts) {
    throw new errors.HttpStatusError(401, "You cannot delete this brand before deleting it's products");
  }
  brandVehicle.remove();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Brands', 'Brand');
  }

  return {
    status: 201,
    message: 'Brand has been deleted successfully'
  };
}

deleteBrandAccessories.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/* Add brand in Home Page */
const addBrandInHomePage = async (brandVehicleId, userId) => {
  const brandVehicle = await helper.ensureEntityExists(
    BrandAccessories,
    { _id: brandVehicleId },
    'Brand Vehicle does not exist'
  );
  if (brandVehicle.hasInHomePage === true) {
    throw new errors.HttpStatusError(401, 'Brand is already added to Home Page');
  }
  brandVehicle.hasInHomePage = true;
  await brandVehicle.save();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Add in Landing Page', 'Brands', 'Brand');
  }

  return {
    code: 201,
    message: 'Brand has been added to Home page'
  };
};

/* Remove brand from Home Page */
const removeBrandInHomePage = async (brandVehicleId, userId) => {
  const brandVehicle = await helper.ensureEntityExists(
    BrandAccessories,
    { _id: brandVehicleId },
    'Brand Vehicle does not exist'
  );
  if (brandVehicle.hasInHomePage === false) {
    throw new errors.HttpStatusError(401, 'Brand is already removed from Home Page');
  }
  brandVehicle.hasInHomePage = false;
  await brandVehicle.save();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Remove from Landing Page', 'Brands', 'Brand');
  }

  return {
    code: 201,
    message: 'Brand has been removed from Home page'
  };
};

/* Upload Brand Image In brandVehicle  */
const uploadBrandImages = async (brandVehicleId, entity) => {
  const brandVehicle1 = await helper.ensureEntityExists(
    BrandAccessories,
    { _id: brandVehicleId },
    `The Brand Vehicle ${brandVehicleId} does not exist.`
  );
  brandVehicle1.uploadBrandImage = entity.uploadBrandImage;
  await brandVehicle1.save();

  return {
    status: 200,
    message: 'Brand image has been added successfully'
  };
};

/**
 * @returns {Object} get  all Brand Models
 */

const getAccessoriesCategoryService = async (search, options) => {
  const paginateResult = await Category.paginate(search, options);

  return paginateResult;
};
/**
 * @returns {Object} get  all SUb Brand Models
 */

const getSubCategoryService = async (search, options) => {
  const paginateResult = await SubCategory.paginate(search, options);
  return paginateResult;
};

/**
 * Delete Sub Category by id
 * @returns {Object} Search result
 */
async function deleteSubCategoryByIdService(deleteId, userId) {
  const subcategory = await helper.ensureEntityExists(SubCategory, { _id: deleteId }, 'Sub Category does not exist.');
  const usedInProducts = await Accessories.findOne({ subCategoryId: deleteId });
  if (usedInProducts) {
    throw new errors.HttpStatusError(401, "You cannot delete this category before deleting it's products");
  }

  subcategory.remove();

  if (subcategory) {
    await helper.createLogManagement(userId, 'Delete Data', 'Variants', 'Variant');
  }

  return {
    status: 201,
    message: 'Sub category been deleted successfully'
  };
}

deleteSubCategoryByIdService.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/**
 * Delete Category by Id
 * @returns {Object} Search result
 */
async function deleteCategoryByIdService(deleteId, userId) {
  const brandVehicle = await helper.ensureEntityExists(Category, { _id: deleteId }, 'Category does not exist.');
  const modelInVariant = await SubCategory.findOne({ categoryId: deleteId });
  // eslint-disable-next-line eqeqeq
  if (
    modelInVariant !== null &&
    typeof modelInVariant.categoryId !== 'undefined' &&
    // eslint-disable-next-line eqeqeq
    modelInVariant.categoryId == deleteId
  ) {
    throw new errors.HttpStatusError(401, "You cannot delete this category before deleting it's subcategory");
  }
  brandVehicle.remove();

  if (brandVehicle) {
    await helper.createLogManagement(userId, 'Delete Data', 'Models', 'Model');
  }

  return {
    status: 201,
    message: 'Category has been deleted successfully'
  };
}

deleteCategoryByIdService.schema = {
  deleteId: joi.string().required(),
  userId: joi.string().required()
};

/**
 * handles the update  brandVehicle
 * @param {String} userId the user id
 * s@param {Object} entity the entity
 */
async function updateSubCategoryByIdService(subcategoryId, updateBody) {
  const oldValue = await SubCategory.findOne({ _id: subcategoryId });
  const alreadyExist = await SubCategory.findOne({ subCategoryName: updateBody.subCategoryName });
  if (alreadyExist) {
    throw new errors.HttpStatusError(401, 'Sub category name already exist');
  }
  const accessoriesBrandDetail = await Accessories.find({ subCategoryName: oldValue.subCategoryName });
  // eslint-disable-next-line object-curly-newline
  accessoriesBrandDetail.map(async (res) => {
    // eslint-disable-next-line object-curly-newline
    const { brandName, tagLine, subCategoryName, productTitle } = res;
    const tagLineUpdated = typeof tagLine !== 'undefined' && tagLine !== '' ? tagLine.concat(' ') : '';
    const newProductTitle = `${brandName.concat(' ') + tagLineUpdated.concat(' ') + updateBody.subCategoryName}`;
    await Accessories.updateMany({ subCategoryName }, { $set: { subCategoryName: updateBody.subCategoryName } });
    await Accessories.updateMany({ productTitle }, { $set: { productTitle: newProductTitle } });
    await draftAccessories.updateMany({ subCategoryName }, { $set: { subCategoryName: updateBody.subCategoryName } });
    await draftAccessories.updateMany({ productTitle }, { $set: { productTitle: newProductTitle } });
  });
  // eslint-disable-next-line no-unused-vars
  const value = await SubCategory.updateOne({ _id: subcategoryId }, updateBody);

  // if (value) {
  //   await helper.createLogManagement(userId, 'Update Data', 'Sub Category', 'Sub category');
  // }

  return {
    code: 201,
    message: 'Sub category is updated successfully.'
  };
}

/**
 * handles the update  brandVehicle
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateCategoryService(categoryId, updateBody, userId) {
  const oldBrandValue = await Category.findOne({ _id: categoryId });
  const alreadyExist = await Category.findOne({ categoryName: updateBody.categoryName });
  if (alreadyExist) {
    throw new errors.HttpStatusError(401, 'Category name already exist');
  }
  const accessoriesBrandDetail = await Accessories.find({ categoryName: oldBrandValue.categoryName });
  // eslint-disable-next-line object-curly-newline
  accessoriesBrandDetail.map(async (res) => {
    await Accessories.updateMany(
      { categoryName: res.categoryName },
      { $set: { categoryName: updateBody.categoryName } }
    );
  });

  // eslint-disable-next-line no-unused-vars
  const value = await Category.updateOne({ _id: categoryId }, updateBody);

  if (value) {
    await helper.createLogManagement(userId, 'Update Data', 'Models', 'Category');
  }

  return {
    code: 201,
    message: 'Category is updated successfully.'
  };
}

/* Get Category without auth */
const getAccessoriesCategoryNoAuth = async () => {
  const results = Category.find({});

  const reverseResult = (await results).map((res) => ({
    categoryId: res.id,
    categoryName: res.categoryName
  }));
  return reverseResult.reverse();
};

/* Get Category without auth */
const getAccessoriesSubCategoryNoAuth = async () => {
  const results = SubCategory.find({});

  const reverseResult = (await results).map((res) => ({
    subCategoryId: res.id,
    subCategoryName: res.subCategoryName
  }));
  return reverseResult.reverse();
};

/* Get category without auth pagination search */
const getCategoryWithoutAuthLimit = async (options, search) => {
  const results = await Category.paginate(search, options);
  return results;
};

/**
 * @returns {Object} get  all sub category
 */

const getAccessoriesSubCategoryNoAuthLimit = async (options, search) => {
  const results = await SubCategory.paginate(search, options);
  return results;
};
module.exports = {
  createBrandAccessories,
  getBrandAccessoriesDetails,
  getBrandAccessories,
  updateBrandAccessories,
  deleteBrandAccessories,
  addBrandInHomePage,
  removeBrandInHomePage,
  uploadBrandImages,
  createCategoryService,
  createSubCategoryService,
  getCategoryByBrandIdService,
  getSubCategoryByCategoryIdService,
  getAccessoriesCategoryService,
  getCategoryByIdService,
  getSubCategoryByIdService,
  getSubCategoryService,
  deleteSubCategoryByIdService,
  deleteCategoryByIdService,
  updateSubCategoryByIdService,
  updateCategoryService,
  getBrandWithoutAuth,
  getHomePageBrandWithoutAuth,
  getBrandWithoutAuthLimit,
  getAccessoriesCategoryNoAuth,
  getAccessoriesSubCategoryNoAuth,
  getCategoryWithoutAuthLimit,
  getAccessoriesSubCategoryNoAuthLimit
};
