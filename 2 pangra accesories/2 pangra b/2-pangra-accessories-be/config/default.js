/* eslint-disable max-len */

/**
 * Copyright (C) Two Pangra
 */

/**
 * the default
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports = {
  LOG_LEVEL: process.env.LOG_LEVEL || 'debug',
  HOST_URL: process.env.HOST || 'http://localhost:3000',
  PORT: process.env.PORT || 3100,
  API_VERSION: process.env.API_VERSION || '/api/v1',
  emailVerificationContent: `Dear %s<br/> <br/>
  Thank you for signing up to Dui Pangra. Please click on this <a href="%s">link</a> which will validate the email address that you used to register. Once you click on this link you are ready to log in. We look forward to talking with you soon.<br/> <br/> If you are unable to click on the link above, please copy paste this link %s <br/> <br/> If any questions or problems, please call (000) 000-0000 Or email to support@duipangra`,
  bookVehicleSellContent: '%s<br/> <br/> Thank you.',
  forgotPasswordContent: `Dear %s<br/> <br/>
  Please click on this <a href="%s">link</a> which will guide you through the process of changing the password. <br/> <br/>If you are unable to click on the link above, please copy paste this link %s <br/> <br/> If any questions or problems, please call (000) 000-0000 Or email to support@ars`,
  sendEmailContent:
    'Dear Customer you are highly welcome to Dui-Pangra Family <br/> <br/> If any questions or problems, please call (000) 000-0000 Or email to support@ars',
  PASSWORD_HASH_SALT_LENGTH:
    (process.env.PASSWORD_HASH_SALT_LENGTH && Number(process.env.PASSWORD_HASH_SALT_LENGTH)) || 10,
  JWT_SECRET: process.env.JWT_SECRET || 'hjijfvbw859',
  JWT_EXPIRATION: process.env.JWT_EXPIRATION || '1 days',
  // default page, Page starts at 1
  DEFAULT_PAGE_INDEX: process.env.DEFAULT_PAGE_INDEX || 1,
  // page size
  DEFAULT_PER_PAGE: process.env.DEFAULT_PER_PAGE || 500,
  // max page size
  MAX_PER_PAGE: process.env.MAX_PER_PAGE || 500,
  email: {
    name: process.env.EMAIL_SERVER || 'smtp.gmail.com',
    host: process.env.EMAIL_SERVER || 'smtp.gmail.com',
    port: process.env.EMAIL_PORT || 465,
    secure: true, // use TLS
    auth: {
      user: process.env.EMAIL_USER || 'arsmatecrm@gmail.com',
      pass: process.env.EMAIL_PASS || 'Susan7872@@@'
    },
    // tls: {
    //   rejectUnauthorized: true
    // },
    debug: true // include SMTP traffic in the logs
  },

  db: {
    url:
      // eslint-disable-next-line operator-linebreak
      process.env.MONGODB_URI ||
      'mongodb+srv://admin:admin@cluster0.3rnsz.mongodb.net/2pangra?retryWrites=true&w=majority',
    poolSize: 5,
    settings: {
      reconnectTries: Number.MAX_VALUE,
      autoReconnect: true
    }
  },
  messageBirdApiKey: process.env.MESSAGEBIRD_API_KEY,
  adminEmail: process.env.ADMIN_EMAIL,
  sparrowToken: process.env.SPARROW_TOKEN
};
