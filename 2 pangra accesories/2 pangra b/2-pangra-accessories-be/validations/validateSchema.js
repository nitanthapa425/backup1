const joi = require('joi');
// const { objectId } = require('./custom.validation');

const createVehicleDetailValidate = {
  body: joi.object().keys({
    brandId: joi.string().required(),
    modelId: joi.string().required(),
    varientId: joi.string().optional().allow(''),
    brandName: joi.string().optional().allow(''),
    modelName: joi.string().optional().allow(''),
    varientName: joi.string().optional().allow(''),
    otherDetail: joi.string().optional().allow(''),
    vehicleName: joi.string().optional().allow(''),
    vehicleType: joi.string().required(),
    fuelType: joi.string().required(),
    marketPrice: joi.number().optional().allow('').min(10000).max(10000000),
    vehicleImgPath: joi.array().items({
      imageUrl: joi.string().required(),
      publicKey: joi.string().required()
    }),
    createdBy: joi.string().optional().allow(''),
    updatedBy: joi.string().optional().allow(''),
    basicFactor: joi.object().keys({
      transmissionType: joi.string().required().min(2).max(150),
      mileage: joi.string().optional().required(),
      bodyType: joi.string().required().min(2).max(150),
      seatingCapacity: joi.string().required(),
      fuelCapacity: joi.string().optional().allow('')
    }),
    optionFeature: joi.object().keys({
      instrumentation: joi.array().items(joi.string().required().min(2).max(150)),
      safety: joi.array().items(joi.string().required().min(2).max(150)),
      features: joi.array().items(joi.string().required().min(2).max(150)),
      ignitionType: joi.string().optional().allow(''),
      chargingPoint: joi.boolean().optional().allow(''),
      speedometer: joi.string().optional().valid(['DIGITAL', 'ANALOG']),
      tripMeter: joi.string().optional().valid(['DIGITAL', 'ANALOG']),
      passSwitch: joi.boolean().optional().allow(''),
      clock: joi.boolean().optional().allow(''),
      ridingModes: joi.boolean().optional().allow(''),
      navigation: joi.boolean().optional().allow(''),
      // motorPower: joi.string().optional().allow(''),
      chargingAtHome: joi.boolean().optional().allow(''),
      chargingAtChargingStation: joi.boolean().optional().allow('')
    }),
    technicalFactor: joi.object().keys({
      frontBrakeSystem: joi.string().required(),
      backBrakeSystem: joi.string().required(),
      frontSuspension: joi.string().optional(),
      rearSuspension: joi.string().optional(),
      noOfGears: joi.number().optional().allow(''),
      driveType: joi.string().optional(),
      clutchType: joi.string().optional().allow(''),
      gearPattern: joi.string().optional().allow(''),
      headlight: joi.string().required(),
      taillight: joi.string().required(),
      starter: joi.array().items(joi.string().required().max(150).min(2)),
      battery: joi.string().required().min(2).max(150),
      lowBatteryIndicator: joi.boolean().optional().allow('')
    }),
    dimensionalFactor: joi.object().keys({
      wheelBase: joi.string().required(),
      overallWidth: joi.string().required(),
      overallLength: joi.string().required(),
      overallHeight: joi.string().required(),
      groundClearance: joi.string().optional(),
      kerbWeight: joi.string().optional(),
      doddleHeight: joi.string().optional(),
      bootSpace: joi.string().optional().allow('')
    }),
    engineFactor: joi.object().keys({
      motorPower: joi.string().optional().allow('').min(0).max(50000),
      displacement: joi.string().optional().allow(''),
      engineType: joi.array().items(joi.string().optional()),
      noOfCylinder: joi.string().optional(),
      valvesPerCylinder: joi.string().optional(),
      valveConfiguration: joi.string().optional().max(150),
      fuelSupplySystem: joi.string().optional().max(150),
      maximumPower: joi.string().optional().min(2).max(150),
      maximumTorque: joi.string().optional(),
      lubrication: joi.string().optional().min(2).max(150),
      engineOil: joi.string().optional().min(2).max(150),
      airCleaner: joi.string().optional().min(2).max(150),
      boreXStroke: joi.string().optional().min(2).max(150),
      compressionRatio: joi.string().optional().min(2).max(150)
    }),
    wheelTyreFactor: joi.object().keys({
      frontWheelType: joi.string().required().min(2).max(150),
      rearWheelType: joi.string().required().min(2).max(150),
      frontWheelSize: joi.string().required(),
      rearWheelSize: joi.string().required(),
      steelRims: joi.string().required().min(2).max(150)
    })
  })
};

const createVehicleDraftValidate = {
  body: joi.object().keys({
    brandId: joi.string().required(),
    modelId: joi.string().required(),
    varientId: joi.string().optional().allow(''),
    brandName: joi.string().optional().allow(''),
    modelName: joi.string().optional().allow(''),
    varientName: joi.string().optional().allow(''),
    otherDetail: joi.string().optional().allow(''),
    vehicleName: joi.string().optional().allow(''),
    vehicleType: joi.string().required().valid(['BIKE', 'SCOOTER']),
    fuelType: joi.string().optional().required().valid(['PETROL', 'DIESEL', 'ELECTRIC']),
    marketPrice: joi.number().optional().allow(''),
    vehicleImgPath: joi.array().items({
      imageUrl: joi.string().required(),
      publicKey: joi.string().required()
    }),
    createdBy: joi.string().optional().allow(''),
    updatedBy: joi.string().optional().allow(''),
    basicFactor: joi.object().keys({
      transmissionType: joi.string().optional().allow(''),
      mileage: joi.string().optional().allow(''),
      bodyType: joi.string().optional().allow(''),
      seatingCapacity: joi.string().optional().allow(''),
      fuelCapacity: joi.string().optional().allow('')
    }),
    optionFeature: joi.object().keys({
      instrumentation: joi.array().items(joi.string().optional().allow('')),
      safety: joi.array().items(joi.string().optional().allow('')),
      features: joi.array().items(joi.string().optional().allow('')),
      ignitionType: joi.string().optional().allow(''),
      chargingPoint: joi.boolean().optional().allow(''),
      speedometer: joi.string().optional().allow(''),
      tripMeter: joi.string().optional().allow(''),
      passSwitch: joi.boolean().optional().allow(''),
      clock: joi.boolean().optional().allow(''),
      ridingModes: joi.boolean().optional().allow(''),
      navigation: joi.boolean().optional().allow(''),
      // motorPower: joi.string().optional().allow(''),
      chargingAtHome: joi.boolean().optional().allow(''),
      chargingAtChargingStation: joi.boolean().optional().allow('')
    }),
    technicalFactor: joi.object().keys({
      frontBrakeSystem: joi.string().optional().allow(''),
      backBrakeSystem: joi.string().optional().allow(''),
      frontSuspension: joi.string().optional().allow(''),
      rearSuspension: joi.string().optional().allow(''),
      noOfGears: joi.string().optional().allow(''),
      driveType: joi.string().optional().allow(''),
      clutchType: joi.string().optional().allow(''),
      gearPattern: joi.string().optional().allow(''),
      headlight: joi.string().optional().allow(''),
      taillight: joi.string().optional().allow(''),
      starter: joi.array().items(joi.string().optional().allow('')),
      battery: joi.string().optional().allow(''),
      lowBatteryIndicator: joi.boolean().optional().allow('')
    }),
    dimensionalFactor: joi.object().keys({
      wheelBase: joi.string().optional().allow(''),
      overallWidth: joi.string().optional().allow(''),
      overallLength: joi.string().optional().allow(''),
      overallHeight: joi.string().optional().allow(''),
      groundClearance: joi.string().optional().allow(''),
      kerbWeight: joi.string().optional().allow(''),
      doddleHeight: joi.string().optional().allow(''),
      bootSpace: joi.string().optional().allow('')
    }),
    engineFactor: joi.object().keys({
      motorPower: joi.string().optional().allow(''),
      displacement: joi.string().optional().allow(''),
      engineType: joi.array().items(joi.string().optional()),
      noOfCylinder: joi.string().optional().allow(''),
      valvesPerCylinder: joi.string().optional().allow(''),
      valveConfiguration: joi.string().optional().allow(''),
      fuelSupplySystem: joi.string().optional().allow(''),
      maximumPower: joi.string().optional().allow(''),
      maximumTorque: joi.string().optional().allow(''),
      lubrication: joi.string().optional().allow(''),
      engineOil: joi.string().optional().allow(''),
      airCleaner: joi.string().optional().allow(''),
      boreXStroke: joi.string().optional().allow(''),
      compressionRatio: joi.string().optional().allow('')
    }),
    wheelTyreFactor: joi.object().keys({
      frontWheelType: joi.string().optional().allow(''),
      rearWheelType: joi.string().optional().allow(''),
      frontWheelSize: joi.string().optional().allow(''),
      rearWheelSize: joi.string().optional().allow(''),
      steelRims: joi.string().optional().allow('')
    })
  })
};

/* Validate User Sign Up Schema */
const signUpSchema = {
  entity: joi.object().keys({
    title: joi.string().optional().allow(''),
    firstName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    middleName: joi
      .string()
      .optional()
      .allow('')
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i),
    lastName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
    userName: joi
      .string()
      .required()
      .min(3)
      .max(15)
      /** Username containing Alphabets and Numeric value */
      .regex(/^[a-zA-Z0-9_.-]*$/),
    dob: joi.string().required(),
    gender: joi.string().required(),
    // password: joi
    //   .string()
    //   /** Password must be minimum of eight characters, with at least */
    //   /** one uppercase letter, one lowercase letter, one string and one special character */
    //   .regex(/^.(?=.{3,})(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[\d\x])(?=.[!@$#%^&]).$/,)
    //   // eslint-disable-next-line max-len
    // eslint-disable-next-line max-len
    //   .required('Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character.'),
    mobile: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[0-9]{10,14}$/),
    accessLevel: joi
      .string()
      .optional()
      .allow('')
      .valid('SUPER_ADMIN', 'ADMIN', 'USER', 'DELIVERY'),
    residential: {
      street: joi.string().optional().allow(''),
      streetAddress: joi.string().optional().allow(''),
      state: joi.string().optional().allow(''),
      country: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      zip: joi
        .string()
        .optional()
        .allow('')
        .max(10)
        .regex(/^[0-9]+$/),
      suburb: joi.string().optional().allow('')
    },
    rolePosition: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[aA-zZ\s]+$/),
    profileImagePath: joi.object().keys({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    }),
    documentImagePath: joi.array().items({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    }),
    companyId: joi.string().optional().allow('')
  })
};

/* Validate Client Register */
const registerClient = {
  body: joi.object().keys({
    firstName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    middleName: joi
      .string()
      .optional()
      .allow('')
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i),
    lastName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
    dob: joi.date().optional(),
    gender: joi.string().optional().allow(''),
    verificationToken: joi.string().required(),
    accessLevel: joi.string().optional().allow(''),
    password: joi
      .string()
      /** Password must be minimum of eight characters, with at least */
      /** one uppercase letter, one lowercase letter, one string and one special character */
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      .required(),
    mobile: joi
      .string()
      .required()
      .regex(/(?:\+977[- ])?\d{2}-?\d{7,8}/),
    location: joi.object().keys({
      country: joi.string().optional(),
      nearByLocation: joi.string().optional().allow(''),
      exactLocation: joi.string().required(),
      combineLocation: joi.string().required(),
      status: joi.string().optional().allow('')
    }),
    receiveNewsletter: joi.boolean().optional(),
    rolePosition: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[aA-zZ\s]+$/),
    isActive: joi.boolean().optional(),
    profileImagePath: joi.object().keys({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    })
  })
};

/* Validate Client Update */
const updateClient = {
  body: joi.object().keys({
    firstName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    middleName: joi
      .string()
      .optional()
      .allow('')
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i),
    lastName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z]){1,30}$/i)
      .required(),
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
    dob: joi.date().optional(),
    gender: joi.string().optional().allow(''),
    accessLevel: joi.string().optional().allow(''),
    mobile: joi
      .string()
      .required()
      .regex(/(?:\+977[- ])?\d{2}-?\d{7,8}/),
    location: joi.object().keys({
      country: joi.string().optional(),
      nearByLocation: joi.string().optional().allow(''),
      exactLocation: joi.string().optional(),
      combineLocation: joi.string().required(),
      status: joi.string().optional().allow('')
    }),
    receiveNewsletter: joi.boolean().optional(),
    rolePosition: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[aA-zZ\s]+$/),
    isActive: joi.boolean().optional(),
    profileImagePath: joi.object().keys({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    })
  })
};

/* Update user validation */

const userUpdate = {
  body: joi.object().keys({
    isActive: joi.string().optional().allow(''),
    // userName: joi.string().optional().allow(''),
    firstName: joi.string().optional().allow(''),
    middleName: joi.string().optional().allow(''),
    lastName: joi.string().optional().allow(''),
    // email: joi.string().optional().allow(''),
    mobile: joi.string().optional().allow(''),
    profileImagePath: joi.object().keys({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    }),
    gender: joi.string().optional().allow(''),
    dob: joi.string().optional().allow('')
  })
};

/* Create vehicle validation  */
const createVehicleSell = {
  body: joi.object().keys({
    bikeImagePath: joi.array().items({
      imageUrl: joi.string().required(),
      publicKey: joi.string().required()
    }),
    billBookImagePath: joi.array().items({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    }),
    location: joi.object().keys({
      country: joi.string().required(),
      nearByLocation: joi.string().optional().allow(''),
      combineLocation: joi.string().required(),
      geoLocation: joi.object().keys({
        type: joi.string().required(),
        coordinates: joi.array().items()
      })
    }),
    bikeDriven: joi.string().optional().allow(''),
    expectedPrice: joi.string().required(),
    isNegotiable: joi.boolean().optional(),
    bikeNumber: joi.string().optional().allow(''),
    lotNumber: joi.string().optional().allow(''),
    note: joi.string().optional().allow(''),
    color: joi.string().optional().allow(''),
    condition: joi
      .string()
      .optional()
      .allow('')
      .valid(['Brand New', 'Like New', 'Excellent', 'Good/Fair', 'Not Working']),
    mileage: joi.string().optional().allow(''),
    ownershipCount: joi.string().optional().allow(''),
    makeYear: joi.string().optional().allow(''),
    postExpiryDate: joi.string().required(),
    hasAccident: joi.boolean().optional(),
    suggestedPrice: joi.string().optional().allow(''),
    productStatus: joi.string().optional().allow(''),
    isApproved: joi.boolean().optional(),
    isVerified: joi.boolean().optional(),
    unapprovedReason: joi.string().optional().allow(''),
    usedFor: joi.string().optional().allow(''),
    vehicleDetailId: joi.string().required(),
    vehicleName: joi.string().optional().allow(''),
    status: joi.string().optional().allow(''),
    fuelType: joi.string().optional().allow(''),
    vehicleType: joi.string().optional().allow(''),
    featureTime: joi.date().optional().allow(''),
    isBooked: joi.boolean().optional().allow(''),
    isSold: joi.boolean().optional().allow(''),
    isSoldMarkedBy: joi.string().optional().allow('')
  })
};

/* Client Login validation */
const clientLogin = {
  body: joi
    .object()
    .keys({
      mobile: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      userName: joi.string().optional().allow(''),
      password: joi.string().required()
    })
    .required()
};

const addToCart = {
  body: joi.object().keys({
    vehicleName: joi.string().optional(),
    vehicleImage: joi.array().items({
      imageUrl: joi.string().optional(),
      publicKey: joi.string().optional()
    }),
    totalPrice: joi.string().optional(),
    color: joi.string().optional(),
    status: joi.boolean().optional(),
    createdBy: joi.string().optional(),
    updateBy: joi.string().optional()
  })
};

const getAddToCart = {
  params: joi.object().keys({
    id: joi.string().required()
  })
};

const locationValidate = {
  body: joi.object().keys({
    status: joi.string().optional().allow(''),
    label: joi.string().required(),
    country: joi.string().optional().allow(''),
    nearByLocation: joi.string().optional().allow(''),
    combineLocation: joi.string().required(),
    exactLocation: joi.string().required(),
    fullName: joi.string().required(),
    mobile: joi.string().required(),
  })
};

const subscribeEmail = {
  body: joi.object().keys({
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow('')
  })
};

const meetingSchedule = {
  params: joi.object().keys({
    id: joi.string().required()
  }),
  body: joi.object().keys({
    combineLocation: joi.string().required(),
    nearByLocation: joi.string().required(),
    meetingPlace: joi.string().required(),
    meetingDateTime: joi.date().required(),
    createdBy: joi.string().optional()
  })
};

const rejectOfferPrice = {
  params: joi.object().keys({
    id: joi.string().required()
  }),
  body: joi.object().keys({
    rejectRemarks: joi.string().required()
  })
};

const finalOfferFromSeller = {
  params: joi.object().keys({
    id: joi.string().required()
  }),
  body: joi.object().keys({
    finalOffer: joi.string().required()
  })
};

const commentValidate = {
  params: joi.object().keys({
    id: joi.string().required()
  }),
  body: joi.object().keys({
    name: joi.string().optional(),
    message: joi.string().required(),
    clientId: joi.string().optional()
    // createdBy: joi.string().optional()
  })
};

/* Create vehicle validation  */
const createVehicleSellByAdmin = {
  body: joi.object().keys({
    bikeImagePath: joi.array().items({
      imageUrl: joi.string().required(),
      publicKey: joi.string().required()
    }),
    billBookImagePath: joi.array().items({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow('')
    }),
    location: joi.object().keys({
      country: joi.string().required(),
      nearByLocation: joi.string().optional().allow(''),
      combineLocation: joi.string().required(),
      geoLocation: joi.object().keys({
        type: joi.string().required(),
        coordinates: joi.array().items()
      })
    }),
    bikeDriven: joi.string().optional().allow(''),
    expectedPrice: joi.string().required(),
    isNegotiable: joi.boolean().optional(),
    bikeNumber: joi.string().optional().allow(''),
    lotNumber: joi.string().optional().allow(''),
    note: joi.string().optional().allow(''),
    color: joi.string().optional().allow(''),
    condition: joi
      .string()
      .optional()
      .allow('')
      .valid(['Brand New', 'Like New', 'Excellent', 'Good/Fair', 'Not Working']),
    mileage: joi.string().optional().allow(''),
    ownershipCount: joi.string().optional().allow(''),
    makeYear: joi.string().optional().allow(''),
    postExpiryDate: joi.string().required(),
    hasAccident: joi.boolean().optional(),
    suggestedPrice: joi.string().optional().allow(''),
    productStatus: joi.string().optional().allow(''),
    isApproved: joi.boolean().optional(),
    isVerified: joi.boolean().optional(),
    unapprovedReason: joi.string().optional().allow(''),
    usedFor: joi.string().optional().allow(''),
    vehicleDetailId: joi.string().required(),
    vehicleName: joi.string().optional().allow(''),
    status: joi.string().optional().allow(''),
    fuelType: joi.string().optional().allow(''),
    vehicleType: joi.string().optional().allow(''),
    featureTime: joi.date().optional().allow(''),
    isBooked: joi.boolean().optional().allow(''),
    isSold: joi.boolean().optional().allow(''),
    isSoldMarkedBy: joi.string().optional().allow(''),
    sellerDetails: joi.object().keys({
      firstName: joi.string().required().allow(''),
      middleName: joi.string().optional().allow(''),
      lastName: joi.string().optional().allow(''),
      mobile: joi.string().optional().allow(''),
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      combineLocation: joi.string().optional().allow(''),
      gender: joi.string().optional().allow(''),
      additionalMobile: joi.string().optional().allow(''),
      customerDescription: joi.string().required()
    })
  })
};
module.exports = {
  createVehicleDetailValidate,
  createVehicleDraftValidate,
  signUpSchema,
  registerClient,
  updateClient,
  userUpdate,
  createVehicleSell,
  clientLogin,
  addToCart,
  getAddToCart,
  locationValidate,
  subscribeEmail,
  meetingSchedule,
  commentValidate,
  rejectOfferPrice,
  finalOfferFromSeller,
  createVehicleSellByAdmin
};
