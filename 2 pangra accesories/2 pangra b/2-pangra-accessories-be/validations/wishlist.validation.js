const joi = require('joi');

const createWishListValidation = {
  params: joi.object().keys({
    id: joi.string().required()
  })
};

module.exports = { createWishListValidation };
