const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createDraftAccessoriesItem = {
  body: joi.object().keys({
    brandId: joi.string().required(),
    categoryId: joi.string().required(),
    subCategoryId: joi.string().required(),
    productTitle: joi.string().optional(),
    description: joi.string().required(),
    costPrice: joi.string().required(),
    discountPrice: joi.string().optional().allow(''),
    weight: joi.string().optional(),
    dimension: joi.object().keys({
      length: joi.string().optional().allow(''),
      breadth: joi.string().optional().allow(''),
      height: joi.string().optional().allow('')
    }),
    dimensionUnit: joi.string().optional().valid(['mm', 'cm', 'm']),
    sizeCalculation: joi.string().optional().allow(''),
    sizeValue: joi.array().optional().allow(''),
    SKU: joi.string().optional().allow(''),
    quantity: joi.string().optional().allow(''),
    hasWarranty: joi.boolean().optional(),
    warrantyTime: joi.string().optional(),
    warrantyUnit: joi.string().optional(),
    isReturnable: joi.boolean().optional(),
    returnableUnit: joi.string().optional().allow(''),
    returnableTime: joi.string().optional().allow(''),
    onSale: joi.boolean().required(),
    tags: joi.string().optional().allow(''),
    tagLine: joi.string().optional().min(3).max(50),
    hasInFeatured: joi.boolean().optional(),
    outOfStock: joi.boolean().optional(),
    hasInHotDeal: joi.boolean().optional(),
    isForFlashSale: joi.boolean().optional(),
    productImages: joi.array().items({
      imageUrl: joi.string().optional().allow(''),
      publicKey: joi.string().optional().allow(''),
    }),
    color: joi.string().optional().optional().allow(''),
    weightUnit: joi.string().optional().allow(),
    status: joi.boolean().optional(),
    createdBy: joi.string().optional().allow(''),
    updatedBy: joi.string().optional().allow(''),
    createdAt: joi.date().optional(),
    updatedAt: joi.date().optional()
  })
};

module.exports = {
  createDraftAccessoriesItem
};
