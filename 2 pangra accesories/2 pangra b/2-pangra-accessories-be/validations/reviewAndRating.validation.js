const joi = require('joi');

const createReviewAndRatingValidate = {
  params: joi.object().keys({
    id: joi.string().required(),
  }),
  body: joi.object().keys({
    ratingNumber: joi.string().required(),
    ratingComment: joi.string().allow('').optional(),
    createdBy: joi.string().optional(),
    status: joi.string().allow('').optional(),
  })
};

module.exports = {
  createReviewAndRatingValidate
};
