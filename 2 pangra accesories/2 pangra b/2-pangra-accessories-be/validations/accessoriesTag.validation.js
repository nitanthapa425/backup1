const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createTag = {
  body: joi.object().keys({
    tagName: joi.string().required(),
    status: joi.string().optional().allow(''),
    createdBy: joi.string().optional().allow(''),
    createdAt: joi.date().optional().allow(''),
    updatedAt: joi.date().optional().allow('')
  })
};

module.exports = {
  createTag
};
