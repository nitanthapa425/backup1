const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createCompanyDetail = {
  body: joi.object().keys({
    companyName: joi.string().required(),
    fullName: joi.string().required(),
    mobile: joi.string().required(),
    email: joi.string().optional().allow(''),
    createdAt: joi.date().optional(),
    updatedAt: joi.date().optional()
  })
};

module.exports = {
  createCompanyDetail
};
