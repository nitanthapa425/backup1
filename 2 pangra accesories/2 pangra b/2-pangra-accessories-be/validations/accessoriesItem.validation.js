const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createAccessoriesItem = {
  body: joi.object().keys({
    brandId: joi.string().required(),
    categoryId: joi.string().required(),
    subCategoryId: joi.string().required(),
    productTitle: joi.string().optional(),
    description: joi.string().required(),
    costPrice: joi.string().required(),
    discountPrice: joi.string().optional().allow(''),
    weight: joi.string().optional(),
    dimension: joi.object().keys({
      length: joi.string().optional().allow(''),
      breadth: joi.string().optional().allow(''),
      height: joi.string().optional().allow('')
    }),
    dimensionUnit: joi.string().optional().valid(['mm', 'cm', 'm']),
    sizeCalculation: joi.string().required(),
    SKU: joi.string().required(),
    quantity: joi.string().required(),
    hasWarranty: joi.boolean().required(),
    warrantyTime: joi.string().optional(),
    warrantyUnit: joi.string().optional(),
    isReturnable: joi.boolean().required(),
    returnableUnit: joi.string().optional(),
    returnableTime: joi.string().optional(),
    onSale: joi.boolean().required(),
    tags: joi.string().optional().allow(''),
    tagLine: joi.string().optional().min(3).max(50),
    hasInFeatured: joi.boolean().optional(),
    outOfStock: joi.boolean().optional(),
    hasInHotDeal: joi.boolean().optional(),
    isForFlashSale: joi.boolean().optional(),
    productImages: joi.array().items({
      imageUrl: joi.string().required(),
      publicKey: joi.string().required()
    }),
    color: joi.array().optional().required(),
    sizeValue: joi.array().required(),
    variant: joi.array().items({
      color: joi.string().optional(),
      size: joi.string().optional(),
      quantity: joi.string().optional(),
    }),
    weightUnit: joi.string().optional().allow(),
    status: joi.boolean().optional(),
    createdBy: joi.string().optional().allow(''),
    updatedBy: joi.string().optional().allow(''),
    createdAt: joi.date().optional(),
    updatedAt: joi.date().optional()
  })
};

module.exports = {
  createAccessoriesItem
};
