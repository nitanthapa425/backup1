const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { commentService } = require('../services');
const { Client } = require('../models');
// const { capitalize } = require('../utils/helper');

/* Create category */
const createComment = catchAsync(async (req, res) => {
  const customerId = req.user.id;
  const vehicleId = req.params.id;
  const customerDetail = await Client.findOne({ _id: customerId });
  const { fullName } = customerDetail;
  const values = {
    ...req.body,
    clientId: customerId,
    name: fullName,
    vehicleId,
    createdAt: new Date()
  };
  await commentService.createComment(values);
  res.send(httpStatus.CREATED, {
    message: 'Comment is posted successfully.'
  });
});

/* Get all categories  */
const getComments = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const userId = req.user.id;
  const result = await commentService.getComments(search, options, userId);
  res.send(result);
});

/* Get category by id */
const getSingleComment = catchAsync(async (req, res) => {
  const comment = await commentService.getCommentById(req.params.id);
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  res.send(comment);
});

/* Get category by id */
const getSellerVehicleComment = catchAsync(async (req, res) => {
  const comment = await commentService.getSellerVehicleComment(req.params.id);
  if (!comment) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Comment not found');
  }
  res.send(comment);
});

/* Update comment by id */
const updateComment = catchAsync(async (req, res) => {
  const commentId = req.params.id;
  const commentDetail = req.body;
  const userId = req.user.id;
  await commentService.updateCommentById(userId, commentId, commentDetail);
  res.send(httpStatus.CREATED, {
    message: 'Comment has been updated successfully'
  });
});

/* Delete category by id */
const deleteComment = catchAsync(async (req, res) => {
  await commentService.deleteCommentById(req.user.id, req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Comment has been removed successfully'
  });
});

/* Approve comment by admin */
const approveCommentByAdmin = catchAsync(async (req, res) => {
  await commentService.approveCommentByAdmin(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Comment has been approved successfully'
  });
});

/**
 * create customer reply comment
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createReplyComment = catchAsync(async (req, res) => {
  res.json(await commentService.createReplyComment(req.user.id, req.params.id, req.body));
});

/**
 * Get customer replied comment
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getReplyComment = catchAsync(async (req, res) => {
  res.json(await commentService.getReplyComment(req.params.id));
});

/**
 * Get customer reply comment by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getReplyCommentById = catchAsync(async (req, res) => {
  res.json(await commentService.getReplyCommentById(req.params.id, req.params.replyId));
});

/**
 * Update customer replied comment by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateReplyCommentById = catchAsync(async (req, res) => {
  res.json(await commentService.updateReplyCommentById(req.user.id, req.params.id, req.params.replyId, req.body));
});
/**
 * Delete customer replied comment by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteReplyCommentById = catchAsync(async (req, res) => {
  res.json(await commentService.deleteReplyCommentById(req.params.id, req.params.replyId));
});
module.exports = {
  createComment,
  getComments,
  getSingleComment,
  getSellerVehicleComment,
  updateComment,
  deleteComment,
  approveCommentByAdmin,
  createReplyComment,
  getReplyComment,
  getReplyCommentById,
  updateReplyCommentById,
  deleteReplyCommentById
};
