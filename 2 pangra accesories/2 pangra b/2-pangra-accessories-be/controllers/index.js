/**
 * Copyright (C) Two Pangra
 */

/**
 * the controller entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports.userProfileController = require('./user.controller');
module.exports.clientProfileController = require('./client.controller');
module.exports.securityController = require('./security.controller');
module.exports.deliveryPersonController = require('./deliveryPerson.controller');
module.exports.deliveryPersonSecurityController = require('./deliverySecurity.controller');
module.exports.brandAccessoriesController = require('./brandAccessories.controller');
module.exports.imageUploadController = require('./imageUpload.controller');
module.exports.vehicleSellController = require('./vehicleSell.controller');
module.exports.geoLocationController = require('./geolocation.controller');
module.exports.emailSubscribeController = require('./emailSubscribe.controller');
module.exports.commentController = require('./comment.controller');
module.exports.accessoriesController = require('./accessoriesItem.controller');
module.exports.draftAccessoriesController = require('./draftAccessoriesItem.controller');
module.exports.buyNowController = require('./buyNow.controller');
module.exports.ratingAndReviewController = require('./reviewAndRating.controller');
module.exports.wishListController = require('./wishList.controller');
module.exports.addToCartController = require('./addToCart.controller');
module.exports.companyController = require('./company.controller');
module.exports.tagController = require('./accessoriesTag.controller');
