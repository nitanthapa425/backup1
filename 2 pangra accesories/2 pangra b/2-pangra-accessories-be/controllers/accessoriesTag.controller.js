/**
 * Copyright (C) Tuki Logic
 */

/**
 * Tag Controller
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const httpStatus = require('http-status');
const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
const { tagService } = require('../services');
/**
 * post tag information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createTagAccessories = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const tags = _.extend(entity, {
    createdBy: userId,
    createdAt: Date.now()
  });
  const tagDetail = await tagService.createTagAccessories(tags, userId);
  res.status(httpStatus.CREATED).send(tagDetail);
});

/**
 * get all tag information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllTags = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await tagService.getAllTags(userId));
});

module.exports = {
  createTagAccessories,
  getAllTags
};
