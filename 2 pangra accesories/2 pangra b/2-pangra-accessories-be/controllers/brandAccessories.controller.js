/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const errors = require('common-errors');
const _ = require('lodash');
const { Category, SubCategory } = require('../models');
const BrandAccessoriesService = require('../services/brandAccessories.service');
const catchAsync = require('../utils/catchAsync');
/**
 * post BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createBrandAccessories = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const vehicle = _.extend(entity, {
    brandName: entity.brandName,
    createdBy: userId,
    createdAt: Date.now()
  });
  const brandVehicle = await BrandAccessoriesService.createBrandAccessories(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 * post Brand Model information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createCategoryController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const vehicle = _.extend(entity, {
    categoryName: entity.categoryName,
    createdBy: userId,
    createdAt: Date.now(),
    updatedAt: Date.now()
  });
  const brandVehicle = await BrandAccessoriesService.createCategoryService(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 *  Updates Brand Model
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateCategoryController = catchAsync(async (req, res) => {
  const categoryId = req.params.id;

  const categoryDetail = await Category.findOne({ _id: categoryId });
  if (!categoryDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Category does not exist');
  }
  const { categoryName } = req.body;
  const userId = req.user.id;
  const categoryDetails = {
    ...req.body,
    categoryName,
    updatedBy: userId
  };

  res.json(await BrandAccessoriesService.updateCategoryService(categoryId, categoryDetails, userId));
});

/**
 * post Brand Model information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createSubCategoryController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const category = await Category.findById(entity.categoryId);

  const vehicle = _.extend(entity, {
    subCategoryName: entity.subCategoryName,
    createdBy: userId,
    createdAt: Date.now(),
    updatedAt: Date.now(),
    categoryName: category.categoryName
  });
  const brandVehicle = await BrandAccessoriesService.createSubCategoryService(vehicle);
  res.status(httpStatus.CREATED).send(brandVehicle);
});

/**
 *  Updates Sub Brand Model
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateSubCategoryByIdController = catchAsync(async (req, res) => {
  const subCategoryId = req.params.id;

  const subCategoryDetail = await SubCategory.findOne({ _id: subCategoryId });
  if (!subCategoryDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Sub category does not exist');
  }
  const { subCategoryName, categoryId } = req.body;
  // const category = await Category.findOne({ _id: categoryId });
  const category = await BrandAccessoriesService.getCategoryByIdService(categoryId);

  const userId = req.user.id;
  const subCategoryDetails = {
    ...req.body,
    subCategoryName,
    categoryName: category.categoryName,
    updatedBy: userId
  };
  res.json(await BrandAccessoriesService.updateSubCategoryByIdService(subCategoryId, subCategoryDetails, userId));
});

/**
 * get Category By Brand Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getCategoryByBrandIdController = catchAsync(async (req, res) => {
  const { brandId } = req.params;
  res.json(await BrandAccessoriesService.getCategoryByBrandIdService(brandId));
});

/**
 * get Category By category id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAccessoriesSubCategoryByCategoryId = catchAsync(async (req, res) => {
  const { categoryId } = req.params;
  res.json(await BrandAccessoriesService.getSubCategoryByCategoryIdService(categoryId));
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandAccessoriesDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getBrandAccessoriesDetails(options, search));
});

/**
 * get brand information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandWithoutAuth = catchAsync(async (req, res) => {
  res.json(await BrandAccessoriesService.getBrandWithoutAuth());
});

const getBrandWithoutAuthLimit = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getBrandWithoutAuthLimit(options, search));
});

const getHomePageBrandWithoutAuth = catchAsync(async (req, res) => {
  res.json(await BrandAccessoriesService.getHomePageBrandWithoutAuth());
});

/**
 * get BrandVehicle information By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBrandAccessories = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandAccessoriesService.getBrandAccessories(id));
});

/**
 *  Updates BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateBrandAccessories = catchAsync(async (req, res) => {
  const brandId = req.params.id;
  const userId = req.user.id;
  const { brandVehicleName } = req.body;
  const brandDetail = {
    ...req.body,
    brandVehicleName,
    updatedBy: userId
  };

  res.json(await BrandAccessoriesService.updateBrandAccessories(brandId, brandDetail, userId));
});

/**
 *  Deletes BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteBrandAccessories = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandAccessoriesService.deleteBrandAccessories(req.params.id, userId));
});

/**
 * add Brand Model in home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addBrandInHomePage = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandAccessoriesService.addBrandInHomePage(req.params.id, userId));
});
/**
 * get Brand Model By Brand Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeBrandInHomePage = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await BrandAccessoriesService.removeBrandInHomePage(req.params.id, userId));
});

/**
 * Add Brand Image in Brand BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const uploadBrandImage = catchAsync(async (req, res) => {
  const entity = {
    ...req.body,
    uploadBrandImage: req.file.originalname && req.file.originalname.length > 0 ? req.file.originalname : ''
  };
  res.json(await BrandAccessoriesService.uploadBrandImages(req.params.brandVehicleId, entity));
});

/**
 * get All information of  Brand Model  information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAccessoriesCategoryController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getAccessoriesCategoryService(search, options));
});

/**
 * get All information of Sub Brand Model  information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSubCategoryController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getSubCategoryService(search, options));
});

/**
 * get Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getCategoryByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandAccessoriesService.getCategoryByIdService(id));
});

/**
 * get Sub Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSubCategoryByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await BrandAccessoriesService.getSubCategoryByIdService(id));
});

/**
 * Delete Sub Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteSubCategoryByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await BrandAccessoriesService.deleteSubCategoryByIdService(id, userId));
});

/**
 * Delete  Brand Model By  Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteCategoryByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await BrandAccessoriesService.deleteCategoryByIdService(id, userId));
});

/**
 * Create Fuel Capacity Controller
 */
const creatVehicleNameValueController = catchAsync(async (req, res) => {
  const { variantName } = req.body;
  const variantDetail = {
    ...req.body,
    variantName
  };
  const result = await BrandAccessoriesService.creatVehicleNameValueService(variantDetail);
  res.status(httpStatus.CREATED).send(result);
});

/**
 * get fuel capacity information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleNameValueController = catchAsync(async (req, res) => {
  res.json(await BrandAccessoriesService.getVehicleNameValueService());
});

/**
 * get category information without auth
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAccessoriesCategoryNoAuth = catchAsync(async (req, res) => {
  res.json(await BrandAccessoriesService.getAccessoriesCategoryNoAuth());
});

/**
 * get category information without auth
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const getCategoryWithoutAuthLimit = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getCategoryWithoutAuthLimit(options, search));
});
/**
 * get SUb category information without auth
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAccessoriesSubCategoryNoAuth = catchAsync(async (req, res) => {
  res.json(await BrandAccessoriesService.getAccessoriesSubCategoryNoAuth());
});

/**
 * get sub category with now auth information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAccessoriesSubCategoryNoAuthLimit = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await BrandAccessoriesService.getAccessoriesSubCategoryNoAuthLimit(options, search));
});
module.exports = {
  createBrandAccessories,
  getBrandAccessoriesDetails,
  getBrandAccessories,
  updateBrandAccessories,
  deleteBrandAccessories,
  addBrandInHomePage,
  removeBrandInHomePage,
  uploadBrandImage,
  createCategoryController,
  createSubCategoryController,
  getCategoryByBrandIdController,
  getAccessoriesSubCategoryByCategoryId,
  getAccessoriesCategoryController,
  getCategoryByIdController,
  getSubCategoryByIdController,
  getSubCategoryController,
  deleteSubCategoryByIdController,
  deleteCategoryByIdController,
  updateCategoryController,
  updateSubCategoryByIdController,
  creatVehicleNameValueController,
  getVehicleNameValueController,
  getBrandWithoutAuth,
  getHomePageBrandWithoutAuth,
  getBrandWithoutAuthLimit,
  getCategoryWithoutAuthLimit,
  getAccessoriesCategoryNoAuth,
  getAccessoriesSubCategoryNoAuth,
  getAccessoriesSubCategoryNoAuthLimit
};
