const httpStatus = require('http-status');
const { reviewAndRatingService } = require('../services');
const catchAsync = require('../utils/catchAsync');
const { Client, Accessories } = require('../models');

const createReviewAndRating = catchAsync(async (req, res) => {
  const accessoriesValues = req.body;
  const accessoriesId = req.params.id;
  const userId = req.user.id;
  const userName = await Client.findOne({ _id: userId });
  const accessoriesName = await Accessories.findOne({ _id: accessoriesId });
  const _accessoriesValues = {
    ...accessoriesValues,
    createdBy: userId,
    fullName: userName.fullName,
    productTitle: accessoriesName.productTitle
  };
  const ratingAndReview = await reviewAndRatingService.createRating(userId, accessoriesId, _accessoriesValues);
  res.status(httpStatus.CREATED).send(ratingAndReview);
});

const getAllRatingAndReview = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const { search, filter } = res.pagination;
  const ratingAndReview = await reviewAndRatingService.getAllRating(search, filter, userId);
  res.send({
    code: 201,
    message: 'All Rating is Found.',
    ratingAndReview
  });
});

const getReviewOfProductId = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const accessoriesId = await reviewAndRatingService.getReviewOfProductId(userId);
  res.status(httpStatus.CREATED).send({ accessoriesId });
});

const deleteRatingAndReview = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const deleteId = req.params.id;
  await reviewAndRatingService.deleteRatingAndReview(deleteId, userId);
  res.send({
    code: 201,
    message: 'Product Rating is Deleted Successfully.'
  });
});

const getRatingById = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const productId = req.params.id;
  const ratingAndReview = await reviewAndRatingService.getRatingById(productId, userId);
  res.send({
    code: 201,
    message: 'Product Rating Is Found Using Product Id .',
    ratingAndReview
  });
});

const UpdateRating = catchAsync(async (req, res) => {
  const productId = req.params.id;
  const product = req.body;
  const userId = req.user.id;
  const ratingAndReview = await reviewAndRatingService.updateRatingById(productId, product, userId);
  res.send({
    code: 201,
    message: 'Product Rating Is Updated Successfully .',
    ratingAndReview
  });
});

/* Get rating of product by id  */
const getRatingOfProductById = catchAsync(async (req, res) => {
  const values = await reviewAndRatingService.getRatingOfProductById(req.params.id);
  res.send(values);
});

/* Approve rating by admin */
const approveRating = catchAsync(async (req, res) => {
  const ratingAndReview = await reviewAndRatingService.approvedFeature(req.params.id);
  res.send({
    code: '201',
    message: 'the rating is approved',
    ratingAndReview
  });
});

/* Dis approve rating by admin */
const removeRating = catchAsync(async (req, res) => {
  const ratingAndReview = await reviewAndRatingService.removedFeature(req.params.id);
  res.send({
    code: '201',
    message: 'Rating is unapproved',
    ratingAndReview
  });
});

module.exports = {
  createReviewAndRating,
  getAllRatingAndReview,
  deleteRatingAndReview,
  getRatingById,
  getRatingOfProductById,
  UpdateRating,
  approveRating,
  removeRating,
  getReviewOfProductId
};
