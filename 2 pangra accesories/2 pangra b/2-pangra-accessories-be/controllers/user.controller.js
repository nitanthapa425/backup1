/**
 * Copyright (C) Two Pangra
 */

/**
 * the user controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const httpStatus = require('http-status');
// eslint-disable-next-line no-use-before-define
const { UserService } = require('../services');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');
// eslint-disable-next-line import/order
/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getUserProfileList(search, options, req.user.id));
});

/* Get Deleted User List   */
const getDeletedUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getDeletedUserProfileList(search, options, req.user.id));
});

/* Get User Profile By Id */
const getUserProfileByIdController = catchAsync(async (req, res) => {
  const userId = req.params.id;
  const adminId = req.user.id;
  res.send(await UserService.getUserProfileByIdService(userId, adminId));
});

/* Get User Profile By Id */
const updateUserProfileByIdController = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const updatedBy = req.user.id;
  const userId = req.params.id;
  const dateOfBirth = new Date(req.body.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');
  const userDetail = {
    ...req.body,
    fullName,
    updatedBy,
    dob
  };

  res.send(await UserService.updateUserProfileByIdService(userId, userDetail, updatedBy));
});

/* Get User Profile By Id */
const deleteUserProfileByIdController = catchAsync(async (req, res) => {
  const deleteId = req.params.id;
  const userId = req.user.id;
  res.send(await UserService.deleteUserProfileByIdService(userId, deleteId));
});

/* Get Deleted User Profile By Id */
const getDeletedUserById = catchAsync(async (req, res) => {
  const userId = req.params.id;
  res.send(await UserService.getDeletedUserById(userId));
});

/* Get User Profile By Id */
const revertDeletedUserById = catchAsync(async (req, res) => {
  const adminId = req.user.id;
  const userId = req.params.id;
  res.send(await UserService.revertDeletedUserById(userId, adminId));
});

module.exports = {
  getUserProfileList,
  getDeletedUserProfileList,
  getUserProfileByIdController,
  updateUserProfileByIdController,
  deleteUserProfileByIdController,
  getDeletedUserById,
  revertDeletedUserById
};
