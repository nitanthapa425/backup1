/**
 * Copyright (C) Two Pangra
 */

/**
 * the user controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// const httpStatus = require('http-status');
// eslint-disable-next-line no-use-before-define
const { DeliveryPersonService } = require('../services');
const catchAsync = require('../utils/catchAsync');
const { capitalize } = require('../common/helper');
// eslint-disable-next-line import/order
/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await DeliveryPersonService.getUserProfileList(search, options));
});

/* Get Deleted User List   */
const getDeletedUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await DeliveryPersonService.getDeletedUserProfileList(search, options, req.user.id));
});

/* Get User Profile By Id */
const getUserProfileByIdController = catchAsync(async (req, res) => {
  const userId = req.params.id;
  const adminId = req.user.id;
  res.send(await DeliveryPersonService.getUserProfileByIdService(userId, adminId));
});

/* Get User Profile By Id */
const updateUserProfileByIdController = catchAsync(async (req, res) => {
  const data = req.body;
  const fullName = req.body.middleName
    ? capitalize(data.firstName).concat(' ') + capitalize(data.middleName).concat(' ') + capitalize(data.lastName)
    : capitalize(data.firstName).concat(' ') + capitalize(data.lastName);
  const updatedBy = req.user.id;
  const userId = req.params.id;
  const dateOfBirth = new Date(req.body.dob);
  const dob = dateOfBirth.toLocaleDateString('fr-CA');
  const userDetail = {
    ...req.body,
    fullName,
    updatedBy,
    dob
  };

  res.send(await DeliveryPersonService.updateUserProfileByIdService(userId, userDetail, updatedBy));
});

/* Get User Profile By Id */
const deleteUserProfileByIdController = catchAsync(async (req, res) => {
  const deleteId = req.params.id;
  const userId = req.user.id;
  res.send(await DeliveryPersonService.deleteUserProfileByIdService(userId, deleteId));
});

/* Get Deleted User Profile By Id */
const getDeletedUserById = catchAsync(async (req, res) => {
  const userId = req.params.id;
  res.send(await DeliveryPersonService.getDeletedUserById(userId));
});

/* Get User Profile By Id */
const revertDeletedUserById = catchAsync(async (req, res) => {
  const adminId = req.user.id;
  const userId = req.params.id;
  res.send(await DeliveryPersonService.revertDeletedUserById(userId, adminId));
});

/* Get Delivery Person to be delivered products list */
const getDeliveryAccessoriesList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const userId = req.user.id;
  res.send(await DeliveryPersonService.getDeliveryAccessoriesList(search, options, userId));
});

/* Get Delivery Person to be delivered products list */
const companyPersonByCompanyId = catchAsync(async (req, res) => {
  res.send(await DeliveryPersonService.companyPersonByCompanyId(req.params.id));
});

/* Get Delivery Person to be delivered products list */
const getDeliveryDetailsOfUser = catchAsync(async (req, res) => {
  res.send(await DeliveryPersonService.getDeliveryDetailsOfUser(req.params.id, req.user.id));
});

module.exports = {
  getUserProfileList,
  getDeletedUserProfileList,
  getUserProfileByIdController,
  updateUserProfileByIdController,
  deleteUserProfileByIdController,
  getDeletedUserById,
  revertDeletedUserById,
  getDeliveryAccessoriesList,
  companyPersonByCompanyId,
  getDeliveryDetailsOfUser
};
