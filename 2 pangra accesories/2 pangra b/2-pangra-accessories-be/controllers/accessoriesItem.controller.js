/* eslint-disable no-unused-vars */
/* eslint-disable operator-linebreak */
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { accessoriesService, BrandAccessoriesService } = require('../services');
const helper = require('../utils/helper');

/* Create category */
const createAccessoriesItem = catchAsync(async (req, res) => {
  /* Get details of brand, category and subcategory */
  const brandDetail = await BrandAccessoriesService.getBrandAccessories(req.body.brandId);
  const categoryDetail = await BrandAccessoriesService.getCategoryByIdService(req.body.categoryId);
  const subCategoryDetail = await BrandAccessoriesService.getSubCategoryByIdService(req.body.subCategoryId);
  // eslint-disable-next-line operator-linebreak
  // eslint-disable-next-line object-curly-newline
  const { discountPrice, costPrice, warrantyTime, warrantyUnit, returnableTime, returnableUnit, weight, weightUnit } =
    req.body;

  /* Convert weight into kg from gm */
  const weightValueInKg = helper.convertGmToKg(weight, weightUnit);

  /* Concat warranty time and warranty unit  */
  const concatWarrantyTime = warrantyTime.concat(' ') + warrantyUnit;

  /* Concat returnable time and returnable unit  */
  const concatReturnableTime = returnableTime.concat(' ') + returnableUnit;
  const discountPriceValue =
    typeof discountPrice === 'undefined' || discountPrice === null || discountPrice === '' ? '0' : discountPrice;

  /* Find discount percentage from total price and discount price */
  const discountPercentage = (discountPriceValue / costPrice) * 100;
  /* If undefined or empty string  */
  // eslint-disable-next-line operator-linebreak
  const tagLine =
    typeof req.body.tagLine !== 'undefined' && req.body.tagLine !== '' ? req.body.tagLine.concat(' ') : '';
  const userId = req.user.id;
  const values = {
    ...req.body,
    brandName: brandDetail.brandName,
    categoryName: categoryDetail.categoryName,
    subCategoryName: subCategoryDetail.subCategoryName,
    productTitle: `${brandDetail.brandName.concat(' ') + tagLine + subCategoryDetail.subCategoryName.concat(' ')}`,
    discountPercentage,
    warrantyTime: concatWarrantyTime,
    returnableTime: concatReturnableTime,
    weight: weightValueInKg,
    createdBy: userId,
    createdAt: new Date()
  };
  const accessoriesDetail = await accessoriesService.createAccessoriesItem(values, userId);
  res.send(httpStatus.CREATED, {
    message: 'Accessories is posted successfully.',
    accessoriesId: accessoriesDetail._id
  });
});

/* Get all products  */
const getAccessoriesItems = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  // const userId = req.user.id;
  const result = await accessoriesService.getAccessoriesItems(search, options);
  res.send(result);
});

/* Get category by id */
const getAccessoriesItem = catchAsync(async (req, res) => {
  const result = await accessoriesService.getAccessoriesItem(req.params.id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Accessories not found');
  }
  res.send(result);
});

/* Update accessories by id */
const updateAccessoriesItem = catchAsync(async (req, res) => {
  const accessoriesId = req.params.id;

  /* Get details of brand, category and subcategory */
  const brandDetail = await BrandAccessoriesService.getBrandAccessories(req.body.brandId);
  const categoryDetail = await BrandAccessoriesService.getCategoryByIdService(req.body.categoryId);
  const subCategoryDetail = await BrandAccessoriesService.getSubCategoryByIdService(req.body.subCategoryId);
  // eslint-disable-next-line operator-linebreak
  // eslint-disable-next-line object-curly-newline
  const { discountPrice, costPrice, warrantyTime, warrantyUnit, returnableTime, returnableUnit, weight, weightUnit } =
    req.body;

  /* Convert weight into kg from gm */
  const weightValueInKg = helper.convertGmToKg(weight, weightUnit);

  /* Concat warranty time and warranty unit  */
  const concatWarrantyTime = warrantyTime.concat(' ') + warrantyUnit;

  /* Concat returnable time and returnable unit  */
  const concatReturnableTime = returnableTime.concat(' ') + returnableUnit;

  /* Check if the discount percentage is null or empty string */
  const newDiscountPrice =
    typeof req.body.discountPrice !== 'undefined' && req.body.discountPrice !== ''
      ? req.body.discountPrice
      : 0;
  /* Find discount percentage from total price and discount price */
  const discountPercentage = ((costPrice - newDiscountPrice) / costPrice) * 100;
  /* If undefined or empty string  */
  // eslint-disable-next-line operator-linebreak
  const tagLine =
    typeof req.body.tagLine !== 'undefined' && req.body.tagLine !== '' ? req.body.tagLine.concat(' ') : '';
  const userId = req.user.id;
  const accessoriesDetails = {
    ...req.body,
    brandName: brandDetail.brandName,
    categoryName: categoryDetail.categoryName,
    subCategoryName: subCategoryDetail.subCategoryName,
    productTitle: `${brandDetail.brandName.concat(' ') + tagLine + subCategoryDetail.subCategoryName.concat(' ')}`,
    discountPercentage,
    warrantyTime: concatWarrantyTime,
    returnableTime: concatReturnableTime,
    weight: weightValueInKg,
    updatedBy: userId,
    updatedAt: new Date()
  };

  await accessoriesService.updateAccessoriesItemById(accessoriesId, accessoriesDetails, userId);
  res.send(httpStatus.CREATED, {
    message: 'Accessories has been updated successfully'
  });
});

/* Delete accessories by id */
const deleteAccessoriesItem = catchAsync(async (req, res) => {
  await accessoriesService.deleteAccessoriesItemById(req.user.id, req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Accessories has been deleted successfully'
  });
});

/**
 * Add Accessories in Home page feature
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addAccessoriesInFeature = catchAsync(async (req, res) => {
  res.json(await accessoriesService.addAccessoriesInFeature(req.params.id, req.user.id));
});

/**
 * Remove Accessories in Home page feature
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeAccessoriesFromFeature = catchAsync(async (req, res) => {
  res.json(await accessoriesService.removeAccessoriesFromFeature(req.params.id, req.user.id));
});
// /* Approve comment by admin */
// const approveCommentByAdmin = catchAsync(async (req, res) => {
//   await accessoriesService.approveCommentByAdmin(req.params.id);
//   res.send(httpStatus.CREATED, {
//     message: 'Comment has been approved successfully'
//   });
// });

/**
 * Get All Accessories in home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllFeaturedAccessoriesNoAuth = catchAsync(async (req, res) => {
  res.json(await accessoriesService.getAllFeaturedAccessoriesNoAuth());
});

/**
 * Add Accessories in Home page feature
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addAccessoriesInHotDeals = catchAsync(async (req, res) => {
  res.json(await accessoriesService.addAccessoriesInHotDeals(req.params.id, req.user.id));
});

/**
 * Remove Accessories in Home page feature
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeAccessoriesInHotDeals = catchAsync(async (req, res) => {
  res.json(await accessoriesService.removeAccessoriesInHotDeals(req.params.id, req.user.id));
});

/**
 * Get all hot deals accessories item for home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllHotDeals = catchAsync(async (req, res) => {
  res.json(await accessoriesService.getAllHotDeals());
});

/* Get all hot deals accessories */
const getHotDealAccessoriesItems = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const result = await accessoriesService.getHotDealAccessoriesItems(search, options);
  res.send(result);
});

/* Get Recommended of Products */
const getRecommendedAccessoriesItem = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await accessoriesService.getRecommendedAccessoriesItem(userId));
});

const deleteInActiveAccessoriesItem = catchAsync(async (req, res) => {
  const item = await accessoriesService.deleteInActiveAccessoriesItem(req.params.id, req.user.id);
  res.send({
    code: '201',
    message: 'Product is Deleted Successfully'
  });
});

const deleteAccesoriesItemPermanently = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const deleteId = req.params.id;
  const item = await accessoriesService.deleteAccessoriesItem(userId, deleteId);
  res.send({
    code: '201',
    message: 'Product is deleted successfully'
  });
});
module.exports = {
  createAccessoriesItem,
  getAccessoriesItems,
  getAccessoriesItem,
  updateAccessoriesItem,
  deleteAccessoriesItem,
  addAccessoriesInFeature,
  removeAccessoriesFromFeature,
  getAllFeaturedAccessoriesNoAuth,
  addAccessoriesInHotDeals,
  removeAccessoriesInHotDeals,
  getAllHotDeals,
  getHotDealAccessoriesItems,
  getRecommendedAccessoriesItem,
  deleteInActiveAccessoriesItem,
  deleteAccesoriesItemPermanently
  // approveCommentByAdmin
};
