const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { draftAccessoriesService, BrandAccessoriesService } = require('../services');
const helper = require('../utils/helper');
// const { capitalize } = require('../utils/helper');

/* Create category */
const createDraftAccessoriesItem = catchAsync(async (req, res) => {
  /* Get details of brand, category and subcategory */
  const brandDetail = await BrandAccessoriesService.getBrandAccessories(req.body.brandId);
  const categoryDetail = await BrandAccessoriesService.getCategoryByIdService(req.body.categoryId);
  const subCategoryDetail = await BrandAccessoriesService.getSubCategoryByIdService(req.body.subCategoryId);
  // eslint-disable-next-line operator-linebreak
  // eslint-disable-next-line object-curly-newline
  const { discountPrice, costPrice, warrantyTime, warrantyUnit, returnableTime, returnableUnit, weight, weightUnit } = req.body;

  /* Convert weight into kg from gm */
  const weightValueInKg = helper.convertGmToKg(weight, weightUnit);

  /* Concat warranty time and warranty unit  */
  const concatWarrantyTime = warrantyTime.concat(' ') + warrantyUnit;

  /* Concat returnable time and returnable unit  */
  const concatReturnableTime = returnableTime.concat(' ') + returnableUnit;
  const discountPriceValue = typeof discountPrice === 'undefined' || discountPrice === null || discountPrice === '' ? '0' : discountPrice;

  /* Find discount percentage from total price and discount price */
  const discountPercentage = (discountPriceValue / costPrice) * 100;
  /* If undefined or empty string  */
  // eslint-disable-next-line operator-linebreak
  const tagLine =
    typeof req.body.tagLine !== 'undefined' && req.body.tagLine !== '' ? req.body.tagLine.concat(' ') : '';
  const userId = req.user.id;
  const values = {
    ...req.body,
    brandName: brandDetail.brandName,
    categoryName: categoryDetail.categoryName,
    subCategoryName: subCategoryDetail.subCategoryName,
    productTitle: `${brandDetail.brandName.concat(' ') + tagLine + subCategoryDetail.subCategoryName.concat(' ')}`,
    discountPercentage,
    warrantyTime: concatWarrantyTime,
    returnableTime: concatReturnableTime,
    weight: weightValueInKg,
    createdBy: userId,
    createdAt: new Date()
  };
  await draftAccessoriesService.createDraftAccessoriesItem(values, userId);
  res.send(httpStatus.CREATED, {
    message: 'Product has been saved in draft successfully.',
  });
});

/* Get all categories  */
const getDraftAccessoriesItems = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const userId = req.user.id;
  res.send(await draftAccessoriesService.getDraftAccessoriesItems(options, search, userId));
});

/* Get category by id */
const getDraftAccessoriesItem = catchAsync(async (req, res) => {
  const result = await draftAccessoriesService.getDraftAccessoriesItem(req.params.id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories not found');
  }
  res.send(result);
});

/* Update accessories by id */
const updateDraftAccessoriesItemById = catchAsync(async (req, res) => {
  /* Get details of brand, category and subcategory */
  const brandDetail = await BrandAccessoriesService.getBrandAccessories(req.body.brandId);
  const categoryDetail = await BrandAccessoriesService.getCategoryByIdService(req.body.categoryId);
  const subCategoryDetail = await BrandAccessoriesService.getSubCategoryByIdService(req.body.subCategoryId);
  // eslint-disable-next-line operator-linebreak
  // eslint-disable-next-line object-curly-newline
  const { discountPrice, costPrice, warrantyTime, warrantyUnit, returnableTime, returnableUnit, weight, weightUnit } = req.body;

  /* Convert weight into kg from gm */
  const weightValueInKg = helper.convertGmToKg(weight, weightUnit);

  /* Concat warranty time and warranty unit  */
  const concatWarrantyTime = warrantyTime.concat(' ') + warrantyUnit;

  /* Concat returnable time and returnable unit  */
  const concatReturnableTime = returnableTime.concat(' ') + returnableUnit;
  const discountPriceValue = typeof discountPrice === 'undefined' || discountPrice === null || discountPrice === '' ? '0' : discountPrice;

  /* Find discount percentage from total price and discount price */
  const discountPercentage = (discountPriceValue / costPrice) * 100;
  /* If undefined or empty string  */
  // eslint-disable-next-line operator-linebreak
  const tagLine =
    typeof req.body.tagLine !== 'undefined' && req.body.tagLine !== '' ? req.body.tagLine.concat(' ') : '';
  const userId = req.user.id;
  const values = {
    ...req.body,
    brandName: brandDetail.brandName,
    categoryName: categoryDetail.categoryName,
    subCategoryName: subCategoryDetail.subCategoryName,
    productTitle: `${brandDetail.brandName.concat(' ') + tagLine + subCategoryDetail.subCategoryName.concat(' ')}`,
    discountPercentage,
    warrantyTime: concatWarrantyTime,
    returnableTime: concatReturnableTime,
    weight: weightValueInKg,
    createdBy: userId,
    createdAt: new Date()
  };
  const draftId = req.params.id;
  await draftAccessoriesService.updateDraftAccessoriesItemById(userId, draftId, values);
  res.send(httpStatus.CREATED, {
    message: 'Accessories Draft is updated successfully.',
  });
});

// /* Approve comment by admin */
// const approveCommentByAdmin = catchAsync(async (req, res) => {
//   await draftAccessoriesService.approveCommentByAdmin(req.params.id);
//   res.send(httpStatus.CREATED, {
//     message: 'Comment has been approved successfully'
//   });
// });

/* Delete category by id */
const finishDraftAccessoriesItemById = catchAsync(async (req, res) => {
  const value = await draftAccessoriesService.finishDraftAccessoriesItemById(req.user.id, req.body, req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Draft has been finish successfully.',
    draftId: value._id
  });
});

const deleteDraftItem = catchAsync(async (req, res) => {
  const deleteId = req.params.id;
  const userId = req.user.id;
  await draftAccessoriesService.deleteDraftItem(deleteId, userId);
  res.send({
    code: '201',
    message: 'Accessories Draft is deleted successfully'
  });
});
module.exports = {
  createDraftAccessoriesItem,
  getDraftAccessoriesItems,
  getDraftAccessoriesItem,
  updateDraftAccessoriesItemById,
  // approveCommentByAdmin,
  finishDraftAccessoriesItemById,
  deleteDraftItem
};
