/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */
const ImageUploadService = require('../services/imageUpload.service');
const catchAsync = require('../utils/catchAsync');
/**
 * Add Image in different Routes
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const uploadImages = catchAsync(async (req, res) => {
  const entity = {
    imageOriginalName:
      typeof req.files !== 'undefined' && req.files && req.files.length > 0
        ? [...req.files.map((file) => file.originalname)]
        : [],
    imagePath:
      typeof req.files !== 'undefined' && req.files && req.files.length > 0
        ? [...req.files.map((file) => file.filename)]
        : [],
    imageOriginalPath:
      typeof req.files !== 'undefined' && req.files && req.files.length > 0
        ? [...req.files.map((file) => file.path)]
        : []
  };
  res.json(await ImageUploadService.uploadImages(entity, req));
});

/**
*  Delete Images
* @param {Object} req the http request
* @param {Object} res the http response
*/
const deleteImages = catchAsync(async (req, res) => {
  res.json(await ImageUploadService.deleteImages(req.params.fileName));
});

/**
*  Get All Images
* @param {Object} req the http request
* @param {Object} res the http response
*/
const getAllImages = catchAsync(async (req, res) => {
  res.json(await ImageUploadService.getAllImages());
});
module.exports = {
  uploadImages,
  deleteImages,
  getAllImages
};
