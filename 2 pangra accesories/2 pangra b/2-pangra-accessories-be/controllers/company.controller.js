const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { companyService } = require('../services');
const { Company } = require('../models');
// const { capitalize } = require('../utils/helper');

/* Create company detail */
const createCompanyDetail = catchAsync(async (req, res) => {
  const ifCompanyExist = await Company.findOne({ companyName: req.body.companyName });

  const ifPhoneExist = await Company.findOne({ mobile: req.body.mobile });

  if (ifCompanyExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Company name already exist');
  }
  if (ifPhoneExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Mobile number already exist');
  }
  const value = await companyService.createCompanyDetail(req.body, req.user.id);
  res.send(httpStatus.CREATED, {
    message: 'Company detail is posted successfully.',
    companyId: value._id
  });
});

/* Get all company details  */
const getCompanyDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const result = await companyService.getCompanyDetails(search, options, req.user.id);
  res.send(result);
});

/* Get company detail by id */
const getCompanyDetail = catchAsync(async (req, res) => {
  const company = await companyService.getCompanyDetailById(req.params.id);
  if (!company) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Company detail not found');
  }
  res.send(company);
});

/* Update company detail by id */
const updateCompanyDetail = catchAsync(async (req, res) => {
  const companyId = req.params.id;
  const companyDetail = req.body;
  const userId = req.user.id;
  await companyService.updateCompanyDetailById(companyId, companyDetail, userId);
  res.send(httpStatus.CREATED, {
    message: 'Company detail has been updated successfully'
  });
});

/* Delete company detail by id */
const deleteCompanyDetail = catchAsync(async (req, res) => {
  await companyService.deleteCompanyDetailById(req.params.id, req.user.id);
  res.send(httpStatus.CREATED, {
    message: 'Company detail has been removed successfully'
  });
});

module.exports = {
  createCompanyDetail,
  getCompanyDetails,
  getCompanyDetail,
  updateCompanyDetail,
  deleteCompanyDetail
};
