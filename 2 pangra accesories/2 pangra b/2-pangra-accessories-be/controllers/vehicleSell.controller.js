/**
 * Copyright (C) Two Pangra
 */

/**
 * Vehicle Detail Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const errors = require('common-errors');
const { VehicleSellService } = require('../services');
// const { VehicleDetailService } = require('../services');
// const _ = require('lodash');
// const { Client } = require('../models');
const catchAsync = require('../utils/catchAsync');

/**
 * Post BrandVehicle Sell information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

// const createVehicleDetailSellInFeature = catchAsync(async (req, res) => {
//   const vehicleId = req.body.vehicleDetailId;
//   const vehicleDetailById = await VehicleDetailService.getVehicleDetail(vehicleId);

//   const { vehicleName, brandName, varientName, modelName } = vehicleDetailById;

//   const date = new Date(req.body.makeYear);
//   const makeYears = date.getFullYear();

//   const newDate = date.setDate(date.getDate() + 1);
//   const timeFormat = new Date(newDate);
//   const makeYearDate = timeFormat.toISOString();

//   const nameValue = vehicleName.split(' ');
//   const [brand, model, variant] = nameValue;
//   const vehicleNameInsell = brand.concat(' ') + model.concat(' ') + variant.concat(' ') + 'cc'.concat(' ') + makeYears;
//   const vehicleDetail = {
//     ...req.body,
//     vehicleName: vehicleNameInsell,
//     makeYear: makeYearDate,
//     createdBy: req.user.id,
//     brandName,
//     varientName,
//     modelName,
//     hasInFeature: true
//   };

//   await VehicleSellService.createVehicleDetailSellInFeature(vehicleDetail);
//   res.status(httpStatus.CREATED).send({
//     code: 201,
//     message: 'Feature Vehicle Sell is created successfully.'
//   });
// });

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getSellVehicleDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getSellVehicleDetails(search, options));
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
// const getFeatureVehicleSellDetails = catchAsync(async (req, res) => {
//   const { options, search } = res.pagination;
//   res.json(await VehicleSellService.getFeatureVehicleSellDetails(search, options));
// });

/**
 * Get Specific Vehicle Detail by Id
 */
const getVehicleSellDetail = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await VehicleSellService.getVehicleSellDetail(id));
});

/**
 * Get Specific Vehicle Detail by Id
 */
// const getFeatureVehicleSellDetail = catchAsync(async (req, res) => {
//   const { id } = req.params;
//   res.send(await VehicleSellService.getFeatureVehicleSellDetail(id));
// });

/**
 * Add Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addFeatureInHomePage = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addFeatureInHomePage(req.params.id));
});
/**
 * Remove Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeFeatureInHomePage = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.removeFeatureInHomePage(req.params.id));
});

/**
 *  Deletes BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteVehicleSellDetail = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await VehicleSellService.deleteVehicleSellDetail(id));
});

/**
 *  Upload Image
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const uploadVehicleSellImages = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await VehicleSellService.uploadVehicleSellImages(id));
});

/**
 *  Search  BrandVehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleSellDraftDetailsController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getVehicleSellDraftDetailsService(search, options));
});

/**
 * Get Specific Draft Vehicle Detail by Id
 */
const getVehicleSellDraftDetailsByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await VehicleSellService.getVehicleSellDraftDetailsByIdService(id));
});

/**
 * Get Specific Draft Vehicle Detail by Id
 */
const getUserVehicleSellDraftController = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.send(await VehicleSellService.getUserVehicleSellDraftService(userId));
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getUserVehicleSellController = catchAsync(async (req, res) => {
  const { id } = req.user;
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getUserVehicleSellService(search, options, id));
});

/**
 * create Vehicle Draft to  Vehicle Service Detail by Id
 */
const createVehicleSellDraftToVehicleController = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.createVehicleSellDraftToVehicleService(req.params.id));
});

// const getAllVehicleValueController = catchAsync(async (req, res) => {
//   res.send(await VehicleSellService.getAllVehicleValueService());
// });

/**
 *  Updates Vehicle Sell Draft Detail
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteVehicleSellDraftDetailController = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.deleteVehicleSellDraftService(req.params.id));
});

/**
 *  Add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addToCart = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addToCart(req.user.id, req.params.id));
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getClientAddToCart = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.getClientAddToCart(req.user.id));
});

/* Get category by id */
const getCartById = catchAsync(async (req, res) => {
  const cart = await VehicleSellService.getCartById(req.params.id);
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Cart is not found');
  }
  res.send(cart);
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
/* Delete category by id */
const deleteCart = catchAsync(async (req, res) => {
  await VehicleSellService.deleteCart(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Cart is deleted successfully'
  });
});
/**
 *  Add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addTowishlist = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addTowishlist(req.user.id, req.params.id));
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getClientWishlist = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.getClientWishlist(req.user.id));
});

/* Get category by id */
const getWishlistById = catchAsync(async (req, res) => {
  const cart = await VehicleSellService.getWishlistById(req.params.id);
  if (!cart) {
    throw new httpStatus.NOT_FOUND('Wishlist is not found');
  }
  res.send(cart);
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
/* Delete category by id */
const deleteWishlist = catchAsync(async (req, res) => {
  await VehicleSellService.deleteWishlist(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Wishlist is deleted successfully'
  });
});

/**
 *  Add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addToCartWishlist = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addToCartWishlist(req.user.id, req.params.id));
});

/**
 *  Get Hot Deals
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleSellHotDeals = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.getVehicleSellHotDeals());
});

const getAdminVehicleSellHotDeals = catchAsync(async (req, res) => {
  const { search, options } = res.pagination;
  res.json(await VehicleSellService.getAdminVehicleSellHotDeals(search, options));
});
/**
 * Add Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addToHotDeals = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addToHotDeals(req.params.id));
});
/**
 * Remove Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const removeFromHotDeal = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.removeFromHotDeal(req.params.id));
});

/**
 * get cart information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAddToCart = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getAddToCart(search, options));
});
/**
 * get wishlist information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getWishlist = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getWishlist(search, options));
});

/* Delete Vehicle in Wishlist by id */
const deleteWishlistByAdmin = catchAsync(async (req, res) => {
  await VehicleSellService.deleteWishlistByAdmin(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Wishlist is deleted successfully'
  });
}); /* Delete Cart by id */
const deleteCartByAdmin = catchAsync(async (req, res) => {
  await VehicleSellService.deleteCartByAdmin(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Cart is deleted successfully'
  });
});
/* get Booked vehicle by id */
const getBookedVehicleByAdmin = catchAsync(async (req, res) => {
  const book = await VehicleSellService.getBookedVehicleByAdmin(req.params.id);
  if (!book) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Book vehicle is not found');
  }
  res.send(book);
});

/* get wishlist by id */
const getWishlistByAdmin = catchAsync(async (req, res) => {
  const wishlist = await VehicleSellService.getWishlistByAdmin(req.params.id);
  if (!wishlist) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle is not found');
  }
  res.send(wishlist);
});

/* get cart by id */
const getCartByAdmin = catchAsync(async (req, res) => {
  const cart = await VehicleSellService.getCartByAdmin(req.params.id);
  if (!cart) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Vehicle is not found');
  }
  res.send(cart);
});

/**
 * Verify Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const verifySellerVehicle = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.verifySellerVehicle(req.params.id));
});
/**
 * Unverify Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const unverifySellerVehicle = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.unverifySellerVehicle(req.params.id));
});

/**
 * Update Buyer location
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateLocation = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.updateLocation(req.user.id, req.body));
});

/**
 * Approve Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const approveSellerVehicle = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.approveSellerVehicle(req.params.id));
});
/**
 * Unapprove Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const unApproveSellerVehicle = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.unApproveSellerVehicle(req.params.id));
});

/**
 * Book a meeting with Seller
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const meetingSchedule = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const bookId = req.params.id;
  const meetingValues = {
    ...req.body,
    createdBy: userId
  };
  res.json(await VehicleSellService.meetingSchedule(bookId, meetingValues));
});

/* Make vehicle as sold */
const makeVehicleSellAsSold = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.makeVehicleSellAsSold(req.body.vehicleSellId, req.user.id));
});

/* Change vehicle sell to unsold */
const makeVehicleSellAsUnsold = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.makeVehicleSellAsUnsold(req.body.vehicleSellId, req.user.id));
});

/**
 * get BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getMeetingSchedules = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getMeetingSchedules(search, options));
});

/**
 * Get Specific Meeting Schedule Detail by Id
 */
const getMeetingSchedule = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await VehicleSellService.getMeetingSchedule(id));
});

/**
 * Update Specific Meeting Schedule Detail by Id
 */
const updateMeetingSchedule = catchAsync(async (req, res) => {
  const { id, deleteId } = req.params;
  const { scheduleBody } = req.body;
  res.send(await VehicleSellService.updateMeetingSchedule(id, deleteId, scheduleBody));
});

/**
 * Delete Specific Meeting Schedule Detail by Id
 */
const deleteMeetingSchedule = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.send(await VehicleSellService.deleteMeetingSchedule(id));
});

/* Remove ad by seller */
const removeAdByVehicleSeller = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.send(await VehicleSellService.removeAdByVehicleSeller(id, userId));
});

/**
 * Add Seller Vehicle in Home page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const addFeatureInHomePageByClient = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.addFeatureInHomePageByClient(req.params.id));
});

/**
 * Offer Price to Seller Vehicle
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const offerPriceToSeller = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.offerPriceToSeller(req.user.id, req.params.id, req.body));
});
/**
 * Approve buyer offer by Admin
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const aprroveBuyerOffer = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.aprroveBuyerOffer(req.user.id, req.params.id, req.body));
});

/**
 * Reject buyer offer by Admin
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const rejectBuyerOffer = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.rejectBuyerOffer(req.user.id, req.params.id, req.body));
});

/**
 * Reject buyer offer by Admin
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const finalOfferFromSeller = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.finalOfferFromSeller(req.user.id, req.params.id, req.body));
});

/**
 * get Offer details
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getOfferDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await VehicleSellService.getOfferDetails(search, options));
});

/**
 * get Offer details
 * @param {Object} req the http request
 * @param {Array of Object} res the http response
 */

const getClientOfferDetail = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.getClientOfferDetail(req.user.id, req.params.id));
});

/**
 * Delete Offer by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const deleteOfferById = catchAsync(async (req, res) => {
  res.json(await VehicleSellService.deleteOfferById(req.params.id));
});

module.exports = {
  getSellVehicleDetails,
  getVehicleSellDetail,
  deleteVehicleSellDetail,
  addFeatureInHomePage,
  removeFeatureInHomePage,
  uploadVehicleSellImages,
  getVehicleSellDraftDetailsController,
  getVehicleSellDraftDetailsByIdController,
  getUserVehicleSellDraftController,
  getUserVehicleSellController,
  createVehicleSellDraftToVehicleController,
  deleteVehicleSellDraftDetailController,
  addToCart,
  getClientAddToCart,
  getCartById,
  deleteCart,
  addToCartWishlist,
  getVehicleSellHotDeals,
  addToHotDeals,
  removeFromHotDeal,
  addTowishlist,
  getClientWishlist,
  getWishlistById,
  deleteWishlist,
  getAddToCart,
  getWishlist,
  deleteWishlistByAdmin,
  deleteCartByAdmin,
  getBookedVehicleByAdmin,
  getWishlistByAdmin,
  getCartByAdmin,
  verifySellerVehicle,
  unverifySellerVehicle,
  updateLocation,
  approveSellerVehicle,
  unApproveSellerVehicle,
  meetingSchedule,
  getMeetingSchedules,
  getMeetingSchedule,
  updateMeetingSchedule,
  deleteMeetingSchedule,
  makeVehicleSellAsSold,
  makeVehicleSellAsUnsold,
  getAdminVehicleSellHotDeals,
  removeAdByVehicleSeller,
  addFeatureInHomePageByClient,
  offerPriceToSeller,
  aprroveBuyerOffer,
  rejectBuyerOffer,
  getOfferDetails,
  finalOfferFromSeller,
  deleteOfferById,
  getClientOfferDetail
  // getAllVehicleValueController
};
