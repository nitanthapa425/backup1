/**
 * Copyright (C) Two Pangra
 */

/**
 * Vehicle Detail Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
// const errors = require('common-errors');
const { buyNowService } = require('../services');
const ApiError = require('../utils/ApiError');
// const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
// const { capitalize } = require('../common/helper');
const { User, DeliveryPerson } = require('../models');

/**
 *  Buy Now
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const buyNow = catchAsync(async (req, res) => {
  res.json(await buyNowService.buyNow(req.user.id, req.body));
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getCustomerBuyNow = catchAsync(async (req, res) => {
  res.json(await buyNowService.getCustomerBuyNow(req.user.id));
});

/* Get buy now by id */
const getBuyNowDetail = catchAsync(async (req, res) => {
  const result = await buyNowService.getBuyNowDetail(req.params.id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Buy now detail is not found');
  }
  res.send(result);
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
/* Delete category by id */
const deleteBuyNowByCustomer = catchAsync(async (req, res) => {
  await buyNowService.deleteBuyNowByCustomer(req.params.id, req.user.id);
  res.send(httpStatus.CREATED, {
    message: 'Product has been cancelled successfully.'
  });
});

/**
 * get cart information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBuyNowDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const userId = req.user.id;
  res.json(await buyNowService.getBuyNowDetails(search, options, userId));
});

/* Delete Booked Vehicle by id */
const deleteBuyNow = catchAsync(async (req, res) => {
  await buyNowService.deleteBuyNowById(req.params.id, req.user.id);
  res.send(httpStatus.CREATED, {
    message: 'The ordered product has been cancelled.'
  });
});

/* Delete Booked Vehicle by id */
const deleteBuyNowByAdmin = catchAsync(async (req, res) => {
  await buyNowService.deleteBuyNowByAdmin(req.params.id, req.user.id);
  res.send(httpStatus.CREATED, {
    message: 'The ordered product has been cancelled.'
  });
});

/**
 *  Get customer buy now by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const getCustomerBuyNowById = catchAsync(async (req, res) => {
  const value = await buyNowService.getCustomerBuyNowById(req.user.id, req.params.id);
  res.send(value);
});

/**
 * Cancel Order by Admin
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const changeOrderStatus = catchAsync(async (req, res) => {
  let orderBody = {};
  const userId = req.user.id;
  const productId = req.params.id;
  const { orderStatus } = req.query;
  const adminDetail = await User.findOne({ _id: userId });
  const deliveryPersonDetail = await DeliveryPerson.findOne({ _id: req.body.deliveryPersonId });
  if (orderStatus === 'processing') {
    orderBody = {
      approveMessage: req.body.approveMessage,
      approvePersonName: adminDetail.fullName,
      approvePersonEmail: adminDetail.email,
      approvedBy: userId,
      approveAt: new Date()
    };
  }
  if (orderStatus === 'on the way') {
    orderBody = {
      deliveryPersonId: req.body.deliveryPersonId,
      deliveryPersonName: deliveryPersonDetail.fullName,
      deliveryPersonEmail: deliveryPersonDetail.email,
      deliveryPersonMobile: deliveryPersonDetail.mobile,
      companyId: deliveryPersonDetail.companyId,
      companyName: deliveryPersonDetail.companyName,
      assignBy: userId,
      assignAt: new Date()
    };
  }
  if (orderStatus === 'delivered') {
    orderBody = 'delivered';
  }
  if (orderStatus === 'cancelled') {
    orderBody = {
      cancelMessage: req.body.cancelMessage,
      cancelledBy: userId,
      cancelledAt: new Date()
    };
  }
  const value = await buyNowService.changeOrderStatus(productId, orderStatus, orderBody);
  res.send(value);
});
module.exports = {
  buyNow,
  getCustomerBuyNow,
  deleteBuyNowByCustomer,
  deleteBuyNowByAdmin,
  getBuyNowDetail,
  getBuyNowDetails,
  deleteBuyNow,
  getCustomerBuyNowById,
  changeOrderStatus
};
