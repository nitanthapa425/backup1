/**
 * Copyright (C) Two Pangra
 */

/**
 * the pagination
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

/**
 * Validate the search query params
 * @param searchString
 * @returns {any}
 */
const validateSearch = (searchString) => JSON.parse(`{${searchString}}`);

/**
 * Convert plain search object to regex object to implement text search in mongo
 * @param searchObject
 */
const convertSearchToRegex = (searchObject, noCheckRegex = []) => {
  const regexObject = {};
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const key of Object.keys(searchObject)) {
    if (noCheckRegex.includes(key)) {
      regexObject[key] = searchObject[key];
    } else {
      const splitWork = searchObject[key].split(' ');
      const arrWords = splitWork.map((n) => [`(?=.*${n})`]);
      regexObject[key] = new RegExp(`^${arrWords.join('')}.*$`, 'im');
      // regexObject[key] = new RegExp(searchObject[key], 'i');
    }
  }

  return regexObject;
};

/**
 * Middleware to Convert Pagination and Search Params
 */

const paginate = async (req, res, next) => {
  // eslint-disable-next-line object-curly-newline
  let { limit, page, search, sortBy, sortOrder, select, noRegex, type } = req.query;
  limit = limit || 1000; // Default Page Size 1000
  page = page || 1; // Default Page
  search = search || '';
  sortBy = sortBy || '';
  sortOrder = sortOrder || 1;
  select = select || '';
  select = select.replace(/,/g, ' ');
  noRegex = noRegex ? noRegex.split(',') : [];
  type = type || '';
  try {
    if (type === 'accessoriesDraft') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      const searchRange = [];

      if (typeof search.createdAt !== 'undefined' && search.createdAt !== '') {
        searchRange.push({
          createdAt: {
            $gte: new Date(`${search.createdAt}T00:00:00.000Z`),
            $lte: new Date(`${search.createdAt}T23:59:59.999Z`)
          }
        });

        delete search.createdAt;
      }

      search.$and = searchRange;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'accessoriesSell') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      const searchRange = [];

      if (typeof search.costPrice !== 'undefined' && search.costPrice !== '') {
        const splitPrice = search.costPrice.split('-');

        if (splitPrice.length > 1) {
          searchRange.push({
            costPrice: {
              $gte: splitPrice[0],
              $lte: splitPrice[1]
            }
          });

          delete search.costPrice;
        }
      }

      if (typeof search.brandName !== 'undefined' && search.brandName !== '') {
        const splitBrandName = search.brandName.split(',');

        if (splitBrandName.length > 1) {
          searchRange.push({
            brandName: {
              $in: splitBrandName
            }
          });

          delete search.brandName;
        }
      }

      if (typeof search.categoryName !== 'undefined' && search.categoryName !== '') {
        const splitCategoryName = search.categoryName.split(',');

        if (splitCategoryName.length > 1) {
          searchRange.push({
            categoryName: {
              $in: splitCategoryName
            }
          });

          delete search.categoryName;
        }
      }

      if (typeof search.subCategoryName !== 'undefined' && search.subCategoryName !== '') {
        const splitSubCategoryName = search.subCategoryName.split(',');

        if (splitSubCategoryName.length > 1) {
          searchRange.push({
            subCategoryName: {
              $in: splitSubCategoryName
            }
          });

          delete search.subCategoryName;
        }
      }

      if (typeof search.district !== 'undefined' && search.district !== '') {
        const splitDistrict = search.district.split(',');
        if (splitDistrict.length > 0) {
          searchRange.push({
            'location.district': {
              $in: splitDistrict
            }
          });

          delete search.district;
        }
      }

      if (typeof search.createdAt !== 'undefined' && search.createdAt !== '') {
        let makeYearSplit = [];

        const postDateSplit = search.createdAt.split('T');
        makeYearSplit = postDateSplit[0].split('-');

        if (makeYearSplit.length > 1) {
          searchRange.push({
            createdAt: {
              $gte: `${makeYearSplit[0]}-01-01T00:00:00.000Z`,
              $lte: `${makeYearSplit[0]}-12-30T23:59:59.999Z`
            }
          });

          delete search.createdAt;
        }
      }

      search.$and = searchRange;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'vehicleSellTable') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      const searchRange = [];

      searchRange.push({
        status: true
      });

      if (typeof search.usedFor !== 'undefined' && search.usedFor !== '') {
        const splitUserFor = search.usedFor.split(',');

        if (splitUserFor.length > 1) {
          searchRange.push({
            usedFor: {
              $in: splitUserFor
            }
          });

          delete search.usedFor;
        }
      }

      search.$and = searchRange;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'userDob') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      let dobSplit = [];
      let searchFinal = '';
      const searchData = [];
      if (typeof search.dob !== 'undefined' && search.dob !== '' && search.dob !== {}) {
        const dobDateSplit = search.dob.split('T');

        dobSplit = dobDateSplit[0].split('-');
        const dates = new Date(dobDateSplit[0]);

        searchFinal = dates.toLocaleDateString('fr-CA');
      }

      if (dobSplit.length > 1) {
        searchData.push({
          dob: {
            $gte: `${searchFinal}T00:00:00.000Z`,
            $lte: `${searchFinal}T23:59:59.999Z`
          }
        });

        delete search.dob;
      }

      if (typeof search.accessLevel !== 'undefined' && search.accessLevel !== '') {
        const splitAccessLevel = search.accessLevel.split(',');

        if (splitAccessLevel.length > 1) {
          searchData.push({
            accessLevel: {
              $in: splitAccessLevel
            }
          });

          delete search.accessLevel;
        }
      }

      if (typeof search.gender !== 'undefined' && search.gender !== '') {
        const splitGender = search.gender.split(',');

        if (splitGender.length > 1) {
          searchData.push({
            gender: {
              $in: splitGender
            }
          });

          delete search.gender;
        }
      }

      search.$and = searchData;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'clientDob') {
      search = convertSearchToRegex(validateSearch(search), noRegex);
      let dobSplit = [];
      let searchFinal = '';
      const searchData = [];

      if (typeof search.dob !== 'undefined' && search.dob !== '' && search.dob !== {}) {
        const dobDateSplit = search.dob.split('T');

        dobSplit = dobDateSplit[0].split('-');
        const dates = new Date(dobDateSplit[0]);

        searchFinal = dates.toLocaleDateString('fr-CA');
      }

      if (dobSplit.length > 1) {
        searchData.push({
          dob: {
            $gte: `${searchFinal}T00:00:00.000Z`,
            $lte: `${searchFinal}T23:59:59.999Z`
          }
        });

        delete search.dob;
      }

      if (typeof search.gender !== 'undefined' && search.gender !== '') {
        const splitGender = search.gender.split(',');

        if (splitGender.length > 1) {
          searchData.push({
            gender: {
              $in: splitGender
            }
          });

          delete search.gender;
        }
      }

      search.$and = searchData;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else {
      search = convertSearchToRegex(validateSearch(search), noRegex);
    }
  } catch (ignored) {
    return res.status(400).send({ message: 'Search parameter is mis configured', stack: ignored.message });
  }
  res.pagination = {
    options: {
      sort: { [sortBy]: parseInt(sortOrder, 10) },
      page,
      limit,
      select
    },
    search
  };
  // DELETE SORTING IF NO SORT KEY DEFINED
  if (!sortBy) {
    delete res.pagination.options.sort;
  }
  next();

  return res;
};

module.exports = {
  paginate
};
