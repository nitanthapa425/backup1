/**
 * Copyright (C) Two Pangra
 */

/**
 * the constant
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const ACCESS_LEVEL = {
  SUPER_ADMIN: 'SUPER_ADMIN',
  ADMIN: 'ADMIN',
  USER: 'USER',
  CLIENT: 'CLIENT'
};

const FUEL_TYPE = {
  PETROL: 'PETROL',
  ELECTRIC: 'ELECTRIC'
};

const VEHICLE_TYPE = {
  BIKE: 'BIKE',
  SCOOTER: 'SCOOTER'
};

const GENDER = {
  Male: 'Male',
  Female: 'Female',
  Other: 'Other'
};

// default data fetch limit
const DefaultQueryLimit = 10;

module.exports = {
  DefaultQueryLimit,
  ACCESS_LEVEL,
  FUEL_TYPE,
  VEHICLE_TYPE,
  GENDER
};
