/**
 * Copyright (C) Two Pangra Accessories
 */

/**
 * the shipping model
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ShippingLocationSchema = new Schema(
  {
    clientId: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    label: {
      type: String,
      enum: ['home', 'office', 'other']
    },
    country: {
      type: String,
      default: 'Nepal'
    },
    combineLocation: {
      type: String
    },
    exactLocation: {
      type: String
    },
    nearByLocation: {
      type: String
    },
    mobile: {
      type: String
    },
    fullName: {
      type: String
    },
    status: {
      type: String,
      default: 'active'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

ShippingLocationSchema.plugin(mongoosePaginate);
module.exports = {
  ShippingLocationSchema
};
