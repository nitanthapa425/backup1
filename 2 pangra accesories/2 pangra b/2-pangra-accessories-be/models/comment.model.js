const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const replySchema = new Schema({
  replyMessage: {
    type: String
  },
  fullName: {
    type: String
  },
  profileImagePath: {
    imageUrl: { type: String },
    publicKey: { type: String }
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  }
});

const commentSchema = new Schema({
  clientId: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  vehicleId: {
    type: Schema.Types.ObjectId,
    ref: 'SellerVehicle'
  },
  name: {
    type: String
  },
  message: {
    type: String
  },
  isApproved: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date
  },
  replyComment: [replySchema]
});

commentSchema.plugin(mongoosePaginate);
module.exports = {
  commentSchema
};
