/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const tagSchema = new Schema(
  {
    tagName: {
      type: String
    },
    status: {
      type: String,
      default: 'active'
    },
    createdBy: {
      type: String
    }
  },

  {
    timestamps: true
  }
);

tagSchema.pre('save', function (next) {
  //  fist letter capital
  this.tagName = this.tagName.charAt(0).toUpperCase() + this.tagName.substr(1);
  next();
});

tagSchema.plugin(mongoosePaginate);
module.exports = {
  tagSchema
};
