/**
 * Copyright (C) Two Pangra
 */

/**
 * the model entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const config = require('config');
const db = require('../datasource').getDb(config.db.url, config.db.poolSize);
const { UserSchema } = require('./user.model');
const { brandSchema, categorySchema, subCategorySchema } = require('./brandAccessories.model');
const { accessoriesItemSchema } = require('./accessoriesItem.model');
const { draftAccessoriesItemSchema } = require('./draftAccessoriesItem.model');
const { ImageSchema } = require('./image.model');
const { ClientSchema } = require('./client.model');
const { deliveryPersonSchema } = require('./delivery.model');
const { LogSchema } = require('./logSchema.model');
const { cartSchema } = require('./addToCart.model');
const { buyNowSchema } = require('./buyNow.model');
const { meetingSchedule } = require('./meetingSchedule.model');
const { municipalitySchema, toleSchema } = require('./geoLocation.model');
const { logManagementSchema } = require('./logManagement.model');
const { notificationSchema } = require('./notification.model');
const { emailSubscribe } = require('./emailSubscribe.model');
const { postExpiryDateSchema } = require('./postExpiryDate.model');
const { commentSchema } = require('./comment.model');
const { offerSchema } = require('./offer.model');
// const { addToCartSchema } = require('./addToCart.model');
const { ShippingLocationSchema } = require('./shippingAddress.model');
const { ratingAndReviewSchema } = require('./reviewAndRating.model');
const { companySchema } = require('./company.model');
const { tagSchema } = require('./accessoriesTag.model');

module.exports = {
  db,
  User: db.model('User', UserSchema),
  Client: db.model('Client', ClientSchema),
  DeliveryPerson: db.model('DeliveryPerson', deliveryPersonSchema),
  BrandAccessories: db.model('BrandAccessories', brandSchema),
  Category: db.model('Category', categorySchema),
  SubCategory: db.model('SubCategory', subCategorySchema),
  Accessories: db.model('Accessories', accessoriesItemSchema),
  draftAccessories: db.model('draftAccessories', draftAccessoriesItemSchema),
  CartModel: db.model('CartModel', cartSchema),
  EmailSubscribe: db.model('EmailSubscribe', emailSubscribe),
  PostExpiryDateModel: db.model('PostExpiryDateModel', postExpiryDateSchema),
  Comment: db.model('Comment', commentSchema),
  BuyNow: db.model('BuyNow', buyNowSchema),
  MeetingSchedule: db.model('MeetingSchedule', meetingSchedule),
  Image: db.model('Image', ImageSchema),
  LogSchemaModel: db.model('LogSchemaModel', LogSchema),
  GeoLocationSchemaModel: db.model('GeoLocationSchemaModel', municipalitySchema),
  fullLocationModel: db.model('fullLocationModel', toleSchema),
  LogManagementModel: db.model('LogManagementModel', logManagementSchema),
  NotificationModel: db.model('NotificationModel', notificationSchema),
  OfferModel: db.model('OfferModel', offerSchema),
  RatingAndReview: db.model('RatingAndReview', ratingAndReviewSchema),
  // AddToCart: db.model('AddToCart', addToCartSchema),
  ShippingLocation: db.model('ShippingLocation', ShippingLocationSchema),
  Company: db.model('Company', companySchema),
  Tag: db.model('Tag', tagSchema),
};
