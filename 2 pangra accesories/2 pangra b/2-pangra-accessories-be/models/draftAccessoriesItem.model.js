/**
 * Copyright (C) Dui Pangra Accessories
 */

/**
 * Draft Accessories Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

// const { templateSettings } = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
// const { validateMaxTenNum, validateFloatNumLess100 } = require('../utils/helper');

// const LocationSchema = new Schema(
//   {
//     country: {
//       type: String,
//       default: 'Nepal',
//       required: [true, 'Country is required']
//     },
//     combineLocation: {
//       type: String
//     },
//     nearByLocation: {
//       type: String
//     },
//     status: {
//       type: String,
//       enum: ['Location']
//     },
//     geoLocation: {
//       type: {
//         type: String, // Don't do `{ location: { type: String } }`
//         enum: ['Point'] // 'location.type' must be 'Point'
//       },
//       coordinates: {
//         type: [Number]
//       }
//     }
//   },
//   { id: false }
// );

// const productImageSchema = new Schema(
//   {
//     imageUrl: {
//       type: String,
//       required: [true, 'At least a image is required']
//     },
//     publicKey: {
//       type: String,
//       required: [true, 'Public key is required']
//     }
//   },
//   { _id: false }
// );

const draftAccessoriesItemSchema = new Schema(
  {
    brandId: {
      type: Schema.Types.ObjectId,
      ref: 'Brand',
      required: true
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: true
    },
    subCategoryId: {
      type: Schema.Types.ObjectId,
      ref: 'SubCategory',
      required: true
    },
    productTitle: {
      type: String
    },
    description: {
      type: String
    },
    color: [
      {
        type: String
      }
    ],
    costPrice: {
      type: String
      // required: true
    },
    // Discount Percent => string (automatically generate from calculation)
    discountPrice: {
      type: String
    },
    discountPercentage: {
      type: String
    },
    weight: {
      type: String // save weight in gram
    },
    dimension: {
      length: {
        type: String
      },
      breadth: {
        type: String
      },
      height: {
        type: String
      }
    },
    dimensionUnit: {
      type: String,
      enum: ['mm', 'cm', 'm']
    },
    sizeCalculation: {
      type: String,
      enum: ['Numbers', 'Letters']
    },
    sizeValue: [
      {
        type: String
      }
    ],
    SKU: {
      type: String
    },
    quantity: {
      type: String
    },
    hasWarranty: {
      type: Boolean,
      default: false
    },
    // If warranty is true than only validate warranty
    warrantyTime: {
      type: String
    },
    isReturnable: {
      type: Boolean,
      default: false
    },
    // If return is true than validate
    returnableTime: {
      type: String
    },
    /* This should be made creatable */
    tags: [
      {
        type: String
      }
    ],
    tagLine: {
      type: String
    },
    productImage: {
      imageUrl: {
        type: String,
      },
      publicKey: {
        type: String,
      }
    },
    productImages: [
      {
        imageUrl: {
          type: String,
        },
        publicKey: {
          type: String,
        }
      }
    ],
    hasInFeatured: {
      type: Boolean,
      default: false
    },
    hasInHotDeal: {
      type: Boolean,
      default: false
    },
    isForFlashSale: {
      type: Boolean,
      default: false
    },
    outOfStock: {
      type: Boolean,
      default: false
    },
    brandName: {
      type: String
    },
    categoryName: {
      type: String
    },
    subCategoryName: {
      type: String
    },
    status: {
      type: Boolean,
      default: true
    },
    numViews: {
      type: Number
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    deleteBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    wishList: [
      {
        userId: {
          type: Schema.Types.ObjectId,
          ref: 'Client'
        },
        fullName: {
          type: String
        }
      }
    ],
    updatedAt: {
      type: Date
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

// /* If quantity is zero change out of stuck to false  */
// accessoriesItemSchema.pre('save', function (next) {
//   if (this.quantity === '0') {
//     this.outOfStock = false;
//   }
//   next();
// });

// /* Make first letter to uppercase of front brake system */
// VehicleSellSchema.pre('save', function (next) {
//   this.color = this.color.trim()[0].toUpperCase() + this.color.slice(1).toLowerCase();
//   next();
// });

draftAccessoriesItemSchema.plugin(mongoosePaginate);
module.exports = {
  draftAccessoriesItemSchema
};
