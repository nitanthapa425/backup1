/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
// const uniqueValidator = require('mongoose-beautiful-unique-validation');
const { Schema } = require('mongoose');

// const VaraiantNameValueSchema = new Schema({
//   variantName: {
//     type: String,
//     minlength: [2, 'Please, fill the variant name with maximum 2 characters only.'],
//     maxlength: [150, 'Please, fill the variant name with maximum 150 characters only.']
//   }
// });

const subCategorySchema = new Schema({
  subCategoryName: {
    type: String,
    // unique: true,
    required: [true, 'A variant must have a name.']
  },
  subCategoryDescription: {
    type: String
  },
  categoryId: {
    type: Schema.Types.ObjectId,
    ref: 'Category',
    required: true
  },
  categoryName: {
    type: String,
  },
  brandName: {
    type: String
  },
  status: {
    type: String,
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

// Apply the uniqueValidator plugin to VarientSchema.
// VarientSchema.plugin(uniqueValidator);

const uploadBrandImageSchema = new Schema(
  {
    imageUrl: {
      type: String
    },
    publicKey: {
      type: String
    }
  },
  { _id: false }
);

// const companyImageSchema = new Schema(
//   {
//     imageUrl: {
//       type: String
//     },
//     publicKey: {
//       type: String
//     }
//   },
//   { _id: false }
// );

const brandSchema = new Schema(
  {
    brandName: {
      type: String,
      unique: true,
      required: [true, 'A brand vehicle must have a name']
    },
    brandDescription: {
      type: String
    },
    uploadBrandImage: uploadBrandImageSchema,

    hasInHomePage: {
      type: Boolean,
      default: false
    },
    status: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

brandSchema.statics.isUserNameTaken = async function (brandName, excludeUserId) {
  const brand = await this.findOne({ brandName, _id: { $ne: excludeUserId } });
  return !!brand;
};
// Apply the uniqueValidator plugin to BrandVehicleSchema.
// BrandVehicleSchema.plugin(uniqueValidator);

const categorySchema = new Schema({
  categoryName: {
    type: String,
    unique: true,
    required: [true, 'A brand model must have a name.']
  },
  categoryDescription: {
    type: String
  },
  status: {
    type: String
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

brandSchema.plugin(mongoosePaginate);
categorySchema.plugin(mongoosePaginate);
subCategorySchema.plugin(mongoosePaginate);

module.exports = {
  brandSchema,
  categorySchema,
  subCategorySchema,
};
