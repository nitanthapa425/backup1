/**
 * Copyright (C) Two Pangra
 */

/**
 * the user  model
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ProfileImageSchema = new Schema(
  {
    imageUrl: { type: String },
    publicKey: { type: String }
  },
  { _id: false }
);

const LocationSchema = new Schema({
  country: {
    type: String,
    default: 'Nepal'
  },
  combineLocation: {
    type: String
  },
  exactLocation: {
    type: String
  },
  nearByLocation: {
    type: String
  },
  status: {
    type: String,
    default: 'active'
  }
});

const deliveryPersonSchema = new Schema(
  {
    userName: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
      trim: true
    },
    firstName: { type: String, required: true },
    middleName: { type: String },
    lastName: { type: String, required: true },
    fullName: { type: String },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: true,
      trim: true
    },
    passwordHash: { type: String, private: true },
    profileImagePath: ProfileImageSchema,
    mobile: { type: String },
    accessLevel: { type: String, enum: 'DELIVERY' },
    gender: { type: String, enum: ['Male', 'Female', 'Other'] },
    dob: { type: Date },
    isActive: { type: Boolean, default: true },
    verified: { type: Boolean, default: false },
    receiveNewsletter: { type: Boolean },
    forgotPasswordToken: { type: String },
    lastLoginAt: { type: Date },
    verificationToken: { type: String },
    accessToken: { type: String },
    location: LocationSchema,
    companyId: {
      type: Schema.Types.ObjectId,
      ref: 'Company'
    },
    companyName: {
      type: String,
    },
    documentImagePath: [
      {
        imageUrl: { type: String, required: true },
        publicKey: { type: String, required: true }
      }
    ],
    deletedBy: String,
    deletedAt: Date
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;

        // Keep only necessary details for GET requests
        delete ret.passwordHash;
        delete ret.verificationToken;
        delete ret.forgotPasswordToken;
        delete ret.accessToken;
        return ret;
      }
    }
  }
);

deliveryPersonSchema.plugin(mongoosePaginate);
module.exports = {
  deliveryPersonSchema
};
