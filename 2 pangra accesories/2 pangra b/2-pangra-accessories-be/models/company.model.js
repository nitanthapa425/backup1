const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const companySchema = Schema(
  {
    companyName: {
      type: String,
    },
    fullName: {
      type: String,
    },
    mobile: {
      type: String,
    },
    email: {
      type: String,
      lowercase: true
    }
  },
  { timestamps: true }
);

companySchema.plugin(mongoosePaginate);

module.exports = {
  companySchema
};
