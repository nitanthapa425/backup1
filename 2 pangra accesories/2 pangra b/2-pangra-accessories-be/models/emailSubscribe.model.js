/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { lowerCase } = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const emailSubscribe = new Schema({
  email: {
    type: String,
    trim: lowerCase,
  }
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

emailSubscribe.plugin(mongoosePaginate);
module.exports = {
  emailSubscribe
};
