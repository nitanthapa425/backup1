/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const postExpiryDateSchema = new Schema({
  email: {
    type: String,
    required: true,
    lowercase: true,
    trim: true
  },
  sellerName: { type: String },
  sellerVehicleId: { type: String },
  adminEmail: { type: String },
  postExpiryDate: { type: Date },
  status: {
    type: Boolean,
    default: true
  },
},
{
  timestamps: true,
  toJSON: {
    transform(doc, ret) {
      if (ret._id) {
        ret.id = String(ret._id);
        delete ret._id;
      }
      delete ret.__v;
      return ret;
    }
  }
});

postExpiryDateSchema.plugin(mongoosePaginate);
module.exports = {
  postExpiryDateSchema
};
