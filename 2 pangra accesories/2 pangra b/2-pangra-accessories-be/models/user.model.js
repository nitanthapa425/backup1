/**
 * Copyright (C) Two Pangra
 */

/**
 * the user  model
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

const _ = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { ACCESS_LEVEL } = require('../constants');

const ProfileImageSchema = new Schema(
  {
    imageUrl: { type: String },
    publicKey: { type: String }
  },
  { _id: false }
);

const LocationSchema = new Schema({
  country: {
    type: String,
    default: 'Nepal'
  },
  combineLocation: {
    type: String
  },
  exactLocation: {
    type: String
  },
  nearByLocation: {
    type: String
  },
  status: {
    type: String,
    default: 'active'
  }
});

const UserSchema = new Schema(
  {
    userName: {
      type: String,
      required: true,
      lowercase: true,
      unique: true,
      trim: true
    },
    firstName: { type: String, required: true },
    middleName: { type: String },
    lastName: { type: String, required: true },
    fullName: { type: String },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      required: true,
      trim: true
    },
    passwordHash: { type: String, private: true },
    profileImagePath: ProfileImageSchema,
    mobile: { type: String },
    accessLevel: { type: String, enum: _.values(ACCESS_LEVEL) },
    gender: { type: String, enum: ['Male', 'Female', 'Other'] },
    dob: { type: Date },
    isActive: { type: Boolean, default: true },
    verified: { type: Boolean, default: false },
    receiveNewsletter: { type: Boolean },
    forgotPasswordToken: { type: String },
    lastLoginAt: { type: Date },
    verificationToken: { type: String },
    accessToken: { type: String },
    location: LocationSchema,
    deletedBy: String,
    deletedAt: Date
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;

        // Keep only necessary details for GET requests
        delete ret.passwordHash;
        delete ret.verificationToken;
        delete ret.forgotPasswordToken;
        delete ret.accessToken;
        return ret;
      }
    }
  }
);

/* Make first letter to uppercase of user first name */
UserSchema.pre('save', function (next) {
  //  fist letter capital
  this.firstName = this.firstName.charAt(0).toUpperCase() + this.firstName.substr(1);
  next();
});

/* Make first letter to uppercase of user middle name */
UserSchema.pre('save', function (next) {
  //  fist letter capital
  this.middleName = this.middleName.charAt(0).toUpperCase() + this.middleName.substr(1);
  next();
});
/* Make first letter to uppercase of user last name */
UserSchema.pre('save', function (next) {
  //  fist letter capital
  this.lastName = this.lastName.charAt(0).toUpperCase() + this.lastName.substr(1);
  next();
});

/* Make first letter to uppercase of user last name */
UserSchema.pre('save', function (next) {
  //  fist letter capital
  this.fullName = this.fullName.charAt(0).toUpperCase() + this.fullName.slice(1);
  next();
});

UserSchema.plugin(mongoosePaginate);
module.exports = {
  UserSchema
};
