/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */
const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const buyNowSchema = new Schema(
  {
    accessoriesId: {
      type: Schema.Types.ObjectId,
      ref: 'Accessories',
      required: true
    },
    productName: {
      type: String
    },
    customerName: {
      type: String
    },
    customerMobile: {
      type: String
    },
    productImage: [
      {
        imageUrl: {
          type: String,
          required: [true, 'At least a image is required']
        },
        publicKey: {
          type: String,
          required: [true, 'Public key is required']
        }
      },
      { id: false }
    ],
    price: {
      type: String
    },
    color: {
      type: String
    },
    size: {
      type: String
    },
    quantity: {
      type: String
    },
    totalPrice: {
      type: String
    },
    shippingAddress: {
      type: Schema.Types.ObjectId,
      ref: 'ShippingLocation'
    },
    shippingAddressLocation: {
      combineLocation: {
        type: String
      },
      exactLocation: {
        type: String
      },
      nearByLocation: {
        type: String
      },
      mobile: {
        type: String
      }
    },
    shippingFee: {
      type: String,
      default: '50'
    },
    deliveryStart: {
      type: Date
    },
    deliveryEnd: {
      type: Date
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    numViews: {
      type: Number
    },
    status: {
      type: Boolean
    },
    deliveryStatus: {
      type: String,
      enum: ['ordered', 'processing', 'on the way', 'delivery problem', 'delivered', 'cancelled']
    },
    approveMessages: [
      {
        approveMessage: {
          type: String
        },
        approvedBy: {
          type: Schema.Types.ObjectId,
          ref: 'User'
        },
        approvePersonName: {
          type: String
        },
        approvePersonEmail: {
          type: String
        },
        approveAt: {
          type: Date
        }
      }
    ],
    cancelMessages: {
      cancelMessage: {
        type: String
      },
      cancelledBy: {
        type: Object
      },
      cancelName: {
        type: String
      },
      cancelEmail: {
        type: String
      },
      cancelledAt: {
        type: Date
      }
    },
    onTheWayDetail: {
      deliveryPersonId: {
        type: Schema.Types.ObjectId,
        ref: 'Delivery'
      },
      deliveryPersonName: {
        type: String
      },
      companyId: {
        type: Schema.Types.ObjectId,
        ref: 'Company'
      },
      companyName: {
        type: String
      },
      deliveryPersonEmail: {
        type: String
      },
      deliveryPersonMobile: {
        type: String
      },
      assignBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      assignAt: {
        type: Date
      }
    },
    deliveredDetail: {
      deliveryPersonId: {
        type: Schema.Types.ObjectId,
        ref: 'Delivery'
      },
      assignBy: {
        type: Schema.Types.ObjectId,
        ref: 'User'
      },
      deliveryPersonName: {
        type: String
      },
      deliveryPersonEmail: {
        type: String
      },
      deliveryPersonMobile: {
        type: String
      },
      assignAt: {
        type: Date
      }
    },
    deliveryCancelDetail: {
      cancelMessage: {
        type: String
      },
      cancelledBy: {
        type: Object
      },
      cancelName: {
        type: String
      },
      cancelEmail: {
        type: String
      },
      cancelledAt: {
        type: Date
      }
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

buyNowSchema.plugin(mongoosePaginate);
module.exports = {
  buyNowSchema
};
