const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ratingAndReviewSchema = new Schema({
  accessoriesId: {
    type: Schema.Types.ObjectId,
    ref: 'Accessories',
  },
  ratingNumber: {
    type: String,
    enum: ['0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5'],
  },
  ratingComment: {
    type: String,
  },
  status: {
    type: Boolean,
    default: false
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  },
  fullName: {
    type: String
  },
  productTitle: {
    type: String
  }
});
ratingAndReviewSchema.plugin(mongoosePaginate);

module.exports = {
  ratingAndReviewSchema
};
