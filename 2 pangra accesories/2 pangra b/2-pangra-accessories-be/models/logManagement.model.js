/**
 * Copyright (C) Tuki Logic
 */

/**
 * the log management schema
 * @author      SUSAN DHAKAL
 * @version     1.0
 */

const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const logManagementSchema = new Schema(
  {
    logType: {
      type: String,
      enum: [
        'Add Data',
        'Update Data',
        'Delete Data',
        'Buy Data',
        'Remove Data',
        'View Data',
      ],
      required: true
    },
    category: {
      type: String,
      enum: [
        'Users',
        'Vehicle Details',
        'Brands',
        'Models',
        'Variants',
        'Carts',
        'View Cart',
        'Add To Cart',
        'Cart Delete',
        'Add Wishlist',
        'Delete Wishlist',
        'View Wishlist',
        'Rating',
        'Buy Now',
        'Add Draft',
        'Update Draft',
        'Draft Data',
        'Add Accessories',
        'Update Accessories',
        'Add Accessories in Hot Deals',
        'Remove Accessories',
        'Delete Draft',
        'Get Accessories',
        'Delete Accessories',
        'Remove Accessories In Hot Deals',
        'Remove Accessories From Feature',
        'Update Rating',
        'Add Company Detail',
        'View Company Detail',
        'Update Company Detail',
        'Delete Company Detail',
        'Add Accessories Tag',
        'View Accessories Tag',
        'revert Deleted User',
        'Delete User Profile',
        'Update User Profile',
        'View User Profile',
        'View Deleted User Profile List',
        'View User Profile List',
        'View All Province',
        'View All District By Province',
        'Create Muncipality',
        'create FullLocation',
        'update FullLocation',
        'View Muncipality District',
        'View FullLocation',
        'View Municipality',
        'Update Municipality',
        'Delete FullLocation',
        'Login User',
        'SignUp User',
        'Confirm Email',
        'Forget Password',
        'Reset Password',
        'Change Password',
        'Update Profile',
        'Confirm Mobile',
        'Add Accessories in feature'
      ],
      required: true
    },
    logDescription: {
      type: String,
      required: true
    },
    status: {
      type: String,
      default: 'active'
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
      required: true
    },
    createdByName: {
      type: String
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

logManagementSchema.plugin(mongoosePaginate);

module.exports = {
  logManagementSchema
};
