const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/container/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: "1rem",

      screens: {
        xl: "1250px",
      },
    },
    extend: {
      fontFamily: {
        sans: ["'Nunito'", ...defaultTheme.fontFamily.serif],
      },
      colors: {
        primary: {
          light: "#A7EB4A",
          DEFAULT: "#8DC63F",
          dark: "#75A535",
        },

        secondary: {
          light: "#ee9433",
          DEFAULT: "#d49653",
          dark: "#ca7314",
        },

        error: {
          DEFAULT: "#ec1c24",
          dark: "#a01319",
        },

        info: {
          light: "#0096c7",
          DEFAULT: "#0077b6",
          dark: "#023e8a",
        },

        warning: {
          light: "#ecec5f",
          DEFAULT: "#c6c651",
          dark: "#ffff00",
        },

        deliveryFail: {
          light: "#F87171",
          DEFAULT: "#EF4444",
          dark: "#991B1B",
        },
        reviewPending: {
          light: "#FDBA74",
          DEFAULT: "#FB923C",
          dark: "#F97316",
        },

        textColor: "#383838",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
