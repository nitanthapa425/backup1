export const getCustomerToken = () => {
  return localStorage.getItem("customerTwoPangraToken");
};
export const setCustomerToken = (token) => {
  if (token) {
    localStorage.setItem("customerTwoPangraToken", token);
  }
};
export const getCustomer = () => {
  return localStorage.getItem("customerTwoPangra");
};
export const setCustomer = (customer) => {
  if (customer) {
    localStorage.setItem("customerTwoPangra", JSON.stringify(customer));
  }
};
export const removeCustomerToken = () => {
  localStorage.removeItem("customerTwoPangraToken");
  localStorage.removeItem("customerTwoPangra");
};
