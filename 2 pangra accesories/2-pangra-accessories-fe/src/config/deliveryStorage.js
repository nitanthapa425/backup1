export const getDeliveryToken = () => {
  return localStorage.getItem("deliveryTwoPangraToken");
};
export const setDeliveryToken = (token) => {
  if (token) {
    localStorage.setItem("deliveryTwoPangraToken", token);
  }
};
export const getDelivery = () => {
  return localStorage.getItem("deliveryTwoPangra");
};
export const setDelivery = (delivery) => {
  if (delivery) {
    localStorage.setItem("deliveryTwoPangra", JSON.stringify(delivery));
  }
};
export const removeDeliveryToken = () => {
  localStorage.removeItem("deliveryTwoPangraToken");
  localStorage.removeItem("deliveryTwoPangra");
};
