import { beforeAfterDate } from "utils/beforeAfterdate";
import { nNumberArray } from "utils/nNumberOfArray";

export const productQuantity = 5;

export const messengerId = {
  pageId: "112421938142897",
  appId: "1107779203135767",
};

export const role = {
  admin: "ADMIN",
  super_admin: "SUPER_ADMIN",
  user: "USER",
};

export const weightUnits = [
  {
    label: "gm",
    value: "gm",
  },
  {
    label: "kg",
    value: "kg",
  },
];

export const lengthUnits = [
  {
    label: "mm",
    value: "mm",
  },
  {
    label: "cm",
    value: "cm",
  },
  {
    label: "m",
    value: "m",
  },
];

export const sizeFormatUnits = [
  {
    label: "Numbers",
    value: "Numbers",
  },
  {
    label: "Letters",
    value: "Letters",
  },
  {
    label: "Litres",
    value: "Litres",
  },
  {
    label: "Free Size",
    value: "Free Size",
  },
];

export const sizesLetters = ["XS", "S", "M", "L", "1XL", "2XL", "3XL", "4XL"];

export const numbersArrGenerator = (start = 0, end = 10) => {
  const arr = [];
  for (let i = start; i <= end; i++) {
    arr.push(i);
  }
  return arr;
};

export const warrantyTime = [
  {
    label: "1",
    value: "1",
  },
  {
    label: "3",
    value: "3",
  },
  {
    label: "6",
    value: "6",
  },
  {
    label: "9",
    value: "9",
  },
  {
    label: "12",
    value: "12",
  },
];

export const returnableTime = [
  {
    label: "1",
    value: "1",
  },
  {
    label: "2",
    value: "2",
  },
  {
    label: "3",
    value: "3",
  },
];

export const tags = [
  {
    label: "Helmets",
    value: "Helmets",
  },
  {
    label: "Jackets",
    value: "Jackets",
  },
  {
    label: "Windcheaters",
    value: "Windcheaters",
  },
];

export const color = [
  "Beige",
  "Black",
  "Blue",
  "Brown",
  "Gold",
  "Green",
  "Grey",
  "Maroon",
  "Orange",
  "Purple",
  "Red",
  "Silver",
  "White",
  "Yellow",
  "Other",
];

export const usedFor = [
  "Not Available",
  "1 Month",
  "3 Months",
  "6 Months",
  "1 Year",
  "2 Years",
  "3 Years",
  "4 Years",
  "5 Years",
  "6 Years",
  "7 Years",
  "8 Years",
  "9 Years",
  "10 Years",
  "More than 10",
];

export const postExpiryDate = [
  {
    label: "1 Month",
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 2
      }-${new Date().getDate()}`
    ),
  },
  {
    label: "3 Months",
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 4
      }-${new Date().getDate()}`
    ),
  },
  {
    label: "6 Months",
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 7
      }-${new Date().getDate()}`
    ),
  },
  {
    label: "9 Months",
    value: new Date(
      `${new Date().getFullYear()}-${
        new Date().getMonth() + 10
      }-${new Date().getDate()}`
    ),
  },
  {
    label: "1 Year",
    value: new Date(
      `${new Date().getFullYear() + 1}-${
        new Date().getMonth() + 1
      }-${new Date().getDate()}`
    ),
  },
];

export const featureDate = [
  {
    label: "3 Days",
    value: beforeAfterDate(new Date(), 3, 0, 0),
  },
  {
    label: "7 Days",
    value: beforeAfterDate(new Date(), 7, 0, 0),
  },
  {
    label: "15 Days",
    value: beforeAfterDate(new Date(), 15, 0, 0),
  },
];

export const condition = [
  "Brand New",
  "Like New",
  "Excellent",
  "Good/Fair",
  "Not Working",
];

export const ownershipCount = nNumberArray(5);
export const budget = ["Below 1 Lakh ", "1 Lakh - 2 Lakh", "Above 2 Lakh"];
export const verified = [
  {
    label: "Verified",
    value: true,
  },
  {
    label: "Unverified",
    value: false,
  },
];
export const approved = [
  {
    label: "Approved",
    value: true,
  },
  {
    label: "Unapproved",
    value: false,
  },
];
export const sold = [
  {
    label: "Sold",
    value: true,
  },
  {
    label: "Unsold",
    value: false,
  },
];

export const genders = ["Male", "Female", "Other"];
export const accessLevels = ["ADMIN", "SUPER_ADMIN", "USER", "DELIVERY"];

export const addressLabel = ["Home", "Office", "Other"];

export const orderStatus = ["Ordered", "Processing", "On The Way", "Cancelled"];
export const orderStatusSuperAdmin = [
  "Ordered",
  "Processing",
  "On The Way",
  "Cancelled",
  "Delivered",
];

export const subjects = [
  "Location Not Found",
  "Product Color Not Found",
  "Product Size Not Found",
  "Need New Product",
  "Others",
];
