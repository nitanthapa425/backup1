// return n number of array
export const nNumberArray = (n) => {
  let array = [];

  for (let i = 1; i <= n; i++) {
    array = [...array, `${i}`];
  }

  return array;
};
