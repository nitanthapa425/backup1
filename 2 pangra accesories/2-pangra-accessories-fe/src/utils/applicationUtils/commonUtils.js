export const badgeColorFun = (value) => {
  const badgeVar =
    value === "Ordered"
      ? "badge-orange"
      : value === "Processing"
      ? "badge-blue"
      : value === "On The Way"
      ? "badge-yellow"
      : value === "Delivered"
      ? "badge-success"
      : value === "Cancelled"
      ? "badge-error"
      : value === "delivery problem"
      ? "delivery-problem"
      : "";

  return badgeVar;
};
