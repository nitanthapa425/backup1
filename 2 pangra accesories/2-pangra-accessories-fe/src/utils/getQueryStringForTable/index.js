export const getQueryStringForTable = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }
  return `?limit=${pageSize}&page=${pageIndex + 1}${sortString}${searchString}`;
};
export const getQueryStringForUserTable = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const type = "&type=userDob";
  const noRegexQuery = "&noRegex=accessLevel,verified,dob,gender";
  const requiredDataColumns =
    "&select=userName,fullName,email,mobile,companyName,dob,gender,verified,accessLevel";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }
  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${requiredDataColumns}${noRegexQuery}${type}`;
};

export const getQueryStringForCompanyTable = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const type = "&type=company";
  const noRegexQuery = "";
  const requiredDataColumns = "&select=companyName,fullName,mobile";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }
  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${requiredDataColumns}${noRegexQuery}${type}`;
};

export const getQueryStringForCustomerTable = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const type = "&type=clientDob";

  const noRegexQuery =
    "&noRegex=emailVerified,phoneVerified,dob,gender,receiveNewsletter,status";
  const requiredDataColumns =
    "&select=fullName,email,mobile,dob,gender,emailVerified,phoneVerified,receiveNewsletter,status";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${requiredDataColumns}${noRegexQuery}${type}`;
};

export const getQueryStringForSellingVehicle = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery =
    "&noRegex=hasInHotDeal,hasInFeatured,createdAt,status,costPrice,adminPrice,isForFlashSale";
  const type = "&type=vehicleSellTable";
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";

  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  // console.log(searchString);

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};
export const getQueryStringForOfferList = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery = "&noRegex=createdAt,status";
  const type = "&type=offerListTable";
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";

  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  // console.log(searchString);

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};

export const getQueryStringForHotDeals = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery =
    "&noRegex=startDate,endDate,hasInFeatured,isReturnable,status,createdAt,costPrice";
  const type = "&type=vehicleSellTable";
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";

  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  // console.log(searchString);

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};

export const getQueryStringFlashSell = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery = "&noRegex=costPrice,startDate,endDate";
  const type = "&type=flashSell";
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";

  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  // console.log(searchString);

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};

export const getQueryStringForReview = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery = "&noRegex=,status";
  const type = "&type=accessoriesSell";
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";

  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  // console.log(searchString);

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};
export const getQueryStringForVehicle = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery =
    "&noRegex=status,color,verified,brandName,district,hasInFeatured,costPrice,isForFlashSale,categoryName,subCategoryName";

  const type = "&type=accessoriesSell";
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}${type}`;
};

export const getQueryStringForWishList = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery = "&noRegex=isVerified,bikeDriven,numViews,price";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}`;
};

export const getQueryStringForCart = (pageIndex, pageSize, sortBy, filters) => {
  const noRegexQuery =
    "&noRegex=isVerified,bikeDriven,numViews,costPrice,discountedPrice,color";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}&type=cart`;
};

export const getQueryStringForBook = (pageIndex, pageSize, sortBy, filters) => {
  const noRegexQuery =
    "&noRegex=deliveryStatus,createdAt,combineLocation,totalPrice";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}`;
};

export const getQueryStringForInventory = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery =
    "&noRegex=color,size,quantity,discountPercentage,discountedAmount";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}`;
};

export const getQueryStringForLog = (pageIndex, pageSize, sortBy, filters) => {
  const noRegexQuery = "&noRegex=createdBy";
  // const type = '&type=vehicleSellTable';
  // const requiredDataColumns =
  //   '&select=vehicleName,bikeDriven,expectedPrice,bikeNumber,ownershipCount,color,condition,postExpiryDate,makeYear,usedFor,createdBy';
  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }

  //  &search="vehicleName":"vehicle","bikeDriven":"2000"

  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${noRegexQuery}`;
};

export const getQueryStringForDeliveryTable = (
  pageIndex,
  pageSize,
  sortBy,
  filters
) => {
  const noRegexQuery = "";
  const requiredDataColumns = "&select=userName,fullName,email,mobile,location";

  let sortString = "";
  if (sortBy.length) {
    sortString = "&sortBy=" + sortBy[0].id;
    sortString += sortBy[0].desc ? "&sortOrder=-1" : "&sortOrder=1";
  } else {
    sortString = "&sortBy=createdAt&sortOrder=-1";
  }

  let searchString = "";
  if (filters && filters.length) {
    searchString =
      "&search=" +
      filters
        .map((v) => {
          return `"${v.id}":"${v.value}"`;
        })
        .join(",");
  }
  return `?limit=${pageSize}&page=${
    pageIndex + 1
  }${sortString}${searchString}${requiredDataColumns}${noRegexQuery}`;
};
