function reverseString(str) {
  return str.split('').reverse().join('');
}

export function thousandNumberSeparator(num) {
  const emptyStr = '';

  // delete extra comma by regex replace.
  const trimComma = (str) => str.replace(/^[,]+|[,]+$/g, emptyStr);

  const str = num + emptyStr;
  const [integer, decimal] = str.split('.');

  const conversed = reverseString(integer);

  const grouped = trimComma(
    reverseString(conversed.replace(/\d{3}/g, (match) => `${match},`))
  );

  if (!num) {
    return num;
  }

  return !decimal ? grouped : `${grouped}.${decimal}`;
}

// console.log(thousandNumberSeparator(1234567890.1234567)); // 1,234,567,890.1234
// console.log(thousandNumberSeparator(1234567890)); // 1,234,567,890.1234
