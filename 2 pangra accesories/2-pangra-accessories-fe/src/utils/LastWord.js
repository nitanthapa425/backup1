export const CityLastWord = (value) => {
  const lastWord = value?.substring(value?.lastIndexOf(' ') + 1);
  return lastWord;
};
