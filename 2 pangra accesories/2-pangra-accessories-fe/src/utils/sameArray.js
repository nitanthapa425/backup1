const containsAll = (arr1, arr2) => {
  if (arr1.length === 0 || arr2.length === 0) {
    return false;
  } else {
    return arr2.every((arr2Item) => arr1.includes(arr2Item));
  }
};

export const sameArrayIrrespectiveToPosition = (arr1, arr2) =>
  containsAll(arr1, arr2) && containsAll(arr2, arr1);
