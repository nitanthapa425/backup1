export const sumOfEachElementOfArray = (array = []) => {
  const total = array?.reduce((cur, pre) => cur + pre, 0);
  return total;
};
