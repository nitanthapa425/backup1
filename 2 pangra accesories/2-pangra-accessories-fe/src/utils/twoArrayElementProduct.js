export const twoArrayElementProduct = (ar1 = [], ar2 = []) => {
  let productArr = [];

  ar1.forEach((v1, i) => {
    ar2.forEach((v2, j) => {
      if (i === j) {
        productArr = [...productArr, v1 * v2];
      }
    });
  });

  return productArr;
};
