const moment = require('moment');

function getWeeks(days) {
  return {
    weeks: Math.floor(days / 7),
    days: days,
  };
}

export const agodate = (date) => {
  const starts = moment(date);
  const ends = moment();

  const duration = moment.duration(ends.diff(starts));

  // return(duration._data);
  if (duration._data.years >= 1) {
    if (duration._data.years === 1) {
      return duration._data.years + ' year ago';
    } else return duration._data.years + ' years ago';
  } else if (duration._data.months >= 1) {
    if (duration._data.months === 1) {
      return duration._data.months + ' month ago';
    } else return duration._data.months + ' months ago';
  } else if (duration._data.days >= 1) {
    if (getWeeks(duration._data.days).weeks >= 1) {
      if (getWeeks(duration._data.days).weeks === 1) {
        return getWeeks(duration._data.days).weeks + ' week ago';
      } else {
        return getWeeks(duration._data.days).weeks + ' weeks ago';
      }
    } else if (getWeeks(duration._data.days).weeks < 1) {
      if (getWeeks(duration._data.days).days === 1)
        return getWeeks(duration._data.days).days + ' day ago';
      else return getWeeks(duration._data.days).days + ' days ago';
    }
  } else if (duration._data.hours >= 1) {
    if (duration._data.hours === 1) return duration._data.hours + ' hour ago';
    else return duration._data.hours + ' hours ago';
  } else if (duration._data.minutes >= 1) {
    if (duration._data.minutes === 1)
      return duration._data.minutes + ' minute ago';
    else return duration._data.minutes + ' minutes ago';
  } else if (duration._data.seconds >= 1) {
    if (duration._data.seconds === 1)
      return duration._data.seconds + ' second ago';
    else return duration._data.seconds + ' seconds ago';
  }
};

// agodate();
