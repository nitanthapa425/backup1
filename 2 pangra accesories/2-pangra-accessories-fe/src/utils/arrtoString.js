export function arrayToString(array) {
  const arrString = array.map((value) => value.value).toString();
  return arrString;
}
