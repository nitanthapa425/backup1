export const valueSeparatedByComma = (arr = []) => {
  const value = arr.join(", ");

  return value;
};
