export const uniqueIdAmountSum = (oldArrOfObj = [], newArrOfObj = {}) => {
  let filterOldArrObj = [];
  let mergeOldNewWithUniqueId = [];

  filterOldArrObj = oldArrOfObj.filter(
    (value, i) => value?.id !== newArrOfObj?.id
  );

  mergeOldNewWithUniqueId = [...filterOldArrObj, newArrOfObj];

  return mergeOldNewWithUniqueId;
};

// console.log(
//   uniqueIdAmountSum(
//     [
//       { id: "1", value: 1 },
//       { id: "2", value: 2 },
//     ],
//     { id: "1", value: 99 }
//   )
// );

// expected output [ { id: '2', value: 2 }, { id: '1', value: 99 } ]
