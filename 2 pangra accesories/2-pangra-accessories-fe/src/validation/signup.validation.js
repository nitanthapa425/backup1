import * as yup from "yup";
import { PhoneValidation, UserNameValidation } from "./yupValidations";

export const SignUpValidationSchema = yup.object({
  userName: UserNameValidation(),
  email: yup.string().email("Email is invalid.").required("Email is required."),
  accessLevel: yup.string().required("Access Level is required."),

  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  dob: yup.string().required("Date of Birth is required."),
  mobile: PhoneValidation(),
  gender: yup.string().required("Gender is required."),
  documentImagePath: yup.array().when("accessLevel", {
    is: (accessLevel) => {
      return accessLevel === "DELIVERY";
    },
    then: yup.array().min(1, `At least one document image is required.`),
    otherwise: yup.array().nullable(),
  }),
  companyId: yup.string().when("accessLevel", {
    is: (accessLevel) => {
      return accessLevel === "DELIVERY";
    },
    then: yup.string().required("Company Name is required"),
    otherwise: yup.string().nullable(),
  }),
});

export const UpdateSignUpValidationSchema = yup.object({
  accessLevel: yup.string().required("Access Level is required."),
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    ),
  middleName: yup
    .string()

    .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field "),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    ),
  dob: yup.string().required("Date of Birth is required."),
  mobile: PhoneValidation(),
  gender: yup.string().required("Gender is required."),
});
export const UpdateMyValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    ),
  middleName: yup
    .string()

    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    ),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    ),
  dob: yup.string().required("Date of Birth is required."),
  mobile: PhoneValidation(),
  gender: yup.string().required("Gender is required."),
});
