import * as yup from 'yup';

export const loginPagevalidationSchema = yup.object({
  email: yup.string().required('Email/Username is required.'),
  password: yup.string().required('Password is required.'),
});
