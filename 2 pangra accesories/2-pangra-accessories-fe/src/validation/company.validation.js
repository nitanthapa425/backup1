import * as yup from "yup";
import { PhoneValidations } from "./yupValidations";

export const companyValidationSchema = yup.object({
  companyName: yup.string().required("Company Name is required."),
  fullName: yup.string().required("Full Name is required."),
  mobile: PhoneValidations(),
  email: yup.string(),
});
