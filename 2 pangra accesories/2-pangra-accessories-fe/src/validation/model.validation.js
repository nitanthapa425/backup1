import * as yup from "yup";
export const modelValidationSchema = yup.object({
  brandId: yup.string().required("Brand Name is required."),
  modelName: yup.string().required("Model Name is required."),
  modelDescription: yup
    .string()
    .max(1000, "Model Description must be at must 1000 characters"),
});

export const shippingValidationSchema = yup.object({
  shippingCharge: yup.string().required("Shipping Charge is required."),
});
