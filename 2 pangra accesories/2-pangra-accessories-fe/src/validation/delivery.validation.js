import * as yup from "yup";
import { PhoneValidations } from "./yupValidations";

export const deliveryMyUpdateValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  mobile: PhoneValidations(),
  gender: yup.string().required("Gender is required."),
  profileImagePath: yup.object(),
});
