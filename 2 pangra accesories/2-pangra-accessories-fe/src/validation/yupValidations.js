import * as yup from "yup";

export const NumberWithLimit = (field, min = 0, max = 100, reqd = true) => {
  if (reqd) {
    return yup
      .number()
      .typeError(`Only numbers are allowed for this field.`)
      .max(max, `${field} must be at most ${max}.`)
      .min(min, `${field} must be at least ${min}.`)
      .required(`${field} is required.`);
  } else {
    return yup
      .number()
      .typeError(`Only numbers are allowed for this field.`)
      .max(max, `${field} must be at most ${max}.`)
      .min(min, `${field} must be at least ${min}.`);
  }
};

export const checkEmailValidity = (email) =>
  yup
    .object()
    .shape({
      email: yup.string().email(),
    })
    .isValid({ email });

export const numberValidation = (dimension) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .required(`${dimension} is required.`);
  // .nullable();
};
export const mileageNumberValidation = (dimension, max = 3, min = 2) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field")
    .required(`${dimension} is required`)
    .max(max, `${dimension} is at must ${max} characters`)
    .min(min, `${dimension} is at least${min} characters`);
};
export const numberMaxValidation = (dimension, max = 7) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .required(`${dimension} is required.`)
    .max(max, `${dimension} is at must ${max} characters.`)
    .nullable();
};
export const sellerMaxValidation = (dimension, max = 10) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .required(`${dimension} is required.`)
    .max(max, `${dimension} is at must ${max} characters.`)
    .nullable();
};
export const countMaxValidation = (dimension, max = 1) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .required(`${dimension} is required.`)
    .max(max, `${dimension} is at must ${max} characters.`)
    .nullable();
};
export const numberValidationWithDecimal = (dimension) => {
  return yup
    .string()
    .matches(/^\d*\.?\d*$/, "Only numbers are allowed for this field.")
    .required(`${dimension} is required.`)
    .nullable();
};

export const UserNameValidation = () => {
  return yup
    .string()
    .required("Username is required.")
    .matches(
      /^[a-zA-Z0-9_.-]*$/,
      "Only alphabets and numbers are allowed for this field."
    )
    .min(3, "Username should be minimum 3 characters length.")
    .max(15, "Username should be maximum 15 characters length.");
};

export const PhoneValidation = (min = 10, max = 14) => {
  return yup
    .string()
    .required("Phone Number is required.")
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .min(min, "Phone number is not valid.")
    .max(max, "Phone number is not valid.")
    .nullable();
};
export const PhoneValidations = (min = 10, max = 14) => {
  return yup
    .string()
    .required("Mobile Number is required.")
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .min(min, "Phone number is not valid.")
    .max(max, "Phone number is not valid.")
    .nullable();
};
export const PhoneValidationsWithoutRequired = (min = 10, max = 14) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .min(min, "Phone number is not valid.")
    .max(max, "Phone number is not valid.")
    .nullable();
};

export const EmailValidation = (isRequired = false) => {
  if (isRequired) {
    return yup
      .string()
      .email("Enter a valid email.")
      .required("Email is required.");
  } else {
    return yup.string().email("Enter a valid email.");
  }
};

export const EmailValidationWithoutRequiredField = (message) => {
  return yup.string().email(message);
};

export const AlphabetsValidation = (name) => {
  return yup
    .string()
    .required(name + " is required.")
    .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field.");
};

export const PasswordValidation = () => {
  return yup
    .string("Password is required.")
    .required("New Password is required.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};
export const PasswordValidations = () => {
  return yup
    .string("Password is required.")
    .required("Password is required.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};
export const ConfirmPassword = () => {
  return yup
    .string("Password is required.")
    .required("Confirm Password is required.")
    .oneOf([yup.ref("newPassword")], "Passwords do not match.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};
export const ConfirmPasswords = () => {
  return yup
    .string("Password is required.")
    .required("Confirm Password is required.")
    .oneOf([yup.ref("password")], "Passwords do not match.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};
export const VerifyConfirmPassword = () => {
  return yup
    .string("Password is required.")
    .required("Confirm Password is required.")
    .oneOf([yup.ref("passwordHash")], "Passwords do not match.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};

export const ConfirmsPassword = () => {
  return yup
    .string("Password is required.")
    .required("Confirm Password is required.")
    .oneOf([yup.ref("password")], "Passwords do not match.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};

export const ConfirmPasswordNot = () => {
  return yup
    .string("Password is required.")
    .oneOf([yup.ref("newPassword")], "Passwords do not match.")
    .matches(
      /^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@$#%^&*]).*$/,
      "Password must be minimum of eight characters, with at least one uppercase letter, one lowercase letter, one number and one special character."
    );
};

export const AlphabetsWithoutRequiredValidation = () => {
  return yup
    .string()
    .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field. ");
};

export const NumberWithoutRequiredValidation = (name) => {
  return yup
    .string()
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field.")
    .max(10, name + " should be maximum 10 characters length.");
};
export const EmailValidationWithoutRequiredValidation = () => {
  return yup.string("Email is required.").email("Enter a valid email.");
};

export const RequiredValidation = (name) => {
  return yup.string().required(`${name} is required.`);
};
export const OnlyNumberWithoutRequired = (message) => {
  return yup.number().typeError(message);
};
export const RequiredField = (message) => {
  return yup.string().required(`${message} is required.`);
};
export const RequiredArray = (message) => {
  return yup.array().min(1, `${message} is required.`);
};

export const DecimalValidation = () => {
  return yup.string().required("Maximum Torque is required.");
};
