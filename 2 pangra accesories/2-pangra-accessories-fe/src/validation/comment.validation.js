import * as yup from 'yup';

export const replyValidationSchema = yup.object({
  replyMessage: yup.string().required('Reply message is required.'),
});
