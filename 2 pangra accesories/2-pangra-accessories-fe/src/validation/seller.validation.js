import * as yup from "yup";
import { countMaxValidation } from "./yupValidations";

export const SellerValidationSchema = yup.object({
  vehicleDetailId: yup.string().required("Vehicle Name is required."),
  bikeImagePath: yup.array().min(1, `At least a image is required.`),
  billBookImagePath: yup.array(),

  bikeDriven: yup
    .number()
    .min(0, "Bike Driven Must not be less than 1")
    .max(10000000, "Bike Driven must be at must 7 number")
    .typeError("Bike Driven must be number.")
    .required("Bike Driven is required."),
  expectedPrice: yup
    .string()
    .required(`Expected Price is required.`)
    .max(7, `Expected Price must be at must 7 numbers.`), // ***note 4 char is for NRs. and space , 7 for number  and 2 char for comma
  bikeNumber: yup
    .string()
    .max(20, `Bike number must be at must 20 characters`)
    .min(0, `Bike number  must be at least 0 characters.`)
    .required("Bike Number is required."),
  lotNumber: yup
    .number()
    .min(1, "Lot number Must not be less than 1")
    .max(100, "Lot number must be at must 3 number")
    .typeError("Lot number must be number.")
    .required("Lot number is required."),

  isNegotiable: yup.boolean().required("Is Negotiable is required"),
  condition: yup.string().required("condition is required"),
  mileage: yup
    .number()
    .min(0, "Mileage Must not be less than 1")
    .max(100, "Mileage must be at must 100")
    .typeError("Mileage must be number.")
    .required("Mileage is required."),

  hasAccident: yup.boolean().required("had accident is required"),
  color: yup.string().required("Color is required."),
  postExpiryDate: yup.string().required("Post Expiry Date is required."),
  note: yup.string().max(100000, "Note must be at must 10000"),
  ownershipCount: countMaxValidation("Ownership Count"),
  makeYear: yup.string().required("Make year is required."),
  location: yup.object({
    combineLocation: yup.string().required("Location is required."),
    nearByLocation: yup.string().required("Nearby location is required."),
  }),
});

export const OfferPriceValidationSchema = yup.object({
  offerPrice: yup
    .string()
    .required(`Offer price is required.`)
    .min(5, `Offer price must be at least 5 numbers.`)
    .max(7, `Offer price must be at must 7 numbers.`),
});

export const FinalOfferPriceValidationSchema = yup.object({
  finalOffer: yup

    .string()
    .required(`Final offer price is required.`)
    .min(5, `Final offer price must be at least 5 numbers.`)
    .max(7, `Final offer price must be at must 7 numbers.`),
});

export const RejectOfferPriceValidationSchema = yup.object({
  rejectRemarks: yup.string().required("Reject Remarks is required."),
});
