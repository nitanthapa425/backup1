import * as yup from 'yup';

export const categoryValidationSchema = yup.object({
  categoryName: yup.string().required('Category Name is required.'),
  categoryDescription: yup.string().max(1000, 'Category Description must be at most 1000 characters.'),
});
