import * as yup from "yup";
import {
  PasswordValidations,
  ConfirmPasswords,
  PhoneValidations,
  PasswordValidation,
  VerifyConfirmPassword,
} from "./yupValidations";

export const customerValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  password: PasswordValidations(),
  confirmPassword: ConfirmPasswords(),
  mobile: PhoneValidations(),
});

export const customerLoginValidationSchema = yup.object({
  mobile: PhoneValidations(),
  password: yup.string().required("Password is required."),
});

export const customerUpdateValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  mobile: PhoneValidations(),
  dob: yup.string().required("Date of Birth is required."),
  gender: yup.string().required("Gender is required."),
  profileImagePath: yup.object(),
});

export const shippingAddressValidation = yup.object({
  fullName: yup.string().required("Full Name is required."),
  mobile: PhoneValidations(),
  nearByLocation: yup.string().required("Nearby location is required."),
  exactLocation: yup.string().required("Exact location is required."),
  label: yup.string().required("Label is required."),
  districtName: yup.string().required("District  is required."),
  municipalityId: yup.string().required("Municipality/VDC is required."),
  wardNumber: yup
    .number()
    .typeError("Only numbers are allowed for this field.")
    .max(40, "Ward Number must be at must 40.")
    .required("Ward Number is required."),
  toleName: yup.string().required("Tole/Marg is required."),
});

export const customerMyUpdateValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  mobile: PhoneValidations(),
  dob: yup.string().required("Date of Birth is required."),
  gender: yup.string().required("Gender is required."),
  profileImagePath: yup.object(),
  location: yup.object({
    combineLocation: yup.string().required("Location is required."),
    nearByLocation: yup.string().required("Nearby location is required."),
    exactLocation: yup.string().required("Exact location is required."),
  }),
});

export const customerForgotValidationSchema = yup.object({
  mobile: PhoneValidations(),
});
export const customerOtpValidationSchema = yup.object({
  verificationToken: yup.string().required("OTP Code is required."),
  passwordHash: PasswordValidation("New Password"),
  confirmPassword: VerifyConfirmPassword("Confirm Password"),
});
export const PhoneVerificationValidationSchema = yup.object({
  token: yup.string().required("OTP Code is required."),
});

export const customerByAdminValidationSchema = yup.object({
  firstName: yup
    .string()
    .required("First Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "First Name must be at must 30 characters long"),
  middleName: yup
    .string()
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Middle Name must be at must 30 characters long"),
  lastName: yup
    .string()
    .required("Last Name is required.")
    .matches(
      /^([a-zA-Z]){1,30}$/i,
      "Only alphabets are allowed for this field."
    )
    .max(30, "Last Name must be at must 30 characters long"),
  mobile: PhoneValidations(),
});
