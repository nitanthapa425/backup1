import * as yup from 'yup';
import { ConfirmPassword, PasswordValidation } from './yupValidations';

export const PasswordUpdateSchema = yup.object({
  newPassword: PasswordValidation(),
  confirmPassword: ConfirmPassword(),
});
