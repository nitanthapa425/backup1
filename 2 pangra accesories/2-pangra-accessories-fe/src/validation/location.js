import * as yup from "yup";

export const LocationValidationSchema = yup.object({
  combineLocation: yup.string().required("Location is required."),
  nearByLocation: yup.string().required("Nearby location is required."),
});
