import * as yup from "yup";

export const brandvalidationSchema = yup.object({
  brandName: yup.string().required("Brand Name is required."),
  uploadBrandImage: yup.object().required("Brand Image is required."),
});

export const BannerValidationSchema = yup.object({
  bannerImage: yup.object().required("Banner Image is required."),
});
