import * as yup from "yup";

export const FullLocationValidationSchema = yup.object({
  districtName: yup.string().required("District  is required."),
  municipalityId: yup.string().required("Municipality/VDC is required."), // *****change Municipality message */
  wardNumber: yup
    .number()
    .typeError("Only numbers are allowed for this field.")
    .max(30, "Ward Number must be at must 30.")
    .required("Ward Number is required."),
  toleName: yup.string().required("Tole/Marg is required."),
});
