import * as yup from "yup";

export const verifyDeliveryValidationSchema = yup.object({
  deliveryToken: yup
    .string()
    .required(`Delivery token is required.`)
    .max(5, `Offer price must be at must 5 numbers.`),
  accessoriesId: yup.string().required(`Delivery token is required.`),
});
