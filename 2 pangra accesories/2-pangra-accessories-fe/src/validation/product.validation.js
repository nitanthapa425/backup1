import * as yup from "yup";
import {
  NumberWithLimit,
  PhoneValidation,
  PhoneValidations,
} from "./yupValidations";

export const createProductValidation = (type) => [
  yup.object({
    brandId:
      type === "add"
        ? yup.object().required("Brand is required.")
        : yup.string().required("Brand is required"),
    categoryId: yup.string().required("Category is required."),
    subCategoryId: yup.string().required("Sub Category is required."),
    modelId: yup.string().required("Model is required."),
    highLight: yup
      .string()
      .min(2, "Highlight must be at least 2 characters.")
      .max(50, "Highlight must be at most 50 characters."),

    costPrice: NumberWithLimit("MRP", 10, 1000000),
    adminPrice: NumberWithLimit("Admin Price", 10, 1000000, false),
    discountedPrice: NumberWithLimit("Discount Price", 0, 1000000, true)
      .test(
        "discountedPrice",
        "Discount Price must not be greater than MRP",
        function () {
          return !this.parent.discountedPrice
            ? true
            : this.parent.discountedPrice <= this.parent.costPrice;
        }
      )
      .nullable(),
    productImage: yup.object().required("Thumbnail Image is required."),
    productImages: yup
      .array()
      .min(1, `At least one product image is required.`),
  }),
  yup.object({
    weight: yup.number().typeError(`Only numbers are allowed for this field.`),
    weightUnit: yup.string(),
    dimension: yup.object({
      length: NumberWithLimit("Length", 1, 1000, false),
      breadth: NumberWithLimit("Breadth", 1, 1000, false),
      height: NumberWithLimit("Height", 1, 1000, false),
    }),
    dimensionUnit: yup.object({
      length: yup.string(),
      breadth: yup.string(),
      height: yup.string(),
    }),

    sizeCalculation: yup.string().required("Size Calculation unit is required"),
    sizeValue: yup
      .array()
      .min(1, `Size is required.`)
      .of(yup.string().min(1, "Must have at least 1 characters.")),

    color: yup
      .array()
      .min(1, `Color is required.`)
      .of(
        yup
          .string()
          .min(2, "Color must be at least 2 characters.")
          .max(50, "Color must be at most 50 characters.")
      ),

    availableOptions: yup
      .array()
      .of(
        yup.object().shape({
          quantity: NumberWithLimit("Quantity", 1, 100000),
          discountPercentage: NumberWithLimit(
            "Discount Percentage",
            0,
            100,
            true
          ),
          discountedAmount: NumberWithLimit(
            "Discount Amount",
            0,
            10000000,
            true
          ),
        })
      )
      .optional(),
  }),
  yup.object({
    productCode: yup.string(),
    hasWarranty: yup
      .string()
      .oneOf(["true", "false"])
      .required("Warranty is required"),

    warrantyTime: yup.string().when("hasWarranty", {
      is: "true",
      then: yup.string().required("Warranty Time is Required"),
    }),
    isReturnable: yup
      .string()
      .oneOf(["true", "false"])
      .required("Returnable is required"),

    returnableTime: yup.string().when("isReturnable", {
      is: "true",
      then: yup.string().required("Returnable Time is Required"),
    }),
    onSale: yup.boolean().required("On Sale is required"),
    tags: yup
      .array()
      .of(
        yup
          .string()
          .min(2, "Tags must be at least 2 characters.")
          .max(25, "Tags must be at most 25 characters.")
      ),
    description: yup
      .string()
      .min(10, "Description must be at least 10 characters.")
      .max(5000, "Description must be at most 5000 characters.")
      .required("Description is required"),
    shortDescription: yup
      .string()
      .min(5, "Short description must be at least 5 characters.")
      .max(300, "Short description must be at most 300 characters.")
      .required("Short description is required"),
  }),
];

export const AddToFlashSaleValidation = (flashSaleMaxVal) =>
  yup.object({
    startDate: yup.string().required("Start Date is required."),
    endDate: yup.string().required("End Date is required."),
    // flashSalePrice: NumberWithLimit("Flash Sale Price", 10, flashSaleMaxVal),
    flashSalePrice: NumberWithLimit("Flash Sale Price", 10, 1000000, true)
      .test(
        "flashSalePrice",
        "Flash Sale Price must not be greater than MRP",
        function () {
          return !this.parent.flashSalePrice
            ? true
            : this.parent.flashSalePrice <= flashSaleMaxVal;
        }
      )
      .nullable(),
  });
export const UpdateOrderedQuantityValidation = yup.object({
  quantity: NumberWithLimit("Quantity", 1, 10000000),
});
export const ReplyFeedBackValidation = yup.object({
  replyInquiry: yup.string().required("Message is required."),
});

export const InquiryFormValidation = yup.object({
  fullName: yup.string().required("Full Name is required."),
  mobile: PhoneValidations(),
  subject: yup.string().required("Subject is required."),

  description: yup.string(),

  districtName: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("District Name is required"),
    otherwise: yup.string().nullable(),
  }),
  municipalityId: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("Municipality Id is required"),
    otherwise: yup.string().nullable(),
  }),
  wardNumber: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("Ward Number is required"),
    otherwise: yup.string().nullable(),
  }),
  toleName: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("Tole Name is required"),
    otherwise: yup.string().nullable(),
  }),
  nearByLocation: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("Nearby location is required"),
    otherwise: yup.string().nullable(),
  }),
  exactLocation: yup.string().when("subject", {
    is: (subject) => {
      return subject === "Location Not Found";
    },
    then: yup.string().required("Exact location is required"),
    otherwise: yup.string().nullable(),
  }),
  productURL: yup.string().when("subject", {
    is: (subject) => {
      return (
        subject === "Product Color Not Found" ||
        subject === "Product Size Not Found"
      );
    },
    then: yup.string().required("Product URl is required"),
    otherwise: yup.string().nullable(),
  }),
});

export const sellNowValidationSchema = yup.object({
  color: yup.string().required("Color is required."),
  size: yup.string().required("Size is required."),
  quantity: NumberWithLimit("Quantity"),
  discountedPrice: NumberWithLimit("Price", 10, 1000000),
});
export const checkMobileNumberValidationSchema = yup.object({
  mobile: PhoneValidation(),
});
