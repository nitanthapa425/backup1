import * as yup from "yup";
import {
  countMaxValidation,
  PhoneValidations,
  PhoneValidationsWithoutRequired,
} from "./yupValidations";

export const sellVehicle = [
  yup.object({
    vehicleDetailId: yup.string().required("Vehicle Name is required."),
    bikeDriven: yup
      .number()
      .min(0, "Bike Driven Must not be less than 1")
      .max(10000000, "Bike Driven must be at must 7 number")
      .typeError("Bike Driven must be number.")
      .required("Bike Driven is required."),
    bikeNumber: yup
      .string()
      .max(20, `Bike number must be at must 20 characters`)
      .min(0, `Bike number  must be at least 0 characters.`)
      .required("Bike Number is required."),
    lotNumber: yup
      .number()
      .min(1, "Lot Number Must not be less than 1")
      .max(1000, "Lot Number must be at must 3 number")
      .typeError("Lot Number must be number.")
      .required("Lot Number is required."),

    mileage: yup
      .number()
      .min(0, "Mileage Must not be less than 1")
      .max(100, "Mileage must be at must 100")
      .typeError("Mileage must be number.")
      .required("Mileage is required."),

    color: yup.string().required("Color is required."),
    makeYear: yup.string(),
  }),
  yup.object({
    bikeImagePath: yup.array().min(1, `At least a image is required.`),
    billBookImagePath: yup.array(),
  }),
  yup.object({
    expectedPrice: yup
      .string()
      .required(`Expected Price is required.`)
      .min(5, `Expected Price must be at least 5 numbers.`)
      .max(7, `Expected Price must be at must 7 numbers.`), // ***note 4 char is for NRs. and space , 7 for number  and 2 char for comma

    condition: yup.string().required("condition is required"),

    ownershipCount: countMaxValidation("Ownership Count"),
    postExpiryDate: yup.string().required("Post Expiry Date is required."),
    note: yup.string().max(1000, "Note must be at must 1000"),
  }),

  yup.object({
    location: yup.object({
      combineLocation: yup.string().required("Location is required."),
      nearByLocation: yup.string().required("Nearby location is required."),
    }),
  }),

  yup.object({
    sellerDetails: yup.object({
      firstName: yup
        .string()
        .required("First Name is required.")
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "First Name must be at must 30 characters long"),
      middleName: yup
        .string()
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "Middle Name must be at must 30 characters long"),
      lastName: yup
        .string()
        .required("Last Name is required.")
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "Last Name must be at must 30 characters long"),

      email: yup.string().email("Enter a valid email."),
      mobile: PhoneValidations(),
      additionalMobile: PhoneValidationsWithoutRequired(),
    }),
  }),
];

export const sellVehicleAdmin = [
  yup.object({
    vehicleDetailId: yup.string().required("Vehicle Name is required."),
    bikeDriven: yup
      .number()
      .min(0, "Bike Driven Must not be less than 1")
      .max(10000000, "Bike Driven must be at must 7 number")
      .typeError("Bike Driven must be number."),
    // .required('Bike Driven is required.')
    bikeNumber: yup
      .string()
      .max(20, `Bike number must be at must 20 characters`)
      .min(0, `Bike number  must be at least 0 characters.`)
      .required("Bike Number is required."),
    lotNumber: yup
      .number()
      .min(1, "Lot Number Must not be less than 1")
      .max(1000, "Lot Number must be at must 3 number")
      .typeError("Lot Number must be number.")
      .required("Lot Number is required."),

    mileage: yup
      .number()
      .min(0, "Mileage Must not be less than 1")
      .max(100, "Mileage must be at must 100")
      .typeError("Mileage must be number."),
    color: yup.string().required("Color is required."),
    makeYear: yup.string(),
  }),
  yup.object({
    bikeImagePath: yup.array().min(1, `At least a image is required.`),
    billBookImagePath: yup.array(),
  }),
  yup.object({
    expectedPrice: yup
      .string()
      .required(`Expected Price is required.`)
      .min(5, `Expected Price must be at least 5 numbers.`)
      .max(7, `Expected Price must be at must 7 numbers.`), // ***note 4 char is for NRs. and space , 7 for number  and 2 char for comma

    condition: yup.string().required("condition is required"),

    ownershipCount: countMaxValidation("Ownership Count"),
    postExpiryDate: yup.string().required("Post Expiry Date is required."),
    note: yup.string().max(1000, "Note must be at must 1000"),
  }),

  yup.object({
    location: yup.object({
      combineLocation: yup.string().required("Location is required."),
      nearByLocation: yup.string().required("Nearby location is required."),
    }),
  }),

  yup.object({
    sellerDetails: yup.object({
      firstName: yup
        .string()
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "First Name must be at must 30 characters long"),
      middleName: yup
        .string()
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "Middle Name must be at must 30 characters long"),
      lastName: yup
        .string()
        .matches(
          /^([a-zA-Z]){1,30}$/i,
          "Only alphabets are allowed for this field."
        )
        .max(30, "Last Name must be at must 30 characters long"),

      email: yup.string().email("Enter a valid email."),
      mobile: PhoneValidationsWithoutRequired(),
      additionalMobile: PhoneValidationsWithoutRequired(),
      customerDescription: yup.string().required("Description is required."),
    }),
  }),
];
