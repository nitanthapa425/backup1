import * as yup from "yup";

export const subCategoryValidationSchema = yup.object({
  categoryId: yup.string().required("Category Name is required."),
  subCategoryName: yup.string().required("Sub Category Name is required."),
  subCategoryDescription: yup
    .string()
    .max(1000, "Sub Category Description must be at must 1000 characters."),
});
