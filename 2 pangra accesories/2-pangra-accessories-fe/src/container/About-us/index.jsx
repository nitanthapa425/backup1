const AboutUS = () => {
  return (
    <div>
      <main id="main">
        <section className="vehicle-listing-section pt-10 pb-20">
          <div className="container">
            <h1 className="h3 mb-4">About Us</h1>
            The vision of 2Pangra (Dui Pangra - or ‘two wheels’ in Nepali) is to
            be the one stop shop for almost anything related to two wheelers in
            Nepal. From bicycles to top of the line motorcycles. 2Pangra
            currently sells branded motorcycle gear and accessories through its
            social media pages, its website and various retailers throughout
            Nepal. 2Pangra is the official distributor for various brands in the
            motorcycle accessories space like Rynox, Raida, XDI, Dirtsack,
            Viaterra, FastBend Clothing. The company also plans to expand its
            association with other Indian and International brands in the
            immediate future. 2Pangra is already working on an online platform
            that enables the purchase and sale of second-hand motorcycles
            powered by AI & Data Science. After intensive tests, the company
            aims to launch the platform to make buying and selling second hand
            motorcycles a trusted, seamless and profitable experience for
            everyone. The platform will work with various companies in the
            automotive and finance sectors to bring motorcycle financing,
            insurance, servicing and related services to its customers. The
            investors behind 2Pangra have a deep love for two wheelers and are
            from sectors like advertising, sales & marketing, IT and understand
            brands.
            <br></br>
            <br></br>
            <br></br>
            <div className="h5"> WHY 2PANGRA?</div>
            2Pangra was formed by people with a history of trustworthiness,
            capability and foresight. Everything done at 2Pangra adheres to high
            standards of transparency and corporate governance. 2Pangra brings
            the same diligence and trustworthiness to its product and services.
            We don’t sell “First Copy” and “Second Copy” products. We don’t sell
            imitation, fake or shoddy products. You can be assured that products
            sold by 2Pangra are procured straight from BRANDED manufacturers
            after complete due diligence and research on them.
          </div>
        </section>
      </main>
    </div>
  );
};

export default AboutUS;
