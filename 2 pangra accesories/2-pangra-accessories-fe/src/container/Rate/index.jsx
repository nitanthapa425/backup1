// import "./styles.css";
import ReactStars from "react-rating-stars-component";

export default function Rate({ populateRatingValue }) {
  return (
    <div className="App">
      <ReactStars
        size={25}
        count={5}
        color="gray"
        activeColor="#A7EB4A"
        value={0}
        a11y={true}
        isHalf={true}
      />
    </div>
  );
}
