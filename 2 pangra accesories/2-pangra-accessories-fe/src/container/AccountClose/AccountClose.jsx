import React, { useRef, useEffect } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import { useToasts } from "react-toast-notifications";

import { useCloseAccountMutation } from "services/api/seller";
import * as yup from "yup";

import Textarea from "components/Input/textarea";
import Input from "components/Input";
import { numberValidation } from "validation/yupValidations";

const AccountClose = ({ id, setIsOpen, accountCloseDetails }) => {
  const formikBag = useRef();
  const { addToast } = useToasts();

  const [closeAccount, { isError, error, isSuccess, isLoading, data }] =
    useCloseAccountMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(data?.message || "You have successfully close account", {
        appearance: "success",
      });

      setIsOpen(false);
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to close account . Please try again later.",
        {
          appearance: "error",
          autoDismiss: false,
        }
      );
    }
  }, [isSuccess, isError]);

  // useDeliveryCompanyQuery;

  //   console.log("deliveryPerson", );

  const accountCloseValidation = yup.object({
    accountCloseMessage: yup.string(),
    totalClosingPrice: numberValidation("Product Price"),
    totalClosingShippingCharge: numberValidation("Shipping Price"),
  });

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    closeAccount({ body: values, id: id });
    setSubmitting(false);
  };

  const initialValues = {
    accountCloseMessage: accountCloseDetails.accountCloseMessage,
    totalClosingPrice: accountCloseDetails.totalClosingPrice,
    totalClosingShippingCharge: accountCloseDetails.totalClosingShippingCharge,
  };

  return (
    <>
      <h3 className="h5 mb-2">Release Order </h3>
      <Formik
        initialValues={initialValues}
        onSubmit={handleOnSubmit}
        validationSchema={accountCloseValidation}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
          setFieldTouched,
        }) => (
          <Form>
            <div>
              <Input
                required={true}
                label="Product Price"
                name="totalClosingPrice"
                type="text"
              />

              <Input
                required={true}
                label="Shipping Price"
                name="totalClosingShippingCharge"
                type="text"
              />

              <Textarea
                label="Account Close Description"
                required={false}
                name="accountCloseMessage"
                type="text"
              />

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Account Close
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
      {/* </div> */}
    </>
  );
};

export default AccountClose;
