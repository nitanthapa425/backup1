import { useEffect } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import "react-datepicker/dist/react-datepicker.css";
import { ReplyFeedBackValidation } from "validation/product.validation";

import Textarea from "components/Input/textarea";
import { useCreateEnquiryReplyMutation } from "services/api/EnquiryForm.Service/enquiryFormService";

import DOMPurify from "dompurify";

const initialValues = {
  replyInquiry: "",
  //   productId: "",
};

const InquiryFeedBack = ({ inquiryId, description, subject, setIsOpen }) => {
  const { addToast } = useToasts();

  const [
    replyFeedBack,
    {
      isSuccess,
      isError,
      isLoading: isLoadingReplyFeedback,
      error,
      data: dataReplyFeedback,
    },
  ] = useCreateEnquiryReplyMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast(
        dataReplyFeedback?.message || "Inquiry is replied successfully",
        {
          appearance: "success",
        }
      );

      setIsOpen(false);
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message || "error occurred while replying Inquiry",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        // console.log({ body: values, id: orderQuantityId });
        // setOrderQuantityId({
        //   inventoryId: original?.id,
        //   productId: original?.productId,
        // });

        replyFeedBack({
          ...values,
          id: inquiryId,
        });
        setSubmitting(false);
      }}
      validationSchema={ReplyFeedBackValidation}
      enableReinitialize
    >
      {({
        setFieldValue,
        setFieldTouched,
        values,
        errors,
        touched,
        // resetForm,
        // isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h4 mb-3">Inquiry Reply</h1>
          <h3 className="h6 mb-2">
            Inquiry Subject:
            {description && (
              <div>
                <div
                  className="text-sm text-primary"
                  dangerouslySetInnerHTML={{
                    __html: DOMPurify.sanitize(subject),
                  }}
                ></div>{" "}
              </div>
            )}
          </h3>
          <h3 className="h6 mb-2">
            Inquiry Question:{" "}
            {description && (
              <div>
                <div
                  className="text-sm text-primary"
                  dangerouslySetInnerHTML={{
                    __html: DOMPurify.sanitize(description),
                  }}
                ></div>{" "}
              </div>
            )}
          </h3>

          <Form>
            <div className="w-full mb-2">
              <div className="mb-2">
                <Textarea
                  label="Reply Message"
                  required={true}
                  name="replyInquiry"
                  type="text"
                />
              </div>
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingReplyFeedback}
                disabled={isLoadingReplyFeedback || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              <Button
                disabled={isLoadingReplyFeedback || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default InquiryFeedBack;
