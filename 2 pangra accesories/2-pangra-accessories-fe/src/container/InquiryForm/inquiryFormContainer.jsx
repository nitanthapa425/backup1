import Button from "components/Button";
import Table from "components/Table/table";
import DOMPurify from "dompurify";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { useReadEnquiryListQuery } from "services/api/EnquiryForm.Service/enquiryFormService";
import Modal from "react-modal";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import InquiryFeedBack from "./InquiryFeedBack";

function InquiryContainer() {
  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [modelDataQuery, setModelDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const [inquiryId, setInquiryId] = useState("");
  const [desc, setDesc] = useState("");
  const [subject, setSubject] = useState("");

  const {
    data: modelData,
    isError: modelError,
    isFetching: isLoadingModel,
  } = useReadEnquiryListQuery(modelDataQuery);

  const {
    data: modelDataAll,

    isFetching: isLoadingAll,
  } = useReadEnquiryListQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (modelDataAll) {
      setTableDataAll(
        modelDataAll?.docs.map((model) => {
          return {
            id: model?._id,
            fullName: model?.fullName,
            mobile: model?.mobile,
            location: model?.location,
            districtName: model?.districtName,
            municipalityName: model?.municipalityName,
            wardNumber: model?.wardNumber,
            toleName: model?.toleName,
            nearByLocation: model?.nearByLocation,
            exactLocation: model?.exactLocation,
            productURL: model?.productURL,
            subject: model?.subject,
            description: model?.description,
            replyInquiry: model?.replyInquiry,
          };
        })
      );
    }
  }, [modelDataAll]);

  //   const [
  //     deleteItems,
  //     {
  //       isLoading: isDeleting,
  //       isSuccess: isDeleteSuccess,
  //       isError: isDeleteError,
  //       error: DeletedError,
  //     },
  //   ] = useDeleteModelMutation();

  const columns = useMemo(
    () => [
      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "mobile",
        Header: "Mobile Number",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "subject",
        Header: "Subject",
        accessor: "subject",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "districtName",
        Header: "District Name",
        accessor: "districtName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "municipalityName",
        Header: "Municipality Name",
        accessor: "municipalityName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "toleName",
        Header: "Tole Name",
        accessor: "toleName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "wardNumber",
        Header: "Exact location",
        accessor: "wardNumber",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "exactLocation",
        Header: "Exact location",
        accessor: "exactLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "nearByLocation",
        Header: "Nearby Location",
        accessor: "nearByLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "productURL",
        Header: "Product URL",
        accessor: "productURL",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "description",
        Header: "Description",
        accessor: "description",
        Cell: ({ cell: { value } }) =>
          (
            <div
              dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(value) }}
            ></div>
          ) || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "replyInquiry",
        Header: "Reply Inquiry",
        accessor: "replyInquiry",
        Cell: ({ cell: { value } }) =>
          (
            <div
              dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(value) }}
            ></div>
          ) || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "inquiry ",
        Header: "Reply Inquiry",
        accessor: "inquiry ",
        Cell: ({ row: { original }, cell: { value } }) => {
          // console.log("original", original);
          return (
            <Button
              onClick={() => {
                // setInquiry(original?.productId);
                setInquiryId(original?.id);
                setDesc(original?.description);
                setSubject(original?.subject);
                setIsOpen(true);
              }}
            >
              Reply
            </Button>
          );
        },
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setModelDataQuery(query);
  };

  useEffect(() => {
    if (modelData) {
      setPageCount(modelData.totalPages);
      setTotalData(modelData.totalDocs);
      setTableData(
        modelData?.docs?.map((model) => {
          return {
            id: model?._id,
            fullName: model?.fullName,
            mobile: model?.mobile,
            location: model?.location,
            districtName: model?.districtName,
            municipalityName: model?.municipalityName,
            wardNumber: model?.wardNumber,
            toleName: model?.toleName,
            nearByLocation: model?.nearByLocation,
            exactLocation: model?.exactLocation,
            productURL: model?.productURL,
            subject: model?.subject,
            description: model?.description,
            replyInquiry: model?.replyInquiry,
          };
        })
      );
    }
  }, [modelData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Inquiry List",
      route: "/inquiry",
    },
  ];

  return (
    <AdminLayout documentTitle="Inquiry List" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}
          <Modal
            isOpen={modalIsOpen}
            className="mymodal order"
            overlayClassName="myoverlay"
          >
            <button
              type="button"
              onClick={closeModal}
              className="text-error absolute top-[5px] right-[5px]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>

            <InquiryFeedBack
              setIsOpen={setIsOpen}
              inquiryId={inquiryId}
              description={desc}
              subject={subject}
            ></InquiryFeedBack>
          </Modal>

          <Table
            tableName="Inquiry/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={modelError}
            isLoadingData={isLoadingModel}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="admin/model/update"
            // viewRoute="admin/model"
            // deleteQuery={deleteItems}
            // isDeleting={isDeleting}
            // isDeleteError={isDeleteError}
            // isDeleteSuccess={isDeleteSuccess}
            // DeletedError={DeletedError}
            hasExport={true}
            // addPage={{ page: "Add Model", route: "/admin/model/create" }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default InquiryContainer;
