import { useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import "react-datepicker/dist/react-datepicker.css";
import { InquiryFormValidation } from "validation/product.validation";
import Input from "components/Input";
import Select from "components/Select";
import { subjects } from "constant/constant";
import Editor from "components/Editor";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Popup from "components/Popup";
import { useCreateEnquiryFormMutation } from "services/api/EnquiryForm.Service/enquiryFormService";
import { useSelector } from "react-redux";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import ReactSelect from "components/Select/ReactSelect";
import { nNumberArray } from "utils/nNumberOfArray";

import {
  useCreateMunicipalityVdcByDistrictMutation,
  useReadMunicipalityVdcByDistrictQuery,
  useReadAllDistrictQuery,
} from "services/api/location";

const InquiryForm = ({ value, closeModelOpenInquiry }) => {
  const [openModal, setOpenModal] = useState(false);
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);
  const { addToast } = useToasts();

  const loginInfo = useSelector((state) => state.customerAuth);
  const [districtValue, setDistrictValue] = useState(null);

  const initialValues = {
    userId: loginInfo?.customer?._id || "",
    fullName: loginInfo?.customer?.fullName || "",
    mobile: loginInfo?.customer?.mobile || "",
    subject: value || "",
    // location: "",
    districtName: "",
    municipalityName: "",
    wardNumber: "",
    toleName: "",
    exactLocation: "",
    nearByLocation: "",
    productURL: "",
    description: "",
  };

  const [
    inquiryForm,
    {
      isSuccess,
      isError,
      isLoading: isLoadingInquiry,
      error,
      data: dataInquiryForm,
    },
  ] = useCreateEnquiryFormMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast(
        dataInquiryForm?.message ||
          "Thank you! Form has been submitted successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);
      closeModelOpenInquiry();
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message || "error occurred while during your inquiry",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  const [editorLoaded, setEditorLoaded] = useState(false);

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  const { data: district, isFetching: isFetchingDistrict } =
    useReadAllDistrictQuery();

  const {
    data: municipalityVdc,
    isFetching: isFetchingMunicipalityVdcByDistrict,
  } = useReadMunicipalityVdcByDistrictQuery(districtValue, {
    skip: !districtValue,
    // keepUnusedDataFor: 0,
  });
  const [createMunicipalityVdcByDistrict] =
    useCreateMunicipalityVdcByDistrictMutation();
  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        if (!values.userId) {
          delete values.userId;
        }
        inquiryForm(values);
        setSubmitting(false);
      }}
      validationSchema={InquiryFormValidation}
      enableReinitialize
    >
      {({
        setFieldValue,
        setFieldTouched,
        resetForm,
        values,
        errors,
        touched,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">Inquire Your Product</h1>

          <Form>
            <div className="row-sm">
              <div className="two-col-sm">
                <Input
                  label="Full Name"
                  name="fullName"
                  type="text"
                  placeholder="E.g: John Deo"
                />
              </div>
              <div className="two-col-sm">
                <Input
                  label="Mobile Number"
                  name="mobile"
                  type="text"
                  placeholder="E.g: 9867########"
                />
              </div>
              <div className="two-col-sm">
                <Select label="Subject" name="subject" required="true">
                  <option value="">Select Subject</option>

                  {subjects.map((subject, i) => (
                    <option key={i} value={subject}>
                      {subject}
                    </option>
                  ))}
                </Select>
              </div>

              {values.subject === "Location Not Found" ? (
                <>
                  <div className="row-sm  pt-5 pb-2">
                    <div className="two-col-sm">
                      <SelectWithoutCreate
                        required
                        loading={isFetchingDistrict}
                        label="Select District"
                        placeholder="District"
                        error={
                          touched?.districtName ? errors?.districtName : ""
                        }
                        value={values?.districtName || null}
                        onChange={(selectedValue) => {
                          // setFieldTouched('district');
                          setFieldValue("districtName", selectedValue.value);
                          setDistrictValue(selectedValue.value);
                          setFieldValue("municipalityId", "");
                          setFieldTouched("municipalityId", false);
                        }}
                        options={district?.map((value) => ({
                          label: value.district,
                          value: value.district,
                        }))}
                        onBlur={() => {
                          setFieldTouched("districtName");
                        }}
                      />
                    </div>

                    <div className="two-col-sm">
                      <ReactSelect
                        required
                        isDisabled={!values?.districtName}
                        loading={isFetchingMunicipalityVdcByDistrict}
                        label="Select Municipality/VDC"
                        placeholder="Select Municipality/VDC"
                        error={
                          touched?.municipalityId ? errors?.municipalityId : ""
                        }
                        value={values?.municipalityId || ""}
                        onChange={(selectedValue, actionMeta) => {
                          setFieldTouched("municipalityId");
                          if (
                            selectedValue.value &&
                            actionMeta.action === "create-option"
                          ) {
                            createMunicipalityVdcByDistrict({
                              // provinceName: values?.province,
                              districtName: values?.districtName,
                              municipalityName: selectedValue.value,
                            }).then((res, i) => {
                              setFieldValue("municipalityId", res.data.id);
                            });
                          } else {
                            setFieldValue(
                              "municipalityId",
                              selectedValue.value
                            );
                          }
                        }}
                        options={municipalityVdc?.map((value) => ({
                          label: value.municipalityName,
                          value: value.id,
                        }))}
                        onBlur={() => {
                          setFieldTouched("municipalityId");
                        }}
                      />
                    </div>

                    <div className="two-col-sm">
                      <Select
                        label="Ward Number"
                        name="wardNumber"
                        required={true}
                      >
                        <option value="">Select Ward Number</option>

                        {nNumberArray(40).map((value, i) => (
                          <option key={i} value={value}>
                            {value}
                          </option>
                        ))}
                      </Select>
                    </div>
                    <div className="two-col-sm">
                      <Input
                        required={true}
                        name="toleName"
                        label="Tole/Marg"
                        type="text"
                      />
                    </div>
                  </div>
                  <div className="two-col-sm">
                    <Input
                      label="Exact Location"
                      name="exactLocation"
                      type="text"
                      placeholder="E.g: Ananya Apartment Phase I"
                    />
                  </div>
                  <div className="two-col-sm">
                    <Input
                      label="Nearby Location"
                      name="nearByLocation"
                      type="text"
                      placeholder="E.g: Nic Asia Bank"
                    />
                  </div>
                </>
              ) : null}

              {values.subject === "Product Color Not Found" ||
              values.subject === "Product Size Not Found" ? (
                <div className="two-col-sm">
                  <Input
                    label="Product URL"
                    name="productURL"
                    type="text"
                    placeholder="E.g: http://localhost:3000/product-listing/628b8025124a380023f6ae63#"
                  />
                </div>
              ) : null}

              <div className="one-col-sm product-description-editor">
                <Editor
                  name="description"
                  onChange={(data) => {
                    setFieldTouched("description");
                    setFieldValue("description", data);
                  }}
                  value={values?.description || ""}
                  editorLoaded={editorLoaded}
                  label="Description"
                  onBlur={() => {
                    setFieldTouched("description");
                  }}
                  touched={touched?.description}
                  errors={errors?.description}
                  required={false}
                />
              </div>
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingInquiry}
                disabled={isLoadingInquiry || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              {/* <Button
                disabled={isLoadingInquiry || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button> */}
              <Button
                onClick={() => {
                  setOpenModal(true);
                }}
                variant="outlined-error"
                type="button"
                disabled={isSubmitting || !dirty || isLoadingInquiry}
              >
                Clear
              </Button>

              {openModal && (
                <Popup
                  title="Are you sure to clear all fields?"
                  description="If you clear all fields, the data will not be saved."
                  onOkClick={() => {
                    resetForm();
                    setChanged(false);
                    setOpenModal(false);
                  }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )}
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default InquiryForm;
