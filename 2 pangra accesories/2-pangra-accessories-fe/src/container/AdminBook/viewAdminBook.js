import { useState, useEffect, useMemo } from "react";

import Table from "components/Table/table";

import {
  useDeleteBookedListMutation,
  useReadAllBookInvQuery,
  useReadBookInvQuery,
} from "services/api/seller";
import { getQueryStringForBook } from "utils/getQueryStringForTable";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import { SelectColumnFilter } from "components/Table/Filter";

function AdminBook() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [bookQuery, setBookQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataBook,
    isError: isErrorBook,
    isFetching: isFetchingBook,
  } = useReadBookInvQuery(bookQuery);

  const { data: dataAllBook, isFetching: isFetchingAllBook } =
    useReadAllBookInvQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });
  useEffect(() => {
    if (dataAllBook) {
      setTableDataAll(
        dataAllBook.docs.map((value) => {
          return {
            id: value.id,
            customerName: value.customerName,
            customerMobile: value.customerMobile,
            createdAt: value.createdAt,
            // combineLocation: value.shippingAddressLocation?.combineLocation
            //   .split(">")
            //   .join(","),
            combineLocation: value.shippingAddressLocation?.exactLocation,
            nearByLocation: value.shippingAddressLocation?.nearByLocation,
            totalQuantity: value.totalQuantity,
            totalPrice: thousandNumberSeparator(value.totalPrice),
            totalShippingCharge: thousandNumberSeparator(
              value.totalShippingCharge
            ),
          };
        })
      );
    }
  }, [dataAllBook]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteBookedListMutation();

  const columns = useMemo(
    () => [
      {
        id: "id",
        Header: "id",
        accessor: "id",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "customerName",
        Header: "Customer Name",
        accessor: "customerName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "customerMobile",
        Header: "Customer Mobile",
        accessor: "customerMobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "orderType",
        Header: "Order Type",
        accessor: "orderType",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Online",
            value: "Online",
          },
          {
            label: "Physical",
            value: "Physical",
          },
        ],
      },

      {
        id: "combineLocation",
        Header: "Combine Location",
        accessor: "combineLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        // canBeFiltered: true,
      },
      {
        id: "exactLocation",
        Header: "Exact Location",
        accessor: "exactLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "nearByLocation",
        Header: "Nearby Location",
        accessor: "nearByLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "totalQuantity",
        Header: "Total Quantity",
        accessor: "totalQuantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "totalPrice",
        Header: " Total Price (NRs.)",
        accessor: "totalPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "totalShippingCharge",
        Header: "Total shipping Charge (NRs.)",
        accessor: "totalShippingCharge",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "createdAt",
        Header: "CreatedAt",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForBook(pageIndex, pageSize, sortBy, filters);
    setBookQuery(query);
  };

  useEffect(() => {
    if (dataBook) {
      setPageCount(dataBook.totalPages);
      setTotalData(dataBook.totalDocs);
      setTableData(
        dataBook.docs.map((value) => {
          return {
            id: value.id,
            customerName: value.customerName,
            customerMobile: value.customerMobile,
            orderType: value.orderType,
            createdAt: value.createdAt,
            // combineLocation: value.shippingAddressLocation?.exactLocation
            //   ?.split?.(">")
            //   ?.join?.(","),
            combineLocation: value.shippingAddressLocation?.exactLocation,
            nearByLocation: value.shippingAddressLocation?.nearByLocation,
            totalQuantity: value.totalQuantity,
            totalPrice: thousandNumberSeparator(value.totalPrice),
            totalShippingCharge: thousandNumberSeparator(
              value.totalShippingCharge
            ),
          };
        })
      );
    }
  }, [dataBook]);
  return (
    <div>
      <h3 className="mb-3">Ordered List</h3>

      <Table
        tableName="Ordered-List/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllBook}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorBook}
        isLoadingData={isFetchingBook}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        viewRoute="/book/orderedItemList"
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
        addPage={{
          page: "Delivered Product",
          route: "/admin/delivered-product",
        }}
      />
    </div>
  );
}

export default AdminBook;
