// import { useState, useEffect, useMemo } from "react";

// import Table from "components/Table/table";

// import {
//   useDeleteBookedListMutation,
//   useReadAllBookInvQuery,
//   useReadBookInvQuery,
// } from "services/api/seller";
// import { getQueryStringForBook } from "utils/getQueryStringForTable";
// import { thousandNumberSeparator } from "utils/thousandNumberFormat";
// import Button from "components/Button";
// import OrderStatus from "container/OrderStatus/OrderStatus";
// import Modal from "react-modal";
// import { SelectColumnFilter } from "components/Table/Filter";
// import { color, orderStatus } from "constant/constant";
// import { badgeColorFun } from "utils/applicationUtils/commonUtils";
// import OutOfStock from "container/Outofstock/OutOfStock";

// function AdminBook() {
//   const [modalIsOpen, setIsOpen] = useState(false);
//   const closeModal = () => {
//     setIsOpen(false);
//   };

//   const [modalIsOpenStock, setIsOpenStock] = useState(false);
//   const closeModalStock = () => {
//     setIsOpenStock(false);
//   };

//   const [tableData, setTableData] = useState([]);
//   const [pageCount, setPageCount] = useState(0);
//   const [totalData, setTotalData] = useState(0);
//   const [bookQuery, setBookQuery] = useState("");
//   const [tableDataAll, setTableDataAll] = useState([]);
//   const [skipTableDataAll, setSkipTableDataAll] = useState(true);
//   const [orderDetails, setOrderDetails] = useState("");

//   const [outOfStockId, setOutOfStockId] = useState({});

//   const {
//     data: dataBook,
//     isError: isErrorBook,
//     isFetching: isFetchingBook,
//   } = useReadBookInvQuery(bookQuery);

//   const { data: dataAllBook, isFetching: isFetchingAllBook } =
//     useReadAllBookInvQuery("?&sortBy=createdAt&sortOrder=-1", {
//       skip: skipTableDataAll,
//     });

//   useEffect(() => {
//     if (dataAllBook) {
//       setTableDataAll(
//         dataAllBook.docs.map((value) => {
//           return {
//             id: value.id || "-",
//             productName: value.productName || "-",
//             color: value.color || "-",
//             numViews: value.numViews || "-",
//             price: value.totalPrice || "-",
//             shippingFee: value.shippingFee || "-",
//           };
//         })
//       );
//     }
//   }, [dataAllBook]);

//   const [
//     deleteItems,
//     {
//       isLoading: isDeleting,
//       isSuccess: isDeleteSuccess,
//       isError: isDeleteError,
//       error: DeletedError,
//     },
//   ] = useDeleteBookedListMutation();

//   const columns = useMemo(
//     () => [
//       {
//         id: "id",
//         Header: "id",
//         accessor: "id",
//         Cell: ({ cell: { value } }) => value || "-",
//         canBeSorted: true,
//         canBeFiltered: true,
//       },

//       {
//         id: "productName",
//         Header: "Product Title",
//         accessor: "productName",
//         Cell: ({ cell: { value } }) => value || "-",
//         canBeSorted: true,
//         canBeFiltered: true,
//       },

//       {
//         id: "size",
//         Header: "Size",
//         accessor: "size",
//         Cell: ({ cell: { value } }) => value || "-",
//         // canBeSorted: true,
//         // canBeFiltered: true,
//       },

//       {
//         id: "color",
//         Header: "Color",
//         accessor: "color",
//         Cell: ({ cell: { value } }) => value || "-",
//         Filter: SelectColumnFilter,

//         possibleFilters: color.map((value) => {
//           return {
//             label: value,
//             value: value,
//           };
//         }),

//         canBeSorted: true,
//         canBeFiltered: true,
//       },

//       {
//         id: "orderQuantity",
//         Header: "Order Quantity",
//         accessor: "orderQuantity",
//         Cell: ({ cell: { value } }) => value || "-",
//         canBeSorted: true,
//         canBeFiltered: true,
//       },
//       {
//         id: "totalPrice",
//         Header: " Cost Price (NRs.)",
//         accessor: "totalPrice",
//         Cell: ({ cell: { value } }) => value || "-",
//         canBeSorted: true,
//         canBeFiltered: true,
//       },

//       {
//         id: "shippingFee",
//         Header: "shippingFee (NRs.)",
//         accessor: "shippingFee",
//         Cell: ({ cell: { value } }) => value || "-",
//         canBeSorted: true,
//         canBeFiltered: true,
//       },
//       {
//         id: "deliveryStatus",
//         Header: "Delivery Status",
//         accessor: "deliveryStatus",
//         Cell: ({ cell: { value } }) => {
//           return <div className={badgeColorFun(value)}>{value}</div>;
//         },
//         Filter: SelectColumnFilter,

//         possibleFilters: orderStatus.map((order) => {
//           return {
//             label: order,
//             value: order,
//           };
//         }),

//         canBeSorted: true,
//         canBeFiltered: true,
//       },
//       {
//         id: "changeStatus",
//         Header: "Change Status",
//         accessor: "changeStatus",
//         Cell: ({ row: { original }, cell: { value } }) => {
//           return (
//             <Button
//               onClick={() => {
//                 setOrderDetails({
//                   id: original.id,
//                   deliveryStatus: original.deliveryStatus,
//                   approveMessages: original.approveMessages,
//                   cancelMessages: original.cancelMessages,
//                   deliveryPersonId: original.deliveryPersonId,
//                   deliveryCompanyId: original.deliveryCompanyId,
//                 });
//                 setIsOpen(true);
//               }}
//               disabled={original.notInStock}
//             >
//               Change
//             </Button>
//           );
//         },
//       },
//       {
//         id: "outOfStock",
//         Header: "Release Order",
//         accessor: "outOfStock",
//         Cell: ({ row: { original }, cell: { value } }) => {
//           if (original.notInStock) {
//             return (
//               <Button
//                 onClick={() => {
//                   setOutOfStockId({
//                     id: original?.id,
//                     orderQuantity: original?.orderQuantity,
//                   });
//                   setIsOpenStock(true);
//                 }}
//               >
//                 Release Order
//               </Button>
//             );
//           } else {
//             return null;
//           }
//         },
//       },
//     ],
//     []
//   );

//   const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
//     const query = getQueryStringForBook(pageIndex, pageSize, sortBy, filters);
//     setBookQuery(query);
//   };

//   useEffect(() => {
//     if (dataBook) {
//       setPageCount(dataBook.totalPages);
//       setTotalData(dataBook.totalDocs);
//       setTableData(
//         dataBook.docs.map((value) => {
//           return {
//             id: value.id,
//             productName: value.productName,
//             bikeDriven: value.bikeDriven,
//             numViews: value.numViews,
//             size: value.size,
//             color: value.color,
//             orderQuantity: value.quantity,
//             totalPrice: thousandNumberSeparator(value.totalPrice),
//             shippingFee: thousandNumberSeparator(value.shippingFee),
//             deliveryStatus: value.deliveryStatus,
//             approveMessages: value.approveMessages,
//             cancelMessages: value.cancelMessages,
//             deliveryPersonId: value?.onTheWayDetail?.deliveryPersonId,
//             deliveryCompanyId: value?.onTheWayDetail?.companyId,
//             notInStock: value?.notInStock,
//           };
//         })
//       );
//     }
//   }, [dataBook]);
//   return (
//     <div>
//       <h3 className="mb-3">Order Status</h3>

//       <Modal
//         isOpen={modalIsOpen}
//         className="mymodal order"
//         overlayClassName="myoverlay"
//       >
//         <button
//           type="button"
//           onClick={closeModal}
//           className="text-error absolute top-[5px] right-[5px]"
//         >
//           <svg
//             xmlns="http://www.w3.org/2000/svg"
//             className="h-5 w-5"
//             viewBox="0 0 20 20"
//             fill="currentColor"
//           >
//             <path
//               fillRule="evenodd"
//               d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
//               clipRule="evenodd"
//             />
//           </svg>
//         </button>

//         <OrderStatus
//           setIsOpen={setIsOpen}
//           orderDetails={orderDetails}
//         ></OrderStatus>
//       </Modal>

//       <Modal
//         isOpen={modalIsOpenStock}
//         className="mymodal order"
//         overlayClassName="myoverlay"
//       >
//         <button
//           type="button"
//           onClick={closeModalStock}
//           className="text-error absolute top-[5px] right-[5px]"
//         >
//           <svg
//             xmlns="http://www.w3.org/2000/svg"
//             className="h-5 w-5"
//             viewBox="0 0 20 20"
//             fill="currentColor"
//           >
//             <path
//               fillRule="evenodd"
//               d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
//               clipRule="evenodd"
//             />
//           </svg>
//         </button>

//         <OutOfStock
//           setIsOpen={setIsOpenStock}
//           id={outOfStockId?.id}
//           noOfQuantity={outOfStockId?.orderQuantity}
//         ></OutOfStock>
//       </Modal>

//       <Table
//         tableName="Orders/s"
//         tableDataAll={tableDataAll}
//         setSkipTableDataAll={setSkipTableDataAll}
//         isLoadingAll={isFetchingAllBook}
//         columns={columns}
//         data={tableData}
//         fetchData={getData}
//         isFetchError={isErrorBook}
//         isLoadingData={isFetchingBook}
//         pageCount={pageCount}
//         defaultPageSize={10}
//         totalData={totalData}
//         rowOptions={[...new Set([10, 20, 30, totalData])]}
//         viewRoute="book"
//         deleteQuery={deleteItems}
//         isDeleting={isDeleting}
//         isDeleteError={isDeleteError}
//         isDeleteSuccess={isDeleteSuccess}
//         DeletedError={DeletedError}
//         hasExport={true}
//         addPage={{
//           page: "Delivered Product",
//           route: "/admin/delivered-product",
//         }}
//       />
//     </div>
//   );
// }

// export default AdminBook;
