import { useState, useEffect, useMemo } from "react";

import Table from "components/Table/table";

import {
  useDeleteBookedListMutation,
  // useReadAllBookInvQuery,
  // useReadBookInvQuery,
  useReadDeliveredProductQuery,
} from "services/api/seller";
import { getQueryStringForBook } from "utils/getQueryStringForTable";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
// import Button from "components/Button";

import { DateColumnFilter, SelectColumnFilter } from "components/Table/Filter";
import {
  color,
  // orderStatus
} from "constant/constant";
import Button from "components/Button";
// import { badgeColorFun } from "utils/applicationUtils/commonUtils";
import Modal from "react-modal";
import AccountClose from "container/AccountClose/AccountClose";

function DeliveredProductList() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [bookQuery, setBookQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const [orderDetails, setOrderDetails] = useState("");

  const [accountCloseId, setAccountCloseId] = useState("");
  const [accountCloseDetails, setAccountCloseDetails] = useState("");

  const [isOpenAccountClose, setIsOpenAccountClose] = useState(false);
  const closeModalStock = () => {
    setIsOpenAccountClose(false);
  };

  const {
    data: dataBook,
    isError: isErrorBook,
    isFetching: isFetchingBook,
  } = useReadDeliveredProductQuery(bookQuery);

  const { data: dataAllBook, isFetching: isFetchingAllBook } =
    useReadDeliveredProductQuery(`?&sortBy=createdAt&sortOrder=-1`, {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (dataAllBook) {
      setTableDataAll(
        dataAllBook.docs.map((value) => {
          return {
            id: value.id,
            productName: value.productName,
            bikeDriven: value.bikeDriven,
            numViews: value.numViews,
            size: value.size,
            color: value.color,
            orderQuantity: value.quantity,
            totalPrice: value.totalPrice,
            shippingCharge: value.shippingCharge,
            deliveryStatus: value.deliveryStatus,
            approveMessages: value.approveMessages,
            cancelMessages: value.cancelMessages,
            deliveryPersonId: value?.onTheWayDetail?.deliveryPersonId,
            deliveryCompanyId: value?.onTheWayDetail?.companyId,
            notInStock: value?.notInStock,
            deliveryEnd: value?.deliveryEnd,
            accountCloseMessage: value?.accountCloseMessage,
            totalClosingPrice: value?.totalClosingPrice,
            totalClosingShippingCharge: value?.totalClosingShippingCharge,
          };
        })
      );
    }
  }, [dataAllBook]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteBookedListMutation();

  const columns = useMemo(
    () => [
      {
        id: "id",
        Header: "id",
        accessor: "id",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "productName",
        Header: "Product Title",
        accessor: "productName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "size",
        Header: "Size",
        accessor: "size",
        Cell: ({ cell: { value } }) => value || "-",
        // canBeSorted: true,
        // canBeFiltered: true,
      },

      {
        id: "color",
        Header: "Color",
        accessor: "color",
        Cell: ({ cell: { value } }) => value || "-",
        Filter: SelectColumnFilter,

        possibleFilters: color.map((value) => {
          return {
            label: value,
            value: value,
          };
        }),

        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "orderQuantity",
        Header: "Order Quantity",
        accessor: "orderQuantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "totalPrice",
        Header: " Total Price (NRs.)",
        accessor: "totalPrice",
        Cell: ({ cell: { value } }) => thousandNumberSeparator(value) || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "shippingCharge",
        Header: "Shipping Charge (NRs.)",
        accessor: "shippingCharge",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "totalClosingPrice",
        Header: "Total Closing Price",
        accessor: "totalClosingPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "totalClosingShippingCharge",
        Header: "Total Closing Shipping Charge",
        accessor: "totalClosingShippingCharge",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "accountCloseMessage",
        Header: "Account CloseMessage",
        accessor: "accountCloseMessage",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "deliveryEnd",
        Header: "Delivery Date",
        accessor: "deliveryEnd",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "closeAccount",
        Header: "Account Close",
        accessor: "closeAccount",
        Cell: ({ row: { original }, cell: { value } }) => {
          return (
            <Button
              onClick={() => {
                setAccountCloseId(original?.id);
                setAccountCloseDetails({
                  accountCloseMessage: original.accountCloseMessage || "",
                  totalClosingPrice:
                    original.totalClosingPrice || `${original.totalPrice}`,
                  totalClosingShippingCharge:
                    original.totalClosingShippingCharge ||
                    `${original.shippingCharge}`,
                });
                setIsOpenAccountClose(true);
              }}
            >
              {original.totalClosingPrice ? "Update" : "Account Close"}
            </Button>
          );
        },
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const newFilters = [
      ...filters,
      { id: "deliveryStatus", value: "Delivered" },
    ];
    const query = getQueryStringForBook(
      pageIndex,
      pageSize,
      sortBy,
      newFilters
    );
    setBookQuery(query);
  };

  useEffect(() => {
    if (dataBook) {
      setPageCount(dataBook.totalPages);
      setTotalData(dataBook.totalDocs);
      setTableData(
        dataBook.docs.map((value) => {
          return {
            id: value.id,
            productName: value.productName,
            bikeDriven: value.bikeDriven,
            numViews: value.numViews,
            size: value.size,
            color: value.color,
            orderQuantity: value.quantity,
            totalPrice: value.totalPrice,
            shippingCharge: value.shippingCharge,
            deliveryStatus: value.deliveryStatus,
            approveMessages: value.approveMessages,
            cancelMessages: value.cancelMessages,
            deliveryPersonId: value?.onTheWayDetail?.deliveryPersonId,
            deliveryCompanyId: value?.onTheWayDetail?.companyId,
            notInStock: value?.notInStock,
            deliveryEnd: value?.deliveryEnd,
            accountCloseMessage: value?.accountCloseMessage,
            totalClosingPrice: value?.totalClosingPrice,
            totalClosingShippingCharge: value?.totalClosingShippingCharge,
          };
        })
      );
    }
  }, [dataBook]);
  return (
    <div>
      <h3 className="mb-3">Delivered Products</h3>

      <Modal
        isOpen={isOpenAccountClose}
        className="mymodal "
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={closeModalStock}
          className="text-error absolute top-[5px] right-[5px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>

        <AccountClose
          setIsOpen={setIsOpenAccountClose}
          id={accountCloseId}
          accountCloseDetails={accountCloseDetails}
        ></AccountClose>
      </Modal>

      <Table
        tableName="Orders/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllBook}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorBook}
        isLoadingData={isFetchingBook}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        viewRoute="book"
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
      />
    </div>
  );
}

export default DeliveredProductList;
