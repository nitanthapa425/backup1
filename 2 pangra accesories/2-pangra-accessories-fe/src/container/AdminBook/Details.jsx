import { useReadSingleOrderDetailsQuery } from "services/api/seller";
import { useRouter } from "next/router";
import Link from "next/link";
import { ArrowRightIcon } from "@heroicons/react/solid";
import { badgeColorFun } from "utils/applicationUtils/commonUtils";

const OrderedItemDetail = () => {
  const router = useRouter();

  const { data: dataOrder } = useReadSingleOrderDetailsQuery(router.query.id);

  return (
    <section className="brand-detail-section">
      <div className="flex justify-end"></div>
      <div className="container">
        <h1 className="h3 mb-5">Ordered Item Details</h1>
        <div className="row ">
          <div className="two-col">
            <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
              <div className="text-center">
                <h1 className="h4">General Details</h1>
              </div>
              <p>
                <span className="font-bold">Product Name:</span>&nbsp;{" "}
                {dataOrder?.productName || "N/A"}
              </p>
              <p>
                <span className="font-bold">Color:</span>&nbsp;
                {dataOrder?.color || "N/A"}
              </p>
              <p>
                <span className="font-bold">Size :</span>&nbsp;
                {dataOrder?.size || "N/A"}
              </p>
              <p>
                <span className="font-bold">Discounted Price:</span>&nbsp; NRs.{" "}
                {dataOrder?.discountedPrice || "N/A"}
              </p>
              <p>
                <span className="font-bold">Shipping Fee :</span>&nbsp; NRs.{" "}
                {dataOrder?.shippingCharge || "N/A"}
              </p>
              <p>
                <span className="font-bold">Quantity :</span>&nbsp;
                {dataOrder?.quantity || "N/A"}
              </p>
              <p>
                <span className="font-bold">Total Shipping Fee :</span>&nbsp;
                NRs. {dataOrder?.totalShippingCharge || "N/A"}
              </p>
              <p>
                <span className="font-bold">Total Price :</span>&nbsp;
                {dataOrder?.totalPrice || "N/A"}
              </p>

              <p>
                <span className="font-bold">Delivery Status:</span>&nbsp;
                <span className={badgeColorFun(dataOrder?.deliveryStatus)}>
                  {dataOrder?.deliveryStatus || "N/A"}
                </span>{" "}
              </p>
              {dataOrder?.deliveryStatus === "On The Way" ? (
                <>
                  {" "}
                  <p>
                    <span className="font-bold">Delivery Person:</span>&nbsp;
                    {dataOrder?.onTheWayDetail?.deliveryPersonName}
                    {` (${dataOrder?.onTheWayDetail?.deliveryPersonEmail})` ||
                      "N/A"}
                  </p>
                  <p>
                    <span className="font-bold">Delivery Company:</span>&nbsp;
                    {dataOrder?.onTheWayDetail?.companyName || "N/A"}
                  </p>
                </>
              ) : null}

              <p>
                <span className="font-bold">Delivery Start Time:</span>&nbsp;
                {new Date(dataOrder?.deliveryStart).toLocaleString() || "N/A"}
              </p>
              <p>
                <span className="font-bold">Expected Delivery End Time:</span>
                &nbsp;
                {new Date(dataOrder?.deliveryEnd).toLocaleString() || "N/A"}
              </p>
              <Link
                disabled
                href={`/admin/product/${dataOrder?.accessoriesId}`}
              >
                <a className="text-blue-600  hover:text-primary">
                  Product Details
                </a>
              </Link>

              <div>
                <Link
                  disabled
                  href={`/customer/details/${dataOrder?.createdBy}`}
                >
                  <a className="text-blue-600  hover:text-primary">
                    Customer Details
                  </a>
                </Link>
              </div>
            </div>
          </div>
          <div className="two-col">
            <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
              <div className="text-center">
                <h1 className="h4">Shipping Address</h1>
              </div>
              <p>
                <span className="font-bold">Shipping Address:</span>&nbsp;{" "}
                {dataOrder?.orderGroupId?.shippingAddressLocation?.combineLocation
                  ?.split(">")
                  .join(",") || "N/A"}
              </p>
              <p>
                <span className="font-bold">Exact Location:</span>&nbsp;
                {dataOrder?.orderGroupId?.shippingAddressLocation
                  ?.exactLocation || "N/A"}
              </p>
              <p>
                <span className="font-bold">Nearby Location:</span>&nbsp;
                {dataOrder?.orderGroupId?.shippingAddressLocation
                  ?.nearByLocation || "N/A"}
              </p>
              <p>
                <span className="font-bold">Shipping Fee :</span>&nbsp;
                {dataOrder?.shippingFee || "N/A"}
              </p>
              <p>
                <span className="font-bold">Mobile Number :</span>&nbsp;
                {dataOrder?.orderGroupId?.shippingAddressLocation?.mobile ||
                  "N/A"}
              </p>
            </div>
          </div>

          {dataOrder?.approveMessages?.length <= 0 ? null : (
            <div className="two-col">
              <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                <h1 className="h4 font-bold">Approve Message</h1>
                {dataOrder?.approveMessages.map((value, i) => {
                  return (
                    <div
                      key={i}
                      className="border border-gray-200 pt-3 pb-2 px-4 rounded-md mb-4"
                    >
                      <div>
                        <p>{new Date(value?.approveAt).toLocaleString()}</p>
                        <div className="flex items-center">
                          <p className="mr-4 font-bold">Approved By:</p>
                          <div title="More">
                            <Link
                              disabled
                              href={`/admin/user/${value?.approvedBy}`}
                            >
                              <a className="text-blue-600  hover:text-primary">
                                <ArrowRightIcon className="w-5"></ArrowRightIcon>
                              </a>
                            </Link>
                          </div>
                        </div>
                      </div>
                      <div className="break-words">
                        <span className="font-bold ">Message: </span>{" "}
                        {value?.approveMessage || "N/A"}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          )}

          {!Object.keys(dataOrder?.deliveredDetail || {}).length ? null : (
            <div className="two-col">
              <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                <h1 className="h4">Delivered Detail</h1>

                <div className=" pt-3 pb-2 px-4 rounded-md mb-4">
                  <div>
                    <p>
                      <span className="font-bold ">Delivered At: </span>
                      {new Date(
                        dataOrder?.deliveredDetail?.assignAt
                      ).toLocaleString()}
                    </p>
                    <p className="mr-4">
                      <span className="font-bold">Mobile: </span>
                      <span>
                        {dataOrder?.deliveredDetail?.deliveryPersonMobile}
                      </span>
                    </p>

                    <div className="flex items-center">
                      <p className="mr-4">
                        <span className="font-bold">Delivered By: </span>
                        <span>
                          {dataOrder?.deliveredDetail?.deliveryPersonName}
                        </span>
                      </p>

                      <div title="More">
                        <Link
                          disabled
                          href={`/admin/user/${dataOrder?.deliveredDetail?.deliveryPersonId}`}
                        >
                          <a className="text-blue-600  hover:text-primary">
                            <ArrowRightIcon className="w-5"></ArrowRightIcon>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          {!dataOrder?.cancelMessages?.cancelMessage ? null : (
            <div className="two-col">
              <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                <h1 className="h4">Cancelled Message</h1>

                <div className=" pt-3 pb-2 px-4 rounded-md mb-4">
                  <div>
                    <p>
                      {new Date(
                        dataOrder?.cancelMessages?.cancelledAt
                      ).toLocaleString()}
                    </p>
                    <div className="flex items-center">
                      <p className="mr-4 font-bold">Cancelled By</p>
                      <div title="More">
                        <Link
                          disabled
                          href={`/admin/user/${dataOrder?.cancelMessages?.cancelledBy}`}
                        >
                          <a className="text-blue-600  hover:text-primary">
                            <ArrowRightIcon className="w-5"></ArrowRightIcon>
                          </a>
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="break-words">
                    <span className="font-bold">Message: </span>{" "}
                    {dataOrder?.cancelMessages?.cancelMessage || ""}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default OrderedItemDetail;
