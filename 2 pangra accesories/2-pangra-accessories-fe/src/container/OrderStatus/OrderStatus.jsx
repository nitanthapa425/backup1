import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import { useToasts } from "react-toast-notifications";

import Textarea from "components/Input/textarea";
import {
  useCreateOrderStatusMutation,
  useDeliveryCompanyQuery,
  useDeliveryPersonQuery,
} from "services/api/seller";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { orderStatus, orderStatusSuperAdmin } from "constant/constant";
import * as yup from "yup";
import { useSelector } from "react-redux";

const OrderStatus = ({ orderDetails, setIsOpen }) => {
  const {
    id,
    deliveryStatus,
    approveMessages,
    cancelMessages,
    deliveryPersonId,
    deliveryCompanyId,
  } = orderDetails;

  const loginInfo = useSelector((state) => state.adminAuth);
  const user = loginInfo?.user?.accessLevel;

  const formikBag = useRef();
  const { addToast } = useToasts();
  const [orderInitialValue, setOrderInitialValue] = useState({});

  const [CompanyId, setCompanyId] = useState(deliveryCompanyId);

  const [changeOrderStatus, { isError, error, isSuccess, isLoading }] =
    useCreateOrderStatusMutation();

  useEffect(() => {
    let newApproveMessage = "";
    if (deliveryStatus === "Processing") {
      newApproveMessage = approveMessages.at(-1).approveMessage;
    } else if (deliveryStatus === "Cancelled") {
      newApproveMessage = cancelMessages?.cancelMessage;
    }

    const newOrderStatus = {
      message: newApproveMessage,
      orderStatus: deliveryStatus,
      deliveryPersonId: deliveryPersonId || "",
      deliveryCompanyId: deliveryCompanyId || "",
    };
    setOrderInitialValue(newOrderStatus);
  }, []);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast("You have successfully changed the order status", {
        appearance: "success",
      });

      setIsOpen(false);
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to change order status . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  const { data: deliveryPerson, isFetching: isFetchingDeliveryPersion } =
    useDeliveryPersonQuery(CompanyId, {
      skip: !CompanyId,
    });

  const { data: deliveryCompany, isFetching: isFetchingDeliveryCompany } =
    useDeliveryCompanyQuery();

  // useDeliveryCompanyQuery;

  //   console.log("deliveryPerson", );

  const orderValidation = yup.object({
    message: yup.string().when("orderStatus", {
      is: "Cancelled",
      then: yup
        .string()
        .min(2, "Message must bet at least 2 characters")
        .max(150, "Message must be at must 150 characters.")
        .required(`Message is required.`),
      otherwise: yup.string(),
    }),
    deliveryPersonId: yup.string().when("orderStatus", {
      is: "On The Way",
      then: yup.string().required(`Delivery person is required.`),
      otherwise: yup.string(),
    }),
    deliveryCompanyId: yup.string().when("orderStatus", {
      is: "On The Way",
      then: yup.string().required(`Delivery company is required.`),
      otherwise: yup.string(),
    }),
  });

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    let payload = {};
    let query = "";

    if (values.orderStatus === "Ordered") {
      payload = {
        orderStatus: values.orderStatus,
      };
      query = "?orderStatus=Ordered";
    } else if (values.orderStatus === "Processing") {
      payload = {
        orderStatus: values.orderStatus,
        approveMessage: values.message,
      };
      query = "?orderStatus=Processing";
    } else if (values.orderStatus === "On The Way") {
      payload = {
        orderStatus: values.orderStatus,
        deliveryPersonId: values.deliveryPersonId,
      };
      query = "?orderStatus=On The Way";
    } else if (values.orderStatus === "Delivered") {
      payload = {
        orderStatus: values.orderStatus,
        approveMessage: values.message,
      };
      query = "?orderStatus=Delivered";
    } else if (values.orderStatus === "Cancelled") {
      payload = {
        orderStatus: values.orderStatus,
        cancelMessage: values.message,
      };
      query = "?orderStatus=Cancelled";
    }

    changeOrderStatus({ body: payload, id: id, query: query });
    setSubmitting(false);
  };

  const optionStatus = () => {
    let isDisabled = false;
    let status = [];
    if (user === "SUPER_ADMIN") {
      status = orderStatusSuperAdmin;
    } else {
      status = orderStatus;
    }

    const option = status?.map((value) => {
      isDisabled = false;

      if (deliveryStatus === "Ordered") {
        if (value === "On The Way") {
          isDisabled = true;
        }
      }

      if (deliveryStatus === "Processing") {
        if (value === "Ordered") {
          isDisabled = true;
        }
      }

      if (deliveryStatus === "On The Way") {
        if (value === "Ordered" || value === "Processing") {
          isDisabled = true;
        }
      }
      if (deliveryStatus === "Delivered") {
        if (
          value === "Ordered" ||
          value === "Processing" ||
          value === "On The Way" ||
          value === "Cancelled"
        ) {
          isDisabled = true;
        }
      }

      if (deliveryStatus === "Cancelled") {
        if (
          value === "Processing" ||
          value === "On The Way" ||
          value === "Ordered" ||
          value === "Delivered"
        ) {
          isDisabled = true;
        }
      }

      return {
        label: value,
        value: value,
        isDisabled: isDisabled,
      };
    });

    return option;
  };

  return (
    <>
      <h3 className="h5 mb-2">Order Status </h3>
      <Formik
        initialValues={orderInitialValue}
        onSubmit={handleOnSubmit}
        validationSchema={orderValidation}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
          setFieldTouched,
        }) => (
          <Form>
            <div className="form-holder">
              <div className="w-full mb-5">
                <SelectWithoutCreate
                  required
                  label="Select Status"
                  placeholder="Location"
                  value={values?.orderStatus || null}
                  onChange={(selectedValue) => {
                    setFieldValue("orderStatus", selectedValue.value);
                  }}
                  options={optionStatus()}
                  isDisabled={
                    deliveryStatus === "Cancelled" ||
                    deliveryStatus === "Delivered"
                  }
                />
              </div>

              {values.orderStatus === "Cancelled" ||
              values.orderStatus === "Processing" ||
              values.orderStatus === "Delivered" ? (
                <div>
                  <Textarea
                    label="Message"
                    required={values.orderStatus === "Cancelled"}
                    name="message"
                    type="text"
                    placeholder="E.g. The order has been confirm."
                    disabled={
                      deliveryStatus === "Cancelled" ||
                      deliveryStatus === "Delivered"
                    }
                    style={
                      deliveryStatus === "Cancelled"
                        ? {
                            backgroundColor: "#f2f2f2",
                            border: "solid #e6e7e9 1px",
                            color: "#a2a0a4",
                          }
                        : {}
                    }
                  />
                </div>
              ) : null}

              {values.orderStatus === "On The Way" ? (
                <div>
                  <div className="w-full mb-5">
                    <SelectWithoutCreate
                      required
                      loading={isFetchingDeliveryCompany}
                      label="Select Delivery Company"
                      placeholder="Delivery Company"
                      error={
                        touched?.deliveryCompanyId
                          ? errors?.deliveryCompanyId
                          : ""
                      }
                      value={values?.deliveryCompanyId || null}
                      onChange={(selectedValue) => {
                        setFieldValue("deliveryCompanyId", selectedValue.value);
                        setCompanyId(selectedValue.value);
                      }}
                      options={deliveryCompany?.docs?.map((value) => ({
                        label: value.companyName,
                        value: value._id,
                      }))}
                      onBlur={() => {
                        setFieldTouched("deliveryCompanyId");
                      }}
                    />
                  </div>
                  <div className="w-full mb-5">
                    <SelectWithoutCreate
                      required
                      isDisabled={!values.deliveryCompanyId}
                      loading={isFetchingDeliveryPersion}
                      label="Select Delivery Person"
                      placeholder="Delivery Person"
                      error={
                        touched?.deliveryPersonId
                          ? errors?.deliveryPersonId
                          : ""
                      }
                      value={values?.deliveryPersonId || null}
                      onChange={(selectedValue) => {
                        setFieldValue("deliveryPersonId", selectedValue.value);
                      }}
                      options={deliveryPerson?.map((value) => ({
                        label: `${value.fullName} (${value.email})`,
                        value: value.id,
                      }))}
                      onBlur={() => {
                        setFieldTouched("deliveryPersonId");
                      }}
                    />
                  </div>
                </div>
              ) : (
                ""
              )}

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Change
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
      {/* </div> */}
    </>
  );
};

export default OrderStatus;
