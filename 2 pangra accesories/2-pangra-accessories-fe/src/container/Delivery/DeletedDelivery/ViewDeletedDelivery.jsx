import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForUserTable } from "utils/getQueryStringForTable";

import {
  useActiveDeletedDeliveryMutation,
  useReadSingleDeletedDeliveryQuery,
  // useDeleteDeletedUserMutation,
} from "services/api/adminDelivery/delivery";
import { SelectColumnFilter } from "components/Table/Filter";

function ViewDeletedDeliveryContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [deletedUserDataQuery, setDeletedUserDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  const {
    data: DeletedUserData,
    isError: userError,
    isFetching: isLoadingUser,
  } = useReadSingleDeletedDeliveryQuery(deletedUserDataQuery);
  const {
    data: deletedUsersDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadSingleDeletedDeliveryQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (deletedUsersDataAll) {
      setTableDataAll(
        deletedUsersDataAll.docs.map((users) => {
          return {
            userName: users?.userName,
            accessLevel: users?.accessLevel,
            password: users?.password,
            // firstName: users?.firstName,
            // lastName: users?.lastName,
            // middleName: users?.middleName,
            fullName: users?.fullName,
            email: users?.email,
            mobile: users?.mobile,
            dob: users?.dob,
            gender: users?.gender,
            _id: users?._id,
            verified: users?.verified ? "Verified" : "Unverified",
          };
        })
      );
    }
  }, [deletedUsersDataAll]);

  const [
    activeDeleteItems,
    {
      isLoading: isActivating,
      isSuccess: isActivatedSuccess,
      isError: isActivatedError,
      error: activatedError,
    },
  ] = useActiveDeletedDeliveryMutation();

  const columns = useMemo(
    () => [
      {
        id: "userName",
        Header: "User Name",
        accessor: "userName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "accessLevel",
        Header: "Access Level",
        accessor: "accessLevel",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "USER",
            value: "USER",
          },
          {
            label: "ADMIN",
            value: "ADMIN",
          },
          {
            label: "SUPER_ADMIN",
            value: "SUPER_ADMIN",
          },
        ],
      },
      {
        id: "email",
        Header: "Email",
        accessor: "email",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "mobile",
        Header: "Mobile",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "dob",
        Header: "Date of birth",
        accessor: "dob",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "gender",
        Header: "Gender",
        accessor: "gender",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Male",
            value: "Male",
          },
          {
            label: "Female",
            value: "Female",
          },
          {
            label: "Other",
            value: "Other",
          },
        ],
      },
      {
        id: "verified",
        Header: "Status",
        accessor: "verified",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Verified",
            value: true,
          },
          {
            label: "Unverified",
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForUserTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setDeletedUserDataQuery(query);
  };

  useEffect(() => {
    if (DeletedUserData) {
      setPageCount(DeletedUserData.totalPages);
      setTotalData(DeletedUserData.totalDocs);

      setTableData(
        DeletedUserData.docs.map((users) => {
          return {
            userName: users?.userName,
            accessLevel: users?.accessLevel,
            password: users?.password,
            // firstName: users?.firstName,
            // lastName: users?.lastName,
            // middleName: users?.middleName,
            fullName: users?.fullName,
            email: users?.email,
            mobile: users?.mobile,
            dob: users?.dob,
            gender: users?.gender,
            id: users?.id,
            verified: users?.verified ? "Verified" : "Unverified",
          };
        })
      );
    }
  }, [DeletedUserData]);
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Deleted-DeliveryUser List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="Deleted DeliveryUser"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={userError}
            isLoadingData={isLoadingUser}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="signup/edit"
            viewRoute="admin/delivery/inactive"
            activeDeleteItems={activeDeleteItems}
            isActivating={isActivating}
            isActivatedError={isActivatedError}
            isActivatedSuccess={isActivatedSuccess}
            activatedError={activatedError}
            revertButtonName="Activate User"
            revertMultipleButtonName="Activate Multiple User"
            // deleteQuery={deleteQuery}
            // isDeleting={isDeleting}
            // isDeleteError={isDeleteError}
            // isDeleteSuccess={isDeleteSuccess}
            // DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add User", route: "/admin/user/create" }}
            tableName="DeletedUser/s"
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewDeletedDeliveryContainer;
