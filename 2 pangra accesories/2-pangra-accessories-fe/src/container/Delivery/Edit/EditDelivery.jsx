import React, { useRef, useEffect, useState } from "react";

import { Form, Formik } from "formik";
import AdminLayout from "layouts/Admin";

import { useToasts } from "react-toast-notifications";
import {
  useReadSingleDeliveryQuery,
  useUpdateDeliveryMutation,
} from "services/api/adminDelivery/delivery";

import Input from "components/Input";

import Button from "components/Button";
import { UpdateSignUpValidationSchema } from "validation/signup.validation";

import Popup from "components/Popup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { useRouter } from "next/router";
import DropZone from "components/DropZone";
import { beforeAfterDate } from "utils/beforeAfterdate";
import { genders } from "constant/constant";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useReadCompanyQuery } from "services/api/company";
import Select from "components/Select";

const DeliveryEditComponent = () => {
  const router = useRouter();

  const [editUserInitialValue, setEditUserInitialValue] = useState({});

  const formikBag = useRef();
  const { addToast } = useToasts();

  const { data: companyData, isLoading: fetchingCompany } =
    useReadCompanyQuery();

  const [openModal, setOpenModal] = useState(false);

  const {
    data: userProfile,
    // error: userProfileFetchError,
  } = useReadSingleDeliveryQuery(router.query.id, {
    skip: !router.query.id,
  });

  const [
    updateSignUp,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
      // error: updateError,
    },
  ] = useUpdateDeliveryMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("User/Email already exists.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(updateSuccessData?.message || "User updated successfully.", {
        appearance: "success",
      });
      // setChanged(false);
      router.push(`/admin/delivery/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  useEffect(() => {
    if (userProfile) {
      const newUserDetails = {
        id: userProfile?._id,
        userName: userProfile?.userName,
        accessLevel: userProfile?.accessLevel,
        firstName: userProfile?.firstName,
        middleName: userProfile?.middleName,
        lastName: userProfile?.lastName,
        email: userProfile?.email,
        mobile: userProfile?.mobile,
        dob: userProfile?.dob,
        gender: userProfile?.gender,
        profileImagePath: userProfile?.profileImagePath,
        documentImagePath: userProfile?.documentImagePath,
        companyId: userProfile?.companyId,
      };

      setEditUserInitialValue(newUserDetails);
    }
  }, [userProfile]);

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      // console.log(formikBag.current.values);
      formikBag?.current?.setFieldValue("profileImagePath", {});
    }
  };
  const setNewFiles = (newFiles) => {
    const allFiles = [
      ...formikBag?.current?.values.documentImagePath,
      ...newFiles,
    ];
    formikBag?.current?.setFieldValue("documentImagePath", allFiles);
  };

  const handleRemoveFile = (idx) => {
    const currentFiles = [...formikBag?.current?.values.documentImagePath];
    currentFiles.splice(idx, 1);
    formikBag?.current?.setFieldValue("documentImagePath", currentFiles);
  };

  const BreadCrumbList = [
    {
      routeName: "Add vehicle",
      route: "/admin/product/create",
    },

    {
      routeName: "Add User",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle={"Edit Delivery user"}
      BreadCrumbList={BreadCrumbList}
    >
      <Formik
        initialValues={editUserInitialValue}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          updateSignUp(values);
          setSubmitting(false);
        }}
        validationSchema={UpdateSignUpValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          dirty,
        }) => (
          <div className="container">
            <h3 className="mb-3">{`Edit Delivery User Details`}</h3>

            <Form>
              <div className="row-sm  pt-5 pb-2">
                <div className="three-col-sm">
                  <Input
                    label="User Name"
                    name="userName"
                    type="text"
                    placeholder="E.g: john1"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder="E.g: John"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Middle Name"
                    name="middleName"
                    type="text"
                    placeholder="E.g: Jung"
                    required={false}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder="E.g: Deo"
                  />
                </div>

                {/* <div className="three-col-sm">
                  <Select label="Access Level" name="accessLevel">
                    <option value="">Select Access Level</option>

                    {accessLevels.map((accessLevel, i) => (
                      <option key={i} value={accessLevel}>
                        {accessLevel}
                      </option>
                    ))}
                  </Select>
                </div> */}

                <div className="three-col-sm">
                  <Input
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="E.g: john320@gmail.com"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    label="Phone Number"
                    name="mobile"
                    type="text"
                    placeholder="E.g: 98XXXXXXXX"
                  />
                </div>
                <div className="three-col-sm">
                  <label htmlFor="">
                    Date Of Birth(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={values?.dob ? new Date(values?.dob) : null}
                    onChange={(date) => {
                      setFieldValue("dob", date?.toLocaleDateString());
                      setFieldTouched("dob");
                    }}
                    onBlur={() => {
                      setFieldTouched("dob");
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    maxDate={beforeAfterDate(new Date(), 0, 0, -16)}
                  />
                  <h6 className="text-gray-500 text-xs font-thin">
                    age must be greater than 16
                  </h6>

                  {touched.dob && errors.dob && (
                    <div className="text-error">{errors.dob}</div>
                  )}
                </div>
                <div className="three-col-sm">
                  <Select label="Gender" name="gender">
                    <option value="">Select Gender</option>

                    {genders.map((gender, i) => (
                      <option key={i} value={gender}>
                        {gender}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={fetchingCompany}
                    label="Select Company"
                    placeholder="E.g: Tuki Logic"
                    error={touched?.companyId ? errors?.companyId : ""}
                    value={values.companyId}
                    onChange={(selectedCompany) => {
                      setFieldTouched("companyId");
                      setFieldValue("companyId", selectedCompany.value);
                    }}
                    options={companyData?.docs?.map((d) => ({
                      value: d?._id,
                      label: d?.companyName,
                    }))}
                  />
                </div>
                <div className="three-col-sm">
                  <div className="">
                    <div>
                      <DropZone
                        label="Upload Profile Image"
                        currentFiles={
                          Object.keys(values.profileImagePath || {}).length
                            ? [values.profileImagePath]
                            : []
                        }
                        setNewFiles={handleProfileImagePath}
                        handleRemoveFile={handleRemoveProfileImagePath}
                        error={
                          touched?.profileImagePath
                            ? errors?.profileImagePath
                            : ""
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="three-col-sm">
                  <DropZone
                    label="Document/s Image"
                    required
                    currentFiles={values.documentImagePath}
                    setNewFiles={setNewFiles}
                    handleRemoveFile={handleRemoveFile}
                    error={
                      touched?.documentImagePath
                        ? errors?.documentImagePath
                        : ""
                    }
                  />
                </div>
              </div>

              <div className="btn-holder mt-2">
                <Button type="submit" loading={updating} disabled={!dirty}>
                  Update
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      // setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </AdminLayout>
  );
};

export default DeliveryEditComponent;
