import { useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Form, Formik } from "formik";

import { loginPagevalidationSchema } from "validation/login.validation";
import Input from "components/Input";
import Button from "components/Button";
import Alert from "components/Alert";
import { checkEmailValidity } from "validation/yupValidations";
import PasswordInput from "components/Input/PasswordInput";
import { useToasts } from "react-toast-notifications";
import { useDeliveryLoginMutation } from "services/api/deliveryLogin";

const initialValues = {
  email: "",
  password: "",
};

const DeliveryLoginForm = () => {
  const router = useRouter();
  const { addToast } = useToasts();
  const [loginDelivery, { isSuccess, isError, isLoading, error }] =
    useDeliveryLoginMutation();

  useEffect(() => {
    if (isSuccess) {
      router.push("/deliveryItem");
      addToast("Login success.", {
        appearance: "success",
      });
    }
  }, [isSuccess]);

  const handleSubmit = async (values, actions) => {
    const isEmail = await checkEmailValidity(values.email);
    if (isEmail) {
      loginDelivery({
        email: values.email,
        password: values.password,
      });
    } else {
      loginDelivery({
        userName: values.email,
        password: values.password,
      });
    }
    actions.setSubmitting(false);
  };

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={loginPagevalidationSchema}
      enableReinitialize
    >
      <Form>
        <div className="w-full mb-2">
          <Input
            label="Email/Username"
            name="email"
            type="text"
            placeholder="Email/Username"
          />
        </div>
        <PasswordInput
          label="Password"
          name="password"
          placeholder="Password"
        ></PasswordInput>
        <div className="mt-3">
          <Button loading={isLoading} type="submit" className="btn-lg">
            Login
          </Button>
          <Button disabled={isLoading} type="reset">
            Clear
          </Button>
          <div className="mt-3">
            <Link href="/delivery/forgot-password">
              <a className="text-textColor">Forgot Password?</a>
            </Link>
          </div>
        </div>
        {isError && (
          <div className="max-w-xs mt-3">
            <Alert
              message={error?.data?.message || "Error logging in."}
              type="error"
            />
          </div>
        )}
      </Form>
    </Formik>
  );
};
export default DeliveryLoginForm;
