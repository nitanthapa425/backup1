import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import {
  // getQueryStringForTable,
  getQueryStringForUserTable,
} from "utils/getQueryStringForTable";

import {
  // useReadUserQuery,
  // useDeleteUserMutation,
  useReadAllDeliveryQuery,
  useReadDeletedDeliveryMutation,
} from "services/api/adminDelivery/delivery";
import {
  DateColumnFilter,
  MultiSelectFilter,
  SelectColumnFilter,
} from "components/Table/Filter";
// import { role } from "constant/constant";

const ViewDeliveryContainer = () => {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [UserDataQuery, setUserDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  const {
    data: userData,
    isError: userError,
    isFetching: isLoadingUser,
  } = useReadAllDeliveryQuery(UserDataQuery);

  const {
    data: userDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllDeliveryQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (userDataAll) {
      setTableDataAll(
        userDataAll.docs.map((users) => {
          return {
            userName: users?.userName,
            // accessLevel: users?.accessLevel,
            password: users?.password,
            companyName: users?.companyName,
            // firstName: users?.firstName,
            // lastName: users?.lastName,
            // middleName: users?.middleName,
            fullName: users?.fullName,
            email: users?.email,
            mobile: users?.mobile,
            dob: users?.dob,
            gender: users?.gender,
            id: users?.id,
            verified: users?.verified ? "Verified" : "Unverified",
          };
        })
      );
    }
  }, [userDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useReadDeletedDeliveryMutation();

  const columns = useMemo(
    () => [
      {
        id: "userName",
        Header: "User Name",
        accessor: "userName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "companyName",
        Header: "Delivery Company Name",
        accessor: "companyName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      // {
      //   id: "accessLevel",
      //   Header: "Access Level",
      //   accessor: "accessLevel",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      //   // Filter: SelectColumnFilter,
      //   Filter: MultiSelectFilter,
      //   possibleFilters: [
      //     {
      //       label: role.user,
      //       value: role.user,
      //     },
      //     {
      //       label: role.admin,
      //       value: role.admin,
      //     },
      //     {
      //       label: role.super_admin,
      //       value: role.super_admin,
      //     },
      //   ],
      // },
      {
        id: "email",
        Header: "Email",
        accessor: "email",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "mobile",
        Header: "Mobile",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "dob",
        Header: "Date of birth",
        accessor: "dob",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "gender",
        Header: "Gender",
        accessor: "gender",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        // Filter: SelectColumnFilter,
        Filter: MultiSelectFilter,
        possibleFilters: [
          {
            label: "Male",
            value: "Male",
          },
          {
            label: "Female",
            value: "Female",
          },
          {
            label: "Other",
            value: "Other",
          },
        ],
      },
      {
        id: "verified",
        Header: "Status",
        accessor: "verified",
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Verified",
            value: true,
          },
          {
            label: "Unverified",
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForUserTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setUserDataQuery(query);
  };

  useEffect(() => {
    if (userData) {
      setPageCount(userData.totalPages);
      setTotalData(userData.totalDocs);

      setTableData(
        userData.docs.map((users) => {
          return {
            userName: users?.userName,
            // accessLevel: users?.accessLevel,
            password: users?.password,
            companyName: users?.companyName,
            // firstName: users?.firstName,
            // lastName: users?.lastName,
            // middleName: users?.middleName,
            fullName: users?.fullName,
            email: users?.email,
            mobile: users?.mobile,
            dob: users?.dob,
            gender: users?.gender,
            id: users?.id,
            verified: users?.verified ? "Verified" : "Unverified",
          };
        })
      );
    }
  }, [userData]);
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Delivery user List",
      route: "",
    },
  ];

  return (
    <AdminLayout documentTitle="View User" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Table
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={userError}
            isLoadingData={isLoadingUser}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="delivery/edit"
            viewRoute="admin/delivery"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add User", route: "/admin/user/create" }}
            tableName="Delivery/s"
          />
        </div>
      </section>
    </AdminLayout>
  );
};

export default ViewDeliveryContainer;
