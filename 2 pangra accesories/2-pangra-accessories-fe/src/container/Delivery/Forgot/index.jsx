import React, { useRef, useEffect } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import Input from "components/Input";
import { useToasts } from "react-toast-notifications";
import { useDeliveryForgotPasswordMutation } from "services/api/forgotPassword";
import { emailLinkSchema } from "validation/resetLink.validation";
import Link from "next/link";
const DeliveryForgotPasswordContainer = () => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [
    createForgotPassword,
    { isError, error, isLoading, isSuccess, dataCreateForgotPassword },
  ] = useDeliveryForgotPasswordMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataCreateForgotPassword?.message ||
          "The reset link has been sent to your email address.",
        {
          appearance: "success",
        }
      );
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "you might have entered wrong email . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);
  return (
    <>
      <Formik
        initialValues={{
          email: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          createForgotPassword(values);
          setSubmitting(false);
        }}
        validationSchema={emailLinkSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">Forgot Password</h3>
                  <p className="text-lg">
                    Enter your email address and we will send you a link to
                    reset your password
                  </p>
                  <Input
                    label="Email"
                    name="email"
                    type="text"
                    placeholder="Email"
                  />

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Send
                    </Button>
                    <Button
                      type="button"
                      variant="outlined-error"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Clear
                    </Button>
                    <Link href="/admin">
                      <Button type="button" variant="link">
                        Back
                      </Button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default DeliveryForgotPasswordContainer;
