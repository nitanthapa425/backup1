import Table from "components/Table/table";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import { useReadBrandListQuery } from "services/api/BrandService";
import { useToasts } from "react-toast-notifications";
import {
  useDeleteShippingAddressMutation,
  useReadShippingAddressQuery,
} from "services/api/customer";
import ShippingCom from "./ShippingCom";
import Modal from "react-modal";
import Popup from "components/Popup";
import { PencilIcon } from "@heroicons/react/outline";
import { TrashIcon } from "@heroicons/react/solid";
import { firstLetterCapital } from "utils/firstLetterCapita.";
import Button from "components/Button";
import { useDispatch, useSelector } from "react-redux";
import { setAddress } from "store/features/book/BookSlice";

function ShippingAddressTable({
  hiddenColumns = ["id", "verify", "approved", "sold", "meetingSchedule"],
}) {
  const { addToast } = useToasts();

  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [, setshippingAddressDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const [editId, setId] = useState("");

  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  const [modalIsOpenn, setIsOpenn] = useState(false);
  const closeModall = () => {
    setIsOpenn(false);
  };

  const [openModal, setOpenModal] = useState(false);

  const [
    deleteAddress,
    {
      isError: deleteIsError,
      isSuccess: deleteIsSuccess,
      error: deleteError,
      data: dataDeleteAddress,
    },
  ] = useDeleteShippingAddressMutation();

  const dispatch = useDispatch();

  const book = useSelector((state) => state.book);

  useEffect(() => {
    if (deleteIsSuccess) {
      addToast(
        dataDeleteAddress?.message ||
          "Shipping Address has been deleted successfully.",
        {
          appearance: "success",
        }
      );
      refetch();
    }
    if (deleteIsError) {
      addToast(deleteError?.data?.message, {
        appearance: "error",
      });
    }
  }, [deleteIsSuccess, deleteIsError]);
  const {
    data: shippingAddressData,
    isError: shippingAddressIsError,
    isFetching: shippingAddressIsFetching,
    refetch,
  } = useReadShippingAddressQuery();

  const { data: brandDataAll, isFetching: isLoadingAll } =
    useReadBrandListQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (brandDataAll) {
      setTableDataAll(
        brandDataAll.docs.map((brand) => {
          return {
            id: brand?.id,
            brandName: brand?.brandName,
            brandDescription: brand?.brandDescription,
          };
        })
      );
    }
  }, [brandDataAll]);

  const columns = useMemo(
    () => [
      {
        id: "selectAddress",
        Header: "Select Address",
        accessor: "selectAddress",
        Cell: ({ row: { original }, cell: { value } }) => {
          return (
            <input
              checked={`${original.id}` === `${book.address}`}
              onChange={(e) => {
                dispatch(setAddress(original.id));
              }}
              type="radio"
            ></input>
          );
        },
        canBeSorted: false,
        canBeFiltered: false,
        show: false,
      },
      {
        id: "label",
        Header: "Label",
        accessor: "label",
        Cell: ({ cell: { value } }) => {
          return <span className="badge">{firstLetterCapital(value)}</span>;
        },
        canBeSorted: false,
        canBeFiltered: false,
      },

      {
        id: "combineLocation",
        Header: "Combine Location",
        accessor: "combineLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: false,
        canBeFiltered: false,
      },
      {
        id: "exactLocation",
        Header: "Exact Location",
        accessor: "exactLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: false,
        canBeFiltered: false,
      },

      {
        id: "nearByLocation",
        Header: "Nearby Location",
        accessor: "nearByLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: false,
        canBeFiltered: false,
      },
      {
        id: "action",
        Header: "Action",
        accessor: "action",
        Cell: ({ row: { original }, cell: { value } }) => {
          return (
            <div>
              <div title="Edit" className="inline-block">
                <PencilIcon
                  className="w-4 h-4 mr-3  hover:text-primary-light"
                  onClick={() => {
                    setIsOpen(true);
                    setId(original.id);
                  }}
                >
                  Edit
                </PencilIcon>
              </div>

              <div title="Delete" className="inline-block">
                <TrashIcon
                  className="w-4 h-4  hover:text-error"
                  onClick={() => {
                    setOpenModal(true);
                    setId(original.id);
                  }}
                >
                  Delete
                </TrashIcon>
              </div>
            </div>
          );
        },
      },

      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: false,
        canBeFiltered: false,
      },

      {
        id: "mobile",
        Header: "Mobile",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: false,
        canBeFiltered: false,
      },
    ],
    [book]
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setshippingAddressDataQuery(query + "&noRegex=hasInHomePage");
  };

  useEffect(() => {
    if (shippingAddressData) {
      setPageCount(shippingAddressData.totalPages);
      setTotalData(shippingAddressData.totalDocs);
      setTableData(
        shippingAddressData.map((address) => {
          return {
            id: address?.id,
            combineLocation: address?.combineLocation,
            exactLocation: address?.exactLocation,
            label: address?.label,
            nearByLocation: address?.nearByLocation,
            mobile: address?.mobile,
            fullName: address?.fullName,
          };
        })
      );
    }
  }, [shippingAddressData]);

  return (
    <section className="mt-3">
      <div>
        {openModal && (
          <Popup
            title="Do you want to Delete Shipping Address?"
            description="if it is done it can not be undo."
            onOkClick={() => {
              deleteAddress(editId);
              setOpenModal(false);
            }}
            onCancelClick={() => setOpenModal(false)}
            okText="Yes"
            cancelText="No"
          />
        )}
        <Modal
          isOpen={modalIsOpen}
          // onRequestClose={closeModal}
          className="mymodal"
          overlayClassName="myoverlay"
        >
          <button
            type="button"
            onClick={closeModal}
            className="text-error absolute top-[5px] right-[5px]"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <ShippingCom
            id={editId}
            setIsOpen={setIsOpen}
            refetch={refetch}
            type="edit"
          >
            {" "}
          </ShippingCom>
        </Modal>

        <div className="text-right">
          <Button
            type="button"
            onClick={() => {
              setIsOpenn(true);
            }}
          >
            Add Shipping Address
          </Button>
        </div>
        <Modal
          isOpen={modalIsOpenn}
          // onRequestClose={closeModal}
          className="mymodal"
          overlayClassName="myoverlay"
        >
          <button
            type="button"
            onClick={closeModall}
            className="text-error absolute top-[5px] right-[5px]"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>

          <ShippingCom setIsOpen={setIsOpenn} refetch={refetch}></ShippingCom>
        </Modal>

        <Table
          tableName="Brand/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={shippingAddressIsError}
          isLoadingData={shippingAddressIsFetching}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          showCheckBox={false}
          hasExport={false}
          showTableFoot={false}
          hiddenColumns={hiddenColumns}
        />
      </div>
    </section>
  );
}

export default ShippingAddressTable;
