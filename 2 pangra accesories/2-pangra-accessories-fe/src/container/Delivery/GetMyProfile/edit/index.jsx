import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";

import Button from "components/Button";

import Input from "components/Input";

import { deliveryMyUpdateValidationSchema } from "validation/delivery.validation";
import Popup from "components/Popup";

import Select from "components/Select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { beforeAfterDate } from "utils/beforeAfterdate";

import { useRouter } from "next/router";
import DropZone from "components/DropZone";
import {
  useDeliveryUpdateMyProfileMutation,
  useDeliveryMyProfileQuery,
} from "services/api/deliveryLogin";
// import SelectWithoutCreate from "components/Select/SelectWithoutCreate";

import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";

const DeliveryProfileEdit = () => {
  const router = useRouter();

  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);

  const [editCustomerProfileValue, setEditCustomerProfileValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const genders = ["Male", "Female", "Other"];

  const formikBag = useRef();

  const { data: customerProfile } = useDeliveryMyProfileQuery();

  const [updateCustomer, { isError, isSuccess, error, isLoading, data }] =
    useDeliveryUpdateMyProfileMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(data.message || "Your profile has been updated successfully.", {
        appearance: "success",
      });
      setChanged(false);
      router.push(`/delivery/myProfile`);
    }
    if (isError) {
      addToast(error?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccess, isError]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (customerProfile) {
      const newCustomereProfileDetails = {
        firstName: customerProfile?.firstName,
        middleName: customerProfile?.middleName,
        lastName: customerProfile?.lastName,
        mobile: customerProfile?.mobile,
        // email: customerProfile?.email,
        gender: customerProfile?.gender,
        dob: customerProfile?.dob,
        profileImagePath: customerProfile?.profileImagePath,

        // location: {
        //   combineLocation: customerProfile?.location?.combineLocation,
        //   nearByLocation: customerProfile?.location?.nearByLocation,
        //   exactLocation: customerProfile?.location?.exactLocation,
        // },
      };

      setEditCustomerProfileValue(newCustomereProfileDetails);
    }
  }, [customerProfile]);

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", {});
    }
  };

  const profileOnSubmit = (values, { resetForm, setSubmitting }) => {
    updateCustomer(values);
    setSubmitting(false);
  };

  return (
    <>
      <Formik
        initialValues={editCustomerProfileValue}
        onSubmit={profileOnSubmit}
        validationSchema={deliveryMyUpdateValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div className="container mt-4">
            <h3 className="mb-3">Edit My Profile</h3>

            <Form onChange={() => setChanged(true)}>
              <div className="row-sm  pt-5 pb-2">
                <div className="three-col-sm">
                  <Input
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder="E.g: John"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Middle Name"
                    name="middleName"
                    type="text"
                    placeholder="E.g: Jung"
                    required={false}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder="E.g: Deo"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Mobile Number"
                    name="mobile"
                    type="text"
                    placeholder="E.g: 98XXXXXXXX"
                  />
                </div>
                {/* <div className="three-col-sm">
                  <Input
                    required={false}
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="E.g: john320@gmail.com"
                  />
                </div> */}
                <div className="three-col-sm">
                  <label htmlFor="">
                    Date Of Birth(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={values?.dob ? new Date(values?.dob) : null}
                    onChange={(date) => {
                      setFieldValue("dob", date?.toLocaleDateString());
                      setFieldTouched("dob");
                    }}
                    onBlur={() => {
                      setFieldTouched("dob");
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    // minDate={beforeAfterDate(new Date(), 0, 0, 0)}
                    maxDate={beforeAfterDate(new Date(), 0, 0, -16)}
                  />

                  {touched.dob && errors.dob && (
                    <div className="text-error text-sm">{errors.dob}</div>
                  )}
                </div>
                <div className="three-col-sm">
                  <Select label="Gender" name="gender">
                    <option value="">Select Gender</option>

                    {genders.map((gender, i) => (
                      <option key={i} value={gender}>
                        {gender}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  <div className="">
                    <div>
                      <DropZone
                        label="Upload Profile Image"
                        currentFiles={
                          Object.keys(values.profileImagePath || {}).length
                            ? [values.profileImagePath]
                            : []
                        }
                        setNewFiles={handleProfileImagePath}
                        handleRemoveFile={handleRemoveProfileImagePath}
                        error={
                          touched?.profileImagePath
                            ? errors?.profileImagePath
                            : ""
                        }
                      />
                    </div>
                  </div>
                </div>

                {/* <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    label="Select Location"
                    placeholder="Location"
                    error={
                      touched?.location?.combineLocation
                        ? errors?.location?.combineLocation
                        : ""
                    }
                    value={values?.location?.combineLocation || null}
                    onChange={(selectedValue) => {
                      setFieldValue(
                        "location.combineLocation",
                        selectedValue.value
                      );
                    }}
                    onBlur={() => {
                      setFieldTouched("location.combineLocation");
                    }}
                  />
                </div> */}
                {/* 
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name="location.nearByLocation"
                    label="Nearby Location"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name="location.exactLocation"
                    label="Exact location"
                    type="text"
                  />
                </div> */}
              </div>
              <div className="btn-holder mt-2">
                <Button type="submit" disabled={!dirty} loading={isLoading}>
                  Update
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </>
  );
};

export default DeliveryProfileEdit;
