import Button from "components/Button";
import Input from "components/Input";
import LoadingCustom from "components/LoadingCustom/LoadingCustom";
import Popup from "components/Popup";
import Select from "components/Select";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { addressLabel } from "constant/constant";
import { Form, Formik } from "formik";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import React, { useEffect, useRef, useState } from "react";
import { useToasts } from "react-toast-notifications";
import {
  useAddShippingAddressMutation,
  useReadSingleShippingAddressQuery,
  useUpdateShippingAddressMutation,
} from "services/api/customer";
import { useGetDetailLocationQuery } from "services/api/getFullLocation";
import { shippingAddressValidation } from "validation/customer.validation";

const ShippingCom = ({
  setIsOpen = () => {},
  type = "add",
  id,
  refetch = () => {},
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [changed, setChanged] = useState(false);
  const [editAddress, setEditAddress] = useState({});
  useWarnIfUnsavedChanges(changed);
  const { addToast } = useToasts();
  const { data: combineLocations, isFetching: isFetchingCombineLocation } =
    useGetDetailLocationQuery();

  const {
    data: shippingAddress,
    isSuccess: shippingAddressIsSuccess,
    isLoading: shippingAddressLoading,
  } = useReadSingleShippingAddressQuery(id, {
    skip: !id,
  });

  useEffect(() => {
    const initial = {
      combineLocation: shippingAddress?.combineLocation,
      nearByLocation: shippingAddress?.nearByLocation,
      exactLocation: shippingAddress?.exactLocation,
      label: shippingAddress?.label,
      fullName: shippingAddress?.fullName,
      mobile: shippingAddress?.mobile,
    };
    setEditAddress(initial);
  }, [shippingAddressIsSuccess]);

  const formikBag = useRef();
  const [
    addShippingAddress,
    {
      isError: isErrorAdd,
      isSuccess: isSuccessAdd,
      error: errorAdd,
      isLoading: isLoadingAdd,
      data: dataAdd,
    },
  ] = useAddShippingAddressMutation();
  useEffect(() => {
    if (isSuccessAdd) {
      formikBag.current?.resetForm();

      addToast(dataAdd.message || "Shipping address added successfully.", {
        appearance: "success",
      });
      setIsOpen(false);
      refetch();

      // console.log(refecthList);

      setChanged(false);
      //   router.push(`/customer-profile/get`);
    }
    if (isErrorAdd) {
      addToast(errorAdd?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAdd, isErrorAdd]);

  const [updateAddress, { isError, isSuccess, error, isLoading }] =
    useUpdateShippingAddressMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast("Shipping Address has been updated successfully.", {
        appearance: "success",
      });
      setChanged(false);
      setIsOpen(false);
      refetch();
    }
    if (isError) {
      addToast(error?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccess, isError]);

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      addShippingAddress(values);
    } else {
      updateAddress({ body: values, id: id });
    }
    // updateCustomer(values);
    setSubmitting(false);
  };

  const initial = {
    combineLocation: "",
    nearByLocation: "",
    exactLocation: "",
    label: "",
  };
  const shippintInitialValues = () => {
    if (type === "add") {
      return initial;
    } else return editAddress;
  };

  return (
    <div
      className={`relative ${
        shippingAddressLoading
          ? `bg-gray-100 before:absolute before:top-[-28px] before:bottom-[-28px] before:left-[-20px] before:right-[-20px] before:bg-gray-200 before:z-10 before:bg-opacity-80`
          : ""
      } `}
    >
      <LoadingCustom show={[shippingAddressLoading]}></LoadingCustom>
      <Formik
        initialValues={shippintInitialValues()}
        onSubmit={handleOnSubmit}
        validationSchema={shippingAddressValidation}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div>
            <h3 className="mb-3">Shipping Address</h3>

            <Form onChange={() => setChanged(true)}>
              <div className="row-sm">
                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={isFetchingCombineLocation}
                    label="Select Location"
                    placeholder="Location"
                    error={
                      touched?.combineLocation ? errors?.combineLocation : ""
                    }
                    value={values.combineLocation || null}
                    onChange={(selectedValue) => {
                      setFieldValue(`combineLocation`, selectedValue.value);
                    }}
                    options={combineLocations?.map((value) => ({
                      label: value.fullAddress,
                      // value: `${value.id}`,
                      value: value.fullAddress,
                    }))}
                    onBlur={() => {
                      setFieldTouched(`combineLocation`);
                    }}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`nearByLocation`}
                    label="Nearby Location"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`exactLocation`}
                    label="Exact location"
                    type="text"
                  />
                </div>

                <div className="three-col-sm">
                  <Select label="Label" name="label" required={true}>
                    <option value="" disabled="disabled">
                      Select Label
                    </option>

                    {addressLabel.map((label, i) => (
                      <option key={i} value={label}>
                        {label}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className="three-col-sm">
                  <Input
                    required={false}
                    name={`fullName`}
                    label="Full Name"
                    type="text"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    required={false}
                    name={`mobile`}
                    label="Mobile Number"
                    type="text"
                  />
                </div>
              </div>

              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  disabled={!dirty || isSubmitting}
                  loading={isLoadingAdd || isLoading}
                >
                  {type === "add" ? "Add" : "Edit"}
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </div>
  );
};

export default ShippingCom;
