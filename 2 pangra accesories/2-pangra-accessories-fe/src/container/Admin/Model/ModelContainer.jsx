import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import {
  useDeleteModelMutation,
  useReadModelListQuery,
} from "services/api/ModelService";
import { getQueryStringForTable } from "utils/getQueryStringForTable";

function ModelContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [modelDataQuery, setModelDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: modelData,
    isError: modelError,
    isFetching: isLoadingModel,
  } = useReadModelListQuery(modelDataQuery);

  const {
    data: modelDataAll,

    isFetching: isLoadingAll,
  } = useReadModelListQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (modelDataAll) {
      setTableDataAll(
        modelDataAll.docs.map((model) => {
          return {
            id: model?.id,
            modelName: model?.modelName,
            modelDescription: model?.modelDescription,
          };
        })
      );
    }
  }, [modelDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteModelMutation();

  const columns = useMemo(
    () => [
      {
        id: "modelName",
        Header: "Model Name",
        accessor: "modelName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "modelDescription",
        Header: "Model Description",
        accessor: "modelDescription",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setModelDataQuery(query);
  };

  useEffect(() => {
    if (modelData) {
      setPageCount(modelData.totalPages);
      setTotalData(modelData.totalDocs);
      setTableData(
        modelData.docs.map((model) => {
          return {
            id: model?.id,
            modelName: model?.modelName,
            modelDescription: model?.modelDescription,
          };
        })
      );
    }
  }, [modelData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Model List",
      route: "/admin/model",
    },
  ];

  return (
    <AdminLayout documentTitle="View Model" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Model/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={modelError}
            isLoadingData={isLoadingModel}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/model/update"
            viewRoute="admin/model"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add Model", route: "/admin/model/create" }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ModelContainer;
