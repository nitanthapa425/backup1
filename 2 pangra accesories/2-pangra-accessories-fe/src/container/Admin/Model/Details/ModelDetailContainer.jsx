import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";

import AdminLayout from "layouts/Admin";

import Link from "next/link";
import Button from "components/Button";
import { useReadModelDetailsQuery } from "services/api/ModelService";

const DetailModelContainer = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: modelDetails,
    error: modelFetchError,
    isLoading: isLoadingModel,
    error,
  } = useReadModelDetailsQuery(router.query.id, { skip: !router.query.id });

  useEffect(() => {
    if (modelFetchError) {
      addToast(
        error?.data?.message ||
          "Error occured while fetching model details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [modelFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Model List",
      route: "/admin/model",
    },
    {
      routeName: "Model Details",
      route: "",
    },
  ];
  return (
    <AdminLayout documentTitle="Model Details" BreadCrumbList={BreadCrumbList}>
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Model Details</h1>
          <div className="flex justify-end">
            <Link href={`/admin/model/update/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>

          {isLoadingModel ? (
            "Loading..."
          ) : (
            <>
              <div className="row">
                <div className="mx-3">
                  <p>
                    <strong>Model Name:</strong> &nbsp;
                    {modelDetails?.modelName}
                  </p>
                  <p>
                    <strong>Brand Name:</strong>&nbsp;
                    <Link href={`/admin/brand/${modelDetails?.brandId?.id}`}>
                      <a className="underline text-blue-700">
                        {modelDetails?.brandId?.brandName || "N/A"}
                      </a>
                    </Link>
                  </p>
                  <p>
                    <strong>Model Description:&nbsp;</strong>
                    {modelDetails?.modelDescription
                      ? modelDetails.modelDescription
                      : "N/A"}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default DetailModelContainer;
