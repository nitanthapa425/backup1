import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";
import PropTypes from "prop-types";
import Link from "next/link";

import Button from "components/Button";

import Input from "components/Input";
import AdminLayout from "layouts/Admin";

import Popup from "components/Popup";
import { useRouter } from "next/router";
import Textarea from "components/Input/textarea";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import { modelValidationSchema } from "validation/model.validation";
import {
  useCreateModelMutation,
  useReadModelDetailsQuery,
  useUpdateModelMutation,
} from "services/api/ModelService";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useReadBrandNoAuthQuery } from "services/api/BrandService";

const CreateEditModelContainer = ({ type }) => {
  const router = useRouter();
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [editModelInitialValue, setEditModelInitialValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();

  const { data: brandData, isLoading: fetchingBrand } =
    useReadBrandNoAuthQuery();
  const [BrandData, setBrandData] = useState([]);

  useEffect(() => {
    if (brandData) {
      setBrandData(brandData);
    }
  }, [brandData]);

  const addModelInitialValue = {
    brandId: router?.query?.brand || "",
    modelName: "",
    modelDescription: "",
  };

  const [
    createModel,
    { isError, isLoading, isSuccess, data: dataCreateModel },
  ] = useCreateModelMutation();

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(
        <>
          New Model added successfully.{" "}
          <Link href={`/admin/model/${dataCreateModel?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Model
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast("Model name already exist.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const { data: modelDetails, error: modelFetchError } =
    useReadModelDetailsQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  useEffect(() => {
    if (modelFetchError) {
      addToast(
        "Error occured while fetching Model details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [modelFetchError]);

  useEffect(() => {
    if (modelDetails) {
      const newModelDetails = {
        brandId: modelDetails?.brandId?.id,
        modelName: modelDetails?.modelName,
        modelDescription: modelDetails?.modelDescription,
      };
      setEditModelInitialValue(newModelDetails);
    }
  }, [modelDetails]);

  const [
    updateModel,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateUpdateModel,
    },
  ] = useUpdateModelMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("Model name already exist.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(updateUpdateModel?.message || "Model updated successfully.", {
        appearance: "success",
      });
      setChanged(false);
      // redirect to model page
      // ..........redirect is left
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
      router.push(`/admin/model/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      // createBrand(values, resetForm);
      createModel(values);
    }
    if (type === "edit") {
      updateModel({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Model",
        route: "/admin/model/create",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Model List",
        route: "/admin/model",
      },
      {
        routeName: "Edit Model",
        route: "",
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={type === "add" ? "Add New Model" : "Edit Model Details"}
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? editModelInitialValue : addModelInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={modelValidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
            setFieldTouched,
          }) => (
            <Form onChange={() => setChanged(true)}>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add" ? "Add New Model" : "Edit Model Details"}
                  </h3>
                  {/* <div className="mb-2">
                    <Input
                      label="Brand Name"
                      name="modelName"
                      type="text"
                      placeholder="E.g: Cruise Model"
                    />
                  </div> */}
                  <div className="mb-2">
                    <SelectWithoutCreate
                      required
                      loading={fetchingBrand}
                      label="Select Brand"
                      placeholder="E.g: Riding Gears"
                      error={touched?.brandId ? errors?.brandId : ""}
                      value={values.brandId}
                      onChange={(selectedBrand) => {
                        setFieldTouched("brandId");
                        setFieldValue("brandId", selectedBrand.value);
                      }}
                      options={BrandData?.map((d) => ({
                        value: d?.brandId,
                        label: d?.brandName,
                      }))}
                    />
                  </div>
                  <div className="mb-2">
                    <Input
                      label="Model Name"
                      name="modelName"
                      type="text"
                      placeholder="E.g: Cruise Model"
                    />
                  </div>

                  <div className="mb-2">
                    <Textarea
                      label="Model Description"
                      name="modelDescription"
                      type="text"
                      placeholder="E.g: Model Description"
                      required={false}
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === "add" ? "Submit" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditModelContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditModelContainer;
