import Link from "next/link";
import { useState, useCallback, useRef, useEffect } from "react";
import { useRouter } from "next/router";
import { Formik, Form, setNestedObjectValues } from "formik";
import Steps, { Step } from "rc-steps";
import { ConnectedFocusError } from "focus-formik-error";
import { useToasts } from "react-toast-notifications";
import PropTypes from "prop-types";

import AdminLayout from "layouts/Admin";

import Popup from "components/Popup";
import Button from "components/Button";

import { createProductValidation } from "validation/product.validation";

import {
  useCreateProductMutation,
  useUpdateProductMutation,
  useReadProductDetailsQuery,
} from "services/api/ProductService";

import "rc-steps/assets/index.css";
import "rc-steps/assets/iconfont.css";

import { stepsData } from "./data";
import { productInitialValues } from "./initialValues";
import {
  useGetDraftedProductQuery,
  useProductSaveAsDraftMutation,
  useUpdateDraftProductMutation,
  useFinishDraftProductMutation,
} from "services/api/ProductDraft";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";

const CreateEditProductContainer = ({ type }) => {
  const router = useRouter();
  const formikBag = useRef(null);
  const { addToast } = useToasts();

  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  // for edit/update
  const [currentProductDetails, setCurrentProductDetails] =
    useState(productInitialValues);

  // for draft update
  const [currentDraftProductDetails, setCurrentDraftProductDetails] =
    useState(productInitialValues);

  const [currentStep, setCurrentStep] = useState(0);
  const isLastStep = currentStep === stepsData.length - 1;
  const [openModal, setOpenModal] = useState(false);

  const handleBackClick = useCallback(
    () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null),
    [currentStep]
  );

  const [
    createProduct,
    { data: createdProductData, isLoading, isError, error, isSuccess },
  ] = useCreateProductMutation();

  const [
    updateProduct,
    { isLoading: updating, isError: isUpdateError, isSuccess: isUpdateSuccess },
  ] = useUpdateProductMutation();

  const [
    saveAsDraft,
    {
      isLoading: savingAsDraft,
      isSuccess: isDraftSuccess,
      isError: isDraftError,
      error: draftError,
    },
  ] = useProductSaveAsDraftMutation();

  const [
    finishDraft,
    {
      data: finishedDraftData,
      isLoading: finishingDraft,
      isSuccess: isFinishSuccess,
      isError: isFinishError,
    },
  ] = useFinishDraftProductMutation();

  const { data: productDetails, error: productFetchError } =
    useReadProductDetailsQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  const { data: draftProductDetails, error: draftProductFetchError } =
    useGetDraftedProductQuery(router.query.id, {
      skip: type !== "draft" || !router.query.id,
    });

  const [
    updateSaveAsDraft,
    {
      isLoading: updatingDraft,
      isSuccess: isUpdateDraftSuccess,
      isError: isUpdateDraftError,
      error: updateDraftError,
    },
  ] = useUpdateDraftProductMutation();

  useEffect(() => {
    if (productDetails) {
      setCurrentProductDetails({
        brandId: productDetails?.brandId,
        categoryId: productDetails?.categoryId,
        subCategoryId: productDetails?.subCategoryId,
        highLight: productDetails?.highLight,
        color: productDetails?.color,
        costPrice: productDetails?.costPrice,
        adminPrice: productDetails?.adminPrice,
        discountedPrice:
          productDetails?.discountedPrice || productDetails?.costPrice,
        productImage: productDetails?.productImage,
        productImages: productDetails?.productImages,
        weight: productDetails?.weight?.split(" ")[0],
        weightUnit: productDetails?.weight?.split(" ")[1],
        dimension: {
          length: productDetails?.dimension?.length,
          breadth: productDetails?.dimension?.breadth,
          height: productDetails?.dimension?.height,
        },
        dimensionUnit: {
          length: "mm",
          breadth: "mm",
          height: "mm",
        },
        // dimensionUnit: productDetails?.dimensionUnit,
        sizeCalculation: productDetails?.sizeCalculation,
        sizeValue: productDetails?.sizeValue,
        SKU: productDetails?.SKU,
        hasWarranty: productDetails?.hasWarranty ? "true" : "false",
        warrantyUnit: productDetails?.warrantyTime?.split(" ")[1],
        warrantyTime: productDetails?.warrantyTime?.split(" ")[0],
        isReturnable: productDetails?.isReturnable ? "true" : "false",
        returnableUnit: productDetails?.returnableTime?.split(" ")[1],
        returnableTime: productDetails?.returnableTime?.split(" ")[0],
        // onSale: productDetails?.onSale ? "true" : "false",
        onSale: "false",
        tags: productDetails?.tags,
        description: productDetails?.description,
        availableOptions: productDetails?.availableOptions || [],
        sizeChart: productDetails?.sizeChart?._id,
        modelId: productDetails?.modelId || "",
        shortDescription: productDetails?.shortDescription,
      });
    }
  }, [productDetails]);

  useEffect(() => {
    if (draftProductDetails) {
      setCurrentDraftProductDetails({
        brandId: draftProductDetails?.brandId,
        categoryId: draftProductDetails?.categoryId,
        subCategoryId: draftProductDetails?.subCategoryId,
        highLight: draftProductDetails?.highLight,
        color: draftProductDetails?.color,
        quantity: draftProductDetails?.quantity,
        costPrice: draftProductDetails?.costPrice,
        adminPrice: draftProductDetails?.adminPrice,
        discountedPrice:
          draftProductDetails?.discountedPrice ||
          draftProductDetails?.adminPrice,
        productImage: draftProductDetails?.productImage,
        productImages: draftProductDetails?.productImages,
        weight: draftProductDetails?.weight?.split(" ")[0],
        weightUnit: draftProductDetails?.weight?.split(" ")[1],
        dimension: {
          length: draftProductDetails?.dimension?.length,
          breadth: draftProductDetails?.dimension?.breadth,
          height: draftProductDetails?.dimension?.height,
        },
        dimensionUnit: {
          length: "mm",
          breadth: "mm",
          height: "mm",
        },
        // dimensionUnit: draftProductDetails?.dimensionUnit,
        sizeCalculation: draftProductDetails?.sizeCalculation,
        sizeValue: draftProductDetails?.sizeValue,
        SKU: draftProductDetails?.SKU,
        hasWarranty: draftProductDetails?.hasWarranty ? "true" : "false",
        warrantyUnit: draftProductDetails?.warrantyTime?.split(" ")[1],
        warrantyTime: draftProductDetails?.warrantyTime?.split(" ")[0],
        isReturnable: draftProductDetails?.isReturnable ? "true" : "false",
        returnableUnit: draftProductDetails?.returnableTime?.split(" ")[1],
        returnableTime: draftProductDetails?.returnableTime?.split(" ")[0],
        // onSale: draftProductDetails?.onSale ? "true" : "false",
        onSale: "false",
        tags: draftProductDetails?.tags,
        description: draftProductDetails?.description,
        availableOptions: draftProductDetails?.availableOptions || [],
        sizeChart: draftProductDetails?.sizeChart,
        modelId: draftProductDetails?.modelId || "",
        shortDescription: draftProductDetails?.shortDescription,
      });
    }
  }, [draftProductDetails]);

  const _submitForm = (values, actions) => {
    if (type === "add") {
      createProduct(values);
    }
    if (type === "edit") {
      const availableOptions = values?.availableOptions.filter(
        (item) =>
          values.color.includes(item.color) &&
          values.sizeValue.includes(item.size)
      );
      updateProduct({ ...values, availableOptions, id: router.query.id });
    }
    if (type === "draft") {
      finishDraft({ ...values, id: router.query.id });
    }
    actions.setSubmitting(false);
  };

  const handleSubmit = (values, actions) => {
    if (isLastStep) {
      // Deleting the key name whose values are stored in enum in the backend.

      if (values?.sizeChart === "") {
        delete values.sizeChart;
      }
      if (!values?.adminPrice) {
        delete values.adminPrice;
      }

      if (!values?.costPrice) {
        delete values.costPrice;
      }

      // if (!values?.discountedPrice) {
      //   delete values.discountedPrice;
      // }

      // values?.availableOptions.forEach((data, i) => {
      //   if (!data.discountedAmount) {
      //     delete values.availableOptions[i].discountedAmount;
      //   }
      // });

      if (values?.weightUnit === "") {
        delete values.weightUnit;
      }
      if (values?.dimensionUnit?.length === "") {
        delete values.dimensionUnit.length;
      }
      if (values?.dimensionUnit?.breadth === "") {
        delete values.dimensionUnit.breadth;
      }
      if (values?.dimensionUnit?.height === "") {
        delete values.dimensionUnit.height;
      }

      values.brandId = values.brandId?.value || values.brandId; // use only value from brandId dropdown object in add
      _submitForm(values, actions);
    } else {
      setCurrentStep((prev) => prev + 1);
      actions.setTouched({});
      actions.setSubmitting(false);
    }
  };

  const handleSaveAsDraft = (values) => {
    if (values?.sizeChart === "") {
      delete values.sizeChart;
    }
    if (type === "add") {
      // Deleting the key name whose values are stored in enum in the backend.
      if (values?.weightUnit === "") {
        delete values.weightUnit;
      }
      if (values?.dimensionUnit === "") {
        delete values.dimensionUnit;
      }
      if (values?.hasWarranty === "") {
        delete values.hasWarranty;
      }
      if (values?.isReturnable === "") {
        delete values.isReturnable;
      }
      if (values?.sizeCalculation === "") {
        delete values.sizeCalculation;
      }

      values.brandId = values.brandId?.value || values.brandId; // use only value from brandId dropdown object in add

      saveAsDraft(values);
    } else if (type === "draft") {
      // Deleting the key name whose values are stored in enum in the backend.
      if (values?.weightUnit === "") {
        delete values.weightUnit;
      }
      // if (values?.dimensionUnit === "") {
      //   delete values.dimensionUnit;
      // }
      if (values?.dimensionUnit?.length === "") {
        delete values.dimensionUnit.length;
      }
      if (values?.dimensionUnit?.breadth === "") {
        delete values.dimensionUnit.breadth;
      }
      if (values?.dimensionUnit?.height === "") {
        delete values.dimensionUnit.height;
      }
      if (values?.hasWarranty === "") {
        delete values.hasWarranty;
      }
      if (values?.isReturnable === "") {
        delete values.isReturnable;
      }
      if (values?.sizeCalculation === "") {
        delete values.sizeCalculation;
      }
      const availableOptions = values?.availableOptions?.filter(
        (item) =>
          values?.color?.includes(item?.color) &&
          values?.sizeValue?.includes(item?.size)
      );
      values.brandId = values.brandId?.value || values.brandId; // use only value from brandId dropdown object in add

      updateSaveAsDraft({ ...values, availableOptions, id: router.query.id });
    }
  };

  const _sucessAction = (message) => {
    formikBag.current?.resetForm();

    setCurrentStep(0);
    addToast(message, {
      appearance: "success",
    });
  };

  useEffect(() => {
    if (isSuccess) {
      _sucessAction(
        <>
          New Product added successfully.{" "}
          <Link href={`/admin/product/${createdProductData?.accessoriesId}`}>
            <a className="text-md font-semibold underline text-blue-700">
              View Product
            </a>
          </Link>
        </>
      );
    }
    if (isFinishSuccess) {
      _sucessAction("New Product added successfully.");
      router.push(`/admin/product/${finishedDraftData?.draftId}`);
    }
    setChanged(false);
  }, [isSuccess, isFinishSuccess]);

  useEffect(() => {
    if (isFinishError) {
      addToast("Error occured while adding product. Please try again later.", {
        appearance: "error",
      });
    }
    if (isError) {
      if (error?.data?.message) {
        addToast(error?.data?.message, {
          appearance: "error",
        });
      } else {
        addToast(
          "Error occurred while adding product. Please try again later.",
          {
            appearance: "error",
          }
        );
      }
    }
  }, [isError, isFinishError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      _sucessAction("Product updated successfully.");
      setChanged(false);
      router.push(`/admin/product/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  useEffect(() => {
    if (isUpdateError) {
      addToast(
        "Error occurred while updating product. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (productFetchError) {
      addToast(
        "Error occured while fetching product details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
    if (draftProductFetchError) {
      addToast(
        "Error occurred while fetching drafted vehicle details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [productFetchError, draftProductFetchError]);

  useEffect(() => {
    if (isDraftSuccess) {
      addToast(
        <>
          Product added to Draft successfully{" "}
          <Link href={`/admin/product/drafts`}>
            <a className="text-md font-semibold underline text-blue-700">
              View Drafts
            </a>
          </Link>
        </>,
        {
          appearance: "success",
        }
      );
      setChanged(false);
    }
    if (isUpdateDraftSuccess) {
      addToast(
        <>
          Draft updated successfully{" "}
          <Link href={`/admin/product/drafts`}>
            <a className="text-md font-semibold underline text-blue-700">
              View Drafts
            </a>
          </Link>
        </>,
        {
          appearance: "success",
        }
      );
      setChanged(false);
    }
  }, [isDraftSuccess, isUpdateDraftSuccess]);

  useEffect(() => {
    if (isDraftError) {
      addToast(
        draftError?.message ||
          "Error occured while adding to draft. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
    if (isUpdateDraftError) {
      addToast(
        updateDraftError?.message ||
          "Error occured while updating draft. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isDraftError, isUpdateDraftError]);

  const _getTitle = () => {
    let title = "";
    if (type === "add") {
      title = "Add New Product";
    } else if (type === "edit") {
      title = "Edit Product";
    } else if (type === "draft") {
      title = "Edit Drafted Product Details";
    }
    return title;
  };

  const _getInitialValues = () => {
    let initialValues = productInitialValues;
    if (type === "edit") initialValues = currentProductDetails;
    else if (type === "draft") initialValues = currentDraftProductDetails;
    return initialValues;
  };

  return (
    <AdminLayout documentTitle={_getTitle()} BreadCrumbList={[]}>
      <section className="stepper-section-pt-4 mt-5">
        <div className="container lg:px-10">
          <h4 className="my-3 ">{_getTitle()}</h4>
          <Formik
            initialValues={_getInitialValues()}
            validationSchema={createProductValidation(type)[currentStep]}
            onSubmit={handleSubmit}
            innerRef={formikBag}
            enableReinitialize
          >
            {({
              setFieldValue,
              values,
              errors,
              touched,
              resetForm,
              validateForm,
              setTouched,
              dirty,
              setFieldTouched,
              isValid,
            }) => {
              return (
                <Form>
                  <ConnectedFocusError />
                  <Steps current={currentStep} direction="vertical">
                    {stepsData.map(
                      ({ id, title, component: StepContent, key }, index) => (
                        <Step
                          key={id + key}
                          title={
                            <span
                              className="cursor-pointer"
                              onClick={() => {
                                if (id < currentStep) {
                                  setCurrentStep(id);
                                }
                              }}
                            >
                              {title}
                            </span>
                          }
                          description={
                            <>
                              {currentStep === index && (
                                <>
                                  <StepContent
                                    values={values}
                                    setFieldValue={setFieldValue}
                                    errors={errors}
                                    touched={touched}
                                    setChanged={setChanged}
                                    dirty={dirty}
                                    setFieldTouched={setFieldTouched}
                                    isValid={isValid}
                                    type={type}
                                  />
                                  <Button
                                    loading={
                                      isLoading || updating || finishingDraft
                                    }
                                    type="submit"
                                    variant={
                                      isLastStep ? "primary" : "secondary"
                                    }
                                  >
                                    {isLastStep && type === "add"
                                      ? "Submit"
                                      : isLastStep && type === "edit"
                                      ? "Update"
                                      : isLastStep && type === "draft"
                                      ? "Submit"
                                      : "Next"}
                                  </Button>
                                  <Button
                                    onClick={() => {
                                      setOpenModal(true);
                                    }}
                                    variant="outlined-error"
                                    type="button"
                                  >
                                    Clear
                                  </Button>
                                  {openModal && (
                                    <Popup
                                      title="Are you sure to clear all fields?"
                                      description="If you clear all fields, the data will not be saved."
                                      onOkClick={() => {
                                        resetForm();
                                        setCurrentStep(0);
                                        setOpenModal(false);
                                      }}
                                      onCancelClick={() => setOpenModal(false)}
                                      okText="Clear All"
                                      cancelText="Cancel"
                                    />
                                  )}

                                  {(type === "add" || type === "draft") &&
                                    values.brandId && (
                                      <Button
                                        loading={savingAsDraft || updatingDraft}
                                        type="button"
                                        variant="tertiary"
                                        onClick={() => {
                                          validateForm().then((errs) => {
                                            if (
                                              Object.keys(errs).length === 0
                                            ) {
                                              handleSaveAsDraft(values);
                                              resetForm();
                                            } else {
                                              setTouched(
                                                setNestedObjectValues(
                                                  errs,
                                                  true
                                                )
                                              );
                                            }
                                            // resetForm();
                                          });
                                        }}
                                      >
                                        {type === "draft"
                                          ? "Update Draft"
                                          : "Save as draft"}
                                      </Button>
                                    )}
                                  {currentStep !== 0 && (
                                    <Button
                                      type="button"
                                      disabled={isLoading}
                                      onClick={handleBackClick}
                                      variant="link"
                                    >
                                      Back
                                    </Button>
                                  )}
                                </>
                              )}
                            </>
                          }
                        />
                      )
                    )}
                  </Steps>
                </Form>
              );
            }}
          </Formik>
        </div>
      </section>
    </AdminLayout>
  );
};

CreateEditProductContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditProductContainer;
