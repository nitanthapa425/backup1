import BasicDetails from "./steps/BasicDetails";
import DimensionAndSizes from "./steps/DimSizes";
import OtherDetails from "./steps/OtherDetails";

export const stepsData = [
  {
    id: 0,
    key: "BASIC_DETAILS",
    title: "Basic Details",
    component: BasicDetails,
  },
  {
    id: 1,
    key: "DIMENSION_AND_SIZES",
    title: "Dimensions and Sizes",
    component: DimensionAndSizes,
  },
  {
    id: 2,
    key: "OTHER_DETAILS",
    title: "Other Details",
    component: OtherDetails,
  },
];
