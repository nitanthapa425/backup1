import { useEffect, useRef, useState } from "react";
import Input from "components/Input";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import ReactSelect from "components/Select/ReactSelect";
import Modal from "react-modal";
import { TrashIcon } from "@heroicons/react/solid";
import {
  weightUnits as weightUnitsData,
  lengthUnits as lengthUnitsData,
  sizeFormatUnits as sizeFormatUnitsData,
  sizesLetters as sizesLettersData,
  numbersArrGenerator,
} from "constant/constant";
import {
  useCreateColorsMutation,
  useGetSizeChartQuery,
} from "services/api/ProductService";

import { useReadColorsQuery } from "services/api/Color/colorService";
import { useToasts } from "react-toast-notifications";
import SizeChart from "container/Admin/SizeChart/SizeChart";
import Button from "components/Button";

import "react-medium-image-zoom/dist/styles.css";
import Zoom from "react-medium-image-zoom";
import Select from "components/Select";

const sizesNumbersData = numbersArrGenerator(1, 50)?.map((d) => ({
  value: d.toString(),
  label: d,
}));

const DimensionAndSizes = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
  type,
}) => {
  const [availableOptions, setAvailableOptions] = useState([]);

  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const _getAvailableOptions = (availableOpt) => {
    const availableOptions = [];
    availableOpt?.sizeValue?.forEach((size) => {
      availableOpt?.color?.forEach((color) => {
        const option = values.availableOptions.find(
          (item) => item.color === color && item.size === size
        );
        availableOptions.push({
          size,
          color,
          discountPercentage: values.discountedPrice
            ? ((values?.costPrice - values.discountedPrice) /
                values?.costPrice) *
                100 +
              ""
            : "",
          discountedAmount: values?.discountedPrice + "",
          ...option,
        });
      });
    });
    return availableOptions;
  };

  const formikBag = useRef();
  const { addToast } = useToasts();
  const { data: dataReadColors, isLoading: isLoadingDataReadColors } =
    useReadColorsQuery();
  // console.log("dataReadColors", dataReadColors);

  const { data: dataSizeChart } = useGetSizeChartQuery();

  const sizeChartImage = dataSizeChart?.find?.(
    (v1, i) => v1._id === values.sizeChart
  )?.imageUrl;

  const [
    createColors,
    {
      isLoading: isLoadingCreateColors,
      isError: isErrorCreateColors,
      isSuccess: isSuccessCreateColors,
      error: errorUpdate,
      data: dataCreateColors,
    },
  ] = useCreateColorsMutation();

  useEffect(() => {
    if (isSuccessCreateColors) {
      formikBag.current?.resetForm();

      addToast(
        dataCreateColors?.message || "New color is created successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);
    }
  }, [isSuccessCreateColors]);

  useEffect(() => {
    if (isErrorCreateColors) {
      addToast(errorUpdate?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorCreateColors]);

  const [openModal, setOpenModal] = useState(false);
  const closeModal = () => {
    setOpenModal(false);
  };

  useEffect(() => {
    // if (type === "edit") {
    setAvailableOptions(values?.availableOptions);
    // }
  }, []);

  const removeAvailabeOption = (index) => {
    const availableOptions = [...values.availableOptions];
    const availableOptions2 = [...availableOptions];
    availableOptions.splice(index, 1);
    availableOptions2.splice(index, 1);
    setFieldValue("availableOptions", availableOptions);
    setAvailableOptions(availableOptions2);
  };

  const fetchAvailableOptions = ({ sizeValue, color }) => {
    const availableOptions = _getAvailableOptions({ sizeValue, color });
    setAvailableOptions(availableOptions);
    setFieldValue("availableOptions", availableOptions);
  };

  return (
    <>
      <div className="row-sm pb-8">
        <div className="three-col-sm unit-converter-container flex">
          <div>
            <Input
              label="Weight"
              name="weight"
              type="text"
              placeholder="E.g: 10"
              required={false}
              disabled={values?.weightUnit === ""}
            />
          </div>
          <div className="mt-7" style={{ width: "4.5rem" }}>
            <Select name="weightUnit">
              {weightUnitsData?.map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        <div className="three-col-sm flex">
          <div>
            <Input
              label="Dimensions (Length)"
              name="dimension.length"
              type="text"
              placeholder="E.g: 12"
              required={false}
              disabled={values?.dimensionUnit?.length === ""}
            />
          </div>
          <div className="mt-7" style={{ width: "6.2rem" }}>
            <Select name="dimensionUnit.length">
              {lengthUnitsData?.map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        <div className="three-col-sm flex">
          <div>
            <Input
              label="Dimensions (Breadth)"
              name="dimension.breadth"
              type="text"
              placeholder="E.g: 6"
              required={false}
              disabled={values?.dimensionUnit?.breadth === ""}
            />
          </div>
          <div className="mt-7" style={{ width: "6.2rem" }}>
            <Select name="dimensionUnit.breadth">
              {lengthUnitsData?.map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        <div className="three-col-sm flex">
          <div>
            <Input
              label="Dimensions (Height)"
              name="dimension.height"
              type="text"
              placeholder="E.g: 3"
              required={false}
              disabled={values?.dimensionUnit?.height === ""}
            />
          </div>
          <div className="mt-7" style={{ width: "6.2rem" }}>
            <Select name="dimensionUnit.height">
              {lengthUnitsData?.map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            label="Size Format"
            placeholder="E.g: Numbers"
            error={touched?.sizeCalculation ? errors?.sizeCalculation : ""}
            value={values.sizeCalculation}
            onChange={(selectedData) => {
              setFieldTouched("sizeCalculation");
              setFieldValue("sizeCalculation", selectedData.value);
            }}
            options={sizeFormatUnitsData?.map((d) => ({
              value: d?.value,
              label: d?.label,
            }))}
            onBlur={() => {
              setFieldTouched("sizeCalculation");
            }}
          />
        </div>
        <div className="three-col-sm">
          <ReactSelect
            required
            label="Available Sizes"
            placeholder={
              values?.sizeCalculation === "Numbers"
                ? "E.g: 5,10 "
                : values?.sizeCalculation === "Litres"
                ? "E.g: 1,10 "
                : values?.sizeCalculation === "Free Size"
                ? ""
                : "E.g: XL, XXL"
            }
            error={touched?.sizeValue ? errors?.sizeValue : ""}
            value={values.sizeValue || []}
            onChange={(selectedData) => {
              setFieldTouched("sizeValue");
              setFieldValue(
                "sizeValue",
                selectedData.map((v) => v.value)
              );
              // reset available options when size value changes
              fetchAvailableOptions({
                color: values.color,
                sizeValue: selectedData.map((v) => v.value),
              });
            }}
            options={
              values?.sizeCalculation === "Numbers"
                ? // numbersArrGenerator(1, 50)?.map((d) => ({
                  //     value: d,
                  //     label: d,
                  // }))
                  sizesNumbersData
                : values?.sizeCalculation === "Litres"
                ? sizesNumbersData
                : values?.sizeCalculation === "Free Size"
                ? [{ value: "Free Size", label: "Free Size" }]
                : sizesLettersData?.map((d) => ({
                    value: d,
                    label: d,
                  }))
            }
            onBlur={() => {
              setFieldTouched("sizeValue");
            }}
            isMulti={true}
            isDisabled={values?.sizeCalculation === ""}
          />
        </div>

        <div className="three-col-sm">
          <ReactSelect
            required
            label="Available Colors"
            placeholder="E.g: Red And Green"
            error={touched?.color ? errors?.color : ""}
            loading={isLoadingCreateColors || isLoadingDataReadColors}
            value={values.color || []}
            onChange={(selectedBodyType, actionMeta) => {
              setFieldTouched("color");
              setFieldValue(
                "color",
                selectedBodyType.map((v) => v.value)
              );
              // reset available options when color changes
              fetchAvailableOptions({
                sizeValue: values.sizeValue,
                color: selectedBodyType.map((v) => v.value),
              });

              if (
                selectedBodyType[0] &&
                actionMeta.action === "create-option"
              ) {
                createColors({
                  color: selectedBodyType[0].value,
                });
              }
            }}
            options={dataReadColors?.map((color) => ({
              value: color?.color,
              label: color?.color,
            }))}
            onBlur={() => {
              setFieldTouched("color");
            }}
            isMulti={true}
          />
        </div>

        <div className="three-col-sm">
          <div className="mt-7">
            <Button
              type="button"
              onClick={() => {
                setOpenModal(true);
              }}
            >
              Select Size Chart
            </Button>
          </div>

          {sizeChartImage ? (
            <div>
              <label className="block">Size Chart</label>
              <Zoom zoomMargin={40}>
                <img
                  src={sizeChartImage}
                  className="
                  rounded
                  w-[150px]
                  h-[100px]
                  "
                />
              </Zoom>
            </div>
          ) : null}
        </div>

        {values?.color?.length > 0 && values?.sizeValue?.length > 0 && (
          <div className="w-full">
            <table className="w-full dimension-table">
              <thead>
                <tr>
                  <th>Size</th>
                  <th>Color</th>
                  <th>
                    Quantity
                    <span className="required" style={{ display: "inline" }}>
                      {" "}
                      *
                    </span>
                  </th>
                  <th>
                    Discount Percentage
                    <span className="required" style={{ display: "inline" }}>
                      {" "}
                      *
                    </span>
                  </th>
                  <th>
                    Discounted Price(NRs.)
                    <span className="required" style={{ display: "inline" }}>
                      {" "}
                      *
                    </span>
                  </th>
                  {type === "edit" && <th className="code">Product Code</th>}
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {availableOptions?.map((option, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        {option?.size === "Free Size"
                          ? "Free Size"
                          : option?.size}
                      </td>
                      <td>{option?.color}</td>
                      <td>
                        <input
                          type="text"
                          placeholder="E.g: 10"
                          className="mx-2 w-20 mt-2"
                          value={
                            values?.availableOptions[index]?.quantity || ""
                          }
                          onChange={(e) =>
                            setFieldValue(`availableOptions[${index}]`, {
                              ...option,
                              ...values?.availableOptions[index],
                              quantity: e.target.value,
                            })
                          }
                        />
                        <div className="-mt-1 ml-2 mt-2">
                          {touched?.availableOptions &&
                            errors?.availableOptions &&
                            errors?.availableOptions[index] && (
                              <small className="text-error">
                                {errors?.availableOptions[index]?.quantity}
                              </small>
                            )}
                        </div>
                      </td>
                      <td>
                        <input
                          type="text"
                          placeholder="E.g: 10"
                          className="mx-1 w-20 mt-2"
                          value={
                            values?.availableOptions[index]?.discountPercentage
                          }
                          onChange={(e) =>
                            setFieldValue(`availableOptions[${index}]`, {
                              ...option,
                              ...values?.availableOptions[index],
                              discountPercentage: e.target.value + "",
                              discountedAmount:
                                values?.costPrice -
                                (+e.target.value * values?.costPrice) / 100 +
                                "",
                            })
                          }
                        />
                        <div className="-mt-1 ml-2 mt-2">
                          {touched?.availableOptions &&
                            errors?.availableOptions &&
                            errors?.availableOptions[index] && (
                              <small className="text-error">
                                {
                                  errors?.availableOptions[index]
                                    ?.discountPercentage
                                }
                              </small>
                            )}
                        </div>
                      </td>
                      <td>
                        <input
                          type="text"
                          placeholder="E.g: 1000"
                          className="mx-2 w-24 mt-2"
                          value={
                            values?.availableOptions[index]?.discountedAmount
                          }
                          onChange={(e) =>
                            setFieldValue(`availableOptions[${index}]`, {
                              ...option,
                              ...values?.availableOptions[index],
                              discountedAmount: e.target.value + "",
                              discountPercentage:
                                ((values?.costPrice - +e.target.value) /
                                  values?.costPrice) *
                                  100 +
                                "",
                            })
                          }
                        />
                        <div className="-mt-1 ml-2">
                          {touched?.availableOptions &&
                            errors?.availableOptions &&
                            errors?.availableOptions[index] && (
                              <small className="text-error">
                                {
                                  errors?.availableOptions[index]
                                    ?.discountedAmount
                                }
                              </small>
                            )}
                        </div>
                      </td>
                      {type === "edit" && (
                        <td>
                          <input
                            type="text"
                            // placeholder="E.g: 10"
                            className="mx-2  mt-2 bg-gray-100"
                            value={
                              values?.availableOptions[index]?.productCode || ""
                            }
                            onChange={(e) =>
                              setFieldValue(`availableOptions[${index}]`, {
                                ...option,
                                ...values?.availableOptions[index],
                                productCode: e.target.value,
                              })
                            }
                            disabled
                          />
                          <div className="-mt-1 ml-2 mt-2">
                            {touched?.availableOptions &&
                              errors?.availableOptions &&
                              errors?.availableOptions[index] && (
                                <small className="text-error">
                                  {errors?.availableOptions[index]?.productCode}
                                </small>
                              )}
                          </div>
                        </td>
                      )}
                      <td>
                        <div
                          title="Delete this option"
                          onClick={() => removeAvailabeOption(index)}
                        >
                          <TrashIcon className="w-8 text-red-600 cursor-pointer" />
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        )}

        <div className="size-chart-modal">
          <Modal
            isOpen={openModal}
            className="mymodal"
            overlayClassName="myoverlay"
          >
            <button
              type="button"
              onClick={closeModal}
              className="text-error absolute top-[5px] right-[5px]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
            <div>
              <SizeChart
                setIsOpen={setOpenModal}
                values={values}
                setFieldValue={setFieldValue}
                errors={errors}
                touched={touched}
                setChanged={setChanged}
                dirty={dirty}
                setFieldTouched={setFieldTouched}
              ></SizeChart>
            </div>
          </Modal>
        </div>
      </div>
    </>
  );
};

export default DimensionAndSizes;
