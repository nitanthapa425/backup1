import { useState, useEffect } from "react";
import { useReadBrandNoAuthQuery } from "services/api/BrandService";
import { useReadCategoryNoAuthQuery } from "services/api/CategoryService";
import { useReadSubCategoryFromCategoryNoAuthQuery } from "services/api/SubCategoryService";
import Input from "components/Input";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import DropZone from "components/DropZone";
// import { color as colorData } from "constant/constant";
import { useReadBrandModelListQuery } from "services/api/ModelService";
import SelectAsync from "components/Select/AsyncSelect";
import { searchBrands } from "services/api/brand";

const BasicDetails = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
  type,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  const [selectedCatId, setSelectedCatId] = useState(values?.categoryId);
  const [selectedBrandId, setSelectedBrandId] = useState(values?.brandId);

  const { data: brandData, isLoading: fetchingBrands } =
    useReadBrandNoAuthQuery("", {
      skip: type === "add",
    });
  const { data: categoryData, isLoading: fetchingCategory } =
    useReadCategoryNoAuthQuery();
  const { data: subCategoryData, isLoading: fetchingSubCategory } =
    useReadSubCategoryFromCategoryNoAuthQuery(selectedCatId, {
      skip: !selectedCatId,
    });
  const { data: modelData, isLoading: fetchingModel } =
    useReadBrandModelListQuery(selectedBrandId, {
      skip: !selectedBrandId,
    });

  useEffect(() => {
    setSelectedCatId(values?.categoryId);
  }, [values?.categoryId]);
  useEffect(() => {
    if (type === "add") {
      setSelectedBrandId(values?.brandId?.value);
    } else {
      setSelectedBrandId(values?.brandId);
    }
  }, [values?.brandId]);

  const setThumbnailImage = (newFiles) => {
    setFieldValue("productImage", newFiles[0]);
  };

  const handleRemoveThumbnailImage = () => {
    setFieldValue("productImage", {});
  };

  const setNewFiles = (newFiles) => {
    const allFiles = [...values.productImages, ...newFiles];
    setFieldValue("productImages", allFiles);
  };

  const handleRemoveFile = (idx) => {
    const currentFiles = [...values.productImages];
    currentFiles.splice(idx, 1);
    setFieldValue("productImages", currentFiles);
  };

  const searchBrandsLoader = (brandName) => searchBrands(brandName, true);

  return (
    <>
      <div className="row-sm">
        <div className="three-col-sm">
          {type === "add" ? (
            <SelectAsync
              label="Brand"
              placeholder="Search Brand"
              error={touched?.brandId ? errors?.brandId : ""}
              dataLoader={searchBrandsLoader}
              onChange={(selectedBrand) => {
                setFieldTouched("brandId");
                setFieldValue("brandId", selectedBrand);
                setFieldValue("modelId", "");
              }}
              onBlur={() => {
                setFieldTouched("brandId");
              }}
              value={values.brandId}
            />
          ) : (
            <SelectWithoutCreate
              required
              loading={fetchingBrands}
              label="Brand"
              placeholder="E.g: Rynox"
              error={touched?.brandId ? errors?.brandId : ""}
              value={values.brandId}
              onChange={(selectedBrand) => {
                setFieldTouched("brandId");
                setFieldValue("brandId", selectedBrand.value);
              }}
              components={{ Option }}
              options={brandData?.map((brand) => ({
                value: brand.brandId,
                // label: brand.brandName,
                label: brand.brandName,
              }))}
              onBlur={() => {
                setFieldTouched("brandId");
              }}
            />
          )}
        </div>

        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={fetchingModel}
            label="Model"
            placeholder="E.g: Rynox"
            error={touched?.modelId ? errors?.modelId : ""}
            value={values.modelId}
            onChange={(selectedModel) => {
              setFieldTouched("modelId");
              setFieldValue("modelId", selectedModel.value);
            }}
            options={modelData?.map((model) => ({
              value: model.id,
              label: model.modelName,
            }))}
            onBlur={() => {
              setFieldTouched("modelId");
            }}
            isDisabled={values?.brandId === ""}
          />
        </div>
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={fetchingCategory}
            label="Category"
            placeholder="E.g: Riding Gears"
            error={touched?.categoryId ? errors?.categoryId : ""}
            value={values.categoryId}
            onChange={(selectedCategory) => {
              setFieldTouched("categoryId");
              setFieldValue("categoryId", selectedCategory.value);
              setSelectedCatId(selectedCategory?.value);
            }}
            options={categoryData?.map((d) => ({
              value: d?.categoryId,
              label: d?.categoryName,
            }))}
            onBlur={() => {
              setFieldTouched("categoryId");
            }}
          />
        </div>
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            loading={fetchingSubCategory}
            label="Sub Category"
            placeholder="E.g: Helmets"
            error={touched?.subCategoryId ? errors?.subCategoryId : ""}
            value={values.subCategoryId}
            onChange={(selectedSubCategory) => {
              setFieldTouched("subCategoryId");
              setFieldValue("subCategoryId", selectedSubCategory.value);
            }}
            options={subCategoryData?.map((d) => ({
              value: d?.id,
              label: d?.subCategoryName,
            }))}
            onBlur={() => {
              setFieldTouched("subCategoryId");
            }}
            isDisabled={values?.categoryId === ""}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="Highlight"
            name="highLight"
            type="text"
            placeholder="E.g: Windproof Leather"
            required={false}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="Cost Price (NRs.)"
            name="adminPrice"
            type="text"
            placeholder="E.g: 4,000"
            required={false}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="MRP (NRs.)"
            name="costPrice"
            type="text"
            placeholder="E.g: 4,000"
            onChange={(e) => {
              setFieldValue("costPrice", e.target.value);
              setFieldValue("discountedPrice", e.target.value);
            }}
          />
        </div>

        <div className="three-col-sm">
          <Input
            label="Discounted Price (NRs.)"
            name="discountedPrice"
            type="text"
            placeholder="E.g: 2,000"
            required={true}
            disabled={values?.costPrice === ""}
          />
        </div>
        <div className="three-col-sm">
          <DropZone
            label="Product Image (Accepts Single Thumbnail Image)"
            required
            currentFiles={
              Object.keys(values.productImage).length
                ? [values.productImage]
                : []
            }
            setNewFiles={setThumbnailImage}
            handleRemoveFile={handleRemoveThumbnailImage}
            error={touched?.productImage ? errors?.productImage : ""}
            maxFiles={1}
          />
        </div>
        <div className="three-col-sm">
          <DropZone
            label="Product Gallery (Accepts Multiple Images)"
            required
            currentFiles={values.productImages}
            setNewFiles={setNewFiles}
            handleRemoveFile={handleRemoveFile}
            error={touched?.productImages ? errors?.productImages : ""}
          />
        </div>
      </div>
    </>
  );
};

export default BasicDetails;
