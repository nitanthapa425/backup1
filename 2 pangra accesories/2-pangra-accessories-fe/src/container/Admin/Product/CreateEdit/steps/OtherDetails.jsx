import { useState, useEffect } from "react";
// import Input from "components/Input";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import ReactSelect from "components/Select/ReactSelect";
import Editor from "components/Editor";
import { numbersArrGenerator } from "constant/constant";

import {
  useCreateTagMutation,
  useReadTagsQuery,
} from "services/api/ProductService";
import Select from "components/Select";
// import { propTypes } from "react-modal-videojs";

const OtherDetails = ({
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
  // type,
}) => {
  const [editorLoaded, setEditorLoaded] = useState(false);

  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  useEffect(() => {
    setEditorLoaded(true);
  }, []);

  const { data: tagsDataValue } = useReadTagsQuery();

  const [createBodyType, { isLoading: isLoadingCreateBodyType }] =
    useCreateTagMutation();
  return (
    <>
      <div className="row-sm">
        {/* {type === "edit" && (
          <div className="three-col-sm">
            <Input
              required={false}
              label="Product Code"
              name="productCode"
              type="text"
              placeholder="E.g: AB-XXX-XX"
              disabled
            />
          </div>
        )} */}
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            label="Warranty Available"
            placeholder="E.g: Yes or No"
            error={touched?.hasWarranty ? errors?.hasWarranty : ""}
            value={values.hasWarranty}
            onChange={(selectedData) => {
              setFieldTouched("hasWarranty");
              setFieldValue("hasWarranty", selectedData.value);
              // console.log("selectedData.value", !!selectedData.value);
              if (selectedData.value === "false") {
                setFieldValue("warrantyTime", "");
                setFieldValue("warrantyUnit", "");
              }
            }}
            options={[
              {
                value: "true",
                label: "Yes",
              },
              {
                value: "false",
                label: "No",
              },
            ]}
            onBlur={() => {
              setFieldTouched("hasWarranty");
            }}
          />
        </div>

        {/* <div className="three-col-sm">
          <SelectWithoutCreate
            required={values?.hasWarranty === "true"}
            label="Warranty Given In"
            placeholder="E.g: month or year"
            error={touched?.warrantyUnit ? errors?.warrantyUnit : ""}
            value={values.warrantyUnit}
            onChange={(selectedData) => {
              setFieldTouched("warrantyUnit");
              setFieldValue("warrantyUnit", selectedData.value);
            }}
            options={[
              {
                value: "month",
                label: "month",
              },
              {
                value: "year",
                label: "year",
              },
            ]}
            onBlur={() => {
              setFieldTouched("warrantyUnit");
            }}
            isDisabled={
              values?.hasWarranty === "false" || values?.hasWarranty === ""
            }
          />
        </div> */}

        <div className="three-col-sm flex">
          <div className="flex-1">
            <SelectWithoutCreate
              required={values?.hasWarranty === "true"}
              label="Warranty Time"
              placeholder="E.g: 1 month"
              error={touched?.warrantyTime ? errors?.warrantyTime : ""}
              value={values.warrantyTime}
              onChange={(selectedData) => {
                setFieldTouched("warrantyTime");
                setFieldValue("warrantyTime", selectedData.value);
              }}
              options={numbersArrGenerator(1, 12)?.map((d) => ({
                value: d.toString(),
                label: d,
              }))}
              onBlur={() => {
                setFieldTouched("warrantyTime");
              }}
              isDisabled={
                values?.hasWarranty === "false" || values?.hasWarranty === ""
              }
            />
          </div>
          <div className="mt-[25px]" style={{ width: "6.2rem" }}>
            <Select
              name="warrantyUnit"
              disabled={
                values?.hasWarranty === "false" || values?.hasWarranty === ""
              }
            >
              {[
                {
                  value: "month",
                  label: "month",
                },
                {
                  value: "year",
                  label: "year",
                },
              ].map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        <div className="three-col-sm">
          <SelectWithoutCreate
            required
            label="Is Returnable"
            placeholder="E.g: Yes or No"
            error={touched?.isReturnable ? errors?.isReturnable : ""}
            value={values.isReturnable}
            onChange={(selectedData) => {
              setFieldTouched("isReturnable");
              setFieldValue("isReturnable", selectedData.value);
              if (selectedData.value === "false") {
                setFieldValue("returnableUnit", "");
                setFieldValue("returnableTime", "");
              }
            }}
            options={[
              {
                value: "true",
                label: "Yes",
              },
              {
                value: "false",
                label: "No",
              },
            ]}
            onBlur={() => {
              setFieldTouched("isReturnable");
            }}
          />
        </div>
        {/* <div className="three-col-sm">
          <SelectWithoutCreate
            required={values?.isReturnable === "true"}
            label="Returnable In"
            placeholder="E.g: day, week or month"
            error={touched?.returnableUnit ? errors?.returnableUnit : ""}
            value={values.returnableUnit}
            onChange={(selectedData) => {
              setFieldTouched("returnableUnit");
              setFieldValue("returnableUnit", selectedData.value);
            }}
            options={[
              {
                value: "day",
                label: "day",
              },
              {
                value: "week",
                label: "week",
              },
              {
                value: "month",
                label: "month",
              },
            ]}
            onBlur={() => {
              setFieldTouched("returnableUnit");
            }}
            isDisabled={
              values?.isReturnable === "false" || values?.isReturnable === ""
            }
          />
        </div> */}
        <div className="three-col-sm flex">
          <div className="flex-1">
            <SelectWithoutCreate
              required={values?.isReturnable === "true"}
              label="Returnable Time"
              placeholder="E.g: 1 day"
              error={touched?.returnableTime ? errors?.returnableTime : ""}
              value={values.returnableTime}
              onChange={(selectedData) => {
                setFieldTouched("returnableTime");
                setFieldValue("returnableTime", selectedData.value);
              }}
              options={numbersArrGenerator(1, 32)?.map((d) => ({
                value: d.toString(),
                label: d,
              }))}
              onBlur={() => {
                setFieldTouched("returnableTime");
              }}
              isDisabled={
                values?.isReturnable === "false" || values?.isReturnable === ""
              }
            />
          </div>
          <div className="mt-[25px]" style={{ width: "6.2rem" }}>
            <Select
              name="returnableUnit"
              disabled={
                values?.isReturnable === "false" || values?.isReturnable === ""
              }
            >
              {[
                {
                  value: "day",
                  label: "day",
                },
                {
                  value: "week",
                  label: "week",
                },
                {
                  value: "month",
                  label: "month",
                },
              ].map((d, i) => (
                <option key={i} value={d?.value}>
                  {d?.label}
                </option>
              ))}
            </Select>
          </div>
        </div>
        {/* <div className="three-col-sm">
          <SelectWithoutCreate
            required
            label="Is on Sale"
            placeholder="E.g: Yes or No"
            error={touched?.onSale ? errors?.onSale : ""}
            value={values.onSale}
            onChange={(selectedData) => {
              setFieldTouched("onSale");
              setFieldValue("onSale", selectedData.value);
            }}
            options={[
              {
                value: "true",
                label: "Yes",
              },
              {
                value: "false",
                label: "No",
              },
            ]}
            onBlur={() => {
              setFieldTouched("onSale");
            }}
          />
        </div> */}

        <div className="three-col-sm">
          <ReactSelect
            label="Slugs"
            placeholder="E.g: Helmets, Windproof"
            error={touched?.tags ? errors?.tags : ""}
            loading={isLoadingCreateBodyType}
            value={values.tags || []}
            onChange={(selectedBodyType, actionMeta) => {
              setFieldTouched("tags");
              setFieldValue(
                "tags",
                selectedBodyType.map((v) => v.value)
              );
              if (
                selectedBodyType[0] &&
                actionMeta.action === "create-option"
              ) {
                createBodyType({
                  tagName: selectedBodyType[0].value,
                });
              }
            }}
            options={tagsDataValue?.map((tagName) => ({
              value: tagName?.tagName,
              label: tagName?.tagName,
            }))}
            onBlur={() => {
              setFieldTouched("tags");
            }}
            isMulti={true}
          />
        </div>
        <div className="two-col-sm product-description-editor">
          <Editor
            name="description"
            onChange={(data) => {
              setFieldTouched("description");
              setFieldValue("description", data);
            }}
            value={values?.description || ""}
            editorLoaded={editorLoaded}
            label="Product Description"
            onBlur={() => {
              setFieldTouched("description");
            }}
            touched={touched?.description}
            errors={errors?.description}
          />
        </div>
        <div className="two-col-sm product-description-editor">
          <Editor
            name="shortDescription"
            onChange={(data) => {
              setFieldTouched("shortDescription");
              setFieldValue("shortDescription", data);
            }}
            value={values?.shortDescription || ""}
            editorLoaded={editorLoaded}
            label="Short Description"
            onBlur={() => {
              setFieldTouched("description");
            }}
            touched={touched?.shortDescription}
            errors={errors?.shortDescription}
          />
        </div>
      </div>
    </>
  );
};

export default OtherDetails;
