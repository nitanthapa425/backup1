import React, { useEffect } from "react";
import ReactModal from "react-modal";
import { useToasts } from "react-toast-notifications";
import { useAddToCartAdminMutation } from "services/api/adminCartList";
import OrderItem from "../SellNow/steps/OrderItem";

const AddToGroupModal = ({
  open,
  closeModal,
  productName,
  productId,
  productAvailableOptions,
}) => {
  const { addToast } = useToasts();

  const [addToCart, { isLoading, isSuccess, isError, error }] =
    useAddToCartAdminMutation();
  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully added to group", {
        appearance: "success",
      });
      closeModal();
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "Error occurred while adding to group. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <>
      <ReactModal
        isOpen={open}
        onRequestClose={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
        id="OrderItemModal"
        preventScroll={false}
        shouldCloseOnOverlayClick={false}
        shouldCloseOnEsc={false}
      >
        <button
          type="button"
          onClick={closeModal}
          className="text-error absolute top-[10px] right-[10px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <div className="container">
          <h1 className="h6 mb-3">
            Add to Group: <span className="text-primary">{productName}</span>
          </h1>
          <OrderItem
            productAvailableOptions={productAvailableOptions}
            productId={productId}
            addToGroup={addToCart}
            type="addToGroup"
            addingToGroup={isLoading}
          />
        </div>
      </ReactModal>
    </>
  );
};

export default AddToGroupModal;
