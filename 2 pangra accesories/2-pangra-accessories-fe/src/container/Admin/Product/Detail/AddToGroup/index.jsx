/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import { useAddToCartAdminMutation } from "services/api/adminCartList";

const AddToGroup = ({ productId, productName, productAvailableOptions }) => {
  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const openModal = () => setOpen(true);

  const { addToast } = useToasts();

  const [addToCart, { isLoading, isSuccess, isError, error }] =
    useAddToCartAdminMutation();
  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully added to group", {
        appearance: "success",
      });
      closeModal();
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "Error occurred while adding to group. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <>
      <Button
        loading={isLoading}
        variant="primary"
        type="button"
        onClick={() => {
          addToCart({ productId: productId });
        }}
      >
        Add To Group
      </Button>
      {/* <AddToGroupModal
        productName={productName}
        productId={productId}
        open={open}
        closeModal={closeModal}
        productAvailableOptions={productAvailableOptions}
      /> */}
    </>
  );
};

export default AddToGroup;
