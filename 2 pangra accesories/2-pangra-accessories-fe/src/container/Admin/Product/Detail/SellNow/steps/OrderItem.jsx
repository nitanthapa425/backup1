import Button from "components/Button";
import Input from "components/Input";
import Select from "components/Select";
// import { productQuantity } from 'constant/constant';
import { Form, Formik } from "formik";
import React, { useCallback, useEffect, useMemo } from "react";
import { useToasts } from "react-toast-notifications";
import { useCreateOrderItemMutation } from "services/api/ProductService";
import { disableOneOf } from "utils/disableOneOf";
import { nNumberArray } from "utils/nNumberOfArray";
import { sellNowValidationSchema } from "validation/product.validation";

const OrderItem = ({
  productAvailableOptions,
  productId,
  setOrderGroupId,
  next,
  addToGroup,
  type,
  addingToGroup,
}) => {
  const { addToast } = useToasts();

  const productAvailableSize = useMemo(() => {
    const productAvailableSizeValue = productAvailableOptions?.map?.(
      (value, i) => value.size
    );
    const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
    return uniqueAvailableSizeValue;
  }, [productAvailableOptions]);

  const getProductAvailableColorBySize = useCallback(
    (size) => {
      const productAvailableColors = productAvailableOptions
        ?.filter?.((value, i) => value.size === size)
        .map?.((value, i) => value.color);

      const uniqueAvailableColor = [...new Set(productAvailableColors)];
      return uniqueAvailableColor;
    },
    [productAvailableOptions]
  );

  const getProductAvailableQuantityByColorSize = useCallback(
    (size, color) => {
      const product = productAvailableOptions?.find?.(
        (value, i) => value.size === size && value.color === color
      );
      if (product) return +product.quantity;
      return 0;
    },
    [productAvailableOptions]
  );

  const [createOrderItem, { isLoading, isSuccess, isError, error, data }] =
    useCreateOrderItemMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully created the order", {
        appearance: "success",
      });
      // apply only for sell now
      if (type !== "addToGroup") {
        setOrderGroupId(data?.orderGroupId);
        next();
      }
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "Error occurred while creating order. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  const handleCreateOrderItem = useCallback(
    (values) => {
      if (type === "addToGroup") {
        const data = {
          productId,
          data: values,
        };
        addToGroup(data);
      } else {
        const data = {
          bookList: [
            {
              ...values,
              id: productId,
            },
          ],
        };
        createOrderItem(data); // for sell now
      }
    },
    [productId, type]
  );
  return (
    <>
      <Formik
        initialValues={{
          color: "",
          discountedPrice: "",
          quantity: "",
          size: "",
        }}
        onSubmit={(values, { setSubmitting }) => {
          handleCreateOrderItem(values);
          setSubmitting(false);
        }}
        validationSchema={sellNowValidationSchema}
        enableReinitialize
      >
        {({ dirty, values, setFieldValue }) => (
          <Form>
            <div className="select-holder  text-left py-5  ">
              <div>
                <Select
                  label="Size"
                  required
                  name="size"
                  disabled={disableOneOf(
                    `${productAvailableSize?.length}` === `0`
                  )}
                  onChange={(e) => {
                    setFieldValue("size", e.target.value);
                    setFieldValue("color", "");
                    setFieldValue("quantity", "");
                    setFieldValue("discountedPrice", "");
                  }}
                >
                  <option value="" selected disabled={true}>
                    Select Size
                  </option>
                  {productAvailableSize?.map((data, i) => (
                    <option key={i} value={data}>
                      {data}
                    </option>
                  ))}
                </Select>
              </div>
              <div>
                <Select
                  label="Color"
                  required
                  name="color"
                  className={`${
                    disableOneOf(
                      !values.size,
                      `${
                        getProductAvailableColorBySize(values.size)?.length
                      }` === `0`
                    )
                      ? "disabled-input"
                      : ""
                  }`}
                  onChange={(e) => {
                    setFieldValue("color", e.target.value);
                    setFieldValue("quantity", "");
                  }}
                  disabled={disableOneOf(
                    !values.size,
                    `${
                      getProductAvailableColorBySize(values.size)?.length
                        ?.length
                    }` === `0`
                  )}
                >
                  <option value="" selected disabled={true}>
                    Select Color
                  </option>

                  {getProductAvailableColorBySize(values.size)?.map?.(
                    (data, i) => (
                      <option key={i} value={data}>
                        {data}
                      </option>
                    )
                  )}
                </Select>
              </div>
              <div>
                <Select
                  label="Quantity"
                  required
                  name="quantity"
                  className={`${
                    disableOneOf(!values.size, !values.color)
                      ? "disabled-input"
                      : ""
                  }`}
                  disabled={disableOneOf(!values.size, !values.color)}
                >
                  <option value="" selected disabled={true}>
                    Select Quantity
                  </option>

                  {nNumberArray(
                    getProductAvailableQuantityByColorSize(
                      values.size,
                      values.color
                    )
                  ).map((data, i) => (
                    <option key={i} value={data}>
                      {data}
                    </option>
                  ))}
                </Select>
              </div>
            </div>
            <Input
              label="Selling Price"
              name="discountedPrice"
              type="text"
              p
              placeholder="E.g: 5000"
              required
            />
            <div className="mt-3">
              <Button
                loading={isLoading || addingToGroup}
                disabled={isLoading || !dirty || addingToGroup}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              <Button
                disabled={isLoading || !dirty || addingToGroup}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default OrderItem;
