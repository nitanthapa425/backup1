import Button from "components/Button";
import Input from "components/Input";
import { Form, Formik } from "formik";
import React, { useEffect } from "react";
import { useCheckCustomerByMobileMutation } from "services/api/customer";
import { checkMobileNumberValidationSchema } from "validation/product.validation";

const CheckMobile = ({ setMobile, next, setCustomerFullName, closeModal }) => {
  const [checkMobile, { data, isLoading, isSuccess, isError }] =
    useCheckCustomerByMobileMutation();

  useEffect(() => {
    if (isSuccess && data) {
      setCustomerFullName(data);
      next();
    }
  }, [isSuccess, data]);

  useEffect(() => {
    if (isError) {
      setCustomerFullName(null);
      next();
    }
  }, [isError]);

  const skipStep = () => {
    setMobile("");
    closeModal();
    // next();
  };

  return (
    <>
      <Formik
        initialValues={{ mobile: "" }}
        onSubmit={(values) => checkMobile(values.mobile)}
        validationSchema={checkMobileNumberValidationSchema}
        enableReinitialize
      >
        {({ dirty, setFieldValue }) => (
          <Form>
            <Input
              label="Mobile Number"
              name="mobile"
              type="text"
              placeholder="98XXXXXXXX"
              required
              onChange={(e) => {
                setFieldValue("mobile", e.target.value);
                setMobile(e.target.value);
              }}
            />
            <div className="mt-3">
              <Button
                loading={isLoading}
                disabled={isLoading || !dirty}
                type="submit"
                className="btn-lg"
              >
                Check
              </Button>
              <Button
                onClick={() => skipStep()}
                type="button"
                className="btn-lg"
              >
                Skip
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default CheckMobile;
