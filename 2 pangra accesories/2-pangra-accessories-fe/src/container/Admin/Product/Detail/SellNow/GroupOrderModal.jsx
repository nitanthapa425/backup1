import React, { useState } from "react";
import ReactModal from "react-modal";
import Steps, { Step } from "rc-steps";

import CheckMobile from "./steps/CheckMobile";
import CreateCustomer from "./steps/CreateCustomer";

import "rc-steps/assets/index.css";
import "rc-steps/assets/iconfont.css";

const GroupOrderModal = ({ open, closeModal, orderGroupId }) => {
  const [currentStep, setCurrentStep] = useState(0);
  const [mobile, setMobile] = useState("");
  const [customerFullName, setCustomerFullName] = useState(null);

  // const goPrevStep = () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null)
  const goNextStep = () =>
    currentStep < 1 ? setCurrentStep((prev) => prev + 1) : null;
  const resetStep = () => setCurrentStep(0);

  return (
    <>
      <ReactModal
        isOpen={open}
        onRequestClose={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
        id="OrderItemModal"
        preventScroll={false}
        shouldCloseOnOverlayClick={false}
        shouldCloseOnEsc={false}
      >
        <button
          type="button"
          onClick={closeModal}
          className="text-error absolute top-[10px] right-[10px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <div className="">
          <Steps current={currentStep} direction="vertical">
            <Step
              title="Check Mobile"
              description={
                currentStep === 0 ? (
                  <CheckMobile
                    setMobile={setMobile}
                    next={goNextStep}
                    setCustomerFullName={setCustomerFullName}
                    closeModal={closeModal}
                  />
                ) : null
              }
            />
            <Step
              title="Find/Create Customer"
              description={
                currentStep === 1 ? (
                  <CreateCustomer
                    mobile={mobile}
                    closeModal={closeModal}
                    orderGroupId={orderGroupId}
                    customerFullName={customerFullName}
                    resetStep={resetStep}
                  />
                ) : null
              }
            />
          </Steps>
        </div>
      </ReactModal>
    </>
  );
};

export default GroupOrderModal;
