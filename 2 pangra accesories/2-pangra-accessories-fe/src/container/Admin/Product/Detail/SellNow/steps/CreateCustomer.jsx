import Button from 'components/Button'
import Input from 'components/Input'
import { Form, Formik } from 'formik'
import React, { useEffect } from 'react'
import { useToasts } from 'react-toast-notifications'
import { useCreateCustomerAfterOrderMutation } from 'services/api/customer'
import { customerByAdminValidationSchema } from 'validation/customer.validation'

const CreateCustomer = ({ mobile, orderGroupId, closeModal, customerFullName, resetStep, prevStep }) => {
  const { addToast } = useToasts()

  const [createCustomer, {
    isLoading: isCreatingCustomer,
    data: createCustomerData,
    error: createCustomerError,
    isSuccess: createCustomerSuccess,
    isError: createCustomerErrorStatus
  }] = useCreateCustomerAfterOrderMutation()

  const handleCreateCustomer = (values) => {
    createCustomer({ orderGroupId, data: values })
  }


  useEffect(() => {
    if (createCustomerSuccess) {
      addToast(createCustomerData?.message || "Successfully created the order", {
        appearance: "success",
      });
      closeModal()
      resetStep()
    }
  }, [createCustomerData, createCustomerSuccess]);
  useEffect(() => {
    if (createCustomerErrorStatus) {
      addToast(
        createCustomerError?.data?.message || "Error occurred while creating order. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [createCustomerErrorStatus]);
  return customerFullName ? (
    <div>
      <label className='mb-3 block'>Customer Full Name:{' '}
        <strong>{customerFullName}</strong>
      </label>
      <Button
        loading={isCreatingCustomer}
        type="submit"
        className="btn-lg"
        onClick={() => {
          handleCreateCustomer({ fullName: customerFullName, mobile })
        }}
      >
        Complete Order
      </Button>
    </div>
  ) : (
    <>
      <Formik
        initialValues={{ mobile, firstName: '', lastName: '', middleName: '' }}
        onSubmit={handleCreateCustomer}
        validationSchema={customerByAdminValidationSchema}
        enableReinitialize
      >
        {({ dirty }) => (
          <Form>
            <Input
              label="Mobile Number"
              name="mobile"
              type="text"
              placeholder="98XXXXXXXX"
              required
              disabled
              values={mobile}
            />
            <Input
              label="First Name"
              name="firstName"
              type="text"
              placeholder="First Name"
              required
            />
            <Input
              label="Middle Name"
              name="middleName"
              type="text"
              placeholder="Middle Name"
              required={false}
            />
            <Input
              label="Last Name"
              name="lastName"
              type="text"
              placeholder="Last Name"
              required
            />
            <div className="mt-3">
              <Button
                loading={isCreatingCustomer}
                disabled={isCreatingCustomer || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>
              <Button
                onClick={() => prevStep()}
                type="button"
                className="btn-lg"
              >
                Prev
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  )
}

export default CreateCustomer