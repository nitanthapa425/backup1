import { useState } from "react";

import Button from "components/Button";

import OrderModal from "./OrderModal";

const SellNow = ({ productAvailableOptions, productName, productId }) => {
  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const openModal = () => setOpen(true);
  return (
    <>
      <Button variant="primary" type="button" onClick={openModal}>
        Sell Now
      </Button>
      <OrderModal
        open={open}
        closeModal={closeModal}
        productAvailableOptions={productAvailableOptions}
        productName={productName}
        productId={productId}
      />
    </>
  );
};

export default SellNow;
