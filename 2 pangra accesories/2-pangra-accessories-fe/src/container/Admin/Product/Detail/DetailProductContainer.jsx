import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import Link from "next/link";
import parse from "html-react-parser";
import Zoom from "react-medium-image-zoom";

import AdminLayout from "layouts/Admin";
import Button from "components/Button";
import Tabs from "components/Tabs";
import ImgGallery from "components/ImgGallery";
// import { useReadVehicleDetailsQuery } from 'services/api/vehicle';
import { useReadProductDetailsQuery } from "services/api/ProductService";
import BreadCrumb from "components/BreadCrumb/breadCrumb";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import SellNow from "./SellNow/index";
import AddToGroup from "./AddToGroup/index";

import "react-medium-image-zoom/dist/styles.css";

function DetailProductContainer() {
  const router = useRouter();
  const [accordionData, setAccordionData] = useState([]);
  const [imageData, setImageData] = useState([]);

  const {
    data: vehicleDetails,
    isError: isVehicleError,
    isLoading: isLoadingVehicle,
  } = useReadProductDetailsQuery(router.query.id, {
    skip: !router.query.id,
  });

  const tabHeadings = ["Basic Details", "Dimension and Sizes", "Other Details"];

  const dimFunc = (l, b, h, dimUnit) => {
    if (!l && !b && !h) {
      return "-";
    } else {
      return `${l || "-"} x ${b || "-"} x ${h || "-"} ${
        dimUnit ? "(" + dimUnit + ")" : ""
      }`;
    }
  };

  useEffect(() => {
    if (vehicleDetails) {
      // Thumbnail Image
      const images = [
        {
          original: vehicleDetails?.productImage?.imageUrl,
          thumbnail: vehicleDetails?.productImage?.imageUrl,
        },
      ];
      // Product Images
      const moreImages = vehicleDetails?.productImages.map((img) => ({
        original: img.imageUrl,
        thumbnail: img.imageUrl,
      }));
      // First thumbnail will be shown in the gallery, then other images.
      setImageData([...images, ...moreImages] || []);
      setAccordionData([
        {
          heading: "Product Intro",
          tabNumber: 1,
          list: [
            {
              label: "Product Title",
              data: vehicleDetails?.productTitle,
            },
            {
              label: "Brand",
              data: vehicleDetails?.brandName,
            },
            {
              label: "Category",
              data: vehicleDetails?.categoryName,
            },
            {
              label: "Sub Category",
              data: vehicleDetails?.subCategoryName,
            },
            {
              label: "Highlight",
              data: vehicleDetails?.highLight,
            },
            {
              label: "Slugs",
              data: vehicleDetails?.tags.join(" , "),
            },
          ],
        },
        {
          heading: "Inventory",
          tabNumber: 1,
          list: [
            // {
            //   label: "Product Code",
            //   data: vehicleDetails?.availableOptions[0]?.productCode,
            // },
            {
              label: "Out of Stock",
              data: vehicleDetails?.outOfStock ? "Yes" : "No",
            },
            {
              label: "Status (Is active or deleted)",
              data: vehicleDetails?.status ? "Active" : "Inactive",
            },
            {
              label: "Available Colors",
              data: vehicleDetails?.color.join(" , "),
            },
            {
              label: "Quantity",
              data: vehicleDetails?.availableOptions[0]?.quantity,
            },

            {
              label: "Cost Price (NRs)",
              data: `${
                vehicleDetails?.adminPrice
                  ? "NRs. " +
                    thousandNumberSeparator(vehicleDetails?.adminPrice) +
                    " /-"
                  : ""
              }`,
            },
            {
              label: "MRP (NRs.)",
              data: `${
                vehicleDetails?.costPrice
                  ? "NRs. " +
                    thousandNumberSeparator(vehicleDetails?.costPrice) +
                    " /-"
                  : ""
              }`,
            },

            {
              label: "Discounted Price (NRs.)",
              data: `${
                vehicleDetails?.discountedPrice
                  ? "NRs. " +
                    thousandNumberSeparator(vehicleDetails?.discountedPrice) +
                    " /-"
                  : ""
              }`,
            },
            {
              label: "Discount Percent",
              data: `${
                vehicleDetails?.discountPercentage
                  ? vehicleDetails?.discountPercentage + "%"
                  : "N/A"
              }`,
            },
          ],
        },
        {
          heading: "Weight & Dimensions",
          tabNumber: 2,
          list: [
            {
              label: "Weight",
              data: vehicleDetails?.weight,
            },
            {
              label: "Dimensions (l x b x h)",
              data: dimFunc(
                vehicleDetails?.dimension?.length,
                vehicleDetails?.dimension?.breadth,
                vehicleDetails?.dimension?.height,
                "mm"
              ),
              // data: `${vehicleDetails?.dimension?.length ?
              //     vehicleDetails?.dimension?.length : "-"} x ${vehicleDetails?.dimension?.breadth ?
              //         vehicleDetails?.dimension?.breadth : "-"} x ${vehicleDetails?.dimension?.height ?
              //             vehicleDetails?.dimension?.height : "-"} ${vehicleDetails?.dimensionUnit ?
              //                 "(" + vehicleDetails?.dimensionUnit + ")" :
              //                 ""}`,
            },
          ],
        },
        {
          heading: "Sizes",
          tabNumber: 2,
          list: [
            {
              label: "Available Sizes",
              data: vehicleDetails?.sizeValue.join(" , "),
            },
            {
              label: "Size Chart Name",
              data: vehicleDetails?.sizeChart?.imageName,
            },
            {
              label: "Size Chart Image",
              data: (
                <span>
                  {vehicleDetails?.sizeChart?.imageUrl ? (
                    <Zoom zoomMargin={40}>
                      <img
                        className=" inline-block"
                        src={vehicleDetails?.sizeChart?.imageUrl}
                        height="100px"
                        width="100px"
                      ></img>
                    </Zoom>
                  ) : (
                    <span>-</span>
                  )}
                </span>
              ),
            },
          ],
        },
        {
          heading: "Warranty & Returnable Time",
          tabNumber: 3,
          list: [
            {
              label: "Warranty Time",
              data:
                vehicleDetails?.hasWarranty === true
                  ? vehicleDetails?.warrantyTime
                  : "N/A",
            },
            {
              label: "Returnable Time",
              data:
                vehicleDetails?.isReturnable === true
                  ? vehicleDetails?.returnableTime
                  : "N/A",
            },
          ],
        },
        {
          heading: "Is Added In",
          tabNumber: 3,
          list: [
            {
              label: "Is Added in Featured",
              data: vehicleDetails?.hasInFeatured ? "Yes" : "No",
            },
            {
              label: "Is Added in Hot Deals",
              data: vehicleDetails?.hasInHotDeal ? "Yes" : "No",
            },
            {
              label: "Is Added in Flash Sale",
              data: vehicleDetails?.isForFlashSale ? "Yes" : "No",
            },
          ],
        },
      ]);
    }
  }, [vehicleDetails]);
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Product List",
      route: "/admin/product",
    },
    {
      routeName: "Product Details",
      route: "",
    },
  ];

  return (
    <AdminLayout documentTitle="Product Details">
      <main className="container">
        <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        {isVehicleError ? (
          <p>Error Loading Data</p>
        ) : isLoadingVehicle ? (
          <p>Loading Product Details</p>
        ) : vehicleDetails ? (
          <>
            <div className="flex justify-end mb-3">
              <SellNow
                productAvailableOptions={vehicleDetails?.availableOptions}
                productName={vehicleDetails?.productTitle}
                productId={vehicleDetails?.id}
              />
              <AddToGroup
                productId={vehicleDetails?.id}
                productName={vehicleDetails?.productTitle}
                productAvailableOptions={vehicleDetails?.availableOptions}
              />
              <span className="border mr-2 h-10 border-neutral-700" />
              <Link href={`/admin/product/update/${vehicleDetails.id}`}>
                <a>
                  <Button variant="outlined" type="button">
                    Edit
                  </Button>
                </a>
              </Link>
            </div>
            <div className="container">
              <div className="row">
                <div className="two-col mb-5 detail-image-gallery">
                  <ImgGallery images={imageData} />
                  <Tabs
                    tabHeadings={tabHeadings}
                    hasAccordion={true}
                    accordionData={accordionData}
                  />
                </div>
                <div className="two-col">
                  {/* {console.log(dimFunc(vehicleDetails?.dimension?.length, vehicleDetails?.dimension?.breadth, vehicleDetails?.dimension?.height, vehicleDetails?.dimensionUnit))} */}
                  <div className="px-4 pt-6 border border-gray-300">
                    <h4 className="h4 mb-1">{vehicleDetails?.productTitle}</h4>
                    <h4 className="h6 mb-1">{vehicleDetails?.highLight}</h4>
                    <div className="pt-2">
                      <p className="font-bold mb-2">
                        Cost Price:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.adminPrice
                            ? "NRs. " + vehicleDetails?.adminPrice + " /-"
                            : "N/A"}
                        </span>
                      </p>
                      <p className="font-bold mb-2">
                        MRP:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.costPrice
                            ? "NRs. " + vehicleDetails?.costPrice + " /-"
                            : "-"}
                        </span>
                      </p>
                      <p className="font-bold mb-2">
                        Discounted Price:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.discountedPrice
                            ? "NRs. " + vehicleDetails?.discountedPrice + " /-"
                            : "N/A"}
                        </span>
                      </p>
                      <p className="font-bold mb-2">
                        Status:&nbsp;
                        {vehicleDetails?.status ? (
                          <span className="badge-green">Active</span>
                        ) : (
                          <span className="badge-red">Inactive</span>
                        )}
                      </p>
                      <p className="font-bold mb-2">
                        Brand:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.brandName}
                        </span>
                      </p>
                      <p className="font-bold mb-2">
                        Category:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.categoryName}
                        </span>
                      </p>
                      <p className="font-bold mb-2">
                        Sub Category:{" "}
                        <span className="font-normal">
                          {vehicleDetails?.subCategoryName}
                        </span>
                      </p>
                    </div>
                  </div>
                  <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 my-4 shadow-lg">
                    <h3 className="h5 mb-2">Product Description</h3>
                    <div>{parse(vehicleDetails?.description)}</div>
                    <h3 className="h5 mb-2">Short Description</h3>
                    <div>
                      {parse(vehicleDetails?.shortDescription || `N/A`)}
                    </div>
                  </div>
                  <div>
                    {vehicleDetails?.availableOptions?.length > 0 ? (
                      <div className="available-option w-full  available-option-detail-page">
                        <table className="w-full">
                          <thead>
                            <tr>
                              <th>
                                Size
                                {vehicleDetails?.sizeCalculation === "Litres"
                                  ? "(Ltr)"
                                  : ""}
                              </th>
                              <th>Color</th>
                              <th>Quantity</th>
                              <th>Discount(%)&nbsp;&nbsp;</th>
                              <th>Discounted Price</th>
                              <th>Product Code</th>
                            </tr>
                          </thead>
                          <tbody>
                            {vehicleDetails?.availableOptions?.map(
                              (option, index) => {
                                return (
                                  <tr key={index}>
                                    <td>{option?.size}</td>
                                    <td>{option?.color}</td>
                                    <td>{option?.quantity}</td>
                                    <td>{option?.discountPercentage}</td>
                                    <td>{option?.discountedAmount}</td>
                                    <td>{option?.productCode || "N/A"}</td>
                                  </tr>
                                );
                              }
                            )}
                          </tbody>
                        </table>
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : null}

        {/* {vehicleDetails?.availableOptions?.length > 0 ? (
          <div className="w-full">
            <table className="w-full ">
              <thead>
                <tr>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>Discount Percentage&nbsp;&nbsp;</th>
                  <th>Discounted Price(NRs.)</th>
                </tr>
              </thead>
              <tbody>
                {vehicleDetails?.availableOptions?.map((option, index) => {
                  return (
                    <tr key={index}>
                      <td>{option?.color}</td>
                      <td>{option?.size}</td>
                      <td>{option?.quantity}</td>
                      <td>{option?.discountPercentage}</td>
                      <td>{option?.discountedAmount}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        ) : null} */}
      </main>
    </AdminLayout>
  );
}

export default DetailProductContainer;
