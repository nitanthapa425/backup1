import { useEffect } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import { beforeAfterDate } from "utils/beforeAfterdate";

import { useAddProductToFlashSaleMutation } from "services/api/ProductService";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { AddToFlashSaleValidation } from "validation/product.validation";
import Input from "components/Input";

const moment = require("moment");

const AddToFlashSaleForm = (props) => {
  const { addToast } = useToasts();

  const initialValues = {
    startDate: "",
    endDate: "",
    flashSalePrice: props.mrpPrice,
  };

  const [
    addProductToFlashSale,
    { isSuccess, isError, isLoading: isLoadingRejectingOfferPrice, error },
  ] = useAddProductToFlashSaleMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully added product to flash sale", {
        appearance: "success",
      });
      props.setIsOpen(false);
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message || "error occurred while adding to flash sale",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        addProductToFlashSale({
          ...values,
          accessoriesId: props.accessoriesId,
        });
        setSubmitting(false);
      }}
      validationSchema={AddToFlashSaleValidation(props.mrpPrice)}
      enableReinitialize
    >
      {({
        setFieldValue,
        setFieldTouched,
        values,
        errors,
        touched,
        // resetForm,
        // isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">Add To Flash Sale</h1>

          <Form>
            <div className="w-full mb-2">
              <label htmlFor="">
                Start Date
                <span className="required">*</span>
              </label>
              <DatePicker
                selected={
                  values?.startDate ? new Date(values?.startDate) : null
                }
                onChange={(date) => {
                  setFieldValue("startDate", date?.toISOString());
                  setFieldTouched("startDate");
                }}
                onBlur={() => {
                  setFieldTouched("startDate");
                }}
                showTimeSelect
                dateFormat="MM/dd/yyyy h:mm aa"
                placeholderText="Select start date and time"
                minDate={beforeAfterDate(new Date(), 0, 0, 0)}
              />
              {touched?.startDate && errors?.startDate && (
                <div className="text-error text-sm">{errors.startDate}</div>
              )}

              <label htmlFor="">
                End Date
                <span className="required">*</span>
              </label>
              <DatePicker
                selected={values?.endDate ? new Date(values?.endDate) : null}
                onChange={(date) => {
                  setFieldValue("endDate", date?.toISOString());
                  setFieldTouched("endDate");
                }}
                onBlur={() => {
                  setFieldTouched("endDate");
                }}
                showTimeSelect
                dateFormat="MM/dd/yyyy h:mm aa"
                placeholderText="Select end date and time"
                disabled={!values?.startDate}
                minDate={beforeAfterDate(
                  new Date(values?.startDate || ""),
                  0,
                  0,
                  0
                )}
                minTime={
                  moment(values?.startDate).isSame(
                    moment(values?.endDate),
                    "day"
                  )
                    ? moment(values?.startDate || "").toDate()
                    : undefined
                }
                maxTime={
                  moment(values?.startDate).isSame(
                    moment(values?.endDate),
                    "day"
                  )
                    ? moment().endOf("day").toDate()
                    : undefined
                }
              />
              {touched?.endDate && errors?.endDate && (
                <div className="text-error text-sm">{errors.endDate}</div>
              )}
            </div>

            {/* <div>MRP: {props.productDetails}</div> */}

            <Input
              label="Flash Sale Price (NRs.)"
              name="flashSalePrice"
              type="text"
              placeholder="E.g: 1200"
              // required={false}
            />

            <div className="mt-3">
              <Button
                loading={isLoadingRejectingOfferPrice}
                disabled={isLoadingRejectingOfferPrice || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              <Button
                disabled={isLoadingRejectingOfferPrice || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default AddToFlashSaleForm;
