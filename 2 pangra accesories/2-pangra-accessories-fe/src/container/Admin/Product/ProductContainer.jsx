import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForSellingVehicle } from "utils/getQueryStringForTable";
import { SelectColumnFilter, DateColumnFilter } from "components/Table/Filter";
// import ReactModal from "components/ReactModal/ReactModal";
import AddToFlashSaleForm from "./AddToFlashSaleForm";
import Modal from "react-modal";

import {
  useAddFeaturedProductMutation,
  useAddHotDealProductMutation,
  // useAddProductToFlashSaleMutation,
  useDeleteProductMutation,
  useReadProductCustomQuery,
  useRemoveFeaturedProductMutation,
} from "services/api/ProductService";
import { useToasts } from "react-toast-notifications";
import Button from "components/Button";
// import { color } from "constant/constant";

function ProductContainer() {
  const { addToast } = useToasts();
  // const [hideModal, setHideModal] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [productDataQuery, setProductDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  const [accessoriesId, setAccessoriesId] = useState("");
  const [mrpPrice, setMrpPrice] = useState("");

  const {
    data: productData,
    isError: productError,
    isFetching: isLoadingProduct,
  } = useReadProductCustomQuery(productDataQuery, {
    /* 
            When we change brand, cat or sub cat, we could not invalidate this query through brand, cat or subcat services, 
            so we are refetching the api whenever this page gets mounted.
        */
    refetchOnMountOrArgChange: true,
  });

  const {
    data: productDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadProductCustomQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  const [
    addFeaturedProduct,
    {
      error: errorAddFeaturedProduct,
      isLoading: isAddingFeaturedProduct,
      isSuccess: isSuccessAddFeaturedProduct,
      data: dataAddFeaturedProduct,
    },
  ] = useAddFeaturedProductMutation();

  const [
    removeFeaturedProduct,
    {
      error: errorRemoveFeaturedProduct,
      isLoading: isRemovingFeaturedProduct,
      isSuccess: isSuccessRemoveFeaturedProduct,
      data: dataRemoveFeaturedProduct,
    },
  ] = useRemoveFeaturedProductMutation();
  const [
    addToHotDealProduct,
    {
      error: errorAddHotDealProduct,
      isLoading: isAddingHotDealProduct,
      isSuccess: isSuccessAddHotDealProduct,
      data: dataAddToHotDealProduct,
    },
  ] = useAddHotDealProductMutation();

  // const [
  //   addProductToFlashSale,
  //   {
  //     error: errorAddToFlashSale,
  //     isLoading: isAddingToFlashSale,
  //     isSuccess: isSuccessAddHFlashSale,
  //     data: dataAddProductToFlashSale,
  //   },
  // ] = useAddProductToFlashSaleMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };

  useEffect(() => {
    if (productDataAll) {
      setTableDataAll(
        productDataAll.docs.map((d) => {
          return {
            // id: d?.id,
            // productTitle: d?.productTitle,
            // brandId: d?.brandId,
            // categoryId: d?.categoryId,
            // subCategoryId: d?.subCategoryId,
            // modelId: d?.modelId,
            // status: d?.status,
            // color: d?.color,
            // costPrice: d?.costPrice,
            // discountPrice: d?.discountPrice,
            // quantity: d?.quantity,
            // SKU: d?.SKU,
            // createdAt: d?.createdAt,
            // hasInHotDeal: d?.hasInHotDeal,
            // hasInFeatured: d?.hasInFeatured,
            id: d?.id,
            productTitle: d?.productTitle,
            brandName: d?.brandName,
            categoryName: d?.categoryName,
            subCategoryName: d?.subCategoryName,
            modelName: d?.modelName,
            status: d?.status,
            // color: d?.color,
            costPrice: d?.costPrice,
            adminPrice: d?.adminPrice,
            discountedPrice: d?.availableOptions[0]?.discountedAmount,
            // quantity: d?.availableOptions[0]?.quantity,
            // productCode: d?.availableOptions[0]?.productCode,
            // quantity: d?.quantity,
            // productCode: d?.productCode,
            createdAt: d?.createdAt,
            hasInHotDeal: d?.hasInHotDeal,
            hasInFeatured: d?.hasInFeatured,
            isForFlashSale: d?.isForFlashSale,
          };
        })
      );
    }
  }, [productDataAll]);

  useEffect(() => {
    if (errorAddFeaturedProduct) {
      showFailureToast(
        errorAddFeaturedProduct?.data?.message ||
          "Failed to add to featured product"
      );
    }
    if (isSuccessAddFeaturedProduct) {
      showSuccessToast(
        dataAddFeaturedProduct?.message ||
          "Successfully added to featured product"
      );
    }
  }, [errorAddFeaturedProduct, isSuccessAddFeaturedProduct]);

  useEffect(() => {
    if (errorRemoveFeaturedProduct) {
      showFailureToast(
        errorRemoveFeaturedProduct?.data?.message ||
          "Failed to remove from featured product"
      );
    }
    if (isSuccessRemoveFeaturedProduct) {
      showSuccessToast(
        dataRemoveFeaturedProduct?.message ||
          "Successfully removed from featured product"
      );
    }
  }, [errorRemoveFeaturedProduct, isSuccessRemoveFeaturedProduct]);

  useEffect(() => {
    if (errorAddHotDealProduct) {
      showFailureToast(
        errorAddHotDealProduct?.data?.message ||
          "Failed to add in hot deal product"
      );
    }
    if (isSuccessAddHotDealProduct) {
      showSuccessToast(
        dataAddToHotDealProduct?.message ||
          "Product/s added to hot deals successfully."
      );
    }
  }, [errorAddHotDealProduct, isSuccessAddHotDealProduct]);

  // useEffect(() => {
  //   if (errorAddToFlashSale) {
  //     showFailureToast(
  //       errorAddToFlashSale?.data?.message || "Failed to add to flash sale"
  //     );
  //   }
  //   if (isSuccessAddHFlashSale) {
  //     showSuccessToast(
  //       dataAddProductToFlashSale?.message || "Successfully added to flash sale"
  //     );
  //   }
  // }, [errorAddToFlashSale, isSuccessAddHFlashSale]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteProductMutation();

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "brandName",
        Header: "Brand Name",
        accessor: "brandName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "subCategoryName",
        Header: "Sub Category Name",
        accessor: "subCategoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "modelName",
        Header: "Model Name",
        accessor: "modelName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "status",
        Header: "Status",
        accessor: "status",
        Cell: ({ cell: { value } }) => {
          if (value) return <span className="badge-green">Active</span>;
          else return <span className="badge-red">Active</span>;
        },
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Active",
            value: "true",
          },
          {
            label: "Inactive",
            value: "false",
          },
        ],
      },
      // {
      //   id: "color",
      //   Header: "Available Colors",
      //   accessor: "color",
      //   Cell: ({ cell: { value } }) =>
      //     value.length ? (value.length > 0 ? value.join(" , ") : value) : "-",
      //   Filter: SelectColumnFilter,

      //   possibleFilters: color.map((value) => {
      //     return {
      //       label: value,
      //       value: value,
      //     };
      //   }),
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: "adminPrice",
        Header: "Admin Price",
        accessor: "adminPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "MRP",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "discountedPrice",
        Header: "Discount Price",
        accessor: "discountedPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      // {
      //   id: "quantity",
      //   Header: "Available Quantity",
      //   accessor: "quantity",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      // {
      //   id: "productCode",
      //   Header: "Product Code",
      //   accessor: "productCode",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: "createdAt",
        Header: "Created At",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "hasInFeatured",
        Header: "Is Featured ",
        accessor: "hasInFeatured",
        Cell: ({ cell: { value } }) => (value ? "Featured " : "Not Featured "),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Featured ",
            value: true,
          },
          {
            label: "Not Feature",
            value: false,
          },
        ],
      },
      {
        id: "hasInHotDeal",
        Header: "Is Hot Deal",
        accessor: "hasInHotDeal",
        Cell: ({ cell: { value } }) => (value ? " Hot Deal" : "Not Hot Deal"),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Hot Deal",
            value: true,
          },
          {
            label: "Not Hot Deal",
            value: false,
          },
        ],
      },
      {
        id: "isForFlashSale",
        Header: "Is For FlashSale",
        accessor: "isForFlashSale",
        Cell: ({ cell: { value } }) =>
          value ? "Flash Sell" : "Not Flash Sale",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Flash Sell",
            value: true,
          },
          {
            label: "Not Flash Sale",
            value: false,
          },
        ],
      },
      {
        id: "flashSell",
        Header: "Flash Sell",
        accessor: "flashSell",
        Cell: ({ row: { original }, cell: { value } }) => {
          // console.log("original", original);
          return (
            <Button
              onClick={() => {
                // setOrderQuantityId(original?.productId);
                setAccessoriesId([original?.id]);
                setMrpPrice(original?.costPrice);
                // ({
                //   // accessoriesId: original?.id,
                // });
                setIsOpen(true);
              }}
            >
              Add
            </Button>
          );
        },
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForSellingVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    setProductDataQuery(query);
  };

  useEffect(() => {
    if (productData) {
      setPageCount(productData.totalPages);
      setTotalData(productData.totalDocs);
      setTableData(
        productData.docs.map((d) => {
          return {
            id: d?.id,
            productTitle: d?.productTitle,
            brandName: d?.brandName,
            categoryName: d?.categoryName,
            subCategoryName: d?.subCategoryName,
            modelName: d?.modelName,
            status: d?.status,
            // color: d?.color,
            costPrice: d?.costPrice,
            adminPrice: d?.adminPrice,
            discountedPrice: d?.availableOptions[0]?.discountedAmount,
            // quantity: d?.availableOptions[0]?.quantity,
            // productCode: d?.availableOptions[0]?.productCode,
            // quantity: d?.quantity,
            // productCode: d?.productCode,
            createdAt: d?.createdAt,
            hasInHotDeal: d?.hasInHotDeal,
            hasInFeatured: d?.hasInFeatured,
            isForFlashSale: d?.isForFlashSale,
          };
        })
      );
    }
  }, [productData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Product List",
      route: "",
    },
  ];

  return (
    <AdminLayout documentTitle="Product List" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Modal
            isOpen={modalIsOpen}
            className="mymodal order"
            overlayClassName="myoverlay"
          >
            <button
              type="button"
              onClick={closeModal}
              className="text-error absolute top-[5px] right-[5px]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>

            <AddToFlashSaleForm
              setIsOpen={setIsOpen}
              accessoriesId={accessoriesId}
              mrpPrice={mrpPrice}
            ></AddToFlashSaleForm>
          </Modal>
          <Table
            tableName="Product/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={productError}
            isLoadingData={isLoadingProduct}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/product/update"
            viewRoute="admin/product"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add Product", route: "/admin/product/create" }}
            hasDropdownBtn={true}
            dropdownBtnName="More"
            dropdownBtnOptions={[
              {
                name: "Add to Featured Product",
                onClick: addFeaturedProduct,
                isLoading: isAddingFeaturedProduct,
                loadingText: "Adding...",
              },
              {
                name: "Remove from Featured Product",
                onClick: removeFeaturedProduct,
                isLoading: isRemovingFeaturedProduct,
                loadingText: "Removing...",
              },
              {
                name: "Add to HotDeal Product",
                onClick: addToHotDealProduct,
                isLoading: isAddingHotDealProduct,
                loadingText: "Adding...",
              },
              // {
              //   name: "Add to Flash Sale",
              //   onClick: addProductToFlashSale,
              //   isLoading: isAddingToFlashSale,
              //   loadingText: "Adding...",
              //   hasModal: true,
              //   renderModalComponent: (id) => {
              //     return (
              //       <a className="text-textColor hover:text-primary ">
              //         <ReactModal
              //           link="Add Product to Flash Sale"
              //           modal={hideModal}
              //         >
              //           <AddToFlashSaleForm
              //             accessoriesId={id}
              //             setModalFunction={setHideModal}
              //             modal={hideModal}
              //           />
              //         </ReactModal>
              //       </a>
              //     );
              //   },
              // },
            ]}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ProductContainer;
