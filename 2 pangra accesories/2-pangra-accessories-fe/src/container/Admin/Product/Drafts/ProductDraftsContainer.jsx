import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import { DateColumnFilter } from "components/Table/Filter";

import {
  useDeleteDraftProductMutation,
  useReadDraftProductsQuery,
} from "services/api/ProductDraft";

function ProductDraftsContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [productDataQuery, setProductDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: productData,
    isError: productError,
    isFetching: isLoadingProduct,
  } = useReadDraftProductsQuery(productDataQuery);

  const { data: productDataAll, isFetching: isLoadingAll } =
    useReadDraftProductsQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (productDataAll) {
      setTableDataAll(
        productDataAll.docs.map((d) => {
          return {
            id: d?.id,
            productTitle: d?.productTitle,
            brandId: d?.brandId,
            categoryId: d?.categoryId,
            subCategoryId: d?.subCategoryId,
            color: d?.color,
            costPrice: d?.costPrice,
            discountedPrice: d?.discountedPrice,
            quantity: d?.quantity,
            SKU: d?.SKU,
            createdAt: d?.createdAt,
          };
        })
      );
    }
  }, [productDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteDraftProductMutation();

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "brandName",
        Header: "Brand Name",
        accessor: "brandName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "subCategoryName",
        Header: "Sub Category Name",
        accessor: "subCategoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "color",
        Header: "Available Colors",
        accessor: "color",
        Cell: ({ cell: { value } }) =>
          value.length ? (value.length > 0 ? value.join(" , ") : value) : "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "Cost Price",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "discountedPrice",
        Header: "Discount Price",
        accessor: "discountedPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "quantity",
        Header: "Available Quantity",
        accessor: "quantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "SKU",
        Header: "SKU",
        accessor: "SKU",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "createdAt",
        Header: "Created At",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setProductDataQuery(
      query + "&noRegex=hasInHomePage,status,createdAt&type=accessoriesDraft"
    );
  };

  useEffect(() => {
    if (productData) {
      setPageCount(productData.totalPages);
      setTotalData(productData.totalDocs);
      setTableData(
        productData.docs.map((d) => {
          return {
            id: d?.id,
            productTitle: d?.productTitle,
            brandName: d?.brandName,
            categoryName: d?.categoryName,
            subCategoryName: d?.subCategoryName,
            color: d?.color,
            costPrice: d?.costPrice,
            discountedPrice: d?.discountedPrice,
            quantity: d?.quantity,
            SKU: d?.SKU,
            createdAt: d?.createdAt,
          };
        })
      );
    }
  }, [productData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Drafts List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="View Product Drafts"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Product Draft/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={productError}
            isLoadingData={isLoadingProduct}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/product/drafts/update"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ProductDraftsContainer;
