import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import { SelectColumnFilter } from "components/Table/Filter";
import {
  useDeleteBrandMutation,
  useReadBrandListQuery,
  useAddFeaturedBrandMutation,
  useRemoveFeaturedBrandMutation,
} from "services/api/BrandService";
import { useToasts } from "react-toast-notifications";

function BrandContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [vehicleDataQuery, setVehicleDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: vehicleData,
    isError: vehicleError,
    isFetching: isLoadingVehicle,
  } = useReadBrandListQuery(vehicleDataQuery);

  const { data: brandDataAll, isFetching: isLoadingAll } =
    useReadBrandListQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });

  const [
    addFeaturedBrand,
    {
      error: errorAddFeaturedBrand,
      isLoading: isAddingFeaturedBrand,
      isSuccess: isSuccessAddFeaturedBrand,
      data: dataAddFeaturedBrand,
    },
  ] = useAddFeaturedBrandMutation();

  const [
    removeFeaturedBrand,
    {
      error: errorRemoveFeaturedBrand,
      isLoading: isRemovingFeaturedBrand,
      isSuccess: isSuccessRemoveFeaturedBrand,
      data: dataRemoveFeaturedBrand,
    },
  ] = useRemoveFeaturedBrandMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };

  useEffect(() => {
    if (brandDataAll) {
      setTableDataAll(
        brandDataAll.docs.map((brand) => {
          return {
            id: brand?.id,
            brandName: brand?.brandName,
            brandDescription: brand?.brandDescription,
          };
        })
      );
    }
  }, [brandDataAll]);

  useEffect(() => {
    if (errorAddFeaturedBrand) {
      showFailureToast(
        errorAddFeaturedBrand?.data?.message ||
          "Failed to add to featured brands"
      );
    }
    if (isSuccessAddFeaturedBrand) {
      showSuccessToast(
        dataAddFeaturedBrand?.message || "Successfully added to featured brands"
      );
    }
  }, [errorAddFeaturedBrand, isSuccessAddFeaturedBrand]);

  useEffect(() => {
    if (errorRemoveFeaturedBrand) {
      showFailureToast(
        errorRemoveFeaturedBrand?.data?.message ||
          "Failed to remove from featured brands"
      );
    }
    if (isSuccessRemoveFeaturedBrand) {
      showSuccessToast(
        dataRemoveFeaturedBrand?.message ||
          "Successfully removed from featured brands"
      );
    }
  }, [errorRemoveFeaturedBrand, isSuccessRemoveFeaturedBrand]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteBrandMutation();

  const columns = useMemo(
    () => [
      {
        id: "brandName",
        Header: "Brand Name",
        accessor: "brandName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "hasInHomePage",
        Header: "Is Featured",
        accessor: "hasInHomePage",
        Cell: ({ cell: { value } }) => (value ? "Featured" : "Not Featured"),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Featured",
            value: true,
          },
          {
            label: "Not Featured",
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setVehicleDataQuery(query + "&noRegex=hasInHomePage");
  };

  useEffect(() => {
    if (vehicleData) {
      setPageCount(vehicleData.totalPages);
      setTotalData(vehicleData.totalDocs);
      setTableData(
        vehicleData.docs.map((vehicle) => {
          return {
            id: vehicle?.id,
            brandName: vehicle?.brandName,
            brandDescription: vehicle?.brandDescription,
            hasInHomePage: vehicle?.hasInHomePage,
          };
        })
      );
    }
  }, [vehicleData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Brand List",
      route: "/admin/brand",
    },
  ];

  return (
    <AdminLayout documentTitle="View Brands" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Brand/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={vehicleError}
            isLoadingData={isLoadingVehicle}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/brand/update"
            viewRoute="admin/brand"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add Brand", route: "/admin/brand/create" }}
            hasDropdownBtn={true}
            dropdownBtnName="More"
            dropdownBtnOptions={[
              {
                name: "Add to Featured Brands",
                onClick: addFeaturedBrand,
                isLoading: isAddingFeaturedBrand,
                loadingText: "Adding...",
              },
              {
                name: "Remove from Featured Brands",
                onClick: removeFeaturedBrand,
                isLoading: isRemovingFeaturedBrand,
                loadingText: "Removing...",
              },
            ]}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default BrandContainer;
