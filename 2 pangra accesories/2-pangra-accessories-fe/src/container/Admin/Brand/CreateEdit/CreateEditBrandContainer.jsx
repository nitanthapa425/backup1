import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";
import PropTypes from "prop-types";
import Link from "next/link";

import Button from "components/Button";
import DropZone from "components/DropZone";
import Input from "components/Input";
import AdminLayout from "layouts/Admin";

import {
  useCreateBrandMutation,
  useReadBrandDetailsQuery,
  useUpdateBrandMutation,
} from "services/api/BrandService";
import { brandvalidationSchema } from "validation/brand.validation";
import Popup from "components/Popup";
import { useRouter } from "next/router";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";

const CreateEditBrandContainer = ({ type }) => {
  const router = useRouter();
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [editBrandInitialValue, setEditBrandInitialValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();

  const addBrandInitialValue = {
    brandName: "",
    uploadBrandImage: "",
    // companyImage: '',
  };

  const [
    createBrand,
    { isError, isLoading, isSuccess, data: dataCreateBrand },
  ] = useCreateBrandMutation();

  const handleBrandLogo = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("uploadBrandImage", newFiles[0]);
    }
  };

  const handleRemoveBrandImage = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("uploadBrandImage", "");
    }
  };

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(
        <>
          Brand added successfully.{" "}
          <Link href={`/admin/brand/${dataCreateBrand?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Brand
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast("Brand name already exist.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const { data: brandDetails, error: brandFetchError } =
    useReadBrandDetailsQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  useEffect(() => {
    if (brandFetchError) {
      addToast(
        "Error occurred while fetching Brand details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [brandFetchError]);

  useEffect(() => {
    if (brandDetails) {
      const newBrandDetails = {
        brandName: brandDetails?.brandName,
        uploadBrandImage: brandDetails?.uploadBrandImage,
      };
      setEditBrandInitialValue(newBrandDetails);
    }
  }, [brandDetails]);

  const [
    updateBrand,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateBrandMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("Brand Name already exist.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(updateSuccessData?.message || "Brand updated successfully.", {
        appearance: "success",
      });
      setChanged(false);

      router.push(`/admin/brand/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      createBrand(values);
    }
    if (type === "edit") {
      updateBrand({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Brand",
        route: "/admin/brand/create",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Brand List",
        route: "/admin/brand",
      },
      {
        routeName: "Edit Brand",
        route: "",
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={type === "add" ? "Add New Brand" : "Edit Brand Details"}
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? editBrandInitialValue : addBrandInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={brandvalidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add" ? "Add New Brand" : "Edit Brand Details"}
                  </h3>
                  <div className="mb-2">
                    <Input
                      label="Brand Name"
                      name="brandName"
                      type="text"
                      placeholder="E.g: Bajaj"
                    />
                  </div>

                  <div className="mb-2">
                    <DropZone
                      label="Upload Brand Image"
                      required
                      currentFiles={
                        values.uploadBrandImage ? [values.uploadBrandImage] : []
                      }
                      setNewFiles={handleBrandLogo}
                      handleRemoveFile={handleRemoveBrandImage}
                      error={
                        touched?.uploadBrandImage
                          ? errors?.uploadBrandImage
                          : ""
                      }
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === "add" ? "Submit" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditBrandContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditBrandContainer;
