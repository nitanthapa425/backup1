import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";

import AdminLayout from "layouts/Admin";

import Link from "next/link";
import Button from "components/Button";

import { useReadShippingDetailsQuery } from "services/api/Shipping";

const DetailShippingContainer = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: shippingDetails,
    error: shippingFetchError,
    isLoading: isLoadingShipping,
    error,
  } = useReadShippingDetailsQuery(router.query.id, {
    skip: !router.query.id,
  });

  useEffect(() => {
    if (shippingFetchError) {
      addToast(
        error?.data?.message ||
          "Error occured while fetching shipping details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [shippingFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Shipping Charge List",
      route: "/admin/shipping",
    },
    {
      routeName: "Shipping Charge Details",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Shipping Details"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Shipping Details</h1>
          <div className="flex justify-end">
            <Link href={`/admin/shipping/update/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>

          {isLoadingShipping ? (
            "Loading..."
          ) : (
            <>
              <div className="row">
                <div className="mx-3">
                  <p>
                    <strong>Shipping Charge:</strong> &nbsp;
                    {shippingDetails?.[0]?.shippingCharge}
                  </p>
                  {/* <p>
                    <strong>Model Description:&nbsp;</strong>
                    {shippingDetails?.modelDescription
                      ? shippingDetails.modelDescription
                      : "N/A"}
                  </p> */}
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default DetailShippingContainer;
