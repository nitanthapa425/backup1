import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";

import {
  useDeleteShippingMutation,
  useReadShippingListQuery,
} from "services/api/Shipping";
import { getQueryStringForTable } from "utils/getQueryStringForTable";

function ShippingContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [shippingDataQuery, setShippingDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: shipping,
    isError: shippingError,
    isFetching: isLoadingModel,
  } = useReadShippingListQuery(shippingDataQuery);

  const {
    data: shippingAll,

    isFetching: isLoadingAll,
  } = useReadShippingListQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (shippingAll) {
      setTableDataAll(
        shippingAll?.map((shipping) => {
          return {
            id: shipping?._id,
            shippingCharge: shipping?.shippingCharge,
          };
        })
      );
    }
  }, [shippingAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteShippingMutation();

  const columns = useMemo(
    () => [
      {
        id: "shippingCharge",
        Header: "Shipping Charge",
        accessor: "shippingCharge",
        Cell: ({ cell: { value } }) => value || "-",
        // canBeSorted: true,
        // canBeFiltered: true,
      },
      // {
      //   id: "from",
      //   Header: "Starting",
      //   accessor: "from",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      // {
      //   id: "To",
      //   Header: "Ending",
      //   accessor: "To",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setShippingDataQuery(query);
  };

  useEffect(() => {
    if (shipping) {
      setPageCount(shipping.totalPages);
      setTotalData(shipping.totalDocs);
      setTableData(
        shipping?.map((shipping) => {
          return {
            id: shipping?._id,
            shippingCharge: shipping?.shippingCharge,
          };
        })
      );
    }
  }, [shipping]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/shipping/create",
    },
    {
      routeName: "Shipping Charge List",
      route: "/admin/shipping",
    },
  ];

  const addPageOption = () => {
    if (shipping?.length === 0) {
      return {
        page: "Add Shipping Charge",
        route: "/admin/shipping/create",
      };
    } else {
      return null;
    }
  };

  return (
    <AdminLayout
      documentTitle="View Shipping Details"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}

          <Table
            tableName="Shipping/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={shippingError}
            isLoadingData={isLoadingModel}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/shipping/update"
            viewRoute="admin/shipping"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={addPageOption()}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ShippingContainer;
