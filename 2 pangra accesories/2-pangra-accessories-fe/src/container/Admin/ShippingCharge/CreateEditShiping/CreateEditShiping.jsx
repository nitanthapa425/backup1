import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";
import PropTypes from "prop-types";
import Link from "next/link";

import Button from "components/Button";

import Input from "components/Input";
import AdminLayout from "layouts/Admin";

import Popup from "components/Popup";
import { useRouter } from "next/router";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import { shippingValidationSchema } from "validation/model.validation";

import {
  useCreateShippingMutation,
  useReadShippingDetailsQuery,
  useUpdateShippingMutation,
} from "services/api/Shipping";

const CreateEditShippingContainer = ({ type }) => {
  const router = useRouter();
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [editShippingInitialValue, setEditModelInitialValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();

  const addShippingInitialValue = {
    shippingCharge: "",
  };

  // console.log("editShippingInitialValue", editShippingInitialValue);

  const [
    createShipping,
    { isError, isLoading, isSuccess, data: dataCreateShipping },
  ] = useCreateShippingMutation();
  // console.log(data);

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(
        <>
          New Shipping Details added successfully.{" "}
          <Link href={`/admin/shipping/${dataCreateShipping?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Shipping Charge
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast("Shipping details already exist.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const {
    data: shippingDetails,
    error: modelFetchError,
    isSuccess: isSuccessShippingDetails,
  } = useReadShippingDetailsQuery(router.query.id, {
    skip: type !== "edit" || !router.query.id,
  });

  // console.log("shippingDetails", shippingDetails);

  useEffect(() => {
    if (modelFetchError) {
      addToast(
        "Error occured while fetching shipping details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [modelFetchError]);

  useEffect(() => {
    if (isSuccessShippingDetails) {
      const newShippingDetails = {
        shippingCharge: shippingDetails[0]?.shippingCharge,
      };
      setEditModelInitialValue(newShippingDetails);
    }
  }, [isSuccessShippingDetails]);

  const [
    updateShipping,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateShippingMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("Error updating shipping details.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || "shipping details updated successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);
      // redirect to model page
      // ..........redirect is left
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
      router.push(`/admin/shipping`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      // createBrand(values, resetForm);
      createShipping(values);
    }
    if (type === "edit") {
      updateShipping({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Shipping Charge",
        route: "/admin/shipping/create",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Shipping Charge List",
        route: "/admin/shipping",
      },
      {
        routeName: "Edit Shipping charge",
        route: "",
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={
        type === "add" ? "Add New Shipping Details" : "Edit Shipping Details"
      }
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        {/* {console.log("editShippingDetails",editShippingDetails)} */}
        <Formik
          initialValues={
            type === "edit" ? editShippingInitialValue : addShippingInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={shippingValidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add"
                      ? "Add New Shipping Charge"
                      : "Edit Shipping Charge Details"}
                  </h3>
                  <div className="mb-2">
                    <Input
                      label="Shipping Charge"
                      name="shippingCharge"
                      type="text"
                      placeholder="E.g: 12121"
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === "add" ? "Submit" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditShippingContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditShippingContainer;
