import SizeChartTab from "components/Tabs/SizeChartTab";
import React, { useEffect, useState } from "react";
import AddSizeChart from "./AddSizeChart";
import SelectSizeChart from "./SelectSizeChart";

const SizeChart = ({
  setIsOpen,
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
}) => {
  const [tab1, setTab1] = useState([]);

  useEffect(() => {
    setTab1([
      {
        heading: "Select Size Chart",
        Component: (
          <SelectSizeChart
            values={values}
            setFieldValue={setFieldValue}
            errors={errors}
            touched={touched}
            setChanged={setChanged}
            dirty={dirty}
            setFieldTouched={setFieldTouched}
            setIsOpen={setIsOpen}
          ></SelectSizeChart>
        ),
      },
      {
        heading: "Add Size Chart",
        Component: <AddSizeChart></AddSizeChart>,
      },
    ]);
  }, [values]);
  return (
    <>
      <SizeChartTab tabData={tab1}></SizeChartTab>
    </>
  );
};

export default SizeChart;
