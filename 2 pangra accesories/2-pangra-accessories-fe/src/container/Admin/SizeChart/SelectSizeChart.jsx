import SelectWithoutCreateImage from "components/Select/SelectWithoutCreateImage";
import React from "react";
import { useGetSizeChartQuery } from "services/api/ProductService";
import "react-medium-image-zoom/dist/styles.css";

import Zoom from "react-medium-image-zoom";
import Button from "components/Button";

const SelectSizeChart = ({
  setIsOpen,
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
}) => {
  const { data: dataSizeChart, isLoading: isLoadingSizeChart } =
    useGetSizeChartQuery();
  const sizeChartImage = dataSizeChart?.find?.(
    (v1, i) => v1._id === values.sizeChart
  )?.imageUrl;

  return (
    <div>
      <SelectWithoutCreateImage
        loading={isLoadingSizeChart}
        label="Select Size Chart"
        error={touched?.sizeChart ? errors?.sizeChart : ""}
        value={values.sizeChart || ""}
        onChange={(selectedBrand) => {
          setFieldTouched("sizeChart");
          setFieldValue("sizeChart", selectedBrand.value);
          // setRefreshComponent(selectedBrand.value);
        }}
        options={dataSizeChart?.map((value) => ({
          value: value?._id,
          label: value?.imageName,
          flag: value?.imageUrl,
        }))}
        onBlur={() => {
          setFieldTouched("sizeChart");
        }}
      />

      {sizeChartImage ? (
        <Zoom zoomMargin={40}>
          <img
            src={sizeChartImage}
            className="
                  rounded
                  w-[150px]
                  h-[100px]
                  mt-4
                  "
          />
        </Zoom>
      ) : null}

      <div className="absolute bottom-1 right-1">
        <Button
          onClick={() => {
            setIsOpen(false);
          }}
        >
          Save
        </Button>
      </div>
    </div>
  );
};

export default SelectSizeChart;
