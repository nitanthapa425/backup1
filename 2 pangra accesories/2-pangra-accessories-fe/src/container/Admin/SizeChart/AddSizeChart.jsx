import Button from "components/Button";
import DropZone from "components/DropZone";
import Input from "components/Input";
import { Form, Formik } from "formik";
import React, { useEffect, useRef } from "react";
import { useToasts } from "react-toast-notifications";
import { useAddSizeChartMutation } from "services/api/ProductService";
import * as yup from "yup";

const AddSizeChart = () => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const handleSizeChartImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("sizeChartImagePath", newFiles[0]);
    }
  };

  const handleRemoveSizeChartImagePath = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("sizeChartImagePath", {});
    }
  };

  const initialValues = {
    imageName: "",
    sizeChartImagePath: "",
  };

  const sizeChartValidation = yup.object({
    imageName: yup.string().required("Name is required."),
    sizeChartImagePath: yup.object().required("Size Chart Image is required."),
  });

  const [
    addSizeChart,
    {
      data: dataAddSizeChart,
      isError: isErrorAddSizeChart,
      isSuccess: isSuccessAddSizeChart,
      error: errorAddSizeChart,
    },
  ] = useAddSizeChartMutation();

  useEffect(() => {
    if (isSuccessAddSizeChart) {
      formikBag.current?.resetForm();

      addToast(
        dataAddSizeChart?.error?.message || "Size Chart added successfully.",

        {
          appearance: "success",
        }
      );
    }
    if (isErrorAddSizeChart) {
      addToast(errorAddSizeChart?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorAddSizeChart, isSuccessAddSizeChart]);

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={sizeChartValidation}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        addSizeChart({
          imageName: values.imageName,
          imageUrl: values.sizeChartImagePath.imageUrl,
          publicKey: values.sizeChartImagePath.publicKey,
        });
        setSubmitting(false);
      }}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <Form>
          <div>
            <Input
              label="Name"
              name="imageName"
              type="text"
              placeholder="E.g: name"
            />

            <DropZone
              label="Upload Size Chart"
              currentFiles={
                Object.keys(values.sizeChartImagePath || {}).length
                  ? [values.sizeChartImagePath]
                  : []
              }
              setNewFiles={handleSizeChartImagePath}
              handleRemoveFile={handleRemoveSizeChartImagePath}
              error={
                touched?.sizeChartImagePath ? errors?.sizeChartImagePath : ""
              }
              required={true}
            />

            <Button type="submit">Submit</Button>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default AddSizeChart;
