import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";

import Button from "components/Button";
import DropZone from "components/DropZone";

import AdminLayout from "layouts/Admin";

import {
  useGetBannerImageQuery,
  useUploadBannerImageMutation,
} from "services/api/BrandService";
import { BannerValidationSchema } from "validation/brand.validation";
import Popup from "components/Popup";

const UploadBrandImageContainer = () => {
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);

  const formikBag = useRef();

  const [uploadBannerImage, { isError, isLoading, isSuccess }] =
    useUploadBannerImageMutation();

  const handleBannerImage = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("bannerImage", newFiles[0]);
    }
  };

  const handleRemoveBannerImage = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("bannerImage", "");
    }
  };

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(<>Banner image uploaded successfully. </>, {
        appearance: "success",
        autoDismiss: false,
      });
    }
  }, [isSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isError) {
      addToast("Error occured while uploading Banner Image ", {
        appearance: "error",
      });
    }
  }, [isError]);

  const { data: imageUrl, error: bannerFetchError } = useGetBannerImageQuery();

  //   console.log("*********", bannerImage);

  useEffect(() => {
    if (bannerFetchError) {
      addToast(
        "Error occured while fetching banner image. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [bannerFetchError]);

  const addBannerImageInitialValue = {
    bannerImage: imageUrl || "",
  };

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    uploadBannerImage(values);
  };

  let BreadCrumbList = [];

  BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Add Banner Image",
      route: "/admin/brand-image/create",
    },
  ];

  return (
    <AdminLayout
      documentTitle={"Add Banner Image"}
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={addBannerImageInitialValue}
          onSubmit={onSubmit}
          validationSchema={BannerValidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">{"Add Banner Image"}</h3>

                  <div className="mb-2">
                    <DropZone
                      label="Upload Banner Image"
                      required
                      currentFiles={
                        values.bannerImage ? [values.bannerImage] : []
                      }
                      setNewFiles={handleBannerImage}
                      handleRemoveFile={handleRemoveBannerImage}
                      error={touched?.bannerImage ? errors?.bannerImage : ""}
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading}
                      disabled={isSubmitting || !dirty || isLoading}
                    >
                      ADD
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();

                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

export default UploadBrandImageContainer;
