import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Link from "next/link";

import Button from "components/Button";
import Input from "components/Input";
import AdminLayout from "layouts/Admin";
import PropTypes from "prop-types";

import {
  useCreateSubCategoryMutation,
  useReadSubCategoryDetailsQuery,
  useUpdateSubCategoryMutation,
} from "services/api/SubCategoryService";
import { subCategoryValidationSchema } from "validation/subCategory.validation";
import { useReadCategoryNoAuthQuery } from "services/api/CategoryService";
import { useToasts } from "react-toast-notifications";
import Popup from "components/Popup";
import { useRouter } from "next/router";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Textarea from "components/Input/textarea";

const CreateEditSubCategoryContainer = ({ type }) => {
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const [editSubCategoryInitialValue, setEditSubCategoryInitialValue] =
    useState({});

  const { addToast, removeAllToasts } = useToasts();
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const { data: categoryData, isLoading: fetchingCategory } =
    useReadCategoryNoAuthQuery();
  const [CategoryData, setCategoryData] = useState([]);

  useEffect(() => {
    if (categoryData) {
      setCategoryData(categoryData);
    }
  }, [categoryData]);

  const formikSubModelBag = useRef();

  const [createSubCategory, { isError, error, isLoading, isSuccess, data }] =
    useCreateSubCategoryMutation();

  useEffect(() => {
    if (isSuccess) {
      formikSubModelBag.current?.resetForm();
      addToast(
        <>
          Sub category added successfully.
          <Link href={`/admin/subcategory/${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Sub Category
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      addToast(error?.data?.message || "Error adding Sub Category", {
        appearance: "error",
      });
    }
  }, [isError, error]);

  const [
    updateSubCategory,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateSubCategoryMutation();

  const { data: subCategoryDetails, error: subCategoryFetchError } =
    useReadSubCategoryDetailsQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  useEffect(() => {
    if (subCategoryFetchError) {
      addToast(
        "Error occurred while fetching Sub Category details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [subCategoryFetchError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      createSubCategory(values, resetForm);
    }
    if (type === "edit") {
      updateSubCategory({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  useEffect(() => {
    if (isUpdateError) {
      addToast("Sub category name already exist.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikSubModelBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || "Sub Category updated successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);
      router.push(`/admin/subcategory/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  const addModelInitialValue = {
    categoryId: router?.query?.category || "",
    subCategoryName: "",
    subCategoryDescription: "",
  };

  useEffect(() => {
    if (subCategoryDetails) {
      const newsubCategoryDetails = {
        categoryId: subCategoryDetails?.categoryId?.id,
        subCategoryName: subCategoryDetails?.subCategoryName,
        subCategoryDescription: subCategoryDetails?.subCategoryDescription,
      };
      setEditSubCategoryInitialValue(newsubCategoryDetails);
    }
  }, [subCategoryDetails]);

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Sub Category",
        route: "",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Sub Category List",
        route: "/admin/subcategory",
      },
      {
        routeName: "Edit Variant",
        route: "",
      },
    ];
  }

  useEffect(() => {
    return removeAllToasts;
  }, []);

  return (
    <AdminLayout
      documentTitle={
        type === "add" ? "Add New Sub Category" : "Edit Sub Category"
      }
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? editSubCategoryInitialValue : addModelInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={subCategoryValidationSchema}
          enableReinitialize
          innerRef={formikSubModelBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            setFieldTouched,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add"
                      ? "Add New Sub Category"
                      : "Edit Sub Category"}
                  </h3>
                  <div className="mb-2">
                    <SelectWithoutCreate
                      required
                      loading={fetchingCategory}
                      label="Select Category"
                      placeholder="E.g: Riding Gears"
                      error={touched?.categoryId ? errors?.categoryId : ""}
                      value={values.categoryId}
                      onChange={(selectedCategory) => {
                        setFieldTouched("categoryId");
                        setFieldValue("categoryId", selectedCategory.value);
                      }}
                      options={CategoryData?.map((d) => ({
                        value: d?.categoryId,
                        label: d?.categoryName,
                      }))}
                    />
                  </div>

                  <div className="mb-2">
                    <Input
                      label="Sub Category Name"
                      name="subCategoryName"
                      type="text"
                      placeholder="E.g: Gloves"
                    />
                  </div>

                  <div className="mb-2">
                    <Textarea
                      label="Sub Category Description"
                      name="subCategoryDescription"
                      type="text"
                      placeholder="E.g: Sub Category Description"
                      required={false}
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === "add" ? "Create" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditSubCategoryContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditSubCategoryContainer;
