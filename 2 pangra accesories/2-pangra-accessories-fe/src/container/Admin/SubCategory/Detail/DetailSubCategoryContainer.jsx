import React, { useEffect } from "react";
import AdminLayout from "layouts/Admin";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";

import { useReadSubCategoryDetailsQuery } from "services/api/SubCategoryService";
import Link from "next/link";
import Button from "components/Button";

const DetailSubCategoryContainer = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: subCategoryDetails,
    error: subCategoryFetchError,
    isLoading: isLoadingSubCategory,
  } = useReadSubCategoryDetailsQuery(router.query.id, {});

  useEffect(() => {
    if (subCategoryFetchError) {
      addToast(
        "Error occured while fetching Sub Category details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [subCategoryFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Sub Category List",
      route: "/admin/subcategory",
    },
    {
      routeName: "Sub Category Details",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="Sub Category Details"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Sub Category Details</h1>
          <div className="flex justify-end">
            <Link href={`/admin/subcategory/update/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          <div className="row ">
            {isLoadingSubCategory ? (
              "Loading..."
            ) : (
              <div className="flex items-start">
                <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md ">
                  <p>
                    <strong>Sub Category Name:</strong>&nbsp;
                    {subCategoryDetails?.subCategoryName || "N/A"}
                  </p>
                  <p>
                    <strong>Category Name:</strong>&nbsp;
                    <Link
                      href={`/admin/category/${subCategoryDetails?.categoryId?.id}`}
                    >
                      <a className="underline text-blue-700">
                        {subCategoryDetails?.categoryName || "N/A"}
                      </a>
                    </Link>
                  </p>
                  <p>
                    <strong>Category Description:</strong>&nbsp;
                    {subCategoryDetails?.subCategoryDescription || "N/A"}
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default DetailSubCategoryContainer;
