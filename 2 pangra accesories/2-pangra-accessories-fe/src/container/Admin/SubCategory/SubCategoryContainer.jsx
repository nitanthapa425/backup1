import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForTable } from "utils/getQueryStringForTable";

import {
  useReadSubCategoryQuery,
  useDeleteSubCategoryMutation,
} from "services/api/SubCategoryService";

function SubCategoryContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [subCategoryDataQuery, setSubCategoryDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: subCategoryData,
    isError: subCategoryError,
    isFetching: isLoadingSubCategory,
  } = useReadSubCategoryQuery(subCategoryDataQuery);

  const { data: subCategoryDataAll, isFetching: isLoadingAll } =
    useReadSubCategoryQuery("", {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (subCategoryDataAll) {
      setTableDataAll(
        subCategoryDataAll.docs.map((data) => {
          return {
            id: data?.id,
            subCategoryName: data?.subCategoryName,
            categoryName: data?.categoryName,
          };
        })
      );
    }
  }, [subCategoryDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteSubCategoryMutation();

  const columns = useMemo(
    () => [
      {
        id: "subCategoryName",
        Header: "Sub Category Name",
        accessor: "subCategoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);

    setSubCategoryDataQuery(query);
  };

  useEffect(() => {
    if (subCategoryData) {
      setPageCount(subCategoryData.totalPages);
      setTotalData(subCategoryData.totalDocs);

      setTableData(
        subCategoryData.docs.map((d) => {
          return {
            id: d?.id,
            subCategoryName: d?.subCategoryName,
            categoryName: d?.categoryName,
          };
        })
      );
    }
  }, [subCategoryData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Sub Category List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="View Sub Category"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Sub Category/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={subCategoryError}
            isLoadingData={isLoadingSubCategory}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/subcategory/update"
            viewRoute="admin/subcategory"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{
              page: "Add Sub Category",
              route: "/admin/subcategory/create",
            }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default SubCategoryContainer;
