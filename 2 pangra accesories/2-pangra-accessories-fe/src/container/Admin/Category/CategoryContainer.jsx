import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import {
  useDeleteCategoryMutation,
  useReadCategoryCustomQuery,
} from "services/api/CategoryService";

function CategoryContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [categoryDataQuery, setCategoryDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const {
    data: categoryData,
    isError: categoryError,
    isFetching: isLoadingCategory,
  } = useReadCategoryCustomQuery(categoryDataQuery);

  const { data: categoryDataAll, isFetching: isLoadingAll } =
    useReadCategoryCustomQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });
  useEffect(() => {
    if (categoryDataAll) {
      setTableDataAll(
        categoryDataAll.docs.map((category) => {
          return {
            id: category?.id,
            categoryName: category?.categoryName,
            categoryDescription: category.categoryDescription,
          };
        })
      );
    }
  }, [categoryDataAll]);
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteCategoryMutation();

  const columns = useMemo(
    () => [
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryDescription",
        Header: "Category Description",
        accessor: "categoryDescription",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setCategoryDataQuery(query);
  };

  useEffect(() => {
    if (categoryData) {
      setPageCount(categoryData.totalPages);
      setTotalData(categoryData.totalDocs);
      setTableData(
        categoryData.docs.map((category) => {
          return {
            id: category?.id,
            categoryName: category?.categoryName,
            categoryDescription: category.categoryDescription,
          };
        })
      );
    }
  }, [categoryData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Category List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="View Categories"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Category/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={categoryError}
            isLoadingData={isLoadingCategory}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/category/update"
            viewRoute="admin/category"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add Category", route: "/admin/category/create" }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default CategoryContainer;
