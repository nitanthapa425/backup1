import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Link from "next/link";

import Button from "components/Button";
import Input from "components/Input";
import AdminLayout from "layouts/Admin";

import { categoryValidationSchema } from "validation/category.validation";
import {
  useCreateCategoryMutation,
  useReadCategoryDetailsQuery,
  useUpdateCategoryMutation,
} from "services/api/CategoryService";
import { useToasts } from "react-toast-notifications";
import Popup from "components/Popup";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Textarea from "components/Input/textarea";

const CreateEditCategoryContainer = ({ type }) => {
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const [currentCategoryDetails, setCurrentCategoryDetails] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const { addToast, removeAllToasts } = useToasts();

  const addCategoryInitialValue = {
    categoryName: "",
    categoryDescription: "",
  };

  const formikModelBag = useRef();
  const [createCategory, { isError, error, isLoading, isSuccess, data }] =
    useCreateCategoryMutation();

  const [
    updateCategory,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateCategoryMutation();

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    formikModelBag.current?.resetForm();
    if (isSuccess) {
      formikModelBag.current?.resetForm();
      addToast(
        <>
          Category added successfully.{" "}
          <Link href={`/admin/category/${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Category
            </a>
          </Link>
          <Link href={`/admin/subcategory/create?category=${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700">
              Add Sub Category
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikModelBag.current?.resetForm();
      addToast(updateSuccessData?.message || "Category updated successfully.", {
        appearance: "success",
      });
      setChanged(false);
      router.push(`/admin/category/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  useEffect(() => {
    if (isUpdateError) {
      addToast("Category name already exist.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isError) {
      addToast(error?.data?.message || "Error creating property", {
        appearance: "error",
      });
    }
  }, [isError, error]);

  const {
    data: categoryDetails,
    error: categoryFetchError,
    // isLoading: isLoadingModel,
  } = useReadCategoryDetailsQuery(router.query.id, {
    skip: type !== "edit" || !router.query.id,
  });

  useEffect(() => {
    if (categoryDetails) {
      const newcategoryDetails = {
        categoryName: categoryDetails?.categoryName,
        categoryDescription: categoryDetails?.categoryDescription,
      };
      setCurrentCategoryDetails(newcategoryDetails);
    }
  }, [categoryDetails]);

  useEffect(() => {
    if (categoryFetchError) {
      addToast(
        "Error occured while fetching category details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [categoryFetchError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      createCategory(values, resetForm);
    }
    if (type === "edit") {
      updateCategory({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Category",
        route: "",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Category List",
        route: "/admin/category",
      },
      {
        routeName: "Edit Category",
        route: "",
      },
    ];
  }

  return (
    <AdminLayout
      documentTitle={
        type === "add" ? "Add New Category" : "Edit Category Details"
      }
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? currentCategoryDetails : addCategoryInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={categoryValidationSchema}
          enableReinitialize
          innerRef={formikModelBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            setFieldTouched,
            dirty,
          }) => {
            return (
              <Form onChange={() => setChanged(true)}>
                <div className="flex justify-center pt-3 pb-10">
                  <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-xl mx-auto">
                    <h3 className="mb-3">
                      {type === "add" ? "Add New Category" : "Edit Category"}
                    </h3>
                    <div className="mb-2">
                      <Input
                        label="Category Name"
                        name="categoryName"
                        type="text"
                        placeholder="E.g: Riding Gears"
                      />
                    </div>
                    <div className="mb-2">
                      <Textarea
                        label="Category Description"
                        name="categoryDescription"
                        type="text"
                        placeholder="E.g: Category Description"
                        required={false}
                      />
                    </div>

                    <div className="btn-holder mt-3">
                      <Button
                        type="submit"
                        loading={isLoading || updating}
                        disabled={
                          isSubmitting || !dirty || isLoading || updating
                        }
                      >
                        {type === "add" ? "Create" : "Update"}
                      </Button>

                      <Button
                        onClick={() => {
                          setOpenModal(true);
                        }}
                        variant="outlined-error"
                        type="button"
                        disabled={
                          isSubmitting || !dirty || isLoading || updating
                        }
                      >
                        Clear
                      </Button>

                      {openModal && (
                        <Popup
                          title="Are you sure to clear all fields?"
                          description="If you clear all fields, the data will not be saved."
                          onOkClick={() => {
                            setFieldValue("brandId", null);
                            resetForm();
                            setChanged(false);
                            setOpenModal(false);
                          }}
                          onCancelClick={() => setOpenModal(false)}
                          okText="Clear All"
                          cancelText="Cancel"
                        />
                      )}
                    </div>
                  </div>
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditCategoryContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditCategoryContainer;
