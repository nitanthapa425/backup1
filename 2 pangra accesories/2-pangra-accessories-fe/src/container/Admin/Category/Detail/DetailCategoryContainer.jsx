import React, { useEffect } from "react";
import AdminLayout from "layouts/Admin";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";
import { useReadCategoryDetailsQuery } from "services/api/CategoryService";
import Link from "next/link";
import Button from "components/Button";

const DetailCategoryContainer = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: categoryDetails,
    error: categoryFetchError,
    isLoading: isLoadingCategory,
  } = useReadCategoryDetailsQuery(router.query.id, {});

  useEffect(() => {
    if (categoryFetchError) {
      addToast(
        "Error occured while fetching category details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [categoryFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Category List",
      route: "/admin/category",
    },
    {
      routeName: "Category Details",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Category Details"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Category Details</h1>
          <div className="flex justify-end">
            <Link href={`/admin/category/update/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          <div className="row ">
            {isLoadingCategory ? (
              "Loading..."
            ) : (
              <div className="flex items-start">
                <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md ">
                  <p>
                    <strong>Category Name:</strong>&nbsp;
                    {categoryDetails?.categoryName || "N/A"}
                  </p>
                  <p>
                    <strong>Category Description:</strong>&nbsp;
                    {categoryDetails?.categoryDescription || "N/A"}
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default DetailCategoryContainer;
