import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForInventory } from "utils/getQueryStringForTable";
// import { SelectColumnFilter, DateColumnFilter } from "components/Table/Filter";
// import OrderStatus from "container/OrderStatus/OrderStatus";
// import { color } from "constant/constant";
import Modal from "react-modal";
import { useReadInventoryQuery } from "services/api/Inventory/inventoryService";
import Button from "components/Button";
import InventoryManagement from "container/InventoryManagement/InventoryManagement";
// import { Input } from "postcss";

function InventoryContainer() {
  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [inventoryDataQuery, setInventoryDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const [orderQuantityId, setOrderQuantityId] = useState("");

  // const [stockAddedValue, setStockAddedVaue] = useState("");

  // console.log("stockAddedValue", stockAddedValue);

  const {
    data: inventoryData,
    isError: inventoryError,
    isFetching: isLoadingInventory,
  } = useReadInventoryQuery(inventoryDataQuery, {
    refetchOnMountOrArgChange: true,
  });

  const {
    data: inventoryDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadInventoryQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (inventoryDataAll) {
      setTableDataAll(
        inventoryDataAll.map((data) => {
          return {
            id: data?._id,
            productTitle: data?.productTitle,
            productId: data?.productId,
            color: data?.color,
            size: data?.size,
            quantity: data?.quantity,
            discountPercentage: data?.discountPercentage,
            discountedAmount: data?.discountedAmount,
          };
        })
      );
    }
  }, [inventoryDataAll]);

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "color",
        Header: "Color",
        accessor: "color",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "size",
        Header: "Size",
        accessor: "size",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        // canBeFiltered: true,
      },
      {
        id: "quantity",
        Header: "Quantity",
        accessor: "quantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        // canBeFiltered: true,
      },
      {
        id: "discountPercentage",
        Header: "Discount Percentage",
        accessor: "discountPercentage",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        // canBeFiltered: true,
      },
      {
        id: "discountedAmount",
        Header: "Discounted Price",
        accessor: "discountedAmount",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        // canBeFiltered: true,
      },
      {
        id: "addQuantity",
        Header: " Add Quantity",
        accessor: "addQuantity",
        Cell: ({ row: { original }, cell: { value } }) => {
          // console.log("original", original);
          return (
            <Button
              onClick={() => {
                // setOrderQuantityId(original?.productId);
                setOrderQuantityId({
                  inventoryId: original?.id,
                  productId: original?.productId,
                });
                setIsOpen(true);
              }}
            >
              Add
            </Button>
          );
        },
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForInventory(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    setInventoryDataQuery(query);
  };

  useEffect(() => {
    if (inventoryData) {
      setPageCount(inventoryData.totalPages);
      setTotalData(inventoryData.totalDocs);
      const value = setTableDataValue(inventoryData || []);

      setTableData(
        value.map((data) => {
          // console.log("data", data);
          return {
            id: data?._id,
            productTitle: data?.productTitle,
            productId: data?.productId,
            color: data?.color,
            size: data?.size,
            quantity: data?.quantity,
            discountPercentage: data?.discountPercentage,
            discountedAmount: data?.discountedAmount,
          };
        })
      );
    }
  }, [inventoryData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Inventory List",
      route: "",
    },
  ];

  const setTableDataValue = (productDetails) => {
    let arrayObject = [];
    const allAvailableOptions = productDetails?.docs?.map?.((detail) => {
      const availableOptionWithTitle = detail?.availableOptions?.map?.(
        (option) => {
          const availableOptionWithTitleValue = {
            ...option,
            productTitle: detail.productTitle,
            productId: detail.id,
          };

          return availableOptionWithTitleValue;
        }
      );

      return availableOptionWithTitle;
    });

    allAvailableOptions?.forEach?.((list) => {
      list.forEach((object) => {
        arrayObject = [...arrayObject, object];
      });
    });

    return arrayObject;
  };

  return (
    <AdminLayout documentTitle="View Inventory" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Modal
            isOpen={modalIsOpen}
            className="mymodal order"
            overlayClassName="myoverlay"
          >
            <button
              type="button"
              onClick={closeModal}
              className="text-error absolute top-[5px] right-[5px]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>

            <InventoryManagement
              setIsOpen={setIsOpen}
              orderQuantityId={orderQuantityId}
            ></InventoryManagement>
          </Modal>
          <Table
            tableName="Inventory/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={inventoryError}
            isLoadingData={isLoadingInventory}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="admin/product/update"
            // viewRoute="admin/product"
            hasExport={true}
            // addPage={{ page: "Add Product", route: "/admin/product/create" }}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default InventoryContainer;
