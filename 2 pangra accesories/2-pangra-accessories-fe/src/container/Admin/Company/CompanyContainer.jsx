import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForCompanyTable } from "utils/getQueryStringForTable";
// import { SelectColumnFilter } from "components/Table/Filter";
import {
  useReadCompanyQuery,
  useDeleteCompanyMutation,
} from "services/api/company";

function CompanyContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [companyDataQuery, setCompanyDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);

  const {
    data: companyDataAll,
    isError: isCompanyErrorAll,
    isFetching: isLoadingAll,
  } = useReadCompanyQuery(companyDataQuery);

  useEffect(() => {
    if (companyDataAll) {
      setTableDataAll(
        companyDataAll.docs.map((company) => {
          return {
            id: company?.id,
            companyName: company?.companyName,
            fullName: company?.fullName,
            mobile: company?.mobile,
          };
        })
      );
    }
  }, [companyDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteCompanyMutation();

  const columns = useMemo(
    () => [
      {
        id: "companyName",
        Header: "Delivery Company Name",
        accessor: "companyName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "fullName",
        Header: "Company Contact Person Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "mobile",
        Header: "Company Contact Person Mobile",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForCompanyTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    setCompanyDataQuery(query);
  };

  useEffect(() => {
    if (companyDataAll) {
      setPageCount(companyDataAll.totalPages);
      setTotalData(companyDataAll.totalDocs);
      setTableData(
        companyDataAll.docs.map((company) => {
          return {
            id: company?._id,
            companyName: company?.companyName,
            fullName: company?.fullName,
            mobile: company?.mobile,
          };
        })
      );
    }
  }, [companyDataAll]);

  const BreadCrumbList = [
    {
      routeName: "Company List",
      route: "/admin/company",
    },
  ];

  return (
    <AdminLayout
      documentTitle="Delivery Company"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Delivery Company/s"
            tableDataAll={tableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={isCompanyErrorAll}
            isLoadingData={isLoadingAll}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            editRoute="admin/company/update"
            viewRoute="admin/company"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            addPage={{ page: "Add Company", route: "/admin/company/create" }}
            hasDropdownBtn={true}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default CompanyContainer;
