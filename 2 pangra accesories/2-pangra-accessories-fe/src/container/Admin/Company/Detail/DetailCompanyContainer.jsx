import Image from "next/image";
import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";

import AdminLayout from "layouts/Admin";
import { useReadSingleCompanyQuery } from "services/api/company";
import Link from "next/link";
import Button from "components/Button";

const DetailBrandContainer = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: companyDetails,
    error: brandFetchError,
    isLoading: isLoadingBrand,
    error,
  } = useReadSingleCompanyQuery(router.query.id, { skip: !router.query.id });

  useEffect(() => {
    if (brandFetchError) {
      addToast(
        error?.data?.message ||
          "Error occured while fetching Brand details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [brandFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Company List",
      route: "/admin/company",
    },
    {
      routeName: "Company Details",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Company Details"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Company Details</h1>
          <div className="flex justify-end">
            <Link href={`/admin/company/update/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>

          {isLoadingBrand ? (
            "Loading..."
          ) : (
            <>
              <div className="row">
                <div className="three-col">
                  <div className="relative">
                    {companyDetails?.uploadBrandImage?.imageUrl && (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${companyDetails?.uploadBrandImage?.imageUrl}`}
                          alt="Brand Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    )}

                    {companyDetails?.companyImage?.imageUrl && (
                      <div
                        style={{
                          height: "100px",
                          width: "100px",
                        }}
                        className="absolute top-3 left-3 z-10"
                      >
                        <Image
                          src={`${companyDetails?.companyImage?.imageUrl}`}
                          alt="not found"
                          layout="fill"
                          className="
                        max-w-xs 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 my-4 shadow-lg min-w-[500px]">
                  <h5>Company Details</h5>
                  <p>
                    <strong>Company Name:</strong> &nbsp;
                    {companyDetails?.companyName}
                  </p>
                  <h5>Contact Person Details</h5>
                  <p>
                    <strong>Full Name:&nbsp;</strong>
                    {companyDetails?.fullName ? companyDetails.fullName : "N/A"}
                  </p>
                  <p>
                    <strong>Mobile:&nbsp;</strong>
                    {companyDetails?.mobile ? companyDetails.mobile : "N/A"}
                  </p>
                  <p>
                    <strong>Email:&nbsp;</strong>
                    {companyDetails?.email ? companyDetails.email : "N/A"}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default DetailBrandContainer;
