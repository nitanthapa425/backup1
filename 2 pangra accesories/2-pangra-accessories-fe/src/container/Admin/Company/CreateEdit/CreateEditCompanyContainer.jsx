import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";
import PropTypes from "prop-types";
import Link from "next/link";

import Button from "components/Button";
import Input from "components/Input";
import AdminLayout from "layouts/Admin";

import {
  useCreateCompanyMutation,
  useReadSingleCompanyQuery,
  useUpdateCompanyMutation,
} from "services/api/company";
import { companyValidationSchema } from "validation/company.validation";
import Popup from "components/Popup";
import { useRouter } from "next/router";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";

const CreateEditCompanyContainer = ({ type }) => {
  const router = useRouter();
  const { addToast, removeAllToasts } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [editBrandInitialValue, setEditBrandInitialValue] = useState({});
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();

  const addCompanyInitialValue = {
    companyName: "",
    fullName: "",
    mobile: "",
    email: "",
  };

  const [
    createCompany,
    {
      isLoading: createLoading,
      isError: isCreateError,
      isSuccess: isCreateSuccess,
      data: createSuccessData,
      error: errorMessage,
    },
  ] = useCreateCompanyMutation();

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isCreateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        <>
          Company added successfully.{" "}
          <Link href={`/admin/company/${createSuccessData?.companyId}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Company
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isCreateSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isCreateError) {
      addToast(errorMessage?.data?.message || "Error creating company.", {
        appearance: "error",
      });
    }
  }, [isCreateError]);

  const { data: companyDetail, error: companyFetchError } =
    useReadSingleCompanyQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  useEffect(() => {
    if (companyFetchError) {
      addToast(
        "Error occured while fetching Brand details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [companyFetchError]);

  useEffect(() => {
    if (companyDetail) {
      const newCompanyDetails = {
        companyName: companyDetail?.companyName,
        fullName: companyDetail.fullName,
        mobile: companyDetail.mobile,
        email: companyDetail.email,
      };
      setEditBrandInitialValue(newCompanyDetails);
    }
  }, [companyDetail]);

  const [
    updateCompany,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateCompanyMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("Error updating brand.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(updateSuccessData?.message || "Company updated successfully.", {
        appearance: "success",
      });
      setChanged(false);

      router.push(`/admin/company/${router.query.id}`);
    }
  }, [isUpdateSuccess]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      const payloadData = {
        companyName: values.companyName,
        fullName: values.fullName,
        mobile: values.mobile,
        email: values.email,
      };
      createCompany(payloadData);
    }
    if (type === "edit") {
      updateCompany({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };

  const BreadCrumbList = [
    {
      routeName: "Add Delivery Person",
      route: "/admin/user/create",
    },
    {
      routeName: "Company List",
      route: "/admin/company",
    },
    {
      routeName: "Add company",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle={
        type === "add" ? "Add New Company" : "Edit Company Details"
      }
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? editBrandInitialValue : addCompanyInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={companyValidationSchema}
          enableReinitialize
          innerRef={formikBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add"
                      ? "Add New Company"
                      : "Edit Company Details"}
                  </h3>
                  <div className="mb-2">
                    <Input
                      label="Company Name"
                      name="companyName"
                      type="text"
                      placeholder="E.g: Racer"
                    />
                  </div>
                  <div className="mb-2">
                    <span>
                      <h4>Contact Person</h4>
                    </span>
                  </div>
                  <div className="mb-2">
                    <Input
                      label="Full Name"
                      name="fullName"
                      type="text"
                      placeholder="E.g: Arjun Subedi"
                    />
                  </div>
                  <div className="mb-2">
                    <Input
                      label="Mobile"
                      name="mobile"
                      type="text"
                      placeholder="E.g: 9862972729"
                    />
                  </div>
                  <div className="mb-2">
                    <Input
                      label="Email"
                      name="email"
                      type="email"
                      placeholder="E.g: arjunsubedi360@gmail.com"
                      required={false}
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={createLoading || updating}
                      disabled={
                        isSubmitting || !dirty || createLoading || updating
                      }
                    >
                      {type === "add" ? "Submit" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={
                        isSubmitting || !dirty || createLoading || updating
                      }
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

CreateEditCompanyContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default CreateEditCompanyContainer;
