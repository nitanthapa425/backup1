import { ChevronDownIcon, ChevronUpIcon } from "@heroicons/react/solid";
import { DateColumnFilter, SelectColumnFilter } from "components/Table/Filter";
import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useCallback, useMemo, useState } from "react";
import { useGetCommentsQuery } from "services/api/comment";
import { getQueryStringForTable } from "utils/getQueryStringForTable";
import ReplyModal from "./ReplyModal";

const breadCrumbList = [
  {
    routeName: "Add Product",
    route: "/admin/product/create",
  },
  {
    routeName: "All Comments",
    route: "",
  },
];

function CommentsContainer() {
  const [searchQuery, setSearchQuery] = useState(
    "?&sortBy=createdAt&sortOrder=-1"
  );
  const { data, isLoading, isError } = useGetCommentsQuery(searchQuery);

  const columns = useMemo(
    () => [
      {
        Header: () => null,
        id: "expander",
        accessor: "expander",
        Cell: ({ row }) => (
          <span {...row.getToggleRowExpandedProps()}>
            {row.isExpanded ? (
              <ChevronUpIcon className="w-4" />
            ) : (
              <ChevronDownIcon className="w-4" />
            )}
          </span>
        ),
      },
      {
        id: "message",
        Header: "Comment",
        accessor: "message",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "name",
        Header: "Commenter",
        accessor: "name",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "productTitle",
        Header: "Product",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        id: "isApproved",
        Header: "Is Approved ",
        accessor: "isApproved",
        Cell: ({ cell: { value } }) => (value ? "Approved " : "Not Approved "),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Approved",
            value: true,
          },
          {
            label: "Not Approved",
            value: false,
          },
        ],
      },
      {
        id: "createdAt",
        Header: "Commented At",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForTable(pageIndex, pageSize, sortBy, filters);
    setSearchQuery(query + "&noRegex=isApproved");
  };

  const renderRowSubComponent = useCallback(({ row }) => {
    return row?.original?.replyComment?.length > 0 ? (
      <table className="lg:w-3/4 ml-14 sm:w-full reply-table my-2 shadow">
        <thead>
          <tr>
            <th>Reply</th>
            <th>Replier</th>
            <th>Replied At</th>
          </tr>
        </thead>
        <tbody>
          {row?.original?.replyComment?.map((reply) => (
            <tr key={reply._id}>
              <td> {reply.replyMessage}</td>
              <td>{reply.fullName}</td>
              <td>
                {reply.createdAt
                  ? new Date(reply.createdAt).toLocaleDateString()
                  : "-"}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    ) : (
      <p className="ml-14 my-2 text-error">Sorry, No replies found.</p>
    );
  }, []);
  return (
    <AdminLayout BreadCrumbList={breadCrumbList} documentTitle="All Comments">
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Comments"
            columns={columns}
            data={data?.docs?.map((rep) => ({ ...rep, id: rep?._id })) || []}
            fetchData={getData}
            isFetchError={isError}
            isLoadingData={isLoading}
            pageCount={data?.totalPages}
            defaultPageSize={10}
            totalData={data?.totalDocs}
            rowOptions={[...new Set([10, 20, 30, data?.totalDocs])]}
            hasExport={true}
            hasDropdownBtn={true}
            dropdownBtnName="More"
            hideMultiple
            dropdownBtnOptions={[
              {
                hasModal: true,
                renderModalComponent: (id) => {
                  return <ReplyModal commentId={id} />;
                },
              },
            ]}
            renderRowSubComponent={renderRowSubComponent}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default CommentsContainer;
