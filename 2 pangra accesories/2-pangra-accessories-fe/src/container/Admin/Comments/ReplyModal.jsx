import Button from 'components/Button'
import Textarea from 'components/Input/textarea'
import { Form, Formik } from 'formik'
import React, { useEffect, useState } from 'react'
import ReactModal from 'react-modal'
import { useToasts } from 'react-toast-notifications'
import { usePostReplyToCommentMutation } from 'services/api/comment'
import { replyValidationSchema } from 'validation/comment.validation'

const ReplyModal = ({ commentId }) => {

  const { addToast } = useToasts();


  const [open, setOpen] = useState(false)
  const closeModal = () => setOpen(false)
  const openModal = () => setOpen(true)

  const [replyToComment, { isError, isLoading, isSuccess, error }] = usePostReplyToCommentMutation()


  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully replied to the comment", {
        appearance: "success",
      });
      closeModal()
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message || "Error occurred while replying to comment",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);
  return (
    <>
      <span onClick={openModal}>
        Reply To Comment
      </span>
      <ReactModal
        isOpen={open}
        onRequestClose={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
        id='replyModal'
      >
        <button
          type="button"
          onClick={closeModal}
          className="text-error absolute top-[10px] right-[10px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        {/* form */}
        <Formik
          initialValues={{ replyMessage: '' }}
          onSubmit={(values, { setSubmitting }) => {
            replyToComment({
              data: values,
              commentId,
            });
            setSubmitting(false);
          }}
          validationSchema={replyValidationSchema}
          enableReinitialize
        >
          {({ dirty, }) => (
            <div className="container mt-4">
              <h1 className="h6 mb-3">Reply to comment</h1>
              <Form>
                <Textarea
                  label="Reply Message"
                  name="replyMessage"
                  type="text"
                  placeholder="Type here..."
                  required={false}
                />

                <div className="mt-3">
                  <Button
                    loading={isLoading}
                    disabled={isLoading || !dirty}
                    type="submit"
                    className="btn-lg"
                  >
                    Submit
                  </Button>

                  <Button
                    disabled={isLoading || !dirty}
                    type="reset"
                    variant="outlined-error"
                  >
                    Clear
                  </Button>
                </div>
              </Form>
            </div>
          )}
        </Formik>
      </ReactModal>

    </>
  )
}

export default ReplyModal