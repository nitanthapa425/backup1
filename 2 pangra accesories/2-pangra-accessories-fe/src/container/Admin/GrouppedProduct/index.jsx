import React, { useEffect, useState } from "react";
import { TrashIcon } from "@heroicons/react/outline";
import { useToasts } from "react-toast-notifications";
import { sameArrayIrrespectiveToPosition } from "utils/sameArray";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import Popup from "components/Popup";
import Link from "next/link";
import Button from "components/Button";
import { sumOfEachElementOfArray } from "utils/methods";

import AdminLayout from "layouts/Admin";
import AdminSuperAdminCart from "container/Customer/ProductListingCom/AdminCart";
import { useCreateOrderItemMutation } from "services/api/ProductService";
import GroupOrderModal from "../Product/Detail/SellNow/GroupOrderModal";
import {
  useDeleteMyCartAdminMutation,
  useGetMyCartAdminQuery,
} from "services/api/adminCartList";

const GrouppedProducts = () => {
  const [open, setOpen] = useState(false);
  const closeModal = () => setOpen(false);
  const openModal = () => setOpen(true);
  const [orderGroupId, setOrderGroupId] = useState(null);
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Product Cart Group",
      route: "",
    },
  ];
  const { addToast } = useToasts();
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [totalCostList, setTotalCostList] = useState([]);
  const [deleteSingleCart, setDeleteSingleCart] = useState(null);
  const [openModalDeleteSingleCart, setOpenModalDeleteSingleCart] =
    useState(false);

  const [bookDet, setBookDet] = useState([]);
  const [bookList, setBookLis] = useState([]);

  const [deleteCartItems, setDeleteCartItems] = useState([]);
  // const bookList = useSelector((state) => state.book.bookList);

  const {
    data: dataMyCart,
    isFetching: isFetchingMyCart,
    isSuccess: isSuccessMyCart,
    refetch,
  } = useGetMyCartAdminQuery();

  useEffect(() => {
    refetch();
  }, []);

  const [
    deleteCart,
    {
      isLoading: isLoadingDeleteCart,
      isError: isErrorDeleteCart,
      isSuccess: isSuccessDeleteCart,
      error: errorDeleteCart,
      data: dataDeleteCart,
    },
  ] = useDeleteMyCartAdminMutation();

  const sellProductList = () => {
    const filterIfQuantity = bookDet.filter((data, i) => {
      return Number(
        data.quantity && data.size && data.color && data.discountedPrice
      );
    });

    const mapBookList = filterIfQuantity.map((data, i) => {
      return {
        color: data?.color,
        size: data?.size,
        quantity: data?.quantity,
        id: data?.id,
        discountedPrice: data?.discountedPrice,
      };
    });

    return mapBookList;
  };

  const handleBookNow = () => {
    createOrderItem({ bookList: sellProductList() });
  };

  const totalNumberOfQuantityOrder = () => {
    const filtredBookList = bookDet.filter((data, i) => {
      return bookList.includes(data.id);
    });

    const filterIfQuantity = filtredBookList.filter((data, i) => {
      return Number(data.quantity);
    });

    const totalQuantityList = filterIfQuantity.map((data, i) => {
      return Number(data?.quantity);
    });

    const totalQuantityListSum = sumOfEachElementOfArray(totalQuantityList);
    return totalQuantityListSum;
  };

  useEffect(() => {
    if (isSuccessDeleteCart) {
      addToast(
        dataDeleteCart?.message || "Item/s removed from the cart successfully.",
        {
          appearance: "success",
        }
      );
    }
  }, [isSuccessDeleteCart]);
  useEffect(() => {
    if (isErrorDeleteCart) {
      addToast(errorDeleteCart?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorDeleteCart, errorDeleteCart]);

  const totalPrice = (bookList = []) => {
    const array1 = totalCostList?.filter((data, i) =>
      bookList.includes(data.id)
    );

    const arrayPrice = array1?.map((data, i) => {
      return data.value || 0;
    });

    const total = sumOfEachElementOfArray(arrayPrice);
    return total;
  };

  useEffect(() => {
    const book = dataMyCart?.map?.((data, i) => {
      return {
        color: data?.color,
        size: data?.sizeValue,
        quantity: data?.quantity,
        id: data?.accessoriesId?.id,
        discountedPrice: 0,
        _id: data._id,
      };
    });
    setBookDet(book);
  }, [isSuccessMyCart]);

  const [createOrderItem, { isLoading, isSuccess, isError, error, data }] =
    useCreateOrderItemMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast("Successfully created the order", {
        appearance: "success",
      });
      refetch();
      openModal();

      setOrderGroupId(data?.orderGroupId);
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "Error occurred while creating order. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <AdminLayout
      BreadCrumbList={BreadCrumbList}
      documentTitle="Product Cart Group"
    >
      <div className="pb-[80px]">
        {isFetchingMyCart ? (
          <div className="container flex justify-center">
            <svg
              className={`animate-spin h-10 w-10 text-primary`}
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
            >
              <circle
                className="opacity-25"
                cx="12"
                cy="12"
                r="10"
                stroke="currentColor"
                strokeWidth="4"
              ></circle>
              <path
                className="opacity-75"
                fill="currentColor"
                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
              ></path>
            </svg>
          </div>
        ) : dataMyCart?.length ? (
          <div className="container">
            {openModalVehicleType && (
              <Popup
                title=" Are you sure you want to remove item/s from the cart?"
                description=" Please note that, once removed this cannot be undone."
                onOkClick={() => {
                  if (deleteCartItems?.length === 1) {
                    deleteCart(deleteCartItems[0])
                      .then(() => {
                        setOpenModalVehicleType(false);
                      })
                      .catch(() => {
                        setOpenModalVehicleType(false);
                      });
                  } else {
                    deleteCartItems.forEach((id) => {
                      deleteCart(id)
                        .then(() => {
                          setOpenModalVehicleType(false);
                        })
                        .catch(() => {
                          setOpenModalVehicleType(false);
                        });
                    });
                  }
                  // bookList.forEach((id, i) => {
                  //   deleteCart(id)
                  //     .then(() => {
                  //       setOpenModalVehicleType(false);
                  //     })
                  //     .catch(() => {
                  //       setOpenModalVehicleType(false);
                  //     });
                  // });
                }}
                onCancelClick={() => setOpenModalVehicleType(false)}
                okText="Delete"
                cancelText="Cancel"
                loading={isLoadingDeleteCart}
              />
            )}

            {openModalDeleteSingleCart && (
              <Popup
                title="Are you sure to remove item form cart list?"
                description="Please note that,
              once removed, this cannot be undone."
                onOkClick={() => {
                  deleteCart(deleteSingleCart)
                    .then(() => {
                      setOpenModalDeleteSingleCart(false);
                    })
                    .catch(() => {
                      setOpenModalDeleteSingleCart(false);
                    });
                }}
                onCancelClick={() => setOpenModalDeleteSingleCart(false)}
                okText="Delete"
                cancelText="Cancel"
                loading={isLoadingDeleteCart}
              />
            )}

            <div className="flex flex-wrap items-start">
              <div className="flex-1 md:mr-[25px] ">
                <div className="flex justify-between items-center py-2 mb-2">
                  <div className="flex items-center space-x-2">
                    <input
                      type="checkbox"
                      // checked if two array are same
                      checked={sameArrayIrrespectiveToPosition(
                        bookList,
                        dataMyCart?.map((data, i) => data?.accessoriesId?.id)
                        // dataMyCart?.map((data, i) => data?._id)
                      )}
                      className="checkbox w-4 h-4 border border-gray-500 inline-block"
                      onClick={(e) => {
                        if (e.target.checked) {
                          setBookLis((bookList) => {
                            bookList = [];
                            return dataMyCart?.map((data, i) => {
                              return data?.accessoriesId?.id;
                              // return data?._id;
                            });
                          });
                          setDeleteCartItems((deleteCartItems) => {
                            deleteCartItems = [];
                            return dataMyCart?.map((data, i) => {
                              return data?._id;
                            });
                          });
                        } else {
                          setBookLis([]);
                          setDeleteCartItems([]);
                        }
                      }}
                    ></input>
                    <span className="inline-block ml-2">
                      Select all items{" "}
                      {bookList.length ? `(${bookList.length} item/s)` : ""}
                    </span>
                  </div>
                  <div className="flex items-center ">
                    <button
                      onClick={() => {
                        setOpenModalVehicleType(true);
                      }}
                    >
                      {bookList.length ? (
                        <TrashIcon className="w-5 h-5 inline-block mr-2 hover:text-error " />
                      ) : (
                        ""
                      )}
                    </button>
                  </div>
                </div>
                {dataMyCart?.map((data, index) => {
                  return (
                    <div key={index}>
                      <AdminSuperAdminCart
                        data={data}
                        index={index}
                        bookList={bookList}
                        setBookLis={setBookLis}
                        deleteCartItems={deleteCartItems}
                        setDeleteCartItems={setDeleteCartItems}
                        bookDet={bookDet}
                        setBookDet={setBookDet}
                        setOpenModalDeleteSingleCart={
                          setOpenModalDeleteSingleCart
                        }
                        setDeleteSingleCart={setDeleteSingleCart}
                        setTotalCostList={setTotalCostList}
                        totalCostList={totalCostList}
                      ></AdminSuperAdminCart>
                    </div>
                  );
                })}
              </div>
              <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-[75px]">
                <h1 className="h5 mb-3">Order Summary</h1>
                {/* <div className="border-b-2 border-gray-200 mb-2">
                <div className="flex flex-wrap justify-between ">
                  <p className="mb-1">
                    SubTotal{" "}
                    {bookList.length ? `(${bookList.length} item/s)` : ""}
                  </p>
                  <p className="mb-1">
                    NRs. {thousandNumberSeparator(totalPrice(bookList))}
                  </p>
                </div>
              </div> */}
                <div className="flex flex-wrap justify-between mb-3">
                  <p className="mb-1">
                    Total Amount{" "}
                    {bookList.length
                      ? `(${totalNumberOfQuantityOrder()} product/s)`
                      : ""}
                  </p>
                  <p className="mb-1">
                    NRs. {thousandNumberSeparator(totalPrice(bookList))}
                  </p>
                </div>

                <Button
                  loading={isLoading}
                  disabled={bookList.length === 0 || !sellProductList().length}
                  onClick={handleBookNow}
                >
                  Sell Now
                </Button>
              </div>
            </div>
          </div>
        ) : (
          <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
            <div className="max-w-full">
              <img src="/images/empty-cart.svg" />
            </div>
            <div className="text-holder">
              <h1 className="h4">Oops! Your cart is empty</h1>
              <p>
                Looks like you havent made your <br />
                choice yet
              </p>

              <Link href={"/product-listing"}>
                <a className="btn btn-primary">Continue Browsing</a>
              </Link>
            </div>
          </div>
        )}
      </div>
      <GroupOrderModal
        open={open}
        closeModal={closeModal}
        orderGroupId={orderGroupId}
      ></GroupOrderModal>
    </AdminLayout>
  );
};

export default GrouppedProducts;
