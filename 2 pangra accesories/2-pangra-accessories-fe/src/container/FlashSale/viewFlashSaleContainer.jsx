// import { DateColumnFilter } from 'components/Table/Filter';
import { DateTimeColumnFilter } from "components/Table/Filter";
import Table from "components/Table/table";
import { useState, useEffect, useMemo } from "react";
import { useToasts } from "react-toast-notifications";

import { getQueryStringFlashSell } from "utils/getQueryStringForTable";

import {
  useGetAllFlashSaleProductsQuery,
  useRemoveProductFromFlashSaleMutation,
} from "services/api/flashSale";

function ViewFlashSaleContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [flashSaleDataQuery, setflashSaleDataQuery] = useState("");

  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: flashSaleData,
    isError: flashSaleError,
    isFetching: isLoadingFlashSale,
  } = useGetAllFlashSaleProductsQuery(flashSaleDataQuery);

  const { data: flashSaleDataAll, isFetching: isLoadingAll } =
    useGetAllFlashSaleProductsQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });
  const [
    removeFlashSale,
    {
      error: errorRemoveFlashSale,
      isLoading: isRemovingFlashSale,
      isSuccess: isSuccessRemoveaFlashSale,
      data: dataRemoveFlashSale,
    },
  ] = useRemoveProductFromFlashSaleMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };
  useEffect(() => {
    if (errorRemoveFlashSale) {
      showFailureToast(
        errorRemoveFlashSale?.data?.message ||
        "Failed to remove from flash sale"
      );
    }
    if (isSuccessRemoveaFlashSale) {
      showSuccessToast(
        dataRemoveFlashSale?.message || "Successfully removed from flash sale"
      );
    }
  }, [errorRemoveFlashSale, isSuccessRemoveaFlashSale]);

  useEffect(() => {
    if (flashSaleDataAll) {
      setTableDataAll(
        flashSaleDataAll?.docs?.map((product) => {
          return {
            id: product?._id,
            productTitle: product?.productTitle,
            startDate: product?.startDate,
            endDate: product?.endDate,
            costPrice: product?.costPrice,
            discountPercentage: product?.discountPercentage,
            flashSalePrice: product?.flashSalePrice,
          };
        })
      );
    }
  }, [flashSaleDataAll]);

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "Cost Price",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "discountPercentage",
        Header: "Discount Percentage",
        accessor: "discountPercentage",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "flashSalePrice",
        Header: "Flash Sale Price",
        accessor: "flashSalePrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "startDate",
        Header: "Start Date",
        accessor: "startDate",
        Cell: ({ cell: { value } }) =>
          `${new Date(value).toLocaleDateString()} ${new Date(
            value
          ).toLocaleTimeString()}` || "-",
        Filter: DateTimeColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "endDate",
        Header: "End Date",
        accessor: "endDate",
        Cell: ({ cell: { value } }) =>
          `${new Date(value).toLocaleDateString()} ${new Date(
            value
          ).toLocaleTimeString()}` || "-",
        Filter: DateTimeColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringFlashSell(pageIndex, pageSize, sortBy, filters);

    setflashSaleDataQuery(query);
  };

  useEffect(() => {
    if (flashSaleData) {
      setPageCount(flashSaleData?.totalPages);
      setTotalData(flashSaleData?.totalDocs);

      setTableData(
        flashSaleData?.docs?.map((product) => {
          return {
            id: product?._id,
            productTitle: product?.productTitle,
            startDate: product?.startDate,
            endDate: product?.endDate,
            costPrice: product?.costPrice,
            discountPercentage: product?.discountPercentage,
            flashSalePrice: product?.flashSalePrice,
          };
        })
      );
    }
  }, [flashSaleData]);

  return (
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="HotDeal/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={flashSaleError}
          isLoadingData={isLoadingFlashSale}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          hasExport={true}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          hasDropdownBtn={true}
          dropdownBtnName="Remove"
          dropdownBtnOptions={[
            {
              name: " From Flash Sale",
              onClick: removeFlashSale,
              isLoading: isRemovingFlashSale,
              loadingText: "Removing...",
            },
          ]}
        />
      </div>
    </section>
    // </AdminLayout>
  );
}

export default ViewFlashSaleContainer;
