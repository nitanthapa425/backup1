import { useRouter } from "next/router";
import React, { useRef, useEffect } from "react";
import { useVerifyEmailMutation } from "services/api/signup";
import { useToasts } from "react-toast-notifications";
import Button from "components/Button";
import { Form, Formik } from "formik";
import PasswordInput from "components/Input/PasswordInput";

import { VerifyEmailSchema } from "validation/confirm.Validation";

const VerifyEmail = () => {
  const { addToast } = useToasts();
  const formikBag = useRef();
  const router = useRouter();
  const query = router.query;

  const [
    verifyEmail,
    {
      isLoading: isLoadingVerifyEmail,
      isSuccess: isSuccessVerifyEmail,
      isError: isErrorVerifyEmail,
      error: errorVerifyEmail,
      data: dataVerifyEmail,
    },
  ] = useVerifyEmailMutation();

  useEffect(() => {
    if (isSuccessVerifyEmail) {
      addToast(
        dataVerifyEmail?.message ||
          "Congratulation your account has been verified successfully.",
        {
          appearance: "success",
        }
      );
      router.push("/admin");
    }
  }, [isSuccessVerifyEmail]);

  useEffect(() => {
    if (isErrorVerifyEmail) {
      addToast(errorVerifyEmail?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorVerifyEmail]);

  // if (isLoadingVerifyEmail) {
  //   return <div>Loading...</div>;
  // }

  if (isErrorVerifyEmail) {
    return (
      <div className="flex justify-center items-center">
        <div>Sorry your account could not be verified at this time</div>
      </div>
    );
  }

  return (
    <>
      <Formik
        initialValues={{
          passwordHash: "",
          confirmPassword: "",
        }}
        onSubmit={(values, { resetForm }) => {
          const payloadData = {
            passwordHash: values.passwordHash,
            email: query.email,
            verificationToken: query.verificationToken,
          };

          verifyEmail(payloadData);
        }}
        validationSchema={VerifyEmailSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div
                  className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
                  style={{ position: "relative" }}
                >
                  <h3 className="mb-3">Set Your Password</h3>

                  <div className="row-sm">
                    <PasswordInput
                      label="New Password"
                      name="passwordHash"
                      placeholder="New Password"
                    ></PasswordInput>
                    <PasswordInput
                      label="Confirm Password"
                      name="confirmPassword"
                      placeholder="Confirm Password"
                    ></PasswordInput>
                  </div>

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      disabled={
                        isSubmitting ||
                        !dirty ||
                        isLoadingVerifyEmail ||
                        isLoadingVerifyEmail
                      }
                    >
                      Set
                    </Button>
                    <Button
                      variant="outlined"
                      type="button"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={
                        isSubmitting ||
                        !dirty ||
                        isLoadingVerifyEmail ||
                        isLoadingVerifyEmail
                      }
                    >
                      Clear
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default VerifyEmail;
