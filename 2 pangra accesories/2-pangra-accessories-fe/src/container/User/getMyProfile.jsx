import React, { useEffect } from "react";
import Image from "next/image";
import AdminLayout from "layouts/Admin";

import { useReadMyProfileDetailsQuery } from "services/api/signup";
import Link from "next/link";
import Button from "components/Button";
import { useDispatch } from "react-redux";
import { setAdminToken, setUser } from "config/adminStorage";
import { adminActions } from "store/features/adminAuth/adminAuthSlice";

const GetMyProfileComponent = () => {
  const dispatch = useDispatch();

  const {
    data: userProfile,
    isLoading: isLoadinguserProfile,
    // isSuccess: isSuccesUserProfile,
  } = useReadMyProfileDetailsQuery();

  useEffect(() => {
    setAdminToken(userProfile?.accessToken);
    setUser(userProfile);
    dispatch(adminActions.setAdminToken());
    dispatch(adminActions.setUser());
  }, [userProfile]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "User List",
      route: "/admin/user",
    },
    {
      routeName: "User Profile",
      route: "",
    },
  ];
  return (
    <AdminLayout documentTitle="Get My Profile" BreadCrumbList={BreadCrumbList}>
      <section className="brand-detail-section">
        <div className="container">
          <div className="flex justify-end">
            <Link href={`/admin/user/my-profile/update`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>
          {isLoadinguserProfile ? (
            "Loading..."
          ) : (
            <>
              <div className="row">
                <div className="four-col">
                  <div
                  // border border-gray-200 pt-3 pb-2 px-4 rounded-md
                  // className="relative"
                  >
                    {userProfile?.profileImagePath?.imageUrl ? (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${userProfile?.profileImagePath?.imageUrl}`}
                          alt="Profile Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    ) : (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src="/user.JPG"
                          alt="Profile Image"
                          layout="fill"
                        />
                      </div>
                    )}
                    <p>{userProfile?.accessLevel}</p>
                  </div>
                </div>
                <div className="mx-3">
                  <p>
                    <strong>Username:&nbsp;</strong>
                    <strong>{userProfile?.userName}&nbsp;</strong>
                  </p>

                  <p>
                    <strong>Email:&nbsp;</strong>
                    {/* {brandDetails?.brandVehicleDescription
                      ? brandDetails.brandVehicleDescription
                      : 'N/A'} */}
                    {userProfile?.email}
                  </p>
                  <p>
                    <strong>Full Name:</strong> &nbsp;
                    {userProfile?.fullName}
                  </p>

                  <p>
                    <strong>Gender:</strong> &nbsp;
                    {userProfile?.gender && userProfile?.gender}
                  </p>
                  <p>
                    <strong>Mobile Number:</strong> &nbsp;
                    {userProfile?.mobile}
                  </p>

                  <p>
                    <strong>Date of Birth:</strong> &nbsp;
                    {userProfile?.dob
                      ? new Date(userProfile?.dob).toLocaleDateString()
                      : null}
                  </p>
                </div>
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetMyProfileComponent;
