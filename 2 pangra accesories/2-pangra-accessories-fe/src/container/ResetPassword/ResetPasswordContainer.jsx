import React, { useRef, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Form, Formik } from "formik";
import Head from "next/head";

import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import { useDeliveryResetPasswordPostMutation } from "services/api/resetPassword";

import { PasswordUpdateSchema } from "validation/RestPasswordValidation";
import PasswordInput from "components/Input/PasswordInput";
import Popup from "components/Popup";

const ResetPasswordContainer = () => {
  const [openModal, setOpenModal] = useState(false);

  const router = useRouter();
  const query = router.query;

  const formikBag = useRef();
  const { addToast } = useToasts();
  const [createResetPassword, { isError, error, isSuccess }] =
    useDeliveryResetPasswordPostMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast("Password reset successfully done.", {
        appearance: "success",
      });
      router.push("/admin");
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "Error occurred while resetting password. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <>
      <Head>
        <title>2Pangra | Reset Password</title>
      </Head>
      <Formik
        initialValues={{
          newPassword: "",
          confirmPassword: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          const payloadData = {
            newPassword: values.newPassword,
            email: query.email,
            verificationToken: query.verificationToken,
          };

          createResetPassword(payloadData);
          setSubmitting(false);
        }}
        validationSchema={PasswordUpdateSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,

          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div
                  className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
                  style={{ position: "relative" }}
                >
                  <h3 className="mb-3">Reset Password</h3>

                  <div className="row-sm">
                    <PasswordInput
                      label="New Password"
                      name="newPassword"
                      placeholder="New Password"
                    ></PasswordInput>
                    <PasswordInput
                      label="Confirm Password"
                      name="confirmPassword"
                      placeholder="Confirm Password"
                    ></PasswordInput>
                  </div>

                  <div className="btn-holder mt-3">
                    <Button type="submit" disabled={isSubmitting || !dirty}>
                      Reset
                    </Button>
                    <Button
                      variant="outlined-error"
                      type="button"
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      disabled={isSubmitting || !dirty}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default ResetPasswordContainer;
