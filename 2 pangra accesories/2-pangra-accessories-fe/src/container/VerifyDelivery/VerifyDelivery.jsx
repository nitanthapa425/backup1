import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";

// import DropZone from "components/DropZone";
import Input from "components/Input";
// import DeliveryLayout from "layouts/Delivery";

import { useVerifyDeliveryProductMutation } from "services/api/deliveryLogin";
// import { companyValidationSchema } from "validation/company.validation";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Button from "components/Button";

const VerifyDeliveryContainer = (props) => {
  const { addToast, removeAllToasts } = useToasts();
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const formikBag = useRef();

  const addCompanyInitialValue = {
    deliveryToken: "",
  };

  const [
    verifyDelivery,
    {
      isLoading: verifyLoading,
      isError: isCreateError,
      isSuccess: isCreateSuccess,
      //   data: createSuccessData,
      error: errorMessage,
      data,
    },
  ] = useVerifyDeliveryProductMutation();

  useEffect(() => {
    formikBag.current?.resetForm();
    if (isCreateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        data?.message || "You have successfully delivered product. Thank you.",
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      props.setIsOpen(false);
      setChanged(false);
    }
  }, [isCreateSuccess]);

  useEffect(() => {
    return removeAllToasts;
  }, []);

  useEffect(() => {
    if (isCreateError) {
      addToast(errorMessage?.data?.message || "Error creating company.", {
        appearance: "error",
      });
    }
  }, [isCreateError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    // createBrand(values, resetForm);
    const payloadData = {
      deliveryToken: values.deliveryToken,
      orderItemsIds: props.deliveredItems,
    };
    verifyDelivery(payloadData);
    setSubmitting(false);
  };

  return (
    // <DeliveryLayout
    //   BreadCrumbList={BreadCrumbList}
    // >
    <div className="container">
      <Formik
        initialValues={addCompanyInitialValue}
        onSubmit={onSubmit}
        // validationSchema={companyValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form onChange={() => setChanged(true)}>
            {/* <button
                className="mb-3 hover:text-primary"
                onClick={() => router.back()}
              >
                Go Back
              </button> */}
            <div className="flex justify-center pt-3 pb-4">
              <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                <h3 className="mb-3">Please enter delivery token</h3>
                <div className="mb-2">
                  <Input
                    label="Delivery Token"
                    name="deliveryToken"
                    type="text"
                    placeholder="E.g: 1234"
                  />
                </div>
                <Button type="submit" disabled={isSubmitting || verifyLoading}>
                  Submit
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
    // </DeliveryLayout>
  );
};

export default VerifyDeliveryContainer;
