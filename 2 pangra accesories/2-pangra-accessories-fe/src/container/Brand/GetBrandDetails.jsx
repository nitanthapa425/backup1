import Image from 'next/image';
import React from 'react';
import AdminLayout from 'layouts/Admin';
import { useReadBrandQuery } from 'services/api/brand';
// import { IMAGE_BASE_URL } from 'config';

const GetBrandDetails = () => {
  const { data: brandData } = useReadBrandQuery();

  return (
    <AdminLayout documentTitle="Get Brand">
      {/* {isError && <Alert type="error" message={'error occured'}></Alert>} */}
      {/* {isError && (
        <Alert type="error" message={<pre>{error?.data?.message}</pre>}></Alert>
      )}
      {isSuccess && <Alert type="success" message="Post Successfully"></Alert>} */}
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Brand Details</h1>
          <div className="row ">
            {brandData?.docs?.map?.((brand, i) => {
              return (
                <div key={i} className="three-col">
                  <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md relative">
                    <div
                      // style={{ height: '255px', width: '100%' }}
                      className="relative brand-img "
                    >
                      <Image
                        src={`${brand.uploadBrandImage}`}
                        alt="not found"
                        layout="fill"
                        className=" 
                 
                  rounded
                  cursor-pointer
                  transition duration-200 ease-in-out
                  transform  hover:scale-125"
                      />
                    </div>
                    <div
                      style={{
                        height: '100px',
                        width: '100px',
                      }}
                      className="absolute top-3 left-3 z-10"
                    >
                      <Image
                        src={`${brand.companyImage}`}
                        alt="not found"
                        layout="fill"
                        className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                      />
                    </div>
                    <p className="ellipsis">
                      Brand Vehicle Description:&nbsp;
                      {brand?.brandVehicleDescription}
                    </p>
                    <p className="ellipsis">
                      Brand Vehicle Name:&nbsp;{brand?.brandVehicleName}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetBrandDetails;
