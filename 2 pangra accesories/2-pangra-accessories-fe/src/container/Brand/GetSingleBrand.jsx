import Image from "next/image";
import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useToasts } from "react-toast-notifications";

import AdminLayout from "layouts/Admin";
import { useReadBrandDetailsQuery } from "services/api/brand";
import { useGetModelFromBrandQuery } from "services/api/model";
import Link from "next/link";
import Button from "components/Button";

const GetSingleBrand = () => {
  const router = useRouter();
  const { addToast } = useToasts();

  const {
    data: brandDetails,
    error: brandFetchError,
    isLoading: isLoadingBrand,
    error,
  } = useReadBrandDetailsQuery(router.query.id, { skip: !router.query.id });

  const { data: modelData } = useGetModelFromBrandQuery(router.query.id);

  useEffect(() => {
    if (brandFetchError) {
      addToast(
        error?.data?.message ||
          "Error occured while fetching Brand details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [brandFetchError]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Brand List",
      route: "/vehicle/brand/view",
    },
    {
      routeName: "Brand Details",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Get Single Brand"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="brand-detail-section">
        <div className="container">
          {/* <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb> */}

          {/* <button
            className="mb-3 hover:text-primary"
            onClick={() => router.back()}
          >
            Go Back
          </button> */}
          <h1 className="h3 mb-5">Brand Details</h1>
          <div className="flex justify-end">
            <Link href={`/vehicle/brand/edit/${router.query.id}`}>
              <a>
                <Button variant="outlined" type="button">
                  Edit
                </Button>
              </a>
            </Link>
          </div>

          {isLoadingBrand ? (
            "Loading..."
          ) : (
            <>
              <div className="row">
                <div className="three-col">
                  <div
                    // border border-gray-200 pt-3 pb-2 px-4 rounded-md
                    className="relative"
                  >
                    {brandDetails?.uploadBrandImage?.imageUrl && (
                      <div className="rounded-md relative brand-img mb-3 border border-gray-200 pt-3 pb-2 px-4">
                        <Image
                          src={`${brandDetails?.uploadBrandImage?.imageUrl}`}
                          alt="Brand Image"
                          layout="fill"
                          className=" 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    )}

                    {brandDetails?.companyImage?.imageUrl && (
                      <div
                        style={{
                          height: "100px",
                          width: "100px",
                        }}
                        className="absolute top-3 left-3 z-10"
                      >
                        <Image
                          src={`${brandDetails?.companyImage?.imageUrl}`}
                          alt="not found"
                          layout="fill"
                          className="
                        max-w-xs 
                        rounded
                        cursor-pointer
                        transition duration-200 ease-in-out
                        transform  hover:scale-125"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="mx-3">
                  <p>
                    <strong>Brand Name:</strong> &nbsp;
                    {brandDetails?.brandVehicleName}
                  </p>
                  <p>
                    <strong>Brand Description:&nbsp;</strong>
                    {brandDetails?.brandVehicleDescription
                      ? brandDetails.brandVehicleDescription
                      : "Not available"}
                  </p>
                </div>

                {modelData?.length && (
                  <div className="mx-3">
                    <h5>Model List</h5>
                    {modelData?.map((brandModel, i) => {
                      return (
                        <div key={i}>
                          <Link href={`/vehicle/model/get/${brandModel?.id}`}>
                            <a>{brandModel?.modelName}</a>
                          </Link>
                          <br></br>
                        </div>
                      );
                    })}
                  </div>
                )}
              </div>
            </>
          )}
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetSingleBrand;
