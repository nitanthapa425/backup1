import { DateColumnFilter, SelectColumnFilter } from "components/Table/Filter";
import Table from "components/Table/table";

import { useState, useEffect, useMemo } from "react";
import { useSelector } from "react-redux";
import { useToasts } from "react-toast-notifications";
// import {
//   useApproveOfferPriceMutation,
//   useDeleteOfferPriceMutation,
//   useGetOfferDetailsQuery,
// } from 'services/api/offerPrice';
import ReactModal from "components/ReactModal/ReactModal";

import { getQueryStringForOfferList } from "utils/getQueryStringForTable";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import RejectOfferPricePopUp from "./reject";
import FinalOfferPricePopUp from "./finalOffer";
import {
  useApproveOfferPriceMutation,
  useDeleteOfferPriceMutation,
  useGetOfferDetailsQuery,
} from "services/api/seller";

function ViewOfferPriceContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [offersDataQuery, setOffersDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  const level = useSelector((state) => state.levelReducer.level);

  const {
    data: offersData,
    isError: offersError,
    isFetching: isLoadingOffers,
  } = useGetOfferDetailsQuery(offersDataQuery);

  const [
    approveOffer,
    {
      error: errorApproveOffer,
      isLoading: isAddingApproveOffer,
      isSuccess: isSuccessApproveOffer,
    },
  ] = useApproveOfferPriceMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };
  useEffect(() => {
    if (errorApproveOffer) {
      showFailureToast(
        errorApproveOffer?.data?.message || "Failed to approve offer"
      );
    }
    if (isSuccessApproveOffer) {
      showSuccessToast("Offer is successfully approved ");
    }
  }, [errorApproveOffer, isSuccessApproveOffer]);

  const {
    data: offersDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useGetOfferDetailsQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (offersDataAll) {
      setTableDataAll(
        offersDataAll?.docs?.map((offers) => {
          return {
            vehicleName: offers?.vehicleName,
            offerPrice: thousandNumberSeparator(offers?.offerPrice),
            realPrice: thousandNumberSeparator(offers?.realPrice),

            sellerName: offers?.sellerName,
            buyerName: offers?.buyerName,
            status: offers?.status,
            finalOffer: thousandNumberSeparator(offers?.finalOffer),

            createdAt: offers?.createdAt,
            id: offers?.id,
          };
        })
      );
    }
  }, [offersDataAll]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteOfferPriceMutation();

  const columns = useMemo(
    () => [
      {
        id: "vehicleName",
        Header: "Vehicle Name",
        accessor: "vehicleName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "offerPrice",
        Header: "Offer Price (NRs.)",
        accessor: "offerPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "realPrice",
        Header: "Real Price (NRs.)",
        accessor: "realPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "sellerName",
        Header: "Seller Name",
        accessor: "sellerName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "buyerName",
        Header: "Buyer Name",
        accessor: "buyerName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "finalOffer",
        Header: "Final Offer (NRs.)",
        accessor: "finalOffer",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "createdAt",
        Header: "CreatedAt",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "status",
        Header: "Status",
        accessor: "status",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "pending",
            value: "pending",
          },
          {
            label: "approved",
            value: "approved",
          },
          {
            label: "rejected",
            value: "rejected",
          },
          {
            label: "final offer",
            value: "final offer",
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForOfferList(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setOffersDataQuery(query);
  };

  useEffect(() => {
    if (offersData) {
      setPageCount(offersData.totalPages);
      setTotalData(offersData.totalDocs);

      setTableData(
        offersData?.docs?.map((offers) => {
          // console.log(offers);
          return {
            vehicleName: offers?.vehicleName,
            offerPrice: thousandNumberSeparator(offers?.offerPrice),
            realPrice: thousandNumberSeparator(offers?.realPrice),

            sellerName: offers?.sellerName,
            buyerName: offers?.buyerName,
            status: offers?.status,
            finalOffer: thousandNumberSeparator(offers?.finalOffer),

            createdAt: offers?.createdAt,
            id: offers?.id,
          };
        })
      );
    }
  }, [offersData]);

  return (
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="Offer/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={offersError}
          isLoadingData={isLoadingOffers}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          deleteQuery={deleteItems}
          isDeleting={isDeleting}
          isDeleteError={isDeleteError}
          isDeleteSuccess={isDeleteSuccess}
          DeletedError={DeletedError}
          hasExport={true}
          hasDropdownBtn={true}
          hideMultiple={true}
          dropdownBtnName={
            level === "superAdmin"
              ? "Accept/Reject"
              : level === "customer"
              ? ""
              : ""
          }
          dropdownBtnOptions={
            level === "superAdmin"
              ? [
                  {
                    name: "Approve Offer",
                    onClick: approveOffer,
                    isLoading: isAddingApproveOffer,
                    loadingText: "Approving...",
                  },
                  {
                    hasModal: true,
                    // modalComponent: (
                    //   <a className="text-textColor hover:text-primary ">
                    //     <ReactModal link="Reject Offer">
                    //       <RejectOfferPricePopUp />
                    //     </ReactModal>
                    //   </a>
                    // ),
                    renderModalComponent: (id) => {
                      return (
                        <a className="text-textColor hover:text-primary ">
                          <ReactModal link="Reject Offer">
                            <RejectOfferPricePopUp id={id} />
                          </ReactModal>
                        </a>
                      );
                    },
                  },
                  {
                    hasModal: true,
                    // modalComponent: (
                    //   <a className="text-textColor hover:text-primary ">
                    //     <ReactModal link="Reject Offer">
                    //       <RejectOfferPricePopUp />
                    //     </ReactModal>
                    //   </a>
                    // ),
                    renderModalComponent: (id) => {
                      return (
                        <a className="text-textColor hover:text-primary ">
                          <ReactModal link="Final Offer">
                            <FinalOfferPricePopUp id={id} />
                          </ReactModal>
                        </a>
                      );
                    },
                  },
                  // {
                  //   hasModal: true,
                  //   modalComponent: (
                  //     <a className="text-textColor hover:text-primary  ">
                  //       <ReactModal link="Final Offer">
                  //         <FinalOfferPricePopUp />
                  //       </ReactModal>
                  //     </a>
                  //   ),
                  // },
                ]
              : level === "customer"
              ? []
              : []
          }
        />
      </div>
    </section>
  );
}

export default ViewOfferPriceContainer;
