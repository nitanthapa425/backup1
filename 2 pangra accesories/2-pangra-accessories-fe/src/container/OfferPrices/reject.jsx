import { useEffect } from 'react';
import { Form, Formik } from 'formik';
// import Input from 'components/Input';
import Button from 'components/Button';
import { useToasts } from 'react-toast-notifications';
import { RejectOfferPriceValidationSchema } from 'validation/seller.validation';

// import { useRouter } from 'next/router';
// import { useRejectOfferPriceMutation } from 'services/api/offerPrice';
import Textarea from 'components/Input/textarea';
import { useRejectOfferPriceMutation } from 'services/api/seller';

const initialValues = {
  rejectRemarks: '',
};

const RejectOfferPricePopUp = (props) => {
  const { addToast } = useToasts();
  // const router = useRouter();
  // console.log('propsid', props.id[0]);

  const [
    rejectOfferPrice,
    { isSuccess, isError, isLoading: isLoadingRejectingOfferPrice, error },
  ] = useRejectOfferPriceMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast('you have rejected Offer Price  successfully', {
        appearance: 'success',
      });
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message || 'error occurred while rejecting offer price',
        {
          appearance: 'error',
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        rejectOfferPrice({ values, id: props.id[0] });
        setSubmitting(false);
      }}
      validationSchema={RejectOfferPriceValidationSchema}
      enableReinitialize
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">
            Please enter remarks for rejecting offer Price !
          </h1>
          {/* {console.log(props?.id)} */}

          <Form>
            <div className="w-full mb-2">
              {/* <Input
                label="Reasons"
                name="rejectRemarks"
                type="text"
                placeholder="We have better offer price "
              /> */}
              <Textarea
                label="Reject Remarks"
                required={true}
                name="rejectRemarks"
                type="text"
                placeholder="E.g. we have got best offer price?"
              />
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingRejectingOfferPrice}
                disabled={isLoadingRejectingOfferPrice || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              <Button
                disabled={isLoadingRejectingOfferPrice || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button>

              {/* <div className="mt-3"></div> */}
            </div>
            {/* {isError && (
          <div className="max-w-xs mt-3">
            <Alert
              message={error?.data?.message || 'Error logging in.'}
              type="error"
            />
          </div>
        )} */}
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default RejectOfferPricePopUp;
