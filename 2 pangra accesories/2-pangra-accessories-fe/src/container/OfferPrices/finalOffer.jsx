import { useEffect } from "react";
import { Form, Formik } from "formik";
// import Input from 'components/Input';
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import { FinalOfferPriceValidationSchema } from "validation/seller.validation";
import { useFinalOfferMutation } from "services/api/seller";
// import Input from 'components/Input';
import InputNumberFormat from "components/Input/InputNumberFormat";
import { removePrefixComma } from "utils/removePrefixComma";

// import { useRouter } from 'next/router';
// import { useFinalOfferMutation } from 'services/api/offerPrice';

const initialValues = {
  finalOffer: "",
};

const FinalOfferPricePopUp = (props) => {
  const { addToast } = useToasts();
  // const router = useRouter();

  const [
    createFinalOffer,
    { isSuccess, isError, isLoading: isLoadingFinalOfferPrice, error },
  ] = useFinalOfferMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast("you have successfully set yours final Offer Price ", {
        appearance: "success",
      });
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "error occurred while setting yours final offer price",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        // console.log(props.id);
        createFinalOffer({ values, id: props.id[0] });
        setSubmitting(false);
      }}
      validationSchema={FinalOfferPriceValidationSchema}
      enableReinitialize
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">Please enter your Final offer Price !</h1>

          <Form>
            <div className="w-full mb-2">
              {/* <Input
                label=" Final Offer(NRs.)"
                name="finalOffer"
                type="text"
                placeholder="E.g: NRs. 20000"
              /> */}
              <InputNumberFormat
                label=" Final Offer Price(NRs.)"
                name="finalOffer"
                value={values?.finalOffer || null}
                // displayType="text"
                thousandSeparator={true}
                prefix="NRs. "
                onChange={(e) => {
                  setFieldValue(
                    "finalOffer",
                    removePrefixComma("NRs. ", e.target.value)
                  );
                }}
              />
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingFinalOfferPrice}
                disabled={isLoadingFinalOfferPrice || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              {/* <Button
                disabled={isLoadingFinalOfferPrice || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button> */}

              {/* <div className="mt-3"></div> */}
            </div>
            {/* {isError && (
          <div className="max-w-xs mt-3">
            <Alert
              message={error?.data?.message || 'Error logging in.'}
              type="error"
            />
          </div>
        )} */}
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default FinalOfferPricePopUp;
