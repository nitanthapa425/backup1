import React, { useRef, useEffect } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import { useToasts } from "react-toast-notifications";

import { useCreateAddStockMutation } from "services/api/seller";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import * as yup from "yup";
import { nNumberArray } from "utils/nNumberOfArray";

const OutOfStock = ({ id, noOfQuantity = 5, setIsOpen }) => {
  const formikBag = useRef();
  const { addToast } = useToasts();

  const [createAddStock, { isError, error, isSuccess, isLoading, data }] =
    useCreateAddStockMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(data?.message || "You have successfully increase stock", {
        appearance: "success",
      });

      setIsOpen(false);
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to add stock . Please try again later.",
        {
          appearance: "error",
          autoDismiss: false,
        }
      );
    }
  }, [isSuccess, isError]);

  // useDeliveryCompanyQuery;

  //   console.log("deliveryPerson", );

  const stockValidation = yup.object({
    quantity: yup.string().required("Quantity is required"),
  });

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    createAddStock({ body: values, id: id });
    setSubmitting(false);
  };

  const optionStatus = () => {
    const option = nNumberArray(noOfQuantity)?.map((value) => {
      return {
        label: value,
        value: value,
      };
    });

    return option;
  };

  const stockInitialValue = {
    quantity: "",
  };

  return (
    <>
      <h3 className="h5 mb-2">Release Order </h3>
      <Formik
        initialValues={stockInitialValue}
        onSubmit={handleOnSubmit}
        validationSchema={stockValidation}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
          setFieldTouched,
        }) => (
          <Form>
            <div>
              <div className="w-full mb-5">
                <SelectWithoutCreate
                  required
                  label="Select Quantity"
                  placeholder="Select Quantity"
                  value={values?.quantity || null}
                  onChange={(selectedValue) => {
                    setFieldValue("quantity", selectedValue.value);
                  }}
                  options={optionStatus()}
                  //   isDisabled={
                  //     deliveryStatus === "cancelled" ||
                  //     deliveryStatus === "delivered"
                  //   }
                />
              </div>

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Add
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
      {/* </div> */}
    </>
  );
};

export default OutOfStock;
