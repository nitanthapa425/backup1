import React, { useEffect, useState } from "react";
import "@heroicons/react/outline";
import { useGetHotDealsQuery } from "services/api/seller";
import { getQueryStringForVehicle } from "utils/getQueryStringForTable";

import Button from "components/Button";

import ListingCard from "components/Card/ListingCard";
import LoadingCard from "components/LoadingCard";
import { useSelector } from "react-redux";

const HotDeals = ({ isSold, isVerified }) => {
  const [hotDealList, setHotDealsList] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [hotDealsDataQuery, setHotDealsDataQuery] = useState("");
  const loginInfo = useSelector((state) => state.customerAuth);
  const loginUser = loginInfo?.customer?._id;

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setHotDealsDataQuery(query);
  };

  useEffect(() => {
    getData({
      pageIndex: 0,
      pageSize: pageSize,
      sortBy: [],
    });
  }, [getData, pageSize]);

  const { data: hotDealsData, isLoading: isFetchingSeller } =
    useGetHotDealsQuery(hotDealsDataQuery);

  useEffect(() => {
    if (hotDealsData) {
      setHasNextPage(hotDealsData.hasNextPage);

      setHotDealsList(hotDealsData);
    }
  }, [hotDealsData]);

  return (
    <div>
      <main id="main">
        <section className="vehicle-listing-section pt-10 pb-20">
          <div className="container">
            <h1 className="h3 mb-4">Hot-Deals</h1>
            <div className="row">
              {hotDealList?.hotDeals?.length ? (
                hotDealList?.hotDeals?.map((product, i) => {
                  const userIds = product.wishList.map((data, i) => {
                    return data.userId;
                  });

                  const isInWishList = userIds.includes(loginUser);
                  return (
                    <ListingCard
                      key={i}
                      cardColsSize="three"
                      route={`/product-listing/${product?.id}`}
                      imgUrl={product?.productImage.imageUrl}
                      totalImages={product?.productImages.length - 1}
                      productTitle={product?.productTitle}
                      costPrice={product?.costPrice}
                      id={product.id}
                      isSold={product?.isSold}
                      isForFlashSale={product?.isForFlashSale}
                      hasWarrenty={product?.hasWarrenty}
                      isReturnable={product?.isReturnable}
                      color={product?.color?.[0]}
                      isInWishList={isInWishList}
                    />
                  );
                })
              ) : (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "100vw",
                  }}
                ></div>
              )}
            </div>

            {isFetchingSeller && (
              <div className="row">
                <LoadingCard cardColsSize="three" />
                <LoadingCard cardColsSize="three" />
              </div>
            )}

            <div className="w-full flex justify-center items-center">
              {hotDealList?.clientVehicleSellHotDeals?.length > 0 && (
                <div>
                  {isFetchingSeller && <div>...Loading</div>}

                  {hasNextPage && (
                    <Button
                      type="button"
                      disabled={!hasNextPage}
                      onClick={() => {
                        setPageSize((pageSize) => pageSize + 10);
                      }}
                      loading={isFetchingSeller}
                    >
                      Load More
                    </Button>
                  )}
                </div>
              )}
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default HotDeals;
