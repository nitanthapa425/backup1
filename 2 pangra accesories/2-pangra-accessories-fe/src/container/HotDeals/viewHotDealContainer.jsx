// import { DateColumnFilter } from 'components/Table/Filter';
import {
  DateColumnFilter,
  MultiSelectFilter,
  // DateColumnFilter,
  // MultiSelectFilter,
  SelectColumnFilter,
  // YearColumnFilter,
} from "components/Table/Filter";
import Table from "components/Table/table";
// import { color } from "constant/constant";
// import AdminLayout from 'layouts/Admin';
import { useState, useEffect, useMemo } from "react";
// import { useSelector } from "react-redux";
// import { useSelector } from 'react-redux';
import { useToasts } from "react-toast-notifications";
import {
  useReadAllHotDealsQuery,
  useRemoveHotDealMutation,
} from "services/api/hotdeals";

import { getQueryStringForHotDeals } from "utils/getQueryStringForTable";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";

// import { SelectColumnFilter } from 'components/Table/Filter';
import DOMPurify from "dompurify";
import { color } from "constant/constant";

function ViewHotDealsContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [hotDealDataQuery, setHotDealDataQuery] = useState("");
  // console.log(hotDealDataQuery);

  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();
  // const level = useSelector((state) => state.levelReducer.level);

  const {
    data: hotDealsData,
    isError: hotDealError,
    isFetching: isLoadingHotDeal,
  } = useReadAllHotDealsQuery(hotDealDataQuery);

  const {
    data: hotDealDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllHotDealsQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });
  const [
    removeHotDeals,
    {
      error: errorRemoveHotDeal,
      isLoading: isRemovingHotDeal,
      isSuccess: isSuccessRemoveHotDeal,
      data: dataRemoveHotDeals,
    },
  ] = useRemoveHotDealMutation();

  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };
  useEffect(() => {
    if (errorRemoveHotDeal) {
      showFailureToast(
        errorRemoveHotDeal?.data?.message || "Failed to remove from hot deals"
      );
    }
    if (isSuccessRemoveHotDeal) {
      showSuccessToast(
        dataRemoveHotDeals?.message ||
          "Product/s removed from hot deals successfully."
      );
    }
  }, [errorRemoveHotDeal, isSuccessRemoveHotDeal]);

  useEffect(() => {
    if (hotDealDataAll) {
      setTableDataAll(
        hotDealDataAll?.docs?.map((hotDeal) => {
          return {
            id: hotDeal?.id,
            productTitle: hotDeal?.productTitle,
            brandName: hotDeal?.brandName,
            categoryName: hotDeal?.categoryName,
            subCategoryName: hotDeal?.subCategoryName,
            status: hotDeal?.status,
            quantity: hotDeal?.quantity,
            SKU: hotDeal?.SKU,
            createdAt: hotDeal?.createdAt,
            costPrice: thousandNumberSeparator(hotDeal?.costPrice),
            discountedPrice: thousandNumberSeparator(hotDeal?.discountedPrice),
            color: hotDeal?.color[0],
            description: hotDeal?.description,
            hasInFeatured: hotDeal?.hasInFeatured ? "Featured" : "NotFeatured",
            isReturnable: hotDeal?.isReturnable
              ? "Returnable"
              : "NotReturnable",
          };
        })
      );
    }
  }, [hotDealDataAll]);

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "brandName",
        Header: "Brand Name",
        accessor: "brandName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "subCategoryName",
        Header: "Sub Category Name",
        accessor: "subCategoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "description",
        Header: "Description",
        accessor: "description",
        Cell: ({ cell: { value } }) =>
          (
            <div
              dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(value) }}
            ></div>
          ) || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "status",
        Header: "Status",
        accessor: "status",
        Cell: ({ cell: { value } }) => (value ? "Active" : "Inactive"),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: MultiSelectFilter,
        possibleFilters: [
          {
            label: "Active",
            value: "true",
          },
          {
            label: "InActive",
            value: "false",
          },
        ],
      },
      // {
      //   id: "color",
      //   Header: "Available Colors",
      //   accessor: "color",
      //   Cell: ({ cell: { value } }) => value || "-",
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: "color",
        Header: "Available Colors",
        accessor: "color",
        // Cell: ({ cell: { value } }) =>
        //   value.length ? (value.length > 0 ? value.join(" , ") : value) : "-",
        Cell: ({ cell: { value } }) => value || "-",
        Filter: SelectColumnFilter,

        possibleFilters: color.map((value) => {
          return {
            label: value,
            value: value,
          };
        }),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "Cost Price",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "discountedPrice",
        Header: "Discounted Price",
        accessor: "discountedPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "quantity",
        Header: "Available Quantity",
        accessor: "quantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "SKU",
        Header: "SKU",
        accessor: "SKU",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "createdAt",
        Header: "Created At",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "hasInFeatured",
        Header: "Has In Featured",
        accessor: "hasInFeatured",

        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Featured",
            value: true,
          },
          {
            label: "NotFeatured",
            value: false,
          },
        ],
      },
      {
        id: "isReturnable",
        Header: "Is Returnable",
        accessor: "isReturnable",

        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Returnable",
            value: true,
          },
          {
            label: "NotReturnable",
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForHotDeals(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setHotDealDataQuery(query);
  };

  useEffect(() => {
    if (hotDealsData) {
      setPageCount(hotDealsData?.totalPages);
      setTotalData(hotDealsData?.totalDocs);

      setTableData(
        hotDealsData?.docs?.map((hotDeal) => {
          return {
            id: hotDeal?.id,
            productTitle: hotDeal?.productTitle,
            brandName: hotDeal?.brandName,
            categoryName: hotDeal?.categoryName,
            subCategoryName: hotDeal?.subCategoryName,
            status: hotDeal?.status,
            quantity: hotDeal?.quantity,
            SKU: hotDeal?.SKU,
            createdAt: hotDeal?.createdAt,
            costPrice: thousandNumberSeparator(hotDeal?.costPrice),
            discountedPrice: thousandNumberSeparator(hotDeal?.discountedPrice),
            color: hotDeal?.color[0],
            description: hotDeal?.description,
            hasInFeatured: hotDeal?.hasInFeatured ? "Featured" : "NotFeatured",
            isReturnable: hotDeal?.isReturnable
              ? "Returnable"
              : "NotReturnable",
          };
        })
      );
    }
  }, [hotDealsData]);
  // const BreadCrumbList = [
  //   {
  //     routeName: 'Add Product',
  //     route: '/admin/product/create',
  //   },
  //   {
  //     routeName: 'Selling Vehicle List',
  //     route: '',
  //   },
  // ];

  return (
    // <AdminLayout documentTitle="View Seller" BreadCrumbList={BreadCrumbList}>
    <section className="mt-3">
      <div className="container">
        <Table
          tableName="HotDeal/s"
          tableDataAll={tableDataAll}
          setSkipTableDataAll={setSkipTableDataAll}
          isLoadingAll={isLoadingAll}
          columns={columns}
          data={tableData}
          fetchData={getData}
          isFetchError={hotDealError}
          isLoadingData={isLoadingHotDeal}
          pageCount={pageCount}
          defaultPageSize={10}
          totalData={totalData}
          hasExport={true}
          rowOptions={[...new Set([10, 20, 30, totalData])]}
          // editRoute={
          //   level === "superAdmin"
          //     ? "seller/edit"
          //     : level === "customer"
          //     ? "sell/edit"
          //     : ""
          // }
          // viewRoute={
          //   level === "superAdmin"
          //     ? "seller/get"
          //     : level === "customer"
          //     ? "sell/get"
          //     : ""
          // }
          hasDropdownBtn={true}
          dropdownBtnName="Remove"
          dropdownBtnOptions={[
            {
              name: " From Hot Deals",
              onClick: removeHotDeals,
              isLoading: isRemovingHotDeal,
              loadingText: "Removing...",
              // conditionalShow: true,
              // showFor: 'hasInHomePage',
              // dontShowWhen: false,
            },
          ]}
        />
      </div>
    </section>
    // </AdminLayout>
  );
}

export default ViewHotDealsContainer;
