import React from 'react';
import AdminLayout from 'layouts/Admin';
import { useReadSubModelQuery } from 'services/api/varient';

const GetSubModelDetails = () => {
  const { data: subModelData } = useReadSubModelQuery();

  return (
    <AdminLayout documentTitle="Get Sub Model">
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Brand Sub-Model</h1>
          <div className="row ">
            {subModelData?.docs?.map?.((subModel, i) => {
              return (
                <div key={i} className="three-col">
                  <div className="border border-gray-200 pt-3 pb-2 px-4 rounded-md">
                    <p className="ellipsis">
                      Variant Name :&nbsp; {subModel?.brandSubModelName}
                    </p>

                    {/* <p className="ellipsis">
                      Variant Description :&nbsp;
                      {subModel?.brandSubModelDescription}
                    </p> */}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </AdminLayout>
  );
};

export default GetSubModelDetails;
