import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Link from "next/link";

import Button from "components/Button";
import Input from "components/Input";
import AdminLayout from "layouts/Admin";
import PropTypes from "prop-types";

import {
  useCreateSubModelMutation,
  useReadVariantDetailsQuery,
  useUpdateVariantMutation,
} from "services/api/varient";
import { subCategoryValidationSchema } from "validation/subCategory.validation";
import { useReadModelQuery } from "services/api/model";
import { useToasts } from "react-toast-notifications";
import Popup from "components/Popup";
import { useRouter } from "next/router";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
// import Textarea from 'components/Input/textarea';

const VariantContainer = ({ type }) => {
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const [editVariantInitialValue, setEditVariantInitialValue] = useState({});

  const { addToast, removeAllToasts } = useToasts();
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const { data: modelData, isLoading: fethingModel } = useReadModelQuery();
  const [ModelData, setModelData] = useState([]);
  useEffect(() => {
    if (modelData) {
      setModelData(modelData);
    }
  }, [modelData]);
  const formikSubModelBag = useRef();
  const [createSubModel, { isError, isLoading, isSuccess, data }] =
    useCreateSubModelMutation();

  useEffect(() => {
    if (isSuccess) {
      formikSubModelBag.current?.resetForm();
      addToast(
        <>
          New Variant added successfully.
          <Link href={`/vehicle/submodel/get/${data?.id}`}>
            <a className="text-md font-semibold underline text-blue-700 mr-2">
              View Variant
            </a>
          </Link>
        </>,
        {
          appearance: "success",
          autoDismiss: false,
        }
      );
      setChanged(false);
    }
  }, [isSuccess]);

  useEffect(() => {
    if (isError) {
      addToast("Variant Name already exist.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const [
    updateVariant,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useUpdateVariantMutation();

  const { data: variantDetails, error: variantFetchError } =
    useReadVariantDetailsQuery(router.query.id, {
      skip: type !== "edit" || !router.query.id,
    });

  useEffect(() => {
    if (variantFetchError) {
      addToast(
        "Error occurred while fetching Variant details. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [variantFetchError]);
  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      createSubModel(values, resetForm);
    }
    if (type === "edit") {
      updateVariant({ ...values, id: router.query.id });
    }
    setSubmitting(false);
  };
  useEffect(() => {
    if (isUpdateError) {
      addToast("Variant Name already exist.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikSubModelBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || "Variant details updated successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);

      router.push(`/vehicle/submodel/get/${router.query.id}`);
    }
  }, [isUpdateSuccess]);
  const addModelInitialValue = {
    varientName: "",
    // varientDescription: '',
    modelId: router?.query?.model || "",
  };
  useEffect(() => {
    if (variantDetails) {
      const newVariantDetails = {
        varientName: variantDetails?.varientName,
        // varientDescription: variantDetails?.varientDescription,
        modelId: variantDetails?.modelId?.id,
      };

      setEditVariantInitialValue(newVariantDetails);
    }
  }, [variantDetails]);

  let BreadCrumbList = [];

  if (type === "add") {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Add Variant",
        route: "/vehicle/submodel/add",
      },
    ];
  } else {
    BreadCrumbList = [
      {
        routeName: "Add Product",
        route: "/admin/product/create",
      },
      {
        routeName: "Variant List",
        route: "/vehicle/submodel/view",
      },
      {
        routeName: "Edit Variant",
        route: "",
      },
    ];
  }

  useEffect(() => {
    return removeAllToasts;
  }, []);

  return (
    <AdminLayout
      documentTitle={
        type === "add" ? "Add New Variant" : "Edit Variant Details"
      }
      BreadCrumbList={BreadCrumbList}
    >
      <div className="container">
        <Formik
          initialValues={
            type === "edit" ? editVariantInitialValue : addModelInitialValue
          }
          onSubmit={onSubmit}
          validationSchema={subCategoryValidationSchema}
          enableReinitialize
          innerRef={formikSubModelBag}
        >
          {({
            setFieldValue,
            values,
            errors,
            touched,
            resetForm,
            isSubmitting,
            dirty,
          }) => (
            <Form onChange={() => setChanged(true)}>
              {/* <button
                className="mb-3 hover:text-primary"
                onClick={() => router.back()}
              >
                Go Back
              </button> */}

              <div className="flex justify-center pt-3 pb-10">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-xl mx-auto">
                  <h3 className="mb-3">
                    {type === "add"
                      ? "Add New Variant"
                      : "Edit Variant Details"}
                  </h3>
                  {/* <Select label="Select Model" name="modelId">
                    <option value="">Select Model</option>
                    {ModelData.map((model, i) => (
                      <option key={model._id} value={model._id}>
                        {model.modelName}
                      </option>
                    ))}
                  </Select> */}
                  <div className="mb-2">
                    <SelectWithoutCreate
                      required
                      loading={fethingModel}
                      label="Select Model"
                      placeholder="E.g: Pulsar"
                      error={touched?.modelId ? errors?.modelId : ""}
                      value={values.modelId}
                      onChange={(selectedModel) => {
                        setFieldValue("modelId", selectedModel.value);
                      }}
                      options={ModelData?.docs?.map((model) => ({
                        value: model.id,
                        label: model.modelName,
                      }))}
                    />
                  </div>

                  <div className="mb-2">
                    <Input
                      label="Variant Name"
                      name="varientName"
                      type="text"
                      placeholder="E.g: NS 200"
                    />
                  </div>

                  {/* <div className="mb-2">
                    <Textarea
                      label="Variant Description"
                      name="varientDescription"
                      type="text"
                      placeholder="E.g: Variant Description"
                      required={false}
                    />
                  </div> */}

                  <div className="btn-holder mt-3">
                    <Button
                      type="submit"
                      loading={isLoading || updating}
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      {type === "add" ? "Submit" : "Update"}
                    </Button>
                    <Button
                      onClick={() => {
                        setOpenModal(true);
                      }}
                      variant="outlined-error"
                      type="button"
                      disabled={isSubmitting || !dirty || isLoading || updating}
                    >
                      Clear
                    </Button>

                    {openModal && (
                      <Popup
                        title="Are you sure to clear all fields?"
                        description="If you clear all fields, the data will not be saved."
                        onOkClick={() => {
                          resetForm();
                          setChanged(false);
                          setOpenModal(false);
                        }}
                        onCancelClick={() => setOpenModal(false)}
                        okText="Clear All"
                        cancelText="Cancel"
                      />
                    )}
                  </div>
                </div>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </AdminLayout>
  );
};

VariantContainer.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default VariantContainer;
