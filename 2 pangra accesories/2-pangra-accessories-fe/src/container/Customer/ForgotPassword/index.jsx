import React, { useState, useCallback } from "react";
// import { Form, Formik } from "formik";

// import Button from "components/Button";

// import Input from "components/Input";
// import { useToasts } from "react-toast-notifications";

// import Link from "next/link";
// import { customerForgotValidationSchema } from "validation/customer.validation";
// import { useCustomerForgotMutation } from "services/api/customer";
import Steps from "rc-steps/lib/Steps";
import { Step } from "rc-steps";
import { ForgotPasswordSteps } from "./data";
const CustomerForgotPasswordContainer = ({ setModalFunction, modal }) => {
  const [currentStep, setCurrentStep] = useState(0);
  const handleBackClick = useCallback(
    () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null),
    [currentStep]
  );

  return (
    <>
      <section className="horizontal-steps pb-0 ">
        <Steps current={currentStep}>
          {ForgotPasswordSteps.map(
            ({ id, title, component: StepContent, key }, index) => (
              <Step
                key={id + key}
                title={
                  <span
                    className="cursor-pointer"
                    onClick={() => {
                      if (id < currentStep) {
                        setCurrentStep(id);
                      }
                    }}
                  >
                    {title}
                  </span>
                }
                description={
                  <>
                    {currentStep === index && (
                      <>
                        <StepContent
                          setCurrentStep={setCurrentStep}
                          currentStep={currentStep}
                          handleBackClick={handleBackClick}
                          setModalFunction={setModalFunction}
                          modal={modal}
                        />
                      </>
                    )}
                  </>
                }
              />
            )
          )}
        </Steps>
      </section>
    </>
  );
};

export default CustomerForgotPasswordContainer;
