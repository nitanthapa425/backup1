import Button from "components/Button";
import Input from "components/Input";
import { Form, Formik } from "formik";
// import Link from "next/link";
import React, { useEffect } from "react";
import { useToasts } from "react-toast-notifications";
import { useCustomerForgotMutation } from "services/api/customer";
import { customerForgotValidationSchema } from "validation/customer.validation";

const Mobile = ({ setCurrentStep, currentStep, handleBackClick }) => {
  const { addToast } = useToasts();
  const [
    createCustomerForgotPassword,
    {
      isError,
      error,
      isLoading,
      isSuccess,
      data: dataCreateCustomerForgotPassword,
    },
  ] = useCustomerForgotMutation();

  // const [createCustomerSparrow] = useCustomerSignUpSparrowMutation();

  useEffect(() => {
    if (isSuccess) {
      // formikBag.current?.resetForm();

      addToast(
        dataCreateCustomerForgotPassword?.message ||
          "The OTP Code has been sent to your Mobile Number.",
        {
          appearance: "success",
        }
      );
      // createCustomerSparrow(formDataa);
      setCurrentStep((currentStep) => {
        return currentStep + 1;
      });
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "you might have entered wrong Phone Number . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <Formik
      initialValues={{
        mobile: localStorage.getItem("mobile") || "",
      }}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        localStorage.setItem("mobile", values.mobile);
        createCustomerForgotPassword(values);

        setSubmitting(false);
      }}
      validationSchema={customerForgotValidationSchema}
      enableReinitialize
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <Form>
          <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
            <h3 className="mb-3">Forgot Password</h3>
            <p className="text-lg">
              Enter your Mobile Number and we will send OTP Code to reset your
              password
            </p>
            <Input
              label="Mobile Number"
              name="mobile"
              type="text"
              placeholder="Mobile Number"
            />

            <div className="btn-holder mt-3">
              <Button type="submit" disabled={isSubmitting} loading={isLoading}>
                Send
              </Button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default Mobile;
