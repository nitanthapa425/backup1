import Button from "components/Button";
import Input from "components/Input";
import PasswordInput from "components/Input/PasswordInput";
import { Form, Formik } from "formik";
import useCustomTimer from "hooks/useTimer";
import React, { useEffect, useState } from "react";
import { useToasts } from "react-toast-notifications";
import {
  useCustomerForgotMutation,
  useSendPasswordOtpMutation,
  // useVerifyPhoneMutation,
} from "services/api/customer";
import { customerOtpValidationSchema } from "validation/customer.validation";

const Otp = ({ setCurrentStep, currentStep, handleBackClick }) => {
  const [timer, setStartTimer] = useCustomTimer(59);

  const [showOtp, setShowOtp] = useState(true);
  const [
    resendToken,
    {
      isLoading: isLoadingResendToken,
      isSuccess: isSuccessResendToken,
      isError: isErrorResendToken,
      error: errorResendToken,
      data: dataResendToken,
    },
  ] = useCustomerForgotMutation();

  useEffect(() => {
    setStartTimer(true);
  }, []);

  useEffect(() => {
    if (isSuccessResendToken) {
      addToast(
        dataResendToken?.message ||
          "Congratulation OTP has been resent successfully.",
        {
          appearance: "success",
        }
      );

      // setTimeout(() => {setDisableResendButton(true)}), 5000);

      setStartTimer(true);

      // createCustomerSparrow(formDataa);
    }

    if (isErrorResendToken) {
      addToast(
        errorResendToken?.data?.message || "Sorry we could not send OTP",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccessResendToken, isErrorResendToken]);

  const ReSend = () => {
    // const formData = new FormData();

    // const randomNumber = Math.floor(10000 + Math.random() * 90000);
    // formData.append("token", sparrowToken);
    // formData.append("from", "TUKIALERT");
    // formData.append("to", localStorage.getItem("mobile"));
    // formData.append(
    //   "text",
    //   ` ${randomNumber} is your account verification code. \n Thank you !`
    // );
    // setFormDataa(formData);
    resendToken({
      mobile: localStorage.getItem("mobile"),
    });
  };
  const { addToast } = useToasts();
  const [
    sendPassword,
    {
      isLoading: isLoadingVerifyPhone,
      isSuccess: isSuccessVerifyPhone,
      isError: isErrorVerifyPhone,
      error: errorVerifyPhone,
      data: dataSendPassword,
    },
  ] = useSendPasswordOtpMutation();

  useEffect(() => {
    if (isSuccessVerifyPhone) {
      addToast(
        dataSendPassword?.message ||
          "Congratulation Password is reset successfully",
        {
          appearance: "success",
        }
      );

      setCurrentStep((currentStep) => {
        return currentStep + 1;
      });
      localStorage.removeItem("mobile");
    }

    if (isErrorVerifyPhone) {
      addToast(errorVerifyPhone?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessVerifyPhone, isErrorVerifyPhone]);
  return (
    <div>
      <Formik
        initialValues={{
          verificationToken: "",
          passwordHash: "",
          confirmPassword: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          const payloadData = {
            mobile: localStorage.getItem("mobile"),
            verificationToken: values.verificationToken,
            newPassword: values.passwordHash,
          };

          sendPassword(payloadData);
          setSubmitting(false);
        }}
        validationSchema={customerOtpValidationSchema}
        enableReinitialize
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            {showOtp ? (
              <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto ">
                <div className="flex item-center">
                  <h1 className="mb-1 h4 flex-1 ">Verify your OTP code.</h1>
                  {timer ? (
                    <div className="flex items-center ">
                      <div className="p-2">Resend Otp in </div>
                      <div className="w-10 h-10 border-2 border-red-600 rounded-full flex  items-center justify-center">
                        <h2 className="h5 text-error">{timer}</h2>
                      </div>
                    </div>
                  ) : (
                    ""
                  )}
                </div>
                <span className="mb-3 block ">
                  Please enter the 5 digit code sent to your mobile phone
                </span>
                <div className="row-sm mb-3">
                  <Input
                    label="Verification Code"
                    name="verificationToken"
                    type="text"
                    placeholder=""
                  />
                </div>

                <div className="btn-holder mt-3">
                  <Button
                    type="button"
                    disabled={!values.verificationToken}
                    onClick={() => {
                      setShowOtp(false);
                    }}
                  >
                    Verify
                  </Button>
                  <Button
                    variant="secondary"
                    onClick={() => {
                      ReSend();
                    }}
                    loading={isLoadingResendToken}
                    disabled={isLoadingResendToken || timer}
                  >
                    Resend
                  </Button>
                  <Button
                    type="button"
                    //   disabled={isSubmitting || !dirty || isLoading || isLoading}
                    onClick={() => {
                      handleBackClick();
                    }}
                  >
                    Back
                  </Button>
                </div>
              </div>
            ) : (
              <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto ">
                <div className="row-sm">
                  <PasswordInput
                    label="New Password"
                    name="passwordHash"
                    placeholder="New Password"
                  ></PasswordInput>
                  <PasswordInput
                    label="Confirm Password"
                    name="confirmPassword"
                    placeholder="Confirm Password"
                  ></PasswordInput>
                </div>

                <div className="btn-holder mt-3">
                  <Button
                    type="submit"
                    disabled={isSubmitting || !dirty}
                    loading={isLoadingVerifyPhone}
                  >
                    Send
                  </Button>
                  <Button
                    type="button"
                    //   disabled={isSubmitting || !dirty || isLoading || isLoading}
                    onClick={() => {
                      setShowOtp(true);
                    }}
                  >
                    Back
                  </Button>
                </div>
              </div>
            )}
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Otp;
