import Button from "components/Button";
import React from "react";

const SuccessMessage = ({ setModalFunction, modal }) => {
  return (
    <div>
      Congratulations! Your password has been reset successfully. Please Login
      to continue
      <div className="btn-holder mt-3">
        <Button
          type="button"
          onClick={() => {
            if (modal === true) {
              setModalFunction(false);
            }
            if (modal === false) {
              setModalFunction(true);
            }
          }}
        >
          Login
        </Button>
      </div>
    </div>
  );
};

export default SuccessMessage;
