import Mobile from "./steps/Mobile";
import Otp from "./steps/OtpPassword";
// import Password from "./steps/Password";
import SuccessMessage from "./steps/SuccessMessage";

export const ForgotPasswordSteps = [
  {
    id: 0,
    key: "MOBILE",
    title: "",
    component: Mobile,
  },
  {
    id: 1,
    key: "OTP",
    title: "",
    component: Otp,
  },

  {
    id: 2,
    key: "SUCCESS_MESSAGE",
    title: "",
    component: SuccessMessage,
  },
];
