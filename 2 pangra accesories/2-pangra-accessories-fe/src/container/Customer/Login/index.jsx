import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Form, Formik } from "formik";

import Input from "components/Input";
import Button from "components/Button";
// import Alert from 'components/Alert';

import PasswordInput from "components/Input/PasswordInput";
import { useToasts } from "react-toast-notifications";
import { customerLoginValidationSchema } from "validation/customer.validation";

import { useLoginCustomerMutation } from "services/api/customerLogin";
import ReactModal from "components/ReactModal/ReactModal";
import CustomerForgotPasswordContainer from "container/Customer/ForgotPassword";

const initialValues = {
  mobile: "",
  password: "",
};

const CustomerLoginForm = () => {
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };
  const router = useRouter();
  const { addToast } = useToasts();
  const [
    loginCustomer,
    { isSuccess, isError, isLoading, error, data: dataLoginCustomer },
  ] = useLoginCustomerMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast(dataLoginCustomer?.message || "Login success.", {
        appearance: "success",
      });
      router.replace("/");
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(error?.data?.message || "Mobile Number or password incorrect", {
        appearance: "error",
      });
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={loginCustomer}
      validationSchema={customerLoginValidationSchema}
      enableReinitialize
    >
      <Form>
        <div className="w-full mb-2">
          <Input
            label="Mobile Number"
            name="mobile"
            type="text"
            placeholder="E.g: 98XXXXXXXX"
          />
        </div>

        <PasswordInput
          label="Password"
          name="password"
          placeholder="Password"
        ></PasswordInput>
        <div className="mt-3">
          <Button loading={isLoading} type="submit" className="btn-lg">
            Login
          </Button>

          <Button disabled={isLoading} type="reset" variant="outlined-error">
            Clear
          </Button>
          <div className="mt-3">
            <ReactModal
              link="Forgot Password?"
              className="comment-edit p-0"
              modal={modal}
            >
              <CustomerForgotPasswordContainer
                setModalFunction={setModalFunction}
                modal={modal}
              ></CustomerForgotPasswordContainer>
            </ReactModal>

            <Link href="/customer/add">
              <a className="text-textColor ml-2">Sign Up</a>
            </Link>
          </div>
        </div>
      </Form>
    </Formik>
  );
};
export default CustomerLoginForm;
