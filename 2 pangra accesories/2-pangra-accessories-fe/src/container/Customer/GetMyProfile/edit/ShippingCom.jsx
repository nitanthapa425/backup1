import Button from "components/Button";
import Input from "components/Input";
import LoadingCustom from "components/LoadingCustom/LoadingCustom";
import Popup from "components/Popup";
import Select from "components/Select";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { addressLabel } from "constant/constant";
import InquiryForm from "container/InquiryForm/inqueryForm";
import { Form, Formik } from "formik";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import React, { useEffect, useRef, useState } from "react";
import { useToasts } from "react-toast-notifications";
import {
  useAddShippingAddressMutation,
  useReadSingleShippingAddressQuery,
  useUpdateShippingAddressMutation,
} from "services/api/customer";
import {
  useReadAllDistrictQuery,
  useReadMunicipalityVdcByDistrictQuery,
} from "services/api/location";
import { nNumberArray } from "utils/nNumberOfArray";
import { shippingAddressValidation } from "validation/customer.validation";
import Modal from "react-modal";

const ShippingCom = ({
  setIsOpen = () => {},
  type = "add",
  id,
  refetch = () => {},
  setShowShippingAddressTable = () => {},
}) => {
  const [openModal, setOpenModal] = useState(false);
  const [changed, setChanged] = useState(false);
  const [editAddress, setEditAddress] = useState({});

  const [modelOpenInquiry, setModelOpenInquiry] = useState(false);
  const closeModelOpenInquiry = () => {
    setModelOpenInquiry(false);
  };
  useWarnIfUnsavedChanges(changed);
  const { addToast } = useToasts();
  // const { data: combineLocations, isFetching: isFetchingCombineLocation } =
  //   useGetDetailLocationQuery();

  const { data: district, isFetching: isFetchingDistrict } =
    useReadAllDistrictQuery();

  const [districtValue, setDistrictValue] = useState(null);

  const {
    data: shippingAddress,
    isSuccess: shippingAddressIsSuccess,
    isLoading: shippingAddressLoading,
  } = useReadSingleShippingAddressQuery(id, {
    skip: !id,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    const initial = {
      // combineLocation: shippingAddress?.combineLocation,

      nearByLocation: shippingAddress?.nearByLocation,
      exactLocation: shippingAddress?.exactLocation,
      label: shippingAddress?.label,
      fullName: shippingAddress?.fullName,
      mobile: shippingAddress?.mobile,
      districtName: shippingAddress?.districtName,
      municipalityId: shippingAddress?.municipalityId,
      wardNumber: shippingAddress?.wardNumber,
      toleName: shippingAddress?.toleName,
    };
    setEditAddress(initial);
    setDistrictValue(shippingAddress?.districtName);
  }, [shippingAddress, shippingAddressIsSuccess]);

  const formikBag = useRef();
  const [
    addShippingAddress,
    {
      isError: isErrorAdd,
      isSuccess: isSuccessAdd,
      error: errorAdd,
      isLoading: isLoadingAdd,
      data: dataAdd,
    },
  ] = useAddShippingAddressMutation();
  useEffect(() => {
    if (isSuccessAdd) {
      setShowShippingAddressTable(true);
      formikBag.current?.resetForm();

      addToast(dataAdd.message || "Shipping address added successfully.", {
        appearance: "success",
      });
      setIsOpen(false);
      refetch();

      // console.log(refecthList);

      setChanged(false);
      //   router.push(`/customer-profile/get`);
    }
    if (isErrorAdd) {
      addToast(errorAdd?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAdd, isErrorAdd]);

  const [
    updateAddress,
    { isError, isSuccess, error, isLoading, data: dataUpdateAddress },
  ] = useUpdateShippingAddressMutation();

  const {
    data: municipalityVdc,
    isFetching: isFetchingMunicipalityVdcByDistrict,
  } = useReadMunicipalityVdcByDistrictQuery(districtValue, {
    skip: !districtValue,
    // keepUnusedDataFor: 0,
  });

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataUpdateAddress?.message ||
          "Shipping Address has been updated successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);
      setIsOpen(false);
      refetch();
      setShowShippingAddressTable(true);
    }
    if (isError) {
      addToast(error?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccess, isError]);

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") {
      addShippingAddress(values);
    } else {
      updateAddress({ body: values, id: id });
    }
    // updateCustomer(values);
    setSubmitting(false);
  };

  const initial = {
    // combineLocation: "",
    nearByLocation: "",
    exactLocation: "",
    label: "",
    fullName: "",
    mobile: "",
    districtName: "",
    municipalityId: "",
    wardNumber: "",
    toleName: "",
  };
  const shippintInitialValues = () => {
    if (type === "add") {
      return initial;
    } else return editAddress;
  };

  return (
    <div
      className={`relative ${
        shippingAddressLoading
          ? `bg-gray-100 before:absolute before:top-[-28px] before:bottom-[-28px] before:left-[-20px] before:right-[-20px] before:bg-gray-200 before:z-10 before:bg-opacity-80`
          : ""
      } `}
    >
      <Modal
        isOpen={modelOpenInquiry}
        className="mymodal order"
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={closeModelOpenInquiry}
          className="text-error absolute top-[5px] right-[5px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <InquiryForm
          value="Location Not Found"
          closeModelOpenInquiry={closeModelOpenInquiry}
        />
      </Modal>
      <LoadingCustom show={[shippingAddressLoading]}></LoadingCustom>
      <Formik
        initialValues={shippintInitialValues()}
        onSubmit={handleOnSubmit}
        validationSchema={shippingAddressValidation}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div>
            <div className="flex flex-wrap justify-between">
              {" "}
              <h3 className="mb-3 h5">Shipping Address</h3>
              <div
                className=" text-blue-600 hover:text-primary-light hover:cursor-pointer  lg:ml-60"
                onClick={() => {
                  setModelOpenInquiry(true);
                }}
              >
                Didn’t find my location?
              </div>
            </div>

            <Form onChange={() => setChanged(true)}>
              <div className="row-sm">
                {/* <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={isFetchingCombineLocation}
                    label="Select Location"
                    placeholder="Location"
                    error={
                      touched?.combineLocation ? errors?.combineLocation : ""
                    }
                    value={values.combineLocation || null}
                    onChange={(selectedValue) => {
                      setFieldValue(`combineLocation`, selectedValue.value);
                    }}
                    options={combineLocations?.map((value) => ({
                      label: value.fullAddress,
                      // value: `${value.id}`,
                      value: value.fullAddress,
                    }))}
                    onBlur={() => {
                      setFieldTouched(`combineLocation`);
                    }}
                  />
                </div> */}

                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    loading={isFetchingDistrict}
                    label="Select District"
                    placeholder="District"
                    error={touched?.districtName ? errors?.districtName : ""}
                    value={values?.districtName || null}
                    onChange={(selectedValue) => {
                      setFieldValue("districtName", selectedValue.value);
                      setDistrictValue(selectedValue.value);
                      setFieldValue("municipalityId", "");
                      setFieldTouched("municipalityId", false);
                    }}
                    options={district?.map((value) => ({
                      label: value.district,
                      value: value.district,
                    }))}
                    onBlur={() => {
                      setFieldTouched("districtName");
                    }}
                  />
                </div>

                <div className="three-col-sm">
                  <SelectWithoutCreate
                    required
                    isDisabled={!values?.districtName}
                    loading={isFetchingMunicipalityVdcByDistrict}
                    label="Select Municipality/VDC"
                    placeholder="Select Municipality/VDC"
                    error={
                      touched?.municipalityId ? errors?.municipalityId : ""
                    }
                    value={values?.municipalityId || ""}
                    onChange={(selectedValue, actionMeta) => {
                      setFieldTouched("municipalityId");
                      setFieldValue("municipalityId", selectedValue.value);
                    }}
                    options={municipalityVdc?.map((value) => ({
                      label: value.municipalityName,
                      value: value.id,
                    }))}
                    onBlur={() => {
                      setFieldTouched("municipalityId");
                    }}
                  />
                </div>

                <div className="three-col-sm">
                  <Select label="Ward Number" name="wardNumber" required={true}>
                    <option value="">Select Ward Number</option>

                    {nNumberArray(40).map((value, i) => (
                      <option key={i} value={value}>
                        {value}
                      </option>
                    ))}
                  </Select>
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name="toleName"
                    label="Tole/Marg"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`nearByLocation`}
                    label="Nearby Location"
                    type="text"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`exactLocation`}
                    label="Exact location"
                    type="text"
                  />
                </div>

                <div className="three-col-sm">
                  <Select label="Label" name="label" required={true}>
                    <option value="" disabled="disabled">
                      Select Label
                    </option>

                    {addressLabel.map((label, i) => (
                      <option key={i} value={label}>
                        {label}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`fullName`}
                    label="Full Name"
                    type="text"
                  />
                </div>

                <div className="three-col-sm">
                  <Input
                    required={true}
                    name={`mobile`}
                    label="Mobile Number"
                    type="text"
                  />
                </div>
              </div>

              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  disabled={!dirty || isSubmitting}
                  loading={isLoadingAdd || isLoading}
                >
                  {type === "add" ? "Add" : "Edit"}
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </div>
  );
};

export default ShippingCom;
