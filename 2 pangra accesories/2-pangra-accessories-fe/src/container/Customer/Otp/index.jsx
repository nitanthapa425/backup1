import React, { useRef, useEffect } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import Input from "components/Input";
import { useToasts } from "react-toast-notifications";

import Link from "next/link";
import { customerOtpValidationSchema } from "validation/customer.validation";
import { useVerifyPhoneMutation } from "services/api/customer";
import { useRouter } from "next/router";
const OTPCode = () => {
  const formikBag = useRef();
  const router = useRouter();
  const { addToast } = useToasts();
  const [createCustomerOtp, { isError, error, isLoading, isSuccess }] =
    useVerifyPhoneMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast("OTP Code is verified you can Rest-Password now", {
        appearance: "success",
      });
      router.push("/customer/reset-password");
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "you might have entered wrong OTP Code . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);
  return (
    <>
      <Formik
        initialValues={{
          otp: "",
        }}
        onSubmit={(values, { resetForm }) => {
          createCustomerOtp(values);
        }}
        validationSchema={customerOtpValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
                  <h3 className="mb-3">Enter OTP Code</h3>

                  <Input
                    label="OTP Code"
                    name="otp"
                    type="text"
                    placeholder="Enter 6 digit code"
                  />

                  <div className="btn-holder mt-3">
                    <Button type="button">Resend</Button>
                    <Button
                      type="submit"
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Send
                    </Button>
                    <Button
                      type="button"
                      variant="outlined"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={
                        isSubmitting || !dirty || isLoading || isLoading
                      }
                    >
                      Clear
                    </Button>

                    <Link href="/admin">
                      <Button type="button">Back</Button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default OTPCode;
