import { useRouter } from "next/router";
import React, { useRef, useEffect, useState } from "react";

import { useToasts } from "react-toast-notifications";
import Button from "components/Button";
import { Form, Formik } from "formik";

import { PhoneVerificationValidationSchema } from "validation/customer.validation";
import Input from "components/Input";
import {
  useVerifyPhoneMutation,
  useResendOtpMutation,
  useCustomerSignUpSparrowMutation,
} from "services/api/customer";
// import { useDispatch, useSelector } from "react-redux";
// import { customerActions } from "store/features/customerAuth/customerAuthSlice";
import { sparrowToken } from "config";
import useCustomTimer from "hooks/useTimer";

const VerifyPhone = () => {
  const [formDataa, setFormDataa] = useState(null);

  const [timer, setStartTimer] = useCustomTimer(59);

  // const [disableResendButton, setDisableResendButton] = useState(false);
  const { addToast } = useToasts();
  // const customerInfo = useSelector((state) => state.customerAuth);
  // console.log(customerInfo);
  const formikBag = useRef();
  const router = useRouter();
  // const dispatch = useDispatch();

  useEffect(() => {
    setStartTimer(true);
  }, []);

  const [
    verifyPhone,
    {
      // isLoading: isLoadingVerifyPhone,
      isSuccess: isSuccessVerifyPhone,
      isError: isErrorVerifyPhone,
      error: errorVerifyPhone,
      data: dataVerifyPhone,
    },
  ] = useVerifyPhoneMutation();
  const [
    resendToken,
    {
      isLoading: isLoadingResendToken,
      isSuccess: isSuccessResendToken,
      isError: isErrorResendToken,
      error: errorResendToken,
      data: dataResendToken,
    },
  ] = useResendOtpMutation();

  // const mobile = localStorage.getItem("mobile");

  const [createCustomerSparrow] = useCustomerSignUpSparrowMutation();

  const ReSend = () => {
    const formData = new FormData();

    const randomNumber = Math.floor(10000 + Math.random() * 90000);
    formData.append("token", sparrowToken);
    formData.append("from", "TUKIALERT");
    formData.append("to", localStorage.getItem("mobile"));
    formData.append(
      "text",
      `Dear ${localStorage.getItem("firstName")} ${localStorage.getItem(
        "middleName"
      )} ${localStorage.getItem(
        "lastName"
      )}, \n ${randomNumber} is your account verification code. \n Thank you !`
    );
    setFormDataa(formData);

    resendToken({
      mobile: localStorage.getItem("mobile"),
      verificationToken: `${randomNumber}`,
    });
  };

  useEffect(() => {
    if (isSuccessResendToken) {
      addToast(
        dataResendToken?.message ||
          "Congratulation OTP has been resent successfully.",
        {
          appearance: "success",
        }
      );
      // setTimeout(() => {setDisableResendButton(true)}), 5000);
      setStartTimer(true);

      createCustomerSparrow(formDataa);
    }

    if (isErrorResendToken) {
      addToast(
        errorResendToken?.data?.message || "Sorry we could not send OTP",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccessResendToken, isErrorResendToken]);

  useEffect(() => {
    if (isSuccessVerifyPhone) {
      addToast(
        dataVerifyPhone?.message ||
          "Congratulation your mobile has been verified successfully.",
        {
          appearance: "success",
        }
      );

      localStorage.removeItem("mobile");
      localStorage.removeItem("firstName");
      localStorage.removeItem("lastName");
      router.push("/customer/login");
    }

    if (isErrorVerifyPhone) {
      addToast(errorVerifyPhone?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessVerifyPhone, isErrorVerifyPhone]);

  return (
    <>
      <Formik
        initialValues={{
          token: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          const payloadData = {
            // id: localStorage.getItem('otpId'),
            mobile: localStorage.getItem("mobile"),

            verificationToken: values.token,
          };

          // console.log(payloadData);
          setSubmitting(false);

          verifyPhone(payloadData);
        }}
        validationSchema={PhoneVerificationValidationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="container">
              <div className="flex justify-center items-center min-h-screen">
                <div
                  className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto "
                  style={{ position: "relative" }}
                >
                  {/* {timer} */}

                  <div className="flex item-center">
                    <h1 className="mb-1 h4 flex-1 ">Verify your OTP Number.</h1>
                    {timer ? (
                      <div className="flex items-center ">
                        <div className="p-2">Resend OTP in </div>
                        <div className="w-10 h-10 border-2 border-red-600 rounded-full flex  items-center justify-center">
                          <h2 className="h5 text-error">{timer}</h2>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                  <span className="mb-3 block ">
                    Please enter the 5 digit code sent to your mobile phone
                  </span>
                  {/* <strong>
                    Mobile Number: {localStorage.getItem('phone')}
                  </strong> */}

                  <div className="row-sm mb-3">
                    <Input
                      label="Verification Code"
                      name="token"
                      type="text"
                      placeholder=""
                    />
                  </div>

                  <div className="btn-holder mt-3">
                    <Button type="submit" disabled={isSubmitting || !dirty}>
                      Verify
                    </Button>

                    <Button
                      variant="secondary"
                      onClick={() => {
                        ReSend();
                      }}
                      disabled={isLoadingResendToken || timer}
                    >
                      Resend
                    </Button>

                    <Button
                      variant="outlined-error"
                      type="button"
                      onClick={() => {
                        resetForm();
                      }}
                      disabled={isSubmitting || !dirty}
                    >
                      Clear
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default VerifyPhone;
