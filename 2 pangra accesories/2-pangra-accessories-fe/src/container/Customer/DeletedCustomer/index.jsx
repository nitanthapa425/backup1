import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import {
  // useDeleteCustomerMutation,

  useReadAllDeletedCustomerQuery,
  useRevertDeletedCustomerMutation,
} from "services/api/customer";
import {
  getQueryStringForCustomerTable,
  // getQueryStringForTable,
  // getQueryStringForTable,
} from "utils/getQueryStringForTable";

import { DateColumnFilter, SelectColumnFilter } from "components/Table/Filter";

function ViewDeletedCustomerContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [DeletedCustomerDataQuery, setDeletedCustomerDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);
  // const deleteInfo = useDeleteSubModelMutation();

  const {
    data: deletedCustomerData,
    isError: deletedCustomerError,
    isFetching: isLoadingDeletedCustomer,
  } = useReadAllDeletedCustomerQuery(DeletedCustomerDataQuery);
  // console.log(deletedCustomerData);

  const {
    data: deletedCustomerDataAll,
    // isError: isVehicleErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllDeletedCustomerQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });

  useEffect(() => {
    if (deletedCustomerDataAll) {
      setTableDataAll(
        deletedCustomerDataAll.docs.map((deletedCustomers) => {
          return {
            fullName: deletedCustomers?.fullName,
            mobile: deletedCustomers?.mobile,
            email: deletedCustomers?.email || "Not Available",
            condition: deletedCustomers?.condition,
            gender: deletedCustomers?.gender,
            dob: deletedCustomers?.dob,
            status: deletedCustomers?.status,
            phoneVerified: deletedCustomers?.phoneVerified
              ? "Verified"
              : "Unverified",
            emailVerified: deletedCustomers?.emailVerified
              ? "Verified"
              : "Unverified",
            receiveNewsletter: deletedCustomers?.receiveNewsletter
              ? "Yes"
              : "No",
            id: deletedCustomers?.id,
          };
        })
      );
    }
  }, [deletedCustomerDataAll]);
  const [
    activeDeleteItems,
    {
      isLoading: isActivating,
      isSuccess: isActivatedSuccess,
      isError: isActivatedError,
      error: activatedError,
    },
  ] = useRevertDeletedCustomerMutation();

  const columns = useMemo(
    () => [
      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      // {
      //   id: 'middleName',
      //   Header: 'Middle Name',
      //   accessor: 'middleName',
      //   Cell: ({ cell: { value } }) => value || '-',
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      // {
      //   id: 'lastName',
      //   Header: 'Last Name',
      //   accessor: 'lastName',
      //   Cell: ({ cell: { value } }) => value || '-',
      //   canBeSorted: true,
      //   canBeFiltered: true,
      // },
      {
        id: "mobile",
        Header: "Mobile Number",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "email",
        Header: "Email",
        accessor: "email",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "dob",
        Header: "Date of Birth",
        accessor: "dob",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "gender",
        Header: "Gender",
        accessor: "gender",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Male",
            value: "Male",
          },
          {
            label: "Female",
            value: "Female",
          },
          {
            label: "Other",
            value: "Other",
          },
        ],
      },
      {
        id: "phoneVerified",
        Header: "Phone Verified",
        accessor: "phoneVerified",
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Verified",
            value: true,
          },
          {
            label: "Unverified",
            value: false,
          },
        ],
      },
      {
        id: "emailVerified",
        Header: "Email Verified",
        accessor: "emailVerified",
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Verified",
            value: true,
          },
          {
            label: "Unverified",
            value: false,
          },
        ],
      },
      // {
      //   id: 'status',
      //   Header: 'Status',
      //   accessor: 'status',
      //   // Cell: ({ cell: { value } }) => {
      //   //   if (value) {
      //   //     return 'verified';
      //   //   } else {
      //   //     return 'unverified';
      //   //   }
      //   // },
      //   Cell: ({ cell: { value } }) => {
      //     if (value === null || value === undefined) return '-';
      //     else if (value === 'active')
      //       return <span className="badge badge-green">Active</span>;
      //     else return <span className="badge badge-red">Inactive</span>;
      //   },

      //   canBeSorted: true,
      //   canBeFiltered: true,
      //   Filter: SelectColumnFilter,
      //   possibleFilters: [
      //     {
      //       label: 'active',
      //       value: 'active',
      //     },
      //     {
      //       label: 'inactive',
      //       value: 'inactive',
      //     },
      //   ],
      // },

      {
        id: "receiveNewsletter",
        Header: "Receive Newsletter",
        accessor: "receiveNewsletter",
        // Cell: ({ cell: { value } }) => {
        //   if (value) {
        //     return 'verified';
        //   } else {
        //     return 'unverified';
        //   }
        // },
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "yes",
            value: true,
          },
          {
            label: "no",
            value: false,
          },
        ],
      },
    ],

    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForCustomerTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setDeletedCustomerDataQuery(query);
  };

  useEffect(() => {
    if (deletedCustomerData) {
      setPageCount(deletedCustomerData.totalPages);
      setTotalData(deletedCustomerData.totalDocs);

      setTableData(
        deletedCustomerData.docs.map((deletedCustomers) => {
          return {
            fullName: deletedCustomers?.fullName,
            mobile: deletedCustomers?.mobile,
            email: deletedCustomers?.email || "Not Available",
            condition: deletedCustomers?.condition,
            gender: deletedCustomers?.gender,
            dob: deletedCustomers?.dob,
            status: deletedCustomers?.status,
            phoneVerified: deletedCustomers?.phoneVerified
              ? "Verified"
              : "Unverified",
            emailVerified: deletedCustomers?.emailVerified
              ? "Verified"
              : "Unverified",
            receiveNewsletter: deletedCustomers?.receiveNewsletter
              ? "Yes"
              : "No",
            id: deletedCustomers?.id,
          };
        })
      );
    }
  }, [deletedCustomerData]);
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Deleted Customer List",
      route: "",
    },
  ];

  return (
    <AdminLayout documentTitle="View Customer" BreadCrumbList={BreadCrumbList}>
      <section className="mt-3">
        <div className="container">
          <Table
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={deletedCustomerError}
            isLoadingData={isLoadingDeletedCustomer}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            viewRoute="deletedCustomer/details"
            // deleteQuery={deleteItems}
            // isDeleting={isDeleting}
            // isDeleteError={isDeleteError}
            // isDeleteSuccess={isDeleteSuccess}
            // DeletedError={DeletedError}
            activeDeleteItems={activeDeleteItems}
            revertButtonName="Activate Customer"
            revertMultipleButtonName="Activate Multiple Customer"
            isActivating={isActivating}
            isActivatedError={isActivatedError}
            isActivatedSuccess={isActivatedSuccess}
            activatedError={activatedError}
            hasExport={true}
            // addPage={{
            //   page: 'Add Customer',
            //   route: '/customer/add',
            // }}
            tableName="DeletedCustomer/s"
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default ViewDeletedCustomerContainer;
