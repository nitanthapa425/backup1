import React, { useEffect, useState } from "react";

import { TrashIcon } from "@heroicons/react/outline";
import {
  useDeleteMyCartMutation,
  useGetMyCartQuery,
} from "services/api/seller";
import { useToasts } from "react-toast-notifications";
import { sameArrayIrrespectiveToPosition } from "utils/sameArray";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import Popup from "components/Popup";
import { useRouter } from "next/router";
import Link from "next/link";
// import ReactModal from "components/ReactModal/ReactModal";
// import Address from "components/Location";
import { useDispatch } from "react-redux";
import Button from "components/Button";
import { setBookList } from "store/features/book/BookSlice";
import { sumOfEachElementOfArray } from "utils/methods";

import Cart from "./Cart";

const AddToCart = () => {
  const router = useRouter();
  const { addToast } = useToasts();
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [totalCostList, setTotalCostList] = useState([]);
  const [deleteSingleCart, setDeleteSingleCart] = useState(null);
  const [openModalDeleteSingleCart, setOpenModalDeleteSingleCart] =
    useState(false);

  const dispatch = useDispatch();
  const [bookDet, setBookDet] = useState([]);
  // const bookList = useSelector((state) => state.book.bookList);

  const {
    data: dataMyCart,
    isFetching: isFetchingMyCart,
    isSuccess: isSuccessMyCart,
  } = useGetMyCartQuery();

  const [bookList, setBookLis] = useState([]);
  const [
    deleteCart,
    {
      isLoading: isLoadingDeleteCart,
      isError: isErrorDeleteCart,
      isSuccess: isSuccessDeleteCart,
      error: errorDeleteCart,
      data: dataDeleteCart,
    },
  ] = useDeleteMyCartMutation();

  const handleBookNow = () => {
    const filtredBookList = bookDet.filter((data, i) => {
      return bookList.includes(data.cartId);
    });

    const filterIfQuantity = filtredBookList.filter((data, i) => {
      return Number(data.quantity);
    });

    const mapBookList = filterIfQuantity.map((data, i) => {
      return {
        color: data?.color,
        size: data?.size,
        quantity: data?.quantity,
        id: data?.id,
      };
    });

    dispatch(setBookList(mapBookList));

    // dispatch(setBookDetails(mapBookList));

    router.push("/customer/book");
  };

  const totalNumberOfQuantityOrder = () => {
    const filtredBookList = bookDet.filter((data, i) => {
      return bookList.includes(data.cartId);
    });

    const filterIfQuantity = filtredBookList.filter((data, i) => {
      return Number(data.quantity);
    });

    const totalQuantityList = filterIfQuantity.map((data, i) => {
      return Number(data?.quantity);
    });

    const totalQuantityListSum = sumOfEachElementOfArray(totalQuantityList);
    return totalQuantityListSum;
  };

  useEffect(() => {
    if (isSuccessDeleteCart) {
      addToast(
        dataDeleteCart?.message || "Item/s removed from the cart successfully.",
        {
          appearance: "success",
        }
      );
      // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
    }
  }, [isSuccessDeleteCart]);
  useEffect(() => {
    if (isErrorDeleteCart) {
      addToast(errorDeleteCart?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorDeleteCart, errorDeleteCart]);

  const totalPrice = (bookList = []) => {
    // const filterIfQuantity = totalCostList.filter((data, i) => {
    //   return Number(data.quantity);
    // });

    const array1 = totalCostList?.filter((data, i) =>
      bookList.includes(data.id)
    );

    const arrayPrice = array1?.map((data, i) => {
      return data.value || 0;
    });

    // odering the price and quantity according to bookList quantity
    // bookList.forEach((v1, i) => {
    //   const arr4 = arrayPrice.find((v2, i) => {
    //     return v2.id === v1;
    //   });

    //   const arr6 = bookDet.find((v3, i) => {
    //     return v3.cartId === v1;
    //   });
    //   arr5 = [...arr5, arr6];

    //   arr3 = [...arr3, arr4];
    // });

    // const allPrice = arr3?.map((data, i) => {
    //   return data?.price;
    // });

    // const allQuantity = arr5?.map?.((value, i) => {
    //   return value?.quantity;
    // });

    // const productArray = twoArrayElementProduct(allPrice, allQuantity);

    const total = sumOfEachElementOfArray(arrayPrice);
    return total;
  };

  // const cartQuantity = (bookDet, index) => {
  //   const quantity = bookDet?.find((v, i) => {
  //     return i === index;
  //   })?.quantity;
  //   return quantity;
  // };
  useEffect(() => {
    const book = dataMyCart?.map?.((data, i) => {
      return {
        color: data?.color,
        size: data?.sizeValue,
        quantity: data?.quantity,
        id: data?.accessoriesId?.id,
        cartId: data?._id,
      };
    });
    setBookDet(book);
  }, [isSuccessMyCart]);

  return (
    <div className="pb-[80px]">
      {isFetchingMyCart ? (
        <div className="container flex justify-center">
          <svg
            className={`animate-spin h-10 w-10 text-primary`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      ) : dataMyCart?.length ? (
        <div className="container">
          {openModalVehicleType && (
            <Popup
              title=" Are you sure you want to remove item/s from the cart?"
              description=" Please note that, once removed this cannot be undone."
              onOkClick={() => {
                bookList.forEach((id, i) => {
                  deleteCart(id)
                    .then(() => {
                      setOpenModalVehicleType(false);
                    })
                    .catch(() => {
                      setOpenModalVehicleType(false);
                    });
                });
              }}
              onCancelClick={() => setOpenModalVehicleType(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteCart}
            />
          )}

          {openModalDeleteSingleCart && (
            <Popup
              title="Are you sure to remove item form cart list?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                deleteCart(deleteSingleCart)
                  .then(() => {
                    setOpenModalDeleteSingleCart(false);
                  })
                  .catch(() => {
                    setOpenModalDeleteSingleCart(false);
                  });
              }}
              onCancelClick={() => setOpenModalDeleteSingleCart(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteCart}
            />
          )}

          <div className="flex flex-wrap items-start !mt-0">
            <div className="flex-1 md:mr-[25px] ">
              <div className="flex justify-between items-center py-2 mb-2">
                <div className="flex items-center space-x-2">
                  <input
                    type="checkbox"
                    // checked if two array are same
                    checked={sameArrayIrrespectiveToPosition(
                      bookList,
                      dataMyCart?.map((data, i) => data._id)
                    )}
                    className="checkbox w-4 h-4 border border-gray-500 inline-block"
                    onClick={(e) => {
                      if (e.target.checked) {
                        setBookLis((bookList) => {
                          bookList = [];
                          return dataMyCart?.map((data, i) => {
                            return data._id;
                          });
                        });
                      } else {
                        setBookLis([]);
                      }
                    }}
                  ></input>
                  <span className="inline-block ml-2">
                    Select all items{" "}
                    {bookList.length ? `(${bookList.length} item/s)` : ""}
                  </span>
                </div>
                <div className="flex items-center ">
                  <button
                    onClick={() => {
                      setOpenModalVehicleType(true);
                    }}
                  >
                    {bookList.length ? (
                      <TrashIcon className="w-5 h-5 inline-block mr-2 hover:text-error " />
                    ) : (
                      ""
                    )}
                  </button>
                </div>
              </div>
              {/* {[...dataMyCart]?.reverse()?.map((data, index) => { */}
              {dataMyCart?.map((data, index) => {
                return (
                  <div key={index}>
                    <Cart
                      data={data}
                      index={index}
                      bookList={bookList}
                      setBookLis={setBookLis}
                      bookDet={bookDet}
                      setBookDet={setBookDet}
                      setOpenModalDeleteSingleCart={
                        setOpenModalDeleteSingleCart
                      }
                      setDeleteSingleCart={setDeleteSingleCart}
                      setTotalCostList={setTotalCostList}
                      totalCostList={totalCostList}
                    ></Cart>
                  </div>
                );
              })}
            </div>
            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-[75px]">
              <h1 className="h5 mb-3">Order Summary</h1>
              {/* <div className="border-b-2 border-gray-200 mb-2">
                <div className="flex flex-wrap justify-between ">
                  <p className="mb-1">
                    SubTotal{" "}
                    {bookList.length ? `(${bookList.length} item/s)` : ""}
                  </p>
                  <p className="mb-1">
                    NRs. {thousandNumberSeparator(totalPrice(bookList))}
                  </p>
                </div>
              </div> */}
              <div className="flex flex-wrap justify-between mb-3">
                <p className="mb-1">
                  Total Amount{" "}
                  {bookList.length
                    ? `(${totalNumberOfQuantityOrder()} product/s)`
                    : ""}
                </p>
                <p className="mb-1">
                  NRs. {thousandNumberSeparator(totalPrice(bookList))}
                </p>
              </div>

              <Button disabled={bookList.length === 0} onClick={handleBookNow}>
                Order Now
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your cart is empty</h1>
            <p>
              Looks like you havent made your <br />
              choice yet
            </p>

            <Link href={"/product-listing"}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default AddToCart;
