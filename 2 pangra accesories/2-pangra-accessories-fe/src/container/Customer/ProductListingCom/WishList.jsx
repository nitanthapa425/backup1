/* eslint-disable no-empty-pattern */
// import UserHeader from 'components/Header/UserHeader';
import React, { useEffect, useState } from "react";
// import Link from 'next/link';
// import UserLayout from 'layouts/User';
import Link from "next/link";

import {
  // DownloadIcon,
  TrashIcon,
  // HeartIcon,
  XIcon,
} from "@heroicons/react/outline";
// import Button from 'components/Button';
import {
  useAddToCartFromWishListMutation,
  // useAddToBookMutation,
  // useAddToCartMutation,
  // useDeleteMyCartMutationWishList,
  // useAddToCartProductMutation,
  useDeleteMyWishListMutation,
  useGetMyWishListQuery,
} from "services/api/seller";
import { useToasts } from "react-toast-notifications";
import { sameArrayIrrespectiveToPosition } from "utils/sameArray";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import { valueSeparatedByComma } from "utils/valueSeparatedByComma";
import Popup from "components/Popup";
import { useRouter } from "next/router";
// import  valueSeparatedByComma from ""

import Button from "components/Button";

const AddToWishList = () => {
  const { addToast } = useToasts();

  const router = useRouter();
  const [deleteModel, setDeleteModel] = useState(null);
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [openModalDeleteSingleWishList, setOpenModalDeleteSingleWishList] =
    useState(false);
  const [cartDet, setCartDet] = useState([]);
  const {
    data: dataMyWishList,
    // isError: isErrorMyCart,
    // error: errorMyCart,
    isSuccess: isSuccessMyWish,
    isFetching: isFetchingMyWish,
  } = useGetMyWishListQuery();

  const [cartList, setCartList] = useState([]);
  // const proId = dataMyWishList?.productId;

  // console.log('mycart', dataMyWishList);

  // const [
  //   addToCart,
  //   {
  //     isLoading: isLoadingAddToCart,
  //     isError: isErrorAddToCart,
  //     isSuccess: isSuccessAddToCart,
  //     error: addToCartError,
  //   },
  // ] = useAddToCartProductMutation();
  const [
    addToCartFromWishList,
    {
      isLoading: isLoadingAddToCartFromWishList,
      isError: isErrorAddToCartFromWishList,
      isSuccess: isSuccessAddToCartFromWishList,
      error: errorAddToCartFromWishList,
      data: dataAddToCartFromWishList,
    },
  ] = useAddToCartFromWishListMutation();

  useEffect(() => {
    if (isSuccessAddToCartFromWishList) {
      addToast(
        dataAddToCartFromWishList?.message ||
          "You have successfully move a product from wishlist to cart.",
        {
          appearance: "success",
        }
      );
    }
    if (isErrorAddToCartFromWishList) {
      addToast(errorAddToCartFromWishList?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAddToCartFromWishList, isErrorAddToCartFromWishList]);

  const [
    deleteWishList,
    {
      isLoading: isLoadingDeleteWishList,
      isError: isErrorDeleteWishList,
      isSuccess: isSuccessDeleteWishList,
      error: errorDeleteWishList,
    },
  ] = useDeleteMyWishListMutation();

  const handleCart = () => {
    const filteredCartList = cartDet.filter((data, i) => {
      return cartList.includes(data.id);
    });

    const mapCartList = filteredCartList.map((data, i) => {
      return data?.id;
    });

    // console.log("console.log");
    // dispatch(setCartList(mapCartList));

    addToCartFromWishList(mapCartList);

    // dispatch(setBookDetails(mapBookList));

    router.push("/cart");
  };

  useEffect(() => {
    if (isSuccessDeleteWishList) {
      addToast("Item/s removed from the wishlist successfully.", {
        appearance: "success",
      });
    }
    if (isErrorDeleteWishList) {
      addToast(errorDeleteWishList?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessDeleteWishList, isErrorDeleteWishList]);

  // useEffect(() => {
  //   if (isSuccessAddToCart) {
  //     addToast("You just added item/s in cart.", {
  //       appearance: "success",
  //     });
  //     router.push("/cart");
  //     // router.push(`/vehicle/vehicleDetail/${router.query.id}`);
  //   }
  //   if (isErrorAddToCart) {
  //     addToast(addToCartError?.data?.message, {
  //       appearance: "error",
  //     });
  //   }
  // }, [isSuccessAddToCart, isErrorAddToCart]);
  // useEffect(() => {}, [isErrorAddToCart, addToCartError]);

  // console.log(bookList);

  // useAddToCartFromWishListMutation

  const totalPrice = (bookList = []) => {
    const array1 = dataMyWishList?.filter((data, i) =>
      bookList.includes(data.productId)
    );

    const arrayPrice = array1?.map((data, i) => Number(data.price));

    const total = arrayPrice?.reduce((cur, pre) => cur + pre, 0);
    return total;
  };
  useEffect(() => {
    const book = dataMyWishList?.map?.((data, i) => {
      return {
        id: data?.productId,
      };
    });
    // console.log(book);
    setCartDet(book);
  }, [isSuccessMyWish]);

  return (
    <div className="pb-[80px]">
      {/* <UserLayout title="My Wish List"> */}
      {/* <UserHeader></UserHeader> */}
      {isFetchingMyWish ? (
        <div className="container flex justify-center">
          {/* <p className="f-lg">Loading...</p> */}
          <svg
            className={`animate-spin h-10 w-10 text-primary`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      ) : dataMyWishList?.length ? (
        <div className="container">
          {openModalVehicleType && (
            <Popup
              title="Are you sure you want to remove item/s from the wish-list?"
              description="Please note that, once removed this cannot be undone."
              onOkClick={() => {
                cartList.forEach((id, i) => {
                  deleteWishList(id)
                    .then(() => {
                      setOpenModalVehicleType(false);
                    })
                    .catch(() => {
                      setOpenModalVehicleType(false);
                    });
                });
              }}
              onCancelClick={() => setOpenModalVehicleType(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteWishList}
            />
          )}

          {openModalDeleteSingleWishList && (
            <Popup
              title="Are you sure to remove item form wish list?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                deleteWishList(deleteModel)
                  .then(() => {
                    setOpenModalDeleteSingleWishList(false);
                  })
                  .catch(() => {
                    setOpenModalDeleteSingleWishList(false);
                  });
              }}
              onCancelClick={() => setOpenModalDeleteSingleWishList(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteWishList}
            />
          )}

          <div className="flex flex-wrap items-start">
            <div className="flex-1 mr-[25px] ">
              <div className="flex justify-between items-center py-2 mb-2">
                <div className="flex items-center space-x-2">
                  <input
                    type="checkbox"
                    // checked if two array are same
                    checked={sameArrayIrrespectiveToPosition(
                      cartList,
                      dataMyWishList?.map((data, i) => data.productId)
                    )}
                    className="checkbox w-4 h-4 border border-gray-500 inline-block"
                    onClick={(e) => {
                      // console.log(e.target.checked);
                      if (e.target.checked) {
                        // setCartList(dataMyWishList?.cllientCartDetail);
                        setCartList((bookList) => {
                          bookList = [];
                          return dataMyWishList?.map((data, i) => {
                            return data.productId;
                          });
                        });
                      } else {
                        setCartList([]);
                      }
                    }}
                  ></input>

                  <span className="inline-block ml-2">
                    Select all items{" "}
                    {cartList.length ? `(${cartList.length} item/s)` : ""}
                  </span>
                </div>
                {cartList.length !== 0 && (
                  <div className="flex items-center ">
                    <button
                      onClick={() => {
                        setOpenModalVehicleType(true);
                      }}
                    >
                      <TrashIcon className="w-5 h-5 inline-block mr-2" />
                    </button>
                  </div>
                )}
              </div>
              {dataMyWishList?.map((data, index) => {
                return (
                  <div
                    // key={i}
                    key={index}
                    className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5"
                  >
                    {/* <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span> */}
                    <input
                      type="checkbox"
                      checked={cartList.includes(data.productId)}
                      className="checkbox w-4 h-4 border border-gray-400 inline-block mr-4"
                      onClick={(e) => {
                        // console.log(e.target.checked);
                        if (e.target.checked) {
                          const allId = [...cartList, data.productId];
                          const uniqueList = [...new Set(allId)];
                          setCartList(uniqueList);
                        } else {
                          setCartList(
                            cartList.filter((v, j) => {
                              return v !== data.productId;
                            })
                          );
                        }
                      }}
                    ></input>
                    <div
                      className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0"
                      onClick={() => {
                        router.push(`/product-listing/${data.productId}`);
                      }}
                    >
                      <img
                        src={data?.productImage?.imageUrl}
                        alt="image description"
                        className="w-full h-[110px] object-contain"
                      />
                    </div>
                    <div
                      className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[130px]"
                      onClick={() => {
                        router.push(`/product-listing/${data.productId}`);
                      }}
                    >
                      <h1 className="h5 mb-1">{data?.productName}</h1>
                      <div className=" flex justify-between lg:block lg:w-[100px] lg:absolute lg:text-right lg:right-[15px] lg:top-1/2 lg:translate-Y-[-50%]">
                        <span className="price font-bold">
                          NRs. {thousandNumberSeparator(data.price)}
                        </span>
                        {/* <div className="icon-holder flex space-x-2">
                          <HeartIcon className="w-5 h-5" />
                          <TrashIcon className="w-5 h-5" />
                        </div> */}
                      </div>

                      {data.color.length ? (
                        <div className="other-details">
                          <p className="text-sm">
                            <span className="font-semibold">Color :</span>
                            {valueSeparatedByComma(data.color)}
                          </p>
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                    <span
                      className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
                      onClick={() => {
                        setOpenModalDeleteSingleWishList(true);
                        setDeleteModel(data.productId);
                      }}
                    >
                      <XIcon className="w-5 h-5"></XIcon>
                    </span>

                    {/* <div className="w-full cart-select">
                      <div className="flex mt-3 space-x-3 ">
                        <div>
                          <SelectWithoutForm
                            className={`${
                              `${data?.sizeValues?.length}` === `0`
                                ? "disabled-input"
                                : ""
                            } w-24`}
                            label="Size"
                            value={
                              cartDet?.find((v, i) => {
                                return i === index;
                              })?.size
                            }
                            disabled={`${data?.sizeValue?.length}` === `0`}
                            onChange={(e) => {
                              const bookl1 = cartDet?.map?.((obj, i) => {
                                if (i === index) {
                                  return {
                                    ...obj,
                                    size: e.target.value,
                                  };
                                } else {
                                  return obj;
                                }
                              });
                              setCartDet(bookl1);
                              // dispatch(setBookLis([bookl1]));
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Select Size
                            </option>

                            {data?.sizeValues?.map((datas, i) => (
                              <option key={i} value={datas}>
                                {datas}
                              </option>
                            ))}
                          </SelectWithoutForm>
                        </div>
                        <div>
                          <SelectWithoutForm
                            className={`${
                              `${data?.quantity}` === `0`
                                ? "disabled-input"
                                : ""
                            } w-24`}
                            label="Quantity"
                            value={
                              cartDet?.find((v, i) => {
                                return i === index;
                              })?.quantity
                            }
                            disabled={`${data?.quantity}` === `0`}
                            onChange={(e) => {
                              const bookl1 = cartDet.map((obj, i) => {
                                if (i === index) {
                                  return {
                                    ...obj,
                                    quantity: e.target.value,
                                  };
                                } else {
                                  return obj;
                                }
                              });

                              setCartDet(bookl1);
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Select Quantity
                            </option>

                            {nNumberArray(Number(data?.quantity)).map(
                              (datas, i) => (
                                <option key={i} value={datas}>
                                  {datas}
                                </option>
                              )
                            )}
                          </SelectWithoutForm>
                        </div>
                        <div>
                          <SelectWithoutForm
                            className={`${
                              `${data?.colors?.length}` === `0`
                                ? "disabled-input"
                                : ""
                            } w-24`}
                            label="Color"
                            value={
                              cartDet?.find((v, i) => {
                                return i === index;
                              })?.color
                            }
                            disabled={`${data?.color?.length}` === `0`}
                            onChange={(e) => {
                              const bookl1 = cartDet.map((obj, i) => {
                                if (i === index) {
                                  return {
                                    ...obj,
                                    color: e.target.value,
                                  };
                                } else {
                                  return obj;
                                }
                              });
                              setCartDet(bookl1);
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Select Color
                            </option>

                            {data?.colors?.map?.((datas, i) => (
                              <option key={i} value={datas}>
                                {datas}
                              </option>
                            ))}
                          </SelectWithoutForm>
                        </div>
                      </div>
                    </div> */}
                  </div>
                );
              })}
              {/* <div className="flex flex-wrap items-center space-x-3 mb-4 border border-gray-600 py-6 pl-5">
                <span className="checkbox w-5 h-5 border border-gray-500 inline-block"></span>
                <input
                  type="checkbox"
                  className="checkbox w-5 h-5 border border-gray-500 inline-block"
                ></input>
                <div className="image-holder max-w-[300px] w-full lg:w-[220px]">
                  <img
                    src="images/card-img.jpg"
                    alt="image description"
                    className="w-full"
                  />
                </div>
                <div className="detail-holder flex-1 relative md:pr-[110px]">
                  <h1 className="h4">Name Goes Here</h1>
                  <div className=" flex justify-between lg:block lg:w-[85px] lg:absolute lg:right-[-40px] lg:top-1/2 lg:translate-x-[-50%]">
                    <span className="price">1,40,000</span>
                    <div className="icon-holder flex space-x-2">
                      <HeartIcon className="w-5 h-5" />
                      <TrashIcon className="w-5 h-5" />
                    </div>
                  </div>
                  <div className="other-details">
                    <p className="text-sm">
                      <span className="font-semibold">Color :</span> Black ,
                      <span className="font-semibold">location :</span>{' '}
                      Kathmandu ,
                      <span className="font-semibold"> Owner Count: </span> 2 ,
                      <span className="font-semibold"> Kilometer Driven: </span>
                      1000Km
                    </p>
                  </div>
                  <div className="flex flex-wrap space-x-3">
                    <span className="flex">
                      <CheckCircleIcon className="w-5 h-5" />
                      Verified
                    </span>
                    <span className="flex">
                      <EyeIcon className="w-5 h-5" />1 views
                    </span>
                  </div>
                </div>
              </div> */}
            </div>
            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-[75px]">
              <h1 className="h5 mb-3">Order Summary</h1>
              <div className="flex flex-wrap justify-between ">
                <p>
                  Product Amount{" "}
                  {cartList.length ? `(${cartList.length} item/s)` : ""}
                </p>
                <p>NRs. {thousandNumberSeparator(totalPrice(cartList))}</p>
              </div>
              <div className="text-center">
                <Button
                  // type="button"
                  // onClick={() => {
                  //   cartList.forEach((value, i) => {
                  //     // console.log(value);
                  //     addToCart(value);
                  //   });
                  // }}
                  // disabled={cartList.length === 0}
                  loading={isLoadingAddToCartFromWishList}
                  disabled={cartList.length === 0}
                  onClick={handleCart}
                >
                  Add To Cart
                </Button>
              </div>

              {/* <ReactModal name="Book Now" disabled={bookList.length === 0}>
                <Address bookList={bookList}></Address>
              </ReactModal> */}
              {/* <ReactModal name="Book Now" disabled={bookList.length === 0}>
                <Address bookList={bookList}></Address>
              </ReactModal> */}
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your wish list is empty</h1>
            <p>
              Looks like you havent made your <br />
              choice yet
            </p>
            <Link href={"/product-listing"}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}

      {/* <div className="container">
          <div className="row-sm">
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
            <div className="three-col-sm">
              <div className="border border-gray-500">ajsjs</div>
            </div>
          </div>
        </div> */}
      {/* </UserLayout> */}
    </div>
  );
};

export default AddToWishList;
