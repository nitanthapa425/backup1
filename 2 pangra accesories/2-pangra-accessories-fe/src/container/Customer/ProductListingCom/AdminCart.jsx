import { XIcon } from "@heroicons/react/outline";
import SelectWithoutForm from "components/Select/SelectWithoutForm";
import { productQuantity } from "constant/constant";
import { useRouter } from "next/router";
import React, { useEffect, useMemo, useState } from "react";
import {
  useReadQuantityMutation,
  useReadSizeColorQuantityMutation,
} from "services/api/customer";
import { useReadSingleProductQuery } from "services/api/seller";
import { disableOneOf } from "utils/disableOneOf";
import { nNumberArray } from "utils/nNumberOfArray";
import { uniqueIdAmountSum } from "utils/uniqueIdAmountSum";
import ActualPrice from "./ActualPrice";

const AdminSuperAdminCart = ({
  index,
  data,
  bookList,
  setBookLis,
  deleteCartItems,
  setDeleteCartItems,
  bookDet,
  setBookDet,
  setOpenModalDeleteSingleCart,
  setDeleteSingleCart,
  setTotalCostList,
  totalCostList,
}) => {
  const [avalilableColors, setAvailableColors] = useState();
  // eslint-disable-next-line no-unused-vars
  const [avalilableQuanties, setAvailableQuantities] = useState();
  const [discountedAmount, setDiscountedAmount] = useState();
  // eslint-disable-next-line no-unused-vars
  const [discountPercentage, setDiscountedPercent] = useState();

  const { data: productDetails, isSuccess: isSucessProductDetails } =
    useReadSingleProductQuery(data?.accessoriesId?.id);

  const productAvailableSize = useMemo(() => {
    const productAvailableSizeValue = productDetails?.availableOptions?.map?.(
      (value, i) => value.size
    );
    const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
    return uniqueAvailableSizeValue;
  }, [productDetails]);

  const cardSize = useMemo(() => {
    const size = bookDet?.find?.((v, i) => {
      return i === index;
    })?.size;

    return size;
  }, [bookDet]);

  const cardColor = useMemo(() => {
    const color = bookDet?.find?.((v, i) => {
      return i === index;
    })?.color;

    return color;
  }, [bookDet]);

  const cardQuantity = useMemo(() => {
    const quantity = bookDet?.find?.((v, i) => {
      return i === index;
    })?.quantity;

    return quantity;
  }, [bookDet, cardSize, cardColor]);

  const cardDiscountedPrice = useMemo(() => {
    const discountedPrice = bookDet?.find?.((v, i) => {
      return i === index;
    })?.discountedPrice;

    return discountedPrice;
  }, [bookDet, cardSize, cardColor, cardQuantity]);

  const costValue = () => {
    return Number(cardDiscountedPrice);
  };

  useEffect(() => {
    if (isSucessProductDetails) {
      setTotalCostList((totalCostList) => {
        const uniqueIdAmount = uniqueIdAmountSum(totalCostList, {
          id: data?.accessoriesId?.id,
          value: costValue(),
        });

        return uniqueIdAmount;
      }, []);
    }
  }, [
    isSucessProductDetails,
    discountedAmount,
    cardQuantity,
    data,
    cardDiscountedPrice,
  ]);

  useEffect(() => {
    if (cardSize && productDetails?.id) {
      readSizeColorQuantity({
        body: { size: cardSize },
        id: productDetails?.id,
      });

      setDiscountedAmount(null);
      setDiscountedPercent(null);
    }
  }, [cardSize, productDetails]);

  useEffect(() => {
    if (cardColor && productDetails?.id && cardSize) {
      readQuantity({
        body: { size: cardSize, color: cardColor },
        id: productDetails?.id,
      });
      setDiscountedAmount(null);
      setDiscountedPercent(null);
    }
  }, [cardColor, cardSize, productDetails]);

  const [
    readSizeColorQuantity,
    {
      isError: isErrorReadSizeColorQuantity,
      isSuccess: isSuccessReadSizeColorQuantity,
      data: dataReadSizeColorQuantity,
    },
  ] = useReadSizeColorQuantityMutation();

  const [
    readQuantity,
    {
      isError: isErrorReadQuantity,
      isSuccess: isSuccessReadQuantity,
      data: dataReadQuantity,
    },
  ] = useReadQuantityMutation();

  useEffect(() => {
    if (isSuccessReadQuantity) {
      setAvailableQuantities(dataReadQuantity[0].quantity);
      setDiscountedAmount(dataReadQuantity[0].discountedAmount);
      setDiscountedPercent(dataReadQuantity[0].discountPercentage);
    }

    // if (isErrorReadQuantity) {
    //   addToast(errorReadQuantity?.data?.message, {
    //     appearance: "error",
    //   });
    // }
  }, [isSuccessReadQuantity, isErrorReadQuantity]);

  useEffect(() => {
    if (isSuccessReadSizeColorQuantity) {
      setAvailableColors(dataReadSizeColorQuantity);
    }

    // if (isErrorReadSizeColorQuantity) {
    //   addToast(errorReadSizeColorQuantity?.data?.message, {
    //     appearance: "error",
    //   });
    // }
  }, [isSuccessReadSizeColorQuantity, isErrorReadSizeColorQuantity]);

  const router = useRouter();

  return (
    <div
      key={index}
      className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 px-5"
    >
      <input
        type="checkbox"
        checked={bookList.includes(data?.accessoriesId?.id)}
        // checked={bookList.includes(data?._id)}
        className="checkbox w-4 h-4 border border-gray-400 inline-block mr-3"
        onClick={(e) => {
          if (e.target.checked) {
            const allId = [...bookList, data?.accessoriesId?.id];
            // const allId = [...bookList, data?._id];
            const uniqueList = [...new Set(allId)];
            setBookLis(uniqueList);
            const allIdCart = [...deleteCartItems, data?._id];
            const uniqueListCart = [...new Set(allIdCart)];

            setDeleteCartItems(uniqueListCart);
          } else {
            setBookLis(
              bookList.filter((v, j) => {
                return v !== data?.accessoriesId?.id;
              })
            );
            setDeleteCartItems(
              deleteCartItems.filter((v, j) => {
                return v !== data?._id;
              })
            );
          }
        }}
      ></input>

      <div
        className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0"
        onClick={() => {
          router.push(`/admin/product/${productDetails?.id}`);
        }}
      >
        <img
          src={data?.accessoriesId?.productImage?.imageUrl}
          alt="image description"
          className="w-full h-[110px] object-contain"
        />
      </div>
      <div className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[10px]">
        <div className="flex justify-between ">
          <div>
            {" "}
            <h1
              className="h5 mb-1 hover:text-primary"
              onClick={() => {
                router.push(`/admin/product/${productDetails?.id}`);
              }}
            >
              {productDetails?.productTitle}
            </h1>
          </div>
          <div>
            <ActualPrice
              discountedAmount={productDetails?.discountedPrice}
              costPrice={productDetails?.costPrice}
              cardQuantity={cardQuantity}
            />
            {/* {!!Number(discountPercentage) && !!Number(cardQuantity) ? (
              <div className=" flex justify-between ">
                <div className="price font-bold line-through text-error mr-3 ">
                  NRs.{" "}
                  {thousandNumberSeparator(
                    (cardQuantity * productDetails?.costPrice || 0).toFixed(2)
                  )}
                </div>
                <div className="price font-bold text-primary-light">
                  NRs.{" "}
                  {thousandNumberSeparator(
                    (cardQuantity * discountedAmount || 0).toFixed(2)
                  )}
                </div>
              </div>
            ) : !!Number(productDetails?.discountPercentage) &&
              !!Number(cardQuantity) ? (
              <div className=" flex justify-between ">
                <div className="price font-bold line-through text-error mr-3 ">
                  NRs.{" "}
                  {thousandNumberSeparator(
                    cardQuantity * productDetails?.costPrice || 0
                  )}
                </div>
                <div className="price font-bold text-primary-light">
                  NRs.{" "}
                  {thousandNumberSeparator(
                    cardQuantity * productDetails?.discountedPrice || 0
                  )}
                </div>
              </div>
            ) : (
              <div className=" flex justify-between ">
                <div className="price font-bold text-primary-light">
                  NRs.{" "}
                  {thousandNumberSeparator(
                    cardQuantity * productDetails?.costPrice || 0
                  )}
                </div>
              </div>
            )} */}
          </div>
        </div>
      </div>
      <span
        className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
        onClick={() => {
          setOpenModalDeleteSingleCart(true);
          setDeleteSingleCart(data._id);
        }}
      >
        <XIcon className="w-5 h-5"></XIcon>
      </span>

      <div className="w-full cart-select !ml-0">
        <div className="row ">
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(`${productAvailableSize?.length}` === `0`)
                  ? "disabled-input"
                  : ""
              } w-24`}
              label="Size"
              value={cardSize}
              disabled={disableOneOf(`${productAvailableSize?.length}` === `0`)}
              onChange={(e) => {
                const bookl1 = bookDet?.map?.((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      size: e.target.value,
                      color: "",
                      quantity: "",
                    };
                  } else {
                    return obj;
                  }
                });
                setBookDet(bookl1);
                // dispatch(setBookLis([bookl1]));
              }}
            >
              <option value="" selected disabled={true}>
                Select Size
              </option>

              {productAvailableSize?.map((size, i) => (
                <option key={i} value={size}>
                  {size}
                </option>
              ))}
            </SelectWithoutForm>
          </div>
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(!cardSize || `${avalilableColors?.length}` === `0`)
                  ? "disabled-input"
                  : ""
              } w-24`}
              label="Color"
              value={cardColor}
              disabled={disableOneOf(
                !cardSize || `${avalilableColors?.length}` === `0`
              )}
              onChange={(e) => {
                const bookl1 = bookDet.map((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      color: e.target.value,
                      quantity: "",
                    };
                  } else {
                    return obj;
                  }
                });
                setBookDet(bookl1);
              }}
            >
              <option value="" selected disabled={true}>
                Select Color
              </option>

              {avalilableColors?.map?.((color, i) => (
                <option key={i} value={color}>
                  {color}
                </option>
              ))}
            </SelectWithoutForm>
          </div>
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(!cardSize, !cardColor) ? "disabled-input" : ""
              } w-24`}
              label="Quantity"
              value={cardQuantity}
              disabled={disableOneOf(!cardSize, !cardColor)}
              onChange={(e) => {
                const bookl1 = bookDet.map((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      quantity: e.target.value,
                    };
                  } else {
                    return obj;
                  }
                });

                setBookDet(bookl1);
              }}
            >
              <option value="" selected disabled={true}>
                Select Quantity
              </option>

              {nNumberArray(productQuantity).map((quantity, i) => (
                <option key={i} value={quantity}>
                  {quantity}
                </option>
              ))}
            </SelectWithoutForm>
          </div>

          <div className="three-col">
            <label>
              Selling Price (NRs.)
              <span className="required">
                <span>*</span>
              </span>
            </label>
            <input
              type="number"
              value={cardDiscountedPrice}
              className=" border border-gray-300 placeholder-primary-dark h-[35px] w-[200px]"
              onChange={(e) => {
                const bookl1 = bookDet.map((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      discountedPrice: e.target.value,
                    };
                  } else {
                    return obj;
                  }
                });

                setBookDet(bookl1);
              }}
              min={0}
            />
          </div>
        </div>
        <div>
          {Number(cardQuantity) &&
          cardSize &&
          cardColor &&
          cardDiscountedPrice ? null : (
            <div className="mt-1 text-error">
              To sell please select all fields
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default AdminSuperAdminCart;
