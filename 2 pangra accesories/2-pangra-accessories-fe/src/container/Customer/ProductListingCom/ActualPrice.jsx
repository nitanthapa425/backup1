import React from "react";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";

const ActualPrice = ({ discountedAmount = "", costPrice, cardQuantity }) => {
  return (
    <div>
      {Number(discountedAmount) === Number(costPrice) ? (
        <div>
          <div className="price font-bold ">
            NRs.{" "}
            {thousandNumberSeparator(
              (cardQuantity * discountedAmount || 0).toFixed(2)
            )}
          </div>
        </div>
      ) : `${discountedAmount}` && cardQuantity ? (
        <div>
          <div className="price font-bold line-through text-gray-400 inline-block ">
            NRs{" "}
            {thousandNumberSeparator(
              (cardQuantity * costPrice || 0).toFixed(2)
            )}
          </div>
          <div className="price font-bold inline-block ml-2">
            NRs.{" "}
            {thousandNumberSeparator(
              (cardQuantity * discountedAmount || 0).toFixed(2)
            )}
          </div>
        </div>
      ) : (
        <div className="price font-bold ">NRs. 0</div>
      )}
    </div>
  );
};

export default ActualPrice;
