/* eslint-disable no-unused-vars */
import { useRouter } from "next/router";
import React, { useEffect, useMemo, useState } from "react";
import { useReadSingleProductQuery } from "services/api/seller";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import SelectWithoutForm from "components/Select/SelectWithoutForm";
import { nNumberArray } from "utils/nNumberOfArray";
import { useDispatch, useSelector } from "react-redux";
import { setBookList } from "store/features/book/BookSlice";
import { disableOneOf } from "utils/disableOneOf";
import {
  useReadQuantityMutation,
  useReadSizeColorQuantityMutation,
} from "services/api/customer";
import { useToasts } from "react-toast-notifications";
import Zoom from "react-medium-image-zoom";
import "react-medium-image-zoom/dist/styles.css";
import { uniqueIdAmountSum } from "utils/uniqueIdAmountSum";
import { productQuantity } from "constant/constant";
// import Modal from "react-modal";
import ChangeAvailableOption from "./ChangeAvilableOption";
import Popup from "components/Popup";
import ActualPrice from "./ActualPrice";

const BookCard = ({
  index,
  bookDetail,
  // setTotalPrice,
  setTotalCostList,
  totalCostList,
  shippingChargeAmount,
}) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const { addToast } = useToasts();
  const [avalilableColors, setAvailableColors] = useState();
  const [avalilableQuanties, setAvailableQuantities] = useState();
  const [discountedAmount, setDiscountedAmount] = useState();
  const [discountPercentage, setDiscountedPercent] = useState();
  const [changeAvailableOptions, setChangeAvailableOption] = useState();

  const [changeVariantOpen, setChangeVariantOpen] = useState(false);
  const [changeMessage, setChangeMessage] = useState(false);

  const { data: productDetails, isSuccess: isSucessProductDetails } =
    useReadSingleProductQuery(bookDetail.id);

  const productAvailableSize = useMemo(() => {
    const productAvailableSizeValue = productDetails?.availableOptions?.map?.(
      (value, i) => value.size
    );
    const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
    return uniqueAvailableSizeValue;
  }, [productDetails]);

  const [
    readSizeColorQuantity,
    {
      // isLoading: isLoadingReadSizeColorQuantity,
      isError: isErrorReadSizeColorQuantity,
      isSuccess: isSuccessReadSizeColorQuantity,
      error: errorReadSizeColorQuantity,
      data: dataReadSizeColorQuantity,
    },
  ] = useReadSizeColorQuantityMutation();

  const [
    readQuantity,
    {
      // isLoading: isLoadingReadQuantity,
      isError: isErrorReadQuantity,
      isSuccess: isSuccessReadQuantity,
      error: errorReadQuantity,
      data: dataReadQuantity,
    },
  ] = useReadQuantityMutation();

  useEffect(() => {
    if (isSuccessReadQuantity) {
      setAvailableQuantities(dataReadQuantity[0].quantity);
      setDiscountedAmount(dataReadQuantity[0].discountedAmount);
      setDiscountedPercent(dataReadQuantity[0].discountPercentage);
    }

    if (isErrorReadQuantity) {
      addToast(errorReadQuantity?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessReadQuantity, isErrorReadQuantity]);

  useEffect(() => {
    if (isSuccessReadSizeColorQuantity) {
      setAvailableColors(dataReadSizeColorQuantity);
    }

    if (isErrorReadSizeColorQuantity) {
      addToast(errorReadSizeColorQuantity?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessReadSizeColorQuantity, isErrorReadSizeColorQuantity]);

  const bookList = useSelector((state) => state.book.bookList);

  const cardSize = useMemo(() => {
    const size = bookList?.find((v, i) => {
      return i === index;
    }).size;

    return size;
  }, [bookList]);

  const cardColor = useMemo(() => {
    const color = bookList?.find((v, i) => {
      return i === index;
    }).color;

    return color;
  }, [bookList]);

  const cardQuantity = useMemo(() => {
    const quantity = bookList?.find((v, i) => {
      return i === index;
    }).quantity;

    return quantity;
  }, [bookList, cardSize, cardColor]);

  const costValue = () => {
    // if (Number(discountPercentage)) {
    //   return cardQuantity * Number(discountedAmount) || 0;
    // } else if (Number(productDetails?.discountPercentage)) {
    //   return cardQuantity * Number(productDetails.discountedPrice) || 0;
    // } else {
    //   return cardQuantity * productDetails.costPrice;
    // }
    return cardQuantity * Number(discountedAmount) || 0;
  };

  useEffect(() => {
    if (isSucessProductDetails) {
      setTotalCostList((totalCostList) => {
        const uniqueIdAmount = uniqueIdAmountSum(totalCostList, {
          id: productDetails.id,
          value: costValue(),
        });

        return uniqueIdAmount;
      }, []);
    }
  }, [isSucessProductDetails, discountedAmount, cardQuantity]);

  useEffect(() => {
    if (cardSize && productDetails?.id) {
      readSizeColorQuantity({
        body: { size: cardSize },
        id: productDetails?.id,
      });

      setDiscountedAmount("");
      setDiscountedPercent("");
    }
  }, [cardSize, productDetails]);

  useEffect(() => {
    if (cardColor && productDetails?.id && cardSize) {
      readQuantity({
        body: { size: cardSize, color: cardColor },
        id: productDetails?.id,
      });
      setDiscountedAmount("");
      setDiscountedPercent("");
    }
  }, [cardColor, cardSize, productDetails]);

  return (
    <div className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5">
      <div className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0">
        {changeVariantOpen && (
          <Popup
            title={`Are you sure you want to change ${changeMessage} ?`}
            onOkClick={() => {
              changeAvailableOptions();
              setChangeVariantOpen(false);
            }}
            onCancelClick={() => setChangeVariantOpen(false)}
            okText="Yes"
            cancelText="No"
          />
        )}

        <img
          src={
            productDetails?.productImage?.imageUrl ||
            productDetails?.productImages?.[0]?.imageUrl
          }
          alt="image description"
          className="w-full h-[110px] object-contain"
          onClick={() => {
            router.push(`/product-listing/${productDetails?.id}`);
          }}
        />
      </div>
      <div className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[20px]">
        <div className="flex justify-between">
          <div>
            <h1
              className="h5 mb-1 hover:text-primary"
              onClick={() => {
                router.push(`/product-listing/${productDetails?.id}`);
              }}
            >
              {productDetails?.productTitle}
            </h1>
          </div>
          <ActualPrice
            discountedAmount={discountedAmount}
            costPrice={productDetails?.costPrice}
            cardQuantity={cardQuantity}
          />
        </div>

        <div className="flex flex-wrap space-x-3">
          <div className="w-full cart-select">
            <div className="row mt-3">
              <div className="three-col">
                <SelectWithoutForm
                  className={`${
                    `${productAvailableSize?.length}` === `0`
                      ? "disabled-input"
                      : ""
                  } w-24`}
                  label="Size"
                  value={cardSize}
                  disabled={disableOneOf(
                    `${productDetails?.sizeValue?.length}` === `0`
                  )}
                  onChange={(e) => {
                    const bookl1 = bookList.map((obj, i) => {
                      if (i === index) {
                        return {
                          ...obj,
                          size: e.target.value,
                          color: "",
                          quantity: "",
                        };
                      } else {
                        return obj;
                      }
                    });
                    setChangeVariantOpen(true);
                    setChangeMessage(() => {
                      return "size";
                    });

                    const changeSize = () => {
                      dispatch(setBookList(bookl1));
                    };

                    setChangeAvailableOption(() => {
                      return changeSize;
                    });
                  }}
                >
                  <option value="" selected disabled={true}>
                    Select Size
                  </option>

                  {productAvailableSize?.map((size, i) => (
                    <option key={i} value={size}>
                      {size}
                    </option>
                  ))}
                </SelectWithoutForm>
              </div>

              <div className="three-col">
                <SelectWithoutForm
                  className={`${
                    disableOneOf(
                      !cardSize || `${avalilableColors?.length}` === `0`
                    )
                      ? "disabled-input"
                      : ""
                  } w-24`}
                  label="Color"
                  value={cardColor}
                  disabled={disableOneOf(
                    !cardSize || `${avalilableColors?.length}` === `0`
                  )}
                  onChange={(e) => {
                    const bookl1 = bookList.map((obj, i) => {
                      if (i === index) {
                        return {
                          ...obj,
                          color: e.target.value,
                          quantity: "",
                        };
                      } else {
                        return obj;
                      }
                    });

                    setChangeVariantOpen(true);
                    setChangeMessage(() => {
                      return "color";
                    });

                    const changeSize = () => {
                      dispatch(setBookList(bookl1));
                    };

                    setChangeAvailableOption(() => {
                      return changeSize;
                    });
                  }}
                >
                  <option value="" selected disabled={true}>
                    Select Color
                  </option>

                  {avalilableColors?.map?.((color, i) => (
                    <option key={i} value={color}>
                      {color}
                    </option>
                  ))}
                </SelectWithoutForm>
              </div>

              <div className="three-col">
                <SelectWithoutForm
                  className={`${
                    disableOneOf(!cardSize, !cardColor) ? "disabled-input" : ""
                  } w-24`}
                  label="Quantity"
                  value={cardQuantity}
                  disabled={disableOneOf(!cardSize, !cardColor)}
                  onChange={(e) => {
                    const bookl1 = bookList.map((obj, i) => {
                      if (i === index) {
                        return {
                          ...obj,
                          quantity: e.target.value,
                        };
                      } else {
                        return obj;
                      }
                    });

                    setChangeVariantOpen(true);
                    setChangeMessage(() => {
                      return "quantity";
                    });

                    const changeSize = () => {
                      dispatch(setBookList(bookl1));
                    };

                    setChangeAvailableOption(() => {
                      return changeSize;
                    });
                  }}
                >
                  <option value="" selected disabled={true}>
                    Select Quantity
                  </option>

                  {nNumberArray(productQuantity).map((quantity, i) => (
                    <option key={i} value={quantity}>
                      {quantity}
                    </option>
                  ))}
                </SelectWithoutForm>
                {Number(cardQuantity) ? null : (
                  <div className="mt-1 text-error">
                    To order please select quantity
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookCard;
