import React from "react";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";

const PriceCom = ({
  productCostPrice,
  discountedAmount,
  productDiscountedPrice,
}) => {
  return (
    <div>
      {!`${discountedAmount}` ? (
        <div className="text-primary-dark font-semibold">
          <span className="line-through text-error">
            NRs.&nbsp;
            {thousandNumberSeparator(productCostPrice)}
          </span>
          <span>
            &nbsp;NRs. &nbsp;
            {thousandNumberSeparator(productDiscountedPrice)}
          </span>
        </div>
      ) : Number(discountedAmount) !== Number(productCostPrice) ? (
        <div className="text-primary-dark font-semibold">
          <span className="line-through text-error">
            NRs.&nbsp;
            {thousandNumberSeparator(productCostPrice)}
          </span>
          <span>
            &nbsp;NRs. &nbsp;
            {thousandNumberSeparator(discountedAmount)}
          </span>
        </div>
      ) : (
        <div className="text-primary-dark font-semibold">
          <span>
            NRs. &nbsp;
            {thousandNumberSeparator(productCostPrice)}
          </span>
        </div>
      )}
    </div>
  );
};

export default PriceCom;
