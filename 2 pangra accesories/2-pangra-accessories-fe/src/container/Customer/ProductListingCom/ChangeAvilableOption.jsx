import Button from "components/Button";
import React from "react";

const ChangeAvailableOption = ({
  message,
  closeChangeVariantModal,
  changeAvailableOption,
}) => {
  return (
    <div>
      <div>{message}</div>
      <Button
        onClick={() => {
          changeAvailableOption();
          closeChangeVariantModal();
        }}
      >
        ok
      </Button>
      <Button
        onClick={() => {
          closeChangeVariantModal();
        }}
      >
        Cancel
      </Button>
    </div>
  );
};

export default ChangeAvailableOption;
