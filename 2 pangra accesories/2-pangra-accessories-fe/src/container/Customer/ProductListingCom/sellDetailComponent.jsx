/* eslint-disable array-callback-return */
import "react-accessible-accordion/dist/fancy-example.css";
import Head from "next/head";
import {
  CheckCircleIcon,
  // EyeIcon,
  CalendarIcon,
  // ArchiveIcon,
  // BadgeCheckIcon,
  DotsVerticalIcon,
} from "@heroicons/react/outline";
import ImgGallery from "components/ImgGallery";
import { useEffect, useMemo, useState } from "react";
import { useRouter } from "next/router";
import {
  useAddToBuyingListProductMutation,
  useAddToCartProductMutation,
  useReadSingleProductQuery,
  useReadSingleRecommendedProductQuery,
  useReadRatingReviewQuery,
  useReadRatingStatusReviewQuery,
} from "services/api/seller";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";

import CustomTabs from "components/Tabs";
import ReactAccordions from "components/Accordions/ReactAccordions";
import Button from "components/Button";
import { useDispatch, useSelector } from "react-redux";
import ReactModal from "components/ReactModal/ReactModal";
import Comment from "components/Comment";

import { useToasts } from "react-toast-notifications";
import CustomerLoginPopUp from "components/LoginPopUp";
import DOMPurify from "dompurify";

import SelectWithoutForm from "components/Select/SelectWithoutForm";
import { setBookList } from "store/features/book/BookSlice";
import { nNumberArray } from "utils/nNumberOfArray";
import RatingReview from "container/RatingReview";
import { valueSeparatedByComma } from "utils/valueSeparatedByComma";
import ReactStars from "react-rating-stars-component";
import {
  useReadQuantityMutation,
  useReadSizeColorQuantityMutation,
} from "services/api/customer";

import "react-medium-image-zoom/dist/styles.css";
import Zoom from "react-medium-image-zoom";
import { disableOneOf } from "utils/disableOneOf";
import { productQuantity } from "constant/constant";
import PriceCom from "./PriceCom";
import Link from "next/link";

function SellDetailComponent() {
  const dispatch = useDispatch();

  // eslint-disable-next-line no-unused-vars
  const [bookDetails, setBookDetails] = useState({
    color: "",
    size: "",
    quantity: "",
    id: "",
  });

  const [avalilableColors, setAvailableColors] = useState();
  // eslint-disable-next-line no-unused-vars
  const [avalilableQuanties, setAvailableQuantities] = useState();
  // eslint-disable-next-line no-unused-vars
  const [discountedAmount, setDiscountedAmount] = useState("");
  const [discountPercentage, setDiscountedPercent] = useState();

  const [sizeValue, setSizeValue] = useState();
  const [colorValue, setColorValue] = useState();
  const [quantityValue, setQuantityValue] = useState();

  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;

  const router = useRouter();
  const productId = router?.query?.id;
  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const handleCartList = (query, token) => {
    const body = {
      colors: colorValue,
      sizeValues: sizeValue,
      quantity: quantityValue,
    };
    if (token) {
      addToCartList({ productId: query, body: body });
    } else {
      setOpenModal(true);
    }
  };
  // eslint-disable-next-line no-unused-vars
  const handleBuyingList = (query, token) => {
    if (token) {
      addToBuyingList(query);
    } else {
      // router.push('/customer/login');
      // setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const [addToBuyingList, { isLoading: isLoadingAddBuyingList }] =
    useAddToBuyingListProductMutation();

  const [
    addToCartList,
    {
      isLoading: isLoadingAddToCartList,
      isError: isErrorAddToCartList,
      isSuccess: isSuccessAddToCartList,
      error: addToWishListError,
      data: success,
    },
  ] = useAddToCartProductMutation();

  const [
    readSizeColorQuantity,
    {
      // isLoading: isLoadingReadSizeColorQuantity,
      isError: isErrorReadSizeColorQuantity,
      isSuccess: isSuccessReadSizeColorQuantity,
      error: errorReadSizeColorQuantity,
      data: dataReadSizeColorQuantity,
    },
  ] = useReadSizeColorQuantityMutation();

  // console.log('readSizeColorQuantity')

  const [
    readQuantity,
    {
      // isLoading: isLoadingReadQuantity,
      isError: isErrorReadQuantity,
      isSuccess: isSuccessReadQuantity,
      error: errorReadQuantity,
      data: dataReadQuantity,
    },
  ] = useReadQuantityMutation();

  useEffect(() => {
    if (isSuccessReadQuantity) {
      setAvailableQuantities(dataReadQuantity[0].quantity);
      setDiscountedAmount(dataReadQuantity[0].discountedAmount);
      setDiscountedPercent(dataReadQuantity[0].discountPercentage);
    }

    if (isErrorReadQuantity) {
      addToast(errorReadQuantity?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessReadQuantity, isErrorReadQuantity]);

  // const { data: brandDataAll, isFetching: isLoadingAll } =
  //   useGetSizeColorQuantityQuery("");

  // const {
  //   data: shippingAddressData,
  //   isError: shippingAddressIsError,
  //   isFetching: shippingAddressIsFetching,
  //   refetch,
  // } = useReadShippingAddressQuery();

  // useGetSizeColorQuantityQuery,

  useEffect(() => {
    if (isSuccessReadSizeColorQuantity) {
      setAvailableColors(dataReadSizeColorQuantity);
    }

    if (isErrorReadSizeColorQuantity) {
      addToast(errorReadSizeColorQuantity?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessReadSizeColorQuantity, isErrorReadSizeColorQuantity]);

  const { data: getAllReview } = useReadRatingReviewQuery(productId, {
    skip: !productId,
  });

  const { data: getAllsReviewStatus } = useReadRatingStatusReviewQuery();

  useEffect(() => {
    if (isSuccessAddToCartList) {
      addToast(success.message || "Product added to cart successfully.", {
        appearance: "success",
      });
      // router.push("/cart");
    }

    if (isErrorAddToCartList) {
      addToast(addToWishListError?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAddToCartList, isErrorAddToCartList, addToWishListError]);

  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };
  const [modal1, setModal1] = useState(false);
  const setModalFunction1 = (value) => {
    setModal1(value);
  };
  const [tab1, setTab1] = useState([]);

  const AccordionItems = [
    {
      uuid: 1,
      heading: "Detailed Description",
      content: (
        <div>
          <CustomTabs tabData={tab1} hasAccordion={false} />
        </div>
      ),
    },
  ];

  const [imageData, setImageData] = useState([]);

  const { data: productDetails, isSuccess: isSuccessProduct } =
    useReadSingleProductQuery(router.query.id, {
      skip: !router.query.id,
    });

  const productAvailableSize = useMemo(() => {
    const productAvailableSizeValue = productDetails?.availableOptions?.map?.(
      (value, i) => value.size
    );
    const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
    return uniqueAvailableSizeValue;
  }, [productDetails]);

  useEffect(() => {
    setBookDetails({
      color: productDetails?.color?.[0],
      quantity: productDetails?.quantity ? "1" : "0",
      size: productDetails?.sizeValue?.[0],

      id: productDetails?.id,
    });
  }, [isSuccessProduct]);

  const { data: sellerData } = useReadSingleRecommendedProductQuery();

  useEffect(() => {
    if (productDetails) {
      const images = [
        {
          original: productDetails?.productImage?.imageUrl,
          thumbnail: productDetails?.productImage?.imageUrl,
        },
      ];
      // Product Images
      const moreImages = productDetails?.productImages.map((img) => ({
        original: img.imageUrl,
        thumbnail: img.imageUrl,
      }));
      // First thumbnail will be shown in the gallery, then other images.

      setImageData([...images, ...moreImages] || []);

      setTab1([
        {
          heading: "Basic Details",
          list: [
            {
              label: "Brand Name",
              data: `${
                productDetails?.brandName
                  ? productDetails?.brandName
                  : "Not Available"
              }`,
            },
            {
              label: "Category Name",
              data: `${
                productDetails?.categoryName
                  ? productDetails?.categoryName
                  : "Not Available"
              }`,
            },
            {
              label: "Sub Category Name",
              data: `${
                productDetails?.subCategoryName
                  ? productDetails?.subCategoryName
                  : "Not Available"
              }`,
            },
            {
              label: "Model Name",
              data: `${
                productDetails?.modelName
                  ? productDetails?.modelName
                  : "Not Available"
              }`,
            },
            {
              label: "Colors",
              data: `${
                productDetails?.color?.length
                  ? valueSeparatedByComma(productDetails?.color)
                  : "Not Available"
              }`,
            },

            {
              label: "Cost Price (NRs.)",
              data: `${
                productDetails?.costPrice
                  ? "NRs. " +
                    thousandNumberSeparator(productDetails?.costPrice) +
                    " /-"
                  : "Not Available"
              }`,
            },
            {
              label: "Discounted price (NRs.)",
              data: `${
                productDetails?.discountedPrice
                  ? "NRs. " +
                    thousandNumberSeparator(productDetails?.discountedPrice) +
                    " /-"
                  : "Not Available"
              }`,
            },

            // {
            //   label: "Quantity",
            //   data: `${
            //     productDetails?.quantity
            //       ? productDetails?.quantity
            //       : "Not Available"
            //   }`,
            // },
            {
              label: "Weight",
              data: `${
                productDetails?.weight
                  ? productDetails?.weight
                  : "Not Available"
              }`,
            },
          ],
        },
        {
          heading: "Other Details",
          list: [
            {
              label: "Product Title",
              data: `${
                productDetails?.productTitle
                  ? productDetails?.productTitle
                  : "Not Available"
              }`,
            },
            // {
            //   label: "Number of Views",
            //   data: `${
            //     productDetails?.numViews
            //       ? productDetails?.numViews
            //       : "Not Available"
            //   }`,
            // },
            {
              label: "Highlight",
              data: `${
                productDetails?.highLight
                  ? productDetails?.highLight
                  : "Not Available"
              }`,
            },
            {
              label: "Warranty",
              data: productDetails?.hasWarranty ? "Yes" : "No",
            },
            {
              label: "Warranty Time",
              data: `${
                productDetails?.warrantyTime
                  ? productDetails?.warrantyTime
                  : "Not Available"
              }`,
            },

            {
              label: "Returnable",
              data: productDetails?.isReturnable ? "Yes" : "No",
            },
            {
              label: "Returnable Time",
              data: `${
                productDetails?.returnableTime
                  ? productDetails?.returnableTime
                  : "Not Available"
              }`,
            },
            {
              label: "Flash Sale",
              data: productDetails?.isForFlashSale ? "Yes" : "No",
            },
            // {
            //   label: "OnStock",
            //   data: productDetails?.outOfStock ? "Yes" : "No",
            // },

            // {
            //   label: "SKU",
            //   data: `${
            //     productDetails?.SKU ? productDetails?.SKU : "Not Available"
            //   }`,
            // },
            {
              label: "Breadth (mm)",
              data: `${
                productDetails?.dimension.breadth
                  ? productDetails?.dimension.breadth
                  : "Not Available"
              }`,
            },
            {
              label: "Height (mm)",
              data: `${
                productDetails?.dimension.height
                  ? productDetails?.dimension.height
                  : "Not Available"
              }`,
            },
            {
              label: "Length (mm)",
              data: `${
                productDetails?.dimension.length
                  ? productDetails?.dimension.length
                  : "Not Available"
              }`,
            },
            // {
            //   label: "Slugs",
            //   data: `${
            //     productDetails?.tags ? productDetails?.tags : "Not Available"
            //   }`,
            // },
          ],
        },
      ]);
    }
  }, [productDetails]);

  const isProductRatedByCustomer = () => {
    const isRated = getAllReview?.ratingOfProducts.some((review, items) => {
      return loginInfo?.customer?._id === review?.createdBy;
    });
    return isRated;
  };

  // const IsRatingApproved = (ratingOfProducts) => {};

  return (
    <div>
      <Head>
        <meta name="description" content={productDetails?.tags?.join?.(",")} />
      </Head>

      <main id="main">
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}
        <section className="vehicle-detail-section py-3">
          <div className="container">
            <div className="md:flex flex-wrap items-center justify-end mb-5  ">
              {/* *** dont delete */}
              {getAllReview?.averageRating === "NaN" ? null : (
                <div>
                  {getAllReview?.averageRating !== undefined && (
                    <ReactStars
                      size={25}
                      count={5}
                      color="gray"
                      activeColor="#A7EB4A"
                      value={getAllReview?.averageRating}
                      a11y={true}
                      isHalf={true}
                      edit={false}
                    />
                  )}
                </div>
              )}

              <div className="flex flex-wrap  mr-2 md:max-w-[75%] space-x-3 sm:space-x-6 items-center">
                <div className="rating flex space-x-2 ">
                  {/* <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-light fill-current" />
                  <StarIcon className="w-3 h-3 sm:w-4 sm:h-4 text-secondary-dark" /> */}
                  {/* <Rate /> */}
                </div>

                {/* <span>
                  <a
                    href="#"
                    className="text-sm text-textColor hover:text-primary"
                  >
                    1 Reviews
                  </a>
                </span> */}

                {/* <a
                    href="#"
                    className="text-sm text-textColor hover:text-primary"
                  >
                    Write a review
                  </a> */}
                {token &&
                  !isProductRatedByCustomer() &&
                  getAllsReviewStatus?.accessoriesId.includes(
                    router.query.id
                  ) && (
                    <a className="text-blue hover:text-primary px-4 py-[7px]">
                      <ReactModal link="Write a review" modal={modal1}>
                        <RatingReview
                          setModalFunction={setModalFunction1}
                          modal={modal1}
                          type="add"
                        />
                      </ReactModal>
                    </a>
                  )}
              </div>
              <div className="flex space-x-6 max-w-[290px] text-sm">
                {productDetails?.sellerValue?.isVerified ? (
                  <span>
                    <CheckCircleIcon className="w-4 text-secondary-dark mt-[-3px] inline-block mr-1" />
                    Verified
                  </span>
                ) : null}

                {productDetails?.sellerValue?.updatedAt && (
                  <span className="tooltip-parent group">
                    <span className="tooltip">Created At</span>
                    <CalendarIcon className="w-4 text-secondary-dark mt-[-3px] inline-block mr-1 " />
                    {new Date(
                      productDetails?.sellerValue?.updatedAt
                    ).toLocaleDateString()}
                  </span>
                )}
              </div>
            </div>
            <div className="row-sm">
              <div className="w-full lg:w-[55%] two-col-sm mb-5">
                <ImgGallery
                  images={imageData}
                  discountPercentage={
                    discountPercentage || productDetails?.discountPercentage
                  }
                />

                <div className=" lg:hidden bg-white border border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                  <div className="flex flex-wrap justify-between items-center pb-4 mb-5 border border-t-0 border-l-0 border-r-0 border-b-gray-300">
                    <div>
                      <span className="text-xl mb-1 inline-block">Price</span>

                      <h2 className="text-[18px] sm:h4 text-primary-dark">
                        NRs.{" "}
                        {productDetails?.costPrice &&
                          thousandNumberSeparator(productDetails?.costPrice)}
                      </h2>
                    </div>
                  </div>
                  <div>
                    <h3 className="h5 text-textColor mb-5">Product Summary</h3>
                    {productDetails?.sizeChart?.imageUrl ? (
                      <div>
                        <Zoom zoomMargin={40}>
                          <span className="chart-label">Size Chart </span>
                          <img
                            className=" inline-block"
                            src={productDetails?.sizeChart?.imageUrl}
                            height="100px"
                            width="100px"
                          ></img>
                        </Zoom>
                      </div>
                    ) : null}

                    <div className="selection-holder flex flex-wrap items-end justify-end ">
                      <div className="select-holder row-sm text-left py-5  ">
                        <div className="four-col-sm mb-0">
                          <SelectWithoutForm
                            className={`${
                              `${productAvailableSize?.length}` === `0`
                                ? "disabled-input"
                                : ""
                            }`}
                            value={sizeValue}
                            label="Size"
                            disabled={disableOneOf(
                              `${productAvailableSize?.length}` === `0`
                            )}
                            onChange={(e) => {
                              setSizeValue(e.target.value);
                              setColorValue("");
                              setQuantityValue("");
                              setDiscountedAmount("");
                              setDiscountedPercent("");
                              readSizeColorQuantity({
                                body: { size: e.target.value },
                                id: router.query.id,
                              });
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Size
                            </option>

                            {productAvailableSize?.map((data, i) => (
                              <option key={i} value={data}>
                                {data}
                              </option>
                            ))}
                          </SelectWithoutForm>
                        </div>

                        <div className="two-col-sm !w-[40%] mb-0">
                          <SelectWithoutForm
                            className={`${
                              disableOneOf(
                                !sizeValue,
                                `${avalilableColors?.length}` === `0`
                              )
                                ? "disabled-input"
                                : ""
                            }`}
                            value={colorValue}
                            label="Color"
                            disabled={disableOneOf(
                              !sizeValue,
                              `${avalilableColors?.length}` === `0`
                            )}
                            onChange={(e) => {
                              setColorValue(e.target.value);
                              setQuantityValue("");
                              readQuantity({
                                body: {
                                  size: sizeValue,
                                  color: e.target.value,
                                },
                                id: router.query.id,
                              });
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Color
                            </option>

                            {avalilableColors?.map?.((data, i) => (
                              <option key={i} value={data}>
                                {data}
                              </option>
                            ))}
                          </SelectWithoutForm>
                        </div>
                        <div className="two-col-sm !w-[35%] mb-0">
                          <SelectWithoutForm
                            className={`${
                              disableOneOf(!sizeValue, !colorValue)
                                ? "disabled-input"
                                : ""
                            }`}
                            value={quantityValue}
                            label="Quantity"
                            disabled={disableOneOf(!sizeValue, !colorValue)}
                            onChange={(e) => {
                              setQuantityValue(e.target.value);
                            }}
                          >
                            <option value="" selected disabled={true}>
                              Quantity
                            </option>

                            {nNumberArray(productQuantity).map((data, i) => (
                              <option key={i} value={data}>
                                {data}
                              </option>
                            ))}
                          </SelectWithoutForm>
                        </div>
                      </div>
                      <div className="button-holder  py-5">
                        {productDetails?.sellerValue?.isSold ? (
                          <Button type="button" disabled>
                            Sold Out
                          </Button>
                        ) : (
                          <>
                            <Button
                              type="button"
                              onClick={() => {
                                handleCartList(router.query.id, token);
                              }}
                              loading={isLoadingAddToCartList}
                              disabled={disableOneOf(!quantityValue)}
                            >
                              Add to cart
                            </Button>

                            <Button
                              type="button"
                              variant="outlined"
                              onClick={() => {
                                dispatch(
                                  setBookList([
                                    {
                                      color: colorValue,
                                      quantity: quantityValue,
                                      size: sizeValue,
                                      id: productDetails?.id,
                                      availableSize: productAvailableSize,
                                    },
                                  ])
                                );
                                router.push("/customer/book");
                              }}
                              loading={isLoadingAddBuyingList}
                              disabled={disableOneOf(!quantityValue)}
                            >
                              Buy Now
                            </Button>
                          </>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                {sellerData?.length ? (
                  <div className="rounded-md p-5 mb-4 mt-10">
                    <h3 className="h5 mb-5">Product You may like</h3>
                    <div className="vechile-listing-horizontal space-y-6">
                      {sellerData?.map((sellInfo, i) => {
                        // console.log("sellInfo", sellInfo);
                        return (
                          <div
                            key={i}
                            className="list flex flex-wrap border-b-2 border-gray-200 pb-1 max-w-[365px] sm:max-w-[550px] mx-auto last-of-type:border-0"
                          >
                            <figure className="w-full sm:w-[190px] mb-2 sm:mb-0 ">
                              <Link href={`/product-listing/${sellInfo.id}`}>
                                <a className="w-full">
                                  <img
                                    src={sellInfo?.productImage?.imageUrl}
                                    alt="image description"
                                    className="h-[220px] sm:h-[140px] w-full object-cover"
                                  />
                                </a>
                              </Link>
                            </figure>
                            <div className="detail-holder flex-1 pl-3">
                              <div className="flex flex-wrap justify-between items-center pb-4 mb-2">
                                <div>
                                  <span className="inline-block">
                                    <Link
                                      href={`/product-listing/${sellInfo.id}`}
                                    >
                                      <a className="vehicle-name text-textColor">
                                        {" "}
                                        {sellInfo?.productTitle}
                                      </a>
                                    </Link>
                                  </span>
                                  <h4 className="text-lg sm:h5 text-primary-dark">
                                    NRs.{" "}
                                    {thousandNumberSeparator(
                                      sellInfo?.costPrice
                                    )}
                                  </h4>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                ) : null}

                {/* <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 mb-4 shadow-lg">
                  <h3 className="h5 mb-3">Rating and reviews</h3>
                  

                </div> */}

                {/* {

                  IsRatingApproved(ratingOfProducts)
                } */}

                {!getAllReview?.ratingOfProducts.length ? null : (
                  <div className="border bg-white bg-opacity-75 border-gray-300 rounded-md p-5 mb-4 shadow-lg relative">
                    <div className="average-rating mb-3">
                      <h3 className="h5 mb-3">
                        Rating And Reviews{" "}
                        <div className="flex items-center">
                          <h3 className="h6 mr-3">
                            {getAllReview?.averageRating === "NaN" ? (
                              // <div> {getAllReview?.averageRating}/5</div>
                              <div> 0/5</div>
                            ) : (
                              // <div> 0/5</div>
                              <div> {getAllReview?.averageRating}/5</div>
                            )}
                          </h3>
                          {getAllReview?.averageRating !== undefined && (
                            <ReactStars
                              size={25}
                              count={5}
                              color="gray"
                              activeColor="#A7EB4A"
                              value={getAllReview?.averageRating}
                              a11y={true}
                              isHalf={true}
                              edit={false}
                            />
                          )}
                        </div>
                      </h3>
                    </div>
                    <h3 className="h5 mb-3">
                      {getAllReview?.numberOfRating} Rating
                    </h3>
                    <div className="vechile-listing-horizontal space-y-6 md:space-y-3">
                      {getAllReview?.ratingOfProducts?.map?.((review, i) => {
                        if (review.status) {
                          return (
                            <div
                              key={i}
                              className="list flex flex-wrap border-b-2 border-gray-300 pb-1  max-w-[365px] sm:max-w-[600px] mx-auto last-of-type:border-0 relative"
                            >
                              <div className="detail-holder flex-1 pl-3">
                                {/* <h3 className="h5 mb-3">Product Reviews</h3> */}
                                <div className="flex flex-wrap justify-between items-center pb-4 mb-2">
                                  <div className="rating-holder">
                                    <h3 className="text-sm">
                                      <span className="h6 mb-3">by </span>
                                      {review?.fullName}
                                    </h3>
                                    {/* {review?.status ? (
                                      <div className="verified inline-block text-[8px] px-2 py-[1px] w-auto bg-primary-dark text-white ">
                                        <BadgeCheckIcon className="w-4 h-4 inline-block mt-[-2px] mr-1" />
                                        Approved
                                      </div>
                                    ) : null} */}
                                    <ReactStars
                                      size={25}
                                      count={5}
                                      color="gray"
                                      activeColor="#A7EB4A"
                                      value={review.ratingNumber}
                                      a11y={true}
                                      isHalf={true}
                                      edit={false}
                                    />
                                    <span> {review?.ratingComment}</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        } else {
                          if (loginInfo?.customer?._id === review?.createdBy) {
                            return (
                              <div
                                key={i}
                                className="list flex flex-wrap border-b-2 border-gray-300 pb-1  max-w-[365px] sm:max-w-[600px] mx-auto last-of-type:border-0 relative"
                              >
                                <div className="detail-holder flex-1 pl-3">
                                  {/* <h3 className="h5 mb-3">Product Reviews</h3> */}
                                  <div className="flex flex-wrap justify-between items-center pb-4 mb-2">
                                    <div>
                                      <h3 className="text-sm">
                                        <span className="h6 mb-3">By: </span>
                                        {review?.fullName}
                                      </h3>
                                      {/* {review?.status ? (
                                        <div className="verified inline-block text-[13px] px-3 py-[2px] w-auto bg-primary-dark text-white absolute top-2 left-2 rounded-r-md">
                                          <BadgeCheckIcon className="w-4 h-4 inline-block mt-[-2px] mr-1" />
                                          Verified
                                        </div>
                                      ) : null} */}
                                      <ReactStars
                                        size={25}
                                        count={5}
                                        color="gray"
                                        activeColor="#A7EB4A"
                                        value={review.ratingNumber}
                                        a11y={true}
                                        isHalf={true}
                                        edit={false}
                                      />
                                      <span> {review?.ratingComment}</span>
                                      {loginInfo?.customer?._id ===
                                      review?.createdBy ? (
                                        <div>
                                          <div className="absolute  top-[1px] right-[46px] rounded p-1">
                                            <span className="review-pending">
                                              Pending
                                            </span>
                                          </div>
                                          <div className="absolute right-3 top-[13px] group hover:cursor-pointer">
                                            <DotsVerticalIcon className="h-4" />
                                            <div className="px-4 py-3 rounded-lg bg-white border-[1px] border-gray-200 shadow-lg w-[110px] absolute top-5 right-0 opacity-0 z-10 invisible group-hover:opacity-100 group-hover:visible transition-all">
                                              <ReactModal
                                                link="Edit"
                                                modal={modal1}
                                              >
                                                <RatingReview
                                                  setModalFunction={
                                                    setModalFunction1
                                                  }
                                                  modal={modal1}
                                                  id={review?._id}
                                                  type="edit"
                                                />
                                              </ReactModal>
                                            </div>
                                          </div>
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          }
                        }
                      })}
                    </div>
                  </div>
                )}
              </div>

              <div className="w-full lg:w-[45%] two-col-sm mb-5">
                <div className="p-5 mb-5">
                  <div className="mb-4">
                    <h1 className="h3 mb-1">{productDetails?.productTitle}</h1>
                    <div className="flex justify-between">
                      <span className="price text-lg font-semibold text-[#367910]">
                        <PriceCom
                          discountedAmount={discountedAmount}
                          productCostPrice={productDetails?.costPrice}
                          productDiscountedPrice={
                            productDetails?.discountedPrice
                          }
                        ></PriceCom>
                      </span>
                      <div className="">
                        {productDetails?.sizeChart?.imageUrl ? (
                          <div>
                            <Zoom zoomMargin={40}>
                              <span className="chart-label">Size Chart </span>
                              <img
                                className=" inline-block"
                                src={productDetails?.sizeChart?.imageUrl}
                                height="100px"
                                width="100px"
                              ></img>
                            </Zoom>
                          </div>
                        ) : null}
                      </div>
                    </div>
                  </div>
                  {productDetails?.description && (
                    <div>
                      <div
                        className="text-sm"
                        dangerouslySetInnerHTML={{
                          __html: DOMPurify.sanitize(
                            productDetails?.description
                          ),
                        }}
                      ></div>
                    </div>
                  )}
                </div>
                <div className="detail-page-accordion-holder">
                  <ReactAccordions
                    AccordionItems={AccordionItems}
                  ></ReactAccordions>
                  <div>
                    <Comment ProductId={productDetails?.sellerValue?.id} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="add-to-cart-sticky fixed left-0 right-0 bottom-0 min-h-[80px] border border-gray-300 border-b-0 border-l-0 borde-r-0 shadow-md bg-white z-20 pb-[46px] md:pb-0">
            <div className="container">
              <div className="flex flex-wrap justify-between items-center ">
                <div className="w-full md:w-[310px] md:pr-3 product-name py-2 md:py-5">
                  <h5 className="text-base md:text-[18px]">
                    {productDetails?.productTitle}
                  </h5>
                  <span className="price text-sm md:text-lg font-semibold text-[#367910]">
                    <PriceCom
                      discountedAmount={discountedAmount}
                      productCostPrice={productDetails?.costPrice}
                      productDiscountedPrice={productDetails?.discountedPrice}
                    ></PriceCom>
                  </span>
                </div>
                <div className="selection-holder flex flex-wrap md:flex-nowrap items-end flex-1 ">
                  <div className="select-holder w-full md:w-auto row-sm text-left md:py-4 md:px-5 md:pl-7 mx-auto flex-1 ">
                    <div className="w-[26%] four-col-sm mb-0 md:!w-[20%]">
                      <SelectWithoutForm
                        className={`${
                          `${productAvailableSize?.length}` === `0`
                            ? "disabled-input"
                            : ""
                        }`}
                        value={sizeValue}
                        label="Size"
                        disabled={disableOneOf(
                          `${productAvailableSize?.length}` === `0`
                        )}
                        // onChange={(e) => {
                        //   setBookDetails({
                        //     ...bookDetails,
                        //     size: e.target.value,
                        //   });
                        // }}
                        onChange={(e) => {
                          setSizeValue(e.target.value);
                          setColorValue("");
                          setQuantityValue("");
                          setDiscountedAmount("");
                          setDiscountedPercent("");
                          readSizeColorQuantity({
                            body: { size: e.target.value },
                            id: router.query.id,
                          });
                        }}
                      >
                        <option value="" selected disabled={true}>
                          Size
                        </option>
                        {/* {productDetails?.sizeValue?.map((data, i) => (
                          <option key={i} value={data}>
                            {data}
                          </option>
                        ))} */}
                        {productAvailableSize?.map((data, i) => (
                          <option key={i} value={data}>
                            {data}
                          </option>
                        ))}
                      </SelectWithoutForm>
                    </div>
                    <div className="w-[37%] two-col-sm md:!w-[40%] mb-0">
                      <SelectWithoutForm
                        className={`${
                          disableOneOf(
                            !sizeValue,
                            `${avalilableColors?.length}` === `0`
                          )
                            ? "disabled-input"
                            : ""
                        }`}
                        value={colorValue}
                        label="Color"
                        disabled={disableOneOf(
                          !sizeValue,
                          `${avalilableColors?.length}` === `0`
                        )}
                        onChange={(e) => {
                          // setBookDetails({
                          //   ...bookDetails,
                          //   color: e.target.value,
                          // });
                          setColorValue(e.target.value);
                          setQuantityValue("");
                          readQuantity({
                            body: {
                              size: sizeValue,
                              color: e.target.value,
                            },
                            id: router.query.id,
                          });
                        }}
                      >
                        <option value="" selected disabled={true}>
                          Color
                        </option>

                        {avalilableColors?.map?.((data, i) => (
                          <option key={i} value={data}>
                            {data}
                          </option>
                        ))}
                      </SelectWithoutForm>
                    </div>
                    <div className="w-[37%] two-col-sm md:!w-[35%] mb-0">
                      <SelectWithoutForm
                        className={`${
                          disableOneOf(!sizeValue, !colorValue)
                            ? "disabled-input"
                            : ""
                        }`}
                        value={quantityValue}
                        label="Quantity"
                        disabled={disableOneOf(!sizeValue, !colorValue)}
                        onChange={(e) => {
                          // setBookDetails({
                          //   ...bookDetails,
                          //   quantity: e.target.value,
                          // });
                          setQuantityValue(e.target.value);
                        }}
                      >
                        <option value="" selected disabled={true}>
                          Quantity
                        </option>

                        {nNumberArray(productQuantity).map((data, i) => (
                          <option key={i} value={data}>
                            {data}
                          </option>
                        ))}
                      </SelectWithoutForm>
                    </div>
                  </div>
                  <div className="button-holder py-3 md:py-5 mx-auto w-full md:w-auto md:mb-[-4px] ">
                    {productDetails?.sellerValue?.isSold ? (
                      <Button type="button" disabled>
                        Sold Out
                      </Button>
                    ) : (
                      <>
                        <Button
                          type="button"
                          onClick={() => {
                            handleCartList(router.query.id, token);
                          }}
                          loading={isLoadingAddToCartList}
                          disabled={disableOneOf(!quantityValue)}
                        >
                          Add to cart
                        </Button>

                        <Button
                          type="button"
                          variant="outlined"
                          onClick={() => {
                            dispatch(
                              setBookList([
                                {
                                  color: colorValue,
                                  quantity: quantityValue,
                                  size: sizeValue,
                                  id: productDetails?.id,
                                  availableSize: productAvailableSize,
                                },
                              ])
                            );
                            router.push("/customer/book");
                          }}
                          loading={isLoadingAddBuyingList}
                          disabled={disableOneOf(!quantityValue)}
                        >
                          Buy Now
                        </Button>
                      </>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}

export default SellDetailComponent;
