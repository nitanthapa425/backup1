import { useEffect, useState, useRef } from "react";
import { SearchIcon } from "@heroicons/react/outline";
import {
  searchCategories,
  searchSubCategories,
  useReadAllProductQuery,
  // useReadCategoryNoAuthWithLimitQuery,
  // useReadSubCategoryNoAuthWithLimitQuery,
} from "services/api/seller";
import {
  searchBrands,
  //  useReadBrandNoAuthQuery
} from "services/api/brand";
import { getQueryStringForVehicle } from "utils/getQueryStringForTable";
import { useRouter } from "next/router";
import Button from "components/Button";
import useDebounce from "hooks/useDebounce";
// import SearchWithSelect from "components/Search/SearchSingeData";
import LoadingCard from "components/LoadingCard";
// import Select from "react-select";
import ReactAccordions from "components/Accordions/ReactAccordions";
import "react-datepicker/dist/react-datepicker.css";
import "react-accessible-accordion/dist/fancy-example.css";
import RangeSlider from "components/RangeSlider/RangeSlider";
import ListingCard from "components/Card/ListingCard";
import FilterTags from "components/FilterTags";
import { arrayToString } from "utils/arrtoString";
// import { color } from "constant/constant";
import { useSelector } from "react-redux";
// import { useReadColorsQuery } from "services/api/ProductService";
import SelectAsync from "components/Select/AsyncSelect";
import { searchColor } from "services/api/Color/colorService";
// import SearchWithSelect from "components/Search/SearchSingeData";
// import { useReadColorsQuery } from "services/api/Color/colorService";
// import Select from "components/Select";
// import Select from "components/Select";

const ProductListingCom = () => {
  const router = useRouter();
  const loginInfo = useSelector((state) => state.customerAuth);
  const loginUser = loginInfo?.customer?._id;
  const productRef = useRef(null);
  const [mainTag, setMainTag] = useState({});
  const [tags, setTags] = useState([]);
  // const [colorSelectClear, setColorSelectClear] = useState(false);

  const {
    browseBy: browseByQuery,
    brandName: brandNameQuery,
    categoryName: categoryNameQuery,
    subCategoryName: subCategoryNameQuery,
    // color: colorQuery,
    vehicleType: vehicleTypeQuery,
    budgetRange: budgetRangeQuery,
    find: findQuery,
    brand: brandQuery,
    category: categoryQuery,
    subCategory: subCategoryQuery,
    budget: budgetQuery,
    search: searchQuery,
  } = router.query;

  const brandSelectRef = useRef(null);
  const categorySelectRef = useRef(null);
  const subCategorySelectRef = useRef(null);
  const colorSelectRef = useRef(null);

  const [brandNameQueryState, setBrandNameQueryState] = useState("");
  const [categoryNameQueryState, setCategoryNameQueryState] = useState("");
  const [subCategoryNameQueryState, setSubCategoryNameQueryState] =
    useState("");
  // const [colorQueryState, setColorQueryState] = useState("");
  const [budgetRangeQueryState, setBudgetRangeQueryState] = useState("");

  const [globalSearchQueryState, setGlobalSearchQueryState] = useState("");

  // When we refresh the page, our router.query objects get populated after rendering.
  // So to give the selected fields their default values according to the query params in the url route, we used router.ready
  useEffect(() => {
    if (router.isReady) {
      if (findQuery === "true") {
        setBrandNameQueryState(brandQuery);
        setCategoryNameQueryState(categoryQuery);
        setSubCategoryNameQueryState(subCategoryQuery);
        // setColorQueryState(colorQuery);
        setBudgetRangeQueryState(budgetQuery);
      } else if (searchQuery) {
        setGlobalSearchQueryState(searchQuery);
      } else {
        setBrandNameQueryState(brandNameQuery);
        setCategoryNameQueryState(categoryNameQuery);
        setSubCategoryNameQueryState(subCategoryNameQuery);
        // setColorQueryState(colorQuery);
        setBudgetRangeQueryState(budgetRangeQuery);
      }
    }
  }, [router.isReady]);

  useEffect(() => {
    const handleRouteChange = (url) => {
      setSelectedSortBy("latest");
      if (url === "/product-listing") {
        // We want to clear all filters if route is just '/product-listing'
        handleReset();
      } else if (url.includes("/product-listing?search=")) {
        handleReset();
        // This is for when we are in the /product-listing page and we search using the global search in the navbar
        // decodeURI converts the spaces given by %20 in URI into actual spaces and many more
        productRef.current.value = decodeURI(url.split("=")[1]);
      } else if (url.includes("/product-listing?browseBy=brand&brandName=")) {
        handleReset();
        const brandNameFromUrl = decodeURI(url.split("&brandName=")[1]);
        const bNameWithoutAnd = brandNameFromUrl.replace("&", "");
        brandSelectRef.current.setValue([
          { value: bNameWithoutAnd, label: bNameWithoutAnd },
        ]);
      } else if (
        url.includes("/product-listing?browseBy=category&categoryName=")
      ) {
        handleReset();
        const categoryNameFromUrl = decodeURI(url.split("&categoryName=")[1]);
        const categoryWithoutAnd = categoryNameFromUrl.replace("&", "");
        categorySelectRef.current.setValue([
          { value: categoryWithoutAnd, label: categoryWithoutAnd },
        ]);
      } else if (
        url.includes("/product-listing?browseBy=subCategory&subCategoryName=")
      ) {
        handleReset();
        const subCategoryNameFromUrl = decodeURI(
          url.split("&subCategoryName=")[1]
        );
        const subCategoryWithoutAnd = subCategoryNameFromUrl.replace("&", "");
        subCategorySelectRef.current?.setValue([
          { value: subCategoryWithoutAnd, label: subCategoryWithoutAnd },
        ]);
      }
    };
    router.events.on("routeChangeStart", handleRouteChange);
    return () => {
      router.events.off("routeChangeStart", handleRouteChange);
    };
  }, []);

  useEffect(() => {
    if (brandNameQuery) {
      brandSelectRef.current?.setValue([
        { value: brandNameQueryState, label: brandNameQueryState },
      ]);
    } else if (brandQuery) {
      brandSelectRef.current?.setValue([
        { value: brandNameQueryState, label: brandNameQueryState },
      ]);
    } else {
      brandSelectRef.current?.setValue([]);
    }
  }, [brandNameQueryState]);
  useEffect(() => {
    if (categoryNameQuery) {
      categorySelectRef.current?.setValue([
        { value: categoryNameQueryState, label: categoryNameQueryState },
      ]);
    } else if (categoryQuery) {
      categorySelectRef.current?.setValue([
        { value: categoryNameQueryState, label: categoryNameQueryState },
      ]);
    } else {
      categorySelectRef?.current?.setValue?.([]);
    }
  }, [categoryNameQueryState]);
  useEffect(() => {
    if (subCategoryNameQuery) {
      subCategorySelectRef.current?.setValue([
        { value: subCategoryNameQueryState, label: subCategoryNameQueryState },
      ]);
    } else if (subCategoryQuery) {
      subCategorySelectRef.current?.setValue([
        { value: subCategoryNameQueryState, label: subCategoryNameQueryState },
      ]);
    } else {
      subCategorySelectRef?.current?.setValue?.([]);
    }
  }, [subCategoryNameQueryState]);

  // useEffect(() => {
  //   if (colorQuery) {
  //     colorSelectRef.current.setValue([
  //       { value: colorQueryState, label: colorQueryState },
  //     ]);
  //   } else if (subCategoryQuery) {
  //     colorSelectRef.current.setValue([
  //       { value: colorQueryState, label: colorQueryState },
  //     ]);
  //   } else {
  //     colorSelectRef?.current?.setValue?.([]);
  //   }
  // }, [colorQueryState]);

  useEffect(() => {
    if (budgetRangeQuery) {
      const minMaxArr = budgetRangeQuery.split("-");
      setCostPriceRange(minMaxArr);
      // Also need to set this to fetch from api
      setCostPrice({
        id: "costPrice",
        value: budgetRangeQuery,
      });
    } else if (budgetQuery) {
      // When findQuery === 'true'
      const minMaxArr = budgetQuery.split("-");
      setCostPriceRange(minMaxArr);
      // Also need to set this to fetch from api
      setCostPrice({
        id: "costPrice",
        value: budgetQuery,
      });
    }
  }, [budgetRangeQueryState]);

  useEffect(() => {
    if (searchQuery) {
      productRef.current.value = searchQuery;
    }
  }, [globalSearchQueryState]);

  const [costPriceRange, setCostPriceRange] = useState([0, 100000]);
  const [pageSize, setPageSize] = useState(10);
  const [hasNextPage, setHasNextPage] = useState(true);
  const [sortBy, setSortBy] = useState([]);
  const [selectedSortBy, setSelectedSortBy] = useState("latest");
  const [productFilter, setProductFilter] = useState({
    id: "productTitle",
    value: "",
  });

  const [overallFilter, setOverallFilter] = useState([]);

  const [costPrice, setCostPrice] = useState({
    id: "costPrice",
    value: "",
  });

  const [viewAllPageQuery, setViewAllPageQuery] = useState("");
  const debounceProductFilter = useDebounce(productFilter, 1000);

  const debounceCostPrice = useDebounce(costPrice, 1000);
  // const { data: brandData } = useReadBrandNoAuthQuery();

  // const { data: categoryData } = useReadCategoryNoAuthWithLimitQuery();

  // const { data: dataReadColors } = useReadColorsQuery();
  // console.log("dataReadColors", dataReadColors);

  // const { data: subCategoryData } = useReadSubCategoryNoAuthWithLimitQuery();
  // const { data: categoryData } = useReadCategoryNoAuthWithLimitQuery();
  // const { data: subCategoryData } = useReadSubCategoryNoAuthWithLimitQuery();
  const [brandId, setBrandId] = useState({ id: "brandName", value: "" });
  const [categoryId, setCategoryId] = useState({
    id: "categoryName",
    value: "",
  });
  const [subCategoryId, setSubCategoryId] = useState({
    id: "subCategoryName",
    value: "",
  });

  const [colorId, setColorId] = useState({
    id: "color",
    value: "",
  });

  const [colored, setColored] = useState({ id: "color", value: "" });

  const [status, setStatus] = useState({
    id: "status",
    value: true,
  });

  const [listingTitle, setListingTitle] = useState("");

  // For setting viewAllPageQuery using query params from client/browser
  useEffect(() => {
    if (browseByQuery === "featured") {
      setMainTag({ id: "main", value: "Featured" });
    } else if (browseByQuery === "flash-sale") {
      setMainTag({ id: "main", value: "Flash Sale" });
    } else if (browseByQuery === "recentlyAdded") {
      setMainTag({ id: "main", value: "New Arrival" });
      setViewAllPageQuery(
        `?search="status":"true","vehicleType":"${vehicleTypeQuery}"&noRegex=status,costPrice&sortBy="createdAt"&sortOrder=-1&limit=10`
      );
    } else if (browseByQuery === "budget") {
      setViewAllPageQuery(
        `?search="costPrice":"${budgetRangeQuery}"&type=accessoriesSell&noRegex=costPrice&limit=10`
      );
    } else if (browseByQuery === "brand") {
      setViewAllPageQuery(
        `?search="brandName":"${brandNameQuery}","status":"true"&noRegex=status,costPrice`
      );
    } else if (browseByQuery === "category") {
      setViewAllPageQuery(
        `?search="categoryName":"${categoryNameQuery}","status":"true"&noRegex=status,costPrice`
      );
    } else if (browseByQuery === "subCategory") {
      setViewAllPageQuery(
        `?search="subCategoryName":"${subCategoryNameQuery}","status":"true"&noRegex=status,costPrice`
      );
      // } else if (browseByQuery === "color") {
      //   setViewAllPageQuery(
      //     `?search="color":"${colorQuery}","status":"true"&noRegex=status,costPrice`
      //   );
      // }
    } else if (findQuery === "true") {
      setViewAllPageQuery(
        `?search=${brandQuery === "" ? "" : `"brandName":"${brandQuery}",`}${
          budgetQuery === "" ? "" : `"costPrice":"${budgetQuery}",`
        }&type=accessoriesSell&noRegex=status,brandName,costPrice,makeYear,district,isForFlashSale`.replace(
          ",&type=accessoriesSell",
          "&type=accessoriesSell"
        )
      );
    } else {
      setMainTag({});
      setViewAllPageQuery("?null");
    }
  }, [browseByQuery, findQuery]);

  useEffect(() => {
    setBrandId({ id: "brandName", value: brandNameQuery });
  }, [brandNameQuery]);
  useEffect(() => {
    setCategoryId({ id: "categoryName", value: categoryNameQuery });
  }, [categoryNameQuery]);
  useEffect(() => {
    setSubCategoryId({ id: "subCategoryName", value: subCategoryNameQuery });
  }, [subCategoryNameQuery]);
  // useEffect(() => {
  //   setColorId({ id: "color", value: colorQuery });
  // }, [colorQuery]);
  useEffect(() => {
    setStatus({ id: "status", value: true });
  }, []);

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForVehicle(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    if (browseByQuery === "featured") {
      const queryForFeatured = query.replace(
        "&search=",
        `&search="hasInFeatured":"true",`
      );
      setViewAllPageQuery(queryForFeatured);
    } else if (browseByQuery === "flash-sale") {
      const queryForFlashSale = query.replace(
        "&search=",
        `&search="isForFlashSale":"true",`
      );
      setViewAllPageQuery(queryForFlashSale);
    } else if (browseByQuery === "budget" && budgetRangeQuery) {
      setViewAllPageQuery(query);
    } else if (browseByQuery === "brand" && brandNameQuery) {
      setViewAllPageQuery(query);
    } else if (findQuery === "true") {
      setViewAllPageQuery(query);
    } else if (searchQuery) {
      const queryForGlobalSearch = query.replace(
        "&search=",
        `&search="productTitle":"${searchQuery}",`
      );
      setViewAllPageQuery(queryForGlobalSearch);
    } else {
      setViewAllPageQuery(query);
    }
  };

  useEffect(() => {
    const search = [
      debounceProductFilter,

      brandId,
      categoryId,
      subCategoryId,
      colored,
      colorId,

      debounceCostPrice,

      status,
    ].filter((obj) => obj.value);

    search.forEach((s) => {
      if (s.id === "location.district") {
        s.id = "district";
      }
    });
    setOverallFilter(search);

    setTags([...search, mainTag]);
  }, [
    debounceProductFilter,

    brandId,
    categoryId,
    subCategoryId,
    colored,

    debounceCostPrice,

    status,
  ]);

  useEffect(() => {
    getData({
      pageIndex: 0,
      pageSize: pageSize,
      sortBy: sortBy,
      filters: overallFilter,
    });
  }, [getData, pageSize, sortBy]);

  useEffect(() => {
    if (selectedSortBy === "priceDesc") {
      setSortBy([{ id: "costPrice", desc: true }]);
    } else if (selectedSortBy === "priceAsc") {
      setSortBy([{ id: "costPrice", desc: false }]);
    } else {
      setSortBy([]);
    }
  }, [selectedSortBy]);

  const { data: viewAllPageData, isFetching: isFetchingViewAllPage } =
    useReadAllProductQuery(viewAllPageQuery);

  useEffect(() => {
    if (viewAllPageData) {
      setHasNextPage(viewAllPageData.hasNextPage);
      setListingTitle(`${viewAllPageData.totalDocs} products found`);
    }
  }, [viewAllPageData]);

  const handleReset = () => {
    // setColorSelectClear(true);

    brandSelectRef.current.clearValue();
    categorySelectRef.current.clearValue();
    subCategorySelectRef.current.clearValue();
    // colorSelectRef.current.clearValue();
    productRef.current.value = "";

    setCostPriceRange([0, 1000000]);

    setCostPrice({
      id: "costPrice",
      value: "",
    });

    setColored({
      id: "color",
      value: "",
    });
    setProductFilter({
      id: "productTitle",
      value: "",
    });
    setBrandId({
      id: "brandName",
      value: "",
    });
    setCategoryId({
      id: "categoryName",
      value: "",
    });
    setSubCategoryId({
      id: "subCategoryName",
      value: "",
    });
    setColorId({
      id: "color",
      value: "",
    });
  };

  const searchBrandssLoader = (brandName) => searchBrands(brandName);
  const searchCategoriesLoader = (categoryName) =>
    searchCategories(categoryName);
  const searchSubCategoriesLoader = (subCategoryName) =>
    searchSubCategories(subCategoryName);
  const searchColorLoader = (color) => searchColor(color);

  const accordionItems = [
    {
      uuid: 1,
      heading: "Name",
      content: (
        <div className="search relative">
          <SearchIcon className="h-5 w-5 absolute top-1/2 translate-y-[-60%] left-3" />
          <input
            type="search"
            placeholder=" Search By Accessories Name"
            className="pl-[36px]"
            ref={productRef}
            onChange={(e) => {
              setProductFilter({
                id: "productTitle",
                value: e.target.value,
              });
            }}
          />
        </div>
      ),
    },

    {
      uuid: 2,
      heading: "Brand",
      content: (
        <SelectAsync
          isMulti
          placeholder="Search By Brand"
          dataLoader={searchBrandssLoader}
          onChange={(selectedOption) => {
            setCategoryId({
              id: "brandName",
              value: arrayToString(selectedOption),
            });
          }}
          refEl={brandSelectRef}
        />
        // <Select
        //   isMulti={true}
        //   placeholder="Search By Brand"
        //   onChange={(selectedBrand) => {
        //     setBrandId({
        //       id: "brandName",
        //       value: arrayToString(selectedBrand),
        //     });
        //   }}
        //   options={brandData?.docs?.map((brand) => ({
        //     value: brand?.brandName,
        //     label: brand?.brandName,
        //   }))}
        //   ref={brandSelectRef}
        // />
      ),
    },
    {
      uuid: 3,
      heading: "Category",
      content: (
        <>
          <SelectAsync
            isMulti
            placeholder="Search By Category"
            dataLoader={searchCategoriesLoader}
            onChange={(selectedOption) => {
              setCategoryId({
                id: "categoryName",
                value: arrayToString(selectedOption),
              });
            }}
            refEl={categorySelectRef}
          />
        </>
      ),
    },
    {
      uuid: 4,
      heading: "Sub Category",
      content: (
        <SelectAsync
          isMulti
          placeholder="Search By Sub Category"
          dataLoader={searchSubCategoriesLoader}
          onChange={(selectedOption) => {
            setCategoryId({
              id: "subCategoryName",
              value: arrayToString(selectedOption),
            });
          }}
          refEl={subCategorySelectRef}
        />
      ),
    },
    {
      uuid: 5,
      heading: "Color",
      content: (
        // <SearchWithSelect
        //   placeholder="Search By Color"
        //   value={[colored || null]}
        //   onChange={(selectedValue) => {
        //     setColored({
        //       id: "color",
        //       value: arrayToString(selectedValue),
        //     });
        //     setColorSelectClear(false);
        //   }}
        //   options={color?.map((option) => ({
        //     value: option,
        //     label: option,
        //   }))}
        //   customClear={colorSelectClear}
        // />
        // <Select
        //   isMulti={true}
        //   placeholder="Search By Color"
        //   onChange={(selectedColor) => {
        //     setColorId({
        //       id: "color",
        //       value: arrayToString(selectedColor),
        //     });
        //   }}
        //   options={dataReadColors?.map((color) => ({
        //     value: color?.color,
        //     label: color?.color,
        //   }))}
        //   // ref={colorSelectRef}
        // />

        <SelectAsync
          isMulti
          placeholder="Search Color"
          dataLoader={searchColorLoader}
          onChange={(selectedOption) => {
            setCategoryId({
              id: "color",
              value: arrayToString(selectedOption),
            });
          }}
          refEl={colorSelectRef}
        />
      ),
    },
    {
      uuid: 6,
      heading: "Price",
      content: (
        <RangeSlider
          min={0}
          max={100000}
          value={costPriceRange}
          onChange={(value) => {
            const minMaxStr = value.join("-");
            setCostPriceRange(value);
            setCostPrice({
              id: "costPrice",
              value: minMaxStr,
            });
          }}
          postFix="NRs."
          sliderName="Cost Price (NRs.)"
        />
      ),
    },
  ];

  return (
    <div>
      <main id="main">
        <section className="filter-section py-8">
          <div className="container"></div>
        </section>

        <section className="vehicle-listing-section pb-20">
          <div className="container">
            <div className="row">
              <div className="w-full sm:w-1/3 px-2 mb-3">
                <div className="toolbar-accordion-holder">
                  <ReactAccordions
                    AccordionItems={accordionItems}
                    preExp={
                      browseByQuery === "budget" && budgetRangeQuery
                        ? [6]
                        : browseByQuery === "brand" && brandNameQuery
                        ? [2]
                        : browseByQuery === "category" && categoryNameQuery
                        ? [3]
                        : browseByQuery === "subCategory" &&
                          subCategoryNameQuery
                        ? [3]
                        : // : browseByQuery === "color" && colorQuery
                        // ? [5]
                        findQuery === "true"
                        ? [2, 6]
                        : searchQuery !== ""
                        ? [1]
                        : [1]
                    }
                    allowMulExp={true}
                  />
                </div>
                <div className="form-group pl-[35px] text-center">
                  <Button type="button" variant="error" onClick={handleReset}>
                    Reset
                  </Button>
                </div>
              </div>
              <div className="w-full sm:w-2/3 px-2 mb-3">
                {listingTitle ? (
                  <div className="row mb-5 px-3 border-b border-primary">
                    <div className="w-full mb-2">
                      <h4 className="text-xl">{listingTitle}</h4>
                    </div>
                    <div className="mb-1">
                      <span className="text-sm mr-2">Filtered By: </span>
                      <FilterTags
                        tags={tags}
                        setColored={setColored}
                        // setColorSelectClear={setColorSelectClear}
                        brandSelectRef={brandSelectRef}
                        setBrandId={setBrandId}
                        categorySelectRef={categorySelectRef}
                        subCategorySelectRef={subCategorySelectRef}
                        colorSelectRef={colorSelectRef}
                        setCategoryId={setCategoryId}
                        setSubCategoryId={setSubCategoryId}
                        setColorId={setColorId}
                        setCostPriceRange={setCostPriceRange}
                        setCostPrice={setCostPrice}
                        productRef={productRef}
                        setProductFilter={setProductFilter}
                      />
                      {tags.length > 3 ? (
                        <span
                          className="text-sm ml-2 text-red-400 capitalize hover:cursor-pointer hover:text-red-600"
                          onClick={handleReset}
                        >
                          Clear filters
                        </span>
                      ) : null}
                    </div>
                  </div>
                ) : null}
                <div className="row d-flex justify-end mb-3">
                  <div>
                    <label htmlFor="sortBy">Sort By:</label>
                    <select
                      name="sortBy"
                      onChange={(e) => {
                        setSelectedSortBy(e.target.value);
                      }}
                      value={selectedSortBy}
                    >
                      <option value="latest">Latest</option>
                      <option value="priceDesc">Price (High to Low)</option>
                      <option value="priceAsc">Price (Low to High)</option>
                    </select>
                  </div>
                </div>

                <div className="row">
                  {isFetchingViewAllPage ? (
                    <LoadingCard cardColsSize="two" />
                  ) : (browseByQuery || findQuery === "true") &&
                    viewAllPageData?.docs.length ? (
                    viewAllPageData?.docs.map((data, i) => {
                      const userIds = data.wishList.map((data, i) => {
                        return data.userId;
                      });
                      const isInWishList = userIds.includes(loginUser);

                      return (
                        <ListingCard
                          key={i}
                          route={`/product-listing/${data?.id}`}
                          imgUrl={data?.productImage.imageUrl}
                          totalImages={data?.productImages?.length - 1}
                          productTitle={data?.productTitle}
                          costPrice={data?.costPrice}
                          discountedPrice={data?.discountedPrice}
                          discountPercentage={data?.discountPercentage}
                          color={data?.color?.[0]}
                          id={data.id}
                          isSold={data?.isSold}
                          isForFlashSale={data?.isForFlashSale}
                          hasWarrenty={data?.hasWarrenty}
                          isReturnable={data?.isReturnable}
                          isInWishList={isInWishList}
                        />
                      );
                    })
                  ) : browseByQuery || findQuery === "true" ? (
                    <div className="w-full">
                      <p className="text-sm text-center">No Data Found</p>
                    </div>
                  ) : null}
                  {(browseByQuery || findQuery === "true") &&
                  viewAllPageData?.docs.length ? (
                    <div className="w-full flex justify-center items-center">
                      <div>
                        {hasNextPage && (
                          <Button
                            type="button"
                            variant="secondary"
                            disabled={!hasNextPage}
                            onClick={() => {
                              setPageSize((pageSize) => pageSize + 10);
                            }}
                            loading={isFetchingViewAllPage}
                          >
                            Load More
                          </Button>
                        )}
                      </div>
                    </div>
                  ) : null}
                  {isFetchingViewAllPage ? (
                    <LoadingCard cardColsSize="two" />
                  ) : !browseByQuery &&
                    findQuery !== "true" &&
                    viewAllPageData?.docs.length ? (
                    viewAllPageData?.docs.map((data, i) => {
                      const userIds = data.wishList.map((data, i) => {
                        return data.userId;
                      });
                      const isInWishList = userIds.includes(loginUser);

                      return (
                        <ListingCard
                          key={i}
                          route={`/product-listing/${data?.id}`}
                          imgUrl={data?.productImage?.imageUrl}
                          totalImages={data?.productImages?.length - 1}
                          productTitle={data?.productTitle}
                          costPrice={data?.costPrice}
                          discountedPrice={data?.discountedPrice}
                          discountPercentage={data?.discountPercentage}
                          color={data?.color?.[0]}
                          id={data.id}
                          isSold={data?.isSold}
                          hasWarrenty={data?.hasWarrenty}
                          isReturnable={data?.isReturnable}
                          isInWishList={isInWishList}
                        />
                      );
                    })
                  ) : !browseByQuery && findQuery !== "true" ? (
                    <div className="w-full">
                      <p className="text-sm text-center">No Data Found</p>
                    </div>
                  ) : null}
                  {!browseByQuery &&
                  findQuery !== "true" &&
                  viewAllPageData?.docs.length ? (
                    <div className="w-full flex justify-center items-center">
                      <div>
                        {hasNextPage && (
                          <Button
                            type="button"
                            variant="secondary"
                            disabled={!hasNextPage}
                            onClick={() => {
                              setPageSize((pageSize) => pageSize + 10);
                            }}
                            loading={isFetchingViewAllPage}
                          >
                            Load More
                          </Button>
                        )}
                      </div>
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default ProductListingCom;
