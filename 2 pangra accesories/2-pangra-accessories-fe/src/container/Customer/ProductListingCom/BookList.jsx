/* eslint-disable no-empty-pattern */
// import UserHeader from 'components/Header/UserHeader';
import React, { useEffect, useMemo, useState } from "react";
// import Link from 'next/link';
// import UserLayout from 'layouts/User';

// import Button from 'components/Button';
import { useBuyAccessoryMutation } from "services/api/seller";
import { useToasts } from "react-toast-notifications";
import Link from "next/link";
import BookCard from "./BookCard";
import { useDispatch, useSelector } from "react-redux";
import Button from "components/Button";
import ShippingAddressTable from "../GetMyProfile/edit/ShippingAddressTable";
import Modal from "react-modal";
import {
  useReadShippingAddressQuery,
  useReadShippingChargeQuery,
} from "services/api/customer";
import { setAddress, setBookList } from "store/features/book/BookSlice";
import router from "next/router";
import ShippingCom from "../GetMyProfile/edit/ShippingCom";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import { PencilIcon } from "@heroicons/react/outline";
import { sumOfEachElementOfArray } from "utils/methods";
import InquiryForm from "container/InquiryForm/inqueryForm";
import AddEmail from "./AddEmail";
import { useCustomerMyProfileQuery } from "services/api/customerSelf";

const AddToBookList = () => {
  const { addToast } = useToasts();
  const [showShippingAddressTable, setShowShippingAddressTable] =
    useState(true);

  const { data: customerProfile, refetch: refetchCustomerProfile } =
    useCustomerMyProfileQuery();

  // const [totalPrice, setTotalPrice] = useState(0);

  const [totalCostList, setTotalCostList] = useState([]);

  // const [selectedLocationDetail, setSelectedLocationDetail] = useState([]);

  const dispatch = useDispatch();

  const book = useSelector((state) => state.book);

  const bookDetails = book.bookList || [];
  const quantity = bookDetails.map((v, i) => v.quantity);

  const totalCostValue = useMemo(() => {
    return totalCostList.map((data, i) => data.value);
  }, [totalCostList]);

  // const productAvailableSize = useMemo(() => {
  //   const productAvailableSizeValue = productDetails?.availableOptions?.map?.(
  //     (value, i) => value.size
  //   );
  //   const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
  //   return uniqueAvailableSizeValue;
  // }, [productDetails]);

  // const subTotalAmount = twoArrayElementProduct(
  //   quantity,
  //   totalCostValue
  // ).reduce((pre, cur) => pre + cur, 0);

  const subTotalAmount = totalCostValue.reduce((pre, cur) => pre + cur, 0);
  const [modelOpenInquiry, setModelOpenInquiry] = useState(false);
  const closeModelOpenInquiry = () => {
    setModelOpenInquiry(false);
  };

  const [modalIsOpenn, setIsOpenn] = useState(false);
  const closeModall = () => {
    setIsOpenn(false);
  };

  const [openAddEmail, setOpenAddEmail] = useState(false);
  const closeAddEmail = () => {
    setOpenAddEmail(false);
  };

  const {
    data: shippingAddressData,
    isSuccess: shippingAddressIsSuccess,
    isFetching: shippingAddressIsFetching,
    refetch,
  } = useReadShippingAddressQuery();

  const { data: dataReadShippingCharge } = useReadShippingChargeQuery();

  // console.log("******************8ReadShippingCharge", dataReadShippingCharge);

  useEffect(() => {
    if (shippingAddressIsSuccess || shippingAddressIsFetching) {
      if (shippingAddressData?.length) {
        dispatch(setAddress([...shippingAddressData]?.reverse()?.[0]));
      }
    }
  }, [shippingAddressIsSuccess, shippingAddressIsFetching]);

  const [shippingTableModal, setShippingTableModal] = useState(false);
  const closeShippingTableModal = () => {
    setShippingTableModal(false);
  };

  const [
    buyAccessory,
    {
      isLoading: isLoadingAccessory,
      isError: isErrorAccessory,
      isSuccess: isSuccessAccessory,
      error: errorAccessory,
      data: dataBuyAccessory,
    },
  ] = useBuyAccessoryMutation();

  useEffect(() => {
    if (isSuccessAccessory) {
      addToast(
        dataBuyAccessory?.message ||
          "Congratulations! Order placed successfully.",
        {
          appearance: "success",
        }
      );
      dispatch(setBookList([]));
      router.push("/ordered");
    }
  }, [isSuccessAccessory]);
  useEffect(() => {
    if (isErrorAccessory) {
      addToast(errorAccessory?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorAccessory, errorAccessory]);

  //   console.log("bookDetails", bookDetails);

  const shippingChargeAmount = useMemo(() => {
    if (dataReadShippingCharge?.length) {
      const amount = [...dataReadShippingCharge].reverse()[0].shippingCharge;
      return amount;
    } else {
      return null;
    }
  }, [dataReadShippingCharge, subTotalAmount]);

  const totalNumberOfQuantityOrder = () => {
    // const filterIfQuantity = book?.bookList.filter((data, i) => {
    //   return Number(data.quantity);
    // });

    const totalQuantityList = book?.bookList.map((data, i) => {
      return Number(data?.quantity);
    });

    const totalQuantityListSum = sumOfEachElementOfArray(totalQuantityList);
    return totalQuantityListSum;
  };

  const OverallAmount = useMemo(() => {
    if (Number(subTotalAmount) === 0) {
      return 0;
    }

    const totalAmount =
      Number(shippingChargeAmount * totalNumberOfQuantityOrder()) +
      Number(subTotalAmount);

    return totalAmount;
  }, [shippingChargeAmount, subTotalAmount, book]);

  return (
    <div className="pb-[80px]">
      {/* <UserLayout title="My Cart"> */}
      {/* <UserHeader></UserHeader> */}

      <Modal
        isOpen={modelOpenInquiry}
        className="mymodal order"
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={closeModelOpenInquiry}
          className="text-error absolute top-[5px] right-[5px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <InquiryForm
          value="Location Not Found"
          closeModelOpenInquiry={closeModelOpenInquiry}
        />
      </Modal>
      {bookDetails?.length ? (
        <div className="container">
          <div className="flex flex-wrap items-start mt-[46px]">
            <div className="flex-1 mr-[25px] ">
              {bookDetails?.map((data, i) => {
                return (
                  <div key={i}>
                    <BookCard
                      index={i}
                      bookDetail={data}
                      // setTotalPrice={setTotalPrice}
                      setTotalCostList={setTotalCostList}
                      totalCostList={totalCostList}
                      shippingChargeAmount={shippingChargeAmount}
                    ></BookCard>
                  </div>
                );
              })}
            </div>

            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300  py-4 px-6 sticky top-[75px]">
              {shippingAddressData?.length ? (
                <div
                  title="Edit Shipping Address"
                  className="inline-block text-blue-600 hover:text-primary-light hover:cursor-pointer"
                  onClick={() => {
                    setShippingTableModal(true);
                  }}
                >
                  <PencilIcon className="w-4 h-4 mr-1 inline-block  "></PencilIcon>
                  Edit Shipping Address
                </div>
              ) : null}
              <div
                className=" text-blue-600 hover:text-primary-light hover:cursor-pointer"
                onClick={() => {
                  setModelOpenInquiry(true);
                }}
              >
                Didn’t find my location?
              </div>

              {/* {console.log("book", book)} */}

              <h1 className="h5 mb-3">Order Summary</h1>
              <div className="border-b-2 border-gray-200 mb-2">
                <div className="flex flex-wrap justify-between ">
                  {shippingAddressData?.length ? (
                    <p className=" mb-3">
                      {book?.address?.combineLocation
                        ?.split?.(">")
                        ?.join?.(",") +
                        " , " +
                        book?.address?.exactLocation +
                        " , " +
                        book?.address?.mobile}
                    </p>
                  ) : null}

                  <p className="mb-1">
                    SubTotal{" "}
                    {bookDetails.length
                      ? `(${totalNumberOfQuantityOrder()} product/s)`
                      : ""}
                  </p>
                  <p className="mb-1">
                    NRs. {thousandNumberSeparator(subTotalAmount.toFixed(2))}
                  </p>
                </div>
                <div className="flex flex-wrap justify-between ">
                  <p>Shipping Charge</p>
                  <p>
                    NRs. {shippingChargeAmount * totalNumberOfQuantityOrder()}
                  </p>
                </div>
              </div>
              <div className="flex flex-wrap justify-between mb-3">
                <p className="mb-1">Total Amount</p>
                <p className="mb-1">
                  NRs. {thousandNumberSeparator(OverallAmount.toFixed(2))}
                </p>
              </div>

              <Modal
                isOpen={shippingTableModal}
                className={showShippingAddressTable ? "opacity-1" : "hidden"}
                overlayClassName={
                  showShippingAddressTable ? "myoverlay" : "hidden"
                }
              >
                <button
                  type="button"
                  onClick={closeShippingTableModal}
                  className="text-error absolute top-[5px] right-[5px]"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>
                <div>
                  <ShippingAddressTable
                    // setSelectedLocationDetail={setSelectedLocationDetail}
                    setIsOpen={setShippingTableModal}
                    setShowShippingAddressTable={setShowShippingAddressTable}
                  ></ShippingAddressTable>
                  <div className="text-right mt-4">
                    <Button onClick={closeShippingTableModal}>Save</Button>
                  </div>
                </div>
              </Modal>

              <Modal
                isOpen={openAddEmail}
                // onRequestClose={closeModal}
                className="mymodal"
                overlayClassName="myoverlay"
              >
                <button
                  type="button"
                  onClick={closeAddEmail}
                  className="text-error absolute top-[5px] right-[5px]"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>

                <AddEmail
                  setIsOpen={setOpenAddEmail}
                  refetchCustomerProfile={refetchCustomerProfile}
                ></AddEmail>
              </Modal>

              <Modal
                isOpen={modalIsOpenn}
                // onRequestClose={closeModal}
                className="shippingAddressModal"
                overlayClassName="myoverlay"
              >
                <button
                  type="button"
                  onClick={closeModall}
                  className="text-error absolute top-[5px] right-[5px]"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>

                <ShippingCom
                  setIsOpen={setIsOpenn}
                  refetch={refetch}
                ></ShippingCom>
              </Modal>

              {!shippingAddressData?.length ? (
                <Button
                  type="button"
                  onClick={() => {
                    setIsOpenn(true);
                  }}
                  loading={isLoadingAccessory}
                >
                  Add Shipping Address
                </Button>
              ) : (
                <Button
                  type="button"
                  onClick={() => {
                    if (!customerProfile?.email) {
                      setOpenAddEmail(true);
                    } else {
                      buyAccessory({
                        address: book?.address?.id,
                        totalPrice: `${OverallAmount}`,
                        totalShippingCharge: `${
                          shippingChargeAmount * totalNumberOfQuantityOrder()
                        }`,
                        totalQuantity: `${totalNumberOfQuantityOrder()}`,
                        bookList: book?.bookList.map((value, i) => {
                          return {
                            color: value.color,
                            size: value.size,
                            discountedAmount:
                              Number(value.totalPrice) / Number(quantity),
                            quantity: value.quantity,
                            shippingCharge: `${shippingChargeAmount}`,
                            totalPrice: `${
                              value.totalPrice +
                              shippingChargeAmount * value.quantity
                            }`,
                            id: value.id,
                          };
                        }),
                      });
                    }
                  }}
                  loading={isLoadingAccessory}
                >
                  Checkout
                </Button>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="/images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your ordering list is empty</h1>
            <p>
              Looks like you have not made your <br />
              choice yet
            </p>
            <Link href={"/product-listing"}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default AddToBookList;
