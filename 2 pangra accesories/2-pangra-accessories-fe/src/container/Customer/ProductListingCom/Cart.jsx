import { XIcon } from "@heroicons/react/outline";
import SelectWithoutForm from "components/Select/SelectWithoutForm";
import { productQuantity } from "constant/constant";
import { useRouter } from "next/router";
import React, { useEffect, useMemo, useState } from "react";
import {
  useReadQuantityMutation,
  useReadSizeColorQuantityMutation,
} from "services/api/customer";
import { useReadSingleProductQuery } from "services/api/seller";
import { disableOneOf } from "utils/disableOneOf";
import { nNumberArray } from "utils/nNumberOfArray";
import { uniqueIdAmountSum } from "utils/uniqueIdAmountSum";
import ActualPrice from "./ActualPrice";

const Cart = ({
  index,
  data,
  bookList,
  setBookLis,
  bookDet,
  setBookDet,
  setOpenModalDeleteSingleCart,
  setDeleteSingleCart,
  setTotalCostList,
  totalCostList,
}) => {
  const [avalilableColors, setAvailableColors] = useState();
  // eslint-disable-next-line no-unused-vars
  const [avalilableQuanties, setAvailableQuantities] = useState();
  const [discountedAmount, setDiscountedAmount] = useState();
  // eslint-disable-next-line no-unused-vars
  const [discountPercentage, setDiscountedPercent] = useState();

  const { data: productDetails, isSuccess: isSucessProductDetails } =
    useReadSingleProductQuery(data?.accessoriesId?.id);

  const productAvailableSize = useMemo(() => {
    const productAvailableSizeValue = productDetails?.availableOptions?.map?.(
      (value, i) => value.size
    );
    const uniqueAvailableSizeValue = [...new Set(productAvailableSizeValue)];
    return uniqueAvailableSizeValue;
  }, [productDetails]);

  const cardSize = useMemo(() => {
    const size = bookDet?.find?.((v, i) => {
      return i === index;
    })?.size;

    return size;
  }, [bookDet]);

  const cardColor = useMemo(() => {
    const color = bookDet?.find?.((v, i) => {
      return i === index;
    })?.color;

    return color;
  }, [bookDet]);

  const cardQuantity = useMemo(() => {
    const quantity = bookDet?.find?.((v, i) => {
      return i === index;
    })?.quantity;

    return quantity;
  }, [bookDet, cardSize, cardColor]);

  const costValue = () => {
    // if (`${discountedAmount}`) {
    //   return cardQuantity * Number(discountedAmount) || 0;
    // } else if (Number(productDetails?.discountPercentage)) {
    //   return cardQuantity * Number(productDetails.discountedPrice) || 0;
    // } else {
    //   return cardQuantity * productDetails.costPrice;
    // }

    return Number(cardQuantity) * Number(discountedAmount) || 0;
  };

  useEffect(() => {
    if (isSucessProductDetails) {
      setTotalCostList((totalCostList) => {
        const uniqueIdAmount = uniqueIdAmountSum(totalCostList, {
          id: data._id,
          value: costValue(),
        });

        return uniqueIdAmount;
      }, []);
    }
  }, [isSucessProductDetails, discountedAmount, cardQuantity, data]);

  useEffect(() => {
    if (cardSize && productDetails?.id) {
      readSizeColorQuantity({
        body: { size: cardSize },
        id: productDetails?.id,
      });

      setDiscountedAmount("");
      setDiscountedPercent("");
    }
  }, [cardSize, productDetails]);

  useEffect(() => {
    if (cardColor && productDetails?.id && cardSize) {
      readQuantity({
        body: { size: cardSize, color: cardColor },
        id: productDetails?.id,
      });
      setDiscountedAmount("");
      setDiscountedPercent("");
    }
  }, [cardColor, cardSize, productDetails]);

  const [
    readSizeColorQuantity,
    {
      // isLoading: isLoadingReadSizeColorQuantity,
      isError: isErrorReadSizeColorQuantity,
      isSuccess: isSuccessReadSizeColorQuantity,
      data: dataReadSizeColorQuantity,
    },
  ] = useReadSizeColorQuantityMutation();

  const [
    readQuantity,
    {
      // isLoading: isLoadingReadQuantity,
      isError: isErrorReadQuantity,
      isSuccess: isSuccessReadQuantity,
      data: dataReadQuantity,
    },
  ] = useReadQuantityMutation();

  useEffect(() => {
    if (isSuccessReadQuantity) {
      setAvailableQuantities(dataReadQuantity[0].quantity);
      setDiscountedAmount(dataReadQuantity[0].discountedAmount);
      setDiscountedPercent(dataReadQuantity[0].discountPercentage);
    }

    // if (isErrorReadQuantity) {
    //   addToast(errorReadQuantity?.data?.message, {
    //     appearance: "error",
    //   });
    // }
  }, [isSuccessReadQuantity, isErrorReadQuantity]);

  useEffect(() => {
    if (isSuccessReadSizeColorQuantity) {
      setAvailableColors(dataReadSizeColorQuantity);
    }

    // if (isErrorReadSizeColorQuantity) {
    //   addToast(errorReadSizeColorQuantity?.data?.message, {
    //     appearance: "error",
    //   });
    // }
  }, [isSuccessReadSizeColorQuantity, isErrorReadSizeColorQuantity]);

  const router = useRouter();

  return (
    <div
      key={index}
      className="relative group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5 pr-3"
    >
      <input
        type="checkbox"
        checked={bookList.includes(data._id)}
        className="checkbox w-4 h-4 border border-gray-400 inline-block mr-3"
        onClick={(e) => {
          if (e.target.checked) {
            const allId = [...bookList, data._id];
            const uniqueList = [...new Set(allId)];
            setBookLis(uniqueList);
          } else {
            setBookLis(
              bookList.filter((v, j) => {
                return v !== data._id;
              })
            );
          }
        }}
      ></input>

      <div
        className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0"
        onClick={() => {
          router.push(`/product-listing/${data.accessoriesId?.id}`);
        }}
      >
        <img
          src={data?.accessoriesId?.productImage?.imageUrl}
          alt="image description"
          className="w-full h-[110px] object-contain"
        />
      </div>
      <div className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[30px]">
        <div className="flex justify-between ">
          <div>
            {" "}
            <h1
              className="h5 mb-1 hover:text-primary"
              onClick={() => {
                router.push(`/product-listing/${productDetails?.id}`);
              }}
            >
              {productDetails?.productTitle}
            </h1>
          </div>

          <ActualPrice
            discountedAmount={discountedAmount}
            costPrice={productDetails?.costPrice}
            cardQuantity={cardQuantity}
          />
        </div>
      </div>
      <span
        className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
        onClick={() => {
          setOpenModalDeleteSingleCart(true);
          setDeleteSingleCart(data._id);
        }}
      >
        <XIcon className="w-5 h-5"></XIcon>
      </span>

      <div className="w-full cart-select">
        <div className="row flex-wrap mt-3 ">
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(`${productAvailableSize?.length}` === `0`)
                  ? "disabled-input"
                  : ""
              } w-full`}
              label="Size"
              value={cardSize}
              disabled={disableOneOf(`${productAvailableSize?.length}` === `0`)}
              onChange={(e) => {
                const bookl1 = bookDet?.map?.((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      size: e.target.value,
                      color: "",
                      quantity: "",
                    };
                  } else {
                    return obj;
                  }
                });
                setBookDet(bookl1);
              }}
            >
              <option value="" selected disabled={true}>
                Select Size
              </option>

              {productAvailableSize?.map((size, i) => (
                <option key={i} value={size}>
                  {size}
                </option>
              ))}
            </SelectWithoutForm>
          </div>
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(!cardSize || `${avalilableColors?.length}` === `0`)
                  ? "disabled-input"
                  : ""
              } w-24`}
              label="Color"
              value={cardColor}
              disabled={disableOneOf(
                !cardSize || `${avalilableColors?.length}` === `0`
              )}
              onChange={(e) => {
                const bookl1 = bookDet.map((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      color: e.target.value,
                      quantity: "",
                    };
                  } else {
                    return obj;
                  }
                });
                setBookDet(bookl1);
              }}
            >
              <option value="" selected disabled={true}>
                Select Color
              </option>

              {avalilableColors?.map?.((color, i) => (
                <option key={i} value={color}>
                  {color}
                </option>
              ))}
            </SelectWithoutForm>
          </div>
          <div className="three-col">
            <SelectWithoutForm
              className={`${
                disableOneOf(!cardSize, !cardColor) ? "disabled-input" : ""
              } w-24`}
              label="Quantity"
              value={cardQuantity}
              disabled={disableOneOf(!cardSize, !cardColor)}
              onChange={(e) => {
                const bookl1 = bookDet.map((obj, i) => {
                  if (i === index) {
                    return {
                      ...obj,
                      quantity: e.target.value,
                    };
                  } else {
                    return obj;
                  }
                });

                setBookDet(bookl1);
              }}
            >
              <option value="" selected disabled={true}>
                Select Quantity
              </option>

              {nNumberArray(productQuantity).map((quantity, i) => (
                <option key={i} value={quantity}>
                  {quantity}
                </option>
              ))}
            </SelectWithoutForm>
            {Number(cardQuantity) ? null : (
              <div className="mt-1 text-error">
                To order please select quantity
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
