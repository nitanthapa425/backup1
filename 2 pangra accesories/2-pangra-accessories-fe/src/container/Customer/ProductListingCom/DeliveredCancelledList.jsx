import React from "react";

import { useGetMyDeliveredCancelledListQuery } from "services/api/seller";

import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import Link from "next/link";
import { useRouter } from "next/router";

import "rc-tooltip/assets/bootstrap.css";
import { firstLetterCapital } from "utils/firstLetterCapita.";

const DeliveredCancelledList = ({ type }) => {
  const router = useRouter();

  const { data: dataMyOrderedList, isFetching: isFetchingMyOrderedList } =
    useGetMyDeliveredCancelledListQuery(`?type=${type}`);

  return (
    <div className="pb-[80px]">
      {isFetchingMyOrderedList ? (
        <div className="container flex justify-center">
          <svg
            className={`animate-spin h-10 w-10 text-primary`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      ) : dataMyOrderedList?.docs?.length ? (
        <div className="container">
          <div className="flex flex-wrap items-start">
            <div className="flex-1 mr-[25px] ">
              {[...dataMyOrderedList?.docs]?.reverse()?.map((data, i) => {
                return (
                  <div
                    key={i}
                    className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5"
                  >
                    <div className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0">
                      <img
                        src={data?.productImage?.imageUrl}
                        alt="image description"
                        className="w-full h-[110px] object-contain"
                        onClick={() => {
                          router.push(`/product-listing/${data.accessoriesId}`);
                        }}
                      />
                    </div>
                    <div className="detail-holder w-full lg:w-auto lg:flex-1">
                      <div className="flex justify-between mr-1 ">
                        <div
                          onClick={() => {
                            router.push(
                              `/product-listing/${data.accessoriesId}`
                            );
                          }}
                        >
                          <h1 className="h5 mb-1">
                            <span>{data.productName} </span>
                          </h1>
                        </div>
                        <div>
                          <div className=" flex justify-between ">
                            <span className="price font-bold text-primary-light">
                              NRs. {thousandNumberSeparator(data.totalPrice)}
                            </span>
                          </div>
                        </div>
                      </div>

                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold ">
                            Color : {data.color || "N/A"}&nbsp;, &nbsp;
                          </span>
                          <span className="font-semibold">
                            Size : {data.size || "N/A"}&nbsp;, &nbsp;
                          </span>
                          <span className="font-semibold">
                            Quantity : {data.quantity || "N/A"}
                          </span>
                        </p>
                        <p className="text-sm">
                          <span className="font-semibold">
                            Shipping Address :&nbsp;
                            {data?.shippingAddressLocation?.combineLocation
                              ?.split(">")
                              ?.join("/")}
                            &nbsp;, &nbsp;
                          </span>
                          <span className="font-semibold">
                            Exact Location :&nbsp;
                            {data?.shippingAddressLocation?.exactLocation}
                          </span>
                        </p>
                      </div>
                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">
                            Total Shipping Charge : NRs.{" "}
                            {thousandNumberSeparator(
                              data.shippingCharge * data.quantity
                            )}
                          </span>{" "}
                        </p>
                      </div>

                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">
                            Delivered Date:{" "}
                            {new Date(
                              data.deliveredDetail?.assignAt
                            ).toLocaleString()}
                          </span>{" "}
                        </p>
                      </div>

                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">
                            {/* Order Status : {data.deliveryStatus} */}
                            Order Status : &nbsp;
                            <div
                              className={`
            ${
              data.deliveryStatus === "Delivered"
                ? "badge-success"
                : data.deliveryStatus === "Cancelled"
                ? "badge-error"
                : ""
            }
            
            `}
                            >
                              {data.deliveryStatus}
                            </div>
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="/images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">
              Oops! Your {firstLetterCapital(type)} list is empty
            </h1>
            <p>
              Looks like you have not made your <br />
              choice yet
            </p>
            <Link href={"/product-listing"}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default DeliveredCancelledList;
