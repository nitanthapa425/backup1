/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";

import { TrashIcon } from "@heroicons/react/outline";
import {
  useDeleteCustomerBookedListMutation,
  useGetMyBookedListQuery,
} from "services/api/seller";
import { useToasts } from "react-toast-notifications";
// import { sameArrayIrrespectiveToPosition } from "utils/sameArray";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";
import Popup from "components/Popup";
import Link from "next/link";
import { useRouter } from "next/router";
// import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";

const ReadBookedList = () => {
  const { addToast } = useToasts();
  const [openModalVehicleType, setOpenModalVehicleType] = useState(false);
  const [deleteSingleBookList, setDeleteSingleBookList] = useState(null);
  const [openModalDeleteSingleBookList, setOpenModalDeleteSingleBookList] =
    useState(false);
  const router = useRouter();

  const { data: dataMyBookList, isFetching: isFetchingMyCart } =
    useGetMyBookedListQuery();

  // const { data: dataMyBookList, isFetching: isFetchingMyCart } =
  //   useMyOrderList();

  // console.log(dataMyBookList);

  const [bookList, setBookList] = useState([]);

  // console.log("bookList", bookList);

  const [
    deleteBookList,
    {
      isLoading: isLoadingDeleteBookList,
      isError: isErrorDeleteBookList,
      isSuccess: isSuccessDeleteBookList,
      error: errorDeleteBookList,
      data: dataDeleteBookList,
    },
  ] = useDeleteCustomerBookedListMutation();

  useEffect(() => {
    if (isSuccessDeleteBookList) {
      addToast(
        dataDeleteBookList?.message || "Ordered Product canceled successfully",
        {
          appearance: "success",
        }
      );
    }
  }, [isSuccessDeleteBookList]);
  useEffect(() => {
    if (isErrorDeleteBookList) {
      addToast(errorDeleteBookList?.data?.message, {
        appearance: "error",
      });
    }
  }, [isErrorDeleteBookList, errorDeleteBookList]);

  const totalPrice = () => {
    const arrayPrice = dataMyBookList?.map((data, i) =>
      Number(data?.orderGroup?.totalPrice || 0)
    );

    const total = arrayPrice?.reduce((cur, pre) => cur + pre, 0);

    return total;
  };

  return (
    <div className="pb-[80px]">
      {isFetchingMyCart ? (
        <div className="container flex justify-center">
          <svg
            className={`animate-spin h-10 w-10 text-primary`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      ) : dataMyBookList?.length ? (
        <div className="container">
          {/* <input
            type="search"
            placeholder="Search order"
            className="pl-[36px]"
          ></input> */}
          {openModalVehicleType && (
            <Popup
              title="Are you sure you want to cancel ordered item/s?"
              description="Please note that, once cancelled, this cannot be undone."
              onOkClick={() => {
                bookList.forEach((id, i) => {
                  deleteBookList(id)
                    .then(() => {
                      setOpenModalVehicleType(false);
                    })
                    .catch(() => {
                      setOpenModalVehicleType(false);
                    });
                });
              }}
              onCancelClick={() => setOpenModalVehicleType(false)}
              okText="Yes"
              cancelText="No"
              loading={isLoadingDeleteBookList}
            />
          )}

          {openModalDeleteSingleBookList && (
            <Popup
              title="Are you sure to cancel booked item?"
              description="Please note that,
              once removed, this cannot be undone."
              onOkClick={() => {
                deleteBookList(deleteSingleBookList)
                  .then(() => {
                    setOpenModalDeleteSingleBookList(false);
                  })
                  .catch(() => {
                    setOpenModalDeleteSingleBookList(false);
                  });
              }}
              onCancelClick={() => setOpenModalDeleteSingleBookList(false)}
              okText="Delete"
              cancelText="Cancel"
              loading={isLoadingDeleteBookList}
            />
          )}

          <div className="flex flex-wrap items-start">
            <div className="flex-1 mr-[25px] ">
              <div className="flex justify-between items-center py-2 mb-2">
                <div className="flex items-center space-x-2">
                  {/* <input
                    type="checkbox"
                    // checked if two array are same
                    checked={sameArrayIrrespectiveToPosition(
                      bookList,
                      dataMyBookList?.map((data, i) => data?.orderGroup?.id)
                    )}
                    className="checkbox w-4 h-4  border border-gray-500 inline-block"
                    onClick={(e) => {
                      if (e.target.checked) {
                        setBookList((bookList) => {
                          bookList = [];
                          return dataMyBookList?.map((data, i) => {
                            return data?.orderGroup?.id;
                          });
                        });
                      } else {
                        setBookList([]);
                      }
                    }}
                  ></input> */}

                  {/* <span className="inline-block ml-2">
                    Select all items{" "}
                    {bookList.length ? `(${bookList.length} item/s)` : ""}
                  </span> */}
                </div>
                <div className="flex items-center ">
                  <button
                    onClick={() => {
                      setOpenModalVehicleType(true);
                    }}
                  >
                    {bookList.length ? (
                      <TrashIcon className="w-5 h-5 inline-block mr-2 hover:text-error " />
                    ) : (
                      ""
                    )}
                  </button>
                </div>
              </div>

              {[...dataMyBookList]?.reverse().map((data, i) => {
                return Object.keys(data).length ? (
                  <div
                    key={i}
                    className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5"
                  >
                    {/* <input
                      type="checkbox"
                      checked={bookList.includes(data?.orderGroup?.id)}
                      className="checkbox w-4 h-4 m-4 border border-gray-400 inline-block"
                      onClick={(e) => {
                        if (e.target.checked) {
                          const allId = [...bookList, data?.orderGroup?.id];
                          const uniqueList = [...new Set(allId)];
                          setBookList(uniqueList);
                        } else {
                          setBookList(
                            bookList.filter((v, j) => {
                              return v !== data?.orderGroup?.id;
                            })
                          );
                        }
                      }}
                    ></input> */}
                    {/* <div className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0">
                      <img
                        src={data?.productImage?.[0]?.imageUrl}
                        alt="image description"
                        className="w-full h-[110px] object-contain"
                        onClick={() => {
                          router.push(`/product-listing/${data?.orderGroup?.accessoriesId}`);
                        }}
                      />
                    </div> */}

                    {/* {data?.orderGroup?.deliveryStatus === "Ordered" && (
                      <span
                        className="close absolute top-3 right-4 opacity-0 transition-all group-hover:cursor-pointer group-hover:opacity-90 hover:text-error"
                        onClick={() => {
                          setOpenModalDeleteSingleBookList(true);
                          setDeleteSingleBookList(data?.orderGroup?.id);
                        }}
                      >
                        <XIcon className="w-5 h-5"></XIcon>
                      </span>
                    )} */}
                    <div
                      className="detail-holder w-full lg:w-auto lg:flex-1"
                      onClick={() => {
                        router.push(`/ordered/${data?.orderGroup?.id}`);
                      }}
                    >
                      <div className="order-info">
                        <div className="pull-left">
                          <div className="info-order-left-text">
                            Order{" "}
                            <span className="order-link">{`#${data?.orderGroup?.orderTrackingNumber}`}</span>
                          </div>
                          <p className="text info desc">
                            Placed on{" "}
                            {`${new Date(
                              data?.orderGroup?.createdAt
                            ).toLocaleString()}`}
                          </p>
                        </div>
                      </div>

                      <div className="flex justify-between mr-1 ">
                        <div>
                          <div className=" flex justify-between ">
                            <span className="price font-bold text-primary-light">
                              NRs.{" "}
                              {thousandNumberSeparator(
                                data?.orderGroup?.totalPrice
                              )}
                            </span>
                          </div>
                        </div>
                      </div>

                      <div className="other-details">
                        <p className="text-sm">
                          {/* <span className="font-semibold">
                            Shipping Address :&nbsp;
                            {data?.orderGroup?.shippingAddressLocation?.combineLocation
                              ?.split?.(">")
                              ?.join?.("/")}
                            &nbsp;, &nbsp;
                          </span> */}
                          <span className="font-semibold">
                            Exact Location :&nbsp;
                            {
                              data?.orderGroup?.shippingAddressLocation
                                .exactLocation
                            }
                            &nbsp;, &nbsp;
                          </span>
                          <span className="font-semibold">
                            Nearby Location :&nbsp;
                            {
                              data?.orderGroup?.shippingAddressLocation
                                .nearByLocation
                            }
                          </span>
                        </p>
                      </div>
                      <div className="other-details">
                        {/* totalPrice: "25220" totalQuantity: "5"
                        totalShippingCharge: "88" */}
                        <p className="text-sm">
                          <span className="font-semibold">
                            Total Quantity :{" "}
                            {thousandNumberSeparator(
                              data?.orderGroup?.totalQuantity
                            )}
                            ,
                          </span>{" "}
                          <span className="font-semibold">
                            Total Shipping Charge : NRs.{" "}
                            {thousandNumberSeparator(
                              data?.orderGroup?.totalShippingCharge
                            )}
                          </span>{" "}
                          <span className="font-semibold">
                            Total Price{" "}
                            <span className="text-[#b7b8b7]">
                              (including Shipping Charge){" "}
                            </span>
                            : NRs.{" "}
                            {thousandNumberSeparator(
                              data?.orderGroup?.totalPrice
                            )}
                            ,
                          </span>{" "}
                        </p>
                      </div>

                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">
                            Delivery Start : &nbsp;
                            {new Date(
                              data?.orderGroup?.deliveryStart
                            )?.toLocaleString?.()}
                          </span>
                        </p>
                      </div>
                      <div className="other-details">
                        <p className="text-sm">
                          <span className="font-semibold">
                            Expected Delivery : &nbsp;
                            {new Date(
                              data?.orderGroup?.deliveryEnd
                            )?.toLocaleString?.()}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                ) : null;
              })}
            </div>
            <div className="w-full md:w-[320px] lg:w-[380px] xl:w-[400px] bg-white rounded-lg shadow-lg border border-gray-300 mt-[46px] py-4 px-6 sticky top-[75px]">
              <h1 className="h5 mb-3">Order Summary</h1>
              <div className="flex flex-wrap justify-between ">
                <p> Total Price </p>
                <p>NRs. {thousandNumberSeparator(totalPrice(bookList))}</p>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
          <div className="max-w-full">
            <img src="/images/empty-cart.svg" />
          </div>
          <div className="text-holder">
            <h1 className="h4">Oops! Your Order list is empty</h1>
            <p>
              Looks like you have not made your <br />
              choice yet
            </p>
            <Link href={"/product-listing"}>
              <a className="btn btn-primary">Continue Browsing</a>
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default ReadBookedList;
