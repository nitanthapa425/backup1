import Button from "components/Button";
import Input from "components/Input";
import { Form, Formik } from "formik";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import React, { useEffect, useRef, useState } from "react";
import { useToasts } from "react-toast-notifications";
import { useAddCustomerEmailMutation } from "services/api/customer";

import { EmailValidation } from "validation/yupValidations";
import * as yup from "yup";

const AddEmail = ({
  setIsOpen = () => {},
  refetchCustomerProfile = () => {},
}) => {
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);
  const { addToast } = useToasts();

  const formikBag = useRef();
  const [
    addCustomerEmail,
    {
      isError: isErrorAdd,
      isSuccess: isSuccessAdd,
      error: errorAdd,
      isLoading: isLoadingAdd,
      data: dataAdd,
    },
  ] = useAddCustomerEmailMutation();
  useEffect(() => {
    if (isSuccessAdd) {
      refetchCustomerProfile();
      formikBag.current?.resetForm();

      addToast(dataAdd.message || "Email is added successfully.", {
        appearance: "success",
      });
      setIsOpen(false);
      setChanged(false);
    }
    if (isErrorAdd) {
      addToast(errorAdd?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAdd, isErrorAdd]);

  const handleOnSubmit = (values, { resetForm, setSubmitting }) => {
    addCustomerEmail(values);

    setSubmitting(false);
  };

  const initial = {
    email: "",
  };
  const initialValues = () => {
    return initial;
  };

  const validationSchema = yup.object({
    email: EmailValidation(true),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues()}
        onSubmit={handleOnSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          isSubmitting,
          dirty,
        }) => (
          <div>
            <h3 className="mb-3">Add Email</h3>

            <Form onChange={() => setChanged(true)}>
              <Input required={true} name={`email`} label="Email" type="text" />

              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  disabled={!dirty || isSubmitting}
                  loading={isLoadingAdd}
                >
                  Add
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    // setOpenModal(true);
                    resetForm();
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {/* {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setChanged(false);

                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )} */}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </div>
  );
};

export default AddEmail;
