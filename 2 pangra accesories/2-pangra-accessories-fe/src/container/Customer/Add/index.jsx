import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import { useToasts } from "react-toast-notifications";

import Button from "components/Button";

import Input from "components/Input";
// import AdminLayout from 'layouts/Admin';

import {
  useCustomerSignUpMutation,
  useCustomerSignUpSparrowMutation,
  useReadCustomerQuery,
  useUpdateCustomerMutation,
} from "services/api/customer";

import {
  customerUpdateValidationSchema,
  customerValidationSchema,
} from "validation/customer.validation";
import Popup from "components/Popup";
// import { useRouter } from 'next/router';
import PasswordInput from "components/Input/PasswordInput";
import Select from "components/Select";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import { beforeAfterDate } from 'utils/beforeAfterdate';
// import UserLayout from 'layouts/User';
import DropZone from "components/DropZone";
// import { firstLetterCapital } from 'utils/firstLetterCapita.';
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { customerActions } from "store/features/customerAuth/customerAuthSlice";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Link from "next/link";
import { sparrowToken } from "config";

const CustomerSignUp = ({ type = "add" }) => {
  const [formDataa, setFormDataa] = useState("");
  const dispatch = useDispatch();
  const router = useRouter();
  const customerId = router?.query?.id;
  const {
    data: customerProfile,
    // error: customerProfileFetchError,
    // isLoading: isLoadingCustomerProfile,
    // while loading show loading
  } = useReadCustomerQuery(customerId, {
    skip: !customerId,
  });

  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const genders = ["Male", "Female", "Other"];

  const formikBag = useRef();

  const [
    createCustomer,
    {
      isLoading: isLoadingCreate,
      isError: isErrorCreate,
      isSuccess: isSuccessCreate,
      error: errorCreate,
      data: dataCreate,
    },
  ] = useCustomerSignUpMutation();
  const [createCustomerSparrow] = useCustomerSignUpSparrowMutation();

  useEffect(() => {
    if (isSuccessCreate) {
      localStorage.setItem("otpId", dataCreate.id);
      localStorage.setItem("phone", dataCreate.mobile);
      dispatch(customerActions.setOtpId(dataCreate.id));
      dispatch(customerActions.setPhone(dataCreate.mobile));

      formikBag.current?.resetForm();

      addToast(
        dataCreate?.message || "Customer details has been added successfully.",
        {
          appearance: "success",
        }
      );

      createCustomerSparrow(formDataa);
      setChanged(false);
      router.push("/confirmMobile");
    }
  }, [isSuccessCreate]);

  // useEffect(() => {
  //   return removeAllToasts;
  // }, []);

  useEffect(() => {
    if (isErrorCreate) {
      addToast(
        errorCreate?.data?.message,
        // || 'Error occurred while adding customer. Please try again later.',
        {
          appearance: "error",
        }
      );
      // createCustomerSparrow(formDataa);
    }
  }, [isErrorCreate]);
  const createInitialValue = {
    firstName: "",
    middleName: "",
    lastName: "",
    mobile: "",
    password: "",
    confirmPassword: "",
    receiveNewsletter: false,
    // email: '',
    // gender: '',
    // dob: new Date(),
  };
  const updateInitialValue = {
    firstName: customerProfile?.firstName,
    middleName: customerProfile?.middleName,
    lastName: customerProfile?.lastName,
    mobile: customerProfile?.mobile,
    email: customerProfile?.email,
    gender: customerProfile?.gender,
    dob: customerProfile?.dob,
    profileImagePath: customerProfile?.profileImagePath,
    receiveNewsletter: customerProfile?.receiveNewsletter,
  };

  // const BreadCrumbList = [];

  // BreadCrumbList = [
  //   {
  //     routeName: 'Add Product',
  //     route: '/admin/product/create',
  //   },
  //   {
  //     routeName: 'Add Customer',
  //     route: '/customer/add',
  //   },
  // ];
  const [
    updateCustomer,
    {
      isLoading: isLoadingUpdate,
      isError: isErrorUpdate,
      isSuccess: isSuccessUpdate,
      error: errorUpdate,
      data: dataUpdateCustomer,
    },
  ] = useUpdateCustomerMutation();

  useEffect(() => {
    if (isSuccessUpdate) {
      formikBag.current?.resetForm();

      addToast(
        dataUpdateCustomer?.message ||
          "Customer details has been updated successfully.",
        {
          appearance: "success",
        }
      );
      setChanged(false);

      router.push(`/customer/details/${customerId}`);
    }
  }, [isSuccessUpdate]);

  useEffect(() => {
    if (isErrorUpdate) {
      addToast(
        errorUpdate?.data?.message,
        // || 'Error occurred while adding customer. Please try again later.',
        {
          appearance: "error",
        }
      );
    }
  }, [isErrorUpdate]);

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", {});
    }
  };

  const updateSubmit = (values, { resetForm, setSubmitting }) => {
    const payloadData = {
      id: values.id,
      firstName: values.firstName,
      middleName: values.middleName,
      lastName: values.lastName,
      email: values.email,
      mobile: values.mobile,
      gender: values.gender,
      dob: values.dob,
      profileImagePath: values?.profileImagePath,
      receiveNewsletter: values.receiveNewsletter,
    };
    updateCustomer({ ...payloadData, id: customerId });
    setSubmitting(false);
  };

  const createSubmit = (values, { resetForm, setSubmitting }) => {
    const formData = new FormData();

    const randomNumber = Math.floor(10000 + Math.random() * 90000);
    const payloadData = {
      firstName: values.firstName,
      middleName: values.middleName,
      lastName: values.lastName,
      password: values.password,
      mobile: values.mobile,
      dob: new Date(),
      verificationToken: `${randomNumber}`,
      receiveNewsletter: values.receiveNewsletter,
    };
    formData.append("token", sparrowToken);
    formData.append("from", "TUKIALERT");
    formData.append("to", values.mobile);
    formData.append(
      "text",
      `Dear ${values.firstName} ${values.middleName} ${values.lastName}, \n ${randomNumber} is your account verification code. \n Thank you !`
    );

    localStorage.setItem("mobile", values.mobile);
    localStorage.setItem("firstName", values.firstName);
    localStorage.setItem("middleName", values.middleName);
    localStorage.setItem("lastName", values.lastName);

    setFormDataa(formData);

    createCustomer(payloadData);
    // createCustomerSparrow(formData);
    setSubmitting(false);
  };
  return (
    // <AdminLayout
    //   documentTitle={'Add New Customer'}
    //   BreadCrumbList={BreadCrumbList}
    // >
    <Formik
      initialValues={type === "add" ? createInitialValue : updateInitialValue}
      onSubmit={type === "add" ? createSubmit : updateSubmit}
      validationSchema={
        type === "add"
          ? customerValidationSchema
          : customerUpdateValidationSchema
      }
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        setFieldTouched,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          {/* <h3 className="mb-3">{firstLetterCapital(type)} Customer</h3> */}
          <h3 className="mb-3">
            {type === "add" ? "SignUp" : "Edit Customer Details"}
          </h3>

          <Form onChange={() => setChanged(true)}>
            <div className="row-sm  pt-5 pb-2">
              <div className="three-col-sm">
                <Input
                  label="First Name"
                  name="firstName"
                  type="text"
                  placeholder="E.g: John"
                />
              </div>

              <div className="three-col-sm">
                <Input
                  label="Middle Name"
                  name="middleName"
                  type="text"
                  placeholder="E.g: Jung"
                  required={false}
                />
              </div>
              <div className="three-col-sm">
                <Input
                  label="Last Name"
                  name="lastName"
                  type="text"
                  placeholder="E.g: Deo"
                />
              </div>
              {type === "add" && (
                <>
                  <div className="three-col-sm">
                    <Input
                      label="Mobile Number"
                      name="mobile"
                      type="text"
                      placeholder="E.g: 98XXXXXXXX"
                    />
                  </div>
                </>
              )}
              {type === "add" && (
                <>
                  <div className="three-col-sm">
                    <PasswordInput
                      label="Password"
                      name="password"
                      placeholder="Password"
                    ></PasswordInput>
                  </div>
                  <div className="three-col-sm">
                    <PasswordInput
                      label="Confirm Password"
                      name="confirmPassword"
                      placeholder="Confirm Password"
                    ></PasswordInput>
                  </div>
                </>
              )}

              {type === "edit" && (
                <>
                  <div className="three-col-sm">
                    <Input
                      label="Email"
                      name="email"
                      type="email"
                      placeholder="E.g: john320@gmail.com"
                      required={false}
                    />
                  </div>
                  <div className="three-col-sm">
                    <label htmlFor="">
                      Date Of Birth(AD)
                      <span className="required">*</span>
                    </label>
                    <DatePicker
                      selected={values?.dob ? new Date(values?.dob) : null}
                      onChange={(date) => {
                        setFieldValue("dob", date?.toLocaleDateString());
                        setFieldTouched("dob");
                      }}
                      onBlur={() => {
                        setFieldTouched("dob");
                      }}
                      dateFormat="MM/dd/yyyy"
                      placeholderText="mm/dd/yyyy"
                      // minDate={beforeAfterDate(new Date(), 0, 0, 0)}
                      maxDate={new Date()}
                    />

                    {touched.dob && errors.dob && (
                      <div className="text-primary-dark">{errors.dob}</div>
                    )}
                  </div>
                  <div className="three-col-sm">
                    <Select label="Gender" name="gender">
                      <option value="">Select Gender</option>

                      {genders.map((gender, i) => (
                        <option key={i} value={gender}>
                          {gender}
                        </option>
                      ))}
                    </Select>
                  </div>
                  <div className="three-col-sm">
                    <div className="">
                      <div>
                        <DropZone
                          required={true}
                          label="Upload Profile Image"
                          currentFiles={
                            Object.keys(values.profileImagePath || {}).length
                              ? [values.profileImagePath]
                              : []
                          }
                          setNewFiles={handleProfileImagePath}
                          handleRemoveFile={handleRemoveProfileImagePath}
                          error={
                            touched?.profileImagePath
                              ? errors?.profileImagePath
                              : ""
                          }
                        />
                      </div>
                    </div>
                  </div>
                </>
              )}

              {/* {console.log(values.receiveNewsletter)} */}

              <div className="three-col-sm">
                <Input
                  values={values.receiveNewsletter}
                  label="First Name"
                  name="receiveNewsletter"
                  type="checkbox"
                  placeholder="E.g: John"
                  required={false}
                  description="Do you want to subscribe"
                  onChange={(e) => {
                    setFieldValue("receiveNewsletter", e.target.checked);
                  }}
                />
              </div>
            </div>
            <div className="btn-holder mt-2">
              <Button
                type="submit"
                disabled={!dirty}
                loading={isLoadingCreate || isLoadingUpdate}
              >
                {"Submit"}
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  setOpenModal(true);
                }}
                disabled={!dirty}
              >
                Clear
              </Button>

              {openModal && (
                <Popup
                  title="Do you want to clear all fields?"
                  description="if you clear all the filed will be removed"
                  onOkClick={() => {
                    resetForm();
                    setChanged(false);

                    setOpenModal(false);
                  }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )}

              <Link href="/">
                <Button type="button" variant="link">
                  Back
                </Button>
              </Link>
            </div>
          </Form>
        </div>
      )}
    </Formik>
    // </AdminLayout>
  );
};

export default CustomerSignUp;
