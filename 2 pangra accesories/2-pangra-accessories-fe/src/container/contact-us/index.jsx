const ContactUs = () => {
  return (
    <div>
      <main id="main">
        <section className="vehicle-listing-section pt-10 pb-20">
          <div className="container">
            <h1 className="h3 mb-4">Contact Us</h1>
            ADDRESS<br></br> 2Pangra(P) Ltd.<br></br> 37 Ashok Galli <br></br>{" "}
            Amrit Marg, Thamel - 26 <br></br> Kathmandu 44600<br></br> Nepal
          </div>
        </section>
      </main>
    </div>
  );
};

export default ContactUs;
