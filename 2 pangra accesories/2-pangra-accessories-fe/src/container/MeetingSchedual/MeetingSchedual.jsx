import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import * as yup from "yup";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import Button from "components/Button";
import Input from "components/Input";

import { useToasts } from "react-toast-notifications";

import {
  useMeetingScheduleMutation,
  useReadSingleMeetingScheduleQuery,
  useUpdateSingleMeetingScheduleMutation,
} from "services/api/seller";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useGetDetailLocationQuery } from "services/api/getFullLocation";

const MeetingSchedule = ({ type = "add", bookId }) => {
  const [editInitialValue, seteditInitialValue] = useState({});

  const { addToast, removeAllToasts } = useToasts();

  const { data: dataSchedule } = useReadSingleMeetingScheduleQuery(bookId);

  const formikSubModelBag = useRef();
  const [
    meetingSchedule,
    {
      isError: isErrorCreate,
      isLoading: isLoadingCreate,
      isSuccess: isSuccessCreate,
    },
  ] = useMeetingScheduleMutation();

  const [
    updateMeetingSchedule,
    {
      isError: isErrorUpdate,
      isLoading: isLoadingUpdate,
      isSuccess: isSuccessUpdate,
    },
  ] = useUpdateSingleMeetingScheduleMutation();
  const { data: combineLocations, isFetching: isFetchingCombineLocation } =
    useGetDetailLocationQuery();

  useEffect(() => {
    return () => {
      if (isSuccessCreate) {
        // formikSubModelBag.current?.resetForm();
        addToast("Meeting schedule successfully", {
          appearance: "success",
        });
      }
      if (isErrorCreate) {
        addToast("Unable to schedule meeting", {
          appearance: "error",
        });
      }
    };
  }, [isSuccessCreate, isErrorCreate]);

  useEffect(() => {
    return () => {
      if (isSuccessUpdate) {
        // formikSubModelBag.current?.resetForm();
        addToast("Meeting schedule update successfully", {
          appearance: "success",
        });
      }
      if (isErrorUpdate) {
        addToast("Unable to  update schedule meeting", {
          appearance: "error",
        });
      }
    };
  }, [isSuccessUpdate, isErrorUpdate]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (type === "add") meetingSchedule({ values, bookId });
    if (type === "edit") updateMeetingSchedule({ values, bookId });

    setSubmitting(false);
  };

  useEffect(() => {
    // if (dataSchedule) {
    const newScheduleDetail = {
      combineLocation: dataSchedule?.combineLocation || "",
      nearByLocation: dataSchedule?.nearByLocation || "",
      meetingPlace: dataSchedule?.meetingPlace || "",
      meetingDateTime: dataSchedule?.meetingDateTime || new Date(),
    };
    seteditInitialValue(newScheduleDetail);
    // }
  }, [dataSchedule]);

  const addInitialValue = {
    combineLocation: "",
    nearByLocation: "",
    meetingPlace: "",
    meetingDateTime: new Date(),
  };

  useEffect(() => {
    return removeAllToasts;
  }, []);

  const validationSchema = yup.object({
    combineLocation: yup
      .string()
      .required("Location is required.")
      .min(2, "Location must be at least 2 characters")
      .max(150, "Location must be at must 150 characters."),
    nearByLocation: yup
      .string()
      .required("Nearby location  is required.")
      .min(2, "Nearby location  must be at least 2 characters")
      .max(150, "Nearby location  must be at must 150 characters."),
    meetingPlace: yup
      .string()
      .required("Meeting place is required.")
      .min(2, "Meeting place must be at least 2 characters")
      .max(150, "Meeting place must be at must 150 characters."),
    meetingDateTime: yup.string().required("Meeting Date Time is required."),
  });

  return (
    <Formik
      initialValues={type === "edit" ? editInitialValue : addInitialValue}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
      innerRef={formikSubModelBag}
      enableReinitialize
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        validateForm,
        setTouched,
        dirty,
        setFieldTouched,
        isValid,
        isSubmitting,
      }) => {
        return (
          <Form>
            <div className="flex justify-center pt-3 pb-10">
              <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-xl mx-auto">
                <h3 className="mb-3">Meeting Schedule</h3>
                <div className="mb-2">
                  <SelectWithoutCreate
                    required
                    loading={isFetchingCombineLocation}
                    label="Select Location"
                    placeholder="Location"
                    error={
                      touched?.combineLocation ? errors?.combineLocation : ""
                    }
                    value={values?.combineLocation || null}
                    onChange={(selectedValue) => {
                      setFieldValue("combineLocation", selectedValue.value);
                    }}
                    options={combineLocations?.map((value) => ({
                      label: value.fullAddress,
                      // value: `${value.id}`,
                      value: value.fullAddress,
                    }))}
                    onBlur={() => {
                      setFieldTouched("combineLocation");
                    }}
                  />
                </div>
                <div className="mb-2">
                  <Input
                    required={true}
                    name="nearByLocation"
                    label="Nearby Location"
                    type="text"
                  />
                </div>
                <div className="mb-2">
                  <Input
                    required={true}
                    name="meetingPlace"
                    label="Meeting Place"
                    type="text"
                  />
                </div>

                <label htmlFor="">Meeting Date</label>

                <DatePicker
                  selected={
                    values?.meetingDateTime
                      ? new Date(values?.meetingDateTime)
                      : null
                  }
                  onChange={(date) => {
                    setFieldValue("meetingDateTime", date);
                    setFieldTouched("meetingDateTime");
                  }}
                  onBlur={() => {
                    setFieldTouched("meetingDateTime");
                  }}
                  todayButton="Today"
                  // timeInputLabel="Time:"
                  // dateFormat="MM/dd/yyyy"
                  // dateFormat="MM/dd/yyyy h:mm aa"
                  placeholderText="mm/dd/yyyy"
                  minDate={new Date()}
                  // showTimeInput
                  //   maxDate={beforeAfterDate(new Date(), 0, 0, 0)}

                  showTimeSelect
                  // showTimeSelectOnly
                  timeIntervals={15}
                  timeCaption="Time"
                  // dateFormat="h:mm aa"
                  dateFormat="MM/dd/yyyy h:mm aa"
                />

                <div className="btn-holder mt-3">
                  <Button
                    type="submit"
                    loading={isLoadingCreate || isLoadingUpdate}
                    disabled={isSubmitting || !dirty}
                  >
                    {type === "add" ? "Submit" : "Update"}
                  </Button>

                  <Button
                    variant="outlined-error"
                    type="reset"
                    disabled={isSubmitting || !dirty}
                  >
                    Clear
                  </Button>
                </div>
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default MeetingSchedule;
