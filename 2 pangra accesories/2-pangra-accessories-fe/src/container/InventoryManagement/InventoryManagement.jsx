import { useEffect } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";

// import { useUpdateOrderQuantityMutation } from "services/api/ProductService";

import "react-datepicker/dist/react-datepicker.css";
import { UpdateOrderedQuantityValidation } from "validation/product.validation";
import { useUpdateOrderQuantityMutation } from "services/api/Inventory/inventoryService";
import Input from "components/Input";

const initialValues = {
  quantity: "",
  productId: "",
};

const InventoryManagement = ({ orderQuantityId, setIsOpen }) => {
  const { addToast } = useToasts();

  const [
    updateOrderQuantity,
    {
      isSuccess,
      isError,
      isLoading: isLoadingUpdatingOrderQuantity,
      error,
      data: dataUpdateOrderQuantity,
    },
  ] = useUpdateOrderQuantityMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast(
        dataUpdateOrderQuantity?.message ||
          "Ordered quantity is updated successfully",
        {
          appearance: "success",
        }
      );

      setIsOpen(false);
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast(
        error?.data?.message ||
          "error occurred while updating ordered quantity",
        {
          appearance: "error",
        }
      );
    }
  }, [isError]);

  return (
    <Formik
      initialValues={initialValues}
      // onSubmit={createOffer}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        // console.log({ body: values, id: orderQuantityId });
        // setOrderQuantityId({
        //   inventoryId: original?.id,
        //   productId: original?.productId,
        // });

        updateOrderQuantity({
          body: {
            ...values,
            productId: orderQuantityId.productId,
          },
          id: orderQuantityId.inventoryId,
        });
        setSubmitting(false);
      }}
      validationSchema={UpdateOrderedQuantityValidation}
      enableReinitialize
    >
      {({
        setFieldValue,
        setFieldTouched,
        values,
        errors,
        touched,
        // resetForm,
        // isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <h1 className="h6 mb-3">Manage Ordered Quantity</h1>

          <Form>
            <div className="w-full mb-2">
              <div className="mb-2">
                <Input
                  label="Number of Quantity"
                  name="quantity"
                  type="text"
                  placeholder="E.g: 5"
                />
              </div>
            </div>

            <div className="mt-3">
              <Button
                loading={isLoadingUpdatingOrderQuantity}
                disabled={isLoadingUpdatingOrderQuantity || !dirty}
                type="submit"
                className="btn-lg"
              >
                Submit
              </Button>

              <Button
                disabled={isLoadingUpdatingOrderQuantity || !dirty}
                type="reset"
                variant="outlined-error"
              >
                Clear
              </Button>
            </div>
          </Form>
        </div>
      )}
    </Formik>
  );
};
export default InventoryManagement;
