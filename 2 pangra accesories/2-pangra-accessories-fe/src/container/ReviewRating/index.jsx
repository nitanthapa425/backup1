import Table from "components/Table/table";
import AdminLayout from "layouts/Admin";
import { useState, useEffect, useMemo } from "react";
import { getQueryStringForReview } from "utils/getQueryStringForTable";

import {
  useReadAllRatingReviewQuery,
  useDeleteRatingReviewProductMutation,
  useApproveRatingReviewProductMutation,
  useDisapproveRatingReviewProductMutation,
} from "services/api/seller";
import { useToasts } from "react-toast-notifications";
import { SelectColumnFilter } from "components/Table/Filter";
// import Button from "components/Button";
// import Button from "components/Button";

function RatingReviewContainer() {
  const { addToast } = useToasts();
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [reviewDataQuery, setReviewDataQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: reviewData,
    isError: reviewError,
    isFetching: isLoadingReview,
  } = useReadAllRatingReviewQuery(reviewDataQuery);

  const {
    data: reviewDataAll,
    // isError: isreviewErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllRatingReviewQuery("?&sortBy=createdAt&sortOrder=-1", {
    skip: skipTableDataAll,
  });
  useEffect(() => {
    if (reviewDataAll) {
      setTableDataAll(
        reviewDataAll.ratingAndReview?.docs?.map((rating) => {
          return {
            id: rating?._id,
            productTitle: rating?.productTitle,
            fullName: rating?.fullName,
            ratingComment: rating.ratingComment,
            ratingNumber: rating.ratingNumber,
            status: rating.status,
          };
        })
      );
    }
  }, [reviewDataAll]);
  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteRatingReviewProductMutation();
  const [
    approveReview,
    {
      error: errorApproveReview,
      isLoading: isApprovingReview,
      isSuccess: isSuccessApproved,
      data: dataApproveReview,
    },
  ] = useApproveRatingReviewProductMutation();

  const [
    disapproveReview,
    {
      error: errorDisApproveReview,
      isLoading: isDisApprovingReview,
      isSuccess: isSuccessDisApproved,
      data: dataDisapproveReview,
    },
  ] = useDisapproveRatingReviewProductMutation();
  const showSuccessToast = (message) => {
    addToast(message, {
      appearance: "success",
    });
  };

  const showFailureToast = (message) => {
    addToast(message, {
      appearance: "error",
    });
  };

  useEffect(() => {
    if (errorApproveReview) {
      showFailureToast(
        errorApproveReview?.data?.message || "Failed to approve Review"
      );
    }
    if (isSuccessApproved) {
      showSuccessToast(
        dataApproveReview?.message || "Review is successfully approved "
      );
    }
  }, [errorApproveReview, isSuccessApproved]);

  useEffect(() => {
    if (errorDisApproveReview) {
      showFailureToast(
        errorDisApproveReview?.data?.message ||
          "Failed to disapprove the review"
      );
    }
    if (isSuccessDisApproved) {
      showSuccessToast(
        dataDisapproveReview?.message || "Review is successfully disapproved"
      );
    }
  }, [errorDisApproveReview, isSuccessDisApproved]);

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "fullName",
        Header: "Reviewed By",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "ratingComment",
        Header: "Rating Comment",
        accessor: "ratingComment",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "ratingNumber",
        Header: "Rating Number",
        accessor: "ratingNumber",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "status",
        Header: "Status",
        accessor: "status",
        Cell: ({ cell: { value } }) => (value ? " Approved" : "Disapproved"),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Approved",
            value: true,
          },
          {
            label: "Disapproved",
            value: false,
          },
        ],
      },
      // {
      //   id: "changeStatus",
      //   Header: "Change Status",
      //   accessor: "changeStatus",
      //   Cell: ({ row: { original }, cell: { value } }) => {
      //     return (
      //       <div>
      //         <Button>Approve</Button>
      //       </div>
      //     );
      //   },
      // },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForReview(pageIndex, pageSize, sortBy, filters);
    setReviewDataQuery(query);
  };

  useEffect(() => {
    if (reviewData) {
      setPageCount(reviewData.totalPages);
      setTotalData(reviewData.totalDocs);
      setTableData(
        reviewData.ratingAndReview?.docs?.map((rating) => {
          return {
            id: rating?._id,
            productTitle: rating?.productTitle,
            fullName: rating?.fullName,
            ratingComment: rating.ratingComment,
            ratingNumber: rating.ratingNumber,
            status: rating.status,
          };
        })
      );
    }
  }, [reviewData]);

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Rating-Review List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle="View Rating Review"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableName="Rating Review/s"
            tableDataAll={tableDataAll}
            setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={reviewError}
            isLoadingData={isLoadingReview}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="admin/category/update"
            // viewRoute="admin/category"
            deleteQuery={deleteItems}
            isDeleting={isDeleting}
            isDeleteError={isDeleteError}
            isDeleteSuccess={isDeleteSuccess}
            DeletedError={DeletedError}
            hasExport={true}
            // addPage={{ page: "Add Category", route: "/admin/category/create" }}
            hasDropdownBtn={true}
            dropdownBtnName="More"
            dropdownBtnOptions={[
              {
                name: "Approve",
                // onClick: approveReview,
                onClick: approveReview,
                isLoading: isApprovingReview,
                loadingText: "Approving...",
              },
              {
                name: "DisApprove",
                onClick: disapproveReview,
                isLoading: isDisApprovingReview,
                loadingText: "DisApproving...",
              },
            ]}
          />
        </div>
      </section>
    </AdminLayout>
  );
}

export default RatingReviewContainer;
