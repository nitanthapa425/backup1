import React, { useCallback, useEffect, useRef, useState } from "react";
import Steps, { Step } from "rc-steps";
import "rc-steps/assets/index.css";
import "rc-steps/assets/iconfont.css";
import { useAddVehicleInFeatureCustomerMutation } from "services/api/seller";
import { useToasts } from "react-toast-notifications";
import { Form, Formik } from "formik";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import Button from "components/Button";
import Popup from "components/Popup";
// import { Line } from 'rc-progress';
import { ConnectedFocusError } from "focus-formik-error";
import { AddToFeatureData } from "../adminData";
import * as yup from "yup";

const FeatureSteps = ({ closeModal, postVehicleId }) => {
  const formikBag = useRef();
  //   const level = useSelector((state) => state.levelReducer.level);
  const [openModal, setOpenModal] = useState(false);

  const [currentStep, setCurrentStep] = useState(0);

  const [
    addVehicleInFeature,
    {
      isLoading: isLoadingFeature,
      isError: isErrorFeature,
      error: errorFeature,
      isSuccess: isSuccessFeature,
      data: dataFeature,
    },
  ] = useAddVehicleInFeatureCustomerMutation();

  const { addToast } = useToasts();

  const addInitialFeature = {
    featureTime: "",
  };

  const isLastStep = currentStep === AddToFeatureData.length - 1;

  const _submitForm = (values, actions) => {
    addVehicleInFeature({ id: postVehicleId, values });
    actions.setSubmitting(false);
  };

  const handleSubmit = (values, actions) => {
    if (isLastStep) {
      _submitForm(values, actions);
      //   console.log(values);
    } else {
      setCurrentStep((prev) => prev + 1);
      // actions.setTouched({});
      actions.setSubmitting(false);
    }
  };

  useEffect(() => {
    if (isSuccessFeature) {
      // formikBag.current?.resetForm();

      addToast(
        dataFeature?.message ||
          "Your Vehicle  has been added in featured list  successfully.",
        {
          appearance: "success",
        }
      );

      // setOpenModalFeature(false);
      // router.push(level'/seller/view');
      // router.push(
      //   level === 'superAdmin'
      //     ? '/seller/view'
      //     : level === 'customer'
      //     ? '/sell/view'
      //     : ''
      // );
      closeModal();
    }
    if (isErrorFeature) {
      addToast(
        errorFeature?.data?.message ||
          "Error occurred while adding vehicle to featured bike",
        {
          appearance: "error",
        }
      );
      closeModal();
    }
  }, [isSuccessFeature, isErrorFeature]);

  // see why
  const handleBackClick = useCallback(
    () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null),
    [currentStep]
  );

  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const feature = [
    yup.object({}),
    yup.object({
      featureTime: yup.string().required("Feature Time is required field"),
    }),
  ];

  return (
    <section className="horizontal-steps pb-0 ">
      <Formik
        initialValues={addInitialFeature}
        validationSchema={feature[currentStep]}
        onSubmit={handleSubmit}
        innerRef={formikBag}
        enableReinitialize
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          validateForm,
          setTouched,
          dirty,
          setFieldTouched,
          isValid,
        }) => {
          return (
            <Form className="px-5">
              <ConnectedFocusError />

              <Steps current={currentStep}>
                {AddToFeatureData.map(
                  ({ id, title, component: StepContent, key }, index) => (
                    <Step
                      key={id + key}
                      title={
                        <span
                          className="cursor-pointer"
                          onClick={() => {
                            if (id < currentStep) {
                              setCurrentStep(id);
                            }
                          }}
                        >
                          {title}
                        </span>
                      }
                      description={
                        <>
                          {currentStep === index && (
                            <>
                              <StepContent
                                values={values}
                                setFieldValue={setFieldValue}
                                errors={errors}
                                touched={touched}
                                setChanged={setChanged}
                                dirty={dirty}
                                setFieldTouched={setFieldTouched}
                              />

                              <Button
                                loading={isLoadingFeature}
                                type="submit"
                                variant={isLastStep ? "primary" : "secondary"}
                              >
                                {isLastStep ? "Finish" : "Next"}
                              </Button>
                              <Button
                                onClick={() => {
                                  if (!isLastStep) {
                                    closeModal();
                                  }
                                  setOpenModal(true);
                                }}
                                variant="outlined-error"
                                type="button"
                              >
                                {isLastStep ? "Clear" : "Cancel"}
                              </Button>
                              {openModal && (
                                <Popup
                                  title="Are you sure to clear all fields?"
                                  description="If you clear all fields, the data will not be saved."
                                  onOkClick={() => {
                                    resetForm();
                                    setCurrentStep(0);
                                    setOpenModal(false);
                                  }}
                                  onCancelClick={() => setOpenModal(false)}
                                  okText="Clear All"
                                  cancelText="Cancel"
                                />
                              )}

                              {currentStep !== 0 && (
                                <Button
                                  type="button"
                                  // disabled={isLoading}
                                  onClick={handleBackClick}
                                  variant="link"
                                >
                                  Back
                                </Button>
                              )}
                            </>
                          )}
                        </>
                      }
                    />
                  )
                )}
              </Steps>
            </Form>
          );
        }}
      </Formik>
    </section>
  );
};

export default FeatureSteps;
