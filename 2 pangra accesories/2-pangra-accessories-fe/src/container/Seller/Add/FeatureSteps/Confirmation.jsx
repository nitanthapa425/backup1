import React from 'react';
import { CheckCircleIcon } from '@heroicons/react/outline';

const Confirmation = () => {
  return (
    <div className="mb-6">
      <h6 className="mb-2 mt-2">
        Are you sure you want to add this vehicle in Feature sections?
      </h6>

      <ul className="text-sm space-y-2 pl-2">
        <li className="flex items-center">
          <CheckCircleIcon className="w-4 h-4 mr-[6px]" />
          <span className="inline-block flex-1">
            lorem ipsum dolor sit amet, consectetur adip lorem ipsum dolor sit
          </span>
        </li>
        <li className="flex items-center">
          <CheckCircleIcon className="w-4 h-4 mr-[6px]" />
          <span className="inline-block flex-1">
            lorem ipsum dolor sit amet, consectetur adip lorem ipsum dolor sit
            amet
          </span>
        </li>
        <li className="flex items-center">
          <CheckCircleIcon className="w-4 h-4 mr-[6px]" />
          <span className="inline-block flex-1">
            lorem ipsum dolor sit amet, consectetur adip lorem ipsum dolor sit
            amet
          </span>
        </li>
        <li className="flex items-center">
          <CheckCircleIcon className="w-4 h-4 mr-[6px]" />
          <span className="inline-block flex-1">
            lorem ipsum dolor sit amet, consectetur adip lorem ipsum dolor sit
            amet
          </span>
        </li>
      </ul>
    </div>
  );
};

export default Confirmation;
