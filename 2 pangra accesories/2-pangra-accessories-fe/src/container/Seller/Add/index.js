// import React, { useCallback, useEffect, useRef, useState } from 'react';
// import Steps, { Step } from 'rc-steps';
// import 'rc-steps/assets/index.css';
// import 'rc-steps/assets/iconfont.css';
// import { beforeAfterDate } from 'utils/beforeAfterdate';
// import {
//   useReadSingleSellerQuery,
//   useSellerPostMutation,
//   useUpdateSellerMutation,
// } from 'services/api/seller';
// import { useToasts } from 'react-toast-notifications';
// import { useRouter } from 'next/router';
// import { Form, Formik } from 'formik';
// import { sellVehicle } from 'validation/sellVehicle.validation';
// import { useSelector } from 'react-redux';
// import { useWarnIfUnsavedChanges } from 'hooks/useWarnIfUnsavedChanges';
// import Button from 'components/Button';
// import Popup from 'components/Popup';
// import { Line } from 'rc-progress';
// import { ConnectedFocusError } from 'focus-formik-error';
// import Modal from 'react-modal';
// import FeatureSteps from './FeatureSteps';

const AddSellBike = () => {
  // const formikBag = useRef();
  // const level = useSelector((state) => state.levelReducer.level);
  // const [openModal, setOpenModal] = useState(false);
  // const [postVehicleId, setPostVehicleId] = useState(false);
  // const [modalIsOpen, setIsOpen] = useState(false);

  // const router = useRouter();
  // const sellerId = router?.query?.id;
  // const [currentStep, setCurrentStep] = useState(0);
  // const [
  //   createSellerInfo,
  //   { isError, error, isSuccess, isLoading, data: dataSell },
  // ] = useSellerPostMutation();

  // const closeModal = () => {
  //   setIsOpen(false);
  // };

  // const [
  //   updateSeller,
  //   {
  //     isLoading: updating,
  //     isError: isUpdateError,
  //     isSuccess: isUpdateSuccess,
  //     data: updateSuccessData,
  //   },
  // ] = useUpdateSellerMutation();

  // const [editVehicleSellInitialValue, setEditVehicleSellInitialValue] =
  //   useState({});
  // const { addToast } = useToasts();

  // const {
  //   data: singleSellerDetails,
  //   error: singleSellerFetchError,
  //   // isLoading: isLoadingBrand,
  // } = useReadSingleSellerQuery(sellerId, {
  //   skip: !sellerId,
  // });

  // useEffect(() => {
  //   if (singleSellerFetchError) {
  //     addToast(
  //       'Error occured while fetching Vehicle Sell details. Please try again later.',
  //       {
  //         appearance: 'error',
  //       }
  //     );
  //   }
  // }, [singleSellerFetchError]);

  // const addVehicleSellInitialValue = {
  //   vehicleDetailId: '',
  //   bikeImagePath: [],
  //   billBookImagePath: [],
  //   bikeDriven: '',
  //   expectedPrice: '',
  //   bikeNumber: '',
  //   lotNumber: '',
  //   isNegotiable: true,
  //   color: '',
  //   condition: 'Good/Fair',
  //   mileage: '',
  //   postExpiryDate: beforeAfterDate(new Date(), 0, 2, 0),
  //   hasAccident: false,
  //   ownershipCount: '1',
  //   note: '',
  //   usedFor: '1 Year',
  //   makeYear: '',
  //   location: {
  //     combineLocation: location?.combineLocation,
  //     nearByLocation: location?.nearByLocation,
  //   },
  // };

  // useEffect(() => {
  //   if (singleSellerDetails) {
  //     const newsingleSellerDetails = {
  //       vehicleDetailId: singleSellerDetails?.sellerValue?.vehicleDetailId,
  //       bikeImagePath: singleSellerDetails?.sellerValue?.bikeImagePath,
  //       billBookImagePath: singleSellerDetails?.sellerValue?.billBookImagePath,
  //       bikeDriven: singleSellerDetails?.sellerValue?.bikeDriven,
  //       expectedPrice: singleSellerDetails?.sellerValue?.expectedPrice,
  //       bikeNumber: singleSellerDetails?.sellerValue?.bikeNumber,
  //       lotNumber: singleSellerDetails?.sellerValue?.lotNumber,
  //       isNegotiable: singleSellerDetails?.sellerValue?.isNegotiable,
  //       color: singleSellerDetails?.sellerValue?.color,
  //       condition: singleSellerDetails?.sellerValue?.condition,
  //       postExpiryDate: singleSellerDetails?.sellerValue?.postExpiryDate,
  //       hasAccident: singleSellerDetails?.sellerValue?.hasAccident,
  //       ownershipCount: singleSellerDetails?.sellerValue?.ownershipCount,
  //       note: singleSellerDetails?.sellerValue?.note,
  //       mileage: singleSellerDetails?.sellerValue?.mileage,
  //       usedFor: singleSellerDetails?.sellerValue?.usedFor,
  //       makeYear: singleSellerDetails?.sellerValue?.makeYear,
  //       location: {
  //         combineLocation:
  //           singleSellerDetails?.sellerValue?.location?.combineLocation,
  //         nearByLocation:
  //           singleSellerDetails?.sellerValue?.location?.nearByLocation,
  //       },
  //     };

  //     setEditVehicleSellInitialValue(newsingleSellerDetails);
  //     // setCombineLocationValue(singleSellerDetails?.sellerValue?.location?.province);
  //     // setDistrictValue(singleSellerDetails?.sellerValue?.location?.province);
  //   }
  // }, [singleSellerDetails]);

  // const _getInitialValues = () => {
  //   let initialValues = addVehicleSellInitialValue;
  //   if (type === 'edit') initialValues = editVehicleSellInitialValue;
  //   // else if (type === 'draft') initialValues = currentDraftVehicleDetails;
  //   return initialValues;
  // };
  // // const isLastStep = currentStep === sellVehicleStepsData.length - 1;
  // const isLastStep = currentStep === 2 - 1;

  // const _submitForm = (values, actions) => {
  //   values.location.country = 'Nepal';
  //   if (type === 'add') {
  //     createSellerInfo(values);
  //   }
  //   if (type === 'edit') {
  //     updateSeller({ ...values, id: sellerId });
  //   }
  //   actions.setSubmitting(false);
  // };

  // const handleSubmit = (values, actions) => {
  //   if (isLastStep) {
  //     _submitForm(values, actions);
  //   } else {
  //     setCurrentStep((prev) => prev + 1);
  //     // actions.setTouched({});
  //     actions.setSubmitting(false);
  //   }
  // };

  // useEffect(() => {
  //   if (isSuccess) {
  //     formikBag.current?.resetForm();

  //     addToast('Selling Vehicle Details has been added successfully.', {
  //       appearance: 'success',
  //     });
  //     setPostVehicleId(dataSell?.vehicleId);

  //     setChanged(false);
  //     // setOpenModalFeature(true);
  //     setIsOpen(true);
  //     setCurrentStep(0);
  //     // router.push(level'/seller/view');
  //     // router.push(
  //     //   level === 'superAdmin'
  //     //     ? '/seller/view'
  //     //     : level === 'customer'
  //     //     ? '/sell/view'
  //     //     : ''
  //     // );
  //   }
  //   if (isError) {
  //     addToast(
  //       error?.data?.message ||
  //         'Error occured while creating selling vehicle details. Please try again later.',
  //       {
  //         appearance: 'error',
  //       }
  //     );
  //   }
  // }, [isSuccess, isError]);

  // useEffect(() => {
  //   if (isUpdateError) {
  //     addToast('error occurred while updating vehicle sell.', {
  //       appearance: 'error',
  //     });
  //   }
  // }, [isUpdateError]);

  // useEffect(() => {
  //   if (isUpdateSuccess) {
  //     formikBag.current?.resetForm();
  //     addToast(
  //       updateSuccessData?.message ||
  //         'Selling Vehicle Details updated successfully.',
  //       {
  //         appearance: 'success',
  //       }
  //     );
  //     setChanged(false);

  //     // router.push(`/seller/get/${sellerId}`);
  //     router.push(
  //       level === 'superAdmin'
  //         ? `/seller/get/${sellerId}`
  //         : level === 'customer'
  //         ? `/sell/get/${sellerId}`
  //         : ''
  //     );
  //   }
  //   if (isUpdateError) {
  //     addToast('error occurred while updating vehicle sell.', {
  //       appearance: 'error',
  //     });
  //   }
  // }, [isUpdateSuccess, isUpdateError]);

  // // see why
  // const handleBackClick = useCallback(
  //   () => (currentStep > 0 ? setCurrentStep((prev) => prev - 1) : null),
  //   [currentStep]
  // );

  // const [changed, setChanged] = useState(false);
  // useWarnIfUnsavedChanges(changed);

  return (
    <>HELLO</>
    // <section className="stepper-section-pt-4 mt-5 horizontal-steps ">
    //   <div className="container">
    //     <Formik
    //       initialValues={_getInitialValues()}
    //       validationSchema={sellVehicle[currentStep]}
    //       onSubmit={handleSubmit}
    //       innerRef={formikBag}
    //       enableReinitialize
    //     >
    //       {({
    //         setFieldValue,
    //         values,
    //         errors,
    //         touched,
    //         resetForm,
    //         validateForm,
    //         setTouched,
    //         dirty,
    //         setFieldTouched,
    //         isValid,
    //       }) => {
    //         return (
    //           <Form className="border-[2px] bg-white  border-gray-200 rounded-[20px] m-3 py-4 px-8 pb-0 ">
    //             <ConnectedFocusError />

    //             <Steps current={currentStep}>
    //               {sellVehicleStepsData.map(
    //                 ({ id, title, component: StepContent, key }, index) => (
    //                   <Step
    //                     key={id + key}
    //                     title={
    //                       <span
    //                         className="cursor-pointer"
    //                         onClick={() => {
    //                           if (id < currentStep) {
    //                             setCurrentStep(id);
    //                           }
    //                         }}
    //                       >
    //                         {title}
    //                       </span>
    //                     }
    //                     description={
    //                       <>
    //                         {currentStep === index && (
    //                           <>
    //                             <StepContent
    //                               values={values}
    //                               setFieldValue={setFieldValue}
    //                               errors={errors}
    //                               touched={touched}
    //                               setChanged={setChanged}
    //                               dirty={dirty}
    //                               setFieldTouched={setFieldTouched}
    //                               type={type}
    //                             />

    //                             <Button
    //                               loading={isLoading || updating}
    //                               type="submit"
    //                               variant={isLastStep ? 'primary' : 'secondary'}
    //                             >
    //                               {isLastStep && type === 'add'
    //                                 ? 'Finish'
    //                                 : isLastStep && type === 'edit'
    //                                 ? 'Update'
    //                                 : 'Next'}
    //                             </Button>
    //                             <Button
    //                               onClick={() => {
    //                                 setOpenModal(true);
    //                               }}
    //                               variant="outlined-error"
    //                               type="button"
    //                             >
    //                               Clear
    //                             </Button>
    //                             {openModal && (
    //                               <Popup
    //                                 title="Are you sure to clear all fields?"
    //                                 description="If you clear all fields, the data will not be saved."
    //                                 onOkClick={() => {
    //                                   resetForm();
    //                                   setCurrentStep(0);
    //                                   setOpenModal(false);
    //                                 }}
    //                                 onCancelClick={() => setOpenModal(false)}
    //                                 okText="Clear All"
    //                                 cancelText="Cancel"
    //                               />
    //                             )}
    //                             {/* {openModalFeature && (
    //                               <Popup
    //                                 title="Do You Want To Add This Bike In Our Featured Section?"
    //                                 // description="If you clear all fields, the data will not be saved."
    //                                 onOkClick={() => {
    //                                   resetForm();
    //                                   setCurrentStep(0);
    //                                   addVehicleInFeature(postVehicleId);
    //                                   setOpenModalFeature(false);
    //                                 }}
    //                                 onCancelClick={() =>
    //                                   setOpenModalFeature(false)
    //                                 }
    //                                 okText="Yes"
    //                                 cancelText="No"
    //                               />
    //                             )} */}
    //                             <Modal
    //                               isOpen={modalIsOpen}
    //                               onRequestClose={closeModal}
    //                               className="mymodal"
    //                               overlayClassName="myoverlay"
    //                             >
    //                               <button
    //                                 type="button"
    //                                 onClick={closeModal}
    //                                 className="text-error absolute top-[5px] right-[5px]"
    //                               >
    //                                 <svg
    //                                   xmlns="http://www.w3.org/2000/svg"
    //                                   className="h-5 w-5"
    //                                   viewBox="0 0 20 20"
    //                                   fill="currentColor"
    //                                 >
    //                                   <path
    //                                     fillRule="evenodd"
    //                                     d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
    //                                     clipRule="evenodd"
    //                                   />
    //                                 </svg>
    //                               </button>
    //                               {/* {children} */}
    //                               <FeatureSteps
    //                                 closeModal={closeModal}
    //                                 postVehicleId={postVehicleId}
    //                               ></FeatureSteps>
    //                             </Modal>

    //                             {currentStep !== 0 && (
    //                               <Button
    //                                 type="button"
    //                                 disabled={isLoading}
    //                                 onClick={handleBackClick}
    //                                 variant="link"
    //                               >
    //                                 Back
    //                               </Button>
    //                             )}
    //                           </>
    //                         )}
    //                       </>
    //                     }
    //                   />
    //                 )
    //               )}
    //             </Steps>
    //             <div className="progress-bar pt-6 mb-[-2px] ">
    //               <Line
    //                 percent={
    //                   ((currentStep + 1) / sellVehicleStepsData.length) * 100
    //                 }
    //                 strokeWidth="0.2"
    //                 strokeColor="#8dc63f"
    //               />
    //             </div>
    //           </Form>
    //         );
    //       }}
    //     </Formik>
    //   </div>
    // </section>
  );
};

export default AddSellBike;
