import Confirmation from './FeatureSteps/Confirmation';
import FeaturePrice from './FeatureSteps/FeaturePrice';

export const AddToFeatureData = [
  {
    id: 0,
    key: 'CONFIRMATION',
    title: 'Confirmation',
    component: Confirmation,
  },
  {
    id: 1,
    key: 'DURATION',
    title: 'duration',
    component: FeaturePrice,
  },
];
