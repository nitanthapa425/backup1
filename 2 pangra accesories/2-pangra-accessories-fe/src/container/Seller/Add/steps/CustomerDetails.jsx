import Input from 'components/Input';
import Textarea from 'components/Input/textarea';
import Select from 'components/Select';
// import ReactSelect from 'components/Select/ReactSelect';

import { genders } from 'container/Vehicle/Add/data';
import { useEffect } from 'react';
// import CreatableSelect from 'react-select/creatable';

const CustomerDetail = ({
  setFieldValue,
  values,
  errors,
  touched,
  resetForm,
  validateForm,
  setTouched,
  dirty,
  setFieldTouched,
  setChanged,
  type,
}) => {
  useEffect(() => {
    setChanged(dirty);
  }, [dirty]);

  return (
    <div className="min-h-[415px]">
      <div className="row-sm  pt-5 pb-2">
        <div className="three-col-sm">
          <Input
            label="First Name"
            name="sellerDetails.firstName"
            type="text"
            placeholder="E.g: John"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Middle Name"
            name="sellerDetails.middleName"
            type="text"
            placeholder="E.g: Jung"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Last Name"
            name="sellerDetails.lastName"
            type="text"
            placeholder="E.g: Deo"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Email"
            name="sellerDetails.email"
            type="email"
            placeholder="E.g: john320@gmail.com"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <Select label="Gender" name="sellerDetails.gender" required={false}>
            <option value="">Select Gender</option>

            {genders.map((gender, i) => (
              <option key={i} value={gender}>
                {gender}
              </option>
            ))}
          </Select>
        </div>
        <div className="three-col-sm">
          <Input
            label="Mobile Number"
            name="sellerDetails.mobile"
            type="text"
            placeholder="E.g: 98XXXXXXXX"
            required={false}
          />
        </div>
        <div className="three-col-sm">
          <Input
            label="Additional Number"
            name="sellerDetails.additionalMobile"
            type="text"
            placeholder="E.g: 98XXXXXXXX"
            required={false}
          />
        </div>

        <div className="three-col-sm">
          <Textarea
            label="Description"
            name="sellerDetails.customerDescription"
            placeholder="E.g: Description note"
            required={true}
          />
        </div>
      </div>
    </div>
  );
};

export default CustomerDetail;
