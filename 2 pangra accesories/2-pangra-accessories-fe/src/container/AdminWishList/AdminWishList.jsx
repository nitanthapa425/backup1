import { useState, useEffect, useMemo } from "react";
import { getQueryStringForWishList } from "utils/getQueryStringForTable";
import { SelectColumnFilter } from "components/Table/Filter";
import {
  useDeleteWishListMutation,
  useReadAllWishListQuery,
} from "services/api/adminCartList";
import Table from "components/Table/table";
import { color } from "constant/constant";
function AdminWishList() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [wishListQuery, setWishListQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataWishList,
    isError: isErrorWishList,
    isFetching: isFetchingWishList,
  } = useReadAllWishListQuery(wishListQuery);

  const { data: dataAllWishList, isFetching: isFetchingAllWishList } =
    useReadAllWishListQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (dataAllWishList) {
      setTableDataAll(
        dataAllWishList?.docs?.map((wish) => {
          return {
            id: wish?.id,
            productTitle: wish?.productTitle,
            brandName: wish?.brandName,
            categoryName: wish?.categoryName,
            subCategoryName: wish?.subCategoryName,
            modelName: wish?.modelName,
            status: wish?.status,
            color: wish?.color,
            costPrice: wish?.costPrice,
            discountedPrice: wish?.discountedPrice,
            quantity: wish?.quantity,
            SKU: wish?.SKU,
            createdAt: wish?.createdAt,
            hasInHotDeal: wish?.hasInHotDeal,
            hasInFeatured: wish?.hasInFeatured,
          };
        })
      );
    }
  }, [dataAllWishList]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteWishListMutation();

  const columns = useMemo(
    () => [
      {
        id: "productTitle",
        Header: "Product Title",
        accessor: "productTitle",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "brandName",
        Header: "Brand Name",
        accessor: "brandName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "categoryName",
        Header: "Category Name",
        accessor: "categoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "subCategoryName",
        Header: "Sub Category Name",
        accessor: "subCategoryName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "modelName",
        Header: "Model Name",
        accessor: "modelName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "status",
        Header: "Status",
        accessor: "status",
        Cell: ({ cell: { value } }) => {
          if (value) return <span className="badge-green">Active</span>;
          else return <span className="badge-red">Active</span>;
        },
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Active",
            value: "true",
          },
          {
            label: "Inactive",
            value: "false",
          },
        ],
      },
      {
        id: "color",
        Header: "Available Colors",
        accessor: "color",
        Cell: ({ cell: { value } }) =>
          value.length ? (value.length > 0 ? value.join(" , ") : value) : "-",
        Filter: SelectColumnFilter,

        possibleFilters: color.map((value) => {
          return {
            label: value,
            value: value,
          };
        }),
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "Cost Price",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "discountedPrice",
        Header: "Discount Price",
        accessor: "discountedPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "quantity",
        Header: "Available Quantity",
        accessor: "quantity",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "SKU",
        Header: "SKU",
        accessor: "SKU",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "createdAt",
        Header: "Created At",
        accessor: "createdAt",
        Cell: ({ cell: { value } }) =>
          new Date(value).toLocaleDateString() || "-",
        // Filter: DateColumnFilter,
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "hasInFeatured",
        Header: "Is Featured ",
        accessor: "hasInFeatured",
        Cell: ({ cell: { value } }) => (value ? "Featured " : "Not Featured "),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Featured ",
            value: true,
          },
          {
            label: "Not Feature",
            value: false,
          },
        ],
      },
      {
        id: "hasInHotDeal",
        Header: "Is Hot Deal",
        accessor: "hasInHotDeal",
        Cell: ({ cell: { value } }) => (value ? " Hot Deal" : "Not Hot Deal"),
        canBeSorted: true,
        canBeFiltered: true,
        Filter: SelectColumnFilter,
        possibleFilters: [
          {
            label: "Hot Deal",
            value: true,
          },
          {
            label: "Not Hot Deal",
            value: false,
          },
        ],
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForWishList(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );
    setWishListQuery(query);
  };

  useEffect(() => {
    if (dataWishList) {
      setPageCount(dataWishList.totalPages);
      setTotalData(dataWishList.totalDocs);

      setTableData(
        dataWishList?.docs?.map((wish) => {
          return {
            id: wish?.id,
            productTitle: wish?.productTitle,
            brandName: wish?.brandName,
            categoryName: wish?.categoryName,
            subCategoryName: wish?.subCategoryName,
            modelName: wish?.modelName,
            status: wish?.status,
            color: wish?.color,
            costPrice: wish?.costPrice,
            discountedPrice: wish?.discountedPrice,
            quantity: wish?.quantity,
            SKU: wish?.SKU,
            createdAt: wish?.createdAt,
            hasInHotDeal: wish?.hasInHotDeal,
            hasInFeatured: wish?.hasInFeatured,
          };
        })
      );
    }
  }, [dataWishList]);

  return (
    <>
      <h3 className="mb-3">Wish List</h3>
      <Table
        tableName="WishList/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllWishList}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorWishList}
        isLoadingData={isFetchingWishList}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
      />
    </>
  );
}

export default AdminWishList;
