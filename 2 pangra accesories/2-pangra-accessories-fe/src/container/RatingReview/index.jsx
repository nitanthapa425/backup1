import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";

import { useToasts } from "react-toast-notifications";

import {
  // messageSchema,
  reviewMessageSchema,
} from "validation/resetLink.validation";

import { useRouter } from "next/router";

import {
  useEditRatingReviewProductMutation,
  useRatingReviewProductMutation,
  useReadRatingReviewQuery,
} from "services/api/seller";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import TextareaAutosize from "react-textarea-autosize";
import Modal from "react-modal";
import CustomerLoginPopUp from "components/LoginPopUp";
import Rating from "components/Rating";

const RatingReview = (props) => {
  // console.log("helloOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
  // const [ratingValues, setRatingValue] = useState(0);

  // const sendRatingValue = (value) => {
  //   setRatingValue(value);
  // };

  const router = useRouter();
  const productId = router?.query?.id;
  const formikBag = useRef();
  const { addToast } = useToasts();
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo.token;
  const [editReviewInitialValue, setEditReviewInitialValue] = useState({});

  const [
    createReview,
    { isError, error, isSuccess, isLoading: isLoadingReview },
  ] = useRatingReviewProductMutation();
  const [
    updateReview,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: updateSuccessData,
    },
  ] = useEditRatingReviewProductMutation();
  useEffect(() => {
    if (isUpdateError) {
      addToast("Error while updating rating and .", {
        appearance: "review ",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(
        updateSuccessData?.message || "rating and review updated successfully.",
        {
          appearance: "success",
        }
      );
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
  }, [isUpdateSuccess]);

  const {
    data: getAllReview,
    // isSuccess: isSuccessReview,
    // error: customerProfileFetchError,
    // isLoading: isLoadingCustomerProfile,
  } = useReadRatingReviewQuery(productId, {
    // skip: !productId,
    skip: props.type !== "edit" || !productId,
  });

  const addReviewInitialValue = {
    ratingNumber: 0,
    ratingComment: "",
  };

  useEffect(() => {
    const EditReview = getAllReview?.ratingOfProducts?.find((value, i) => {
      return value._id === props.id;
    });
    if (EditReview) {
      const newReviewDetails = {
        ratingNumber: EditReview?.ratingNumber,
        ratingComment: EditReview?.ratingComment,
      };

      // console.log("newReviewDetails", newReviewDetails);
      setEditReviewInitialValue(newReviewDetails);
    }
  }, [getAllReview]);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast("Thank you for your rating and review.", {
        appearance: "success",
      });
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to receive rating and reviewing . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    if (props.type === "add") {
      createReview({
        values: values,
        id: router.query.id,
      });
    }
    if (props.type === "edit") {
      updateReview({ body: values, id: props.id });
    }
    setSubmitting(false);
  };
  return (
    <div className="bg-white bg-opacity-75 border border-gray-300 rounded-md p-5 mb-4 shadow-lg">
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={closeModal}
          className="text-error absolute top-[5px] right-[5px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>

        <CustomerLoginPopUp setIsOpen={setIsOpen} />
      </Modal>
      <h3 className="h5 mb-2">Rating && Review</h3>
      <Formik
        // initialValues={{
        //   ratingNumber: ratingValues,
        //   ratingComment: "",
        // }}
        initialValues={
          props.type === "edit" ? editReviewInitialValue : addReviewInitialValue
        }
        // onSubmit={(values, { resetForm, setSubmitting }) => {
        //   createReview({
        //     values: values,
        //     id: router.query.id,
        //   });
        //   setSubmitting(false);
        // }}
        onSubmit={onSubmit}
        validationSchema={reviewMessageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="mb-3">
              <h3 className="h5 mb-2">Rating</h3>
              <Rating
                sendRatingValue={(newValue) => {
                  setFieldValue("ratingNumber", `${newValue}`);
                }}
                ratingNumber={values.ratingNumber}
              />
              <h3 className="h5 mb-2">Review</h3>
              <div className="comment-box">
                <TextareaAutosize
                  maxRows={6}
                  required={false}
                  name="ratingComment"
                  type="text"
                  value={values.ratingComment}
                  placeholder="E.g.Thank you for the genuine product same as expected"
                  onChange={(e) => {
                    setFieldValue("ratingComment", e.target.value);
                    if (!token) {
                      setFieldValue("ratingComment", "");
                      setIsOpen(true);
                    }
                  }}
                  onClick={() => {
                    if (!token) {
                      setFieldValue("ratingComment", "");
                      setIsOpen(true);
                    }
                  }}
                />
              </div>

              {token ? (
                <div className="btn-holder btn-comment-holder mt-3">
                  <Button
                    type="submit"
                    // disabled={isSubmitting || !dirty}
                    disabled={!values.ratingComment || !values.ratingNumber}
                    loading={isLoadingReview || updating}
                  >
                    {props.type === "add" ? "Submit" : "Update"}
                  </Button>
                  <Button
                    type="button"
                    variant="outlined"
                    disabled={!values.ratingComment || !values.ratingNumber}
                    onClick={() => {
                      resetForm();
                    }}
                  >
                    Clear
                  </Button>
                </div>
              ) : null}
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
RatingReview.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};
export default RatingReview;
