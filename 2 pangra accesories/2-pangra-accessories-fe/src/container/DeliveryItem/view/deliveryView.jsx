import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";

import {
  useUserDeliveryListQuery,
  useSendDeliveryCodeMutation,
} from "services/api/deliveryLogin";
import Card from "components/Card";
import LoadingCard from "components/LoadingCard";
import Button from "components/Button";
import Modal from "react-modal";
import VerifyDeliveryContainer from "container/VerifyDelivery/VerifyDelivery";
import Link from "next/link";
import { sameArrayIrrespectiveToPosition } from "utils/sameArray";

const DeliveryDetail = () => {
  const router = useRouter();
  const { data: deliveryPersonData, isFetching: isLoadingFeaturedProduct } =
    useUserDeliveryListQuery(router.query.id, {
      skip: !router.query.id,
    });
  const [sendDeliveryToken] = useSendDeliveryCodeMutation();

  const [featItems, setFeatItems] = useState([]);
  const [mobile, setDeliveryPersonMobile] = useState("");
  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  const [bookList, setBookLis] = useState([]);

  useEffect(() => {
    if (deliveryPersonData) {
      setFeatItems(
        deliveryPersonData?.map((item, i) => {
          setDeliveryPersonMobile(
            item?.shippingAddressLocation?.mobile || "9862972729"
          );
          return (
            <Card
              imgPath={item?.productImage[0]?.imageUrl}
              productTitle={item?.productName}
              costPrice={item?.price}
              linkPath={`/product-listing/${item.id}`}
              index={i}
              key={i}
              id={item.id}
              isSold={item.isSold}
            />
          );
        })
      );
    }
  }, [deliveryPersonData]);

  return (
    <>
      <section className="brand-detail-section">
        <div className="container">
          <h1 className="h3 mb-5">Delivery Details</h1>
          <Modal
            isOpen={modalIsOpen}
            className="mymodal order"
            overlayClassName="myoverlay"
          >
            <button
              type="button"
              onClick={closeModal}
              className="text-error absolute top-[5px] right-[5px]"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>

            <VerifyDeliveryContainer
              setIsOpen={setIsOpen}
              deliveredItems={bookList}
            ></VerifyDeliveryContainer>
          </Modal>

          <section className="py-[50px] lg:pt-[55px] lg:pb-[75px]  featured-section">
            <div className="container">
              <div className="flex flex-wrap">
                <div className="featured-carousel">
                  {isLoadingFeaturedProduct ? (
                    <div className="row">
                      <LoadingCard cardColsSize="three" />
                    </div>
                  ) : !featItems.length ? (
                    // <p className="text-sm text-center">Data Unavailable</p>
                    <div className="empty-state w-[250px] md:w-[340px] mx-auto text-center  mt-[-15px]">
                      <div className="max-w-full">
                        <img src="/images/empty-cart.svg" />
                      </div>
                      <div className="text-holder">
                        <h1 className="h4">
                          Your delivery is successful. Thank You.!
                        </h1>
                        <p>Hope you have a great day ahead.</p>
                        <Link href={"/deliveryItem"}>
                          <a className="btn btn-primary">Continue Delivering</a>
                        </Link>
                      </div>
                    </div>
                  ) : (
                    <>
                      <div className="flex flex-wrap items-start">
                        <div className="flex-1 mr-[25px] ">
                          <div className="flex items-center space-x-2">
                            <input
                              type="checkbox"
                              // checked if two array are same
                              checked={sameArrayIrrespectiveToPosition(
                                bookList,
                                deliveryPersonData?.map((data, i) => data.id)
                              )}
                              className="checkbox w-4 h-4 border border-gray-500 inline-block"
                              onClick={(e) => {
                                if (e.target.checked) {
                                  setBookLis((bookList) => {
                                    bookList = [];
                                    return deliveryPersonData?.map(
                                      (data, i) => {
                                        return data.id;
                                      }
                                    );
                                  });
                                } else {
                                  setBookLis([]);
                                }
                              }}
                            ></input>
                            <span className="inline-block ml-2">
                              Select all items{" "}
                              {bookList.length
                                ? `(${bookList.length} item/s)`
                                : ""}
                            </span>
                          </div>
                          {deliveryPersonData?.map((data, index) => {
                            return (
                              <div
                                key={index}
                                className="relative  group flex flex-wrap items-center space-x-3 mb-4 border bg-white rounded-lg shadow-lg border-gray-300 py-6 pl-5"
                              >
                                <input
                                  type="checkbox"
                                  checked={bookList.includes(data.id)}
                                  className="checkbox w-4 h-4 border border-gray-400 inline-block mr-3"
                                  onClick={(e) => {
                                    if (e.target.checked) {
                                      const allId = [...bookList, data.id];
                                      const uniqueList = [...new Set(allId)];
                                      setBookLis(uniqueList);
                                    } else {
                                      setBookLis(
                                        bookList.filter((v, j) => {
                                          return v !== data.id;
                                        })
                                      );
                                    }
                                  }}
                                ></input>
                                <div
                                  className="image-holder max-w-[300px] !mx-auto mb-4 w-full lg:w-[180px] lg:mb-0"
                                  // onClick={() => {
                                  //   router.push(
                                  //     `/product-listing/${data.productId}`
                                  //   );
                                  // }}
                                >
                                  <img
                                    src={data?.productImage?.imageUrl}
                                    alt="image description"
                                    className="w-full h-[110px] object-contain"
                                  />
                                </div>
                                <div className="detail-holder w-full lg:w-auto lg:flex-1 relative md:pr-[130px]">
                                  <h1 className="h5 mb-1">
                                    {data?.productName}
                                  </h1>
                                  <div className=" flex justify-between lg:block lg:w-[100px] lg:absolute lg:text-right lg:right-[15px] lg:top-1/2 lg:translate-Y-[-50%]">
                                    <span className="price font-bold text-primary-light">
                                      NRs. {data?.totalPrice}
                                    </span>
                                  </div>
                                  <div className="flex flex-wrap space-x-3">
                                    <span className="flex text-sm items-center font-semibold">
                                      Size : {data?.size}
                                    </span>
                                    <span className="flex text-sm items-center font-semibold">
                                      Color : {data?.color}
                                    </span>
                                    <span className="flex text-sm items-center font-semibold">
                                      Quantity : {data?.quantity}
                                    </span>
                                    <span className="flex text-sm items-center font-semibold">
                                      Delivery Start Date :{" "}
                                      {new Date(
                                        data?.deliveryStart
                                      )?.toLocaleDateString?.()}
                                    </span>
                                    <span className="flex text-sm items-center font-semibold">
                                      Delivery End Date :{" "}
                                      {new Date(
                                        data?.deliveryEnd
                                      )?.toLocaleDateString?.()}
                                    </span>
                                    <div className="flex text-sm items-center font-semibold">
                                      Delivery Status :
                                      <span className=" review-pending">
                                        {data?.deliveryStatus}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                      <Button
                        disabled={!bookList.length}
                        onClick={() => {
                          sendDeliveryToken({ mobile });
                          setIsOpen(true);
                        }}
                      >
                        Send Verification Code
                      </Button>
                    </>
                  )}
                </div>
              </div>
            </div>
          </section>
        </div>
      </section>
    </>
  );
};

export default DeliveryDetail;
