import Table from "components/Table/table";
import DeliveryLayout from "layouts/Delivery";
import { useState, useEffect, useMemo } from "react";

import { getQueryStringForDeliveryTable } from "utils/getQueryStringForTable";

import { useReadAllDeliveryItemQuery } from "services/api/deliveryLogin";

function ViewDeliveryItemContainer() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [UserDataQuery, setUserDataQuery] = useState("");
  // eslint-disable-next-line no-unused-vars
  const [tableDataAll, setTableDataAll] = useState([]);

  const {
    data: userDataAll,
    isError: isErrorAll,
    isFetching: isLoadingAll,
  } = useReadAllDeliveryItemQuery(UserDataQuery);
  // eslint-disable-next-line no-unused-vars

  const columns = useMemo(
    () => [
      {
        id: "fullName",
        Header: "Full Name",
        accessor: "fullName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "mobile",
        Header: "Mobile",
        accessor: "mobile",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "email",
        Header: "Email",
        accessor: "email",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "location.combineLocation",
        Header: "Location",
        accessor: "location",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "location.exactLocation",
        Header: "Exact Location",
        accessor: "exactLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "location.nearByLocation",
        Header: "Nearby Location",
        accessor: "nearByLocation",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForDeliveryTable(
      pageIndex,
      pageSize,
      sortBy,
      filters
    );

    setUserDataQuery(query);
  };

  useEffect(() => {
    if (userDataAll) {
      setPageCount(userDataAll.totalPages);
      setTotalData(userDataAll.totalDocs);

      setTableData(
        userDataAll?.docs?.map((res) => {
          return {
            id: res?.id,
            fullName: res?.fullName,
            mobile: res?.mobile,
            email: res?.email,
            location: res?.location?.combineLocation,
            exactLocation: res?.location?.exactLocation,
            nearByLocation: res?.location?.nearByLocation,
          };
        })
      );
    }
  }, [userDataAll]);
  const BreadCrumbList = [
    {
      routeName: "Delivery Item List",
      route: "",
    },
  ];

  return (
    <DeliveryLayout
      documentTitle="View delivery Item"
      BreadCrumbList={BreadCrumbList}
    >
      <section className="mt-3">
        <div className="container">
          <Table
            tableDataAll={tableDataAll}
            // setSkipTableDataAll={setSkipTableDataAll}
            isLoadingAll={isLoadingAll}
            columns={columns}
            data={tableData}
            fetchData={getData}
            isFetchError={isErrorAll}
            isLoadingData={isLoadingAll}
            pageCount={pageCount}
            defaultPageSize={10}
            totalData={totalData}
            rowOptions={[...new Set([10, 20, 30, totalData])]}
            // editRoute="delivery/edit"
            // viewRoute="deliveryItem/view"
            viewRoute="book/orderedItemsDetails"
            // deleteQuery={deleteItems}
            // isDeleting={isDeleting}
            // isDeleteError={isDeleteError}
            // isDeleteSuccess={isDeleteSuccess}
            // DeletedError={DeletedError}
            hasExport={true}
            // addPage={{ page: "Add User", route: "/admin/user/create" }}
            tableName="DeliveryItem/s"
          />
        </div>
      </section>
    </DeliveryLayout>
  );
}

export default ViewDeliveryItemContainer;
