import Button from "components/Button";
import Input from "components/Input";
import { sparrowToken } from "config";
import { Form, Formik } from "formik";
import React, { useEffect, useRef } from "react";
import { useToasts } from "react-toast-notifications";
import { useCustomerForgotMutation } from "services/api/customer";
import { customerForgotValidationSchema } from "validation/customer.validation";

const Mobile = ({ setCurrentStep, currentStep, handleBackClick }) => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [
    createCustomerForgotPassword,
    {
      isError,
      error,
      isLoading,
      isSuccess,
      data: dataCreateCustomerForgotPassword,
    },
  ] = useCustomerForgotMutation();

  useEffect(() => {
    if (isSuccess) {
      addToast(
        dataCreateCustomerForgotPassword?.message ||
          "The OTP Code has been sent to your Mobile Number.",
        {
          appearance: "success",
        }
      );
      setCurrentStep((currentStep) => {
        return currentStep + 1;
      });
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "you might have entered wrong Phone Number . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <Formik
      initialValues={{
        mobile: localStorage.getItem("mobile") || "",
      }}
      onSubmit={(values, { resetForm, setSubmitting }) => {
        const formData = new FormData();
        const randomNumber = Math.floor(10000 + Math.random() * 90000);
        localStorage.setItem("mobile", values.mobile);
        formData.append("token", sparrowToken);
        formData.append("from", "TUKIALERT");
        formData.append("to", values.mobile);
        formData.append(
          "text",
          ` ${randomNumber} is your account verification code. \n Thank you !`
        );
        createCustomerForgotPassword(formData);

        setSubmitting(false);
      }}
      validationSchema={customerForgotValidationSchema}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        isSubmitting,
        dirty,
      }) => (
        <Form>
          <div className="form-holder border-gray-300 shadow-sm border p-6 md:p-8 rounded-md max-w-3xl mx-auto">
            <h3 className="mb-3">Forgot Password</h3>
            <p className="text-lg">
              Enter your Mobile Number and we will send OTP Code to reset your
              password
            </p>
            <Input
              label="Mobile Number"
              name="mobile"
              type="text"
              placeholder="Mobile Number"
            />

            <div className="btn-holder mt-3">
              <Button type="submit" disabled={isSubmitting} loading={isLoading}>
                Send
              </Button>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
};

export default Mobile;
