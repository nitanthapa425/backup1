import { useState, useEffect, useMemo } from "react";
import { getQueryStringForCart } from "utils/getQueryStringForTable";

import { MultiSelectFilter } from "components/Table/Filter";
import {
  useDeleteCartAdminMutation,
  useReadAllCartQuery,
} from "services/api/adminCartList";
import Table from "components/Table/table";
import { color } from "constant/constant";
function AdminCart() {
  const [tableData, setTableData] = useState([]);
  const [pageCount, setPageCount] = useState(0);
  const [totalData, setTotalData] = useState(0);
  const [cartQuery, setCartQuery] = useState("");
  const [tableDataAll, setTableDataAll] = useState([]);
  const [skipTableDataAll, setSkipTableDataAll] = useState(true);

  const {
    data: dataCart,
    isError: isErrorCart,
    isFetching: isFetchingCart,
  } = useReadAllCartQuery(cartQuery);

  const { data: dataAllCart, isFetching: isFetchingAllCart } =
    useReadAllCartQuery("?&sortBy=createdAt&sortOrder=-1", {
      skip: skipTableDataAll,
    });

  useEffect(() => {
    if (dataAllCart) {
      setTableDataAll(
        dataAllCart.docs.map((value) => {
          return {
            id: value._id || "-",
            productName: value.productName || "-",
            color: value.color || "-",
            costPrice: value.costPrice || "-",
            discountedPrice: value.discountedPrice || "-",
            discountPercentage: value.discountPercentage || "-",
          };
        })
      );
    }
  }, [dataAllCart]);

  const [
    deleteItems,
    {
      isLoading: isDeleting,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
    },
  ] = useDeleteCartAdminMutation();

  const columns = useMemo(
    () => [
      {
        id: "productName",
        Header: "Product Title",
        accessor: "productName",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
      {
        id: "costPrice",
        Header: "Cost Price",
        accessor: "costPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "discountedPrice",
        Header: "Discount Price",
        accessor: "discountedPrice",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "color",
        Header: "Color",
        accessor: "color",
        Cell: ({ cell: { value } }) => value || "-",
        Filter: MultiSelectFilter,
        possibleFilters: color.map((value) => ({
          value: value,
          label: value,
        })),
        canBeSorted: true,
        canBeFiltered: true,
      },

      {
        id: "numViews",
        Header: "Views",
        accessor: "numViews",
        Cell: ({ cell: { value } }) => value || "-",
        canBeSorted: true,
        canBeFiltered: true,
      },
    ],
    []
  );

  const getData = ({ pageIndex, pageSize, sortBy, filters }) => {
    const query = getQueryStringForCart(pageIndex, pageSize, sortBy, filters);
    setCartQuery(query);
  };

  useEffect(() => {
    if (dataCart) {
      setPageCount(dataCart.totalPages);
      setTotalData(dataCart.totalDocs);
      setTableData(
        dataCart.docs.map((value) => {
          return {
            id: value._id || "-",
            productName: value.productName || "-",
            color: value.color || "-",
            costPrice: value.costPrice || "-",
            discountedPrice: value.discountedPrice || "-",
            discountPercentage: value.discountPercentage || "-",
          };
        })
      );
    }
  }, [dataCart]);

  return (
    <>
      <h3 className="mb-3">Cart List</h3>
      <Table
        tableName="Cart/s"
        tableDataAll={tableDataAll}
        setSkipTableDataAll={setSkipTableDataAll}
        isLoadingAll={isFetchingAllCart}
        columns={columns}
        data={tableData}
        fetchData={getData}
        isFetchError={isErrorCart}
        isLoadingData={isFetchingCart}
        pageCount={pageCount}
        defaultPageSize={10}
        totalData={totalData}
        rowOptions={[...new Set([10, 20, 30, totalData])]}
        deleteQuery={deleteItems}
        isDeleting={isDeleting}
        isDeleteError={isDeleteError}
        isDeleteSuccess={isDeleteSuccess}
        DeletedError={DeletedError}
        hasExport={true}
      />
    </>
  );
}

export default AdminCart;
