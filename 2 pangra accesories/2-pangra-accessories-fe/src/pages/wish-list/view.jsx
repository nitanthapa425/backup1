import AdminWishList from "container/AdminWishList/AdminWishList";
import React from "react";
import AdminLayout from "layouts/Admin";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Wish List",
      route: "",
    },
  ];

  return (
    <AdminLayout BreadCrumbList={BreadCrumbList} documentTitle="Wish list">
      <AdminWishList />
    </AdminLayout>
  );
};

export default index;
