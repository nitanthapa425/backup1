import AddToWishList from "container/Customer/ProductListingCom/WishList";
import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Wish List",
      route: "",
    },
  ];
  return (
    <UserLayout documentTitle={"Wish List"} BreadCrumbList={BreadCrumbList}>
      <AddToWishList />
      <Footer />
    </UserLayout>
  );
};

export default index;
