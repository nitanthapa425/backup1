import Footer from "components/Footer";
import ContactUs from "container/contact-us";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Contact us",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Contact Us"
      BreadCrumbList={BreadCrumbList}
    >
      <ContactUs />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
