// import Terms from "container/Terms";

// const index = () => <Terms />;

// export default index;

import Footer from "components/Footer";

import Terms from "container/Terms";
import WebsiteUserLayout from "layouts/WebsiteUser";

const termsPage = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Terms and Conditions",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Terms and Conditions"
      BreadCrumbList={BreadCrumbList}
    >
      <Terms />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default termsPage;
