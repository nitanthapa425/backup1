import SellDetailComponent from "container/Customer/ProductListingCom/sellDetailComponent";
import React from "react";
import WebsiteUserLayout from "layouts/WebsiteUser/";
import Footer from "components/Footer";

const SellDetail = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Product List",
      route: "/product-listing",
    },
    {
      routeName: "Product Details",
      route: "",
    },
  ];
  return (
    <div>
      <WebsiteUserLayout
        documentTitle="Product Details"
        BreadCrumbList={BreadCrumbList}
      >
        <SellDetailComponent />
        <Footer />
      </WebsiteUserLayout>
    </div>
  );
};

export default SellDetail;
