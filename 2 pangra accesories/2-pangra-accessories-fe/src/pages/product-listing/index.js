import ProductListingCom from "container/Customer/ProductListingCom/ProductListingCom";
import React from "react";
import WebsiteUserLayout from "layouts/WebsiteUser/";
import Footer from "components/Footer";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Products List",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Product Listing"
      BreadCrumbList={BreadCrumbList}
    >
      <ProductListingCom />
      <Footer />
    </WebsiteUserLayout>
  );
};

export default index;
