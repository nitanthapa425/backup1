import Footer from 'components/Footer';
import HotDeals from 'container/HotDeals';
import WebsiteUserLayout from 'layouts/WebsiteUser';

const HotDealsPage = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Hot Deal List',
      route: '',
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Hot Deal List"
      BreadCrumbList={BreadCrumbList}
    >
      <HotDeals />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default HotDealsPage;
