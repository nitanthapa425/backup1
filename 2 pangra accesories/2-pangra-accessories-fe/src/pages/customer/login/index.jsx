import CustomerLogin from 'container/Customer/Login/Login';
import CustomerLog from 'layouts/CustomerLogin';

const CustomerLogins = () => (
  <CustomerLog>
    <CustomerLogin />
  </CustomerLog>
);

export default CustomerLogins;
