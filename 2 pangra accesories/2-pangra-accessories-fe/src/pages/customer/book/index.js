import Footer from "components/Footer";
import AddToBookList from "container/Customer/ProductListingCom/BookList";
import React from "react";
import UserLayout from "layouts/User";

const BreadCrumbList = [
  {
    routeName: "Home",
    route: "/",
  },
  {
    routeName: "Ordering List",
    route: "",
  },
];

const index = () => {
  return (
    <UserLayout documentTitle={"Ordered List"} BreadCrumbList={BreadCrumbList}>
      <AddToBookList />;
      <Footer />
    </UserLayout>
  );
};

export default index;
