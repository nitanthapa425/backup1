import CustomerSignUp from "container/Customer/Add";
import WebsiteUserLayout from "layouts/WebsiteUser";

const CustomerSignUpPage = () => {
  return (
    <WebsiteUserLayout documentTitle="Sign-Up">
      <CustomerSignUp />;
    </WebsiteUserLayout>
  );
};

export default CustomerSignUpPage;
