import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
import ReadOrderedStatus from "container/Customer/ProductListingCom/orderedStatusTracking";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Ordered Status",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Ordered Status">
      <ReadOrderedStatus />
      <Footer />
    </UserLayout>
  );
};

export default index;
