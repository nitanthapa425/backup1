import Footer from "components/Footer";
import Return from "container/Return-policy";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Return & Refunds",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Return & Refunds"
      BreadCrumbList={BreadCrumbList}
    >
      <Return />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
