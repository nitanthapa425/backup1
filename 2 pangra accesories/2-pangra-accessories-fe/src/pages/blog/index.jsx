import Footer from "components/Footer";
import Blogs from "container/Blog";
import WebsiteUserLayout from "layouts/WebsiteUser";

const BlogPage = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Blog",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout documentTitle="Blog" BreadCrumbList={BreadCrumbList}>
      <Blogs />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default BlogPage;
