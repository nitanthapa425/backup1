import React from "react";
import AdminLayout from "layouts/Admin";
import ViewLog from "container/AdminLog/viewLog";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Log List",
      route: "",
    },
  ];
  return (
    <AdminLayout
      BreadCrumbList={BreadCrumbList}
      showFor={["SUPER_ADMIN"]}
      documentTitle="Log"
    >
      <ViewLog />
    </AdminLayout>
  );
};

export default index;
