// import PrivateRoutes from 'components/PrivateRoutes/PrivateRoutes';
import CreateEditCompanyContainer from "container/Admin/Company/CreateEdit/CreateEditCompanyContainer";

const CreateCompanyPage = () => {
  return <CreateEditCompanyContainer type="add"></CreateEditCompanyContainer>;
};

export default CreateCompanyPage;
