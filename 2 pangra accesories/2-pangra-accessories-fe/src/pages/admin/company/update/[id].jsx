import CreateEditCompanyContainer from 'container/Admin/Company/CreateEdit/CreateEditCompanyContainer';

const UpdateCompanyPage = () => {
    return <CreateEditCompanyContainer type="edit"></CreateEditCompanyContainer>;
}; 

export default UpdateCompanyPage;
