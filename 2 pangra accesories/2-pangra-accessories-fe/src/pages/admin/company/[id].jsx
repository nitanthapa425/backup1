import DetailCompanyContainer from 'container/Admin/Company/Detail/DetailCompanyContainer';

const CompanyDetailsPage = () => {
  return <DetailCompanyContainer />;
};

export default CompanyDetailsPage;
