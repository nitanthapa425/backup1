import GetSingleProfile from "container/User/GetSingleUser";
import React from "react";

const GetdeletedUser = () => {
  return (
    <div>
      <GetSingleProfile
        canEdit={false}
        list="Deleted User List"
        profile="Deleted User Profile"
        listRoute="/admin/user/inactive"
      />
    </div>
  );
};

export default GetdeletedUser;
