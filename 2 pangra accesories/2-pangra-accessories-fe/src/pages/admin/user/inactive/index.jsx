import React from "react";
import ViewDeletedUser from "container/User/DeletedUser/ViewDeletedUser";

const ViewDeletedUsers = () => {
  return (
    <div>
      <ViewDeletedUser />
    </div>
  );
};

export default ViewDeletedUsers;
