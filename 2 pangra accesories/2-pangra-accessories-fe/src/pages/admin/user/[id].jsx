import GetSingleProfile from "container/User/GetSingleUser";
import React from "react";

const idUserPage = () => {
  return (
    <div>
      <GetSingleProfile listRoute="/admin/user" />
    </div>
  );
};

export default idUserPage;
