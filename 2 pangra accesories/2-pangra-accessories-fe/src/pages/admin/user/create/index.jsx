import SignUpComponent from "components/SignUp";
import React from "react";

const createUserPage = () => {
  return (
    <div>
      <SignUpComponent type="add" />
    </div>
  );
};

export default createUserPage;
