import SignUpComponent from "components/SignUp";
import React from "react";

const updateUserPage = () => {
  return (
    <div>
      <SignUpComponent type="edit" editUser="userProfile" />;
    </div>
  );
};

export default updateUserPage;
