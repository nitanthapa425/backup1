import GetMyProfileComponent from "container/User/getMyProfile";
import React from "react";

const getMyProfile = () => {
  return <GetMyProfileComponent></GetMyProfileComponent>;
};

export default getMyProfile;
