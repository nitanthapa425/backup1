import ViewUserContainer from "container/User/UserViewContainer";
import React from "react";

const userPage = () => {
  return (
    <div>
      <ViewUserContainer />
    </div>
  );
};

export default userPage;
