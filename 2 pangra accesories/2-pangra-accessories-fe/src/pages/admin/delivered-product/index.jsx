import React from "react";
import DeliveredProductList from "container/AdminBook/DeliveredProductList";
import AdminLayout from "layouts/Admin";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Delivered Product List",
      route: "",
    },
  ];

  return (
    <AdminLayout
      BreadCrumbList={BreadCrumbList}
      documentTitle="Delivered Product List"
    >
      <DeliveredProductList />
    </AdminLayout>
  );
};

export default index;
