// import PrivateRoutes from 'components/PrivateRoutes/PrivateRoutes';
import CreateEditBrandContainer from "container/Admin/Brand/CreateEdit/CreateEditBrandContainer";

const CreateBrandPage = () => {
  return <CreateEditBrandContainer type="add"></CreateEditBrandContainer>;
};

export default CreateBrandPage;
