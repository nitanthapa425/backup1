import DetailBrandContainer from 'container/Admin/Brand/Detail/DetailBrandContainer';

const BrandDetailsPage = () => {
  return <DetailBrandContainer />;
};

export default BrandDetailsPage;
