import CreateEditBrandContainer from 'container/Admin/Brand/CreateEdit/CreateEditBrandContainer';

const UpdateBrandPage = () => {
    return <CreateEditBrandContainer type="edit"></CreateEditBrandContainer>;
};

export default UpdateBrandPage;
