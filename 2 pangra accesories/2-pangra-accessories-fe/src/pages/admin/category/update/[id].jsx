import CreateEditCategoryContainer from 'container/Admin/Category/CreateEdit/CreateEditCategoryContainer';

const UpdateCategoryPage = () => {
  return <CreateEditCategoryContainer type="edit"></CreateEditCategoryContainer>;
};

export default UpdateCategoryPage;
