import CategoryContainer from "container/Admin/Category/CategoryContainer";

function ViewCategoryPage() {
  return <CategoryContainer />;
}

export default ViewCategoryPage;