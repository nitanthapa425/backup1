import DetailCategoryContainer from 'container/Admin/Category/Detail/DetailCategoryContainer';

const CategoryDetailsPage = () => {
    return <DetailCategoryContainer />;
};

export default CategoryDetailsPage;