import CreateEditCategoryContainer from 'container/Admin/Category/CreateEdit/CreateEditCategoryContainer';

const CreateCategoryPage = () => {
  return <CreateEditCategoryContainer type="add"></CreateEditCategoryContainer>;
};

export default CreateCategoryPage;
