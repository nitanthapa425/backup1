import DetailProductContainer from 'container/Admin/Product/Detail/DetailProductContainer';

const ProductDetailsPage = () => {
  return <DetailProductContainer />;
};

export default ProductDetailsPage;
