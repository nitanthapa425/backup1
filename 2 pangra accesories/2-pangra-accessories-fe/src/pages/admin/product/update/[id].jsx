import CreateEditProductContainer from "container/Admin/Product/CreateEdit/CreateEditProductContainer";

const UpdateProductPage = () => {
  return <CreateEditProductContainer type="edit"></CreateEditProductContainer>;
};

export default UpdateProductPage;
