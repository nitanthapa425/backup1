import CreateEditProductContainer from "container/Admin/Product/CreateEdit/CreateEditProductContainer";

const CreateProductPage = () => {
  return <CreateEditProductContainer type="add" />;
};

export default CreateProductPage;
