import CreateEditProductContainer from 'container/Admin/Product/CreateEdit/CreateEditProductContainer';

const UpdateProductDraftsPage = () => {
  return <CreateEditProductContainer type="draft"></CreateEditProductContainer>;
};

export default UpdateProductDraftsPage;
