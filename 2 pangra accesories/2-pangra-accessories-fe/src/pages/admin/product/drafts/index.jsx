import ProductDraftsContainer from "container/Admin/Product/Drafts/ProductDraftsContainer";

function ViewProductDraftsPage() {
  return <ProductDraftsContainer />;
}

export default ViewProductDraftsPage;