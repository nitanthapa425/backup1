import ProductContainer from "container/Admin/Product/ProductContainer";

function ViewProductPage() {
  return <ProductContainer />;
}

export default ViewProductPage;