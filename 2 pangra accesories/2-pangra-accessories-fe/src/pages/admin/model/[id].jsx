import DetailModelContainer from "container/Admin/Model/Details/ModelDetailContainer";

const ModelDetailsPage = () => {
  return <DetailModelContainer />;
};

export default ModelDetailsPage;
