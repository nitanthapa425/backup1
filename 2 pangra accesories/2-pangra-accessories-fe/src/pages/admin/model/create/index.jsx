import CreateEditModelContainer from "container/Admin/Model/CreateModel/CreateEditModelContainer";

const CreateModelPage = () => {
  return <CreateEditModelContainer type="add"></CreateEditModelContainer>;
};

export default CreateModelPage;
