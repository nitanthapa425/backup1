import CreateEditModelContainer from "container/Admin/Model/CreateModel/CreateEditModelContainer";

const CreateModelPage = () => {
  return <CreateEditModelContainer type="edit"></CreateEditModelContainer>;
};

export default CreateModelPage;
