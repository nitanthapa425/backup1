import GetSingleProfile from "container/Delivery/getSingleDelivery";
import React from "react";

const idUserPage = () => {
  return (
    <div>
      <GetSingleProfile listRoute="/admin/delivery" />
    </div>
  );
};

export default idUserPage;
