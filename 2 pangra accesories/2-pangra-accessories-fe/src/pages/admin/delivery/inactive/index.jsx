import React from "react";
import ViewDeletedDeliveryContainer from "container/Delivery/DeletedDelivery/ViewDeletedDelivery";

const ViewDeletedDeliveryContainers = () => {
  return (
    <div>
      <ViewDeletedDeliveryContainer />
    </div>
  );
};

export default ViewDeletedDeliveryContainers;
