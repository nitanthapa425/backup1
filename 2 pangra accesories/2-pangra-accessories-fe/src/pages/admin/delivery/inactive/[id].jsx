import GetSingleProfile from "container/Delivery/getSingleDelivery";
import React from "react";

const GetDeletedDelivery = () => {
  return (
    <div>
      <GetSingleProfile
        canEdit={false}
        list="Deleted Delivery List"
        profile="Deleted Delivery Profile"
        listRoute="/admin/delivery/inactive"
      />
    </div>
  );
};

export default GetDeletedDelivery;
