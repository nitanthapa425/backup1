import CreateEditShippingContainer from "container/Admin/ShippingCharge/CreateEditShiping/CreateEditShiping";

const CreateShippingPage = () => {
  return <CreateEditShippingContainer type="add"></CreateEditShippingContainer>;
};

export default CreateShippingPage;
