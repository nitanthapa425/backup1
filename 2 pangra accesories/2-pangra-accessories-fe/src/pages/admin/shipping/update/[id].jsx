import CreateEditShippingContainer from "container/Admin/ShippingCharge/CreateEditShiping/CreateEditShiping";

const CreateShippingPage = () => {
  return (
    <CreateEditShippingContainer type="edit"></CreateEditShippingContainer>
  );
};

export default CreateShippingPage;
