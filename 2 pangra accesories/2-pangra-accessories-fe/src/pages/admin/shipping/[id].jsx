import DetailShippingContainer from "container/Admin/ShippingCharge/Details/ShipingDetails";

const ShippingDetailsPage = () => {
  return <DetailShippingContainer />;
};

export default ShippingDetailsPage;
