import SubCategoryContainer from "container/Admin/SubCategory/SubCategoryContainer";

function ViewSubCategoryPage() {
    return <SubCategoryContainer />;
}

export default ViewSubCategoryPage;