import CreateEditSubCategoryContainer from 'container/Admin/SubCategory/CreateEdit/CreateEditSubCategoryContainer';

const CreateSubCategoryPage = () => {
    return <CreateEditSubCategoryContainer type="add"></CreateEditSubCategoryContainer>;
};

export default CreateSubCategoryPage;
