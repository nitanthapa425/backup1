import DetailSubCategoryContainer from 'container/Admin/SubCategory/Detail/DetailSubCategoryContainer';

const SubCategoryDetailsPage = () => {
    return <DetailSubCategoryContainer />;
};

export default SubCategoryDetailsPage;