import CreateEditSubCategoryContainer from 'container/Admin/SubCategory/CreateEdit/CreateEditSubCategoryContainer';

const UpdateSubCategoryPage = () => {
    return <CreateEditSubCategoryContainer type="edit"></CreateEditSubCategoryContainer>;
};

export default UpdateSubCategoryPage;
