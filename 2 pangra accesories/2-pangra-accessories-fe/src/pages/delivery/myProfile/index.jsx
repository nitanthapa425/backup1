import GetDeliveryMyProfile from "container/Delivery/GetMyProfile/getProfile";
import DeliveryLayout from "layouts/Delivery";

const DeliveryMyProfileDetails = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "My Profile ",
      route: "",
    },
  ];
  return (
    <DeliveryLayout
      documentTitle="Edit Profile "
      BreadCrumbList={BreadCrumbList}
    >
      <GetDeliveryMyProfile />;
    </DeliveryLayout>
  );
};

export default DeliveryMyProfileDetails;
