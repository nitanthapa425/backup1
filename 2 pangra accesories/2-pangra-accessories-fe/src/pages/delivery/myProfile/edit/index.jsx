import Footer from "components/Footer";
import DeliveryProfileEdit from "container/Delivery/GetMyProfile/edit";
import DeliveryLayout from "layouts/Delivery";

const DeliveryProfileUpdate = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Edit Profile ",
      route: "",
    },
  ];
  return (
    <DeliveryLayout
      documentTitle="Edit Profile "
      BreadCrumbList={BreadCrumbList}
    >
      <DeliveryProfileEdit />;
      <Footer />
    </DeliveryLayout>
  );
};

export default DeliveryProfileUpdate;
