import DeliveryForgotPasswordContainer from "container/Delivery/Forgot";

const ForgotPassword = () => {
  return (
    <div>
      <DeliveryForgotPasswordContainer />
    </div>
  );
};

export default ForgotPassword;
