import GetSingleDeliveryProfile from 'container/Delivery/get';

const DeliveryDetails = () => <GetSingleDeliveryProfile />;

export default DeliveryDetails;
