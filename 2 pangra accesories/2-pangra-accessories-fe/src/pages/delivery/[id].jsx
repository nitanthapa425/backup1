import GetSingleDelivery from "container/Delivery/getSingleDelivery";
import React from "react";

const idUserPage = () => {
  return (
    <div>
      <GetSingleDelivery listRoute="/admin/delivery" />
    </div>
  );
};

export default idUserPage;
