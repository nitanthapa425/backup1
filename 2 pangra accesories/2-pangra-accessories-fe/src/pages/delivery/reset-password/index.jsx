import DeliveryResetPasswordContainer from "container/Delivery/Reset";

const ForgotPassword = () => {
  return (
    <div>
      <DeliveryResetPasswordContainer />
    </div>
  );
};

export default ForgotPassword;
