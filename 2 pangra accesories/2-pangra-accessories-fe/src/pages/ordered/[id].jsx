import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
import ReadOrderItems from "container/Customer/ProductListingCom/readOrderItems";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Ordered List",
      route: "/ordered",
    },
    {
      routeName: "Ordered Items",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Ordered Items">
      <ReadOrderItems></ReadOrderItems>
      <Footer />
    </UserLayout>
  );
};

export default index;
