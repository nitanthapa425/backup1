import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
import DeliveredCancelledList from "container/Customer/ProductListingCom/DeliveredCancelledList";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Cancelled List",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Cancelled List">
      <DeliveredCancelledList type="cancelled"></DeliveredCancelledList>
      <Footer />
    </UserLayout>
  );
};

export default index;
