import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
import ReadBookedList from "container/Customer/ProductListingCom/readBooklist";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Ordered List",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Ordered List">
      <ReadBookedList />
      <Footer />
    </UserLayout>
  );
};

export default index;
