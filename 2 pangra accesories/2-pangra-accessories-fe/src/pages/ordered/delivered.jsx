import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
import DeliveredCancelledList from "container/Customer/ProductListingCom/DeliveredCancelledList";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Delivered List",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Delivered List">
      <DeliveredCancelledList type="delivered"></DeliveredCancelledList>
      <Footer />
    </UserLayout>
  );
};

export default index;
