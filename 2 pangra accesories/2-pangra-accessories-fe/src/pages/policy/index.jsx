import Footer from "components/Footer";
import PolicyTerms from "container/PolicyCondition";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Policy = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Privacy Policy",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Privacy Policy"
      BreadCrumbList={BreadCrumbList}
    >
      <PolicyTerms />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Policy;
