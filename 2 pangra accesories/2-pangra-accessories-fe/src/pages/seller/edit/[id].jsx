import CreateSellerForm from "container/Seller/CreateSeller";
import React from "react";
import AdminLayout from "layouts/Admin";

const EditSeller = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Selling List ",
      route: "/seller/view",
    },
    {
      routeName: "Edit",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Edit sell vehicle"
      BreadCrumbList={BreadCrumbList}
    >
      <CreateSellerForm type="edit" />
    </AdminLayout>
  );
};
export default EditSeller;
