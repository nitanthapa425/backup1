import ViewSellerContainer from "container/Seller/viewSellerContainer";
import AdminLayout from "layouts/Admin";

const SellerTable = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Selling List ",
      route: "",
    },
  ];
  return (
    <AdminLayout
      documentTitle="Selling vehicle List"
      BreadCrumbList={BreadCrumbList}
    >
      <ViewSellerContainer />;
    </AdminLayout>
  );
};

export default SellerTable;
