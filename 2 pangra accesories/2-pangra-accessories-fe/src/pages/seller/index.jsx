// import CreateSellerForm from 'container/Seller/CreateSeller';
import React from "react";
import AdminLayout from "layouts/Admin";
import AddAdminSellVehicle from "container/AdminSell";

const Seller = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },

    {
      routeName: "Add Selling Vehicle",
      route: "",
    },
  ];
  return (
    <div>
      <AdminLayout documentTitle="Sell Vehicle" BreadCrumbList={BreadCrumbList}>
        <AddAdminSellVehicle type="add" />
      </AdminLayout>
    </div>
  );
};

export default Seller;
