import Footer from "components/Footer";
import Faqs from "container/Faqs";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "FAQs",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout documentTitle="FAQs" BreadCrumbList={BreadCrumbList}>
      <Faqs />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
