import CreateBrandContainer from 'container/Brand/CreateBrandContainer';

import React from 'react';

const addBrand = () => {
  return (
    <>
      <CreateBrandContainer type="add"></CreateBrandContainer>
    </>
  );
};

export default addBrand;
