import CreateBrandContainer from 'container/Brand/CreateBrandContainer';
import React from 'react';

const EditBrandDetails = () => {
  return (
    <div>
      <CreateBrandContainer type="edit"></CreateBrandContainer>
    </div>
  );
};

export default EditBrandDetails;
