import AdminLayout from "layouts/Admin";
import FlashSaleContainer from "container/FlashSale/viewFlashSaleContainer";

const FlashSales = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Flash Sale List ",
      route: "",
    },
  ];
  return (
    <div>
      <AdminLayout documentTitle="Flash Sales" BreadCrumbList={BreadCrumbList}>
        <FlashSaleContainer />
      </AdminLayout>
    </div>
  );
};

export default FlashSales;
