import Footer from 'components/Footer';
import CustomerMyProfileDetails from 'container/Customer/GetMyProfile/getProfile';
import UserLayout from 'layouts/User';

const GetCustomerProfile = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Profile Details',
      route: '',
    },
  ];
  return (
    <UserLayout
      documentTitle={'Profile Details'}
      BreadCrumbList={BreadCrumbList}
    >
      <CustomerMyProfileDetails />
      <Footer />
    </UserLayout>
  );
};

export default GetCustomerProfile;
