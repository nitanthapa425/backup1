import Footer from 'components/Footer';
import CustomerProfileEdit from 'container/Customer/GetMyProfile/edit';
import UserLayout from 'layouts/User';

const CustomerSelfEdit = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Edit Profile ',
      route: '',
    },
  ];
  return (
    <UserLayout documentTitle="Edit Profile " BreadCrumbList={BreadCrumbList}>
      <CustomerProfileEdit />;
      <Footer />
    </UserLayout>
  );
};

export default CustomerSelfEdit;
