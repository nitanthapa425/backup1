import React from "react";
import DeliveryLayout from "layouts/Delivery";
import AdminBookDetail from "container/DeliveryItem/view/deliveryView";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Delivery List",
      route: "/deliveryItem",
    },
    {
      routeName: "Ordered Details",
      route: "",
    },
  ];

  return (
    <DeliveryLayout
      BreadCrumbList={BreadCrumbList}
      documentTitle="Ordered Details"
    >
      <AdminBookDetail />
    </DeliveryLayout>
  );
};

export default index;
