import Footer from "components/Footer";
import AboutUS from "container/About-us";
import WebsiteUserLayout from "layouts/WebsiteUser";

const we = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "About us",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout documentTitle="About Us" BreadCrumbList={BreadCrumbList}>
      <AboutUS />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default we;
