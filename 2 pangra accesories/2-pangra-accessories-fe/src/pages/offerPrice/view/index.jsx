// import React from 'react';
// import ViewOfferPriceContainer from 'container/OfferPrices/viewOfferPriceContainer';

// const OfferPriceList = () => <ViewOfferPriceContainer />;

// export default OfferPriceList;

import ViewOfferPriceContainer from "container/OfferPrices/viewOfferPriceContainer";
import AdminLayout from "layouts/Admin";

const OfferPriceList = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },

    {
      routeName: "Offers List",
      route: "",
    },
  ];
  return (
    <AdminLayout documentTitle="Offers List" BreadCrumbList={BreadCrumbList}>
      <ViewOfferPriceContainer />;
    </AdminLayout>
  );
};

export default OfferPriceList;
