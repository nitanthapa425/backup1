import Footer from "components/Footer";
import BuyGuide from "container/HowToBuy";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Buy = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "How to buy",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="How to buy"
      BreadCrumbList={BreadCrumbList}
    >
      <BuyGuide />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Buy;
