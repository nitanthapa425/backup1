import React from 'react';
import UserLayout from 'layouts/User';
// import CreateSellerForm from 'container/Seller/CreateSeller';
import Footer from 'components/Footer';
import AddSellBike from 'container/Seller/Add';

const Seller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Sell Vehicle',
      route: '',
    },
  ];
  return (
    <div>
      <UserLayout documentTitle="Sell Vehicle" BreadCrumbList={BreadCrumbList}>
        {/* <CreateSellerForm type="add" /> */}
        <AddSellBike type="add"></AddSellBike>
        <Footer />
      </UserLayout>
    </div>
  );
};

export default Seller;
