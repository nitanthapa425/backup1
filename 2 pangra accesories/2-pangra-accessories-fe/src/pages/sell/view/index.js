import Footer from 'components/Footer';
import ViewSellerContainer from 'container/Seller/viewSellerContainer';
import UserLayout from 'layouts/User';

const BreadCrumbList = [
  {
    routeName: 'Home',
    route: '/',
  },
  {
    routeName: 'Selling Vehicle List',
    route: '',
  },
];

const SellerTable = () => (
  <UserLayout
    documentTitle="Selling vehicle List"
    BreadCrumbList={BreadCrumbList}
  >
    <ViewSellerContainer />;
    <Footer />
  </UserLayout>
);

export default SellerTable;
