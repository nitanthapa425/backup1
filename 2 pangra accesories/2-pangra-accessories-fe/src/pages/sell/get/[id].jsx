import GetSingleSeller from 'container/Seller/getSingleSeller';
import React from 'react';
import UserLayout from 'layouts/User';
import Footer from 'components/Footer';

const viewSingleSeller = () => {
  const BreadCrumbList = [
    {
      routeName: 'Home',
      route: '/',
    },
    {
      routeName: 'Selling Vehicle List ',
      route: '/sell/view',
    },
    {
      routeName: 'Selling Vehicle Details',
      route: '',
    },
  ];
  return (
    <UserLayout
      documentTitle="Selling vehicle Details"
      BreadCrumbList={BreadCrumbList}
    >
      <GetSingleSeller />
      <Footer />
    </UserLayout>
  );
};

export default viewSingleSeller;
