import ViewLocationContainer from "container/GetFullLocation/LocationContainer";
import AdminLayout from "layouts/Admin";

const LocationFullContainer = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },

    {
      routeName: "Location List",
      route: "",
    },
  ];
  return (
    <AdminLayout documentTitle="View Location" BreadCrumbList={BreadCrumbList}>
      <ViewLocationContainer />;
    </AdminLayout>
  );
};

export default LocationFullContainer;
