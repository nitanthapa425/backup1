import FullLocation from "container/GetFullLocation";
import AdminLayout from "layouts/Admin";

const LocationFull = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },

    {
      routeName: "Add Location",
      route: "",
    },
  ];
  return (
    <AdminLayout documentTitle="Add Location" BreadCrumbList={BreadCrumbList}>
      <FullLocation />;
    </AdminLayout>
  );
};

export default LocationFull;
