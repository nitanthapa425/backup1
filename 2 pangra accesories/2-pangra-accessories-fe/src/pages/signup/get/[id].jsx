import GetSingleProfile from 'container/User/GetSingleUser';
import React from 'react';

const GetSingleProfilePage = () => {
  return (
    <div>
      <GetSingleProfile />
    </div>
  );
};

export default GetSingleProfilePage;
