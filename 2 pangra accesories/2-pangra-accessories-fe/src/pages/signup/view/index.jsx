import ViewUserContainer from "container/User/UserViewContainer";
import React from "react";

const ViewUser = () => {
  return (
    <div>
      <ViewUserContainer />
    </div>
  );
};

export default ViewUser;
