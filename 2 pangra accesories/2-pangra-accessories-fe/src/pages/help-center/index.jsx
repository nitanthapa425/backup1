import Footer from "components/Footer";
import Help from "container/Help";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Help Center",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout
      documentTitle="Help Center"
      BreadCrumbList={BreadCrumbList}
    >
      <Help />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
