import ForgotPasswordContainer from "container/ForgotPasswords/ForgotPasswordContainer";
import React from "react";

const ForgotPassword = () => {
  return (
    <div>
      <ForgotPasswordContainer />
    </div>
  );
};

export default ForgotPassword;
