import AddToCart from "container/Customer/ProductListingCom/AddToCart";
import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";
const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Cart List",
      route: "",
    },
  ];
  return (
    <UserLayout documentTitle={"Cart List"} BreadCrumbList={BreadCrumbList}>
      <AddToCart />
      <Footer />
    </UserLayout>
  );
};

export default index;
