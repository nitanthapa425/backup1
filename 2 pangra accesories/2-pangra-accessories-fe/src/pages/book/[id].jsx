import React from "react";
import AdminLayout from "layouts/Admin";
import AdminBookDetail from "container/AdminBook/adminBookDetail";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Ordered List",
      route: "/book/view",
    },
    {
      routeName: "Ordered Details",
      route: "",
    },
  ];

  return (
    <AdminLayout
      BreadCrumbList={BreadCrumbList}
      documentTitle="Ordered Details"
    >
      <AdminBookDetail />
    </AdminLayout>
  );
};

export default index;
