import AddToBookList from "container/Customer/ProductListingCom/BookList";
import React from "react";
import UserLayout from "layouts/User";
import Footer from "components/Footer";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Ordering List",
      route: "",
    },
  ];
  return (
    <UserLayout BreadCrumbList={BreadCrumbList} documentTitle="Buying List">
      <AddToBookList />
      <Footer />
    </UserLayout>
  );
};

export default index;
