import AdminBook from "container/AdminBook/viewAdminBook";
import React from "react";
import AdminLayout from "layouts/Admin";

const index = () => {
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    {
      routeName: "Ordered List",
      route: "",
    },
  ];

  return (
    <AdminLayout BreadCrumbList={BreadCrumbList} documentTitle="Ordered List">
      <AdminBook />
    </AdminLayout>
  );
};

export default index;
