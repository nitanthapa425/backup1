import React from "react";
import AdminLayout from "layouts/Admin";
import OrderedItemDetail from "container/AdminBook/Details";
// import { useRouter } from "next/router";

const index = () => {
  // const router = useRouter();
  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },
    // {
    //   routeName: "Ordered Items",
    //   route: `/book/orderedItemList/${router.query.id}`,
    //   // route: `/book/orderedItemList/${router.query.id}`,
    // },
    {
      routeName: "Ordered Item Details",
      route: "",
    },
  ];

  return (
    <AdminLayout
      BreadCrumbList={BreadCrumbList}
      documentTitle="Ordered Item Details"
    >
      <OrderedItemDetail />
    </AdminLayout>
  );
};

export default index;
