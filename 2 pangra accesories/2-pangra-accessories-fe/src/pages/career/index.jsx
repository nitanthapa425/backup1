import Footer from "components/Footer";
import Careers from "container/Career";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Career",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout documentTitle="Career" BreadCrumbList={BreadCrumbList}>
      <Careers />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
