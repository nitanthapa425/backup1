import Footer from "components/Footer";
import SiteMap from "container/SiteMap";
import WebsiteUserLayout from "layouts/WebsiteUser";

const Contact = () => {
  const BreadCrumbList = [
    {
      routeName: "Home",
      route: "/",
    },
    {
      routeName: "Sitemap",
      route: "",
    },
  ];
  return (
    <WebsiteUserLayout documentTitle="Sitemap" BreadCrumbList={BreadCrumbList}>
      <SiteMap />
      <Footer />
    </WebsiteUserLayout>
  );
};
export default Contact;
