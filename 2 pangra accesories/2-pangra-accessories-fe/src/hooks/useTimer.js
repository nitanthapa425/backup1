import { useEffect, useRef, useState } from "react";

const useCustomTimer = (sec = 0) => {
  // ***  always setTimer in sec setTimer(5) means 5s
  const [timer, setTimer] = useState(0);

  const [startTimer, setStartTimer] = useState(false);
  const ref = useRef(null);

  useEffect(() => {
    ref.current = setInterval(() => {
      setTimer(timer - 1);
    }, 1000);

    if (timer === 0) {
      setTimer(0);
      clearInterval(ref.current);
    }
    return () => {
      clearInterval(ref.current);
    };
  });

  useEffect(() => {
    if (startTimer) {
      setTimer(sec);
    }
    setStartTimer(false);
  }, [startTimer]);

  return [
    timer,
    setStartTimer,
    () => {
      clearInterval(ref.current);
    },
  ];
};

export default useCustomTimer;
