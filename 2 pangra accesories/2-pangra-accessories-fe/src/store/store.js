import { configureStore } from "@reduxjs/toolkit";
import adminAuthReducer from "./features/adminAuth/adminAuthSlice";
import customerAuthReducer from "./features/customerAuth/customerAuthSlice";
import deliveryAuthReducer from "./features/deliveryAuth/deliveryAuthSlice";
import levelReducer from "./features/adminCustomerAuth/adminCustomerAuth";
import BookSlice from "./features/book/BookSlice";
import addVehicleUnitsReducer from "./features/selectedUnits/addVehicleUnitsSlice";
import { adminLogin } from "services/api/adminLogin";
import { customerLogin } from "services/api/customerLogin";
import { deliveryLogin } from "services/api/deliveryLogin";
import { delivery } from "services/api/adminDelivery/delivery";

import { filesUpload } from "services/api/filesUpload";
import { subModel } from "services/api/varient";
import { forgotPassword } from "services/api/forgotPassword";
import { resetPassword } from "services/api/resetPassword";
import { passwordUpdate } from "services/api/passwordUpdate";
import { seller } from "services/api/seller";
import { signup } from "services/api/signup";
import { customer } from "services/api/customer";
import { location } from "services/api/location";
import { customerSelf } from "services/api/customerSelf";
import { adminCart } from "services/api/adminCartList";
import { loginCheck } from "services/api/loginCheck";
import { log } from "services/api/log";
import { hotDeal } from "services/api/hotdeals";
import { emailSubscribe } from "services/api/emailSubscribe";
import { notification } from "services/api/notification";
import { getFullLocation } from "services/api/getFullLocation";
// import { offerPrice } from 'services/api/offerPrice';
// import { Comment } from 'services/api/comment';

// *** For 2Pangra Accessories
import { brand } from "services/api/BrandService";
import { category } from "services/api/CategoryService";
import { subCategory } from "services/api/SubCategoryService";
import { product } from "services/api/ProductService";
import { company } from "services/api/company";
import { productDraft } from "services/api/ProductDraft";
import { model } from "services/api/ModelService";
import { shipping } from "services/api/Shipping";
import { flashSale } from "services/api/flashSale";
import { inventory } from "services/api/Inventory/inventoryService";
import { enquiryForm } from "services/api/EnquiryForm.Service/enquiryFormService";
import { soldItems } from "services/api/SoldItems/soldItemsService";
import { color } from "services/api/Color/colorService";
import { comments } from "services/api/comment";

export const store = configureStore({
  reducer: {
    adminAuth: adminAuthReducer,
    customerAuth: customerAuthReducer,
    deliveryAuth: deliveryAuthReducer,
    addVehicleUnits: addVehicleUnitsReducer,
    book: BookSlice,

    levelReducer: levelReducer,
    [adminLogin.reducerPath]: adminLogin.reducer,
    [customerLogin.reducerPath]: customerLogin.reducer,
    [deliveryLogin.reducerPath]: deliveryLogin.reducer,
    [delivery.reducerPath]: delivery.reducer,
    [customerSelf.reducerPath]: customerSelf.reducer,

    [filesUpload.reducerPath]: filesUpload.reducer,
    [model.reducerPath]: model.reducer,
    [subModel.reducerPath]: subModel.reducer,
    [forgotPassword.reducerPath]: forgotPassword.reducer,
    [resetPassword.reducerPath]: resetPassword.reducer,
    [passwordUpdate.reducerPath]: passwordUpdate.reducer,
    [seller.reducerPath]: seller.reducer,
    [signup.reducerPath]: signup.reducer,
    [customer.reducerPath]: customer.reducer,
    [location.reducerPath]: location.reducer,
    [adminCart.reducerPath]: adminCart.reducer,
    [loginCheck.reducerPath]: loginCheck.reducer,
    [log.reducerPath]: log.reducer,
    [hotDeal.reducerPath]: hotDeal.reducer,
    [emailSubscribe.reducerPath]: emailSubscribe.reducer,
    [notification.reducerPath]: notification.reducer,
    [getFullLocation.reducerPath]: getFullLocation.reducer,
    [brand.reducerPath]: brand.reducer,
    [category.reducerPath]: category.reducer,
    [subCategory.reducerPath]: subCategory.reducer,
    [product.reducerPath]: product.reducer,
    [company.reducerPath]: company.reducer,
    [productDraft.reducerPath]: productDraft.reducer,
    [shipping.reducerPath]: shipping.reducer,
    [flashSale.reducerPath]: flashSale.reducer,
    [inventory.reducerPath]: inventory.reducer,
    [enquiryForm.reducerPath]: enquiryForm.reducer,
    [soldItems.reducerPath]: soldItems.reducer,
    [color.reducerPath]: color.reducer,
    [comments.reducerPath]: comments.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      adminLogin.middleware,
      customerLogin.middleware,
      deliveryLogin.middleware,
      delivery.middleware,
      filesUpload.middleware,
      brand.middleware,

      subModel.middleware,
      forgotPassword.middleware,
      resetPassword.middleware,
      passwordUpdate.middleware,
      seller.middleware,
      signup.middleware,
      customer.middleware,
      location.middleware,
      customerSelf.middleware,
      adminCart.middleware,
      loginCheck.middleware,
      log.middleware,
      hotDeal.middleware,
      emailSubscribe.middleware,
      notification.middleware,
      getFullLocation.middleware,
      model.middleware,
      category.middleware,
      subCategory.middleware,
      product.middleware,
      company.middleware,
      productDraft.middleware,
      shipping.middleware,
      flashSale.middleware,
      inventory.middleware,
      enquiryForm.middleware,
      soldItems.middleware,
      color.middleware,
      comments.middleware,
    ]),
});
