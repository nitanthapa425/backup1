import { createSlice } from "@reduxjs/toolkit";

const initialState = { level: "" };

const slice = createSlice({
  name: "userLevel",
  initialState,
  reducers: {
    setSuperAdminLevel: (state, action) => {
      state.level = "superAdmin";
    },
    setCustomerLevel: (state, action) => {
      state.level = "customer";
    },
    setDeliveryLevel: (state, action) => {
      state.level = "delivery";
    },
  },
});

export default slice.reducer;
export const { actions: levelAction } = slice;
