import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  fuelCapacityUnit: "",
  wheelBaseUnit: "",
  overallWidthUnit: "",
  overallLengthUnit: "",
  overallHeightUnit: "",
  groundClearanceUnit: "",
  bootSpaceUnit: "",
  kerbWeightUnit: "",
  doddleHeightUnit: "",
};

export const addVehicleUnitsSlice = createSlice({
  name: "addVehicleUnits",
  initialState,
  reducers: {
    setFuelCapacityUnit: (state, action) => {
      state.fuelCapacityUnit = action.payload;
    },
    setWheelBaseUnit: (state, action) => {
      state.wheelBaseUnit = action.payload;
    },
    setOverallWidthUnit: (state, action) => {
      state.overallWidthUnit = action.payload;
    },
    setOverallLengthUnit: (state, action) => {
      state.overallLengthUnit = action.payload;
    },
    setOverallHeightUnit: (state, action) => {
      state.overallHeightUnit = action.payload;
    },
    setGroundClearanceUnit: (state, action) => {
      state.groundClearanceUnit = action.payload;
    },
    setBootSpaceUnit: (state, action) => {
      state.bootSpaceUnit = action.payload;
    },
    setKerbWeightUnit: (state, action) => {
      state.kerbWeightUnit = action.payload;
    },
    setDoddleHeightUnit: (state, action) => {
      state.doodleHeightUnit = action.payload;
    },
  },
});

export const {
  setFuelCapacityUnit,
  setWheelBaseUnit,
  setOverallWidthUnit,
  setOverallLengthUnit,
  setOverallHeightUnit,
  setGroundClearanceUnit,
  setBootSpaceUnit,
  setKerbWeightUnit,
  setDoddleHeightUnit,
} = addVehicleUnitsSlice.actions;

export default addVehicleUnitsSlice.reducer;
