import { createSlice, createSelector } from "@reduxjs/toolkit";
import {
  getDeliveryToken,
  setDeliveryToken,
  removeDeliveryToken,
  setDelivery,
  getDelivery,
} from "config/deliveryStorage";
import { deliveryLogin } from "services/api/deliveryLogin";

const initialState = { delivery: null, token: null };

const slice = createSlice({
  name: "deliveryAuth",
  initialState,
  reducers: {
    setDeliveryToken: (state) => {
      const token = getDeliveryToken() || null;
      state.token = token;
    },
    removeDeliveryToken: (state) => {
      removeDeliveryToken();
      state.token = null;
      state.delivery = null;
    },
    setDelivery: (state) => {
      const delivery = getDelivery() || null;
      state.delivery = JSON.parse(delivery);
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      deliveryLogin.endpoints.deliveryLogin.matchFulfilled,
      (state, { payload }) => {
        // set Token
        setDeliveryToken(payload?.delivery?.accessToken);
        setDelivery(payload?.delivery);
        state.token = payload.delivery.accessToken;
        state.delivery = payload.delivery;
      }
    );
  },
});

export default slice.reducer;
export const { actions: deliveryActions } = slice;

const selectDomain = (state) => state.deliveryAuth || slice.initialState;

// SELECTORS
export const selectDeliveryToken = createSelector(
  [selectDomain],
  (deliveryState) => deliveryState.token
);
export const selectDelivery = createSelector(
  [selectDomain],
  (deliveryState) => deliveryState.delivery
);
