import { createSlice, createSelector } from "@reduxjs/toolkit";
import {
  getAdminToken,
  setAdminToken,
  removeAdminToken,
  setUser,
  getUser,
} from "config/adminStorage";
import { adminLogin } from "services/api/adminLogin";
const initialState = { user: null, token: null };
const slice = createSlice({
  name: "adminAuth",
  initialState,
  reducers: {
    setAdminToken: (state) => {
      const token = getAdminToken() || null;
      state.token = token;
    },
    removeAdminToken: (state) => {
      removeAdminToken();
      state.token = null;
      state.user = null;
    },
    setUser: (state) => {
      const user = getUser() || null;
      state.user = JSON.parse(user);
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      adminLogin.endpoints.loginAdmin.matchFulfilled,
      (state, { payload }) => {
        // set Token
        // console.log('payload data', payload?.user);
        setAdminToken(payload?.user?.accessToken);
        setUser(payload?.user);
        state.token = payload.user.accessToken;
        state.user = payload.user;
      }
    );
  },
});
export default slice.reducer;
export const { actions: adminActions } = slice;
const selectDomain = (state) => state.adminAuth || slice.initialState;
// SELECTORS
export const selectAdminToken = createSelector(
  [selectDomain],
  (adminState) => adminState.token
);
export const selectAdminUser = createSelector(
  [selectDomain],
  (adminState) => adminState.user
);
