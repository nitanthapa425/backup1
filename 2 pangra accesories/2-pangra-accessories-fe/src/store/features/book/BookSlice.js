import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  bookList: [],
};

export const BookSlice = createSlice({
  name: "bookSlice",
  initialState,
  reducers: {
    setBookList: (state, action) => {
      state.bookList = action.payload;
    },
    setAddress: (state, action) => {
      state.address = action.payload;
    },
  },
});

export const { setBookList, setAddress } = BookSlice.actions;

export default BookSlice.reducer;
