/* eslint-disable react/display-name */
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { getAdminToken } from "config/adminStorage";
import { useIsLoginQuery } from "services/api/loginCheck";
import { useDispatch } from "react-redux";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";
import { adminActions } from "store/features/adminAuth/adminAuthSlice";
// import { adminActions } from 'store/features/adminAuth/adminAuthSlice';

const withAdminLogin = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== "undefined") {
      const [show, setShow] = useState(false);
      const Router = useRouter();
      const dispatch = useDispatch();

      const [skipFetching, setSkipFetching] = useState(true);

      const { isSuccess, isError } = useIsLoginQuery("", {
        skip: skipFetching,
      });

      useEffect(() => {
        const accessToken = getAdminToken();

        if (!accessToken) {
          setShow(true);
          return null;
        } else {
          dispatch(adminActions.setAdminToken());
          dispatch(adminActions.setUser());
          dispatch(levelAction.setSuperAdminLevel());
          setSkipFetching(false);
        }
      }, []);

      useEffect(() => {
        if (isError === true) {
          setShow(true);
          dispatch(adminActions.removeAdminToken());
        }
      }, [isError]);
      useEffect(() => {
        if (isSuccess === true) {
          Router.push("/admin/product/create");
        }
      }, [isSuccess]);

      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    return <div>Loading...</div>;
  };
};

export default withAdminLogin;
