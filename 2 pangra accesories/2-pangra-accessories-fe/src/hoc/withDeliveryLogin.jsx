/* eslint-disable react/display-name */
import { useEffect, useState } from "react";
// import { getAdminToken } from 'config/adminStorage';

import { useRouter } from "next/router";
import { getDeliveryToken } from "config/deliveryStorage";
import { deliveryActions } from "store/features/deliveryAuth/deliveryAuthSlice";
import { useDispatch } from "react-redux";
import { useIsLoginDeliveryQuery } from "services/api/loginCheck";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";

const withDeliveryLogin = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== "undefined") {
      const [show, setShow] = useState(false);
      const Router = useRouter();
      const dispatch = useDispatch();

      const [skipFetching, setSkipFetching] = useState(true);

      const { isSuccess, isError } = useIsLoginDeliveryQuery("", {
        skip: skipFetching,
      });

      useEffect(() => {
        const accessToken = getDeliveryToken();
        // if token is available land to Home page
        if (!accessToken) {
          setShow(true);

          // Router.push("/delivery/view");
        } else {
          dispatch(deliveryActions.setDeliveryToken());
          dispatch(deliveryActions.setDelivery());
          dispatch(levelAction.setDeliveryLevel());
          setSkipFetching(false);
        }
      }, []);

      useEffect(() => {
        if (isError === true) {
          setShow(true);
          dispatch(deliveryActions.removeDeliveryToken());
        }
      }, [isError]);
      useEffect(() => {
        if (isSuccess === true) {
          Router.push("/deliveryItem");
        }
      }, [isSuccess]);

      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    return <div>Loading...</div>;
  };
};

export default withDeliveryLogin;
