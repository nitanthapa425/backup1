/* eslint-disable react/display-name */
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
// import { adminActions } from 'store/features/adminAuth/adminAuthSlice';
// import { getAdminToken } from 'config/adminStorage';
import { getCustomerToken } from "config/customerStorage";
import { customerActions } from "store/features/customerAuth/customerAuthSlice";
import axios from "axios";
import { baseUrl } from "config";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";

const withCustomerAuth = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== "undefined") {
      const [show, setShow] = useState(false);
      const Router = useRouter();
      const dispatch = useDispatch();

      const [isError, setIsError] = useState(false);
      const [isSuccess, setIsSuccess] = useState(false);
      const loginInfo = useSelector((state) => state?.customerAuth?.token);

      useEffect(() => {
        if (loginInfo) {
          // console.log('loginINfo', loginInfo);
          axios(`${baseUrl}/client/validate-user`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${loginInfo}`,
            },
          })
            .then((res) => {
              setIsSuccess(true);
              setIsError(false);
            })
            .catch((e) => {
              setIsError(true);
              setIsSuccess(false);
            });
        }
      }, [loginInfo]);

      useEffect(() => {
        const accessToken = getCustomerToken();
        // If there is no access token we redirect to admin login page.
        if (!accessToken) {
          Router.replace("/customer/login");
          return null;
        } else {
          // if (Router.pathname === '/admin') {
          //   Router.replace('/admin/product/create');
          // }
          // load admin token to store
          dispatch(customerActions.setCustomerToken());
          dispatch(customerActions.setCustomer());
          dispatch(levelAction.setCustomerLevel());

          // setShow(true);
        }
      }, []);

      useEffect(() => {
        if (isError === true) {
          Router.push("/customer/login");
          // removing token and user from store and local storage
          dispatch(customerActions.removeCustomerToken());
        }
      }, [isError]);
      useEffect(() => {
        if (isSuccess === true) {
          setShow(true);
        }
      }, [isSuccess]);

      // If this is an accessToken we just render the component that was passed with all its props
      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    // If we are on server, return null
    // when return null it was giving error that server and client have different div
    return <div>Loading...</div>;
  };
};

export default withCustomerAuth;
