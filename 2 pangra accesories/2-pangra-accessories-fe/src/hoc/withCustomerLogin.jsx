/* eslint-disable react/display-name */
import { useEffect, useState } from "react";
// import { getAdminToken } from 'config/adminStorage';
import { getCustomerToken } from "config/customerStorage";
import { useRouter } from "next/router";
import { customerActions } from "store/features/customerAuth/customerAuthSlice";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";
import { useDispatch } from "react-redux";
import { useIsLoginCustomerQuery } from "services/api/loginCheck";

const withCustomerLogin = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== "undefined") {
      const [show, setShow] = useState(false);
      const Router = useRouter();

      const dispatch = useDispatch();

      const [skipFetching, setSkipFetching] = useState(true);

      const { isSuccess, isError } = useIsLoginCustomerQuery("", {
        skip: skipFetching,
      });

      useEffect(() => {
        const accessToken = getCustomerToken();
        // if token is available land to Home page
        if (!accessToken) {
          // Router.push("/");
          setShow(true);
        } else {
          dispatch(customerActions.setCustomerToken());
          dispatch(customerActions.setCustomer());
          dispatch(levelAction.setCustomerLevel());
          setSkipFetching(false);
        }
      }, []);

      useEffect(() => {
        if (isError === true) {
          setShow(true);
          dispatch(customerActions.removeCustomerToken());
        }
      }, [isError]);
      useEffect(() => {
        if (isSuccess === true) {
          Router.push("/");
        }
      }, [isSuccess]);

      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    return <div>Loading...</div>;
  };
};

export default withCustomerLogin;
