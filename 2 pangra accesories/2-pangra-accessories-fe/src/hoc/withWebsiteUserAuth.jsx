/* eslint-disable react/display-name */
import { useEffect, useState } from "react";
// import { useRouter } from 'next/router';
import { useDispatch } from "react-redux";
// import { getCustomerToken } from 'config/customerStorage';
import { customerActions } from "store/features/customerAuth/customerAuthSlice";

const withWebsiteUserAuth = (WrappedComponent) => {
  return (props) => {
    // checks whether we are on client / browser or server.
    if (typeof window !== "undefined") {
      //   const Router = useRouter();
      const dispatch = useDispatch();

      const [show, setShow] = useState(false);

      useEffect(() => {
        dispatch(customerActions.setCustomerToken());
        dispatch(customerActions.setCustomer());
        setShow(true);
      }, []);

      // If this is an accessToken we just render the component that was passed with all its props
      return show ? <WrappedComponent {...props} /> : <div>Loading...</div>;
    }

    // If we are on server, return null
    // when return null it was giving error that server and client have different div
    return <div>Loading...</div>;
  };
};

export default withWebsiteUserAuth;
