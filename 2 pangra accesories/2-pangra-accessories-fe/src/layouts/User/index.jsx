import BreadCrumb from "components/BreadCrumb/breadCrumb";

import UserHeader from "components/Header/UserHeader";
import { messengerId } from "constant/constant";

// import withAdminAuth from 'hoc/withAdminAuth';
import withCustomerAuth from "hoc/withCustomerAuth";
import Head from "next/head";
import MessengerCustomerChat from "react-messenger-customer-chat/lib/MessengerCustomerChat";
import { useDispatch } from "react-redux";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";

function UserLayout({ title, children, documentTitle, BreadCrumbList = [] }) {
  const dispatch = useDispatch();
  dispatch(levelAction.setCustomerLevel());
  return (
    <div className="customer-wrapper">
      {/* <Head>
        <title>{title}</title>
      </Head>
      <main className="container">{children}</main> */}
      <Head>
        <title>2Pangra | {documentTitle}</title>
      </Head>
      <MessengerCustomerChat
        pageId={messengerId.pageId}
        appId={messengerId.appId}
      />

      <UserHeader />
      {BreadCrumbList?.length !== 0 && (
        <div className="container">
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div>
      )}

      {children}
    </div>
  );
}

export default withCustomerAuth(UserLayout);
