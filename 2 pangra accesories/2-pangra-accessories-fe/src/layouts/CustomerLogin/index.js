// import withLoginSignUp from 'hoc/withAdminLogin';

import withCustomerLogin from 'hoc/withCustomerLogin';

const CustomerLog = ({ children }) => {
  return <>{children}</>;
};

export default withCustomerLogin(CustomerLog);
