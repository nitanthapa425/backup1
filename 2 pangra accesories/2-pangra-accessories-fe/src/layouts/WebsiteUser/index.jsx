import BreadCrumb from "components/BreadCrumb/breadCrumb";

import UserHeader from "components/Header/UserHeader";

// import withCustomerAuth from 'hoc/withCustomerAuth';
import withWebsiteUserAuth from "hoc/withWebsiteUserAuth";
import Head from "next/head";
import { useDispatch } from "react-redux";
import { levelAction } from "store/features/adminCustomerAuth/adminCustomerAuth";
import MessengerCustomerChat from "react-messenger-customer-chat";
import { messengerId } from "constant/constant";

function WebsiteUserLayout({ children, documentTitle, BreadCrumbList = [] }) {
  const dispatch = useDispatch();
  dispatch(levelAction.setCustomerLevel());
  return (
    <div className="customer-wrapper">
      {/* <Head>
        <title>{title}</title>
      </Head>
      <main className="container">{children}</main> */}
      <Head>
        <title>2Pangra | {documentTitle}</title>
      </Head>
      <MessengerCustomerChat
        pageId={messengerId.pageId}
        appId={messengerId.appId}
      />

      <UserHeader />

      {BreadCrumbList?.length !== 0 ? (
        <div className="container">
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div>
      ) : null}

      {children}
    </div>
  );
}

export default withWebsiteUserAuth(WebsiteUserLayout);
