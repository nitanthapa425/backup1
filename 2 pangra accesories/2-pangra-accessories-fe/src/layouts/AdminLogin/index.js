import withAdminLogin from 'hoc/withAdminLogin';

const AdminLogins = ({ children }) => {
  return <>{children}</>;
};

export default withAdminLogin(AdminLogins);
