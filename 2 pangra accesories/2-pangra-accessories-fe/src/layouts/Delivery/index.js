import Head from "next/head";
import withDeliveryAuth from "hoc/withDeliveryAuth";
// import Header from 'components/Header/Header';
// import BreadCrumb from 'components/BreadCrumb/breadCrumb';
// import Dashboard from "components/Dashboard";
import PrivateRoutes from "components/PrivateRoutes/PrivateRoutes";
import DeliveryHeader from "components/Header/DeliveryHeader";
// import PrivateRoutes from 'components/PrivateRoutes/PrivateRoutes';

const DeliveryLayout = ({
  documentTitle,
  BreadCrumbList,
  children,
  noHeader = false,
  noContainer = false,
  showFor = ["DELIVERY"],
}) => {
  return (
    <>
      <PrivateRoutes showFor={showFor}>
        <Head>
          <title>2Pangra | {documentTitle}</title>
        </Head>
        <DeliveryHeader></DeliveryHeader>
        {children}
        {/* <Dashboard
          noHeader={noHeader}
          noContainer={noContainer}
          BreadCrumbList={BreadCrumbList}
        > */}
        {/* {children} */}
        {/* </Dashboard> */}
        {/* <div className="container">
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div> */}
      </PrivateRoutes>
    </>
  );
};

export default withDeliveryAuth(DeliveryLayout);
