// import withLoginSignUp from 'hoc/withAdminLogin';

import withDeliveryLogin from "hoc/withDeliveryLogin";

const DeliveryLog = ({ children }) => {
  return <>{children}</>;
};

export default withDeliveryLogin(DeliveryLog);
