import { useState, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import PropTypes from "prop-types";

function UnitDropdown({ defaultUnit, restUnits, dispatchFunc, globalState }) {
  const [selectedUnit, setSelectedUnit] = useState(defaultUnit);

  const dispatch = useDispatch();
  const didMount = useRef(false);

  useEffect(() => {
    if (globalState.length) {
      setSelectedUnit(globalState);
    } else {
      dispatch(dispatchFunc(selectedUnit));
    }
  }, []);

  useEffect(() => {
    if (didMount.current) {
      dispatch(dispatchFunc(selectedUnit));
    } else {
      didMount.current = true;
    }
  }, [selectedUnit]);

  const handleChange = (e) => {
    setSelectedUnit(e.target.value);
  };

  return (
    <div className="unit-converter-dropdown">
      <select
        name="unit"
        id="unit"
        value={selectedUnit}
        onChange={handleChange}
      >
        <option value={defaultUnit}>{defaultUnit}</option>
        {restUnits.map((u, index) => (
          <option value={u} key={index}>
            {u}
          </option>
        ))}
      </select>
    </div>
  );
}

UnitDropdown.propTypes = {
  defaultUnit: PropTypes.string.isRequired,
  restUnits: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default UnitDropdown;
