import { useState, useLayoutEffect } from "react";

// Accepted values cardColsSize = four or three.
function LoadingCard({ cardColsSize = "four" }) {
  if (typeof window !== "undefined") {
    const [winWidth, setWinWidth] = useState(0);

    const handleSize = () => {
      setWinWidth(window.innerWidth);
    };

    useLayoutEffect(() => {
      setWinWidth(window.innerWidth);
      window.addEventListener("resize", handleSize);
      return () => window.removeEventListener("resize", handleSize);
    }, []);

    const markup = (
      <div
        className={
          cardColsSize === "three"
            ? "three-col"
            : cardColsSize === "two"
            ? "two-col"
            : "four-col"
        }
      >
        <div className="h-[250px] bg-gray-300 rounded-lg animate-pulse"></div>
      </div>
    );

    // Make sure to use them inside the parent "row" class
    return (
      <>
        {winWidth > 0 && winWidth <= 640 ? (
          markup
        ) : winWidth > 640 && winWidth <= 768 ? (
          <>
            {markup}
            {markup}
          </>
        ) : winWidth > 768 && winWidth <= 1024 ? (
          <>
            {markup}
            {markup}
            {markup}
          </>
        ) : (
          <>
            {cardColsSize === "three" ? (
              <>
                {markup}
                {markup}
                {markup}
              </>
            ) : (
              <>
                {markup}
                {markup}
                {markup}
                {markup}
              </>
            )}
          </>
        )}
      </>
    );
  } else {
    return <div></div>;
  }
}

export default LoadingCard;
