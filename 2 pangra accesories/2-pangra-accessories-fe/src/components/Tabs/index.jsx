import Tabs, { TabPane } from "rc-tabs";
import PropTypes from "prop-types";
import Accordion from "components/Accordion";
import "../../../node_modules/rc-tabs/assets/index.css";

function CustomTabs({
  tabHeadings,
  hasAccordion,
  accordionData = [],
  tabData = [],
}) {
  return (
    <Tabs defaultActiveKey="1">
      {hasAccordion ? (
        <>
          {tabHeadings.map((heading, tabHeadingIndex) => (
            <TabPane tab={heading} key={`${tabHeadingIndex + 1}`}>
              <div className="pb-2 pl-2">
                {accordionData.map((data, accDataIndex) =>
                  // Checking if the accordion data belongs to the correct tab based on tabHeadingIndex and data.tabNumber
                  data.tabNumber === tabHeadingIndex + 1 ? (
                    <Accordion
                      heading={data.heading}
                      data={data.list}
                      key={accDataIndex}
                    />
                  ) : null
                )}
              </div>
            </TabPane>
          ))}
        </>
      ) : (
        <>
          {/* {tabHeadings.map((heading, index) => (
            <TabPane tab={heading} key={`${index + 1}`}>
              {`Tab ${index + 1}`}
            </TabPane>
          ))} */}
          {tabData.map((tabObj, tabHeadingIndex) => (
            <TabPane tab={tabObj.heading} key={`${tabHeadingIndex + 1}`}>
              <div>
                {tabObj.list.map((item, index) => (
                  <div
                    className="detail-page-table  py-2 sm:py-3 px-3 flex flex-wrap"
                    key={index}
                  >
                    <span className="font-bold inline-block  mr-1 md:min-w-[200px] w-[54%] md:w-[40%] ">
                      {item.label} :&emsp;
                    </span>
                    <span className="flex-1">
                      {
                        /* If Data is undefined, then return a - */ !item.data
                          ? "-"
                          : item.data
                      }
                    </span>
                  </div>
                ))}
              </div>
            </TabPane>
          ))}
        </>
      )}
    </Tabs>
  );
}

Tabs.propTypes = {
  tabHeadings: PropTypes.arrayOf(PropTypes.string),
  hasAccordion: PropTypes.bool,
  accordionData: PropTypes.array,
  tabData: PropTypes.array,
};

// const tabHeadings = ['Options & Features', 'Technical Specification'];

/*
    const accordionData = [
        {
            heading: "Engine & Transmission",
            list: [
                {
                    label: 'Sample Text',
                    data: 'Input Text'
                },
                {
                    label: 'Sample Text',
                    data: 'Input Text'
                }
            ]
        },
        {
            heading: "Brakes, Wheels & Suspension",
            list: [
                {
                    label: 'Sample Text 1212',
                    data: 'Input Text'
                },
                {
                    label: 'Sample Text',
                    data: 'Input Text'
                }
            ]
        },
    ]
*/

export default CustomTabs;
