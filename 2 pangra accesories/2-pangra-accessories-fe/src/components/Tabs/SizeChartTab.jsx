import Tabs, { TabPane } from "rc-tabs";
import React from "react";
import "../../../node_modules/rc-tabs/assets/index.css";

const SizeChartTab = ({ tabData = [] }) => {
  // console.log("hello i am on 2");
  return (
    <Tabs defaultActiveKey="1">
      {tabData.map((tabObj, tabHeadingIndex) => (
        <TabPane tab={tabObj.heading} key={`${tabHeadingIndex + 1}`}>
          {tabObj.Component}
        </TabPane>
      ))}
    </Tabs>
  );
};

export default SizeChartTab;
