import { useRef, useEffect } from "react";
import createReactClass from "create-react-class";
import SelectEl, { components } from "react-select";

const SearchWithSelect = ({
  isMulti = true,
  onChange,
  value,
  options,
  placeholder,
  loading = false,
  customClear = false,
}) => {
  const refEl = useRef(null);
  const Option = createReactClass({
    render() {
      return (
        <div>
          <components.Option {...this.props}>
            <input
              type="checkbox"
              checked={this.props.isSelected}
              onChange={(e) => null}
            />{" "}
            <label>{this.props.value} </label>
          </components.Option>
        </div>
      );
    },
  });

  // If we want to manually clear this select by clicking some button, we just pass customClear as false.
  useEffect(() => {
    if (customClear) {
      refEl.current?.clearValue();
    }
  }, [customClear]);

  return (
    <>
      <SelectEl
        onChange={onChange}
        options={options}
        placeholder={placeholder}
        isMulti={isMulti}
        closeMenuOnSelect={false}
        components={{ Option }}
        hideSelectedOptions={false}
        backspaceRemovesValue={false}
        isLoading={loading}
        ref={refEl}
      />
    </>
  );
};

export default SearchWithSelect;
