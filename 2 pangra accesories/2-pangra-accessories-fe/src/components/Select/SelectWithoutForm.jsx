const SelectWithoutForm = ({ label, required = false, disabled, ...props }) => {
  return (
    <div>
      <label htmlFor={props.id || props.name}>
        {label}{" "}
        <span className="required">
          <span>*</span>
        </span>
      </label>
      <select disabled={disabled} {...props} />
    </div>
  );
};

export default SelectWithoutForm;
