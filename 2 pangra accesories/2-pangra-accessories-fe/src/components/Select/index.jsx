import { useField } from "formik";

const Select = ({ label, required = false, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div>
      <label htmlFor={props.id || props.name}>
        {label}
        <span className="required">
          <span>{required && "*"}</span>
        </span>
      </label>
      <select
        {...field}
        {...props}
        className={`${
          meta.touched && meta.error ? "border placeholder-primary-dark" : ""
        } disabled:bg-[#f2f2f2] border-[#e6e6e6] disabled:text-[#B9B9B9]`}
      />
      {meta.touched && meta.error ? (
        <div className=" text-error text-sm">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Select;
