import SelectEl from "react-select/creatable";
const CheckBoxMultiSelect = ({ onChange, value, options, placeholder }) => {
  return (
    <>
      <SelectEl
        options={options}
        value={
          options
            ? options.filter((option) => value?.includes(option.value))
            : []
        }
        onChange={onChange}
        isMulti={true}
      />
    </>
  );
};

export default CheckBoxMultiSelect;
