import SelectEl, { components } from "react-select";
import PropTypes from "prop-types";
import createReactClass from "create-react-class";
import "react-medium-image-zoom/dist/styles.css";

const SelectWithoutCreateImage = ({
  required,
  label,
  error,
  onChange,
  value,
  options,
  placeholder,
  loading,
  isDisabled,
  isMulti = false,
  menuPlacement = "auto",
  onBlur,
}) => {
  const colourStyles = {
    placeholder: (defaultStyles) => {
      return {
        ...defaultStyles,
        color: "#B9B9B9",
      };
    },
  };
  const Option = createReactClass({
    render() {
      return (
        <div>
          <components.Option {...this.props}>
            <span>
              <img
                // alt="that wanaka tree"
                src={this.props.data.flag}
                width="20"
                height="20"
                className="inline-block mr-1"
              />

              <label className="inline-block">{this.props.label} </label>
            </span>
          </components.Option>
        </div>
      );
    },
  });
  return (
    <>
      <label htmlFor="">
        {label}
        <span className="required">
          <span>{required && "*"}</span>
        </span>
      </label>
      <SelectEl
        key={Date.now()}
        value={
          isMulti
            ? options
              ? options.filter((option) => value.includes(option.value))
              : []
            : options
            ? options.find((option) => option.value === value)
            : ""
        }
        isOptionDisabled={(option) => option.isDisabled}
        onChange={onChange}
        options={options}
        placeholder={placeholder}
        isLoading={loading}
        isDisabled={isDisabled}
        isMulti={isMulti}
        className={error ? "border-error placeholder-error" : ""}
        menuPlacement={menuPlacement}
        onBlur={onBlur}
        styles={colourStyles}
        components={{ Option }}
        hideSelectedOptions={false}
        backspaceRemovesValue={false}
      />

      {true && <div className="text-error text-sm">{error}</div>}
    </>
  );
};

SelectWithoutCreateImage.prototype = {
  required: PropTypes.bool,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  options: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  placeholder: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  menuPlacement: PropTypes.string,
};

export default SelectWithoutCreateImage;
