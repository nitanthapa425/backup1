import React, { useState } from "react";
import AsyncSelect from "react-select/async";
import { components } from "react-select";
import { useDebouncedCallback } from "use-debounce";

export function asyncDebounce(func, wait) {
  const debounced = useDebouncedCallback(async (resolve, reject, bindSelf, args) => {
    try {
      const result = await func.bind(bindSelf)(...args);
      resolve(result);
    } catch (error) {
      reject(error);
    }
  }, wait);

  // This is the function that will be bound by the caller, so it must contain the `function` keyword.
  function returnFunc(...args) {
    return new Promise((resolve, reject) => {
      debounced(resolve, reject, this, args);
    });
  }

  return returnFunc;
}

const SelectAsync = ({
  label,
  required,
  placeholder,
  dataLoader = () => { },
  onChange,
  isMulti,
  refEl,
  error,
  onBlur,
  value
}) => {
  const [inputValue, setInputValue] = useState("");

  const handleInputChange = async (inputValue, { action }) => {
    if (action !== "set-value") {
      setInputValue(inputValue);
    }
  };

  const loadOptions = asyncDebounce(
    (inputValue) => dataLoader(inputValue), 100)


  const NoOptionsMessage = (props) => {
    return (
      <components.NoOptionsMessage {...props}>
        <span className="custom-css-class">
          {inputValue ? "No Options" : "Type to search"}
        </span>
      </components.NoOptionsMessage>
    );
  };

  return (
    <>
      {label && (
        <label htmlFor="">
          {label}
          <span className="required">
            <span>{required && "*"}</span>
          </span>
        </label>
      )}
      <AsyncSelect
        // closeMenuOnSelect={false}
        components={{ NoOptionsMessage }}
        inputValue={inputValue}
        onInputChange={handleInputChange}
        loadOptions={loadOptions}
        cacheOptions={true}
        defaultOptions
        placeholder={placeholder}
        onChange={onChange}
        isMulti={isMulti}
        ref={refEl}
        onBlur={onBlur}
        value={value}
        isLoading={false}
      />
      {error && <div className="text-error">{error}</div>}

    </>
  );
};
export default SelectAsync;
