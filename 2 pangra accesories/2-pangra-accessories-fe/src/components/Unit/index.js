import { useField } from "formik";

const Unit = ({ label, onChange, ...props }) => {
  const [field, meta] = useField(props);

  return (
    <div className="unit-converter-dropdown">
      <select
        {...field}
        {...props}
        onChange={onChange}
        className="border-transparent bg-transparent shadow-none focus:ring-0 text-right pr-[46px] !important"
      />
      {meta.touched && meta.error ? (
        <div className="text-primary-dark">{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Unit;
