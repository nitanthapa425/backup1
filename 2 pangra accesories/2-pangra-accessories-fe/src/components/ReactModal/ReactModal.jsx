import Button from "components/Button";
import React, { useEffect, useState } from "react";
import Modal from "react-modal";

import Link from "next/link";
import { useRouter } from "next/router";

const ReactModal = ({
  variant = "primary",
  name,
  component,
  children,
  disabled = false,
  link = "",
  redirect = "",
  directlyOpen = false,
  closeFunc,
  modal = false,
  loading = false,
  RatingReview,
}) => {
  const [modalIsOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(false);
  }, [modal]);

  const router = useRouter();

  useEffect(() => {
    if (directlyOpen) {
      setIsOpen(true);
    }
  }, [directlyOpen]);

  const openModal = () => {
    setIsOpen(true);
  };

  useEffect(() => {
    if (component === "Arjun") {
      setIsOpen(false);
    }
  }, [component]);

  const closeModal = () => {
    setIsOpen(false);
  };
  return (
    <>
      {directlyOpen ? null : link === "" ? (
        <Button
          variant={variant}
          type="button"
          onClick={() => {
            if (redirect) {
              router.push(redirect);
            } else {
              openModal();
            }
          }}
          disabled={disabled}
          loading={loading}
        >
          {name}
        </Button>
      ) : (
        <Link href="#">
          <a
            className="text-textColor hover:text-primary-dark"
            onClick={() => {
              openModal();
            }}
          >
            {link}
          </a>
        </Link>
      )}

      <Modal
        isOpen={modalIsOpen}
        onRequestClose={directlyOpen ? closeFunc : closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={directlyOpen ? closeFunc : closeModal}
          className="text-error absolute top-[10px] right-[10px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>
        <span>{children}</span>
      </Modal>
    </>
  );
};

export default ReactModal;
