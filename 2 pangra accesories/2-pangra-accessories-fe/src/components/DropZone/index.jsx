import { useCallback, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useToasts } from "react-toast-notifications";
import { useDropzone } from "react-dropzone";
import {
  useDeleteFileMutation,
  useUploadFilesMutation,
} from "services/api/filesUpload";

const MAX_NUMBER_OF_FILES = 10;

const DropZone = ({
  label,
  required,
  currentFiles,
  setNewFiles,
  handleRemoveFile,
  error,
  maxFiles,
}) => {
  const [uploadFiles, { isLoading, isError, data, isSuccess }] =
    useUploadFilesMutation();
  const [
    deleteFile,
    {
      isLoading: deletingFile,
      isError: fileDeleteError,
      isSuccess: fileDeleteSuccess,
    },
  ] = useDeleteFileMutation();

  const { addToast } = useToasts();

  const [currentFileIndex, setCurrentFileIndex] = useState(null);

  const onDrop = useCallback((acceptedFiles) => {
    if (acceptedFiles.length > 0) {
      const formData = new FormData();
      acceptedFiles.forEach((file) => {
        formData.append("files", file);
      });
      uploadFiles(formData);
    }
  }, []);

  useEffect(() => {
    if (isSuccess && data) {
      const fileData = data.map((file) => {
        return {
          imageUrl: file.imageUrl,
          publicKey: file.publicKey,
        };
      });
      setNewFiles(fileData);
    }
  }, [isSuccess]);

  const { getRootProps, getInputProps, fileRejections } = useDropzone({
    onDrop,
    accept: ".jpeg,.jpg,.png",
    maxFiles: maxFiles || MAX_NUMBER_OF_FILES,
  });

  useEffect(() => {
    if (fileRejections.length > 0) {
      addToast(fileRejections[0]?.errors[0]?.message, {
        appearance: "error",
      });
    }
  }, [JSON.stringify(fileRejections)]);

  useEffect(() => {
    if (isError) {
      addToast("Error occured while uploading images.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const handleDeleteFile = (index, publicKey) => {
    setCurrentFileIndex(index);
    deleteFile(publicKey);
  };

  useEffect(() => {
    if (fileDeleteError) {
      addToast("Error occured while deleting image.Please try again later.", {
        appearance: "error",
      });
    }
    if (fileDeleteSuccess) {
      currentFileIndex !== null && handleRemoveFile(currentFileIndex);
    }
  }, [fileDeleteError, fileDeleteSuccess]);

  return (
    <>
      <label htmlFor="">
        {label}
        <span className="required">
          <span>{required && "*"}</span>
        </span>
      </label>

      <div
        {...getRootProps()}
        className={`h-40 rounded-md border ${
          error ? "border" : "border-gray-400"
        }`}
      >
        <input {...getInputProps()} disabled={isLoading || deletingFile} />
        <p className="text-sm font-bold h-full w-full flex justify-center items-center">
          {!deletingFile &&
            (isLoading ? "Uploading images..." : "Upload or Drag Image")}
          {deletingFile && "Deleting file..."}
        </p>
      </div>

      {error && <div className="text-error text-sm mt-1">{error}</div>}

      <div className="my-2 grid grid-cols-3 gap-1">
        {currentFiles?.map((file, i) => {
          return (
            <div key={i} className="relative">
              {/* <Image
                src={file.imageUrl}
                // title={file}
                width={50}
                height={50}
                layout="responsive"
                className="
                  rounded
                  cursor-pointer
                  transition duration-200 ease-in-out
                  transform  hover:scale-125"
              /> */}
              <img
                src={file.imageUrl}
                alt="image descriptions"
                className="
                  rounded
                  cursor-pointer
                  w-[150px]
                  h-[100px]
                  object-cover
                  "
              />
              <div
                className="absolute top-0 bottom-0 right-0"
                onClick={
                  !deletingFile
                    ? () => handleDeleteFile(i, file.publicKey)
                    : () => {}
                }
              >
                <svg
                  className={`fill-current h-6 w-6 bg-white text-error ${
                    deletingFile ? "opacity-70" : ""
                  } rounded-full`}
                  role="button"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 20 20"
                >
                  <title>Remove</title>
                  <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                </svg>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

DropZone.propTypes = {
  label: PropTypes.string.isRequired,
  required: PropTypes.bool,
  currentFiles: PropTypes.arrayOf(PropTypes.string),
  setNewFiles: PropTypes.func.isRequired,
  handleRemoveFile: PropTypes.func.isRequired,
  error: PropTypes.string,
  maxFiles: PropTypes.number,
};

export default DropZone;
