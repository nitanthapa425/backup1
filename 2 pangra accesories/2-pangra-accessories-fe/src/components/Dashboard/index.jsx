import Image from "next/image";
import Link from "next/link";
import { useEffect } from "react";
import {
  ViewGridIcon,
  TruckIcon,
  UsersIcon,
  TagIcon,
  // SupportIcon,
  // CubeTransparentIcon,
  ChevronRightIcon,
  ShoppingCartIcon,
  ShoppingBagIcon,
  PresentationChartBarIcon,
  UserIcon,
  AtSymbolIcon,
  LocationMarkerIcon,
  OfficeBuildingIcon,
  FireIcon,
  PencilAltIcon,
  CogIcon,
  LightningBoltIcon,
  CashIcon,
  RefreshIcon,
  ChartBarIcon,
  ChatIcon,
} from "@heroicons/react/outline";
import Header from "components/Header/Header";
import BreadCrumb from "components/BreadCrumb/breadCrumb";
import DashAccordion from "./DashAccordion";
import { adminActions } from "store/features/adminAuth/adminAuthSlice";
import { useToasts } from "react-toast-notifications";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { useLogoutSuperAdminMutation } from "services/api/loginCheck";
import Notification from "components/Notification/Notification";
import ReactModal from "components/ReactModal/ReactModal";
import PasswordUpdateContainer from "container/PasswordUpdate/PasswordUpdateContainer";
import { useGetMyCartAdminQuery } from "services/api/adminCartList";

function Dashboard({ noHeader, children, noContainer, BreadCrumbList }) {
  const body = document.querySelector("body");
  const { addToast } = useToasts();
  const router = useRouter();
  const dispatch = useDispatch();
  const loginInfo = useSelector((state) => state.adminAuth);

  const hFunctions = () => {
    body.classList.toggle("sidebar-toggle");
  };

  const data = [
    {
      id: 1,
      exp: false,
      parentTitle: "Products",
      children: [
        {
          title: "Add",
          link: "/admin/product/create",
        },
        {
          title: "Product List",
          link: "/admin/product",
        },
        {
          title: "Draft List",
          link: "/admin/product/drafts",
        },
      ],
    },

    {
      id: 2,
      exp: false,
      parentTitle: "Product Grouping",
      children: [
        {
          title: "Brand List",
          link: "/admin/brand",
        },
        {
          title: "Model List",
          link: "/admin/model",
        },
        {
          title: "Category List",
          link: "/admin/category",
        },
        {
          title: "Sub Category List",
          link: "/admin/subcategory",
        },
      ],
    },
    // {
    //   id: 3,
    //   exp: false,
    //   parentTitle: "Category",
    //   children: [
    //     {
    //       title: "Add",
    //       link: "/admin/category/create",
    //     },
    //     {
    //       title: "List",
    //       link: "/admin/category",
    //     },
    //   ],
    // },

    // {
    //   id: 4,
    //   exp: false,
    //   parentTitle: "Sub Category",
    //   children: [
    //     {
    //       title: "Add",
    //       link: "/admin/subcategory/create",
    //     },
    //     {
    //       title: "List",
    //       link: "/admin/subcategory",
    //     },
    //   ],
    // },
    // {
    //   id: 5,
    //   exp: false,
    //   parentTitle: "Model",
    //   children: [
    //     {
    //       title: "Add",
    //       link: "/admin/model/create",
    //     },
    //     {
    //       title: "List",
    //       link: "/admin/model",
    //     },
    //   ],
    // },

    {
      id: 3,
      exp: false,
      parentTitle: "Users",
      children: [
        {
          title: "Add",
          link: "/admin/user/create",
        },
        {
          title: "List",
          link: "/admin/user",
        },
        {
          title: "Deleted Users",
          link: "/admin/user/inactive",
        },
        {
          title: "Delivery",
          link: "/delivery/view",
        },
        {
          title: "Deleted Delivery",
          link: "/admin/delivery/inactive",
        },
      ],
    },
    {
      id: 4,
      exp: false,
      parentTitle: "Customers",
      children: [
        {
          title: "List",
          link: "/customer/view",
        },
        {
          title: "Deleted Customers",
          link: "/deletedCustomer/view",
        },
      ],
    },
    {
      id: 5,
      exp: false,
      parentTitle: "Wish List",
      children: [
        {
          title: "List",
          link: "/wish-list/view",
        },
      ],
    },
    {
      id: 6,
      exp: false,
      parentTitle: "Cart",

      children: [
        {
          title: "List",
          link: "/cart/view",
        },
      ],
    },
    {
      id: 7,
      exp: false,
      parentTitle: "Ordered List",
      children: [
        {
          title: "Ordered List",
          link: "/book/view",
        },
        {
          title: "Delivered List",
          link: "/admin/delivered-product",
        },
      ],
    },
    {
      id: 8,
      exp: false,
      parentTitle: "Log",
      children: [
        {
          title: "List",
          link: "/log/view",
        },
      ],
    },
    {
      id: 9,
      exp: false,
      parentTitle: "Hot Deals",
      children: [
        {
          title: "List",
          link: "/hot-deals/view",
        },
      ],
    },
    {
      id: 10,
      exp: false,
      parentTitle: "Location",
      children: [
        {
          title: "Add",
          link: "/location",
        },
        {
          title: "List",
          link: "/location/view",
        },
      ],
    },
    {
      id: 11,
      exp: false,
      parentTitle: "Delivery Company",
      children: [
        {
          title: "Add",
          link: "/admin/company/create",
        },
        {
          title: "List",
          link: "/admin/company",
        },
      ],
    },
    {
      id: 12,
      exp: false,
      parentTitle: "Rating & Review",
      children: [
        {
          title: "List",
          link: "/ratingReview",
        },
      ],
    },
    {
      id: 13,
      exp: false,
      parentTitle: "Shipping Charge",
      children: [
        {
          title: "List",
          link: "/admin/shipping",
        },
      ],
    },
    {
      id: 14,
      exp: false,
      parentTitle: "Flash Sale",
      children: [
        {
          title: "List",
          link: "/flash-sale/view",
        },
      ],
    },
    {
      id: 15,
      exp: false,
      parentTitle: "Inventory",
      children: [
        {
          title: "List",
          link: "/admin/inventory",
        },
      ],
    },
    {
      id: 16,
      exp: false,
      parentTitle: "Inquiry",
      children: [
        {
          title: "List",
          link: "/inquiry",
        },
      ],
    },
    {
      id: 17,
      exp: false,
      parentTitle: "Sold Items",
      children: [
        {
          title: "Sold Items List",
          link: "/admin/soldItems",
        },
      ],
    },
    {
      id: 21,
      exp: false,
      parentTitle: "Comments",
      children: [
        {
          title: "List",
          link: "/admin/comments",
        },
      ],
    },
    {
      id: 19,
      exp: false,
      parentTitle: "Banner Image",
      children: [
        {
          title: "Add",
          link: "/admin/banner-image/create",
        },
      ],
    },
  ];

  const parentIcons = [
    <TruckIcon key={1} />,
    <TagIcon key={2} />,
    // <SupportIcon key={3} />,
    // <CubeTransparentIcon key={4} />,
    // <ViewGridIcon key={5} />,
    <UsersIcon key={3} />,
    <UserIcon key={4} />,
    <ShoppingBagIcon key={5} />,
    <ShoppingCartIcon key={6} />,
    <PresentationChartBarIcon key={7} />,
    <AtSymbolIcon key={8} />,
    <FireIcon key={9} />,
    <LocationMarkerIcon key={10} />,
    <OfficeBuildingIcon key={11} />,
    <PencilAltIcon key={12} />,
    <CogIcon key={13} />,
    <LightningBoltIcon key={14} />,
    <CashIcon key={15} />,
    <RefreshIcon key={16} />,
    <ChartBarIcon key={17} />,
    <ChatIcon key={18} />,
    <ChatIcon key={19} />,
  ];

  useEffect(() => {
    /*
        In the css class 'sidebar-toggle':
        In mobile, the class 'sidebar-toggle' in the body will open the sidebar.
        But in desktop, the class 'sidebar-toggle' in the body closes the sidebar.
        */
    if (window.innerWidth >= 768) {
      if (!body.classList.contains("sidebar-toggle")) {
        hFunctions();
      }
    } else {
      hFunctions();
    }
  }, []);

  const [LogoutSuperAdmin, { isError, isSuccess, data: dataLogoutSuperAdmin }] =
    useLogoutSuperAdminMutation();

  const handleLogout = () => {
    LogoutSuperAdmin();
  };

  useEffect(() => {
    if (isSuccess) {
      addToast(dataLogoutSuperAdmin?.message || "Logged Out successfully .", {
        appearance: "success",
      });
      dispatch(adminActions.removeAdminToken());
      router.replace("/admin");
    }

    if (isError) {
      dispatch(adminActions.removeAdminToken());
      router.replace("/admin");
    }
  }, [isSuccess, isError]);

  const { data: dataMyCart } = useGetMyCartAdminQuery();

  return (
    <section className="wrapper-holder">
      <aside className="sidebar rounded-r-[10px]">
        <div className="logo-details">
          <Link href="#">
            <a className="menu opener move" onClick={() => hFunctions()}>
              <ChevronRightIcon className="db-sidebar-icon" />
            </a>
          </Link>
          <Link href="/admin/product/create">
            <a className="logo-holder">
              <span className="logo-icon">
                <Image
                  src="/images/logo-icon.svg"
                  alt="2Pangra"
                  width={35}
                  height={50}
                />
              </span>

              <span className="logo-text">
                <Image
                  src="/images/logo-text.svg"
                  alt="2Pangra"
                  width={110}
                  height={50}
                />
              </span>
            </a>
          </Link>
        </div>
        <ul className="sidebar-menu">
          <li>
            <Link href="#">
              <a>
                <ViewGridIcon className="db-sidebar-icon" />
                <span className="link-name">Dashboard</span>
              </a>
            </Link>
          </li>
          <DashAccordion data={data} parentIcons={parentIcons} />
        </ul>
        <div className="a-profile">
          <div className="image-holder">
            {loginInfo?.user?.profileImagePath?.imageUrl ? (
              <img
                width="100%"
                height="100%"
                src={`${loginInfo?.user?.profileImagePath?.imageUrl}`}
              ></img>
            ) : (
              <a href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-8 w-8"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="#231F20"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={1}
                    d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
              </a>
            )}
          </div>
          <div className="detail-holder">
            <div className="name-post">
              <span className="name">{loginInfo?.user?.userName}</span>
              <span className="post">{loginInfo?.user?.accessLevel}</span>
            </div>
          </div>
          <ul className="a-profile-dropdown py-[15px] px-[10px] ">
            <li>
              {" "}
              <Link href={`/admin/user/my-profile`}>
                <a className="text-textColor hover:text-primary py-[4px] px-4">
                  My Profile
                </a>
              </Link>
            </li>

            <li>
              <ReactModal link="Update Password">
                <PasswordUpdateContainer />
              </ReactModal>
            </li>
            <li>
              <span
                className="text-textColor block hover:text-primary cursor-pointer px-[12px] py-[4px]"
                onClick={handleLogout}
              >
                Log Out
              </span>
            </li>
          </ul>
        </div>
      </aside>
      <div className="content-wrapper overflow-hidden">
        {!noHeader ? <Header /> : null}
        <div className="container">
          <ul className="admin-notification  hidden md:flex md:gap-5 absolute right-[60px] top-[18px] ">
            <Notification type="admin"></Notification>
            <li className="flex items-center relative has-dropdown cursor-pointer">
              <Link
                href="/admin/groupped-product"
                className="relative text-textColor hover:text-primary opacity-80 p-[12px] md:p-0"
              >
                <a>
                  <ShoppingCartIcon className="w-6 h-6" />

                  {dataMyCart?.length ? (
                    <span className="bg-error  rounded-full w-[22px] h-[22px] flex justify-center items-center text-[13px] absolute text-white top-[-13px] right-[-11px]">
                      {dataMyCart?.length}
                    </span>
                  ) : (
                    ""
                  )}
                </a>
              </Link>
            </li>
          </ul>
          <BreadCrumb BreadCrumbList={BreadCrumbList}></BreadCrumb>
        </div>
        <div className={noContainer ? "" : "container"}>{children}</div>
      </div>
    </section>
  );
}

export default Dashboard;
