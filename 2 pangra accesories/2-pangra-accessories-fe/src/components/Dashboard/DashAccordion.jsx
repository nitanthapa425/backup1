import Link from "next/link";
import { useState, useEffect } from "react";
import PropTypes from "prop-types";

function DashAccordion({ data, parentIcons }) {
  const [expand, setExpand] = useState([]);

  useEffect(() => {
    setExpand(
      data.map((item) => {
        return {
          id: item.id,
          exp: item.exp,
        };
      })
    );
  }, []);

  return (
    <>
      {data.map((item, itemIndex) => {
        return (
          <li
            className="accordion__item"
            onClick={() => {
              if (window.innerWidth >= 768) {
                if (
                  document
                    .querySelector("body")
                    .classList.contains("sidebar-toggle")
                ) {
                  document
                    .querySelector("body")
                    .classList.toggle("sidebar-toggle");
                }
              }
            }}
            key={itemIndex}
          >
            <Link href="#">
              <a
                id="accordion__trigger-1"
                className="accordion__item--trigger"
                aria-expanded={
                  !expand.length
                    ? false
                    : expand.find((ei) => ei.id === item.id).exp
                }
                aria-controls="accordion__panel-1"
                onClick={() => {
                  setExpand(
                    expand.map((i) => {
                      // If id matches, then toggle accordion i.e. open if closed and close if opened
                      if (i.id === item.id) {
                        return {
                          id: i.id,
                          exp: !i.exp,
                        };
                      } else {
                        // Close all acoordions whose id does not match
                        return {
                          id: i.id,
                          exp: false,
                        };
                      }
                    })
                  );
                }}
                key={item.id}
                title={item.parentTitle}
              >
                {parentIcons.map((Icon, iconIndex) => {
                  if (iconIndex === itemIndex) {
                    return (
                      <>
                        <span className="db-sidebar-icon" key={iconIndex}>
                          {Icon}
                        </span>
                      </>
                    );
                  }
                  return null;
                })}
                <span className="link-name">{item.parentTitle}</span>
              </a>
            </Link>
            <ul
              id="accordion__panel-1"
              className="accordion__item--panel sub-menu"
              aria-labelledby="accordion__trigger-1"
            >
              {item.children.map((child, childIndex) => {
                return (
                  <li key={childIndex}>
                    <Link href={child.link}>
                      <a>{child.title}</a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </li>
        );
      })}
    </>
  );
}

DashAccordion.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  parentIcons: PropTypes.arrayOf(PropTypes.element),
};

export default DashAccordion;
