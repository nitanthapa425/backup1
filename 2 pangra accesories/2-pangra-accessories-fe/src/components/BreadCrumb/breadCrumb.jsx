import React from "react";
import Link from "next/link";

const BreadCrumb = ({ BreadCrumbList = [] }) => {
  return (
    <ul className="breadcrumb py-3">
      {BreadCrumbList.map((list, i) => {
        return (
          <React.Fragment key={i}>
            <li className="inline-block">
              {i === BreadCrumbList.length - 1 ? (
                <div className="text-primary">{list.routeName}</div>
              ) : (
                <>
                  <Link disabled href={`${list.route}`}>
                    <a className="text-textColor  hover:text-primary">
                      {list.routeName}
                    </a>
                  </Link>
                  <div className="align-middle inline-block mx-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-4 w-4"
                      viewBox="0 0 20 20"
                      fill="#555"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10.293 15.707a1 1 0 010-1.414L14.586 10l-4.293-4.293a1 1 0 111.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                        clipRule="evenodd"
                      />
                      <path
                        fillRule="evenodd"
                        d="M4.293 15.707a1 1 0 010-1.414L8.586 10 4.293 5.707a1 1 0 011.414-1.414l5 5a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                </>
              )}
            </li>
          </React.Fragment>
        );
      })}
    </ul>
  );
};

export default BreadCrumb;
