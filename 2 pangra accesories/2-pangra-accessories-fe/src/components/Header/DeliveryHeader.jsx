import { useState, useEffect } from "react";
import Link from "next/link";
import { deliveryActions } from "store/features/deliveryAuth/deliveryAuthSlice";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import Notification from "components/Notification/Notification";
import ReactModal from "components/ReactModal/ReactModal";
import useOnclickOutside from "react-cool-onclickoutside";
import CustomerLoginPopUp from "components/LoginPopUp";
import DeliveryPasswordUpdateContainer from "container/Delivery/UpdatePassword";

const DeliveryHeader = () => {
  const [scrollPosition, setScrollPosition] = useState(0);

  const handleScroll = () => {
    if (typeof window !== "undefined") {
      const position = window.pageYOffset;
      setScrollPosition(position);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", handleScroll);

      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    }
  }, []);

  const loginInfo = useSelector((state) => state.deliveryAuth);
  const [openModal, setOpenModal] = useState(false);

  const token = loginInfo?.token;
  const dispatch = useDispatch();
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const switchFunctions = () => {
    const body = document.querySelector("body");
    body.classList.toggle("c-opener-menu");
  };
  const removeClass = () => {
    const body = document.querySelector("body");
    body.classList.remove("c-opener-menu");
  };
  const handleCustomerLogout = () => {
    dispatch(deliveryActions.removeDeliveryToken());
    router.replace("/delivery");
  };
  const ref = useOnclickOutside(removeClass);

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const setModalFunction = (value) => {
    setModal(value);
  };

  return (
    <div>
      <header
        id="header"
        className={`c-header z-[999] bg-white w-full transition delay-150 duration-300 ease-in-out ${
          scrollPosition > 10
            ? "header-fixed fixed shadow-xl !py-[10px]"
            : "!py-[15px]"
        }`}
      >
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}

        <div className="container flex justify-between md:justify-start">
          <div className="logo mr-6 w-[135px] sm:w-[160px]">
            <Link href="/">
              <a className="text-lg md:text-2xl font-bold mb-0">
                <img src="/images/logo.svg" alt="2Pangra" />
              </a>
            </Link>
          </div>
          <nav
            id="nav"
            className="bg-white rounded-t-[15px] z-50 md:rounded-none md:bg-transparent shadow-negative md:shadow-none flex-1 flex items-center fixed left-0 right-0 bottom-0  md:relative"
            ref={ref}
          >
            <div className="c-drop z-30 opacity-0 invisible flex-1 absolute bottom-12 right-[15px] bg-gray-600 rounded-md shadow-sm md:visible md:opacity-100 md:shadow-none md:rounded-none md:bg-transparent md:relative md:bottom-auto md:right-auto"></div>

            <ul className="menu-right flex items-center md:space-x-7 justify-between md:justify-items-center w-full md:w-auto">
              <li className="flex items-center relative has-dropdown"></li>
              {token ? <Notification /> : null}

              <li className="flex items-center relative has-dropdown cursor-pointer">
                {loginInfo?.delivery?.profileImagePath?.imageUrl ? (
                  <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                    <img
                      width="100%"
                      height="100%"
                      src={`${loginInfo?.delivery?.profileImagePath?.imageUrl}`}
                    ></img>
                  </div>
                ) : (
                  <a href="#">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-8 w-7"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="#231F20"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={1}
                        d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </a>
                )}
                {token ? (
                  <ul
                    className="Customer border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[180px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li>
                      {" "}
                      <Link href={`/delivery/myProfile`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          My Profile
                        </a>
                      </Link>
                    </li>
                    <li>
                      <a className="text-textColor hover:text-primary px-4 py-[7px]">
                        <ReactModal link="Update Password">
                          <DeliveryPasswordUpdateContainer />
                        </ReactModal>
                      </a>
                    </li>

                    <span
                      className="text-textColor hover:text-primary cursor-pointer px-4 py-[7px]"
                      onClick={handleCustomerLogout}
                    >
                      Log Out
                    </span>
                  </ul>
                ) : (
                  <ul
                    className="Guest border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[250px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                              transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li className="px-[12px] py-[5px]">
                      <h5 className="text">Hi Guest</h5>
                      <span className="text-sm inline-block mb-3">
                        Login to get your dream vehicle
                      </span>
                      <Link href="/customer/login">
                        <a className="btn btn-primary btn-sm w-full text-center px-4 py-[7px]">
                          Login
                        </a>
                      </Link>
                    </li>
                  </ul>
                )}
              </li>

              <a
                className="opener md:hidden p-[12px] md:p-0"
                href="#"
                onClick={switchFunctions}
              >
                {" "}
                <span> </span>
              </a>
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
};
export default DeliveryHeader;
