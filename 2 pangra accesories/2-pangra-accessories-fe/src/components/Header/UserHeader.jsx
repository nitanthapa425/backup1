import { useState, useEffect } from "react";
import Link from "next/link";
import {
  SearchIcon,
  MenuAlt3Icon,
  ChevronDownIcon,
} from "@heroicons/react/outline";
import { customerActions } from "store/features/customerAuth/customerAuthSlice";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import Button from "components/Button";
import useDebounce from "hooks/useDebounce";
import { useReadBrandNoAuthWithLimitQuery } from "services/api/brand";
import Notification from "components/Notification/Notification";
import ReactModal from "components/ReactModal/ReactModal";
import CustomerPasswordUpdateContainer from "container/Customer/UpdatePassword";
import useOnclickOutside from "react-cool-onclickoutside";
import CustomerLoginPopUp from "components/LoginPopUp";
import LoadingCustom from "components/LoadingCustom/LoadingCustom";
import {
  useGetMyCartQuery,
  useReadCategoryNoAuthWithLimitQuery,
  useReadSubCategoryNoAuthWithLimitQuery,
} from "services/api/seller";
import { useLogoutCustomerMutation } from "services/api/loginCheck";
import { useToasts } from "react-toast-notifications";
import Popup from "components/Popup";
import InquiryForm from "container/InquiryForm/inqueryForm";
import Modal from "react-modal";
import { ShoppingCartIcon } from "@heroicons/react/solid";
import { useCustomerMyProfileQuery } from "services/api/customerSelf";
// import { useReadBrandModelListQuery } from "services/api/ModelService";

const UserHeader = () => {
  const [openModel, setOpenModel] = useState(false);
  const [scrollPosition, setScrollPosition] = useState(0);
  const [modelOpenInquiry, setModelOpenInquiry] = useState(false);
  const closeModelOpenInquiry = () => {
    setModelOpenInquiry(false);
  };

  const { addToast } = useToasts();

  const handleScroll = () => {
    if (typeof window !== "undefined") {
      const position = window.pageYOffset;
      setScrollPosition(position);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", handleScroll);

      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    }
  }, []);

  const loginInfo = useSelector((state) => state.customerAuth);
  const [searchedVehicle, setSearchedVehicle] = useState("");
  const [searchedBrand, setSearchedBrand] = useState("");
  const debSearchedBrand = useDebounce(searchedBrand, 1000);
  const [searchedCategory, setSearchedCategory] = useState("");
  const [searchedSubCategory, setSearchedSubCategory] = useState("");
  const debSearchedCategory = useDebounce(searchedCategory, 1000);
  const debSearchedSubCategory = useDebounce(searchedSubCategory, 1000);
  const [pageSize, setPageSize] = useState(24);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  // const [selectedBrandId, setSelectedBrandId] = useState();

  const [brandQuery, setBrandQuery] = useState(
    `?select=id,brandName&limit=${pageSize}&sortBy=brandName&sortOrder=1`
  );
  const [categoryQuery, setCategoryQuery] = useState(
    `?select=id,categoryName&limit=${pageSize}&sortBy=categoryName&sortOrder=1`
  );
  const [subCategoryQuery, setSubCategoryQuery] = useState(
    `?select=id,subCategoryName&limit=${pageSize}&sortBy=subCategoryName&sortOrder=1`
  );

  const {
    data: brandData,
    isError: isErrorBrandData,
    isLoading: isLoadingBrandData,
    isFetching: isFetchingBrandData,
  } = useReadBrandNoAuthWithLimitQuery(brandQuery);

  // const { data: modelData } = useReadBrandModelListQuery(selectedBrandId, {
  //   skip: !selectedBrandId,
  // });

  // useEffect(() => {
  //   setSelectedBrandId(brandData?.brandId);
  // }, [brandData?.brandId]);

  const { data: customerProfile } = useCustomerMyProfileQuery();

  const {
    data: categoryData,
    isError: isErrorCategoryData,
    isLoading: isLoadingCategoryData,
    isFetching: isFetchingCategoryData,
  } = useReadCategoryNoAuthWithLimitQuery(categoryQuery);
  const {
    data: subCategoryData,
    isError: isErrorSubCategoryData,
    isLoading: isLoadingSubCategoryData,
    isFetching: isFetchingSubCategoryData,
  } = useReadSubCategoryNoAuthWithLimitQuery(subCategoryQuery);

  const [
    logoutCustomer,
    {
      isLoading: isLoadingLogoutCusomer,
      isError: isErrorLogoutCustomer,
      isSuccess: isSuccessLogoutCustomer,
      data: dataLogoutCustomer,
    },
  ] = useLogoutCustomerMutation();

  const { data: dataMyCart } = useGetMyCartQuery();

  const token = loginInfo?.token;
  const dispatch = useDispatch();
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const switchFunctions = () => {
    const body = document.querySelector("body");
    body.classList.toggle("c-opener-menu");
  };
  const removeClass = () => {
    const body = document.querySelector("body");
    body.classList.remove("c-opener-menu");
  };
  const handleCustomerLogout = () => {
    logoutCustomer();
  };
  const ref = useOnclickOutside(removeClass);

  useEffect(() => {
    if (isSuccessLogoutCustomer) {
      addToast(
        dataLogoutCustomer?.message || "You have successfully Log Out.",
        {
          appearance: "success",
        }
      );
      dispatch(customerActions.removeCustomerToken());
      router.replace("/customer/login");
    }
    if (isErrorLogoutCustomer) {
      dispatch(customerActions.removeCustomerToken());
      router.replace("/customer/login");
    }
  }, [isSuccessLogoutCustomer, isErrorLogoutCustomer]);

  useEffect(() => {
    setBrandQuery(
      `?select=id,brandName&search="brandName":"${debSearchedBrand}"&limit=${pageSize}&sortBy=brandName&sortOrder=1`
    );
  }, [debSearchedBrand, pageSize]);

  useEffect(() => {
    if (brandData) {
      setHasNextPage(brandData?.hasNextPage);
    }
  }, [brandData]);

  useEffect(() => {
    setCategoryQuery(
      `?select=id,categoryName&search="categoryName":"${debSearchedCategory}"&limit=${pageSize}&sortBy=categoryName&sortOrder=1`
    );
  }, [debSearchedCategory, pageSize]);
  useEffect(() => {
    setSubCategoryQuery(
      `?select=id,subCategoryName&search="subCategoryName":"${debSearchedSubCategory}"&limit=${pageSize}&sortBy=subCategoryName&sortOrder=1`
    );
  }, [debSearchedSubCategory, pageSize]);

  useEffect(() => {
    if (categoryData) {
      setHasNextPage(categoryData?.hasNextPage);
    }
  }, [categoryData]);
  useEffect(() => {
    if (subCategoryData) {
      setHasNextPage(subCategoryData?.hasNextPage);
    }
  }, [subCategoryData]);

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const setModalFunction = (value) => {
    setModal(value);
  };

  return (
    <div>
      <header
        id="header"
        className={`c-header z-[999] bg-white w-full transition-all  top-0 duration-500  fixed py-[15px]${
          scrollPosition > 20
            ? "header-fixed shadow-xl !py-[10px]"
            : "!py-[15px]"
        }`}
      >
        <Modal
          isOpen={modelOpenInquiry}
          className="mymodal order"
          overlayClassName="myoverlay"
        >
          <button
            type="button"
            onClick={closeModelOpenInquiry}
            className="text-error absolute top-[5px] right-[5px]"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <InquiryForm closeModelOpenInquiry={closeModelOpenInquiry} />
        </Modal>

        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}

        {openModel && (
          <Popup
            title="Are you sure you want to Log Out ?"
            onOkClick={handleCustomerLogout}
            onCancelClick={() => setOpenModel(false)}
            okText="Yes"
            cancelText="No"
            loading={isLoadingLogoutCusomer}
          />
        )}

        <div className="container flex justify-center md:justify-start">
          <div className="logo md:mr-6 w-[135px] sm:w-[160px] h-[33px]">
            <Link href="/">
              <a className="mb-0">
                <img src="/images/logo.svg" alt="2Pangra " />
              </a>
            </Link>
          </div>
          <nav
            id="nav"
            className="bg-white rounded-t-[15px] z-50 md:rounded-none md:bg-transparent shadow-negative md:shadow-none flex-1 flex items-center fixed left-0 right-0 bottom-0  md:relative"
            ref={ref}
          >
            <div className="c-drop  z-30 opacity-0 invisible flex-1 absolute bottom-12 right-[15px] bg-gray-600 rounded-md shadow-sm md:visible md:opacity-100 md:shadow-none md:rounded-none md:bg-transparent md:relative md:bottom-auto md:right-auto">
              <ul className="c-menu-center text-sm md:flex md:flex-wrap md:items-center md:space-x-2 md:justify-center overflow-visible">
                <li>
                  <Link href="/hot-deals">
                    <a className="text-white md:text-textColor hover:tex  t-primary py-[4px] px-4">
                      Hot Deals
                    </a>
                  </Link>
                </li>
                {/* <li>
                  <Link href="/product-listing">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      On Sale
                    </a>
                  </Link>
                </li> */}
                <li className="relative has-dropdown cursor-pointer">
                  <Link href="#">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Brands{" "}
                      <ChevronDownIcon className="w-3 h-3 text-textColor inline-block" />
                    </a>
                  </Link>

                  <ul
                    className=" border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[230px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto   md:top-full md:bottom-auto"
                  >
                    <div className="search relative px-4">
                      <input
                        type="search"
                        placeholder="Search Brands"
                        className=" border-0 p-[5px] shadow-none border-b-[1px] border-gray-400 mb-3 rounded-none focus:outline-none focus:ring-0"
                        onChange={(e) => {
                          setSearchedBrand(e.target.value);
                        }}
                      />
                      <LoadingCustom
                        show={[isLoadingBrandData || isFetchingBrandData]}
                      ></LoadingCustom>
                    </div>
                    <>
                      {isErrorBrandData ? (
                        <li>
                          <Link href="#">
                            <a className="text-textColor hover:text-primary py-[4px] px-4">
                              Error loading data
                            </a>
                          </Link>
                        </li>
                      ) : isLoadingBrandData ? (
                        <p className="f-sm text-center">Loading...</p>
                      ) : brandData?.docs?.length ? (
                        brandData?.docs?.map((b, i) => (
                          <li key={i} className="has-dropdown group">
                            <Link
                              href={`/product-listing?browseBy=brand&brandName=${b?.brandName}&`}
                            >
                              <a className="text-textColor hover:text-primary py-[4px] px-4">
                                {b?.brandName}
                              </a>
                            </Link>
                          </li>
                        ))
                      ) : null}
                    </>
                    {hasNextPage ? (
                      <div className="text-center">
                        <Button
                          type="button"
                          variant="secondary"
                          disabled={!hasNextPage}
                          onClick={() => {
                            setPageSize(pageSize + 12);
                          }}
                          loading={isFetchingBrandData}
                          size="sm"
                        >
                          Load More
                        </Button>
                      </div>
                    ) : null}
                  </ul>
                </li>
                <li className="relative has-dropdown cursor-pointer">
                  <Link href="#">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Category{" "}
                      <ChevronDownIcon className="w-3 h-3 text-textColor inline-block" />
                    </a>
                  </Link>

                  <ul
                    className="border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[230px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <div className="search relative px-4">
                      <input
                        type="search"
                        placeholder="Search Category"
                        className=" border-0 p-[5px] shadow-none border-b-[1px] border-gray-400 mb-3 rounded-none focus:outline-none focus:ring-0"
                        onChange={(e) => {
                          setSearchedCategory(e.target.value);
                        }}
                      />
                      <LoadingCustom
                        show={[isLoadingCategoryData || isFetchingCategoryData]}
                      ></LoadingCustom>
                    </div>
                    <>
                      {isErrorCategoryData ? (
                        <li>
                          <Link href="#">
                            <a className="text-textColor hover:text-primary py-[4px] px-4">
                              Error loading data
                            </a>
                          </Link>
                        </li>
                      ) : isLoadingCategoryData ? (
                        <p className="f-sm text-center">Loading...</p>
                      ) : categoryData?.docs?.length ? (
                        categoryData?.docs?.map((b, i) => (
                          <li key={i}>
                            <Link
                              href={`/product-listing?browseBy=category&categoryName=${b?.categoryName}&`}
                            >
                              <a className="text-textColor hover:text-primary py-[4px] px-4">
                                {b?.categoryName}
                              </a>
                            </Link>
                          </li>
                        ))
                      ) : null}
                    </>
                    {hasNextPage ? (
                      <div className="text-center">
                        <Button
                          type="button"
                          variant="secondary"
                          disabled={!hasNextPage}
                          onClick={() => {
                            setPageSize(pageSize + 12);
                          }}
                          loading={isFetchingCategoryData}
                          size="sm"
                        >
                          Load More
                        </Button>
                      </div>
                    ) : null}
                  </ul>
                </li>
                <li className="relative has-dropdown cursor-pointer">
                  <Link href="#">
                    <a className="text-white md:text-textColor hover:text-primary py-[4px] px-4">
                      Sub Category{" "}
                      <ChevronDownIcon className="w-3 h-3 text-textColor inline-block" />
                    </a>
                  </Link>

                  <ul
                    className="border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[230px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <div className="search relative px-4">
                      <input
                        type="search"
                        placeholder="Search Sub Category"
                        className=" border-0 p-[5px] shadow-none border-b-[1px] border-gray-400 mb-3 rounded-none focus:outline-none focus:ring-0"
                        onChange={(e) => {
                          setSearchedSubCategory(e.target.value);
                        }}
                      />
                      <LoadingCustom
                        show={[
                          isLoadingSubCategoryData || isFetchingSubCategoryData,
                        ]}
                      ></LoadingCustom>
                    </div>
                    <>
                      {isErrorSubCategoryData ? (
                        <li>
                          <Link href="#">
                            <a className="text-textColor hover:text-primary py-[4px] px-4">
                              Error loading data
                            </a>
                          </Link>
                        </li>
                      ) : isLoadingSubCategoryData ? (
                        <p className="f-sm text-center">Loading...</p>
                      ) : subCategoryData?.docs?.length ? (
                        subCategoryData?.docs?.map((b, i) => (
                          <li key={i}>
                            <Link
                              href={`/product-listing?browseBy=subCategory&subCategoryName=${b?.subCategoryName}&`}
                            >
                              <a className="text-textColor hover:text-primary py-[4px] px-4">
                                {b?.subCategoryName}
                              </a>
                            </Link>
                          </li>
                        ))
                      ) : null}
                    </>
                    {hasNextPage ? (
                      <div className="text-center">
                        <Button
                          type="button"
                          variant="secondary"
                          disabled={!hasNextPage}
                          onClick={() => {
                            setPageSize(pageSize + 12);
                          }}
                          loading={isFetchingSubCategoryData}
                          size="sm"
                        >
                          Load More
                        </Button>
                      </div>
                    ) : null}
                  </ul>
                </li>
                <li>
                  <a
                    className=" hover:text-primary-light hover:cursor-pointer"
                    onClick={() => {
                      setModelOpenInquiry(true);
                    }}
                  >
                    Inquiry
                  </a>
                </li>
              </ul>
            </div>

            <ul className="menu-right flex items-center md:space-x-7 justify-between md:justify-items-center w-full md:w-auto">
              <li className="flex items-center relative has-dropdown">
                <a
                  href="#"
                  className="text-textColor hover:text-primary opacity-80 p-[12px] md:p-0"
                >
                  <SearchIcon className="w-5 h-5" />
                </a>
                <ul
                  className="Guest border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[250px] invisible absolute z-50 bottom-[100%] right-0 bg-white shadow-md mt-3
                              transition-all max-h-[400px] overflow-y-auto md:top-full md:bottom-auto"
                >
                  <li className="px-[12px] py-[5px]">
                    <span className="text-sm inline-block mb-3">
                      <div className="search relative">
                        <SearchIcon className="h-5 w-5 absolute top-1/2 translate-y-[-60%] left-3" />
                        <input
                          type="search"
                          placeholder="Search Accessories."
                          className="pl-[36px]"
                          onChange={(e) => {
                            setSearchedVehicle(e.target.value);
                          }}
                        />
                      </div>
                    </span>
                    <div className="text-center">
                      <Button
                        type="button"
                        variant={
                          searchedVehicle !== "" ? "primary" : "disabled"
                        }
                        onClick={() =>
                          router.push(
                            `/product-listing?search=${searchedVehicle}`
                          )
                        }
                      >
                        Search
                      </Button>
                    </div>
                  </li>
                </ul>
              </li>

              {token ? <Notification /> : null}

              <li className="flex items-center relative has-dropdown cursor-pointer">
                <a
                  href="/cart"
                  className="relative text-textColor hover:text-primary opacity-80 p-[12px] md:p-0"
                >
                  <ShoppingCartIcon className="w-5 h-5" />

                  {dataMyCart?.length ? (
                    <span className="bg-error  rounded-full w-[22px] h-[22px] flex justify-center items-center text-[13px] absolute text-white top-[-13px] right-[-11px]">
                      {dataMyCart?.length}
                    </span>
                  ) : (
                    ""
                  )}
                </a>
              </li>

              <li className="flex items-center relative has-dropdown cursor-pointer">
                {customerProfile?.profileImagePath?.imageUrl ? (
                  <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                    <img
                      width="100%"
                      height="100%"
                      src={`${customerProfile?.profileImagePath?.imageUrl}`}
                    ></img>
                  </div>
                ) : (
                  <a href="#">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="h-8 w-7"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="#231F20"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={1}
                        d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                  </a>
                )}
                {token ? (
                  <ul
                    className="Customer border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[180px] invisible absolute z-50 bottom-[100%] -left-20 bg-white shadow-md mt-3
                transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li>
                      {" "}
                      <Link href={`/wish-list`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Wish List
                        </a>
                      </Link>
                    </li>
                    <li>
                      {" "}
                      <Link href={`/cart`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Cart List
                        </a>
                      </Link>
                    </li>

                    <li>
                      {" "}
                      <Link href={`/ordered`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Ordered List
                        </a>
                      </Link>
                    </li>
                    <li>
                      {" "}
                      <Link href={`/ordered/delivered`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Delivered List
                        </a>
                      </Link>
                    </li>
                    <li>
                      {" "}
                      <Link href={`/ordered/cancelled`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          Cancelled List
                        </a>
                      </Link>
                    </li>

                    <li>
                      {" "}
                      <Link href={`/customer-profile/get`}>
                        <a className="text-textColor hover:text-primary py-[4px] px-4">
                          My Profile
                        </a>
                      </Link>
                    </li>
                    <li>
                      <a className="text-textColor hover:text-primary px-4 py-[7px]">
                        <ReactModal link="Update Password">
                          <CustomerPasswordUpdateContainer />
                        </ReactModal>
                      </a>
                    </li>

                    <span
                      className="text-textColor hover:text-primary cursor-pointer px-4 py-[7px]"
                      onClick={() => {
                        setOpenModel(true);
                      }}
                    >
                      Log Out
                    </span>
                  </ul>
                ) : (
                  <ul
                    className="Guest border dropdown border-gray-200 py-2 rounded-md opacity-0 w-[250px] invisible absolute z-50 bottom-[100%] right-0 bg-white shadow-md mt-3
                              transition-all max-h-[400px] overflow-y-auto  md:top-full md:bottom-auto"
                  >
                    <li className="px-[12px] py-[5px]">
                      <h5 className="text">Hi Guest</h5>
                      <span className="text-sm inline-block mb-3">
                        Login to get your safety accessories
                      </span>
                      <Link href="/customer/login">
                        <a className="btn btn-primary btn-sm w-full text-center px-4 py-[7px]">
                          Login
                        </a>
                      </Link>
                    </li>
                  </ul>
                )}
              </li>

              <a
                className="opener md:hidden p-[12px] md:p-0"
                href="#"
                onClick={switchFunctions}
              >
                {" "}
                <span>
                  {" "}
                  <MenuAlt3Icon className="w-6 h-6" />
                </span>
              </a>
            </ul>
          </nav>
        </div>
      </header>
    </div>
  );
};
export default UserHeader;
