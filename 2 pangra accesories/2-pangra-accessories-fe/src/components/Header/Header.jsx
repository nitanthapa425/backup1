import { useState, useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { MenuAlt1Icon } from "@heroicons/react/outline";
import { adminActions } from "store/features/adminAuth/adminAuthSlice";
import { useToasts } from "react-toast-notifications";
import { useLogoutSuperAdminMutation } from "services/api/loginCheck";
import Notification from "components/Notification/Notification";

const Header = () => {
  const [scrollPosition, setScrollPosition] = useState(0);

  const handleScroll = () => {
    if (typeof window !== "undefined") {
      const position = window.pageYOffset;
      setScrollPosition(position);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", handleScroll);

      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    }
  }, []);

  const { addToast } = useToasts();

  const [LogoutSuperAdmin, { isError, isSuccess, dataLogoutSuperAdmin }] =
    useLogoutSuperAdminMutation();
  const router = useRouter();
  const dispatch = useDispatch();
  const loginInfo = useSelector((state) => state.adminAuth);

  const body = document.querySelector("body");

  const hFunctions = () => {
    body.classList.toggle("sidebar-toggle");
  };

  const handleLogout = () => {
    LogoutSuperAdmin();
  };

  useEffect(() => {
    if (isSuccess) {
      addToast(dataLogoutSuperAdmin?.message || "Logged Out successfully .", {
        appearance: "success",
      });
      dispatch(adminActions.removeAdminToken());
      router.replace("/admin");
    }
    if (isError) {
      dispatch(adminActions.removeAdminToken());
      router.replace("/admin");
    }
  }, [isSuccess, isError]);

  return (
    <hea
      id="header"
      className={`py-5 shadow-md bg-white mb-2 w-full z-50 md:hidden ${
        scrollPosition > 10 ? "fixed" : null
      }`}
    >
      <div className="container flex flex-wrap items-center justify-between">
        <Link href="#">
          <a className="opener" onClick={() => hFunctions()}>
            <MenuAlt1Icon className="db-sidebar-icon text-[#5a5959] w-6 mr-3" />
          </a>
        </Link>
        <div className="logo w-[110px]">
          <Link href="/admin/product/create">
            <a className="mb-0">
              <img src="/images/logo.svg" alt="2Pangra" />
            </a>
          </Link>
        </div>
        <nav id="nav">
          <ul className="flex flex-wrap items-center space-x-2 md:space-x-10 z-10">
            <Notification type="admin"></Notification>
            <li className="user-icon flex align-center relative has-dropdown">
              {loginInfo?.user?.profileImagePath?.imageUrl ? (
                <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                  <img
                    width="100%"
                    height="100%"
                    src={`${loginInfo?.user?.profileImagePath?.imageUrl}`}
                  ></img>
                </div>
              ) : (
                <a href="#">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-8 w-8"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="#231F20"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={1}
                      d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                    />
                  </svg>
                </a>
              )}

              <ul
                className="border dropdown !border-gray-200 py-2 rounded-md opacity-0 w-[180px] invisible !absolute top-full right-0 bg-white z-50 shadow-md mt-3
                transition-all "
              >
                <li>
                  {" "}
                  <Link href={`/admin/user/my-profile`}>
                    <a className="text-textColor hover:text-primary py-[4px] px-4">
                      My Profile
                    </a>
                  </Link>
                </li>

                <li>
                  <Link href="/password-update">
                    <a className="text-textColor hover:text-primary px-4 py-[7px]">
                      Update Password
                    </a>
                  </Link>
                </li>
                <li>
                  <span
                    className="text-textColor hover:text-primary cursor-pointer px-4 py-[7px]"
                    onClick={handleLogout}
                  >
                    Log Out
                  </span>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </hea>
  );
};

export default Header;
