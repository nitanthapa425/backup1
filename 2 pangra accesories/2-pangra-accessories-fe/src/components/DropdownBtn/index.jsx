import { ChevronDownIcon } from "@heroicons/react/solid";
function DropdownBtn({ name, options, selectedRowsIds }) {
  return (
    <div className="relative has-dropdown cursor-pointer ml-3">
      <div className="flex items-center text-gray-600 hover:text-primary">
        <span>{name}</span>
        <ChevronDownIcon className="h-5 w-5" />
      </div>
      <ul className="border dropdown border-gray-200 py-2 z-10 rounded-md opacity-0 w-40 invisible absolute top-full -left-4 bg-white shadow-md mt-3 transition-all text-sm">
        {options.map((o, i) => {
          return (
            <li
              className={
                o?.isLoading
                  ? "text-gray-400 py-1 px-4 pointer-events-none"
                  : "text-textColor py-1 px-4 hover:text-primary"
              }
              onClick={
                o?.hasModal
                  ? null
                  : () => {
                    selectedRowsIds.forEach((rowId) => {
                      o?.onClick(rowId);
                    });
                  }
              }
              key={i}
            >
              {o?.hasModal ? (
                <>{o?.renderModalComponent(selectedRowsIds)}</>
              ) : (
                <>{o?.isLoading ? o?.loadingText : o?.name}</>
              )}
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default DropdownBtn;
