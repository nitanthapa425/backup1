import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";

import { Form, Formik } from "formik";
import AdminLayout from "layouts/Admin";

import { useToasts } from "react-toast-notifications";
import {
  useReadMyProfileDetailsQuery,
  useReadUserProfileDetailsQuery,
  useSignUpPostMutation,
  useUpdateUserProfileDetailsMutation,
  useUpdateMyProfileDetailsMutation,
  useSignUpDeliveryMutation,
} from "services/api/signup";

import Input from "components/Input";
import Select from "../Select/index";

import Button from "components/Button";
import {
  SignUpValidationSchema,
  UpdateMyValidationSchema,
  UpdateSignUpValidationSchema,
} from "validation/signup.validation";

import Popup from "components/Popup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useRouter } from "next/router";
import DropZone from "components/DropZone";
import { beforeAfterDate } from "utils/beforeAfterdate";
import { accessLevels, genders } from "constant/constant";
import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useReadCompanyQuery } from "services/api/company";
const SignUpComponent = ({
  type,
  editUser,
  values,
  setFieldValue,
  errors,
  touched,
  setChanged,
  dirty,
  setFieldTouched,
}) => {
  const router = useRouter();
  const [userInitialValue] = useState({
    userName: "",
    accessLevel: "",
    firstName: "",
    middleName: "",
    lastName: "",
    email: "",
    mobile: "",
    dob: beforeAfterDate(new Date(), 0, 0, -16),
    gender: "",
    documentImagePath: [],
    companyId: "",
  });

  const [editUserInitialValue, setEditUserInitialValue] = useState({});
  const [editMyInitialValue, setEditMyInitialValue] = useState({});

  const formikBag = useRef();
  const { addToast } = useToasts();
  const [
    createSignUp,
    { isError, isSuccess, isLoading, data: dataCreateSignUp },
  ] = useSignUpPostMutation();
  const { data: companyData, isLoading: fetchingCompany } =
    useReadCompanyQuery();

  const [openModal, setOpenModal] = useState(false);

  const { data: userProfile } = useReadUserProfileDetailsQuery(
    router.query.id,
    {
      skip: type !== "edit" || !router.query.id,
    }
  );

  const { data: myProfile } = useReadMyProfileDetailsQuery();

  useEffect(() => {
    if (myProfile) {
      const newUserDetails = {
        firstName: myProfile?.firstName,
        middleName: myProfile?.middleName,
        lastName: myProfile?.lastName,
        mobile: myProfile?.mobile,
        dob: myProfile?.dob,
        gender: myProfile?.gender,
        profileImagePath: myProfile?.profileImagePath,
        documentImagePath: myProfile?.documentImagePath,
      };

      setEditMyInitialValue(newUserDetails);
    }
  }, [myProfile]);

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataCreateSignUp?.message ||
          "New account created successfully. Verification link has been sent to your email account..",
        {
          appearance: "success",
        }
      );
    }
  }, [isSuccess]);
  useEffect(() => {
    if (isError) {
      addToast("User/Email already exists.", {
        appearance: "error",
      });
    }
  }, [isError]);

  const [
    updateSignUp,
    {
      isLoading: updating,
      isError: isUpdateError,
      isSuccess: isUpdateSuccess,
      data: dataUpdateSignUp,
    },
  ] = useUpdateUserProfileDetailsMutation();

  const [
    updateMyProfile,
    {
      isLoading: updatingMyProfile,
      isError: isUpdateErrorMyProfile,
      isSuccess: isUpdateSuccessMyProfile,
      data: dataUpdateMyProfile,
    },
  ] = useUpdateMyProfileDetailsMutation();

  useEffect(() => {
    if (isUpdateError) {
      addToast("User/Email already exists.", {
        appearance: "error",
      });
    }
  }, [isUpdateError]);

  useEffect(() => {
    if (isUpdateSuccess) {
      formikBag.current?.resetForm();
      addToast(dataUpdateSignUp?.message || "User updated successfully.", {
        appearance: "success",
      });
      router.push(`/admin/user/${router.query.id}`);
    }
  }, [isUpdateSuccess]);
  useEffect(() => {
    if (isUpdateErrorMyProfile) {
      addToast("User/Email already exists.", {
        appearance: "error",
      });
    }
  }, [isUpdateErrorMyProfile]);

  useEffect(() => {
    if (isUpdateSuccessMyProfile) {
      formikBag.current?.resetForm();
      addToast(dataUpdateMyProfile?.message || "User updated successfully.", {
        appearance: "success",
      });
      router.push(`/admin/user/my-profile`);
    }
  }, [isUpdateSuccessMyProfile]);

  useEffect(() => {
    if (userProfile) {
      const newUserDetails = {
        accessLevel: userProfile?.accessLevel,
        firstName: userProfile?.firstName,
        middleName: userProfile?.middleName,
        lastName: userProfile?.lastName,
        mobile: userProfile?.mobile,
        dob: userProfile?.dob,
        gender: userProfile?.gender,
        profileImagePath: userProfile?.profileImagePath,
        documentImagePath: userProfile?.documentImagePath,
      };

      setEditUserInitialValue(newUserDetails);
    }
  }, [userProfile]);
  const [
    createDeliverySignUp,
    {
      isError: isErrorDelivery,
      isSuccess: isSuccessDelivery,
      data: dataCreateDeliverySignUp,
    },
  ] = useSignUpDeliveryMutation();
  useEffect(() => {
    if (isSuccessDelivery) {
      formikBag.current?.resetForm();

      addToast(
        dataCreateDeliverySignUp?.message ||
          "New account created successfully. Verification link has been sent to your email account.",
        {
          appearance: "success",
        }
      );
    }
  }, [isSuccessDelivery]);
  useEffect(() => {
    if (isErrorDelivery) {
      addToast("User/Email already exists.", {
        appearance: "error",
      });
    }
  }, [isErrorDelivery]);

  const handleProfileImagePath = (newFiles) => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", newFiles[0]);
    }
  };

  const handleRemoveProfileImagePath = () => {
    if (formikBag?.current) {
      formikBag?.current?.setFieldValue("profileImagePath", {});
    }
  };
  const setNewFiles = (newFiles) => {
    const allFiles = [
      ...formikBag?.current?.values.documentImagePath,
      ...newFiles,
    ];
    formikBag?.current?.setFieldValue("documentImagePath", allFiles);
  };

  const handleRemoveFile = (idx) => {
    const currentFiles = [...formikBag?.current?.values.documentImagePath];
    currentFiles.splice(idx, 1);
    formikBag?.current?.setFieldValue("documentImagePath", currentFiles);
  };

  const updateUser = (payloadData, id) => {
    updateSignUp({ ...payloadData, id: router.query.id });
  };

  const BreadCrumbList = [
    {
      routeName: "Add Product",
      route: "/admin/product/create",
    },

    {
      routeName: "Add User",
      route: "",
    },
  ];

  return (
    <AdminLayout
      documentTitle={type === "add" ? "Sign Up" : "Edit user"}
      BreadCrumbList={BreadCrumbList}
    >
      <Formik
        initialValues={
          type === "add"
            ? userInitialValue
            : editUser === "userProfile"
            ? editUserInitialValue
            : editMyInitialValue
        }
        onSubmit={(values, { resetForm, setSubmitting }) => {
          let payloadData = {};

          if (type === "add" && values.accessLevel === "DELIVERY") {
            createDeliverySignUp(values);
          } else if (type === "add") {
            payloadData = {
              userName: values.userName,
              accessLevel: values.accessLevel,
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              email: values.email,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
            };
            createSignUp(payloadData);
          } else if (editUser === "userProfile") {
            payloadData = {
              accessLevel: values.accessLevel,
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
              profileImagePath: values?.profileImagePath,
            };

            updateUser(payloadData, router.query.id);
          } else {
            payloadData = {
              firstName: values.firstName,
              lastName: values.lastName,
              middleName: values.middleName,
              mobile: values.mobile,
              dob: values.dob,
              gender: values.gender,
              profileImagePath: values?.profileImagePath,
            };

            updateMyProfile(payloadData);
          }
          setSubmitting(false);
        }}
        validationSchema={
          type === "add"
            ? SignUpValidationSchema
            : editUser === "userProfile"
            ? UpdateSignUpValidationSchema
            : UpdateMyValidationSchema
        }
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          setFieldTouched,
          dirty,
        }) => (
          <div className="container">
            <h3 className="mb-3">
              {type === "add" ? "Add User" : "Edit User"}
            </h3>

            <Form>
              <div className="row-sm  pt-5 pb-2">
                {type === "add" && (
                  <div className="three-col-sm">
                    <Input
                      label="Username"
                      name="userName"
                      type="text"
                      placeholder="E.g: john1"
                    />
                  </div>
                )}
                <div className="three-col-sm">
                  <Input
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder="E.g: John"
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Middle Name"
                    name="middleName"
                    type="text"
                    placeholder="E.g: Jung"
                    required={false}
                  />
                </div>
                <div className="three-col-sm">
                  <Input
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder="E.g: Deo"
                  />
                </div>
                {editUser !== "myProfile" && (
                  <div className="three-col-sm">
                    <Select label="Access Level" name="accessLevel">
                      <option value="">Select Access Level</option>

                      {accessLevels.map((accessLevel, i) => (
                        <option key={i} value={accessLevel}>
                          {accessLevel}
                        </option>
                      ))}
                    </Select>
                  </div>
                )}
                {type === "add" && (
                  <div className="three-col-sm">
                    <Input
                      label="Email"
                      name="email"
                      type="email"
                      placeholder="E.g: john320@gmail.com"
                    />
                  </div>
                )}

                <div className="three-col-sm">
                  <Input
                    label="Phone Number"
                    name="mobile"
                    type="text"
                    placeholder="E.g: 98XXXXXXXX"
                  />
                </div>
                <div className="three-col-sm">
                  <label htmlFor="">
                    Date Of Birth(AD)
                    <span className="required">*</span>
                  </label>
                  <DatePicker
                    selected={values?.dob ? new Date(values?.dob) : null}
                    onChange={(date) => {
                      setFieldValue("dob", date?.toLocaleDateString());
                      setFieldTouched("dob");
                    }}
                    onBlur={() => {
                      setFieldTouched("dob");
                    }}
                    dateFormat="MM/dd/yyyy"
                    placeholderText="mm/dd/yyyy"
                    maxDate={beforeAfterDate(new Date(), 0, 0, -16)}
                  />
                  <h6 className="text-gray-500 text-xs font-thin">
                    age must be greater than 16
                  </h6>

                  {touched.dob && errors.dob && (
                    <div className="text-error">{errors.dob}</div>
                  )}
                </div>
                <div className="three-col-sm">
                  <Select label="Gender" name="gender">
                    <option value="">Select Gender</option>

                    {genders.map((gender, i) => (
                      <option key={i} value={gender}>
                        {gender}
                      </option>
                    ))}
                  </Select>
                </div>

                <div className="three-col-sm">
                  {type === "edit" && (
                    <div className="">
                      <div>
                        <DropZone
                          label="Upload Profile Image"
                          currentFiles={
                            Object.keys(values.profileImagePath || {}).length
                              ? [values.profileImagePath]
                              : []
                          }
                          setNewFiles={handleProfileImagePath}
                          handleRemoveFile={handleRemoveProfileImagePath}
                          error={
                            touched?.profileImagePath
                              ? errors?.profileImagePath
                              : ""
                          }
                        />
                      </div>
                    </div>
                  )}
                </div>
                <div className="three-col-sm">
                  {values.accessLevel === "DELIVERY" && (
                    <DropZone
                      label="Document/s Image"
                      required
                      currentFiles={values.documentImagePath}
                      setNewFiles={setNewFiles}
                      handleRemoveFile={handleRemoveFile}
                      error={
                        touched?.documentImagePath
                          ? errors?.documentImagePath
                          : ""
                      }
                    />
                  )}
                </div>
                <div className="three-col-sm">
                  {values.accessLevel === "DELIVERY" && (
                    <SelectWithoutCreate
                      required
                      loading={fetchingCompany}
                      label="Select Company"
                      placeholder="E.g: Tuki Logic"
                      error={touched?.companyId ? errors?.companyId : ""}
                      value={values.companyId}
                      onChange={(selectedCompany) => {
                        setFieldTouched("companyId");
                        setFieldValue("companyId", selectedCompany.value);
                      }}
                      options={companyData?.docs?.map((d) => ({
                        value: d?._id,
                        label: d?.companyName,
                      }))}
                    />
                  )}
                </div>
              </div>

              <div className="btn-holder mt-2">
                <Button
                  type="submit"
                  loading={isLoading || updating || updatingMyProfile}
                  disabled={!dirty}
                >
                  {type === "add" ? "Create" : "Update"}
                </Button>
                <Button
                  variant="outlined-error"
                  type="button"
                  onClick={() => {
                    setOpenModal(true);
                  }}
                  disabled={!dirty}
                >
                  Clear
                </Button>
                {openModal && (
                  <Popup
                    title="Do you want to clear all fields?"
                    description="if you clear all the filed will be removed"
                    onOkClick={() => {
                      resetForm();
                      setOpenModal(false);
                    }}
                    onCancelClick={() => setOpenModal(false)}
                    okText="Clear All"
                    cancelText="Cancel"
                  />
                )}
              </div>
            </Form>
          </div>
        )}
      </Formik>
    </AdminLayout>
  );
};

SignUpComponent.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default SignUpComponent;
