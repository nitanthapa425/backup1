import React from "react";
import { useSelector } from "react-redux";
// import AddVehiclePage from 'container/Vehicle/Add';
// import { useRouter } from 'next/router';

const PrivateRoutes = ({ showFor, children }) => {
  // const router = useRouter();
  let userData = {};
  if (showFor[0] === "SUPER_ADMIN" || showFor[0] === "ADMIN") {
    const { user } = useSelector((state) => {
      return state.adminAuth;
    });
    userData = user;
  } else if(showFor[0] === 'DELIVERY') {
    const { delivery } = useSelector((state) => {
      return state.deliveryAuth;
    });
    userData = delivery;
  }

  if (
    userData &&
    userData.accessLevel &&
    showFor.includes(userData.accessLevel)
  ) {
    return <div>{children}</div>;
  } else {
    return <div>You can not access this page</div>;
  }
};

export default PrivateRoutes;
