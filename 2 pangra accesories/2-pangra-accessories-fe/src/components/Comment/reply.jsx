import React, { useRef, useEffect } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";
import { replyMessageSchema } from "validation/resetLink.validation";
import Textarea from "components/Input/textarea";
import { useReplyCommentMutation } from "services/api/seller";
const ReplyComment = (props) => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [
    replyComment,
    {
      isError,
      error,
      isSuccess,
      isLoading: isLoadingComment,
      data: dataReplyComment,
    },
  ] = useReplyCommentMutation();

  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataReplyComment?.message || "You have successfully replied a comment.",
        {
          appearance: "success",
        }
      );
      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to reply your comment . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <>
      <h3 className="h5 mb-2">Reply Comment</h3>
      <Formik
        initialValues={{
          replyMessage: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          replyComment({ ...values, id: props.id });
          setSubmitting(false);
        }}
        validationSchema={replyMessageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="comment-box">
              <Textarea
                required={false}
                name="replyMessage"
                type="text"
                placeholder="E.g. we will contact you"
              />
            </div>
            <div className=" btn-holder mt-3">
              <Button
                type="submit"
                disabled={isSubmitting || !dirty}
                loading={isLoadingComment}
              >
                Reply
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default ReplyComment;
