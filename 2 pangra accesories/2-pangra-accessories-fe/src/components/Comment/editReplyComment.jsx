import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import { useToasts } from "react-toast-notifications";

import { messageSchema } from "validation/resetLink.validation";

import Textarea from "components/Input/textarea";
import {
  useEditReplyCommentMutation,
  useGetSingleReplyOfCommentQuery,
} from "services/api/seller";

const EditReplyComment = (props) => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [EditReplyCommentDetails, setEditReplyCommentDetails] = useState({});

  const [
    updateComment,
    { isError, error, isSuccess, isLoading, data: dataUpdateComment },
  ] = useEditReplyCommentMutation();

  const { data: singleCommentData } = useGetSingleReplyOfCommentQuery({
    commentId: props.commentId,
    replyId: props.replyId,
  });

  useEffect(() => {
    if (singleCommentData) {
      const newCommentDetails = {
        replyMessage: singleCommentData.replyDetails[0].replyMessage,
      };

      setEditReplyCommentDetails(newCommentDetails);
    }
  }, [singleCommentData]);
  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataUpdateComment?.message || "You have successfully edited a comment.",
        {
          appearance: "success",
        }
      );

      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
    if (isError) {
      addToast(
        error?.data?.replyMessage ||
          "We are not able to edit your comment . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <>
      <h3 className="h5 mb-2">Update Reply Comment </h3>
      <Formik
        initialValues={EditReplyCommentDetails}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          updateComment({
            commentId: props.commentId,
            replyId: props.replyId,
            body: values,
          });
          setSubmitting(false);
        }}
        validationSchema={messageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div>
              <div>
                <Textarea
                  required={false}
                  name="replyMessage"
                  type="text"
                  placeholder="E.g. What is the price of bike?"
                />
              </div>

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Update
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EditReplyComment;
