import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";

import Button from "components/Button";

import { useToasts } from "react-toast-notifications";
import { messageSchema } from "validation/resetLink.validation";

import Textarea from "components/Input/textarea";
import {
  useGetSingleCommentQuery,
  useUpdateCommentMutation,
} from "services/api/seller";

const EditComment = (props) => {
  const formikBag = useRef();
  const { addToast } = useToasts();
  const [editCommentDetails, setEditCommentDetails] = useState({});

  const [
    updateComment,
    { isError, error, isSuccess, isLoading, dataUpdateComment },
  ] = useUpdateCommentMutation();

  const { data: singleCommentData } = useGetSingleCommentQuery(props.id);

  useEffect(() => {
    if (singleCommentData) {
      const newCommentDetails = {
        message: singleCommentData?.message,
      };

      setEditCommentDetails(newCommentDetails);
    }
  }, [singleCommentData]);
  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();

      addToast(
        dataUpdateComment?.message || "You have successfully edited a comment.",
        {
          appearance: "success",
        }
      );

      if (props.modal === true) {
        props.setModalFunction(false);
      }
      if (props.modal === false) {
        props.setModalFunction(true);
      }
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to edit your comment . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);

  return (
    <>
      <h3 className="h5 mb-2">Update Comment </h3>
      <Formik
        initialValues={editCommentDetails}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          updateComment({ ...values, id: props.id });
          setSubmitting(false);
        }}
        validationSchema={messageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div>
              <div>
                <Textarea
                  required={false}
                  name="message"
                  type="text"
                  placeholder="E.g. What is the price of bike?"
                />
              </div>

              <div className="btn-holder mt-3">
                <Button
                  type="submit"
                  disabled={isSubmitting || !dirty}
                  loading={isLoading}
                >
                  Update
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default EditComment;
