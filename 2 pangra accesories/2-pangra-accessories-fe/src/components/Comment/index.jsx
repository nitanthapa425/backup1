import React, { useRef, useEffect, useState } from "react";
import { Form, Formik } from "formik";
import Button from "components/Button";
import { useToasts } from "react-toast-notifications";

import { messageSchema } from "validation/resetLink.validation";

import { useRouter } from "next/router";
import { agodate } from "utils/datedifference";
import Popup from "components/Popup";
import { DotsVerticalIcon } from "@heroicons/react/outline";
import ReactModal from "components/ReactModal/ReactModal";
import EditComment from "./editComment";
import {
  useCommentMutation,
  useDeleteCommentMutation,
  useGetCommentQuery,
} from "services/api/seller";
import { useSelector } from "react-redux";
import ReplyComment from "./reply";
import GetReplyComment from "./GetReplyComment";
import TextareaAutosize from "react-textarea-autosize";
import Modal from "react-modal";
import CustomerLoginPopUp from "components/LoginPopUp";
const Comment = (props) => {
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const setModalFunction = (value) => {
    setModal(value);
  };

  const formikBag = useRef();

  const { addToast } = useToasts();
  const [openModel, setOpenModel] = useState(false);

  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo.token;
  const [
    createComment,
    {
      isError,
      error,
      isSuccess,
      isLoading: isLoadingComment,
      data: dataCreateComment,
    },
  ] = useCommentMutation();
  const [editDeleteCommentId, setEditDeleteCommentId] = useState("");
  const { data: commentData } = useGetCommentQuery(router.query.id, {
    skip: !router.query.id,
  });

  const [
    deleteComment,
    {
      isLoading: isLoadingDeleteComment,
      isSuccess: isDeleteSuccess,
      isError: isDeleteError,
      error: DeletedError,
      data: dataDeleteComment,
    },
  ] = useDeleteCommentMutation();
  useEffect(() => {
    if (isSuccess) {
      formikBag.current?.resetForm();
      addToast(
        dataCreateComment?.message || "You have successfully posted a comment.",
        {
          appearance: "success",
        }
      );
    }
    if (isError) {
      addToast(
        error?.data?.message ||
          "We are not able to post your comment . Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [isSuccess, isError]);
  useEffect(() => {
    if (isDeleteSuccess) {
      formikBag.current?.resetForm();
      addToast(
        dataDeleteComment?.message ||
          "You have successfully deleted a comment.",
        {
          appearance: "success",
        }
      );
      setOpenModel(false);
    }
    if (isDeleteError) {
      addToast(
        DeletedError?.data?.message ||
          "We are not able to delete your comment . Please try again later.",
        {
          appearance: "error",
          autoDismiss: false,
        }
      );
    }
  }, [isDeleteSuccess, isDeleteError]);
  const [modalIsOpen, setIsOpen] = useState(false);
  const closeModal = () => {
    setIsOpen(false);
  };
  return (
    <div className=" p-5 mb-4">
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="mymodal"
        overlayClassName="myoverlay"
      >
        <button
          type="button"
          onClick={closeModal}
          className="text-error absolute top-[5px] right-[5px]"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>

        <CustomerLoginPopUp setIsOpen={setIsOpen} />
      </Modal>
      <h3 className="h5 mb-2">Comments</h3>
      <Formik
        initialValues={{
          message: "",
        }}
        onSubmit={(values, { resetForm, setSubmitting }) => {
          createComment({ ...values, id: router.query.id });
          setSubmitting(false);
        }}
        validationSchema={messageSchema}
        enableReinitialize
        innerRef={formikBag}
      >
        {({
          setFieldValue,
          values,
          errors,
          touched,
          resetForm,
          isSubmitting,
          dirty,
        }) => (
          <Form>
            <div className="mb-3">
              {openModel && (
                <Popup
                  title="Are you sure you want to delete comment?"
                  description="Please note that, once deleted, this cannot be undone."
                  onOkClick={() => {
                    deleteComment(editDeleteCommentId);
                  }}
                  onCancelClick={() => setOpenModel(false)}
                  okText="Yes"
                  cancelText="No"
                  loading={isLoadingDeleteComment}
                />
              )}

              <div className="comment-box">
                <TextareaAutosize
                  maxRows={6}
                  required={false}
                  name="message"
                  type="text"
                  value={values.message}
                  placeholder="E.g. What is the price of riding jacket?"
                  onChange={(e) => {
                    setFieldValue("message", e.target.value);
                    if (!token) {
                      setFieldValue("message", "");
                      setIsOpen(true);
                    }
                  }}
                  onClick={() => {
                    if (!token) {
                      setFieldValue("message", "");
                      setIsOpen(true);
                    }
                  }}
                />
              </div>
              {token && values.message ? (
                <div className="btn-holder btn-comment-holder mt-3">
                  <Button
                    type="submit"
                    disabled={isSubmitting || !dirty}
                    loading={isLoadingComment}
                  >
                    Comment
                  </Button>
                  <Button
                    type="button"
                    variant="outlined"
                    disabled={isSubmitting || !dirty}
                    onClick={() => {
                      resetForm();
                    }}
                  >
                    Clear
                  </Button>
                </div>
              ) : null}
            </div>
            <div className="max-h-[300px] overflow-y-auto">
              {commentData?.map((res, i) => {
                // eslint-disable-next-line react/jsx-key
                return (
                  // eslint-disable-next-line react/jsx-key

                  <div key={i} className="relative py-2">
                    <div>
                      {loginInfo?.customer?._id === res?.clientId?.id ? (
                        <div className="absolute right-0 top-[13px] group hover:cursor-pointer">
                          <DotsVerticalIcon className="h-4" />
                          <div className="px-4 py-3 rounded-lg bg-white border-[1px] border-gray-200 shadow-lg w-[110px] absolute top-5 right-0 opacity-0 z-10 invisible group-hover:opacity-100 group-hover:visible transition-all">
                            <div
                              className="mb-1 hover:text-primary-dark"
                              onClick={() => {
                                setEditDeleteCommentId(res._id);
                                setOpenModel(true);
                              }}
                            >
                              <span>Delete</span>
                            </div>

                            <ReactModal
                              link="Edit"
                              className="comment-edit p-0"
                              modal={modal}
                            >
                              <EditComment
                                id={res._id}
                                setModalFunction={setModalFunction}
                                modal={modal}
                              ></EditComment>
                            </ReactModal>
                          </div>
                        </div>
                      ) : null}

                      <div className="flex">
                        <div className="profile-image-holder w-[60px]">
                          {res?.clientId?.profileImagePath?.imageUrl ? (
                            <div className="h-8 w-8 rounded-full inline-block overflow-hidden border-2 border-gray-100">
                              <img
                                width="100%"
                                height="100%"
                                src={`${res?.clientId?.profileImagePath?.imageUrl}`}
                              ></img>
                            </div>
                          ) : (
                            <a>
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                className="h-8 w-7"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="#231F20"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth={1}
                                  d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                                />
                              </svg>
                            </a>
                          )}
                        </div>
                        <div className="flex-1">
                          <h6>{res?.name ? res?.name : "Not available"} </h6>
                          <p className="mb-1">
                            {res?.message ? res?.message : "Not available"}
                          </p>
                          <div className="flex space-x-3 items-center">
                            <span className="text-[12px] text-gray-500 font-normal inline-block   relative">
                              {agodate(new Date(res.createdAt))}
                            </span>

                            {token && (
                              <ReactModal
                                link="reply"
                                className="comment-edit p-0"
                                modal={modal}
                              >
                                <ReplyComment
                                  id={res._id}
                                  setModalFunction={setModalFunction}
                                  modal={modal}
                                ></ReplyComment>
                              </ReactModal>
                            )}
                          </div>
                        </div>
                      </div>
                      <div>
                        <div className="flex pl-4">
                          <div className="flex-1">
                            <div>
                              <GetReplyComment
                                commentId={res._id}
                              ></GetReplyComment>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
export default Comment;
