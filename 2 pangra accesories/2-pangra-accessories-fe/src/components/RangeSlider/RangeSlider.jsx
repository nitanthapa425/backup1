import React from "react";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";

const { Range } = Slider;

const RangeSlider = ({ min, max, value, onChange, postFix, sliderName }) => {
  let PostFix = "";
  if (postFix) {
    PostFix = postFix;
  }
  return (
    <div
      style={{
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: "30px",
        marginTop: "-8px",
        marginLeft: "10px",
        marginRight: "25px",
      }}
    >
      <div style={{ width: "100%", textAlign: "center", fontSize: "14px" }}>
        {sliderName} {value[0]}-{value[1]}
      </div>
      <div style={{ width: "100%" }}>
        <Range
          min={min}
          max={max}
          value={value}
          marks={{
            [min]: `${min}${PostFix}`,
            [max]: `${max}${PostFix}`,
          }}
          tipFormatter={(value) => `${value}${PostFix}`}
          tipProps={{
            placement: "top",
            visible: true,
          }}
          onChange={onChange}
        />
      </div>
    </div>
  );
};

export default RangeSlider;
