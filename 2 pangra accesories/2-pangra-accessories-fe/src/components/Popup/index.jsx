import Button from "components/Button";

const Popup = ({
  title,
  description = "",
  onOkClick,
  onCancelClick,
  okText = "Ok",
  cancelText = "Cancel",
  loading = false,
}) => {
  return (
    <>
      <div className="modalBackground ">
        <div className="modal Container relative ">
          <button
            type="button"
            onClick={onCancelClick}
            className="absolute top-[5px] right-[5px] hover:text-error "
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fillRule="evenodd"
                d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                clipRule="evenodd"
              />
            </svg>
          </button>
          <p className="text-xl text-grey-800 -mb-1 font-semibold pr-[20px]">
            {title}
          </p>
          <p className="text-sm">{description}</p>
          <div className="absolute bottom-3 right-3 flex gap-2">
            <Button
              type="button"
              size="sm"
              onClick={onOkClick}
              loading={loading}
            >
              {okText}
            </Button>

            <Button
              type="button"
              size="sm"
              onClick={onCancelClick}
              variant="secondary"
            >
              {cancelText}
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Popup;
