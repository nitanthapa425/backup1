import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";

import { Form, Formik } from "formik";

import { useToasts } from "react-toast-notifications";

import Input from "components/Input";

import Button from "components/Button";

import Popup from "components/Popup";

import "react-datepicker/dist/react-datepicker.css";

import { useAddToBookMutation } from "services/api/seller";

import SelectWithoutCreate from "components/Select/SelectWithoutCreate";
import { useRouter } from "next/router";

import { useGetAllLocationQuery } from "services/api/location";

import { LocationValidationSchema } from "validation/location";
import { useWarnIfUnsavedChanges } from "hooks/useWarnIfUnsavedChanges";
import { useGetDetailLocationQuery } from "services/api/getFullLocation";

const Address = ({ bookList }) => {
  const router = useRouter();
  const formikBag = useRef();

  const [changed, setChanged] = useState(false);
  useWarnIfUnsavedChanges(changed);

  const { addToast } = useToasts();

  const [openModal, setOpenModal] = useState(false);
  const [editLocationInitialValue, setEditLocationInitialValue] = useState({});

  const { data: combineLocations, isFetching: isFetchingCombineLocation } =
    useGetDetailLocationQuery();

  const { data: locationDetails, error: locationFetchError } =
    useGetAllLocationQuery();

  useEffect(() => {
    if (locationFetchError) {
      addToast(
        "Error occured while booking the vehicle. Please try again later.",
        {
          appearance: "error",
        }
      );
    }
  }, [locationFetchError]);

  useEffect(() => {
    if (locationDetails) {
      const loc = locationDetails?.filter((res) => res.label === "billing");
      const newLocationDetails = {
        combineLocation: loc[loc.length - 1]?.combineLocation || "",
        nearByLocation: loc[loc.length - 1]?.nearByLocation || "",

        label: loc[0]?.label || "billing",
      };

      setEditLocationInitialValue(newLocationDetails);
    }
  }, [locationDetails]);

  const [
    addToBook,
    {
      isLoading: isLoadingAddToBook,
      isError: isErrorAddToBook,
      isSuccess: isSuccessAddToBook,
      error: addToBookError,
      data: dataAddToBook,
    },
  ] = useAddToBookMutation();

  useEffect(() => {
    if (isSuccessAddToBook) {
      addToast(
        dataAddToBook?.message ||
          "Congratulation, You have booked the vehicle! ",
        {
          appearance: "success",
        }
      );
      router.push("/book");
    }
  }, [isSuccessAddToBook]);
  useEffect(() => {
    if (isErrorAddToBook) {
      addToast(addToBookError?.data?.message, {
        appearance: "error",
      });
      if (addToBookError?.data?.message === "This item is already booked.") {
        router.push("/book");
      }
    }
  }, [isErrorAddToBook, addToBookError]);

  const onSubmit = (values, { resetForm, setSubmitting }) => {
    bookList.forEach((id, i) => {
      addToBook({ id, values });
    });

    setSubmitting(false);
  };

  return (
    <Formik
      initialValues={editLocationInitialValue}
      onSubmit={onSubmit}
      validationSchema={LocationValidationSchema}
      enableReinitialize
      innerRef={formikBag}
    >
      {({
        setFieldValue,
        values,
        errors,
        touched,
        resetForm,
        setFieldTouched,
        isSubmitting,
        dirty,
      }) => (
        <div className="container mt-4">
          <Form>
            <h3 className="h3"> Enter your Location</h3>
            <div className="row-sm  pt-5 pb-2">
              <div className="w-full mb-5">
                <SelectWithoutCreate
                  required
                  loading={isFetchingCombineLocation}
                  label="Select Location"
                  placeholder="Location"
                  error={
                    touched?.combineLocation ? errors?.combineLocation : ""
                  }
                  value={values?.combineLocation || null}
                  onChange={(selectedValue) => {
                    setFieldValue("combineLocation", selectedValue.value);
                  }}
                  options={combineLocations?.map((value) => ({
                    label: value.fullAddress,
                    value: value.fullAddress,
                  }))}
                  onBlur={() => {
                    setFieldTouched("combineLocation");
                  }}
                />
              </div>

              <div className="w-full mb-3">
                <Input
                  required={true}
                  name="nearByLocation"
                  label="Nearby Location"
                  type="text"
                />
              </div>
            </div>
            <div className="c-fixed  btn-holder">
              <Button type="submit" loading={isLoadingAddToBook}>
                Confirm
              </Button>
              <Button
                variant="outlined-error"
                type="button"
                onClick={() => {
                  setOpenModal(true);
                }}
                disabled={isSubmitting || !dirty}
              >
                Clear
              </Button>
              {openModal && (
                <Popup
                  title="Do you want to clear all fields?"
                  description="if you clear all the filed will be removed"
                  onOkClick={() => {
                    resetForm();
                    setChanged(false);
                    setOpenModal(false);
                  }}
                  onCancelClick={() => setOpenModal(false)}
                  okText="Clear All"
                  cancelText="Cancel"
                />
              )}
            </div>
          </Form>
        </div>
      )}
    </Formik>
    // </UserLayout>
  );
};

Address.prototype = {
  type: PropTypes.oneOf(["edit", "add"]),
};

export default Address;
