import Link from "next/link";
import Image from "next/image";
import { HeartIcon } from "@heroicons/react/outline";
import {
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookIcon,
} from "react-share";
import {
  useAddToWishListProductMutation,
  useDeleteMyWishListMutation,
} from "services/api/seller";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";
import ReactModal from "components/ReactModal/ReactModal";
import CustomerLoginPopUp from "components/LoginPopUp";
import { useToasts } from "react-toast-notifications";
import ActualPrice from "container/Customer/ProductListingCom/ActualPrice";

function Card({
  imgPath,
  productTitle,
  costPrice,
  discountedPrice,
  discountPercentage,
  linkPath,
  index,
  id,
  isSold = false,
  isVerified,
  isInWishList = false,
  quantity,
  isFlashShaleList = false,
}) {
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);
  const shareUrl = " https://duipangra-fd-uat.herokuapp.com/";
  const setModalFunction = (value) => {
    setModal(value);
  };
  const handleWishList = (query, token) => {
    if (token) {
      addToWishList(query);
    } else {
      setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };
  const [
    addToWishList,
    {
      isLoading: isLoadingAddToWishList,
      isSuccess: isSuccessAddToWishList,
      isError: isErrorAddToWishList,
      error: addToWishListError,
      data: dataAddToWishList,
    },
  ] = useAddToWishListProductMutation();

  const [
    deleteWishList,
    {
      isLoading: isLoadingDeleteWishList,
      isSuccess: isSuccessRemoveWishList,
      isError: isErrorRemoveWishList,
      error: removeFromWishListError,
      data: dataDeleteWishList,
    },
  ] = useDeleteMyWishListMutation();

  useEffect(() => {
    if (isSuccessAddToWishList) {
      addToast(
        dataAddToWishList?.message ||
          "Product added to Wish-list successfully.",
        {
          appearance: "success",
        }
      );
      // router.push("/cart");
    }

    if (isErrorAddToWishList) {
      addToast(addToWishListError?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAddToWishList, isErrorAddToWishList, addToWishListError]);

  useEffect(() => {
    if (isSuccessRemoveWishList) {
      addToast(
        dataDeleteWishList?.message ||
          "Product is successfully removed from Wish-list .",
        {
          appearance: "success",
        }
      );
      // router.push("/cart");
    }

    if (isErrorRemoveWishList) {
      addToast(removeFromWishListError?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessRemoveWishList, isErrorRemoveWishList, removeFromWishListError]);

  return (
    <div className="c-card-holder px-2" data-value={++index} key={index}>
      {openModal ? (
        <ReactModal
          directlyOpen={true}
          closeFunc={handleCloseModal}
          modal={modal}
        >
          <CustomerLoginPopUp
            setModalFunction={setModalFunction}
            modal={modal}
          ></CustomerLoginPopUp>
        </ReactModal>
      ) : null}
      <div className="c-card-sm  ">
        <figure className="image-holder rounded-xl overflow-hidden group">
          <div className="absolute z-20 top-6 space-y-2 right-6 w-[30px] transition-all opacity-0 group-hover:opacity-100 ">
            {!isFlashShaleList && (
              <div>
                {isInWishList ? (
                  <Tooltip
                    overlay="Click to remove from wish list"
                    placement="right"
                  >
                    <HeartIcon
                      onClick={() => {
                        deleteWishList(id, token);
                      }}
                      loading={isLoadingDeleteWishList}
                      className="w-6 h-6 text-red-600 heart-icon-filled"
                    />
                  </Tooltip>
                ) : (
                  <Tooltip
                    overlay="Click to add in wish list"
                    placement="right"
                  >
                    <HeartIcon
                      onClick={() => {
                        handleWishList(id, token);
                      }}
                      loading={isLoadingAddToWishList}
                      className="w-6 h-6 text-white"
                    />
                  </Tooltip>
                )}
              </div>
            )}

            <Tooltip overlay="Click to share" placement="right">
              <FacebookShareButton
                url={shareUrl}
                quote={"2Pangra"}
                hashtag={"#2Pangra"}
              >
                <FacebookIcon size={25} round={true} />
              </FacebookShareButton>
            </Tooltip>
            <Tooltip overlay="Click to share" placement="right">
              <WhatsappShareButton
                url={shareUrl}
                quote={"2Pangra"}
                hashtag={"#2Pangra"}
              >
                <WhatsappIcon size={25} round={true} />
              </WhatsappShareButton>
            </Tooltip>
          </div>

          <div className="absolute z-20 top-1 space-y-2 left-2 whitespace-nowrap ">
            {Number(discountPercentage) ? (
              <div className="absolute z-20 top-1 space-y-2 left-2  ">
                {`${Number(discountPercentage).toFixed()}` === `0` ? (
                  <span className="badge-orange inline-block ">
                    {Number(discountPercentage)?.toFixed(2)}% OFF
                  </span>
                ) : (
                  <span className="badge-orange inline-block ">
                    {Number(discountPercentage)?.toFixed(0)}% OFF
                  </span>
                )}
              </div>
            ) : null}
          </div>

          <Link href={linkPath}>
            <a className="relative w-[200px] h-[200px] rounded-xl overflow-hidden before:transition-all before:absolute before:z-10 before:inset-0 before:bg-textColor before:bg-opacity-0 group-hover:before:bg-opacity-30">
              {imgPath?.substring(0, 4) === "http" ? (
                <Image src={imgPath} alt="Img Desc" layout="fill" />
              ) : (
                <img src={imgPath} alt="image description" />
              )}
              {isSold ? (
                <div className="absolute flex justify-center items-center inset-0 bg-black bg-opacity-60 text-white font-semibold text-lg z-50 ">
                  <span>{isSold ? "SOLD OUT" : ""}</span>
                </div>
              ) : (
                ""
              )}{" "}
            </a>
          </Link>
        </figure>
        <div className="detail-holder">
          <Link href={linkPath}>
            <a className="text-textColor hover:text-primary-dark inline-block !w-auto">
              <h5 className="inline-block">{productTitle}</h5>
            </a>
          </Link>

          {/* <CostDiscountCalc
            discountPercentage={discountPercentage}
            costPrice={costPrice}
            discountedPrice={discountedPrice}
          ></CostDiscountCalc> */}

          <ActualPrice
            discountedAmount={discountedPrice}
            costPrice={costPrice}
            cardQuantity={1}
          />
        </div>
      </div>
    </div>
  );
}

export default Card;
