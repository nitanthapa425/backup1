import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { PhotographIcon, HeartIcon } from "@heroicons/react/outline";
import {
  useAddToWishListProductMutation,
  useDeleteMyWishListMutation,
} from "services/api/seller";
import { useSelector } from "react-redux";
import ReactModal from "components/ReactModal/ReactModal";
import CustomerLoginPopUp from "components/LoginPopUp";
import {
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookIcon,
} from "react-share";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap.css";
import { useToasts } from "react-toast-notifications";
import CostDiscountCalc from "./CostDistountCalc";

function ListingCard({
  key,
  cardColsSize = "two",
  route,
  color,
  imgUrl,
  totalImages = 0,
  productTitle,
  costPrice,
  discountedPrice,
  discountPercentage,
  hasWarrenty,
  isForFlashSale,
  isReturnable,
  id,
  isSold = false,
  isInWishList = false,
}) {
  const loginInfo = useSelector((state) => state.customerAuth);
  const token = loginInfo?.token;
  const { addToast } = useToasts();
  const [openModal, setOpenModal] = useState(false);
  const [modal, setModal] = useState(false);
  const shareUrl = " https://duipangra-fd-uat.herokuapp.com/";
  const setModalFunction = (value) => {
    setModal(value);
  };
  const handleWishList = (query, token) => {
    if (token) {
      addToWishList(query);
    } else {
      setOpenModal(true);
    }
  };
  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const [
    addToWishList,
    {
      isLoading: isLoadingAddToWishList,
      isSuccess: isSuccessAddToWishList,
      isError: isErrorAddToWishList,
      error: addToWishListError,
      data: dataAddToWishList,
    },
  ] = useAddToWishListProductMutation();

  const [
    deleteWishList,
    {
      isLoading: isLoadingDeleteWishList,
      isSuccess: isSuccessRemoveWishList,
      isError: isErrorRemoveWishList,
      error: removeFromWishListError,
      data: dataDeleteWishList,
    },
  ] = useDeleteMyWishListMutation();

  useEffect(() => {
    if (isSuccessAddToWishList) {
      addToast(
        dataAddToWishList?.message || "Product added to Wishlist successfully.",
        {
          appearance: "success",
        }
      );
      // router.push("/cart");
    }

    if (isErrorAddToWishList) {
      addToast(addToWishListError?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessAddToWishList, isErrorAddToWishList, addToWishListError]);

  useEffect(() => {
    if (isSuccessRemoveWishList) {
      addToast(
        dataDeleteWishList?.message ||
          "Product is successfully removed from Wish-list .",

        {
          appearance: "success",
        }
      );
      // router.push("/cart");
    }

    if (isErrorRemoveWishList) {
      addToast(removeFromWishListError?.data?.message, {
        appearance: "error",
      });
    }
  }, [isSuccessRemoveWishList, isErrorRemoveWishList, removeFromWishListError]);

  return (
    <div key={key} className={`${cardColsSize}-col`}>
      <div className="card">
        {openModal ? (
          <ReactModal
            directlyOpen={true}
            closeFunc={handleCloseModal}
            modal={modal}
          >
            <CustomerLoginPopUp
              setModalFunction={setModalFunction}
              modal={modal}
            ></CustomerLoginPopUp>
          </ReactModal>
        ) : null}
        <div className="card-image-holder relative">
          <figure className="image-holder rounded-xl overflow-hidden group">
            <div className="absolute z-40 top-6 space-y-2 right-6 w-[30px] transition-all opacity-0 group-hover:opacity-100 ">
              {isInWishList ? (
                <Tooltip
                  overlay="Click to remove from wish list"
                  placement="right"
                >
                  <HeartIcon
                    onClick={() => {
                      deleteWishList(id, token);
                    }}
                    loading={isLoadingDeleteWishList}
                    className="w-6 h-6 text-red-600 heart-icon-filled"
                  />
                </Tooltip>
              ) : (
                <Tooltip overlay="Click to add in wishlist" placement="right">
                  <HeartIcon
                    onClick={() => {
                      handleWishList(id, token);
                    }}
                    loading={isLoadingAddToWishList}
                    className="w-6 h-6 text-white "
                  />
                </Tooltip>
              )}
              <Tooltip overlay="Click to share" placement="right">
                <FacebookShareButton
                  url={shareUrl}
                  quote={"2Pangra"}
                  hashtag={"#2Pangra"}
                >
                  <FacebookIcon size={25} round={true} />
                </FacebookShareButton>
              </Tooltip>
              <Tooltip overlay="Click to share" placement="right">
                <WhatsappShareButton
                  url={shareUrl}
                  quote={"2Pangra"}
                  hashtag={"#2Pangra"}
                >
                  <WhatsappIcon size={25} round={true} />
                </WhatsappShareButton>
              </Tooltip>
            </div>

            <div className="absolute z-20 top-1 space-y-2 left-2 whitespace-nowrap ">
              {Number(discountPercentage) ? (
                <div className="absolute z-20 top-1 space-y-2 left-2  ">
                  {`${Number(discountPercentage).toFixed()}` === `0` ? (
                    <span className="badge-orange inline-block ">
                      {Number(discountPercentage)?.toFixed(2)}% OFF
                    </span>
                  ) : (
                    <span className="badge-orange inline-block ">
                      {Number(discountPercentage)?.toFixed(0)}% OFF
                    </span>
                  )}
                </div>
              ) : null}
            </div>

            <Link href={route}>
              <a className="relative w-[200px] h-[200px] rounded-xl overflow-hidden before:transition-all before:absolute before:z-10 before:inset-0 before:bg-textColor before:bg-opacity-0 group-hover:before:bg-opacity-30">
                {imgUrl?.substring(0, 4) === "http" ? (
                  <Image src={imgUrl} alt="Img Desc" layout="fill" />
                ) : (
                  <img src={imgUrl} alt="image description" />
                )}
              </a>
            </Link>
          </figure>
          {isSold ? (
            <div className="absolute flex justify-center items-center inset-0 bg-black bg-opacity-60 text-white font-semibold text-lg z-50 ">
              <span>{isSold ? "SOLD OUT" : ""}</span>
            </div>
          ) : (
            ""
          )}{" "}
          <div className="no-of-image absolute right-3 bottom-3 bg-gray-200 flex  items-center px-2 rounded-md ">
            <PhotographIcon className="h-4 w-4" />
            <span className="number text-sm ml-1">
              {totalImages !== 0 && `${totalImages}+`}
            </span>
          </div>
        </div>
        <div className="card-body">
          <Link href={route}>
            <a className="vehicle-name text-textColor">{productTitle}</a>
          </Link>

          <CostDiscountCalc
            discountPercentage={discountPercentage}
            costPrice={costPrice}
            discountedPrice={discountedPrice}
          ></CostDiscountCalc>

          {/* <div className="detail-wrapper">
            <div className="detail">
              <div className="icon-holder">
                <ColorSwatchIcon />
              </div>

              <span className="name-icon">{color}</span>
            </div>
            <div className="detail">
              <div className="icon-holder">
                <SupportIcon />
              </div>
              <span className="name-icon">
                <span className="text-sm">{isForFlashSale ? "Yes" : "No"}</span>
              </span>
            </div>

            <div className="detail">
              <div className="icon-holder">
                <GlobeIcon />
              </div>
              <span className="name-icon">
                <span className="text-sm">{hasWarrenty ? "Yes" : "No"}</span>
              </span>
            </div>
            <div className="detail">
              <div className="icon-holder">
                <CheckCircleIcon />
              </div>
              <span className="name-icon">
                <span className="text-sm">{isReturnable ? "Yes" : "No"}</span>
              </span>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
}

export default ListingCard;
