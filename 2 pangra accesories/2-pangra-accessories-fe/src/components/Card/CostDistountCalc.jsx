import React from "react";
import { thousandNumberSeparator } from "utils/thousandNumberFormat";

const CostDiscountCalc = ({
  discountPercentage,
  costPrice,
  discountedPrice,
}) => {
  return (
    <div>
      {Number(discountPercentage) ? (
        <div className="text-primary-dark font-semibold">
          <span className="line-through text-error">
            NRs. &nbsp;{thousandNumberSeparator(costPrice)}
          </span>
          <span>
            &nbsp;NRs. &nbsp;{thousandNumberSeparator(discountedPrice)}
          </span>
        </div>
      ) : (
        <div className="text-primary-dark font-semibold">
          <span>NRs. &nbsp;{thousandNumberSeparator(costPrice)}</span>
        </div>
      )}
    </div>
  );
};

export default CostDiscountCalc;
