import React, { useEffect, useRef } from "react";

function Editor({
  onChange,
  editorLoaded,
  label,
  name,
  value,
  required = true,
  touched,
  errors,
}) {
  const editorRef = useRef();
  const { CKEditor, ClassicEditor } = editorRef.current || {};

  useEffect(() => {
    editorRef.current = {
      CKEditor: require("@ckeditor/ckeditor5-react").CKEditor, // v3+
      ClassicEditor: require("@ckeditor/ckeditor5-build-classic"),
    };
  }, []);

  return (
    <>
      {editorLoaded ? (
        <>
          <label>
            {label}
            <span className="required">
              <span>{required && "*"}</span>
            </span>
          </label>
          <CKEditor
            type=""
            name={name}
            editor={ClassicEditor}
            config={{
              removePlugins: [
                "CKFinderUploadAdapter",
                "CKFinder",
                "EasyImage",
                "Image",
                "ImageCaption",
                "ImageStyle",
                "ImageToolbar",
                "ImageUpload",
                "MediaEmbed",
                "Table",
                "BlockQuote",
              ],
            }}
            data={value}
            onChange={(event, editor) => {
              const data = editor.getData();
              onChange(data);
            }}
          />
          {touched && errors && (
            <div className="text-error text-sm">{errors}</div>
          )}
        </>
      ) : (
        <div>Editor loading</div>
      )}
    </>
  );
}

export default Editor;
