import { useField } from "formik";

const Input = ({
  label,
  required = true,
  type = "text",
  disabled = false,
  description = "",
  values,
  ...props
}) => {
  const [field, meta] = useField(props);

  if (type === "checkbox") {
    return (
      <>
        <div className="form-check">
          <input
            {...field}
            {...props}
            className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
            type={type}
            value={values}
            checked={field.value}
            id={props.id || props.name}
          />
          <label
            className="form-check-label inline-block text-gray-800"
            htmlFor={props.id || props.name}
          >
            {description}
          </label>
        </div>
      </>
    );
  }
  return (
    <>
      <label htmlFor={props.id || props.name}>
        {label}
        <span className="required">
          <span>{required && "*"}</span>
        </span>
      </label>
      <input
        type={type}
        {...field}
        {...props}
        className={
          meta.touched && meta.error
            ? "border-primary-dark placeholder-primary-dark"
            : disabled
            ? "disabled-input"
            : ""
        }
        disabled={disabled}
      />
      {meta.touched && meta.error && (
        <div className="text-error text-sm">{meta.error}</div>
      )}
    </>
  );
};

export default Input;
