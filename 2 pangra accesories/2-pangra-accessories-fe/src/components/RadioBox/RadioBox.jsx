import { useField } from "formik";
import PropTypes from "prop-types";

const RadioBox = ({ title = "", name = "", values = [], onChange }) => {
  // values=[{label:"",value:""}]
  const [field, meta] = useField(name);

  return (
    <div>
      <span className="text-base">{title}</span>
      <div className="mt-2" onChange={onChange}>
        {values?.map((value, i) => {
          return (
            <label key={i} className="inline-flex items-center mr-3">
              <input
                {...field}
                type="radio"
                className="form-radio"
                name={name}
                value={value.value}
                // converted to string because  e.target.value gives string
                checked={`${value.value}` === `${field.value}`}
              />
              <span className="ml-2 ">{value.label}</span>
            </label>
          );
        })}
      </div>
      {meta.error ? (
        <div className="text-primary-dark">{meta.error}</div>
      ) : null}
    </div>
  );
};

RadioBox.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  required: PropTypes.bool,
  values: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default RadioBox;
