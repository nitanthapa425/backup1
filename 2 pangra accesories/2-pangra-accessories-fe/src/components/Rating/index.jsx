import React from "react";
import ReactStars from "react-rating-stars-component";

export default function Rating({ sendRatingValue, ratingNumber }) {
  return (
    <div className="App">
      {ratingNumber !== undefined && (
        <ReactStars
          size={25}
          count={5}
          color="gray"
          activeColor="#A7EB4A"
          value={ratingNumber}
          a11y={true}
          isHalf={true}
          onChange={sendRatingValue}
        />
      )}
    </div>
  );
}
