import ImageGallery from "react-image-gallery";
import PropTypes from "prop-types";
import "react-image-gallery/styles/css/image-gallery.css";

function ImgGallery({ images, discountPercentage }) {
  return (
    <>
      {images.length === 0 ? (
        <img className="w-full" src="/images/placeholder.jpg"></img>
      ) : (
        <div className=" relative mb-[1.25rem] ">
          {Number(discountPercentage) ? (
            <div className="absolute z-20 top-6 space-y-2 left-3  ">
              {`${Number(discountPercentage).toFixed()}` === `0` ? (
                <span className="badge-orange ">
                  {Number(discountPercentage)?.toFixed(2)}% OFF
                </span>
              ) : (
                <span className="badge-orange ">
                  {Number(discountPercentage)?.toFixed(0)}% OFF
                </span>
              )}
            </div>
          ) : null}

          <ImageGallery
            items={images}
            showThumbnails={true}
            lazyLoad={true}
            showPlayButton={false}
          />
        </div>
      )}
    </>
  );
}

ImgGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ImgGallery;
