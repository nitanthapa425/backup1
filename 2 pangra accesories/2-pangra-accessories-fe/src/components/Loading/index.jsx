const Loading = () => {
  return (
    <div
      className={
        "absolute w-full h-full bg-white/20 py-3 flex flex-col justify-center items-center top-[85px] bottom-0 left-0 right-0 z-[9999]"
      }
    >
      <div className="flex flex-wrap justify-center fixed top-1/2 left-1/2 -translate-y-1/2 -translate-x-1/2 ">
        <div className="w-full flex justify-center">
          <svg
            className={`animate-spin h-10 w-10 text-error`}
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
          >
            <circle
              className="opacity-25"
              cx="12"
              cy="12"
              r="10"
              stroke="currentColor"
              strokeWidth="4"
            ></circle>
            <path
              className="opacity-75"
              fill="currentColor"
              d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
            ></path>
          </svg>
        </div>
      </div>
    </div>
  );
};

export default Loading;
