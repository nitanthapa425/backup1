import Link from "next/link";
import EmailNewsLetter from "components/NewsLetter";

function Footer() {
  return (
    <footer id="footer" className="rounded-t-[25px] overflow-hidden">
      <div className="footer-top py-10 bg-gray-200 ">
        <div className="container">
          <div className="row">
            <div className=" px-4  w-[100%] md:w-[35%] ">
              <Link href="/">
                <a className="mb-2">
                  <img
                    src="/images/logo.svg"
                    alt="2 Pangra"
                    className="w-[180px]"
                  />
                </a>
              </Link>
              <p className="text-sm">
                2Pangra is already working on an online platform that enables
                the purchase and sale of second-hand motorcycles powered by AI &
                Data Science. After intensive tests, the company aims to launch
                the platform to make buying and selling second hand motorcycles
                a trusted, seamless and profitable experience for everyone. The
                platform will work with various companies in the automotive and
                finance sectors to bring motorcycle financing, insurance,
                servicing and related services to its customers.
              </p>
              <ul className="flex space-x-6 text-[24px]">
                <li>
                  <a
                    href="https://business.facebook.com/2Pangra/"
                    className="text-textColor hover:text-primary-dark"
                  >
                    <span className="icon-facebook"></span>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/2pangra/"
                    className="text-textColor hover:text-primary-dark"
                  >
                    <span className="icon-instagram"></span>
                  </a>
                </li>
                <li>
                  <a
                    href="https://np.linkedin.com/company/2pangra-pvt-ltd?trk=public_profile_topcard-current-company"
                    className="text-textColor hover:text-primary-dark"
                  >
                    <span className="icon-linkedin"></span>
                  </a>
                </li>
              </ul>
            </div>
            <div className=" px-4 w-[50%] md:w-[17%] ">
              <ul className="space-y-[8px] text-sm">
                <li className="font-semibold text-[16px]">Quick Links</li>
                <li>
                  <Link href="/about-us">
                    <a className="text-textColor hover:text-primary-dark">
                      About Us
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/blog">
                    <a className="text-textColor hover:text-primary-dark">
                      Blog
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/career">
                    <a className="text-textColor hover:text-primary-dark">
                      Career
                    </a>
                  </Link>
                </li>
                <Link href="/terms-condition">
                  <a className="text-textColor hover:text-primary-dark">
                    Terms & Conditions
                  </a>
                </Link>

                <li>
                  <a
                    href="/policy"
                    className="text-textColor hover:text-primary-dark"
                  >
                    Privacy Policy
                  </a>
                </li>
                <li>
                  <Link href="/sitemap">
                    <a className="text-textColor hover:text-primary-dark">
                      Sitemap
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className=" px-4 w-[50%] md:w-[17%] ">
              <ul className="space-y-[8px] text-sm">
                <li className="font-semibold text-[16px]">Customer Care</li>
                <li>
                  <Link href="/help-center">
                    <a className="text-textColor hover:text-primary-dark">
                      Help Center
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact-us">
                    <a className="text-textColor hover:text-primary-dark">
                      Contact Us
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/how-to-buy">
                    <a className="text-textColor hover:text-primary-dark">
                      How To Buy
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/return-policy">
                    <a className="text-textColor hover:text-primary-dark">
                      Return & Refunds
                    </a>
                  </Link>
                </li>
                <li>
                  <Link href="/faqs">
                    <a className="text-textColor hover:text-primary-dark">
                      FAQs{" "}
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className=" px-4 mt-6 w-[100%] md:w-[31%]  md:mt-0">
              <h2 className="h5 leading-none mb-4">
                Enter Your Email For Latest Offers
              </h2>
              <EmailNewsLetter />
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom py-10">
        <div className="container">
          <div className="trending mb-4 pb-5 border-b-[1px] border-gray-200 ">
            <h3 className="h5"> Trending In Dui Pangra </h3>
            <div className="space-y-2">
              <a href="#">Rynox</a>
              <a href="#">Raida</a>
              <a href="#">Bolt</a>
              <a href="#">Kavac</a>
              <a href="#">Metro</a>
              <a href="#">Condor</a>
            </div>
          </div>
          <div className="must-searched mb-4 pb-5 border-b-[1px] border-gray-200 ">
            <h3 className="h5"> most Searched </h3>
            <div className="space-y-2">
              <a href="#">Rynox</a>
              <a href="#">Raida</a>
              <a href="#">Bolt</a>
              <a href="#">Kavac</a>
              <a href="#">Metro</a>
              <a href="#">Condor</a>
              <a href="#">Urban</a>
              <a href="#">Tornado</a>
            </div>
          </div>
          <div className="pb-5">
            <h3 className="h5">
              Experience Hassle-Free Online Shopping in Nepal.
            </h3>
            <p className="text-sm">
              2Pangra was formed by people with a history of trustworthiness,
              capability and foresight. Everything done at 2Pangra adheres to
              high standards of transparency and corporate governance.
            </p>

            <p className="text-sm">
              2Pangra brings the same diligence and trustworthiness to its
              product and services. We don’t sell “First Copy” and “Second Copy”
              products. We don’t sell imitation, fake or shoddy products. You
              can be assured that products sold by 2Pangra are procured straight
              from BRANDED manufacturers after complete due diligence and
              research on them.
            </p>
          </div>
          <div className="Top Brands">
            <h3 className="h5 mb-2"> Top Brands </h3>
            <div className=" mb-3 pb-3 border-b-[1px] border-gray-200 pl-2">
              <h4 className="h5 ">Accessories</h4>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Jacket : </h6>
                <a href="#">Kavac</a>
                <a href="#">Octane</a>
                <a href="#">Torando</a>
                <a href="#">Urban</a>
              </div>
              <div className="space-y-2 pl-2">
                <h6 className="inline-block mr-2">Bag : </h6>
                <a href="#">Clawn Mini</a>
                <a href="#">Condor</a>
                <a href="#">Kavac</a>
                <a href="#">Octane</a>
                <a href="#">Torando</a>
                <a href="#">Urban</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
