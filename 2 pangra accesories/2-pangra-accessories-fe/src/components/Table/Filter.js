import DatePicker from "react-datepicker";
import createReactClass from "create-react-class";
import "react-datepicker/dist/react-datepicker.css";
import SelectEl, { components } from "react-select";
import { arrayToString } from "utils/arrtoString";
export const DefaultColumnFilter = ({ column: { filterValue, setFilter } }) => {
  return (
    <div className="relative">
      <input
        value={filterValue || ""}
        onChange={(e) => {
          setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
        }}
        className="border-none px-2 py-1 mt-2 text-sm rounded-sm w-full max-w-full pr-7"
        placeholder={`Search`}
      />

      {filterValue && (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5 absolute top-3 right-0 hover:text-primary"
          viewBox="0 0 20 20"
          fill="currentColor"
          onClick={() => {
            setFilter("");
          }}
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      )}
    </div>
  );
};
export const SelectColumnFilter = ({
  column: { filterValue, setFilter, possibleFilters },
}) => {
  return (
    <select
      onChange={(e) => {
        const val = e.target.value;
        setFilter(val);
      }}
      className="border-none px-2 py-1 mt-2 text-sm rounded-sm pr-8 min-w-[150px] "
      style={{
        overFlow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
      }}
    >
      <option value="">All</option>
      {possibleFilters.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
};

export const DateColumnFilter = ({ column: { filterValue, setFilter } }) => {
  const validDate = filterValue ? new Date(filterValue) : "";
  return (
    <div className="relative">
      <DatePicker
        className="border-none px-2 py-1 mt-2 text-sm rounded-sm min-w-[150px]"
        selected={validDate}
        value={filterValue || ""}
        onChange={(date) => {
          let requiredDateFormat;
          let dates;
          let months;
          if (date) {
            const localDateString = new Date(date).toLocaleDateString();
            const localDateStringArray = localDateString?.split("/");

            dates = localDateStringArray[1];
            months = localDateStringArray[0];

            if (dates < 10) {
              dates = `0${localDateStringArray[1]}`;
            }

            if (months < 10) {
              months = `0${localDateStringArray[0]}`;
            }
            requiredDateFormat =
              localDateStringArray[2] + "-" + months + "-" + dates;
          } else {
            requiredDateFormat = null;
          }

          setFilter(requiredDateFormat);
        }}
        dateFormat="MM/dd/yyyy"
        placeholderText="yyyy-dd-MM"
      />
      {filterValue && (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5 absolute top-3 right-0 hover:text-primary"
          viewBox="0 0 20 20"
          fill="currentColor"
          onClick={() => {
            setFilter("");
          }}
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      )}
    </div>
  );
};
export const DateTimeColumnFilter = ({ column: { filterValue, setFilter } }) => {
  const validDate = filterValue ? new Date(filterValue) : null;
  return (
    <div className="relative">
      <DatePicker
        className="border-none px-2 py-1 mt-2 text-sm rounded-sm min-w-[200px]"
        selected={validDate}
        value={validDate || ""}
        withPortal
        onChange={(date) => {
          setFilter(date?.toISOString());
        }}
        showTimeSelect
        dateFormat="MM/dd/yyyy h:mm aa"
        placeholderText="yyyy-dd-MM h:mm aa"
      />
      {filterValue && (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5 absolute top-3 right-0 hover:text-primary"
          viewBox="0 0 20 20"
          fill="currentColor"
          onClick={() => {
            setFilter("");
          }}
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      )}
    </div>
  );
};

export const YearColumnFilter = ({ column: { filterValue, setFilter } }) => {
  const validDate = filterValue ? new Date(filterValue) : null;
  return (
    <div className="relative">
      <DatePicker
        className="border-none px-2 py-1 mt-2 text-sm rounded-sm min-w-[150px]"
        selected={validDate}
        onChange={(date) => {
          let requiredDateFormat;
          if (date) {
            const localDateString = new Date(date).toLocaleDateString();
            const localDateStringArray = localDateString?.split("/");
            requiredDateFormat =
              localDateStringArray[2] +
              "-" +
              localDateStringArray[0] +
              "-" +
              localDateStringArray[1];
          } else {
            requiredDateFormat = null;
          }
          setFilter(requiredDateFormat);
        }}
        showYearPicker
        dateFormat="yyyy"
        yearItemNumber={9}
        placeholderText="yyyy"
        maxDate={new Date()}
      />
      {filterValue && (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5 absolute top-3 right-0 hover:text-primary"
          viewBox="0 0 20 20"
          fill="currentColor"
          onClick={() => {
            setFilter("");
          }}
        >
          <path
            fillRule="evenodd"
            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
            clipRule="evenodd"
          />
        </svg>
      )}
    </div>
  );
};

export const MultiSelectFilter = ({
  column: { filterValue, setFilter, possibleFilters },
}) => {
  // ********only change possibleFilters eg possibleFilters=[{label:'l1',value:'v1'}]
  const Option = createReactClass({
    render() {
      return (
        <div>
          <components.Option {...this.props}>
            <input
              type="checkbox"
              checked={this.props.isSelected}
              onChange={(e) => null}
            />{" "}
            <label>
              {this.props.label ? this.props.label : this.props.value}{" "}
            </label>
          </components.Option>
        </div>
      );
    },
  });
  return (
    <SelectEl
      onChange={(selectedBrand) => {
        setFilter(arrayToString(selectedBrand));
      }}
      options={possibleFilters.map((option) => ({
        value: option.value,
        label: option.label,
      }))}
      isMulti={true}
      closeMenuOnSelect={true}
      components={{ Option }}
      hideSelectedOptions={false}
      backspaceRemovesValue={false}
      className="border-none px-2 py-1 mt-2 text-sm rounded-sm min-w-[200px]"
    />
  );
};
