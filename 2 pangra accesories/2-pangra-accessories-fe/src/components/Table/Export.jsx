import { useState, useEffect, useRef } from "react";
import { generatePDF } from "components/Pdf/ReactPdf";
import { GenerateCSV } from "components/Csv/ReactCsv";
import Popup from "components/Popup";
import { CSVLink } from "react-csv";

/*
PDF headers and data should be in this format
const pdfBody = [{ id: "id1", name: "name1" }, { id: "id2", name: "name2" }];
const pdfColumn = [{ header: 'Id', dataKey: 'id' }, { header: 'Name', dataKey: 'name' }];
*/

/*
CSV headers and data should be in this format
const csvData = [{ firstname: 'Ahmed', lastname: 'Tomi' }, { firstname: 'Ahmed2', lastname: 'Tomi2' }];
const csvHeaders = [{ label: 'First Name', key: 'firstname' }, { label: 'Last Name', key: 'lastname' }];
*/

// headers prop in Export should be sent from Table Component in this way: <Export headers={columns} />. The columns that we defined in our View page should be sent in the headers prop.
// data prop in Export should be sent from Table Component in this way: <Export data={data} />. The data that we defined in our View page should be sent in the data prop.
export const Export = ({
  tableDataAll,
  setSkipTableDataAll,
  isLoadingAll,
  headers,
  data,
  pdfFileName,
  csvFileName,
  className,
}) => {
  const [pdfHeaders, setPdfHeaders] = useState([]);
  const [pdfData, setPdfData] = useState([]);
  const [pdfDataAll, setPdfDataAll] = useState([]);
  const [csvHeaders, setCsvHeaders] = useState([]);
  const [csvData, setCsvData] = useState([]);
  const [csvDataAll, setCsvDataAll] = useState([]);
  const [openExportAll, setOpenExportAll] = useState(false);
  const [toggle, setToggle] = useState(false);
  const [pdfCsv, setPdfCsv] = useState(false);
  const [giveCsv, setGiveCsv] = useState(false);

  const exportAllRef = useRef(null);

  const createHeaders = (headers) => {
    setPdfHeaders(
      headers.map((h) => {
        return {
          header: h.Header,
          dataKey: h.accessor,
        };
      })
    );
    setCsvHeaders(
      headers.map((h) => {
        return {
          label: h.Header,
          key: h.accessor,
        };
      })
    );
  };

  const createData = (data, tableDataAll) => {
    setPdfData(data);
    setPdfDataAll(tableDataAll);
    setCsvData(data);
    setCsvDataAll(tableDataAll);
  };

  useEffect(() => {
    createHeaders(headers);
    createData(data, tableDataAll);
  }, [headers, data, tableDataAll]);

  useEffect(() => {
    if (tableDataAll.length !== 0 && !isLoadingAll && pdfDataAll.length !== 0) {
      if (pdfCsv === "csv") {
        setGiveCsv(true);
      } else if (pdfCsv === "pdf") {
        generatePDF(pdfDataAll, pdfHeaders, pdfFileName);
      }
      setOpenExportAll(false);
      setPdfCsv("");
    }
  }, [tableDataAll, pdfDataAll, pdfHeaders, pdfFileName, toggle]);

  useEffect(() => {
    if (giveCsv) {
      exportAllRef.current.link.click();
    }
  }, [giveCsv]);

  return (
    <>
      {openExportAll ? (
        <Popup
          title="This might take some time. Do you want to Export all data?"
          description=""
          onOkClick={() => {
            setSkipTableDataAll(false);
            setToggle(!toggle);
          }}
          onCancelClick={() => {
            setOpenExportAll(false);
          }}
          okText="Yes"
          cancelText="Cancel"
          loading={isLoadingAll}
        />
      ) : null}
      <div className={"relative has-dropdown cursor-pointer " + className}>
        <div className="flex items-center text-gray-600 hover:text-primary">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="h-5 w-5"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"
            />
          </svg>
          <span className="ml-1">Export</span>
        </div>
        <ul className="z-50 border dropdown border-gray-200 py-2  rounded-md opacity-0 w-40 invisible absolute top-full -left-4 bg-white shadow-md mt-3 transition-all text-sm">
          <li
            onClick={() => {
              generatePDF(pdfData, pdfHeaders, pdfFileName);
            }}
            className="text-textColor py-1 px-4 hover:text-primary "
          >
            Export as Pdf
          </li>
          <li className="text-textColor py-1 px-4 ">
            <GenerateCSV
              data={csvData}
              headers={csvHeaders}
              fileName={csvFileName}
            />
          </li>
          <li
            onClick={() => {
              setOpenExportAll(true);
              setPdfCsv("pdf");
            }}
            className="text-textColor py-1 px-4 hover:text-primary "
          >
            Export All as Pdf
          </li>

          <li
            className="text-textColor py-1 px-4 "
            onClick={() => {
              setOpenExportAll(true);
              setPdfCsv("csv");
              setGiveCsv(false);
            }}
          >
            Export All to CSV
          </li>

          <CSVLink
            style={{ display: "none" }}
            ref={exportAllRef}
            data={csvDataAll}
            headers={csvHeaders}
            filename={`${csvFileName} ${new Date(
              Date.now()
            ).toLocaleDateString()}.csv`}
            className={`${className} text-black hover:text-primary`}
          >
            Export All to CSV
          </CSVLink>
        </ul>
      </div>
    </>
  );
};
