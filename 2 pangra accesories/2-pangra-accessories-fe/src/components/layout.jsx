import React from "react";
import Head from "next/head";

function layout({ children }) {
  return (
    <>
      <Head></Head>
      <header>{/* Markup */}</header>
      <main>{children}</main>
      <footer>{/* Markup */}</footer>
    </>
  );
}

export default layout;
