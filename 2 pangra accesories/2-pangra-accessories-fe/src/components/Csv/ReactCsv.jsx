import { CSVLink } from "react-csv";

export const GenerateCSV = ({
  data,
  headers,
  fileName = "file",
  className,
}) => {
  return (
    <CSVLink
      data={data}
      headers={headers}
      filename={`${fileName} ${new Date(Date.now()).toLocaleDateString()}.csv`}
      className={`${className} text-black hover:text-primary`}
    >
      Export to CSV
    </CSVLink>
  );
};
