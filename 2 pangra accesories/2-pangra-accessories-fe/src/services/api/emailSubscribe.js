import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const emailSubscribe = createApi({
  reducerPath: 'emailSubscribe',
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    emailSubscribe: builder.mutation({
      query: (data) => {
        return {
          url: `/email/subscribe`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { useEmailSubscribeMutation } = emailSubscribe;
