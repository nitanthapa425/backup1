import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const delivery = createApi({
  reducerPath: "delivery",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    readAllDelivery: builder.query({
      query: (query) => {
        return {
          url: `/delivery${query}`,
          method: "GET",
        };
      },
      providesTags: ["readDeliveryDetail"],
    }),
    readDeletedDelivery: builder.mutation({
      query: (id) => {
        return {
          url: `/delivery/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readDeliveryDetail"],
    }),
    readSingleDelivery: builder.query({
      query: (id) => {
        return {
          url: `/delivery/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readDeliveryDetail"],
    }),
    updateDelivery: builder.mutation({
        query: (updatePostData) => {
          const { id, ...data } = updatePostData;
          return {
            url: `/delivery/${id}`,
            method: 'PUT',
            body: data,
          };
        },
        invalidatesTags: ["readDeliveryDetail"],
    }),
    readSingleDeletedDelivery: builder.query({
      query: (id) => {
        return {
          url: `/delivery/deletedUser/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readDeletedDelivery"],
    }),
    activeDeletedDelivery: builder.mutation({
      query: (id) => {
        return {
          url: `/delivery/revertUser/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readDeliveryDetail", "readDeletedDelivery"],
    }),
  }),
});

export const {
  useReadAllDeliveryQuery,
  useReadDeletedDeliveryMutation,
  useReadSingleDeliveryQuery,
  useUpdateDeliveryMutation,
  useReadSingleDeletedDeliveryQuery,
  useActiveDeletedDeliveryMutation
} = delivery;
