import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const subCategory = createApi({
    reducerPath: 'subCategory',
    baseQuery: fetchBaseQuery({
        baseUrl,
        prepareHeaders: (headers, { getState }) => {
            const token = getState().adminAuth.token;
            if (token) {
                headers.set('authorization', `Bearer ${token}`);
            }
            return headers;
        },
    }),
    tagTypes: ['readSubCategoryDetail, readAllSubCategory', 'readAllSubCategoryNoAuth', 'readAllSubCategoryByCatIdNoAuth'],

    endpoints: (builder) => ({
        readSubCategory: builder.query({
            query: (customQuery) => {
                return {
                    url: `/accessoriesSubCategory/${customQuery}`,
                    method: 'GET',
                };
            },
            providesTags: ['readAllSubCategory'],
        }),

        readSubCategoryNoAuth: builder.query({
            query: () => {
                return {
                    url: `/accessoriesSubCategoryNoAuth`,
                    method: 'GET',
                };
            },
            invalidatesTags: ['readAllSubCategoryNoAuth'],
        }),

        deleteSubCategory: builder.mutation({
            query: (id) => {
                return {
                    url: `/accessoriesSubCategory/${id}`,
                    method: 'DELETE',
                };
            },
            invalidatesTags: ['readSubCategoryDetail', 'readAllSubCategory', 'readAllSubCategoryNoAuth', 'readAllSubCategoryByCatIdNoAuth'],
        }),

        updateSubCategory: builder.mutation({
            query: (updatePostData) => {
                const { id, ...data } = updatePostData;
                return {
                    url: `/accessoriesSubCategory/${id}`,
                    method: 'PUT',
                    body: data,
                };
            },
            invalidatesTags: ['readSubCategoryDetail', 'readAllSubCategory', 'readAllSubCategoryNoAuth', 'readAllSubCategoryByCatIdNoAuth'],
        }),

        createSubCategory: builder.mutation({
            query: (data) => {
                return {
                    url: `/accessoriesSubCategory`,
                    method: 'POST',
                    body: data,
                };
            },
            invalidatesTags: ['readSubCategoryDetail', 'readAllSubCategory', 'readAllSubCategoryNoAuth', 'readAllSubCategoryByCatIdNoAuth'],
        }),

        readSubCategoryFromCategoryNoAuth: builder.query({
            query: (categoryId) => {
                return {
                    url: `/subCategoryByCategoryId/${categoryId}`,
                    method: 'GET',
                };
            },
            // transformResponse: (res) => res.result,
            providesTags: ['readAllSubCategoryByCatIdNoAuth'],
        }),

        readSubCategoryDetails: builder.query({
            query: (id) => {
                return {
                    url: `/accessoriesSubCategory/${id}`,
                    method: 'GET',
                };
            },
            providesTags: ['readAllSubCategory'],
        }),
    }),
});

export const {
    useReadSubCategoryQuery,
    useReadSubCategoryNoAuthQuery,
    useDeleteSubCategoryMutation,
    useUpdateSubCategoryMutation,
    useCreateSubCategoryMutation,
    useReadSubCategoryFromCategoryNoAuthQuery,
    useReadSubCategoryDetailsQuery,
} = subCategory;
