import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";
export const customer = createApi({
  reducerPath: "customer",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === "superAdmin"
          ? getState().adminAuth.token
          : getState().levelReducer.level === "customer"
          ? getState().customerAuth.token
          : "";
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readCustomerDetail", "readAllCustomers", "readShippingAddress"],
  endpoints: (builder) => ({
    customerLogin: builder.mutation({
      query: (data) => {
        return {
          url: `/client/clientLogin`,
          method: "POST",
          body: data,
        };
      },
    }),
    customerSignUp: builder.mutation({
      query: (data) => {
        return {
          url: `/client`,
          method: "POST",
          body: data,
        };
      },
    }),
    customerSignUpSparrow: builder.mutation({
      query: (data) => {
        return {
          url: `https://api.sparrowsms.com/v2/sms/`,
          method: "POST",
          body: data,
        };
      },
      // overrideExisting is used to disable baseUrl
      overrideExisting: false,
    }),
    customerForgot: builder.mutation({
      query: (data) => {
        return {
          url: `/client/forgotClientPassword`,
          method: "POST",
          body: data,
        };
      },
    }),

    customerReset: builder.mutation({
      query: (data) => {
        return {
          url: `/client/resetClientPassword`,
          method: "POST",
          body: data,
        };
      },
    }),
    customerUpdate: builder.mutation({
      query: (data) => {
        return {
          url: `/client/changeClientPassword`,
          method: "POST",
          body: data,
        };
      },
    }),
    verifyPhone: builder.mutation({
      query: (data) => {
        return {
          url: `/confirmMobile`,
          method: "POST",
          body: data,
        };
      },
    }),
    sendPasswordOtp: builder.mutation({
      query: (data) => {
        return {
          url: `/client/resetClientPassword`,
          method: "POST",
          body: data,
        };
      },
    }),
    ResendOtp: builder.mutation({
      query: (data) => {
        return {
          url: `/resendVerificationToken`,
          method: "PATCH",
          body: data,
        };
      },
    }),
    ResendOtpForget: builder.mutation({
      query: (data) => {
        return {
          url: `/client/resendForgotVerificationToken`,
          method: "PATCH",
          body: data,
        };
      },
    }),
    readCustomer: builder.query({
      query: (id) => {
        return {
          url: `/client/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),
    readAllCustomer: builder.query({
      query: (query) => {
        return {
          url: `/client/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllCustomers"],
    }),

    readAllDeletedCustomer: builder.query({
      query: (query) => {
        return {
          url: `/client/deletedClients/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllCustomers"],
    }),
    readDeletedCustomer: builder.query({
      query: (id) => {
        return {
          url: `client/deletedClients/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),

    deleteCustomer: builder.mutation({
      query: (id) => {
        return {
          url: `/client/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllCustomers", "readCustomerDetail"],
    }),
    updateCustomer: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/client/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: ["readAllCustomers", "readCustomerDetail"],
    }),
    revertDeletedCustomer: builder.mutation({
      query: (id) => {
        return {
          url: `/client/revertClient/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readAllCustomers", "readCustomerDetail"],
    }),
    customerMyProfile: builder.query({
      query: () => {
        return {
          url: `/client/myProfile`,
          method: "GET",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),
    editCustomerMyProfile: builder.mutation({
      query: () => {
        return {
          url: `/client/myProfile`,
          method: "PUT",
        };
      },
      invalidateTags: ["readCustomerDetail"],
    }),
    readShippingAddress: builder.query({
      query: () => {
        return {
          url: `/client/customerLocation`,
          method: "GET",
        };
      },
      // keepUnusedDataFor: 0,
      providesTags: ["readShippingAddress"],
    }),
    updateShippingAddress: builder.mutation({
      query: ({ id, body }) => {
        return {
          url: `/client/customerLocation/${id}`,
          method: "PATCH",
          body: body,
        };
      },

      invalidateTags: ["readShippingAddress", "readSingleShippingAddress"],
    }),
    deleteShippingAddress: builder.mutation({
      query: (id) => {
        return {
          url: `/client/customerLocation/${id}`,
          method: "DELETE",
        };
      },

      invalidateTags: ["readShippingAddress"],

      // invalidateTags: ["readShippingAddress", "readSingleShippingAddress"],
    }),
    readSingleShippingAddress: builder.query({
      query: (id) => {
        return {
          url: `/client/customerLocation/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readSingleShippingAddress"],
      // invalidateTags: ["readCustomerDetail"],
    }),

    addShippingAddress: builder.mutation({
      query: (data) => {
        return {
          url: `/client/customerLocation`,
          method: "POST",
          body: data,
        };
      },
      invalidateTags: ["readShippingAddress"],
    }),
    readShippingAddressListQuery: builder.mutation({
      query: (data) => {
        return {
          url: `/client/customerLocation`,
          method: "POST",
          body: data,
        };
      },
      // invalidateTags: ["readCustomerDetail"],
    }),
    validateUser: builder.query({
      query: () => {
        return {
          url: `/validate-user`,
          method: "GET",
          // this method check weather the given token is expired or not
        };
      },
    }),
    readSizeColorQuantity: builder.mutation({
      query: ({ body, id }) => {
        return {
          url: `/orderItems/optionalDetail/${id}`,
          method: "POST",
          body: body,
        };
      },
    }),

    readQuantity: builder.mutation({
      query: ({ body, id }) => {
        return {
          url: `/orderItems/getOptionalDetail/${id}`,
          method: "POST",
          body: body,
        };
      },
    }),

    readShippingCharge: builder.query({
      query: () => {
        return {
          url: `/shippingCharge`,
          method: "GET",
          // this method check weather the given token is expired or not
        };
      },
    }),
    // for admin
    checkCustomerByMobile: builder.mutation({
      query: (mobile) => {
        return {
          url: `/client/customer-by-mobile/${mobile}`,
          method: "GET",
        };
      },
    }),
    createCustomerAfterOrder: builder.mutation({
      query: ({ orderGroupId, data }) => {
        return {
          url: `/orderItems/order-item-admin/${orderGroupId} `,
          method: "PATCH",
          body: data,
        };
      },
    }),

    addCustomerEmail: builder.mutation({
      query: (data) => {
        return {
          url: `/orderItems/user/updateEmail`,
          method: "POST",
          body: data,
        };
      },
      invalidateTags: ["readCustomerDetail", "readAllCustomers"],
    }),
  }),
});

export const {
  useCustomerSignUpMutation,
  useCustomerSignUpSparrowMutation,
  useCustomerForgotMutation,
  useCustomerResetMutation,
  useCustomerUpdateMutation,
  useVerifyPhoneMutation,
  useResendOtpMutation,
  useCustomerLoginMutation,
  useUpdateCustomerMutation,
  useDeleteCustomerMutation,
  useRevertDeletedCustomerMutation,
  useReadCustomerQuery,
  useReadDeletedCustomerQuery,
  useReadAllCustomerQuery,
  useReadAllDeletedCustomerQuery,
  useCustomerMyProfileQuery,
  useEditCustomerMyProfileMutation,
  useValidateUserQuery,
  useSendPasswordOtpMutation,
  useResendOtpForgetMutation,
  useAddShippingAddressMutation,
  useReadShippingAddressQuery,
  useReadSingleShippingAddressQuery,
  useUpdateShippingAddressMutation,
  useDeleteShippingAddressMutation,
  useReadSizeColorQuantityMutation,
  useReadQuantityMutation,
  useReadShippingChargeQuery,

  useCheckCustomerByMobileMutation,
  useCreateCustomerAfterOrderMutation,

  useAddCustomerEmailMutation,
} = customer;
