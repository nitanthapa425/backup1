import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const brand = createApi({
  reducerPath: "brand",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: [
    "readBrandDetail",
    "readAllBrand",
    "readAllBrands",
    "readAllBrandsNoAuth",
  ],

  endpoints: (builder) => ({
    readBrand: builder.query({
      query: () => {
        return {
          url: `/brandAccessories`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrands"],
    }),

    readBrandNoAuth: builder.query({
      query: () => {
        return {
          url: `/brandAccessoriesNoAuth`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrandsNoAuth"],
    }),

    deleteBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/brandAccessories/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readBrandDetail", "readAllBrand", "readAllBrands"],
    }),

    updateBrand: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/brandAccessories/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "readBrandDetail",
        "readAllBrand",
        "readAllBrands",
        "readAllBrandsNoAuth",
      ],
    }),

    createBrand: builder.mutation({
      query: (data) => {
        return {
          url: `/brandAccessories`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readBrandDetail", "readAllBrand", "readAllBrands"],
    }),

    readBrandDetails: builder.query({
      query: (BrandId) => {
        return {
          url: `/brandAccessories/${BrandId}`,
          method: "GET",
        };
      },
      providesTags: ["readBrandDetail"],
    }),

    readBrandList: builder.query({
      query: (query) => {
        return {
          url: `/brandAccessories/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrand"],
    }),

    // Adding brand to homepage/featured brands
    addFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/addBrandInHomePage/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readAllBrand"],
    }),

    readFeaturedBrandsNoAuth: builder.query({
      query: (query) => {
        return {
          url: `/brandAccessoriesNoAuthLimitFeatured${query}`,
          method: "GET",
        };
      },
    }),

    readFeaturedALLBrandsNoAuth: builder.query({
      query: () => {
        return {
          url: `/brandAccessoriesNoAuth`,
          method: "GET",
        };
      },
    }),

    removeFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/removeBrandInHomePage/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: "POST",
        };
      },
      invalidatesTags: ["readAllBrand"],
    }),

    readBrandNoAuthWithLimit: builder.query({
      query: (query) => {
        return {
          url: `/brandVehicleNoAuthLimit${query}`,
          method: "GET",
        };
      },
    }),

    uploadBannerImage: builder.mutation({
      query: (data) => {
        return {
          url: `/banner-image`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readBannerImage"],
    }),

    getBannerImage: builder.query({
      query: () => {
        return {
          url: `/banner-image`,
          method: "GET",
        };
      },
      providesTags: ["readBannerImage"],
    }),
  }),
});

export const {
  useReadBrandQuery,
  useReadBrandNoAuthQuery,
  useDeleteBrandMutation,
  useUpdateBrandMutation,
  useCreateBrandMutation,
  useReadBrandDetailsQuery,
  useReadBrandListQuery,
  useAddFeaturedBrandMutation,
  useReadFeaturedBrandsNoAuthQuery,
  useRemoveFeaturedBrandMutation,
  useReadBrandNoAuthWithLimitQuery,
  useUploadBannerImageMutation,
  useGetBannerImageQuery,
  useReadFeaturedALLBrandsNoAuthQuery,
} = brand;
