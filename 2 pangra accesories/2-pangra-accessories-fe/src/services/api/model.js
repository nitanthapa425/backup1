import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const model = createApi({
  reducerPath: 'model',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ['AllModel'],
  tagTypes: ['readModelDetail,readAllModel'],

  endpoints: (builder) => ({
    readModel: builder.query({
      query: () => {
        return {
          url: `/vehicleModel`,
          method: 'GET',
        };
      },
    }),

    deleteModel: builder.mutation({
      query: (id) => {
        return {
          url: `/vehicleModel/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllModel', 'readModelDetail'],
    }),

    updateModel: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/vehicleModel/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllModel', 'readModelDetail'],
    }),

    createModel: builder.mutation({
      query: (data) => {
        return {
          url: `/vehicleModel`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['readAllModel', 'readModelDetail'],
    }),

    getModelFromBrand: builder.query({
      query: (brandId) => {
        return {
          url: `/vehicleModelByBrandId/${brandId}`,
          method: 'GET',
        };
      },
      // transformResponse: (res) => res.result,
    }),
    readModelDetails: builder.query({
      query: (modelId) => {
        return {
          url: `/vehicleModel/${modelId}`,
          method: 'GET',
        };
      },
      providesTags: ['readModelDetail'],
    }),

    readModelCustom: builder.query({
      query: (customQuery) => {
        return {
          url: `/vehicleModel${customQuery}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllModel'],
    }),
  }),
});

export const {
  useReadModelQuery,
  useDeleteModelMutation,
  useUpdateModelMutation,
  useCreateModelMutation,
  useGetModelFromBrandQuery,
  useReadModelDetailsQuery,
  useReadModelCustomQuery,
} = model;
