import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const deliveryLogin = createApi({
  reducerPath: "deliveryLogin",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().deliveryAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    deliveryLogin: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/login`,
          method: "POST",
          body: data,
        };
      },
    }),
    readAllDeliveryItem: builder.query({
      query: (query) => {
        return {
          url: `/delivery/getDeliveryItemsList/${query}`,
          method: "GET",
        };
      },
      // providesTags: ["readAllDeliveryItem"],
    }),
    sendDeliveryCode: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/sendDeliveryToken`,
          method: "POST",
          body: data,
        };
      },
      // providesTags: ["readAllDeliveryItem"],
    }),
    logoutDelivery: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/logout`,
          method: "POST",
          body: data,
        };
      },
      // providesTags: ["readAllDeliveryItem"],
    }),
    deliveryMyProfile: builder.query({
      query: () => {
        return {
          url: `/deliverySecurity/me/profile`,
          method: "GET",
        };
      },
      providesTags: ["deliveryMyProfile"],
      // invalidatesTags: [],
    }),
    deliveryUpdateMyProfile: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/me/profile`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: ["deliveryMyProfile"],
    }),
    changeDeliveryPassword: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/changePassword`,
          method: "POST",
          body: data,
        };
      },
      // providesTags: ["readAllDeliveryItem"],
    }),
    verifyDeliveryProduct: builder.mutation({
      query: (data) => {
        return {
          url: `/deliverySecurity/verifyDeliveryToken`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["userDeliveryList"],
    }),
    readAllDelivery: builder.query({
      query: (query) => {
        return {
          url: `/delivery${query}`,
          method: "GET",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),
    readDeletedDelivery: builder.mutation({
      query: (id) => {
        return {
          url: `/delivery${id}`,
          method: "DELETE",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),
    userDeliveryList: builder.query({
      query: (id) => {
        return {
          url: `/delivery/userDeliveryList/${id}`,
          method: "GET",
        };
      },

      providesTags: ["userDeliveryList"],
    }),
  }),
});

export const {
  useDeliveryLoginMutation,
  useReadAllDeliveryItemQuery,
  useSendDeliveryCodeMutation,
  useLogoutDeliveryMutation,
  useDeliveryMyProfileQuery,
  useChangeDeliveryPasswordMutation,
  useVerifyDeliveryProductMutation,
  useReadAllDeliveryQuery,
  useReadDeletedDeliveryMutation,
  useUserDeliveryListQuery,
  useDeliveryUpdateMyProfileMutation,
} = deliveryLogin;
