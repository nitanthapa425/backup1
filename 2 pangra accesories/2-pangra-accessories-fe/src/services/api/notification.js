import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const notification = createApi({
  reducerPath: "notification",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === "superAdmin"
          ? getState().adminAuth.token
          : getState().levelReducer.level === "customer"
          ? getState().customerAuth.token
          : "";
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readNotification"],

  endpoints: (builder) => ({
    readNotification: builder.query({
      query: (query) => {
        return {
          url: `/notification/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readNotification"],

      //   keepUnusedDataFor: 0,
    }),
    readParticularNotification: builder.mutation({
      query: (body) => {
        return {
          url: `/notification/mark-as-read`,
          method: "PATCH",
          body: body,
        };
      },
      invalidatesTags: ["readNotification"],
      //   keepUnusedDataFor: 0,
    }),
    unreadParticularNotification: builder.mutation({
      query: (body) => {
        return {
          url: `/notification/mark-as-unread`,
          method: "PATCH",
          body: body,
        };
      },
      invalidatesTags: ["readNotification"],
      //   keepUnusedDataFor: 0,
    }),

    deleteParticularNotification: builder.mutation({
      query: (body) => {
        return {
          url: `/notification/delete`,
          method: "DELETE",
          body: body,
        };
      },
      invalidatesTags: ["readNotification"],
    }),

    readAllNotification: builder.mutation({
      query: () => {
        return {
          url: `/notification/read-all`,
          method: "PATCH",
        };
      },
      invalidatesTags: ["readNotification"],
    }),
  }),
});

export const {
  useReadNotificationQuery,
  useReadParticularNotificationMutation,
  useUnreadParticularNotificationMutation,
  useReadAllNotificationMutation,
  useDeleteParticularNotificationMutation,
} = notification;
