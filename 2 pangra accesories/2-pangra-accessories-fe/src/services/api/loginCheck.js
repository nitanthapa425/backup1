import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const loginCheck = createApi({
  reducerPath: "loginCheck",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === "superAdmin"
          ? getState().adminAuth.token
          : getState().levelReducer.level === "customer"
          ? getState().customerAuth.token
          : getState().levelReducer.level === "delivery"
          ? getState().deliveryAuth.token
          : "";

      // const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),

  endpoints: (builder) => ({
    isLogin: builder.query({
      query: () => {
        return {
          url: `/validate-user`,
          method: "GET",
        };
      },

      keepUnusedDataFor: 0,
    }),

    isLoginCustomer: builder.query({
      query: () => {
        return {
          url: `client/validate-user`,
          method: "GET",
        };
      },

      keepUnusedDataFor: 0,
    }),

    isLoginDelivery: builder.query({
      query: () => {
        return {
          url: `deliverySecurity/validate-user`,
          method: "GET",
        };
      },

      keepUnusedDataFor: 0,
    }),

    logoutCustomer: builder.mutation({
      query: () => {
        return {
          url: `/client/clientLogout`,
          method: "POST",
        };
      },
    }),
    logoutSuperAdmin: builder.mutation({
      query: () => {
        return {
          url: `/logout`,
          method: "POST",
        };
      },
    }),
  }),
});

export const {
  useIsLoginQuery,
  useIsLoginCustomerQuery,
  useIsLoginDeliveryQuery,
  useLogoutCustomerMutation,
  useLogoutSuperAdminMutation,
} = loginCheck;
