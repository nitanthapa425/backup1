import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const passwordUpdate = createApi({
  reducerPath: 'changePassword',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    PasswordUpdatePost: builder.mutation({
      query: (data) => {
        return {
          url: `/changePassword`,
          method: 'POST',
          body: data,
        };
      },
    }),
  }),
});

export const { usePasswordUpdatePostMutation } = passwordUpdate;
