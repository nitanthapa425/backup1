import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const soldItems = createApi({
  reducerPath: "soldItems",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ["readCompanyDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readSoldItems: builder.query({
      query: (query) => {
        return {
          url: `/soldItems${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSoldItems"],
    }),
  }),
});

export const { useReadSoldItemsQuery } = soldItems;
