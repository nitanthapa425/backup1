import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const company = createApi({
  reducerPath: "company",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readCompanyDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readCompany: builder.query({
      query: (query) => {
        return {
          url: `/company/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllCompany"],
    }),

    deleteCompany: builder.mutation({
      query: (id) => {
        return {
          url: `/company/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllCompany"],
    }),

    updateCompany: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/company/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: ["readCompanyDetail"],
    }),

    readSingleCompany: builder.query({
      query: (companyId) => {
        return {
          url: `/company/${companyId}`,
          method: "GET",
        };
      },
      providesTags: ["readCompanyDetail"],
    }),

    createCompany: builder.mutation({
      query: (data) => {
        return {
          url: `/company`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readCompanyDetail", "readAllBrand", "readAllBrands"],
    }),
  }),
});

export const {
  useReadCompanyQuery,
  useDeleteCompanyMutation,
  useUpdateCompanyMutation,
  useCreateCompanyMutation,
  useReadSingleCompanyQuery,
} = company;
