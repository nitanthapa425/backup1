import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const flashSale = createApi({
  reducerPath: "flashSale",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["FLASH_SALE"],

  endpoints: (builder) => ({
    getAllFlashSaleProducts: builder.query({
      query: (query) => {
        return {
          url: `/accessories/flash-sale${query}`,
          method: "GET",
        };
      },
      providesTags: ["FLASH_SALE"],
    }),
    removeProductFromFlashSale: builder.mutation({
      query: (id) => {
        return {
          url: `/accessories/flash-sale/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["FLASH_SALE"],
    }),
    getFlashSalesProduct: builder.query({
      query: () => {
        return {
          url: `accessories/flash-sale/non-paginate`,
          method: "GET",
        };
      },
      keepUnusedDataFor:0
    }),
  }),
});

export const { useGetAllFlashSaleProductsQuery,useGetFlashSalesProductQuery,useRemoveProductFromFlashSaleMutation } = flashSale;
