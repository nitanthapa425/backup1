import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import axios from "axios";
import { baseUrl } from "config";

export const color = createApi({
  reducerPath: "color",
  baseQuery: fetchBaseQuery({
    baseUrl,
    // prepareHeaders: (headers, { getState }) => {
    //   const token = getState().adminAuth.token;
    //   if (token) {
    //     headers.set("authorization", `Bearer ${token}`);
    //   }
    //   return headers;
    // },
  }),
  tagTypes: ["readCompanyDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readColors: builder.query({
      query: () => {
        return {
          url: `/color`,
          method: "GET",
        };
      },
      providesTags: ["readAllColor"],
    }),

    createColor: builder.mutation({
      query: (data) => {
        return {
          url: `/color`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readColorDetail", "readAllBrand", "readAllBrands"],
    }),
  }),
});

export const { useReadColorsQuery, useCreateColorMutation } = color;

export const searchColor = (searchTerm) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${baseUrl}/color/?select=color&limit=20&sortBy=color&sortOrder=1&search="color":"${searchTerm}"`
      )
      .then((res) => {
        const data = res?.data?.map((colors) => ({
          label: colors?.color,
          value: colors.color,
        }));
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
