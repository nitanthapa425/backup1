import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const enquiryForm = createApi({
  reducerPath: "enquiryForm",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ["readCompanyDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readEnquiryList: builder.query({
      query: (query) => {
        return {
          url: `/enquiryForm/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllInquiry"],
    }),
    createEnquiryForm: builder.mutation({
      query: (values) => {
        return {
          url: `/enquiryForm`,
          method: "POST",
          body: values,
        };
      },

      invalidatesTags: ["readAllInquiry"],
    }),
    createEnquiryReply: builder.mutation({
      query: (updateInquiryData) => {
        const { id, ...body } = updateInquiryData;
        return {
          url: `/enquiryForm/replyEnquiry/${id}`,
          method: "PUT",
          body: body,
        };
      },

      invalidatesTags: ["readAllInquiry"],
    }),
  }),
});

export const {
  useReadEnquiryListQuery,
  useCreateEnquiryFormMutation,
  useCreateEnquiryReplyMutation,
} = enquiryForm;
