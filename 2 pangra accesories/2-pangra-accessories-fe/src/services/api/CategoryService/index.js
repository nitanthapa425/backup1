import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const category = createApi({
  reducerPath: 'category',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ['AllModel'],
  tagTypes: ['readCategoryDetail, readAllCategory', 'readAllCategoryNoAuth'],

  endpoints: (builder) => ({
    readCategory: builder.query({
      query: () => {
        return {
          url: `/accessoriesCategory`,
          method: 'GET',
        };
      },
    }),

    readCategoryNoAuth: builder.query({
      query: () => {
        return {
          url: `/accessoriesCategoryNoAuth`,
          method: 'GET',
        };
      },
      providesTags: ['readAllCategoryNoAuth'],
    }),

    deleteCategory: builder.mutation({
      query: (id) => {
        return {
          url: `/accessoriesCategory/${id}`,
          method: 'DELETE',
        };
      },
      invalidatesTags: ['readAllCategory', 'readCategoryDetail', 'readAllCategoryNoAuth'],
    }),

    updateCategory: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/accessoriesCategory/${id}`,
          method: 'PUT',
          body: data,
        };
      },
      invalidatesTags: ['readAllCategory', 'readCategoryDetail', 'readAllCategoryNoAuth'],
    }),

    createCategory: builder.mutation({
      query: (data) => {
        return {
          url: `/accessoriesCategory`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['readAllCategory', 'readCategoryDetail', 'readAllCategoryNoAuth'],
    }),

    readCategoryDetails: builder.query({
      query: (id) => {
        return {
          url: `/accessoriesCategory/${id}`,
          method: 'GET',
        };
      },
      providesTags: ['readCategoryDetail'],
    }),

    readCategoryCustom: builder.query({
      query: (customQuery) => {
        return {
          url: `/accessoriesCategory${customQuery}`,
          method: 'GET',
        };
      },
      providesTags: ['readAllCategory'],
    }),
  }),
});

export const {
  useReadCategoryQuery,
  useReadCategoryNoAuthQuery,
  useDeleteCategoryMutation,
  useUpdateCategoryMutation,
  useCreateCategoryMutation,
  useGetCategoryFromBrandQuery,
  useReadCategoryDetailsQuery,
  useReadCategoryCustomQuery,
} = category;
