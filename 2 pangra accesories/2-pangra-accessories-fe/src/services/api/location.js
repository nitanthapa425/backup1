import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const location = createApi({
  reducerPath: 'location',
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token =
        getState().levelReducer.level === 'superAdmin'
          ? getState().adminAuth.token
          : getState().levelReducer.level === 'customer'
          ? getState().customerAuth.token
          : '';
      if (token) {
        headers.set('authorization', `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['readMunicipalityVdcByDistrict'],

  endpoints: (builder) => ({
    readProvinces: builder.query({
      query: () => {
        return {
          url: `/geo-location/provinces`,
          method: 'GET',
        };
      },
    }),

    readDistrictsByProvinces: builder.query({
      query: (query) => {
        return {
          url: `/geo-location/district/${query}`,
          method: 'GET',
        };
      },
    }),

    readMunicipalityVdcByDistrict: builder.query({
      query: (query) => {
        return {
          url: `/geo-location/municipality/${query}`,
          method: 'GET',
        };
      },
      // provideTags=['readMunicipalityVdcByDistrict']
      providesTags: ['readMunicipalityVdcByDistrict'],
      // keepUnusedDataFor: 0,
    }),

    createMunicipalityVdcByDistrict: builder.mutation({
      query: (data) => {
        return {
          url: `/geo-location/municipality`,
          method: 'POST',
          body: data,
        };
      },
      invalidatesTags: ['readMunicipalityVdcByDistrict'],
    }),
    createLocation: builder.mutation({
      query: (data) => {
        return {
          url: `/client/customerLocation`,
          method: 'POST',
          body: data,
        };
      },
    }),

    readAllDistrict: builder.query({
      query: () => {
        return {
          url: `/geo-location/districts`,
          method: 'GET',
        };
      },
    }),
    getAllLocation: builder.query({
      query: () => {
        return {
          url: `/client/customerLocation`,
          method: 'GET',
        };
      },
    }),
  }),
});

export const {
  useReadProvincesQuery,
  useReadDistrictsByProvincesQuery,
  useReadMunicipalityVdcByDistrictQuery,
  useCreateMunicipalityVdcByDistrictMutation,
  useReadAllDistrictQuery,
  useCreateLocationMutation,
  useGetAllLocationQuery,
} = location;
