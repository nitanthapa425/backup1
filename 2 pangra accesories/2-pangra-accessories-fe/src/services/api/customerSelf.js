import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";
export const customerSelf = createApi({
  reducerPath: "customerSelf",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().customerAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readCustomerDetail", "readAllCustomers"],
  endpoints: (builder) => ({
    customerMyProfile: builder.query({
      query: () => {
        return {
          url: `/client/profile/me`,
          method: "GET",
        };
      },
      providesTags: ["readCustomerDetail"],
    }),

    editCustomerMyProfile: builder.mutation({
      query: (updatePostData) => {
        return {
          url: `/client/profile/me`,
          method: "PUT",
          body: updatePostData,
        };
      },
      invalidatesTags: ["readCustomerDetail"],
    }),
  }),
});

export const { useCustomerMyProfileQuery, useEditCustomerMyProfileMutation } =
  customerSelf;
