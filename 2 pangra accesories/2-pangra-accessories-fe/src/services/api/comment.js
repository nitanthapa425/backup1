import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const comments = createApi({
  reducerPath: "comments",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ['COMMENT', 'REPLY'],

  endpoints: (builder) => ({
    getComments: builder.query({
      query: (query = '') => {
        return {
          url: `/comment${query}`,
          method: "GET",
        };
      },
      providesTags: (result) =>
        result
          ? [...result?.docs?.map(({ _id }) => ({ type: 'COMMENT', id: _id })), 'COMMENT']
          : ['COMMENT'],
    }),
    getCommentReplies: builder.query({
      query: (id) => {
        return {
          url: `/comment/createReplyComment/${id}`,
          method: "GET",
        }
      },
      providesTags: (_, __, arg) => [{ type: 'REPLY', id: arg }],
    }),
    postReplyToComment: builder.mutation({
      query: ({ commentId, data }) => {
        return {
          url: `/comment/createReplyComment/${commentId}`,
          method: "POST",
          body: data
        }
      },
      invalidatesTags: (_, __, arg) => [{ type: 'REPLY', id: arg.commentId }],
    }),
  })
})


export const { useGetCommentsQuery, useGetCommentRepliesQuery, usePostReplyToCommentMutation } = comments
