import axios from "axios";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";
import { store } from "store/store";

export const seller = createApi({
  reducerPath: "seller",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      // const token = getState().customerAuth.token;
      const token =
        getState().levelReducer.level === "superAdmin"
          ? getState().adminAuth.token
          : getState().levelReducer.level === "customer"
          ? getState().customerAuth.token
          : "";
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: [
    "readSellerDetail",
    "readAllSellers",
    "readVehicleSell",
    "getMyBook",
    "getMyBookedListItems",
    "readSingleBookDetails",
    "getComment",
    "getOffer",
    "getHotDeals",
    "getReview",
  ],

  endpoints: (builder) => ({
    SellerPost: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/vehicleSell`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    adminSellerPost: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/vehicleSellByAdmin`,
          method: "POST",
          body: data,
        };
      },
    }),

    readSeller: builder.query({
      query: (query) => {
        return {
          url: `/seller/vehicleSell${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers"],
    }),

    readAllProduct: builder.query({
      query: (query) => {
        return {
          url: `/product/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers"],
    }),
    readVehicleSell: builder.query({
      query: (query) => {
        let url = "";
        if (store.getState().levelReducer.level === "superAdmin") {
          url = `/seller/vehicleSell/${query}`;
        } else if (store.getState().levelReducer.level === "customer") {
          url = `/seller/getSellerVehicleDetail/${query}`;
        }
        return {
          url: url,
          method: "GET",
        };
      },
      providesTags: ["readVehicleSell"],
    }),

    getHotDeals: builder.query({
      query: () => {
        return {
          url: `/product/getAllHotDeals`,
          method: "GET",
        };
      },
      providesTags: ["getHotDeals"],
    }),

    // readSingleSeller: builder.query({
    //   query: (id) => {
    //     return {
    //       url: `/seller/vehicleSell/${id}`,
    //       method: "GET",
    //     };
    //   },
    //   providesTags: ["readSellerDetail"],
    // }),

    deleteSeller: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/vehicleSell/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
        "readVehicleSell",
      ],
    }),

    updateSeller: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;

        return {
          url: `/seller/vehicleSell/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
      ],
    }),

    addToWishList: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/addTowishlist/${vehicleId}`,

          method: "POST",
        };
      },
      invalidatesTags: ["getMyWishList"],
    }),
    getMyWishList: builder.query({
      query: (userId) => {
        return {
          url: `/accessories/wishlist/client`,
          method: "GET",
        };
      },
      providesTags: ["getMyWishList"],
    }),
    deleteMyWishList: builder.mutation({
      query: (productID) => {
        return {
          url: `/accessories/wishList/${productID}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["getMyWishList", "readAllSellers", "getHotDeals"],
    }),

    addToCart: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/addToCart/${vehicleId}`,
          method: "POST",
        };
      },
      invalidatesTags: ["getMyCart", "getMyWishList", "CART"],
    }),

    addToCartFromWishList: builder.mutation({
      query: (body) => {
        return {
          url: `/accessories/addToCartFromWishList`,
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["getMyCart", "getMyWishList"],
    }),
    getMyCart: builder.query({
      query: () => {
        return {
          url: `/cart/user/getClientCart`,
          method: "GET",
        };
      },
      providesTags: ["getMyCart"],
    }),
    deleteMyCart: builder.mutation({
      query: (id) => {
        return {
          url: `/cart/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["getMyCart"],
    }),

    addToBook: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/bookNow/${data.id}`,
          method: "POST",
          body: data.values,
        };
      },
      invalidatesTags: ["getMyBook", "getMyCart", "getMyBookedListItems"],
    }),
    getMyBook: builder.query({
      query: (userId) => {
        return {
          url: `/seller/bookNow`,
          method: "GET",
        };
      },
      providesTags: ["getMyBook"],
    }),
    getMyBookedList: builder.query({
      query: () => {
        return {
          url: `/orderItems/getOrderGroupList`,
          method: "GET",
        };
      },
      providesTags: ["getMyBook"],
    }),
    getMyBookedListItems: builder.query({
      query: (id) => {
        return {
          url: `/orderItems/getOrderItemsByOrderGroupId/${id}`,
          method: "GET",
        };
      },
      providesTags: ["getMyBookedListItems"],
    }),
    getMyDeliveredCancelledList: builder.query({
      query: (query) => {
        return {
          url: `/orderItems/getMyDeliveredCancelledList/${query}`,
          method: "GET",
        };
      },
      providesTags: ["getMyDeliveredCancelledList"],
    }),
    deleteMyBook: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/deleteBookedVehicle/${vehicleId}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["getMyBook", "getMyBookedListItems"],
    }),
    deleteCustomerBookedList: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/OrderItems/${vehicleId}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "getMyBook",
        "getMyBookedListItems",
        "getMyDeliveredCancelledList",
      ],
    }),
    addFeaturedBike: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/add-feature-vehicle/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),
    verified: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/verifyVehicle/${vehicleId}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),
    removeFeaturedBike: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/remove-feature-vehicle/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),
    unVerified: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/unverifySellerVerify/${vehicleId}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),
    expirePost: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/removeAd/${id}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),
    addHotDeal: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/addInHotDeals/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    approved: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/approveSellerVehicle/${vehicleId}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    unApproved: builder.mutation({
      query: (vehicleId) => {
        return {
          url: `/seller/unApproveSellerVehicle/${vehicleId}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    sold: builder.mutation({
      query: (body) => {
        return {
          url: `/seller/mark-as-sold`,
          method: "PATCH",
          body: body,
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    unSold: builder.mutation({
      query: (body) => {
        return {
          url: `/seller/mark-as-unsold`,

          method: "PATCH",
          body: body,
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
      ],
    }),

    meetingSchedule: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/meetingSchedule/${data.bookId}`,

          method: "POST",
          body: data.values,
        };
      },

      invalidatesTags: [
        "getMyBook",
        "readSingleBookDetails",
        "getMyBookedListItems",
      ],
    }),

    updateSingleMeetingSchedule: builder.mutation({
      query: (data) => {
        return {
          url: `/seller/meetingSchedule/${data.bookId}`,
          method: "POST",
          body: data.values,
        };
      },
      invalidatesTags: [
        "getMyBook",
        "readMeetingSchedule",
        "readSingleBookDetails",
        "getMyBookedListItems",
      ],
    }),

    readSingleMeetingSchedule: builder.query({
      query: (bookId) => {
        return {
          url: `/seller/meetingSchedule/${bookId}`,
          method: "GET",
        };
      },
      keepUnusedDataFor: 0,
      providesTags: ["readMeetingSchedule"],
    }),

    deleteSingleMeetingSchedule: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/book/${id}/meetingSchedule`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "readMeetingSchedule",
        "readSingleBookDetails",
        "getMyBook",
        "getMyBookedListItems",
      ],
    }),

    readSingleBookDetails: builder.query({
      query: (id) => {
        return {
          url: `/seller/getBookedVehicleById/${id}`,
          method: "GET",
        };
      },
      // keepUnusedDataFor: 0,
      providesTags: ["readSingleBookDetails"],
    }),

    // For Invalidation in book list.
    readAllBookInv: builder.query({
      query: (query) => {
        return {
          url: `/orderItems/getOrderGroupList/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllBookInv"],
    }),

    readBookInv: builder.query({
      query: (query) => {
        return {
          url: `/orderItems/getOrderGroupList/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readBookInv"],
    }),

    readDeliveredProduct: builder.query({
      query: (query) => {
        return {
          url: `/orderItems/getDeliveredOrderItemsDetails/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readDeliveredProduct"],
    }),

    readAllOrderedItemList: builder.query({
      query: ({ query, id }) => {
        return {
          url: `/orderItems/getOrderItemsByOrderGroupId/${id}/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllBookInv"],
    }),

    readOrderedItemList: builder.query({
      query: ({ query, id }) => {
        return {
          url: `/orderItems/getOrderItemsByOrderGroupId/${id}/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readBookInv"],
    }),

    deleteBookedList: builder.mutation({
      query: (id) => {
        return {
          url: `/OrderItems/adminDelete/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "getMyBook",
        "readAllBookInv",
        "readBookInv",
        "getMyBookedListItems",
      ],
    }),
    AddVehicleInFeatureCustomer: builder.mutation({
      query: (data) => {
        return {
          url: `seller/addFeatureByClient/${data.id}`,
          method: "POST",
          body: data.values,
        };
      },
    }),
    AddToFeatureQueue: builder.mutation({
      query: (id) => {
        return {
          url: `seller/addFeatureByClient/${id}`,
          method: "POST",
          // body: data.values,
        };
      },
    }),
    // comment
    comment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/${id}`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "getComment",
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
      ],
    }),
    getComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/${id}`,
          method: "GET",
        };
      },
      providesTags: ["getComment"],
    }),
    deleteComment: builder.mutation({
      query: (id) => {
        return {
          url: `/comment/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "getComment",
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
      ],
    }),
    updateComment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "getComment",
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
        "getSingleComment",
      ],
    }),
    getSingleComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/single/${id}`,
          method: "GET",
        };
      },
      providesTags: ["getSingleComment"],
    }),
    replyComment: builder.mutation({
      query: (vehicleDate) => {
        const { id, ...data } = vehicleDate;
        return {
          url: `/comment/createReplyComment/${id}`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "getComment",
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
        "getReplyOfComment",
      ],
    }),
    getReply: builder.query({
      query: (id) => {
        return {
          url: `/reply/${id}`,
          method: "GET",
        };
      },
      providesTags: ["getComment"],
    }),
    getSingleReply: builder.query({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/createReplyComment${replyId}`,
          method: "GET",
        };
      },
      providesTags: ["getSingleComment"],
    }),
    allReplyComment: builder.query({
      query: (id) => {
        // const { id, ...data } = vehicleDate;
        return {
          url: `/comment/createReplyComment/${id}`,
          method: "GET",
          // body: data,
        };
      },
    }),

    getReplyOfComment: builder.query({
      query: (id) => {
        return {
          url: `/comment/createReplyComment/${id}`,
          method: "GET",
        };
      },
      providesTags: ["getReplyOfComment"],
    }),
    deleteReplyComment: builder.mutation({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["getReplyOfComment", "getSingleReplyOfComment"],
    }),
    editReplyComment: builder.mutation({
      query: ({ commentId, replyId, body }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: "PUT",
          body: body,
        };
      },
      invalidatesTags: ["getReplyOfComment", "getSingleReplyOfComment"],
    }),

    getSingleReplyOfComment: builder.query({
      query: ({ commentId, replyId }) => {
        return {
          url: `/comment/${commentId}/replyComment/${replyId}`,
          method: "GET",
        };
      },
      providesTags: ["getSingleReplyOfComment"],
    }),
    offerPrice: builder.mutation({
      query: (vehicleData) => {
        const { id, ...data } = vehicleData;
        return {
          url: `seller/offerPrice/${id}`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "getOffer",
        "readSellerDetail",
        "readAllSellers",
        "readVehicleSell",
      ],
    }),

    getOfferDetails: builder.query({
      query: (query) => {
        return {
          url: `/seller/getOfferDetails/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers", "getOffer"],
    }),
    rejectOfferPrice: builder.mutation({
      query: ({ values, id }) => {
        return {
          url: `/seller/rejectOfferPrice/${id}`,
          body: values,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
        "getOffer",
      ],
    }),
    approveOfferPrice: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/approveOfferPrice/${id}`,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
        "getOffer",
      ],
    }),
    finalOffer: builder.mutation({
      query: ({ values, id }) => {
        return {
          url: `/seller/finalOffer/${id}`,
          body: values,

          method: "POST",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
        "getOffer",
      ],
    }),
    deleteOfferPrice: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/offer/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "readSellerDetail",
        "readAllSellers",
        "readSellerAllVehicle",
        "readVehicleSell",
        "getOffer",
      ],
    }),
    getOfferStatus: builder.query({
      query: (id) => {
        return {
          url: `/seller/getClientOffer/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers", "getOffer"],
    }),
    // product
    readSingleProduct: builder.query({
      query: (id) => {
        return {
          url: `/product/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers"],
    }),
    readSingleRecommendedProduct: builder.query({
      query: () => {
        return {
          url: `/product/user/recommendations`,
          method: "GET",
        };
      },
      providesTags: ["readAllSellers"],
    }),
    addToCartProduct: builder.mutation({
      query: ({ productId, body }) => {
        return {
          url: `/cart/${productId}`,
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["getMyCart", "getMyWishList"],
    }),
    addToWishListProduct: builder.mutation({
      query: (productId) => {
        return {
          url: `/accessories/wishlist/${productId}`,

          method: "POST",
        };
      },
      invalidatesTags: ["getMyWishList", "readAllSellers", "getHotDeals"],
    }),
    addToBuyingListProduct: builder.mutation({
      query: (productId) => {
        return {
          url: `/buying/${productId}`,

          method: "POST",
        };
      },
      invalidatesTags: ["getMyBook", "getMyCart", "getMyBookedListItems"],
    }),

    buyAccessory: builder.mutation({
      query: (body) => {
        return {
          url: `/orderItems`,

          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["getMyBook", "getMyCart", "getMyBookedListItems"],
    }),

    readCategoryNoAuthWithLimit: builder.query({
      // query: (query) => {
      query: (query = "") => {
        return {
          // url: `//brandAccessoriesNoAuth${query}`,
          url: `/accessoriesCategoryNoAuthLimit/${query}`,
          // url: `/accessoriesCategoryNoAuthLimit`,
          method: "GET",
        };
      },
    }),
    readCategoryAndSubCategoryNested: builder.query({
      // query: (query) => {
      query: () => {
        return {
          // url: `//brandAccessoriesNoAuth${query}`,
          url: `/brandAccessories/getNestedCategoryAndSubCategory`,
          // url: `/accessoriesCategoryNoAuthLimit`,
          method: "GET",
        };
      },
    }),

    readSubCategoryNoAuthWithLimit: builder.query({
      // query: (query) => {
      query: (query = "") => {
        return {
          // url: `//brandAccessoriesNoAuth${query}`,
          url: `/accessoriesSubCategoryNoAuthLimit/${query}`,
          // url: `/accessoriesCategoryNoAuthLimit`,
          method: "GET",
        };
      },
    }),

    // {{URL}}/OrderItems/changeOrderStatus/6253c5ecc0b2fb2cd46dd5b5?orderStatus=Processing

    createOrderStatus: builder.mutation({
      query: ({ id, query, body }) => {
        return {
          url: `/orderItems/changeOrderStatus/${id}/${query}`,
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: [
        "getMyBook",
        "readAllBookInv",
        "readBookInv",
        "readSingleOrderDetails",
        "getMyBookedListItems",
      ],
    }),

    createAddStock: builder.mutation({
      query: ({ id, body }) => {
        return {
          url: `/OrderItems/updateOrderIfNotInStuck/${id}`,
          method: "PUT",
          body: body,
        };
      },
      invalidatesTags: [
        "getMyBook",
        "readAllBookInv",
        "readBookInv",
        "readSingleOrderDetails",
        "getMyBookedListItems",
      ],
    }),

    closeAccount: builder.mutation({
      query: ({ id, body }) => {
        return {
          url: `/orderItems/closeAccountAfterDelivered/${id}`,
          method: "POST",
          body: body,
        };
      },
      invalidatesTags: ["readDeliveredProduct"],
    }),

    deliveryPerson: builder.query({
      query: (id) => {
        return {
          url: `/delivery/companyPersonByCompanyId/${id}`,
          method: "GET",
        };
      },
    }),

    deliveryCompany: builder.query({
      query: () => {
        return {
          url: `/company`,
          method: "GET",
        };
      },
      // keepUnusedDataFor: 0,
    }),
    readSingleOrderDetails: builder.query({
      query: (id) => {
        return {
          url: `/orderItems/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readSingleOrderDetails"],
    }),
    ratingReviewProduct: builder.mutation({
      query: (variable) => {
        return {
          url: `/rating/review/${variable.id}`,
          method: "POST",
          body: variable.values,
        };
      },
      invalidatesTags: [
        "getReview",
        "readRatingStatusReview",
        "readAllRatingReview",
        "readRatingReview",
      ],
    }),
    editRatingReviewProduct: builder.mutation({
      query: (variable) => {
        return {
          url: `/rating/${variable.id}`,
          method: "PUT",
          body: variable.body,
        };
      },
      invalidatesTags: [
        "getReview",
        "readRatingStatusReview",
        "readAllRatingReview",
        "readRatingReview",
      ],
    }),
    readRatingReview: builder.query({
      query: (productId) => {
        return {
          url: `/rating/productRatings/${productId}`,
          method: "GET",
        };
      },
      providesTags: ["readRatingReview"],
    }),

    deleteRatingReviewProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/rating/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "getReview",
        "readRatingStatusReview",
        "readAllRatingReview",
        "readRatingReview",
      ],
    }),
    // admin
    readAllRatingReview: builder.query({
      query: (query) => {
        return {
          url: `/rating/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllRatingReview"],
    }),
    readRatingStatusReview: builder.query({
      query: () => {
        return {
          url: `/rating/review`,
          method: "GET",
        };
      },
      providesTags: ["readRatingStatusReview"],
    }),
    approveRatingReviewProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/rating/approve/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: [
        "getReview",
        "readRatingStatusReview",
        "readAllRatingReview",
        "readRatingReview",
      ],
    }),
    disapproveRatingReviewProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/rating/disapprove/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: [
        "getReview",
        "readRatingStatusReview",
        "readAllRatingReview",
        "readRatingReview",
      ],
    }),
  }),
});

export const {
  useSellerPostMutation,
  useReadSellerQuery,
  useReadSingleSellerQuery,
  useDeleteSellerMutation,
  useUpdateSellerMutation,
  useGetHotDealsQuery,
  useAddToCartMutation,
  useGetMyCartQuery,
  useAddToBookMutation,
  useGetMyBookQuery,
  useAddToWishListMutation,
  useGetMyWishListQuery,
  useDeleteMyWishListMutation,
  useDeleteMyCartMutation,
  useDeleteMyBookMutation,
  useAddToCartFromWishListMutation,
  useReadVehicleSellQuery,
  useAddFeaturedBikeMutation,
  useRemoveFeaturedBikeMutation,
  useAddHotDealMutation,
  useVerifiedMutation,
  useUnVerifiedMutation,
  useApprovedMutation,
  useUnApprovedMutation,
  useSoldMutation,
  useUnSoldMutation,
  useMeetingScheduleMutation,
  useReadSingleMeetingScheduleQuery,
  useReadSingleBookDetailsQuery,
  useUpdateSingleMeetingScheduleMutation,
  useDeleteSingleMeetingScheduleMutation,
  useExpirePostMutation,
  useReadAllBookInvQuery,
  useReadBookInvQuery,
  useReadDeliveredProductQuery,
  useDeleteBookedListMutation,
  useAddVehicleInFeatureCustomerMutation,
  useAddToFeatureQueueMutation,
  useCommentMutation,
  useReplyCommentMutation,
  useGetCommentQuery,
  useDeleteCommentMutation,
  useUpdateCommentMutation,
  useGetSingleCommentQuery,
  useAdminSellerPostMutation,
  useGetReplyQuery,
  useGetSingleReplyQuery,
  useAllReplyCommentQuery,
  useGetReplyOfCommentQuery,
  useDeleteReplyCommentMutation,
  useEditReplyCommentMutation,
  useGetSingleReplyOfCommentQuery,
  useOfferPriceMutation,
  useGetOfferDetailsQuery,
  useRejectOfferPriceMutation,
  useApproveOfferPriceMutation,
  useFinalOfferMutation,
  useDeleteOfferPriceMutation,
  useGetOfferStatusQuery,
  useReadAllProductQuery,
  useReadSingleProductQuery,
  useAddToCartProductMutation,
  useAddToWishListProductMutation,
  useAddToBuyingListProductMutation,
  useBuyAccessoryMutation,
  useReadSingleRecommendedProductQuery,
  useReadCategoryNoAuthWithLimitQuery,
  useReadSubCategoryNoAuthWithLimitQuery,
  useGetMyBookedListQuery,
  useGetMyBookedListItemsQuery,
  useGetMyDeliveredCancelledListQuery,
  useDeleteCustomerBookedListMutation,
  useCreateOrderStatusMutation,
  useDeliveryPersonQuery,
  useReadSingleOrderDetailsQuery,
  useDeliveryCompanyQuery,
  useReadRatingReviewQuery,
  useReadAllRatingReviewQuery,
  useReadRatingStatusReviewQuery,
  useRatingReviewProductMutation,
  useEditRatingReviewProductMutation,
  useDeleteRatingReviewProductMutation,
  useApproveRatingReviewProductMutation,
  useDisapproveRatingReviewProductMutation,
  useCreateAddStockMutation,
  useReadAllOrderedItemListQuery,
  useReadOrderedItemListQuery,
  useReadCategoryAndSubCategoryNestedQuery,
  useCloseAccountMutation,
} = seller;

export const searchCategories = (searchTerm) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${baseUrl}/accessoriesCategoryNoAuthLimit/?select=categoryName&limit=20&sortBy=categoryName&sortOrder=1&search="categoryName":"${searchTerm}"`
      )
      .then((res) => {
        const data = res?.data?.docs.map((cateogry) => ({
          label: cateogry?.categoryName,
          value: cateogry.categoryName,
        }));
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
export const searchSubCategories = (searchTerm) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${baseUrl}/accessoriesSubCategoryNoAuthLimit/?select=categoryName&limit=20&sortBy=categoryName&sortOrder=1&search="categoryName":"${searchTerm}"`
      )
      .then((res) => {
        const data = res?.data?.docs.map((subCategory) => ({
          label: subCategory?.categoryName,
          value: subCategory.categoryName,
        }));
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
