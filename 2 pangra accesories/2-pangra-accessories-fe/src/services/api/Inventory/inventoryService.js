import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const inventory = createApi({
  reducerPath: "inventory",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ["readCompanyDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readInventory: builder.query({
      query: (query) => {
        return {
          url: `/inventory/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllInventory"],
    }),
    updateOrderQuantity: builder.mutation({
      query: ({ id, body }) => {
        return {
          url: `/inventory/${id}`,
          method: "PUT",
          body: body,
        };
      },

      invalidatesTags: ["readAllInventory"],
    }),
  }),
});

export const { useReadInventoryQuery, useUpdateOrderQuantityMutation } =
  inventory;
