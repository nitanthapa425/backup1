import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'config';

export const productDraft = createApi({
    reducerPath: 'productDraft',
    // keepUnusedDataFor: 0,
    baseQuery: fetchBaseQuery({
        baseUrl,
        prepareHeaders: (headers, { getState }) => {
            const token = getState().adminAuth.token;
            if (token) {
                headers.set('authorization', `Bearer ${token}`);
            }
            return headers;
        },
    }),
    tagTypes: ['readDraftedProduct', 'readAllDraftProducts'],
    endpoints: (builder) => ({
        productSaveAsDraft: builder.mutation({
            query: (data) => {
                return {
                    url: `/draftProduct`,
                    method: 'POST',
                    body: data,
                };
            },
            invalidatesTags: ['readDraftedProduct', 'readAllDraftProducts'],
        }),

        readDraftProducts: builder.query({
            query: (customQuery) => {
                return {
                    url: `/draftProduct/${customQuery}`,
                    method: 'GET',
                };
            },
            providesTags: ['readAllDraftProducts'],
        }),

        deleteDraftProduct: builder.mutation({
            query: (id) => {
                return {
                    url: `/draftProduct/${id}`,
                    method: 'DELETE',
                };
            },
            invalidatesTags: ['readDraftedProduct', 'readAllDraftProducts'],
        }),

        updateDraftProduct: builder.mutation({
            query: ({ id, ...data }) => {
                return {
                    url: `/draftProduct/${id}`,
                    method: 'PUT',
                    body: data,
                };
            },
            invalidatesTags: ['readDraftedProduct', 'readAllDraftProducts'],
        }),

        getDraftedProduct: builder.query({
            query: (id) => {
                return {
                    url: `/draftProduct/${id}`,
                    method: 'GET',
                };
            },
            providesTags: ['readDraftedProduct'],
        }),

        finishDraftProduct: builder.mutation({
            query: ({ id, ...data }) => {
                return {
                    url: `/draftProduct/finishDraft/${id}`,
                    method: 'POST',
                    body: data,
                };
            },
            invalidatesTags: ['readDraftedProduct', 'readAllDraftProducts'],
        }),

        // getDraftedVehicles: builder.query({
        //     query: () => {
        //         return {
        //             url: `/getDraftByUserId`,
        //             method: 'GET',
        //         };
        //     },
        //     providesTags: ['draftedVehicles'],
        // }),
    }),
});

export const {
    useReadDraftProductsQuery,
    useProductSaveAsDraftMutation,
    useDeleteDraftProductMutation,
    useUpdateDraftProductMutation,
    useGetDraftedProductQuery,
    // useReadDraftVehicleQuery,
    useFinishDraftProductMutation,
} = productDraft;
