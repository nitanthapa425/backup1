import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const product = createApi({
  reducerPath: "product",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  // tagTypes: ['AllModel'],
  tagTypes: ["readProductDetail, readAllProduct"],

  endpoints: (builder) => ({
    readProduct: builder.query({
      query: () => {
        return {
          url: `/product`,
          method: "GET",
        };
      },
    }),

    readProductNoAuth: builder.query({
      query: () => {
        return {
          url: `/productNoAuth`,
          method: "GET",
        };
      },
      // providesTags: ['readAllBrandsNoAuth'],
    }),

    deleteProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/product/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),

    updateProduct: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/product/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),

    createProduct: builder.mutation({
      query: (data) => {
        return {
          url: `/product`,
          method: "POST",
          body: data,
        };
      },
    }),

    readProductDetails: builder.query({
      query: (id) => {
        return {
          url: `/product/${id}`,
          method: "GET",
        };
      },
      providesTags: ["readProductDetail"],
    }),

    readProductCustom: builder.query({
      query: (customQuery) => {
        return {
          url: `/product${customQuery}`,
          method: "GET",
        };
      },
      providesTags: ["readAllProduct"],
    }),
    addHotDealProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/product/addToHotDeals/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),
    addFeaturedProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/product/addToFeatured/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),
    removeFeaturedProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/product/removeFromFeatured/${id}`,

          method: "POST",
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),
    addProductToFlashSale: builder.mutation({
      query: (body) => {
        return {
          url: `accessories/flash-sale/`,
          method: "POST",
          body,
        };
      },
      invalidatesTags: ["readAllProduct", "readProductDetail"],
    }),

    createTag: builder.mutation({
      query: (data) => {
        return {
          url: `/tags`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readTags"],
    }),

    readTags: builder.query({
      query: () => {
        return {
          url: `/tags`,
          method: "GET",
        };
      },
      providesTags: ["readTags"],
    }),
    createColors: builder.mutation({
      query: (data) => {
        return {
          url: `/color`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readColors"],
    }),

    readColors: builder.query({
      query: () => {
        return {
          url: `/color`,
          method: "GET",
        };
      },
      providesTags: ["readColors"],
    }),

    addSizeChart: builder.mutation({
      query: (data) => {
        return {
          url: `/sizechart`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["getSizeChart"],
    }),

    getSizeChart: builder.query({
      query: () => {
        return {
          url: `/sizechart`,
          method: "GET",
        };
      },

      providesTags: ["getSizeChart"],
    }),
    // Order Item
    createOrderItem: builder.mutation({
      query: (body) => {
        return {
          url: `/orderItems/order-item-admin`,
          method: "POST",
          body,
        };
      },
      invalidatesTags: ["getOrderItem"],
    }),
  }),
});
export const {
  useReadProductQuery,
  useReadProductNoAuthQuery,
  useDeleteProductMutation,
  useUpdateProductMutation,
  useCreateProductMutation,
  useReadProductDetailsQuery,
  useReadProductCustomQuery,
  useAddHotDealProductMutation,
  useAddFeaturedProductMutation,
  useRemoveFeaturedProductMutation,
  useCreateTagMutation,
  useReadTagsQuery,
  useCreateColorsMutation,
  useReadColorsQuery,
  useAddSizeChartMutation,
  useGetSizeChartQuery,
  useAddProductToFlashSaleMutation,
  useCreateOrderItemMutation,
} = product;
