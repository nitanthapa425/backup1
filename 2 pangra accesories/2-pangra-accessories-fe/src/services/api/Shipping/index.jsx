import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const shipping = createApi({
  reducerPath: "shipping",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: [
    "readShippingDetail",
    "readAllShipping",
    "readAllShippings",
    "readAllShippingsNoAuth",
  ],

  endpoints: (builder) => ({
    readShipping: builder.query({
      query: () => {
        return {
          url: `/shippingCharge`,
          method: "GET",
        };
      },
      providesTags: ["readAllShippings"],
    }),

    // readModelNoAuth: builder.query({
    //   query: () => {
    //     return {
    //       url: `/model`,
    //       method: "GET",
    //     };
    //   },
    //   providesTags: ["readAllShippingsNoAuth"],
    // }),

    deleteShipping: builder.mutation({
      query: (id) => {
        return {
          url: `/shippingCharge/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "readShippingDetail",
        "readAllShipping",
        "readAllShippings",
      ],
    }),

    updateShipping: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/shippingCharge/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "readShippingDetail",
        "readAllShipping",
        "readAllShippings",
        "readAllShippingsNoAuth",
      ],
    }),

    createShipping: builder.mutation({
      query: (data) => {
        return {
          url: `/shippingCharge`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "readShippingDetail",
        "readAllShipping",
        "readAllShippings",
      ],
    }),

    readShippingDetails: builder.query({
      query: (ModelId) => {
        return {
          url: `/shippingCharge/${ModelId}`,
          method: "GET",
        };
      },
      providesTags: ["readShippingDetail"],
    }),

    readShippingList: builder.query({
      query: (query) => {
        return {
          url: `/shippingCharge/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllShipping"],
    }),
  }),
});

export const {
  useReadShippingQuery,
  //   useReadModelNoAuthQuery,
  useDeleteShippingMutation,
  useUpdateShippingMutation,
  useCreateShippingMutation,
  useReadShippingDetailsQuery,
  useReadShippingListQuery,
} = shipping;
