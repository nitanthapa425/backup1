import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const adminCart = createApi({
  reducerPath: "adminCart",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readAllWishList", "readAllCart", "readAllBook", "CART"],
  endpoints: (builder) => ({
    readAllWishList: builder.query({
      query: (query) => {
        return {
          url: `/accessories/wishlist/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllWishList"],
    }),

    readAllCart: builder.query({
      query: (query) => {
        return {
          url: `/cart${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllCart"],
    }),

    readAllBook: builder.query({
      query: (query) => {
        return {
          url: `/seller/bookNow/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllBook"],
    }),

    deleteWishList: builder.mutation({
      query: (id) => {
        return {
          url: `/accessories/wishlist/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllWishList"],
    }),

    deleteCart: builder.mutation({
      query: (id) => {
        return {
          url: `/cart/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllCart"],
    }),
    deleteCartAdmin: builder.mutation({
      query: (id) => {
        return {
          url: `/cart/user/cartAdmin/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllCart"],
    }),

    deleteBookedListAdmin: builder.mutation({
      query: (id) => {
        return {
          url: `/seller/bookedVehicleByAdmin/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readAllBook"],
    }),
    addToCartAdmin: builder.mutation({
      query: ({ productId, data }) => {
        return {
          url: `/cart/${productId}`,
          method: "POST",
          body: data,
        };
      },
      // invalidatesTags: (_, __, arg) => [{ type: "CART", id: arg }],
      invalidatesTags: ["getMyCartAdmin"],
    }),
    getMyCartAdmin: builder.query({
      query: () => {
        return {
          url: `/cart/user/getClientCart`,
          method: "GET",
        };
      },
      // providesTags: (result, error, arg) =>
      //   result
      //     ? [...result.map(({ id }) => ({ type: "CART", id })), "CART"]
      //     : ["CART"],

      providesTags: ["getMyCartAdmin"],
    }),
    deleteMyCartAdmin: builder.mutation({
      query: (id) => {
        return {
          url: `/cart/${id}`,
          method: "DELETE",
        };
      },
      // invalidatesTags: (_, __, arg) => [{ type: "CART", id: arg }],
      invalidatesTags: ["getMyCartAdmin"],
    }),
  }),
});

export const {
  useReadAllWishListQuery,
  useReadAllCartQuery,
  useReadAllBookQuery,
  useDeleteWishListMutation,
  useDeleteCartMutation,
  useDeleteCartAdminMutation,
  useDeleteBookedListAdminMutation,
  useAddToCartAdminMutation,
  useGetMyCartAdminQuery,
  useDeleteMyCartAdminMutation,
} = adminCart;
