import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import axios from "axios";
import { baseUrl } from "config";

export const brand = createApi({
  reducerPath: "brand",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["readBrandDetail", "readAllBrand", "readAllBrands"],

  endpoints: (builder) => ({
    readBrand: builder.query({
      query: () => {
        return {
          url: `/brandVehicle`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrands"],
    }),
    readBrandNoAuth: builder.query({
      query: () => {
        return {
          url: `/brandAccessoriesNoAuthLimit`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrandsNoAuth"],
    }),

    deleteBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/brandAccessories/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: [
        "readBrandDetail",
        "readAllBrand",
        "readAllBrands",
        "readAllBrandsNoAuth",
      ],
    }),

    updateBrand: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/brandVehicle/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "readBrandDetail",
        "readAllBrand",
        "readAllBrands",
        "readAllBrandsNoAuth",
      ],
    }),

    createBrand: builder.mutation({
      query: (data) => {
        return {
          url: `/brandVehicle`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: [
        "readBrandDetail",
        "readAllBrand",
        "readAllBrands",
        "readAllBrandsNoAuth",
      ],
    }),

    readBrandDetails: builder.query({
      query: (BrandId) => {
        return {
          url: `/brandVehicle/${BrandId}`,
          method: "GET",
        };
      },
      providesTags: ["readBrandDetail"],
    }),

    readBrandList: builder.query({
      query: (query) => {
        return {
          url: `/brandAccessories/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllBrand"],
    }),

    // Adding brand to homepage/featured brands
    addFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/addBrandInHomePage/${id}`,
          method: "POST",
        };
      },
      invalidatesTags: ["readAllBrand"],
    }),

    readFeaturedBrandsNoAuth: builder.query({
      query: (query) => {
        return {
          url: `/brandAccessoriesNoAuth/${query}`,
          method: "GET",
        };
      },
    }),

    removeFeaturedBrand: builder.mutation({
      query: (id) => {
        return {
          url: `/removeBrandInHomePage/${id}`,
          // Method is still POST not DELETE. Made by our Backend Team.
          method: "POST",
        };
      },
      invalidatesTags: ["readAllBrand"],
    }),

    readBrandNoAuthWithLimit: builder.query({
      // query: (query) => {
      query: (query) => {
        return {
          url: `/brandAccessoriesNoAuthLimit/${query}`,
          // url: `/brandAccessoriesNoAuthLimit`,
          method: "GET",
        };
      },
    }),

    readBrandAndModelNested: builder.query({
      // query: (query) => {
      query: () => {
        return {
          url: `/brandAccessories/getNestedBrandAndModel`,
          // url: `/brandAccessoriesNoAuthLimit`,
          method: "GET",
        };
      },
    }),

    readSubCategoryNoAuthWithLimit: builder.query({
      // query: (query) => {
      query: (id) => {
        return {
          // url: `//brandAccessoriesNoAuth${query}`,
          url: `/subCategoryByCategoryId/${id}`,
          method: "GET",
        };
      },
    }),
  }),
});

export const {
  useReadBrandQuery,
  useReadBrandNoAuthQuery,
  useDeleteBrandMutation,
  useUpdateBrandMutation,
  useCreateBrandMutation,
  useReadBrandDetailsQuery,
  useReadBrandListQuery,
  useAddFeaturedBrandMutation,
  useReadFeaturedBrandsNoAuthQuery,
  useRemoveFeaturedBrandMutation,
  useReadBrandNoAuthWithLimitQuery,
  useReadBrandAndModelNestedQuery,
  useReadSubCategoryNoAuthWithLimitQuery,
} = brand;

export const searchBrands = (searchTerm, showIdAsValue = false) => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${baseUrl}/brandAccessoriesNoAuthLimit?select=brandName,id&limit=5&sortBy=brandName&sortOrder=1&search="brandName":"${searchTerm}"`
      )
      .then((res) => {
        const data = res?.data?.docs.map((brand) => ({
          label: brand?.brandName,
          value: showIdAsValue ? brand.id : brand.brandName,
        }));
        resolve(data);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
