import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "config";

export const model = createApi({
  reducerPath: "model",
  baseQuery: fetchBaseQuery({
    baseUrl,
    prepareHeaders: (headers, { getState }) => {
      const token = getState().adminAuth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  tagTypes: [
    "readModelDetail",
    "readAllModel",
    "readAllModels",
    "readAllModelsNoAuth",
  ],

  endpoints: (builder) => ({
    readModel: builder.query({
      query: () => {
        return {
          url: `/model`,
          method: "GET",
        };
      },
      providesTags: ["readAllModels"],
    }),

    readModelNoAuth: builder.query({
      query: () => {
        return {
          url: `/model`,
          method: "GET",
        };
      },
      providesTags: ["readAllModelsNoAuth"],
    }),

    deleteModel: builder.mutation({
      query: (id) => {
        return {
          url: `/model/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags: ["readModelDetail", "readAllModel", "readAllModels"],
    }),

    updateModel: builder.mutation({
      query: (updatePostData) => {
        const { id, ...data } = updatePostData;
        return {
          url: `/model/${id}`,
          method: "PUT",
          body: data,
        };
      },
      invalidatesTags: [
        "readModelDetail",
        "readAllModel",
        "readAllModels",
        "readAllModelsNoAuth",
      ],
    }),

    createModel: builder.mutation({
      query: (data) => {
        return {
          url: `/model`,
          method: "POST",
          body: data,
        };
      },
      invalidatesTags: ["readModelDetail", "readAllModel", "readAllModels"],
    }),

    readModelDetails: builder.query({
      query: (ModelId) => {
        return {
          url: `/model/${ModelId}`,
          method: "GET",
        };
      },
      providesTags: ["readModelDetail"],
    }),

    readModelList: builder.query({
      query: (query) => {
        return {
          url: `/model/${query}`,
          method: "GET",
        };
      },
      providesTags: ["readAllModel"],
    }),
    readBrandModelList: builder.query({
      query: (brandId) => {
        return {
          url: `/model/data/${brandId}`,
          method: "GET",
        };
      },
      providesTags: ["readAllModel"],
    }),
  }),
});

export const {
  useReadModelQuery,
  useReadModelNoAuthQuery,
  useDeleteModelMutation,
  useUpdateModelMutation,
  useCreateModelMutation,
  useReadModelDetailsQuery,
  useReadModelListQuery,
  useReadBrandModelListQuery,
} = model;
