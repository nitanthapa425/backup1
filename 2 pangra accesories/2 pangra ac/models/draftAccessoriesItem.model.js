/**
 * Copyright (C) Dui Pangra
 */

/**
 * the Vehicle Detail Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

// const { templateSettings } = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
// const { validateMaxTenNum, validateFloatNumLess100 } = require('../utils/helper');

// const LocationSchema = new Schema(
//   {
//     country: {
//       type: String,
//       default: 'Nepal',
//       required: [true, 'Country is required']
//     },
//     combineLocation: {
//       type: String
//     },
//     nearByLocation: {
//       type: String
//     },
//     status: {
//       type: String,
//       enum: ['Location']
//     },
//     geoLocation: {
//       type: {
//         type: String, // Don't do `{ location: { type: String } }`
//         enum: ['Point'] // 'location.type' must be 'Point'
//       },
//       coordinates: {
//         type: [Number]
//       }
//     }
//   },
//   { id: false }
// );

const productImageSchema = new Schema(
  {
    imageUrl: {
      type: String,
      required: [true, 'At least a image is required']
    },
    publicKey: {
      type: String,
      required: [true, 'Public key is required']
    }
  },
  { _id: false }
);

const productImagesSchema = new Schema(
  {
    imageUrl: {
      type: String,
      required: [true, 'At least a image is required']
    },
    publicKey: {
      type: String,
      required: [true, 'Public key is required']
    }
  },
  { _id: false }
);

const draftAccessoriesItemSchema = new Schema(
  {
    brandId: {
      type: Schema.Types.ObjectId,
      ref: 'Brand',
      required: true
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: true
    },
    subCategoryId: {
      type: Schema.Types.ObjectId,
      ref: 'SubCategory',
      required: true
    },
    title: {
      type: String
    },
    description: {
      type: String
    },
    color: {
      type: String
    },
    costPrice: {
      type: String
    },
    // Discount Percent => string (automatically generate from calculation)
    discountPrice: {
      type: String
    },
    weight: {
      type: String // save weight in gram
    },
    dimension: {
      length: {
        type: String
      },
      breadth: {
        type: String
      },
      height: {
        type: String
      }
    },
    dimensionUnit: {
      type: String,
      enum: ['mm', 'cm', 'm']
    },
    sizeCalculation: {
      type: String,
      enum: ['Numbers', 'Letters']
    },
    sizeValue: [
      {
        type: String
      }
    ],
    SKU: {
      type: String,
      required: true
    },
    quantity: {
      type: String
    },
    hasWarranty: {
      type: Boolean,
      default: false
    },
    // If warranty is true than only validate warranty
    warrantyTime: {
      type: String
    },
    isReturnable: {
      type: Boolean,
      default: false
    },
    // If return is true than validate
    returnableTime: {
      type: String
    },
    onSale: {
      type: Boolean,
      default: false
    },
    tags: [
      {
        type: String
      }
    ],
    productImage: productImageSchema,
    productImages: productImagesSchema,
    hasInFeatured: {
      type: Boolean,
      default: false
    },
    hasInHotDeal: {
      type: Boolean,
      default: false
    },
    isForFlashSale: {
      type: Boolean,
      default: false
    },
    status: {
      type: Boolean,
      default: true
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedAt: {
      type: Date
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

// /* Make first letter to uppercase of back brake system */
// TechnicalSchema.pre('save', function (next) {
//   this.backBrakeSystem = this.backBrakeSystem.trim()[0].toUpperCase() + this.backBrakeSystem.slice(1).toLowerCase();
//   next();
// });

// /* Make first letter to uppercase of front brake system */
// VehicleSellSchema.pre('save', function (next) {
//   this.color = this.color.trim()[0].toUpperCase() + this.color.slice(1).toLowerCase();
//   next();
// });

draftAccessoriesItemSchema.plugin(mongoosePaginate);
module.exports = {
  draftAccessoriesItemSchema
};
