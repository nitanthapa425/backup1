/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */
const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const buyNowSchema = new Schema(
  {
    accessoriesId: {
      type: Schema.Types.ObjectId,
      ref: 'Accessories',
      required: true
    },
    productName: {
      type: String
    },
    customerName: {
      type: String
    },
    customerMobile: {
      type: String
    },
    productImage: [
      {
        imageUrl: {
          type: String,
          required: [true, 'At least a image is required']
        },
        publicKey: {
          type: String,
          required: [true, 'Public key is required']
        }
      },
      { id: false }
    ],
    price: {
      type: String
    },
    color: {
      type: String
    },
    size: {
      type: String
    },
    totalPrice: {
      type: String
    },
    shippingAddress: {
      type: Schema.Types.ObjectId,
      ref: 'ShippingLocation'
    },
    shippingFee: {
      type: String,
      default: '50'
    },
    deliveryStart: {
      type: Date
    },
    deliveryEnd: {
      type: Date
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    numViews: {
      type: Number
    },
    status: {
      type: Boolean
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

buyNowSchema.plugin(mongoosePaginate);
module.exports = {
  buyNowSchema
};
