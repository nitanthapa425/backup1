/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Municipality Schema
 * @author      SUSAN DHAKAL
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const municipalitySchema = new Schema(
  {
    districtName: {
      type: String,
      required: true,
      trim: true
    },
    municipalityName: {
      type: String,
      required: true,
      trim: true
    },
    status: {
      type: String,
      default: 'Active'
    },
    createdBy: {
      type: String
    },
    updatedBy: {
      type: String
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

const toleSchema = new Schema(
  {
    districtName: {
      type: String,
      required: true,
      trim: true
    },
    municipalityId: {
      type: Schema.Types.ObjectId,
      ref: 'GeoLocationSchemaModel',
      required: true,
      trim: true
    },
    municipalityName: {
      type: String,
      trim: true
    },
    wardNumber: {
      type: String,
      trim: true
    },
    toleName: {
      type: String,
      trim: true
    },
    fullAddress: {
      type: String,
      trim: true
    },
    status: {
      type: String,
      default: 'Active'
    },
    createdBy: {
      type: String
    },
    updatedBy: {
      type: String
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

toleSchema.plugin(mongoosePaginate);
municipalitySchema.plugin(mongoosePaginate);

module.exports = {
  municipalitySchema,
  toleSchema
};
