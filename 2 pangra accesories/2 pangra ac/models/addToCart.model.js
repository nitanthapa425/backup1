/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Brand Vehicle Schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const cartSchema = new Schema(
  {
    accessoriesId: {
      type: Schema.Types.ObjectId,
      ref: 'Accessories'
    },
    productName: {
      type: String,
    },
    costPrice: {
      type: Number
    },
    discountPrice: {
      type: Number
    },
    discountPercentage: {
      type: String
    },
    numViews: {
      type: Number
    },
    combinelocation: {
      type: String
    },
    status: {
      type: Boolean
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    color: [{
      type: String
    }],
    sizeValue: [{
      type: String
    }],
    quantity: [{
      type: String
    }],
    colors: {
      type: String
    },
    sizeValues: {
      type: String
    },
    quantitys: {
      type: String
    }
  },

  {
    timestamps: true,
  }
);

cartSchema.plugin(mongoosePaginate);
module.exports = {
  cartSchema
};
