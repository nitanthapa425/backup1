const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ratingAndReviewSchema = new Schema({
  accessoriesId: {
    type: Schema.Types.ObjectId,
    ref: 'Accessories',
  },
  ratingNumber: {
    type: String,
    enum: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
  },
  ratingComment: {
    type: String,
  },
  status: {
    type: Boolean,
    default: true
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'Client'
  }
});
ratingAndReviewSchema.plugin(mongoosePaginate);

module.exports = {
  ratingAndReviewSchema
};
