/**
 * Copyright (C) Two Pangra
 */

/**
 * the user  model
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const LocationSchema = new Schema({
  country: {
    type: String,
    default: 'Nepal'
  },
  combineLocation: {
    type: String
  },
  exactLocation: {
    type: String
  },
  nearByLocation: {
    type: String
  },
  status: {
    type: String,
    default: 'active'
  }
});
const ProfileImageSchema = new Schema(
  {
    imageUrl: { type: String },
    publicKey: { type: String }
  },
  { _id: false }
);

const ClientSchema = new Schema(
  {
    firstName: { type: String },
    middleName: { type: String },
    lastName: { type: String },
    fullName: { type: String },
    email: {
      type: String,
      lowercase: true,
      trim: true
    },
    password: { type: String, private: true },

    profileImagePath: ProfileImageSchema,
    mobile: {
      type: String,
      unique: true
    },
    additionalMobile: {
      type: String
    },
    adddedByAdmin: {
      type: Boolean,
      default: false
    },
    gender: { type: String, enum: ['Male', 'Female', 'Other'] },
    dob: { type: Date },
    isActive: { type: Boolean, default: false },
    accessLevel: { type: String, default: 'CLIENT' },
    emailVerified: { type: Boolean, default: false },
    phoneVerified: { type: Boolean, default: false },
    receiveNewsletter: { type: Boolean, default: false },
    forgotPasswordToken: { type: String },
    lastLoginAt: { type: Date },
    verificationToken: { type: String },
    accessToken: { type: String },
    location: LocationSchema,
    customerDescription: { type: String }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;

        // Keep only necessary details for GET requests
        delete ret.passwordHash;
        delete ret.verificationToken;
        delete ret.forgotPasswordToken;
        delete ret.accessToken;
        return ret;
      }
    }
  }
);

ClientSchema.plugin(mongoosePaginate);
module.exports = {
  ClientSchema
};
