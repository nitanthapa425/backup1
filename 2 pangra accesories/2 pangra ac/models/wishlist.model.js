const mongoosePaginate = require('mongoose-paginate-v2');
const { Schema } = require('mongoose');

const wishListModel = new Schema(
  {
    accessoriesId: {
      type: Schema.Types.ObjectId,
      ref: 'Accessories'
    },
    productName: {
      type: String,
    },
    price: {
      type: Number
    },
    totalPrice: {
      type: Number
    },
    numViews: {
      type: Number
    },
    combinelocation: {
      type: String
    },
    status: {
      type: Boolean
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'Client'
    }
  },
  {
    timestamps: true,
  }
);

wishListModel.plugin(mongoosePaginate);
module.exports = {
  wishListModel
};
