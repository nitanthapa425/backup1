const crypto = require('crypto');

const algorithm = 'aes-256-cbc';

// generate 16 bytes of random data
const initVector = crypto.randomBytes(16);

// secret key generate 32 bytes of random data
const securityKey = crypto.randomBytes(32);

async function encryptData(message) {
  const cipher = crypto.createCipheriv(algorithm, securityKey, initVector);

  let encryptedData = cipher.update(message, 'utf-8', 'hex');

  encryptedData += cipher.final('hex');

  return encryptedData;
}

async function decryptData(message) {
  const decipher = crypto.createDecipheriv(algorithm, securityKey, initVector);

  let decryptedData = decipher.update(message, 'hex', 'utf-8');
  decryptedData += decipher.final('utf-8');

  return decryptedData;
}

module.exports = {
  encryptData,
  decryptData
};
