/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { draftVehicleController } = require('../controllers');
const validate = require('../middlewares/validate');
const { uploadMultipleFile } = require('../middlewares/uploadImageMiddleware');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');
const { createVehicleDraftValidate } = require('../validations/validateSchema');
const {
  expressAuthentication,
  isAuthenticated,
  isAuthorized,
} = require('../middlewares/auth');

const router = express.Router();

router
  .route('/vehicleDraftDetail')
  .get(validate(paramsValidation.pagination), paginate, draftVehicleController.getDraftVehicleDetailController)
  .post(
    validate(createVehicleDraftValidate),
    uploadMultipleFile,
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftVehicleController.createDraftVehicleDetailController
  );

router
  .route('/vehicleDraftDetail/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftVehicleController.getVehicleDraftDetailByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftVehicleController.updateVehicleDraftDetailByIdController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftVehicleController.deleteVehicleDraftDetailByIdController
  );

router
  .route('/getDraftByUserId')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    draftVehicleController.getDraftByUserIdController
  );

module.exports = router;
