const express = require('express');
const { expressAuthentication } = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const addToCartValidation = require('../validations/addToCart.validation');
const { addToCartController } = require('../controllers');

const router = express.Router();

router.route('/').get(expressAuthentication, paginate, addToCartController.getAllAddToCart);

router
  .route('/:id')
  .post(validate(addToCartValidation), expressAuthentication, addToCartController.createAddToCart)
  .get(expressAuthentication, addToCartController.getAddToCartById)
  .delete(expressAuthentication, addToCartController.deleteAddToCartById);

router
  .route('/user/getClientCart')
  .get(expressAuthentication, addToCartController.getClientCartById);

module.exports = router;
