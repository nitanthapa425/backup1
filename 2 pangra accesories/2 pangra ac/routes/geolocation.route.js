const express = require('express');
const { geoLocationController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const { expressAuthentication } = require('../middlewares/auth');

const router = express.Router();

router
  .route('/municipality')
  .post(expressAuthentication, geoLocationController.createMunicipality)
  .get(expressAuthentication, paginate, geoLocationController.getAllMunicipality);
router.route('/municipality/:districtName').get(geoLocationController.getMunicipalityByDistrict);
router
  .route('/single-municipality/:municipalityId')
  .get(geoLocationController.getMunicipalityById)
  .patch(expressAuthentication, geoLocationController.updateMunicipality);
router
  .route('/full-location')
  .post(expressAuthentication, geoLocationController.createFullLocation)
  .get(expressAuthentication, paginate, geoLocationController.getFullLocation);

router
  .route('/full-location/:fullLocationId')
  .patch(expressAuthentication, geoLocationController.updateFullLocation)
  .get(expressAuthentication, geoLocationController.getFullLocationById)
  .delete(expressAuthentication, geoLocationController.deleteFullLocationById);

router.route('/full-location-no-auth').get(geoLocationController.getFullLocationWithoutAuth);
router.route('/provinces').get(geoLocationController.getAllProvince);
router.route('/districts').get(geoLocationController.getAllDistrict);
router.route('/district/:provincesId').get(geoLocationController.getAllDistrictByProvince);

module.exports = router;
