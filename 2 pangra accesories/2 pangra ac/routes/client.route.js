/**
 * Copyright (C) Two Pangra
 */

/**
 * the user route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const { clientProfileController } = require('../controllers');
const { paginate } = require('../middlewares/pagination');
const validate = require('../middlewares/validate');
const { expressAuthentication, isAuthenticated, isAuthorized } = require('../middlewares/auth');
const paramsValidation = require('../validations/pagination.validation');
const paramsValidationSchema = require('../validations/validateSchema');

const router = express.Router();

router
  .route('/customerLocation')
  .post(
    validate(paramsValidationSchema.locationValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['CLIENT']),
    clientProfileController.addClientAddress
  )
  .get(expressAuthentication, isAuthenticated, isAuthorized(['CLIENT']), clientProfileController.getAllClientAddress);

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router
  .route('/')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    clientProfileController.getClientProfileList
  )
  .post(validate(paramsValidationSchema.registerClient), clientProfileController.registerClient);

router
  .route('/deletedClients')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    paginate,
    clientProfileController.getDeletedClientProfileList
  );

router.route('/forgotClientPassword').post(clientProfileController.forgotClientPassword);

router.route('/resetClientPassword').post(clientProfileController.resetClientPassword);

router
  .route('/customerLocation/:locationId')
  .get(expressAuthentication, isAuthenticated, isAuthorized(['CLIENT']), clientProfileController.getSingleClientAddress)
  .patch(
    validate(paramsValidationSchema.locationValidate),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['CLIENT']),
    clientProfileController.updateLocationsById
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['CLIENT']),
    clientProfileController.deleteLocationsById
  );

router
  .route('/deletedClients/:id')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    paginate,
    clientProfileController.getDeletedClientProfileById
  );

router
  .route('/:id')
  .get(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    clientProfileController.getClientProfileByIdController
  )
  .put(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    clientProfileController.updateClientProfileByIdController
  )
  .delete(
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN']),
    clientProfileController.deleteClientProfileByIdController
  );

router
  .route('/profile/me')
  .get(expressAuthentication, isAuthenticated, clientProfileController.getClientProfile)
  .put(
    validate(paramsValidationSchema.updateClient),
    expressAuthentication,
    isAuthenticated,
    clientProfileController.updateClientProfile
  );

router
  .route('/revertClient/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    clientProfileController.revertDeletedClientById
  );

router.route('/sendOTP').post(clientProfileController.sendOTPSms);

router.route('/verifyOTP').post(clientProfileController.verifyOTPSms);

router.route('/resendForgotVerificationToken').patch(clientProfileController.resendForgotVerificationToken);

router
  .route('/changeClientPassword')
  .post(expressAuthentication, isAuthenticated, clientProfileController.changeClientPassword);

// eslint-disable-next-line max-len
router.route('/clientLogin').post(validate(paramsValidationSchema.clientLogin), clientProfileController.clientLogin);

router
  .route('/permanentDeleteClient/:id')
  .post(
    expressAuthentication,
    isAuthenticated,
    isAuthorized('SUPER_ADMIN'),
    clientProfileController.permanentDeleteClientById
  );

router.route('/clientLogout').post(expressAuthentication, isAuthenticated, clientProfileController.clientLogout);

module.exports = router;
