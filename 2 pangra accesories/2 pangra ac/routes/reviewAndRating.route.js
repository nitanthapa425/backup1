/**
 * Copyright (C) Two Pangra
 */

/**
 * the Brand Vehicle Route
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const express = require('express');
const validate = require('../middlewares/validate');
const { createReviewAndRatingValidate } = require('../validations/reviewAndRating.validation');
const { expressAuthentication } = require('../middlewares/auth');
const { ratingAndReviewController } = require('../controllers');

const router = express.Router();

router.route('/review/:accessoriesId')
  .post(expressAuthentication,
    validate(createReviewAndRatingValidate),
    ratingAndReviewController.createReviewAndRating);

router.route('/')
  .get(expressAuthentication, ratingAndReviewController.getAllRatingAndReview);

router.route('/:id')
  .get(expressAuthentication, ratingAndReviewController.getRatingById)
  .delete(expressAuthentication, ratingAndReviewController.deleteRatingAndReview)
  .put(expressAuthentication, validate(createReviewAndRatingValidate), ratingAndReviewController.UpdateRating);

module.exports = router;
