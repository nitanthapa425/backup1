const errors = require('common-errors');
const httpStatus = require('http-status');
// const { accessoriesService } = require('.');
const helper = require('../common/helper');
const { Client, Accessories } = require('../models');

/* Create WishList */
const createWishList = async (userId, tagId) => {
  const accessoriesDetail = await helper.ensureEntityExists(Accessories, { _id: tagId }, 'Accessories not found.');
  if (!accessoriesDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Product is not found.');
  }
  const wishListDetail = await helper.ensureEntityExistsInNested(
    Accessories,
    { _id: tagId },
    { wishList: { $elemMatch: { userId } } }
  );
  if (wishListDetail.wishList.length > 0) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This product is already in wishlist.');
  }
  const userDetail = await Client.findOne({ _id: userId });
  const { fullName } = userDetail;
  const values = {
    userId,
    fullName
  };
  accessoriesDetail.wishList.push(values);
  const wishListData = accessoriesDetail.save();
  if (wishListData) {
    await helper.createWishListLogManagement(
      userId,
      'Add Wishlist',
      'Add Wishlist',
      'Wishlist',
      'CLIENT'
    );
  }
  return wishListData;
};

/* Get WishList ById */
const getWishListById = async (userId) => {
  // const wishListDetail = await helper.ensureEntityExistsInNested(
  //   Accessories,
  //   { _id: accessoriesId },
  //   { wishList: { $elemMatch: { userId } } }
  // );
  const wishListDetail = await Accessories.find({ 'wishList.userId': userId });
  const result = wishListDetail.map((res) => ({
    productName: res.productTitle,
    price: res.costPrice,
    productImage: res.productImage
  }));
  return result;
};

/* Delete WishList */
const deleteWishList = async (userId, deleteId) => {
  const accessoriesDetail = await Accessories.findOne({ _id: deleteId });
  if (!accessoriesDetail) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Product is not found.');
  }
  await Accessories.updateOne({ _id: deleteId }, { $pull: { wishList: { userId } } }, { safe: true, multi: true });
  await helper.createWishListLogManagement(
    userId,
    'Delete Wishlist',
    'Delete Wishlist',
    'Wishlist',
    'CLIENT'
  );
  return {
    status: 201,
    message: 'Wishlist is delete successfully.'
  };
};

/* getAllWishList */
const getAllWishList = async () => {
  const wishList = await Accessories.find({}).populate('accessoriesId', 'productTitle productImages');
  if (!wishList) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'WishList is Empty');
  }
  return wishList;
};

/* get login Client WishList  */
const getClientWishlist = async (userId) => {
  const wishList = await Accessories.find({ 'wishList.userId': userId });

  const result = wishList.map((res) => ({
    productId: res._id,
    productName: res.productTitle,
    price: res.costPrice,
    productImage: res.productImage,
    color: res.color,
    numViews: res.numViews
  }));
  return result;
};

module.exports = {
  createWishList,
  getWishListById,
  deleteWishList,
  getAllWishList,
  getClientWishlist
};
