const _ = require('lodash');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const { RatingAndReview, Accessories, BuyNow } = require('../models');

/* Create Rating And Review  */
// eslint-disable-next-line consistent-return
const createRating = async (userId, accessoriesId, reqObj) => {
  /* Check if product exist or not */
  await helper.ensureEntityExists(
    Accessories,
    {
      _id: accessoriesId
    },
    'Accessories is not found'
  );

  /* Check if the user has already given the rating */
  const existingRatingOfUsers = await RatingAndReview.findOne({ accessoriesId, createdBy: userId });

  if (existingRatingOfUsers) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'You cannot give the rating twice');
  }
  /* Check if the user has already given the rating */
  const checkUserHasBoughtSameProduct = await BuyNow.findOne({ accessoriesId, createdBy: userId });

  if (!checkUserHasBoughtSameProduct) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Only buyer of this product is able to review it');
  }

  /* Add accessories id in rating and review */
  const values = {
    ...reqObj,
    accessoriesId
  };
  if (!existingRatingOfUsers) {
    const value = await RatingAndReview.create(values);
    await helper.ratingAndReviewLogManagement(
      userId,
      'Add Rating',
      'Add Rating',
      'Rating',
      'CLIENT'
    );
    return value;
  }
};

/* Get All Rating And Review */
const getAllRating = async () => {
  const getAllRatingAndReview = await RatingAndReview.find({});
  return getAllRatingAndReview;
};

/* Delete Rating And Review ById */
const deleteRatingAndReview = async (deleteId, userId) => {
  const ratingAndReview = await RatingAndReview.findByIdAndDelete({ _id: deleteId, createdBy: userId });
  if (ratingAndReview) {
    await helper.ratingAndReviewLogManagement(
      userId,
      'Delete Rating',
      'Delete Rating',
      'Rating',
      'CLIENT'
    );
  }
  return ratingAndReview;
};
/* Get Rating By Using Id */
const getRatingById = async (productId, userId) => {
  const ratingAndReview = await RatingAndReview.findOne({ _id: productId, createdBy: userId });
  if (!ratingAndReview) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Rating and review not found');
  }
  if (ratingAndReview) {
    await helper.ratingAndReviewLogManagement(
      userId,
      'View Rating',
      'View Rating',
      'Rating',
      'CLIENT'
    );
  }
  return ratingAndReview;
};

/* Update Rating By Using Id */
const updateRatingById = async (productId, product) => {
  const ratingDetailById = await helper.ensureEntityExists(
    RatingAndReview,
    {
      _id: productId
    },
    'Product is not found'
  );

  _.assignIn(ratingDetailById, { ...product });

  const updatedRating = await ratingDetailById.save();

  return updatedRating;
};
module.exports = {
  createRating,
  getAllRating,
  deleteRatingAndReview,
  getRatingById,
  updateRatingById
};
