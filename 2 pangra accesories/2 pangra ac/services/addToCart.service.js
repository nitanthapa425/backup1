const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const { CartModel, Accessories } = require('../models');

/* creating Add To Cart */

const createAddToCart = async (tagId, userId, cartBody) => {
  const addToCart = await CartModel.findOne({ accessoriesId: tagId, createdBy: userId });
  if (addToCart) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'This item is already exist in cart list');
  }
  const productSellDetail = await Accessories.findOne({ _id: tagId });
  const {
    _id,
    costPrice,
    discountPrice,
    discountPercentage,
    onSale,
    sizeValue,
    color,
    quantity
  } = productSellDetail;

  const productDetail = {
    accessoriesId: _id,
    costPrice,
    discountPrice,
    discountPercentage,
    status: onSale,
    sizeValue,
    color,
    quantity,
    sizeValues: cartBody.sizeValues,
    colors: cartBody.colors,
    quantitys: cartBody.quantitys,
    createdBy: userId
  };
  const value = await CartModel.create(productDetail);
  if (value) {
    await helper.createAddToCartLogManagement(
      userId,
      'Add Cart',
      'Add To Cart',
      'Cart',
      'CLIENT'
    );
  }
  return productDetail;
};

/* Get All Add To Cart */
const getAllAddToCart = async (search, options) => {
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };
  const cart = await CartModel.paginate(search, optionValue);
  return cart;
};

/* Get Add To Cart By Id */
const getAddToCartById = async (getId) => {
  const cart = await CartModel.findOne({ _id: getId }).populate('accessoriesId', 'productTitle productImages numViews color');
  if (!cart) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'There is no selected item available on a cart');
  }
  return cart;
};

/* Deleteing Add To Cart */
const deleteAddToCartById = async (id, userId) => {
  const cart = await CartModel.findOne({ _id: id, createdBy: userId });
  if (!cart) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'This item is not available cart list.');
  }
  const deleteCart = await CartModel.findOneAndDelete({ _id: id });
  if (deleteCart) {
    await helper.createAddToCartLogManagement(
      userId,
      'Delete Cart',
      'Cart Delete',
      'Cart',
      'CLIENT'
    );
  }
  return deleteCart;
};

/* Get User Add To Cart */
const getClientCartById = async (userId) => {
  const cart = await CartModel.find({ createdBy: userId }).populate('accessoriesId', 'productTitle productImages numViews color');
  if (!cart) {
    throw new errors.HttpStatusError(httpStatus.NOT_FOUND, 'This item is not available on add to cart.');
  }
  if (cart) {
    await helper.createAddToCartLogManagement(
      userId,
      'View Cart',
      'View Cart',
      'Cart',
      'CLIENT'
    );
  }
  return cart;
};

module.exports = {
  createAddToCart, getAllAddToCart, getAddToCartById, deleteAddToCartById, getClientCartById
};
