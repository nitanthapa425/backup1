const nepalGeojson = require('nepal-geojson');
const httpStatus = require('http-status');
const errors = require('common-errors');
const { GeoLocationSchemaModel, fullLocationModel, VehicleSellModel } = require('../models');
const ApiError = require('../utils/ApiError');

const getAllProvince = async () => {
  const provincesGeojson = nepalGeojson.listProvincesWithDistricts();
  const provinces = provincesGeojson.map((res) => res.details);
  return provinces;
};

const getAllDistrictByProvince = async (provinceId) => {
  const provinceIdNum = parseInt(provinceId, 10);
  const districtsGeojson = nepalGeojson.listProvinceWithDistricts(provinceIdNum);
  return districtsGeojson.districts;
};

async function createMunicipality(entity) {
  const districtExist = nepalGeojson.districtInfo(entity.districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.findOne({
    municipalityName: entity.municipalityName
  });

  if (municipality) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality already exist');
  }

  return GeoLocationSchemaModel.create(entity);
}

async function createFullLocation(entity) {
  return fullLocationModel.create(entity);
}

async function updateFullLocation(fullLocationId, entity) {
  const fullLocation = await fullLocationModel.findById(fullLocationId);

  const value = await fullLocationModel.updateOne({ _id: fullLocationId }, entity);
  await VehicleSellModel.updateMany(
    { 'location.combineLocation': fullLocation.fullAddress },
    { $set: { 'location.combineLocation': entity.fullAddress } }
  );

  return value;
}

const getMunicipalityByDistrict = async (districtName) => {
  const districtExist = nepalGeojson.districtInfo(districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.find({
    districtName
  });

  return municipality;
};

const getAllDistrict = async () => nepalGeojson.districtsInfo();

const getFullLocation = async (options, search) => {
  const results = await fullLocationModel.paginate(search, options);
  return results;
};

const getFullLocationWithoutAuth = async () => {
  const results = await fullLocationModel.find({});
  const fullLocations = results.map((res) => ({
    id: res._id,
    fullAddress: res.fullAddress
  }));
  return fullLocations.reverse();
};

const getMunicipalityById = async (municipalityId) => {
  const results = await GeoLocationSchemaModel.findById(municipalityId);
  return results;
};

const getFullLocationById = async (fullLocationId) => {
  const results = await fullLocationModel.findById(fullLocationId);
  return results;
};

const getAllMunicipality = async (options, search) => {
  const results = await GeoLocationSchemaModel.paginate(search, options);
  return results;
};

async function updateMunicipality(municipalityId, entity) {
  const districtExist = nepalGeojson.districtInfo(entity.districtName);

  if (typeof districtExist === 'undefined') {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'District not found');
  }

  const municipality = await GeoLocationSchemaModel.findById(municipalityId);

  if (!municipality) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality not found');
  }

  const municipalityOldName = municipality.municipalityName;

  const result = await GeoLocationSchemaModel.updateOne({ _id: municipalityId }, entity);

  await fullLocationModel.updateMany(
    { municipalityName: municipalityOldName },
    { $set: { municipalityName: entity.municipalityOldName } }
  );

  return result;
}

const deleteFullLocationById = async (fullLocationId) => {
  const location = await fullLocationModel.findOne({ _id: fullLocationId });
  if (!location) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Location not found');
  }

  const locationExist = await VehicleSellModel.findOne({
    combineLocation: location.fullAddress
  });

  if (locationExist) {
    throw new ApiError(httpStatus.NOT_FOUND, 'This location cannot be deleted since it is used in vehicle sell');
  }

  await location.deleteOne({ _id: fullLocationId });
  return location;
};

module.exports = {
  getAllProvince,
  getAllDistrict,
  createMunicipality,
  getMunicipalityByDistrict,
  getAllDistrictByProvince,
  createFullLocation,
  getFullLocation,
  getMunicipalityById,
  getAllMunicipality,
  updateMunicipality,
  getFullLocationWithoutAuth,
  updateFullLocation,
  getFullLocationById,
  deleteFullLocationById
};
