/**
 * Copyright (C) Two Pangra
 */

/**
 * the Service Entry Point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports.UserService = require('./user.service');
module.exports.SecurityService = require('./security.service');
module.exports.ClientService = require('./client.service');
module.exports.BrandAccessoriesService = require('./brandAccessories.service');
module.exports.ImageUploadService = require('./imageUpload.service');
module.exports.DraftVehicleService = require('./draftVehicle.service');
module.exports.VehicleSellService = require('./vehicleSell.service');
module.exports.GeoLocationService = require('./geoLocation.service');
module.exports.emailSubscribeService = require('./emailSubscribe.service');
module.exports.commentService = require('./comment.service');
module.exports.accessoriesService = require('./accessoriesItem.service');
module.exports.reviewAndRatingService = require('./reviewAndRating.service');
module.exports.draftAccessoriesService = require('./draftAccessoriesItem.service');
module.exports.buyNowService = require('./buyNow.service');
module.exports.wishListService = require('./wishList.service');
module.exports.addToCartService = require('./addToCart.service');
