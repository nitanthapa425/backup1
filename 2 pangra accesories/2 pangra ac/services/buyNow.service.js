/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const httpStatus = require('http-status');
// const errors = require('common-errors');
// const Temporal = require('temporal');
const { add } = require('date-fns');
const helper = require('../common/helper');
const ApiError = require('../utils/ApiError');

const adminEmail = 'arjunsubedi360@gmail.com';
const buyerEmail = 'arjunsubedi360@gmail.com';
// const sellerEmail = 'susandhakal112@gmail.com';
// eslint-disable-next-line object-curly-newline
const { Accessories, BuyNow, Client } = require('../models');

/**
 * @returns {Object} Buy Accessories
 */
const buyNow = async (userId, buyBody) => {
  const clientDetail = await Client.findOne({ _id: userId });
  const deliveryStart = new Date();
  const deliveryEnd = add(new Date(), { days: 2 });
  const { fullName, mobile } = clientDetail;
  const { bookList, address } = buyBody;

  bookList.map(async (res) => {
    const accessoriesDetail = await Accessories.findOne({ _id: res.id });
    const buyNowDetail = {
      ...res,
      accessoriesId: res.id,
      fullName,
      mobile,
      deliveryStart,
      deliveryEnd,
      createdBy: userId,
      productImage: accessoriesDetail.productImage,
      shippingAddress: address
    };
    await BuyNow.create(buyNowDetail);
    await helper.notifyBuyerBookedVehicleByEmail(buyerEmail, fullName, adminEmail, helper.getHostUrl());
  });

  return {
    code: 200,
    message: 'You have successfully bought a product.'
  };
};

/* Get Client book vehicle list */
const getCustomerBuyNow = async (userId) => {
  const customerBuyNowDetail = await BuyNow.find({ createdBy: userId });
  return customerBuyNowDetail;
};

/**
 * @returns {Object} get  Buy Now Details
 */
const getBuyNowDetails = async (search, options) => {
  // search.$or = [{ isApproved: true }, { isApproved: { $exists: true } }];
  const optionValue = {
    ...options,
    collation: {
      locale: 'en_US',
      numericOrdering: true
    }
  };
  const value = BuyNow.paginate(search, optionValue);
  return value;
};

/**
 * Get Cart by id
 * @param {ObjectId} id
 * @returns {Promise<Category>}
 */
const getBuyNowDetail = async (id) => {
  const book = BuyNow.findOne({ _id: id });
  if (!book) {
    throw new httpStatus.NOT_FOUND('Buy now is not found');
  }
  return book;
};

/**
 * Delete Cart by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteBuyNowById = async (buyId) => {
  const buyNowDetail = await BuyNow.findOne({ accessoriesId: buyId });
  if (!buyNowDetail) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Bought detail is not found');
  }

  const accessoriesDetail = await Accessories.findById(buyNowDetail.accessoriesId);
  // eslint-disable-next-line operator-linebreak
  const vehicleImage =
    typeof accessoriesDetail.bikeImagePath !== 'undefined' && accessoriesDetail.bikeImagePath.length > 0
      ? accessoriesDetail.bikeImagePath[0].imageUrl
      : '';

  if (buyNowDetail) {
    await helper.createNotification(
      'Delete',
      'Book',
      'BUYER',
      '/book',
      buyNowDetail.createdBy,
      accessoriesDetail.createdBy,
      accessoriesDetail.productTitle,
      vehicleImage
    );
    await helper.createNotification(
      'Delete',
      'Book',
      'SELLER',
      '/sell/view',
      buyNowDetail.createdBy,
      accessoriesDetail.createdBy,
      accessoriesDetail.productTitle,
      vehicleImage
    );
  }

  await BuyNow.deleteOne({ vehicleId: buyId });
  return buyNowDetail;
};

/**
 * Delete Buy Now by id
 * @param {ObjectId} userId
 * @returns {Promise<Category>}
 */
const deleteBuyNowByAdmin = async (bookId) => {
  const book = await BuyNow.findOne({ _id: bookId });
  if (!book) {
    throw new httpStatus.NOT_FOUND('Buy Now detail is not found');
  }

  await BuyNow.deleteOne({ _id: bookId });
  return book;
};
module.exports = {
  buyNow,
  getBuyNowDetails,
  getBuyNowDetail,
  getCustomerBuyNow,
  deleteBuyNowById,
  deleteBuyNowByAdmin
};
