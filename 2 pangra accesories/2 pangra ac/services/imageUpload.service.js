/**
 * Copyright (C) Dui Pangra
 */

/**
 * the user service
 *
 * @author      Dui Pangra
 * @version     1.0
 */
// const fs = require('fs');
const joi = require('joi');
// const helper = require('../common/helper');
const { Image } = require('../models');
const cloudinary = require('../utils/cloudinary');

/* Upload Brand Image In brandVehicle  */
const uploadImages = async (entity, req) => {
  const obj = {
    imageOriginalName: entity.imageOriginalName,
    imagePath: entity.imagePath,
    imageOriginalPath: entity.imageOriginalPath
  };

  const pictureFiles = req.files;

  const multiplePicturePromise = pictureFiles.map((picture) => cloudinary.uploader.upload(picture.path));

  const imageResponses = await Promise.all(multiplePicturePromise);

  const serverResult = imageResponses.map((res) => ({
    publicKey: res.public_id,
    imageUrl: res.secure_url
  }));

  Image.create(obj);

  return serverResult;
};

/**
 * Delete brandVehicle
 * @returns {Object} Search result
 */
async function deleteImages(publicKey) {
  await cloudinary.uploader.destroy(publicKey);
  return {
    status: 201,
    message: 'The image been deleted successfully'
  };
}

deleteImages.schema = {
  publicKey: joi.string().required()
};

/**
 * @returns {Object} Get  all Images
 */

const getAllImages = async () => {
  const results = await Image.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  uploadImages,
  deleteImages,
  getAllImages
};
