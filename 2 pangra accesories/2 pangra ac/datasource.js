/**
 * Copyright (C) Two Pangra
 */

/**
 * the mongo dataservice
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

// The mongoose instance.
const mongoose = require('mongoose');

// use bluebird promise library instead of mongoose default promise library
mongoose.Promise = global.Promise;

// The database mapping.
const dbs = {};

// print mongoose logs in dev env
if (process.env.NODE_ENV === 'development') {
  mongoose.set('debug', true);
}

/**
 * Gets a db connection for a URL.
 * @param {String} url the url
 * @param {Number} poolSize the db pool size
 * @return {Object} connection for the given URL
 */
function getDb(url, poolSize) {
  const uri = `${url}?poolSize=${poolSize}`;
  if (!dbs[url]) {
    const db = mongoose.createConnection(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
      debug: true
    });
    dbs[url] = db;
  }
  return dbs[url];
}

/**
 * Gets the mongoose.
 * @return {Object} the mongoose instance
 */
function getMongoose() {
  return mongoose;
}

// exports the functions
module.exports = {
  getDb,
  getMongoose,
};
