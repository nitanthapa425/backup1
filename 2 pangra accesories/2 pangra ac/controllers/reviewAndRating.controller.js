const httpStatus = require('http-status');
const { reviewAndRatingService } = require('../services');
const catchAsync = require('../utils/catchAsync');

const createReviewAndRating = catchAsync(async (req, res) => {
  const accessoriesValues = req.body;
  const { accessoriesId } = req.params;
  const userId = req.user.id;

  const _accessoriesValues = {
    ...accessoriesValues,
    createdBy: userId
  };
  const ratingAndReview = await reviewAndRatingService.createRating(userId, accessoriesId, _accessoriesValues);
  res.status(httpStatus.CREATED).send({
    code: 201,
    message: 'Rating created successfully.',
    ratingAndReview
  });
});

const getAllRatingAndReview = catchAsync(async (req, res) => {
  const ratingAndReview = await reviewAndRatingService.getAllRating();
  res.send({
    code: 201,
    message: 'All Rating is Found.',
    ratingAndReview
  });
});

const deleteRatingAndReview = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const deleteId = req.params.id;
  await reviewAndRatingService.deleteRatingAndReview(deleteId, userId);
  res.send({
    code: 201,
    message: 'Product Rating is Deleted Successfully.',

  });
});

const getRatingById = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const productId = req.params.id;
  const ratingAndReview = await reviewAndRatingService.getRatingById(productId, userId);
  res.send({
    code: 201,
    message: 'Product Rating Is Found Using Product Id .',
    ratingAndReview
  });
});

const UpdateRating = catchAsync(async (req, res) => {
  const productId = req.params.id;
  const product = req.body;
  const ratingAndReview = await reviewAndRatingService.updateRatingById(productId, product);
  res.send({
    code: 201,
    message: 'Product Rating Is Updated Successfully .',
    ratingAndReview
  });
});

module.exports = {
  createReviewAndRating, getAllRatingAndReview, deleteRatingAndReview, getRatingById, UpdateRating
};
