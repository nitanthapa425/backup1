const httpStatus = require('http-status');
const errors = require('common-errors');
const _ = require('lodash');
const helper = require('../common/helper');
const catchAsync = require('../utils/catchAsync');
const { GeoLocationSchemaModel, fullLocationModel } = require('../models');
const { GeoLocationService } = require('../services');

const getAllProvince = catchAsync(async (req, res) => {
  res.send(await GeoLocationService.getAllProvince());
});

const getAllDistrictByProvince = catchAsync(async (req, res) => {
  const { provincesId } = req.params;
  res.send(await GeoLocationService.getAllDistrictByProvince(provincesId));
});

const createMunicipality = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const municipality = _.extend(entity, {
    createdBy: userId,
    createdAt: Date.now()
  });

  const results = await GeoLocationService.createMunicipality(municipality);

  res.status(httpStatus.CREATED).send(results);
});

const createFullLocation = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const municipalityExist = await helper.ensureEntityExists(
    GeoLocationSchemaModel,
    { _id: req.body.municipalityId },
    'Municipality does not exist'
  );

  if (!municipalityExist) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality does not exist');
  }

  const fullAddress = req.body.wardNumber !== ''
    ? `${req.body.districtName} > ${municipalityExist.municipalityName} > ${req.body.wardNumber} > ${req.body.toleName}`
    : `${req.body.districtName} > ${municipalityExist.municipalityName} > ${req.body.toleName}`;

  const fullLocationExist = await fullLocationModel.findOne({ fullAddress });

  if (fullLocationExist) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Location already exist');
  }

  const fullLocation = _.extend(entity, {
    createdBy: userId,
    createdAt: Date.now(),
    municipalityName: municipalityExist.municipalityName,
    fullAddress
  });

  await GeoLocationService.createFullLocation(fullLocation);

  res.status(httpStatus.CREATED).send({
    code: 201,
    message: 'Full location saved successfully'
  });
});

const updateFullLocation = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const municipalityExist = await helper.ensureEntityExists(
    GeoLocationSchemaModel,
    { _id: req.body.municipalityId },
    'Municipality does not exist'
  );

  if (!municipalityExist) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Municipality does not exist');
  }

  const fullAddress = req.body.wardNumber !== ''
    ? `${req.body.districtName} > ${municipalityExist.municipalityName} > ${req.body.wardNumber} > ${req.body.toleName}`
    : `${req.body.districtName} > ${municipalityExist.municipalityName} > ${req.body.toleName}`;

  const fullLocation = _.extend(entity, {
    updatedBy: userId,
    updatedAt: Date.now(),
    municipalityName: municipalityExist.municipalityName,
    fullAddress
  });

  await GeoLocationService.updateFullLocation(req.params.fullLocationId, fullLocation);

  res.status(httpStatus.CREATED).send({
    code: 201,
    message: 'Full location updated successfully'
  });
});

const getFullLocationById = catchAsync(async (req, res) => {
  res.json(await GeoLocationService.getFullLocationById(req.params.fullLocationId));
});

const getFullLocation = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await GeoLocationService.getFullLocation(options, search));
});

const getFullLocationWithoutAuth = catchAsync(async (req, res) => {
  res.json(await GeoLocationService.getFullLocationWithoutAuth());
});

const getMunicipalityByDistrict = catchAsync(async (req, res) => {
  const { districtName } = req.params;
  res.json(await GeoLocationService.getMunicipalityByDistrict(districtName));
});

const getAllDistrict = catchAsync(async (req, res) => {
  res.send(await GeoLocationService.getAllDistrict());
});

const getAllMunicipality = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await GeoLocationService.getAllMunicipality(options, search));
});

const getMunicipalityById = catchAsync(async (req, res) => {
  res.json(await GeoLocationService.getMunicipalityById(req.params.municipalityId));
});

const updateMunicipality = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const municipality = _.extend(entity, {
    updatedBy: userId,
    updatedAt: Date.now()
  });

  await GeoLocationService.updateMunicipality(req.params.municipalityId, municipality);

  res.status(httpStatus.CREATED).send({
    code: 201,
    message: 'Municipality updated successfully'
  });
});

const deleteFullLocationById = catchAsync(async (req, res) => {
  await GeoLocationService.deleteFullLocationById(req.params.fullLocationId);
  res.send(httpStatus.CREATED, {
    message: 'Location has been removed successfully'
  });
});

module.exports = {
  getAllDistrictByProvince,
  getAllProvince,
  createMunicipality,
  getMunicipalityByDistrict,
  getAllDistrict,
  createFullLocation,
  getFullLocation,
  getMunicipalityById,
  getAllMunicipality,
  updateMunicipality,
  getFullLocationWithoutAuth,
  updateFullLocation,
  getFullLocationById,
  deleteFullLocationById
};
