const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { draftAccessoriesService } = require('../services');
// const { capitalize } = require('../utils/helper');

/* Create category */
const createDraftAccessoriesItem = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const values = {
    ...req.body,
    createdBy: userId,
    createdAt: new Date()
  };
  await draftAccessoriesService.createDraftAccessoriesItem(values);
  res.send(httpStatus.CREATED, {
    message: 'Draft accessories is posted successfully.'
  });
});

/* Get all categories  */
const getDraftAccessoriesItems = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  const result = await draftAccessoriesService.getDraftAccessoriesItems(search, options);
  res.send(result);
});

/* Get category by id */
const getDraftAccessoriesItem = catchAsync(async (req, res) => {
  const result = await draftAccessoriesService.getDraftAccessoriesItem(req.params.id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Draft accessories not found');
  }
  res.send(result);
});

/* Update accessories by id */
const updateDraftAccessoriesItemById = catchAsync(async (req, res) => {
  const commentId = req.params.id;
  const commentDetail = req.body;
  const userId = req.user.id;
  await draftAccessoriesService.updateDraftAccessoriesItemById(userId, commentId, commentDetail);
  res.send(httpStatus.CREATED, {
    message: 'Draft accessories has been updated successfully'
  });
});

/* Delete category by id */
const deleteDraftAccessoriesItemById = catchAsync(async (req, res) => {
  await draftAccessoriesService.deleteDraftAccessoriesItemById(req.user.id, req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Draft accessories has been deleted successfully'
  });
});

// /* Approve comment by admin */
// const approveCommentByAdmin = catchAsync(async (req, res) => {
//   await draftAccessoriesService.approveCommentByAdmin(req.params.id);
//   res.send(httpStatus.CREATED, {
//     message: 'Comment has been approved successfully'
//   });
// });

/* Delete category by id */
const finishDraftAccessoriesItemById = catchAsync(async (req, res) => {
  await draftAccessoriesService.finishDraftAccessoriesItemById(req.user.id, req.body, req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Draft has been finish successfully.'
  });
});
module.exports = {
  createDraftAccessoriesItem,
  getDraftAccessoriesItems,
  getDraftAccessoriesItem,
  updateDraftAccessoriesItemById,
  deleteDraftAccessoriesItemById,
  // approveCommentByAdmin,
  finishDraftAccessoriesItemById
};
