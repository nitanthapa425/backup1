/**
 * Copyright (C) Two Pangra
 */

/**
 * Vehicle Detail Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const _ = require('lodash');
const { BrandVehicle, BrandModel, VarientModel } = require('../models');
const DraftVehicleService = require('../services/draftVehicle.service');
const catchAsync = require('../utils/catchAsync');
const BrandVehicleService = require('../services/brandAccessories.service');

/**
 * post BrandVehicle information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

/**
 * Create ClientProfile
 */
const createDraftVehicleDetailController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;

  const brand = await BrandVehicle.findById(entity.brandId);
  const brandModel = await BrandModel.findById(entity.modelId);
  let brandSubModel = {};
  if (entity.varientId !== '') {
    brandSubModel = await VarientModel.findById(entity.varientId);
  }

  // const date = new Date(entity.makeYear);
  // const makeYears = date.getFullYear();

  // const newDate = date.setDate(date.getDate() + 1);
  // const timeFormat = new Date(newDate);
  // const makeYearDate = timeFormat.toISOString();

  const otherDetail = typeof entity.otherDetail !== 'undefined' && entity.otherDetail !== '' ? entity.otherDetail.concat(' ') : '';
  const varient = brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' && brandSubModel.varientName !== ''
    ? brandSubModel.varientName.concat(' ')
    : ' ';
  const modelShowName = varient !== '' ? brandModel.modelName.concat('') : brandModel.modelName;

  const vehicle = _.extend(entity, {
    createdBy: userId,
    brandName: brand.brandVehicleName,
    modelName: brandModel.modelName,
    varientName:
      brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' ? brandSubModel.varientName : '',
    // eslint-disable-next-line prefer-template
    vehicleName:
      `${brand.brandVehicleName.concat(' ')
      + modelShowName
      + varient
      + entity.engineFactor.displacement
      }cc ${
        otherDetail}`
    // makeYear: makeYearDate
  });

  const vehicleDetail = await DraftVehicleService.createDraftVehicleDetailService(vehicle);
  res.status(httpStatus.CREATED).send(vehicleDetail);
});

/**
 * get Vehicle Draft information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getDraftVehicleDetailController = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await DraftVehicleService.getDraftVehicleDetailService(search, options));
});

/**
 * get Vehicle Draft information By login User
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getDraftByUserIdController = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await DraftVehicleService.getDraftByUserIdService(userId));
});

/**
 * get Vehicle Draft information By draft id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getVehicleDraftDetailByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  res.json(await DraftVehicleService.getVehicleDraftDetailByIdService(id));
});

/**
 * Update Vehicle Draft information By draft id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const updateVehicleDraftDetailByIdController = catchAsync(async (req, res) => {
  const entity = req.body;
  const userId = req.user.id;
  const vehicleId = req.params.id;

  const brand = await BrandVehicleService.getBrandVehicleDetail(entity.brandId);
  const brandModel = await BrandVehicleService.getBrandModelByIdService(entity.modelId);
  let brandSubModel = {};
  if (entity.varientId !== '') {
    brandSubModel = await BrandVehicleService.getSubBrandModelByIdService(entity.varientId);
  }

  // const date = new Date(entity.makeYear);
  // const makeYears = date.getFullYear();

  // const newDate = date.setDate(date.getDate() + 1);
  // const timeFormat = new Date(newDate);
  // const makeYearDate = timeFormat.toISOString();

  const otherDetail = typeof entity.otherDetail !== 'undefined' && entity.otherDetail !== '' ? entity.otherDetail.concat(' ') : '';
  const varient = brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' && brandSubModel.varientName !== ''
    ? brandSubModel.varientName.concat(' ')
    : ' ';
  const modelShowName = varient !== '' ? brandModel.modelName.concat('') : brandModel.modelName;

  const vehicle = _.extend(entity, {
    updatedBy: userId,
    brandName: brand.brandVehicleName,
    modelName: brandModel.modelName,
    varientName:
      brandSubModel !== {} && typeof brandSubModel.varientName !== 'undefined' ? brandSubModel.varientName : '',
    // eslint-disable-next-line prefer-template
    vehicleName:
      `${brand.brandVehicleName.concat(' ')
      + modelShowName
      + varient
      + entity.engineFactor.displacement
      }cc ${
        otherDetail}`
    // makeYear: makeYearDate
  });
  res.json(await DraftVehicleService.updateVehicleDraftDetailByIdService(vehicleId, vehicle));
  // res.json(await DraftVehicleService.updateVehicleDraftDetailByIdService(req.user.id, req.params.id, req.body));
});

/**
 * Delete Vehicle Draft information By draft id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteVehicleDraftDetailByIdController = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  res.json(await DraftVehicleService.deleteVehicleDraftDetailByIdService(id, userId));
});

module.exports = {
  createDraftVehicleDetailController,
  getDraftVehicleDetailController,
  getDraftByUserIdController,
  getVehicleDraftDetailByIdController,
  updateVehicleDraftDetailByIdController,
  deleteVehicleDraftDetailByIdController
};
