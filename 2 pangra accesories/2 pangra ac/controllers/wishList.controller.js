const { wishListService } = require('../services');
const catchAsync = require('../utils/catchAsync');

/**
 * Create WishList
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createWishList = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const tagId = req.params.id;
  // eslint-disable-next-line no-unused-vars
  const wishlist = await wishListService.createWishList(userId, tagId);
  res.send({
    code: 201,
    message: 'Wishlist is Created Successfully'
  });
});
/**
 * Get wishlist by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getWishListById = catchAsync(async (req, res) => {
  res.json(await wishListService.getWishListById(req.user.id));
});
/**
 * Delete WishList By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

const deleteWishList = catchAsync(async (req, res) => {
  const deleteId = req.params.wishListId;
  const userId = req.user.id;
  res.send(await wishListService.deleteWishList(userId, deleteId));
  // res.send({
  //   code: 201,
  //   message: 'Wishlist is deleted successfully'
  // });
});

/**
 * Get All WishList
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllWishList = catchAsync(async (req, res) => {
  // eslint-disable-next-line no-unused-vars
  res.json(await wishListService.getAllWishList());
});

/**
 * Get Client WishList Only
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getClientWishlist = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await wishListService.getClientWishlist(userId));
});
module.exports = {
  createWishList,
  getWishListById,
  deleteWishList,
  getAllWishList,
  getClientWishlist
};
