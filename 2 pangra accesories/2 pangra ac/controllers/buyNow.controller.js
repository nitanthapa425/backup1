/**
 * Copyright (C) Two Pangra
 */

/**
 * Vehicle Detail Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
// const errors = require('common-errors');
const { buyNowService } = require('../services');
const ApiError = require('../utils/ApiError');
// const _ = require('lodash');
const catchAsync = require('../utils/catchAsync');
// const { capitalize } = require('../common/helper');

/**
 *  Buy Now
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const buyNow = catchAsync(async (req, res) => {
  res.json(await buyNowService.buyNow(req.user.id, req.body));
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getCustomerBuyNow = catchAsync(async (req, res) => {
  res.json(await buyNowService.getCustomerBuyNow(req.user.id));
});

/* Get buy now by id */
const getBuyNowDetail = catchAsync(async (req, res) => {
  const result = await buyNowService.getBuyNowDetail(req.params.id);
  if (!result) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Buy now detail is not found');
  }
  res.send(result);
});

/**
 *  Get add to cart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
/* Delete category by id */
const deleteBuyNowByCustomer = catchAsync(async (req, res) => {
  await buyNowService.deleteBuyNowByCustomer(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'Buy now is deleted successfully'
  });
});

/**
 * get cart information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getBuyNowDetails = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await buyNowService.getBuyNowDetails(search, options));
});

/* Delete Booked Vehicle by id */
const deleteBuyNow = catchAsync(async (req, res) => {
  await buyNowService.deleteBuyNowById(req.params.id);
  res.send(httpStatus.CREATED, {
    message: 'The product bought has been cancelled.'
  });
});

/**
 *  Get customer buy now by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
/* Delete category by id */
const getCustomerBuyNowById = catchAsync(async (req, res) => {
  const value = await buyNowService.getCustomerBuyNowById(req.user.id, req.params.id);
  res.send(value);
});
module.exports = {
  buyNow,
  getCustomerBuyNow,
  deleteBuyNowByCustomer,
  getBuyNowDetail,
  getBuyNowDetails,
  deleteBuyNow,
  getCustomerBuyNowById
};
