const { addToCartService } = require('../services');
const catchAsync = require('../utils/catchAsync');

/**
 * Create AddToCart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const createAddToCart = catchAsync(async (req, res) => {
  const cartBody = req.body;
  const tagId = req.params.id;
  const userId = req.user.id;
  // eslint-disable-next-line no-unused-vars
  const addToCart = await addToCartService.createAddToCart(tagId, userId, cartBody);
  res.send({
    code: 201,
    message: 'products is added to cart successfully.'
  });
});

/**
 * GEt All AddToCart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAllAddToCart = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.json(await addToCartService.getAllAddToCart(search, options));
});

/**
 * GET AddToCart By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getAddToCartById = catchAsync(async (req, res) => {
  const getId = req.params.id;
  const userId = req.user.id;
  res.json(await addToCartService.getAddToCartById(getId, userId));
});

/**
 * DELETE AddToCart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const deleteAddToCartById = catchAsync(async (req, res) => {
  const { id } = req.params;
  const userId = req.user.id;
  // eslint-disable-next-line no-unused-vars
  const cart = await addToCartService.deleteAddToCartById(id, userId);
  res.send({
    code: '201',
    message: 'Products has been removed from cart successfully '
  });
});

/**
 * GET User AddToCart
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
const getClientCartById = catchAsync(async (req, res) => {
  const userId = req.user.id;
  res.json(await addToCartService.getClientCartById(userId));
});

// const incrementAddToCart = catchAsync(async (req, res) => {
//   const incrementId = req.params.id;
//   const accessories = req.body;
//   const cart = await addToCartService.incrementAddToCart(incrementId, accessories);
//   res.send({
//     code: '201',
//     message: 'incremented successfully',
//     cart
//   });
// });

module.exports = {
  createAddToCart, getAllAddToCart, getAddToCartById, deleteAddToCartById, getClientCartById
};
