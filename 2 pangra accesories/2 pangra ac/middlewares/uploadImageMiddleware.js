const multer = require('multer');

// const storage = multer.diskStorage({
//   destination(req, file, cb) {
//     cb(null, './uploads/two-pangra-img');
//   },
//   filename(req, file, cb) {
//     cb(null, `${Date.now()}_${file.originalname}`);
//   }
// });

const storage = multer.diskStorage({});

const fileFilter = (req, file, cb) => {
  if ((file.mimetype).includes('jpeg') || (file.mimetype).includes('png') || (file.mimetype).includes('jpg')) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({ storage, fileFilter });
const fileUploadMiddleware = upload.single('file');
const uploadMultipleFile = upload.array('files', 12);
module.exports = {
  fileUploadMiddleware,
  uploadMultipleFile
};
