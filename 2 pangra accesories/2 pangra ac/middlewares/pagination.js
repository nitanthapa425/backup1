/**
 * Copyright (C) Two Pangra
 */

/**
 * the pagination
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

/**
 * Validate the search query params
 * @param searchString
 * @returns {any}
 */
const validateSearch = (searchString) => JSON.parse(`{${searchString}}`);

/**
 * Convert plain search object to regex object to implement text search in mongo
 * @param searchObject
 */
const convertSearchToRegex = (searchObject, noCheckRegex = []) => {
  const regexObject = {};
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const key of Object.keys(searchObject)) {
    if (noCheckRegex.includes(key)) {
      regexObject[key] = searchObject[key];
    } else {
      const splitWork = searchObject[key].split(' ');
      const arrWords = splitWork.map((n) => [`(?=.*${n})`]);
      regexObject[key] = new RegExp(`^${arrWords.join('')}.*$`, 'im');
      // regexObject[key] = new RegExp(searchObject[key], 'i');
    }
  }

  return regexObject;
};

/**
 * Middleware to Convert Pagination and Search Params
 */

const paginate = async (req, res, next) => {
  let {
    limit, page, search, sortBy, sortOrder, select, noRegex, type
  } = req.query;

  limit = limit || 1000; // Default Page Size 1000
  page = page || 1; // Default Page
  search = search || '';
  sortBy = sortBy || '';
  sortOrder = sortOrder || 1;
  select = select || '';
  select = select.replace(/,/g, ' ');
  noRegex = noRegex ? noRegex.split(',') : [];
  type = type || '';
  try {
    if (type === 'vehicleSell') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      const searchRange = [];

      if (typeof search.bikeDriven !== 'undefined' && search.bikeDriven !== '') {
        const splitBikeDriven = search.bikeDriven.split('-');
        if (splitBikeDriven.length > 1) {
          searchRange.push({
            bikeDriven: {
              $gte: splitBikeDriven[0],
              $lte: splitBikeDriven[1]
            }
          });

          delete search.bikeDriven;
        }
      }

      if (typeof search.mileage !== 'undefined' && search.mileage !== '') {
        const splitMileage = search.mileage.split('-');
        if (splitMileage.length > 1) {
          searchRange.push({
            mileage: {
              $gte: splitMileage[0],
              $lte: splitMileage[1]
            }
          });

          delete search.mileage;
        }
      }

      if (typeof search.expectedPrice !== 'undefined' && search.expectedPrice !== '') {
        const splitPrice = search.expectedPrice.split('-');

        if (splitPrice.length > 1) {
          searchRange.push({
            expectedPrice: {
              $gte: splitPrice[0],
              $lte: splitPrice[1]
            }
          });

          delete search.expectedPrice;
        }
      }

      if (typeof search.brandName !== 'undefined' && search.brandName !== '') {
        const splitBrandName = search.brandName.split(',');

        if (splitBrandName.length > 1) {
          searchRange.push({
            brandName: {
              $in: splitBrandName
            }
          });

          delete search.brandName;
        }
      }

      if (typeof search.district !== 'undefined' && search.district !== '') {
        const splitDistrict = search.district.split(',');
        if (splitDistrict.length > 0) {
          searchRange.push({
            'location.district': {
              $in: splitDistrict
            }
          });

          delete search.district;
        }
      }

      if (typeof search.fuelType !== 'undefined' && search.fuelType !== '') {
        const splitFuelType = search.fuelType.split(',');

        if (splitFuelType.length > 1) {
          searchRange.push({
            fuelType: {
              $in: splitFuelType
            }
          });

          delete search.fuelType;
        }
      }

      if (typeof search.vehicleType !== 'undefined' && search.vehicleType !== '') {
        const splitVehicleType = search.vehicleType.split(',');

        if (splitVehicleType.length > 1) {
          searchRange.push({
            vehicleType: {
              $in: splitVehicleType
            }
          });

          delete search.vehicleType;
        }
      }

      if (typeof search.color !== 'undefined' && search.color !== '') {
        const splitColor = search.color.split(',');

        if (splitColor.length > 1) {
          searchRange.push({
            color: {
              $in: splitColor
            }
          });

          delete search.color;
        }
      }

      if (typeof search.condition !== 'undefined' && search.condition !== '') {
        const splitCondition = search.condition.split(',');

        if (splitCondition.length > 1) {
          searchRange.push({
            condition: {
              $in: splitCondition
            }
          });

          delete search.condition;
        }
      }

      if (typeof search.makeYear !== 'undefined' && search.makeYear !== '') {
        let makeYearSplit = [];

        const postDateSplit = search.makeYear.split('T');
        makeYearSplit = postDateSplit[0].split('-');

        if (makeYearSplit.length > 1) {
          searchRange.push({
            makeYear: {
              $gte: `${makeYearSplit[0]}-01-01T00:00:00.000Z`,
              $lte: `${makeYearSplit[0]}-12-30T23:59:59.999Z`
            }
          });

          delete search.makeYear;
        }
      }

      search.$and = searchRange;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'vehicleSellTable') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      const searchRange = [];

      searchRange.push({
        status: true
      });

      if (typeof search.makeYear !== 'undefined' && search.makeYear !== '') {
        let makeYearSplit = [];

        const postDateSplit = search.makeYear.split('T');
        makeYearSplit = postDateSplit[0].split('-');
        if (makeYearSplit.length > 1) {
          searchRange.push({
            makeYear: {
              $gte: `${makeYearSplit[0]}-01-01T00:00:00.000Z`,
              $lte: `${makeYearSplit[0]}-12-30T23:59:59.999Z`
            }
          });
          delete search.makeYear;
        }
      }

      if (typeof search.postExpiryDate !== 'undefined' && search.postExpiryDate !== '') {
        let postExpirySplit = [];

        const postDateSplit = search.postExpiryDate.split('T');
        postExpirySplit = postDateSplit[0].split('-');

        const dates = new Date(postExpirySplit[0], postExpirySplit[1] - 1, postExpirySplit[2]);

        const searchDate = dates.toLocaleDateString('fr-CA').split('/').join('-');

        if (postExpirySplit.length > 1) {
          searchRange.push({
            postExpiryDate: {
              $gte: `${searchDate}T00:00:00.000Z`,
              $lte: `${searchDate}T23:59:59.999Z`
            }
          });

          if (search.postExpiryDate) {
            delete search.postExpiryDate;
          }
        }
      }

      if (typeof search.condition !== 'undefined' && search.condition !== '') {
        const splitCondition = search.condition.split(',');

        if (splitCondition.length > 1) {
          searchRange.push({
            condition: {
              $in: splitCondition
            }
          });

          delete search.condition;
        }
      }

      if (typeof search.makeYear !== 'undefined' && search.makeYear !== '') {
        let makeYearSplit = [];

        const postDateSplit = search.makeYear.split('T');
        makeYearSplit = postDateSplit[0].split('-');

        if (makeYearSplit.length > 1) {
          searchRange.push({
            makeYear: {
              $gte: `${makeYearSplit[0]}-01-01T00:00:00.000Z`,
              $lte: `${makeYearSplit[0]}-12-30T23:59:59.999Z`
            }
          });

          if (search.makeYear) {
            delete search.makeYear;
          }
        }
      }

      if (typeof search.ownershipCount !== 'undefined' && search.ownershipCount !== '') {
        const splitOwnership = search.ownershipCount.split(',');

        if (splitOwnership.length > 1) {
          searchRange.push({
            ownershipCount: {
              $in: splitOwnership
            }
          });

          delete search.ownershipCount;
        }
      }

      if (typeof search.color !== 'undefined' && search.color !== '') {
        const splitColor = search.color.split(',');

        if (splitColor.length > 1) {
          searchRange.push({
            color: {
              $in: splitColor
            }
          });

          delete search.color;
        }
      }

      if (typeof search.usedFor !== 'undefined' && search.usedFor !== '') {
        const splitUserFor = search.usedFor.split(',');

        if (splitUserFor.length > 1) {
          searchRange.push({
            usedFor: {
              $in: splitUserFor
            }
          });

          delete search.usedFor;
        }
      }

      search.$and = searchRange;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'userDob') {
      search = convertSearchToRegex(validateSearch(search), noRegex);

      let dobSplit = [];
      let searchFinal = '';
      const searchData = [];
      if (typeof search.dob !== 'undefined' && search.dob !== '' && search.dob !== {}) {
        const dobDateSplit = search.dob.split('T');

        dobSplit = dobDateSplit[0].split('-');
        const dates = new Date(dobDateSplit[0]);

        searchFinal = dates.toLocaleDateString('fr-CA');
      }

      if (dobSplit.length > 1) {
        searchData.push({
          dob: {
            $gte: `${searchFinal}T00:00:00.000Z`,
            $lte: `${searchFinal}T23:59:59.999Z`
          }
        });

        delete search.dob;
      }

      if (typeof search.accessLevel !== 'undefined' && search.accessLevel !== '') {
        const splitAccessLevel = search.accessLevel.split(',');

        if (splitAccessLevel.length > 1) {
          searchData.push({
            accessLevel: {
              $in: splitAccessLevel
            }
          });

          delete search.accessLevel;
        }
      }

      if (typeof search.gender !== 'undefined' && search.gender !== '') {
        const splitGender = search.gender.split(',');

        if (splitGender.length > 1) {
          searchData.push({
            gender: {
              $in: splitGender
            }
          });

          delete search.gender;
        }
      }

      search.$and = searchData;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else if (type === 'clientDob') {
      search = convertSearchToRegex(validateSearch(search), noRegex);
      let dobSplit = [];
      let searchFinal = '';
      const searchData = [];

      if (typeof search.dob !== 'undefined' && search.dob !== '' && search.dob !== {}) {
        const dobDateSplit = search.dob.split('T');

        dobSplit = dobDateSplit[0].split('-');
        const dates = new Date(dobDateSplit[0]);

        searchFinal = dates.toLocaleDateString('fr-CA');
      }

      if (dobSplit.length > 1) {
        searchData.push({
          dob: {
            $gte: `${searchFinal}T00:00:00.000Z`,
            $lte: `${searchFinal}T23:59:59.999Z`
          }
        });

        delete search.dob;
      }

      if (typeof search.gender !== 'undefined' && search.gender !== '') {
        const splitGender = search.gender.split(',');

        if (splitGender.length > 1) {
          searchData.push({
            gender: {
              $in: splitGender
            }
          });

          delete search.gender;
        }
      }

      search.$and = searchData;

      if (search.$and.length === 0) {
        delete search.$and;
      }
    } else {
      search = convertSearchToRegex(validateSearch(search), noRegex);
    }
  } catch (ignored) {
    return res.status(400).send({ message: 'Search parameter is mis configured', stack: ignored.message });
  }
  res.pagination = {
    options: {
      sort: { [sortBy]: parseInt(sortOrder, 10) },
      page,
      limit,
      select
    },
    search
  };
  // DELETE SORTING IF NO SORT KEY DEFINED
  if (!sortBy) {
    delete res.pagination.options.sort;
  }
  next();

  return res;
};

module.exports = {
  paginate
};
