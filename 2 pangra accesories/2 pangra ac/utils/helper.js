/**
 * Copyright (C) Two Pangra
 */

/**
 * the helper service
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const httpStatus = require('http-status');
const errors = require('common-errors');

const findNestedValueByKey = (entireObj, keyToFind) => {
  const outputData = [];
  JSON.stringify(entireObj, (_, nestedValue) => {
    if (nestedValue && nestedValue[keyToFind]) {
      outputData.push(nestedValue[keyToFind]);
    }
    return nestedValue;
  });
  return outputData;
};

const escapeRegex = (string) => string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');

const randomString = (length, chars) => {
  let result = '';
  // eslint-disable-next-line no-plusplus
  for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

const existValidation = (value, name) => {
  if (value) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `${name} already exist`);
  }
};

/**
 *  Required validation mongoose required field with  status field
 * @returns {Function} the requiredField function
 */
function requiredField() {
  if (this.status === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with  Basic Factor
 * @returns {Function} the requiredField function
 */
function requiredBasicField() {
  if (this.basicStatus === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with Optional Field
 * @returns {Function} the requiredField function
 */
function requiredOptionField() {
  if (this.optionStatus === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with  status field in Technical Factor
 * @returns {Function} the requiredField function
 */
function requiredTechnicalField() {
  if (this.technicalStatus === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with  basicStatus field in Dimension Factor
 * @returns {Function} the requiredField function
 */
function requiredDimensionField() {
  if (this.dimensionStatus === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with  status field in Engine Field
 * @returns {Function} the requiredField function
 */
function requiredEngineField() {
  if (this.engineStatus === 'approved') {
    return true;
  }
  return false;
}
/**
 *  Required validation mongoose required field with  status field in Wheel Field
 * @returns {Function} the requiredField function
 */
function requiredWheelField() {
  if (this.wheelTyreStatus === 'approved') {
    return true;
  }
  return false;
}

/**
 * Regex Validation for checking Number in String
 * @returns {Function} the validateNumber function
 */
function validateNumber(number) {
  const regex = /^(\s*|\d+)$/;
  // const optRegex = /^[0-9]+$|^$|^\s$/;

  return regex.test(number);
}

/**
 * Regex Validation for checking Dates and Empty String
 * @returns {Function} the validateNumber function
 */
function validateDate(number) {
  const regex = /^\d{4}-\d{1,2}-\d{1,2}/;
  return regex.test(number);
}

/**
 * Regex Validation for checking Floating Number
 * @returns {Function} the validateNumber function
 */
function validateFloatingNumber(value) {
  const regex = /[+-]?([0-9]*[.])?[0-9]+/;
  return regex.test(value);
}

/**
 * Validation for checking Max Number
 * @returns {Function} the validateMaxNum function
 */
const validateMaxFiveNum = (value) => {
  // const oNumber = parseFloat(number);
  if (value === '') {
    return true;
  }
  if (value <= 5) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number 10
 * @returns {Function} the validateMaxNum function
 */
const validateMaxTenNum = (value) => {
  if (value === '') {
    return true;
  }
  if (value <= 10) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number
 * @returns {Function} the validateMaxNum function
 */
const validateMaxSeventyNum = (value) => {
  // const oNumber = parseFloat(number);
  // const oNumber = /^[0-9]+$|^$|^\s$/;
  if (value === '') {
    return true;
  }
  if (value <= 70) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number 50
 * @returns {Function} the validateMaxNum function
 */
const validateMaxOneHunFiftyChar = (value) => {
  // const ovalue = parseFloat(value);
  /* Checks number only */
  // const ovalue = /^[0-9]+$|^$|^\s$/;

  /* Checks from 2 to 50 characters */
  const regex = /^([a-zA-Z0-9, .'-()/_@!$^]){2,150}$/i;

  if (value === '') {
    return true;
  }
  if (regex.test(value)) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number
 * @returns {Function} the validateMaxNum function
 */
const validateFloatNumLess70 = (value) => {
  const _value = parseFloat(value);
  // const oNumber = /^[0-9]+$|^$|^\s$/;
  if (value === '') {
    return true;
  }
  if (_value <= 70) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Less than 1000
 * @returns {Function} the validateMaxNum function
 */
const validateMax1500Num = (value) => {
  // const oNumber = parseFloat(number);
  // const oNumber = /^[0-9]+$|^$|^\s$/;
  if (value === '') {
    return true;
  }
  if (value <= 1500) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number
 * @returns {Function} the validateMaxNum function
 */
const validateFloatNumLess100 = (value) => {
  const _value = parseFloat(value);
  // const oNumber = /^[0-9]+$|^$|^\s$/;
  if (value === '') {
    return true;
  }
  if (_value <= 100) {
    return true;
  }
  return false;
};

/**
 * Validation for checking Max Number
 * @returns {Function} the validateMaxNum function
 */
const validateFloatNum = (value) => {
  // console.log('*******************************value',value)
  const _value = parseFloat(value);
  // const oNumber = /^[0-9]+$|^$|^\s$/;
  // console.log('***************************************_value',_value)
  if (value === '') {
    return true;
  }
  if (_value === 0 || _value) {
    return true;
  }
  return false;
};

/**
 *  Required validation mongoose required field with Optional Field
 * @returns {Function} the requiredField function || this.bikeParking > 0 || this.carParking > 0
 */
function requiredLocationForBook() {
  if (this.status === 'Location') {
    return true;
  }
  return false;
}

/**
 *  Convert weight into kg from gm
 * @returns {Function} take weight and weight unit and convert into kg
 */
// eslint-disable-next-line consistent-return
function convertGmToKg(weight, weightUnit) {
  if (weightUnit === 'kg') {
    return weight.concat(' ') + weightUnit;
  }
  if (weightUnit === 'gm') {
    const gmToKg = (weight / 1000).toString();
    return `${gmToKg.concat(' ')}kg`;
  }
}

/* If quantity is zero change out of stuck to false  */
// eslint-disable-next-line consistent-return
function outOfStuckIfQuantityIsZero(quantity) {
  if (quantity === '0') {
    return false;
  }
}

module.exports = {
  findNestedValueByKey,
  escapeRegex,
  randomString,
  existValidation,
  requiredField,
  requiredBasicField,
  requiredOptionField,
  requiredTechnicalField,
  requiredDimensionField,
  requiredEngineField,
  requiredWheelField,
  validateNumber,
  validateDate,
  validateFloatingNumber,
  validateMaxFiveNum,
  validateMaxTenNum,
  validateMaxSeventyNum,
  validateMaxOneHunFiftyChar,
  validateFloatNumLess70,
  validateMax1500Num,
  validateFloatNumLess100,
  validateFloatNum,
  requiredLocationForBook,
  convertGmToKg,
  outOfStuckIfQuantityIsZero
};
