/**
 * Copyright (C) Two Pangra
 */

/**
 * the Try and Catch Error
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const catchAsync = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch((err) => next(err));
};

module.exports = catchAsync;
