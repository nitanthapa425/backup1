const joi = require('joi');

const addToCartValidation = {
  params: joi.object().keys({
    accessoriesId: joi.string().required()
  })
};

module.exports = { addToCartValidation };
