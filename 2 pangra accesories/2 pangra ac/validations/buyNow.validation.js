const joi = require('joi');
// const { objectId } = require('./custom.validation');

const buyNow = {
  body: {
    bookList: joi.array().items({
      color: joi.string().optional(),
      size: joi.string().optional(),
      quantity: joi.string().optional(),
      id: joi.string().optional()
    }),
    address: joi.string().optional(),
    createdAt: joi.date().optional(),
    updatedAt: joi.date().optional()
  }
};

module.exports = {
  buyNow
};
