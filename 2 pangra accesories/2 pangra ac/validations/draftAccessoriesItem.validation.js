const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createDraftAccessoriesItem = {
  body: joi.object().keys({
    brandId: joi.string().required(),
    categoryId: joi.string().required(),
    subCategoryId: joi.string().required(),
    title: joi.string().required(),
    description: joi.string().required().optional().allow(''),
    costPrice: joi.string().required(),
    discountPrice: joi.string().optional().allow(''),
    weight: joi.string().optional(),
    dimension: joi.object().keys({
      length: joi.string().optional().allow(''),
      breadth: joi.string().optional().allow(''),
      height: joi.string().optional().allow('')
    }),
    dimensionUnit: joi.string().optional().valid(['mm', 'cm', 'm']),
    sizeCalculation: joi.string().required(),
    sizeValue: joi.array().required(),
    SKU: joi.string().optional().allow(''),
    quantity: joi.string().required(),
    hasWarranty: joi.boolean().required(),
    warrantyUnit: joi.string().optional(),
    warrantyTime: joi.string().optional(),
    isReturnable: joi.boolean().required(),
    returnableUnit: joi.string().optional(),
    returnableTime: joi.string().optional(),
    outOfStock: joi.boolean().required(),
    tags: joi.string().optional().allow(''),
    tagLine: joi.string().optional().min(3).max(50),
    hasInFeatured: joi.boolean().optional(),
    hasInHotDeal: joi.boolean().optional(),
    isForFlashSale: joi.boolean().optional(),
    status: joi.boolean().optional(),
    createdBy: joi.string().required().optional().allow(''),
    updatedBy: joi.string().required().optional().allow(''),
    createdAt: joi.date().optional(),
    updatedAt: joi.date().optional()
  })
};

module.exports = {
  createDraftAccessoriesItem
};
