const joi = require('joi');
// const { objectId } = require('./custom.validation');
// const constants = require('../constants');

const createVehicleDetailValidate = {
  body: joi.object().keys({
    brandName: joi.string().required(),
    brandDescription: joi.string().required(),
    createdBy: joi.string().required(),
    createdAt: joi.date().required(),
    updatedAt: joi.date().required()
  })
};

module.exports = {
  createVehicleDetailValidate
};
