# 2 Pangra Accessories



## Getting started

To run a project, simply run: npm start

Lint check: npm run lint

Lint fix: npm run lint-fix

## Manual Setup

```bash
git clone https://gitlab.com/tukilogic/2-pangra-accessories-be.git
cd 2-pangra-accessories-be
```

Install the dependencies:

```bash
npm install
```

## Set the environment variables:
```bash
create .env
paste .env.environment content to .env
```

## Environment Variables

The environment variables can be found and modified in the `.env` file. They come with these default values:

```bash
# Port number
PORT=3100

# URL of the Mongo DB
MONGODB_URL=mongodb+srv://admin:admin@cluster0.3rnsz.mongodb.net/2pangra-accessories?retryWrites=true&w=majority

EMAIL_SERVER=mail.tukilogic.com
EMAIL_PORT=465
EMAIL_USER=duipangra@tukilogic.com
EMAIL_PASS=duipangra@123
CLOUDINARY_CLOUD_NAME=doynwnic5
CLOUDINARY_API_KEY=772325534715938
CLOUDINARY_API_SECRET=-kUEVq9egSMerEy5VbgTFULjoeA
messageBirdApiKey=kl7aOGG558KKnc49UV8ppesWs


