/**
 * Copyright (C) Two Pangra
 */

/**
 * The application entry point
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

require('dotenv').config();
require('./bootstrap');

const config = require('config');
const express = require('express');

const cors = require('cors');
// const httpStatus = require('http-status');
const helmet = require('helmet');
// const autoReap = require('multer-autoreap');
const path = require('path');
const compress = require('compression');
const methodOverride = require('method-override');

const bodyParser = require('body-parser');

const app = express();

const http = require('http').Server(app, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST']
  }
});

const io = require('socket.io')(http);

// const ios = require('socket.io-client');
const logger = require('./common/logger');
const errorMiddleware = require('./common/ErrorMiddleware');

// eslint-disable-next-line import/order
const routes = require('./routes');

app.set('port', config.PORT);

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// parse body params and attache them to req.body
app.use(express.json());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(autoReap);

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet.hidePoweredBy());

// gzip compression
app.use(compress());

app.use(config.API_VERSION, routes);

app.use(errorMiddleware());
// app.use('/uploads', express.static('/uploads/two-pangra-img'));
app.use('/', express.static(path.join(__dirname, '/uploads/two-pangra-img')));
// http://localhost:3100/1636961909356_Attendance.PNG

app.use(express.static(path.join(__dirname, '/frontend/build')));

app.use('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/frontend/build/index.html'));
});

io.on('connection', (socket) => {
  logger.info('New Client is Connected!', socket);
});

if (!module.parent) {
  http.listen(app.get('port'), () => {
    logger.info(`Express server listening on port ${app.get('port')}`);
  });
} else {
  module.exports = app;
}

app.locals.io = io;
