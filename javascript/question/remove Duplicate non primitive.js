// let ar1 = [
//   [1, 2, 3],
//   [1, 2, 3],
// ];
//or
let ar1 = [
  { name: "nitan", age: 29, isMarried: false },
  { name: "nitan", age: 29, isMarried: false },
];

//since element is array which is non primitive thus set can not remove duplicate array

// console.log([...new Set(ar1)]);s

let newArr = ar1.map((item, i) => JSON.stringify(item));
console.log([...new Set(newArr)].map((item) => JSON.parse(item)));
