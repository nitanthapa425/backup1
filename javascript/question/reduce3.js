// names = ["Alice", "Bob", "Tiff", "Bruce", "Alice"];
//  it must produce output { 'Alice': 2, 'Bob': 1, 'Tiff': 1, 'Bruce': 1 }

let names = ["Alice", "Bob", "Tiff", "Bruce", "Alice"];

let result = names.reduce((pre, cur) => {
  if (!pre[cur]) {
    pre[cur] = 1;
  } else {
    pre[cur] = pre[cur] + 1;
  }

  return pre;
}, {});

console.log(result);
