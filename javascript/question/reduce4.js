// all book list
const friends = [
  {
    name: "Anna",
    books: ["Bible", "Harry Potter"],
    age: 21,
  },
  {
    name: "Bob",
    books: ["War and peace", "Romeo and Juliet"],
    age: 26,
  },
  {
    name: "Alice",
    // books: ["The Lord of the Rings", "The Shining"],
    age: 18,
  },
];

// it must  produce output
// [
//   'Alphabet', 'Bible', 'Harry Potter', 'War and peace',
//   'Romeo and Juliet', 'The Lord of the Rings',
//   'The Shining'
// ]

// better to use ? insted of &&

let result = friends.reduce((pre, cur) => {
  let arr = [...pre, ...(cur?.books?.length ? cur.books : [])];
  return arr;
}, []);

console.log(result);
