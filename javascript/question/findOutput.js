let ar1 = [1, 2, 3];
let ar2 = [2, 3];

//output must be [2,4,6,3,6,9]

let ar3 = [];

ar2.forEach((value, i) => {
  ar1.forEach((item, i) => {
    ar3.push(value * item);
  });
});

console.log(ar3);
