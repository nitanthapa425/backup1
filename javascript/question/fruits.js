let fruits = [
  { Apple: 1, Orange: 7, Grapes: 3 },
  { Grapes: 6, Lemon: 4, Banana: 8 },
  { Orange: 5, Pineapple: 7, Apple: 1 },
];

// it must produce output
// {
//     Apple:2,
//     Orange:12,
//     Grapes:9,
//     Lemon:4,
//     Banana:8
//     Pineapple:7
// }

let result = fruits.reduce((pre, cur) => {
  let keys = Object.keys(cur);

  keys.forEach((key, i) => {
    let preValue = pre[key] || 0;
    pre[key] = preValue + cur[key];
  });

  return pre;
}, {});

console.log(result);
