let ar1 = [
  {
    name: "nitan",
    gender: "male",
  },
  {
    name: "sita",
    gender: "female",
  },
  {
    name: "hari",
    gender: "male",
  },
  {
    name: "gita",
    gender: "female",
  },
  {
    name: "utshab",
    gender: "other",
  },
];
// output must {
// male:[
// {
// name:"nitan"
// gender:"male"
// },
// {
// name:"hari"
// gender:"male"
// }
// ],
// female:[
// {
// name:"gita"
// gender:"female"
// },
// {
// name:"sita"
// gender:"female"
// }
// ],
// other:[
// {
// name:"utshab"
// gender:"other"
// }],
// {

let first = () => {
  let result = ar1.reduce((pre, cur) => {
    if (cur.gender === "male") {
      if (!pre.male) {
        return {
          ...pre,
          male: [cur],
        };
      } else {
        return {
          ...pre,
          male: [...pre.female, cur],
        };
      }
    } else if (cur.gender === "female") {
      if (!pre.female) {
        return {
          ...pre,
          female: [cur],
        };
      } else {
        return {
          ...pre,
          female: [...pre.female, cur],
        };
      }
    }
    if (cur.gender === "other") {
      if (!pre.other) {
        return {
          ...pre,
          other: [cur],
        };
      } else {
        return {
          ...pre,
          other: [...pre.female, cur],
        };
      }
    }
  }, {});
  console.log(result);
};
let second = () => {
  let result = ar1.reduce((pre, cur) => {
    if (cur.gender === "male") {
      return {
        ...pre,
        ...(pre?.male?.length ? { male: [...pre.male, cur] } : { male: [cur] }),
      };
    } else if (cur.gender === "female") {
      return {
        ...pre,
        ...(pre?.female?.length
          ? { female: [...pre.female, cur] }
          : { female: [cur] }),
      };
    } else if (cur.gender === "other") {
      return {
        ...pre,
        ...(pre?.other?.length
          ? { other: [...pre.other, cur] }
          : { other: [cur] }),
      };
    }
  }, {});
  console.log(result);
};

let third = () => {
  let result = ar1.reduce((pre, cur) => {
    if (cur.gender === "male") {
      return {
        ...pre,
        male: [...(pre?.male?.length ? [...pre.male, cur] : [cur])],
      };
    } else if (cur.gender === "female") {
      return {
        ...pre,
        female: [...(pre?.female?.length ? [...pre.female, cur] : [cur])],
      };
    } else if (cur.gender === "other") {
      return {
        ...pre,
        other: [...(pre?.other?.length ? [...pre.other, cur] : [cur])],
      };
    }
  }, {});
  console.log(result);
};

let fourth = () => {
  let result = ar1.reduce(
    (pre, cur) => {
      if (cur.gender === "male") {
        return {
          ...pre,
          male: [...pre.male, cur],
        };
      } else if (cur.gender === "female") {
        return {
          ...pre,
          female: [...pre.female, cur],
        };
      } else if (cur.gender === "other") {
        return {
          ...pre,
          other: [...pre.other, cur],
        };
      }
    },
    { male: [], female: [], other: [] }
  );
  console.log(result);
};

let fifth = () => {
  let result = ar1.reduce(
    (previous, current) => {
      //   previous[current.gender] = [...previous[current.gender], current];
      previous[current.gender].push(current);
      return previous;
    },
    { male: [], female: [], other: [] }
  );

  console.log(result);
};
// first();
// second();
// third();
// fourth();
// fifth();
