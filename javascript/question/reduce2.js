//fiding longest word form the given array
let arr1 = ["ram", "nitan", "nitanthapa", "saujan"];

// let longestWord = (arr1) => {
//   let _longestWord = arr1.reduce((pre, curr) => {
//     if (pre.length > curr.length) {
//       return pre;
//     } else {
//       return curr;
//     }
//   }, "");
//   return _longestWord;
// };

let longestWord = (arr1) => {
  let _longestWord = arr1.sort((a, b) => b.length - a.length);
  return _longestWord[0];
};
console.log(longestWord(arr1));
