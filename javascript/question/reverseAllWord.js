let word = "my name is nitan thapa";

// reverse whole word
//output must be "ym eman si natin apaha"

let arr = word.split(" ");
let reverseWordArr = arr.map((value, i) => {
  let reverseValue = value.split("").reverse().join("");
  return reverseValue;
});

let reverseWordString = reverseWordArr.join(" ");
console.log(reverseWordString);
