/**
 * checking  array or not
 * let inp = "ram";//false
 * let inp = {};//false
 * let inp = [];//true
 *
 *
 */

let inp = [1, 2, 3]; //true

console.log("isArray", Array.isArray());

/**
 * checking  Object or not
 * let inp = "ram";//false
 * let inp = {};//false
 * let inp = [];//true
 *
 *
 */

let inpp = () => {};

console.log("isObject", typeof inpp === "object" && !Array.isArray(inpp));
