var fun1 = () => {
  var a = 2;
  const fun2 = () => {
    var a = 3;
    console.log(a);
    // for different block different memory space is created
    //first it always try to modify its local variable then ints see its parent then grand parent
  };

  fun2();
  console.log(a);
  //   console.log(b); through error
};

fun1();
