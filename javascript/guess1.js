let errorHandling = function () {
  try {
    let error = new Error("error message");
    // to add something in error (as error is object)
    error.k = "nitan";
    error.sta = 400; //it is useful
  } catch (error) {
    //to get k an sta
    console.log(error.message);
    console.log(error.a);
    console.log(error.sta);
  }
};
errorHandling();

// let ram = function () {
//   var a, b;
//   try {
//     throw new error("errrr");
//   } catch (error) {
//     error = 5;
//     console.log(a);
//     a = 11;
//     b = 10;
//     console.log(error);
//   }
//   console.log(a);
//   console.log(b);
// };
// ram();

let fun1 = function () {
  var a, b;

  let fun2 = () => {
    console.log(a, b);
  };

  fun2();
};
fun1();
