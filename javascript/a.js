let fun = (obj1, delArr1) => {
  let obj2 = { ...obj1 };
  let func1 = (value, i) => {
    delete obj2[value]; //delete obj1["name"]
  };

  let r = delArr1.map(func1);

  return obj2;
};

let obj = {
  name: "nitan", //"name":"nitan"//["name"]:"nitan"
  age: "28",
  isMarried: false,
};

let deleteArray = ["name", "isMarried"];

let abnotherObj = fun(obj, deleteArray);

console.log(abnotherObj);
console.log(obj);
