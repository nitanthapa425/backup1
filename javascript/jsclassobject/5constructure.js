class Check {
  name;
  age;
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  start() {
    console.log("start");
  }
}

let checkObj = new Check("nitan", 30); // same as constructor("nitan",age)
console.log(checkObj);
console.log(new Check());

//when a object of class is made constructor is automatically invoked
