// class first letter is capital
//making class Check
class Check {
  name = "nitan";
  start() {
    console.log("start");
  }
  end() {
    console.log("end");
  }
}

//making object of that class
let checkObj1 = new Check(); //her object is made with property name and method start()  and ends()
// console.log(checkObj1);
// console.log(checkObj1.name);
// checkObj1.start();
// checkObj1.end();

let checkObj2 = new Check(); //her object is made with property name and method start()  and ends()
console.log(checkObj2);
