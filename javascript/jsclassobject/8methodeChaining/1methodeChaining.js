class Counter {
  count1 = 0;

  incrementCount1() {
    this.count1 += 1;
    return this; // it is done for method chaining
  }
  decrementCount1() {
    this.count1 -= 1;
    return this;
  }
}

let counter1 = new Counter();
counter1.incrementCount1().incrementCount1().incrementCount1();
console.log(counter1);
counter1.decrementCount1();
console.log(counter1);
