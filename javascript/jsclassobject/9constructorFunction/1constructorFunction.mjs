//User is called constructor function
// by default it will return a object
function User(customerName, balance) {
  this.customerName = customerName;
  this.balance = balance;
  this.accountNumber = Date.now();
  this.deposit = function (amount) {
    this.balance += amount;
  };
  //don't return any thing
  // by default when new User(a) is called an object is return
}

let user1 = new User("nitan", 1000);
user1.deposit(1000);
console.log(user1);

// let user = new User("nitan", "thapa");
// user.fullName();

//here User is a constructor function (which return object by default)
