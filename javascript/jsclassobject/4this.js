class Check {
  name = "nitan";
  surname = "thapa";
  fullName(name, surname) {
    this.name = name;
    this.surname = surname;
  }
}

//this always point its own object
//generally this is associated with function

let check1 = new Check(); //{name:"nitan",surname:"thapa",fullName:..}
let check2 = new Check(); //{name:"nitan",surname:"thapa",fullName:..}

check2.fullName("kapil", "shrestha");
console.log(check2);
console.log(check1);
