class Person {
  name = "nitan";
  eat() {
    console.log("eat person");
  }
}

class Engineer extends Person {
  name = "ram";
  work() {
    console.log("design");
  }
  eat() {
    console.log("eat engineer");
  }
}

let engineerObj = new Engineer(); //{name:"ram",eat:eat(){console.log("eat engineer")}, work }
console.log(engineerObj);
console.log(engineerObj.name); //ram
engineerObj.eat(); //"eat engineer"
// child property  and method will override parent (onw property and method has higher preference)
//this is called overriding  (method overriding or property overriding)
