class Person {
  eat() {
    console.log("eat");
  }
  sleep() {
    console.log("sleep");
  }
}

class Engineer extends Person {
  work() {
    console.log("design");
  }
}

let engineerObj = new Engineer(); //{eat, sleep, work}
console.log(engineerObj);
//inheritance is passing down properties and method from parent to child class
