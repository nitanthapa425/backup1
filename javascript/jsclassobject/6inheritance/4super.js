class Person {
  constructor(name) {
    this.name = name;
    console.log(" i am person constructor");
  }
  eat() {
    console.log("person eat");
  }
  sleep() {
    console.log("person sleep");
  }
}

// super is used to indicate parent
class Engineer extends Person {
  constructor(name) {
    console.log("i am Engineer constructor");
    super(name); //trigger parent constructor
  }

  eat() {
    console.log("engineer eat");
  }

  work() {
    // super.eat(); //it is parent  => gives person eat
    // this.eat(); //=> gives engineer eat
    super.sleep(); //gives  person sleep
    this.sleep; // gives person sleep as (it does not have)

    console.log("analyze");
  }
}

let engineerObject = new Engineer("nitan");
engineerObject.work();
