// constructor overriding is same as method overriding (no difference)
class Person {
  constructor() {
    console.log("hello i am person constructor");
  }
}

class Engineer extends Person {}

class Doctor extends Person {
  constructor() {
    console.log("hello i am doctor constructor");
  }
}

// let engineerObj = new Engineer(); // it will run Person constructor
let doctorObj = new Doctor(); // it will run Doctor constructor
