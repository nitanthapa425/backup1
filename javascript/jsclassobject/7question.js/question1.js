//make a class User (name, lastName, viewDetails)
//make a class Teacher (name, lastName, viewDetails, job)
//make a class Student(name, lastName, viewDetails, job)

class User {
  name;
  lastName;
  constructor(name, lastName) {
    this.name = name;
    this.lastName = lastName;
  }
  viewDetails() {
    return `my full name is ${this.name} ${this.lastName}`;
  }
}
class Student extends User {
  constructor(name, lastName) {
    super(name, lastName);
  }
  job() {
    return "i am a learner";
  }
}
class Teacher extends User {
  constructor(name, lastName) {
    super(name, lastName);
  }
  job() {
    return "i am a teacher";
  }
}

let teacherObj = new Teacher("nitan", "thapa");
console.log(teacherObj.viewDetails());
console.log(teacherObj.job());
let studentObj = new Student("manish", "khadka");
console.log(studentObj.viewDetails());
console.log(studentObj.job());
