// primitive type
let name1:string="nitan"
let age:number=30
let check:any = 10//any means you can give any data
let u:undefined= undefined
let n:null= null


// non primitive such as array object

//defining array of number
let list:number[]=[1,2,3]
let list1:string[]=["1","2","3"]
let list2:boolean[]=[true,false]
let list3:(boolean|number|string)[]=[true,false,1,"nitan"]


//defining object 
let obj1:{
    name:string,
    age:number,
    isMarried:boolean
} ={name:"nitan",age:30, isMarried:false}





// union types
let a:string|number="nitan"



//literal types
// we can specify the value for variable
let b:true|1|"1"=1
let gender:"male"|"female"="male"
let a1:string|1|true=1 //it means a1 can be any sring or 1 or true

//tuple
//we can specify type of specific element
let x:[string, number]=["abc",1]


//function
function fun1(a:string,b:string,c?:string):number{
//  c is either optional or string
//we cand define optional  argument by ?:
    return 10
}

fun1("nitan","thapa")


 function fun2():void{
    //if function does not return any thing use void
    console.log("****")

}


// defining type of function for const and normal function
let fun3:()=>number= ()=>{
    return 1
}
fun3()

let fun4:()=>number= function(){
    return 1
}
fun3()


//defining object with function 
let obj11:{
    name:string,
    age:number,
    isMarried:boolean,
    // ()=>it means any function either arrow or normal
    fun1:()=>void,
    fun2:()=>number,
    fun3:()=>void
} ={
    name:"nitan",age:30, isMarried:false,
    fun1:()=>{  },
    fun2:()=>{return(10)},
    fun3:function(){}

    }


// custom type  (use type word to define type)
//use pascal word convension to define your type
type MyFirstType= number|string|true
type Gender = "male"|"female"|"other"

// try to use interface to defin type (for best practice)
type Info = {
    name:string,
    age:number,
    isMarried?:boolean
}

let  customTypeEg:MyFirstType = 15
let custumTypeEg2:Gender="male"
let myInfo:Info={name:"nitan",age:30, isMarried:false}
let myInfo1:Info={name:"nitan",age:30}


//intersection
type MyType1={
    name:string,
}
type MyType2={
    age:number
}
type MyType= MyType1&MyType2&{
    isMarried:boolean
}

let myData:MyType ={
    name:"nitan",
    age:30,
    isMarried:false
}




// interface  (same as defining type)

//interface is mainly for defining types for object only
interface  Animal{
    name:string,

}
interface Animal{
    color:"black"|"white"

}
// here now totalAnimal will be contain name and color

interface Bear extends Animal {
    food:"fish"|"grass"

}

let bearData:Bear = {
    name:"Polar Bear",
    color:"black",
    food:"fish"


}


// question for practice
//define array of object
interface Person {
    name:string,
    age:number,
    isMarried:boolean,
    hobbies?:string[]
}

let person:Person ={
    name:"nitan",
    age:30,
    isMarried:false,
    // hobbies:["cooking","teaching",1]
}



//class

class MatchScore{
    //private, public, static
   static score=1
//if a vairable is static every object of the class share same memory
    public getScore(){

        console.log(MatchScore.score)
    }

    public incrementScore(){


MatchScore.score=MatchScore.score+1
        
    }

}

let match1= new MatchScore()
match1.getScore()

let match2= new MatchScore()
match1.incrementScore()

match1.getScore()
match2.getScore()



//Generic
//control of type during function call



// functional generic

function swap<T>(a:T,b:number){
    //Now T is number if number is passed
    //You can change T

    let name:T
    return 10

}

swap<number>(10,11)









