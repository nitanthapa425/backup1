// 1st

// let promiseFun = (resolve, reject) => {
//   let error = false;
//   console.log("i am not push to background but i wll run in same line");
//   if (error) {
//     reject("reject");
//   } else {
//     resolve("resolve");
//   }
// };
// let onResolved = (result) => {
//   console.log("i will run after promise is resolved,", result);
// };

// let onReject = (error) => {
//   console.log("i will run after promise is rejected,", error);
// };

// console.log("start");
// let prom = new Promise(promiseFun);
// console.log("start1");
// console.log(prom);
// prom.then(onResolved).catch(onReject);
// // here note onResolved and onReject function is asynchronous function because it is pushed to background
// //but promiseFun is not asynchronous function
// //afterResolve will run when promise is resolve (and call stack is free)
// //onReject will run when promise is reject (and call stack is free)
// // console.log(newProm);

// console.log("end");

// 2nd

let promiseFun = (resolve, reject) => {
  let error = false;
  console.log("i am not push to background but i wll run in same line");
  if (error) {
    setTimeout(() => {
      reject("reject");
    }, 2000);
  } else {
    setTimeout(() => {
      resolve("resolve");
    }, 2000);
  }
};
let onResolved = (result) => {
  console.log("i will run after promise is resolved,", result);
};

let onReject = (error) => {
  console.log("i will run after promise is rejected,", error);
};

console.log("start");
// when promises is created the promiseFun start to work
//promiseFun may or may not takes time (depending on timeout used or data fetch)
let prom = new Promise(promiseFun);
console.log("start1");
console.log(prom);
//attaching onResolve and onReject unction
prom.then(onResolved).catch(onReject);

console.log("end");
