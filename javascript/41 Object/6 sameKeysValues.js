//object with sam keys and values
let name1 = "nitan";
let age = "27";
// let obj = { name1:name1, age:age1 };
//or
let obj = { name1, age };
console.log(obj);

// Note it the  below code are wrong because the value must be variable (it shoud not be data)
// let obj1 = { name: "name", age: "age" };
// or
// let obj = { name, age };
