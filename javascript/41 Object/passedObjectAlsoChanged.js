let fun = (obj1) => {
  let obj2 = obj1;
  obj2.age = "28";

  return obj2;
};
let obj = { name: "nitan" };
console.log("orginal", obj);

console.log(fun(obj));
console.log("obj after passed", obj);
