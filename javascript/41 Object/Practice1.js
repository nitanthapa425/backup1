let age = "agee";
obj = {
  name: "nitan thapa",
  isMarried: false,
  favMovies: ["hobbit", "got"],
  favPerson: {
    n1: "name",
  },
  [age]: 28,
  //   here age is variable
  //this type of syntax is used to make age key dynamic
};

console.log(obj);
console.log(obj.name);
console.log(obj["name"]); //note obj[data] //here there must be data(string....)
console.log(obj[age]); //note we do not call like obj.[age]
console.log(obj["agee"]); //it is also possible it is same as obj[age]  ===obj["agee"]
