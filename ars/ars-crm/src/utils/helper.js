const findNestedValueByKey = (entireObj, keyToFind) => {
  const outputData = [];
  JSON.stringify(entireObj, (_, nestedValue) => {
    if (nestedValue && nestedValue[keyToFind]) {
      outputData.push(nestedValue[keyToFind]);
    }
    return nestedValue;
  });
  return outputData;
};

const escapeRegex = (string) => {
  return string.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
};

const randomString = (length, chars) => {
  let result = '';
  // eslint-disable-next-line no-plusplus
  for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

module.exports = {
  findNestedValueByKey,
  escapeRegex,
  randomString,
};
