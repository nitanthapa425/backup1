const Joi = require('joi');

const collectionBody = {
  body: Joi.object()
    .keys({
      name: Joi.string().required().min(4).max(100),
      refLogo: Joi.string().allow('').allow(null),
      order: Joi.number().min(0),
      status: Joi.string().allow('').allow(null),
      storeIds: Joi.array(),
    })
    .unknown(true),
};

module.exports = { collectionBody };
