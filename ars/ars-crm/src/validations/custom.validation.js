const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo object id');
  }
  return value;
};

const capitalAndNumber = (value, helpers) => {
  if (value.match(/[a-zA-Z0-9]*/g).length !== 2) {
    return helpers.message('Code Must be Combination of Letters and Number Only');
  }
  return value;
};

// Update estimatedCost
const estimatedCostValidation = (value) => {
  if (!value.customerInteger || !value.cost) {
    // if any Value has empty change estimatedCost to null
    return null;
  }
  return value;
};

const priceValidation = (value, helpers) => {
  if (!value.match(/[\d]+.[\d]{3}$/g)) {
    return helpers.message('price must have 3 decimal. price must be on XXX.XXX format.');
  }
  return value;
};

const phoneValidation = (value, helpers) => {
  if (!value.match(/^[+]{1}[0-9]{1,4}[-\s./0-9]*$/)) {
    return helpers.message('phone must +XXX-XXXXXXXX or +XXXXXXXXXXX pattern');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 8) {
    return helpers.message('password must be at least 8 characters');
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message('password must contain at least 1 letter and 1 number');
  }
  return value;
};

module.exports = {
  objectId,
  password,
  capitalAndNumber,
  priceValidation,
  phoneValidation,
  estimatedCostValidation,
};
