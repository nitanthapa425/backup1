const Joi = require('joi');
const { password } = require('./custom.validation');
const role = require('../config/firebase/role.json');

const register = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    displayName: Joi.string().required().min(6),
    role: Joi.string()
      .required()
      .valid(...Object.values(role)),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    // TODO CUSTOM ERROR MESSAGE
    password: Joi.string().required().custom(password),
  }),
};

const logout = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().email().required(),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const firebaseUser = {
  query: Joi.object().keys({
    limit: Joi.number().integer().min(1).max(999).optional(),
    pageToken: Joi.string().min(1).max(32).optional(),
  }),
};

const changePassword = {
  body: Joi.object().keys({
    uid: Joi.string().required(),
    password: Joi.string().min(6).max(24).required(),
  }),
};
module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  firebaseUser,
  changePassword,
};
