const Joi = require('joi');

const pagination = {
  query: Joi.object().keys({
    limit: Joi.number().integer().min(1).max(999).optional(),
    page: Joi.number().allow('').min(1).optional(),
    search: Joi.string().min(1).allow('').max(999).optional(),
    sortBy: Joi.string().allow('').optional(),
    sortOrder: Joi.string().allow('').valid('1', '-1').optional(),
    select: Joi.string().allow('').optional(),
    noRegex: Joi.string().allow('').optional()
  })
};

module.exports = {
  pagination
};
