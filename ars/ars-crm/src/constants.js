/**
 * Copyright (C) Tuki Logic
 */

/**
 * Application related constants will be stored in this file
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const ACCESS_LEVEL = {
  SUPER_ADMIN: 'SUPER_ADMIN',
  ADMIN: 'ADMIN',
  USER: 'USER'
};
const POSITIONS = {
  DIRECTOR: 'DIRECTOR',
  SALES_COORDINATOR: 'SALES_COORDINATOR',
  SALES_MANAGER: 'SALES_MANAGER',
  SALES_ASSOCIATES: 'SALES_ASSOCIATES',
  OTHER: 'OTHER'
};
const TITLES = {
  Mr: 'Mr',
  Ms: 'Ms',
  Mrs: 'Mrs',
  Miss: 'Miss'
};

const MARITAL_STATUS = {
  Married: 'Married',
  Single: 'Single',
  Partner: 'Partner',
  Divorced: 'Divorced',
  Widowed: 'Widowed',
  Separated: 'Separated',
  Others: 'Others'
};

const CUSTOMER_TYPE = {
  Clients: 'Clients',
  Prospect: 'Prospect',
  Others: 'Others'
};
const CLIENT_STATUS = {
  Active: 'Active',
  Inactive: 'Inactive',
  Others: 'Others'
};

const MIME_TYPE_MAP = {
  'text/plain': 'txt',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'docx',
  'application/pdf': 'pdf'
};

// default data fetch limit
const DefaultQueryLimit = 10;

module.exports = {
  DefaultQueryLimit,
  ACCESS_LEVEL,
  TITLES,
  MARITAL_STATUS,
  CUSTOMER_TYPE,
  MIME_TYPE_MAP,
  CLIENT_STATUS,
  POSITIONS
};
