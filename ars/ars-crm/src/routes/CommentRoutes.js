/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Comment Routes
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports = {
  '/comment': {
    get: {
      controller: 'CommentController',
      method: 'getComment'
    },
    post: {
      controller: 'CommentController',
      method: 'createComment',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/comment/:id': {
    put: {
      controller: 'CommentController',
      method: 'updateCommentById',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    },
    delete: {
      controller: 'CommentController',
      method: 'deleteCommentById',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  }
};
