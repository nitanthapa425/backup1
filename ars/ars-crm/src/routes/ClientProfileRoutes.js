/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Client Profile Route
 *
 * @author      Ashim Karki
 * @version     1.0
 */

module.exports = {
  '/clients': {
    get: {
      controller: 'ClientProfileController',
      method: 'searchClientProfiles'
    },
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfile',
      upload: true,
      accessLevel: ['SUPER_ADMIN', 'ADMIN'],
      calendarApi: true
    }
  },
  '/clients/:id': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientProfile'
    },
    put: {
      controller: 'ClientProfileController',
      method: 'updateClientProfile',
      upload: true,
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    },
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteClientProfile',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clients/:id/followUpNotes': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientProfileFollowUpNotes'
    },
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfileFollowUpNote',
      accessLevel: ['SUPER_ADMIN', 'ADMIN'],
      calendarApi: true
    }
  },
  '/clients/:id/followUpNotes/:noteId': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientProfileFollowUpNoteById'
    },
    put: {
      controller: 'ClientProfileController',
      method: 'updateClientProfileFollowUpNoteById',
      accessLevel: ['SUPER_ADMIN', 'ADMIN'],
      calendarApi: true
    },
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteClientProfileFollowUpNoteById',
      accessLevel: ['SUPER_ADMIN'],
      calendarApi: true
    }
  },
  '/clients/:id/otherDetails/:transactionId': {
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfileOtherDetails',
      upload: true,
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clients/:id/otherDetails/:id': {
    put: {
      controller: 'ClientProfileController',
      method: 'updateClientProfileOtherDetails',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clients/:id/getOtherDetails/:otherId': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientProfileOtherDetailsById'
    }
  },
  '/clients/:id/transactionDetails/:transactionId/otherDetails/:otherId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteClientProfileOtherDetails',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/customerDetails': {
    get: {
      controller: 'ClientProfileController',
      method: 'getTotalClientsType'
    }
  },
  '/clients/:id/transactionDetails': {
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfileTransactionDetails',
      upload: true
    }
  },
  '/clients/:id/transactionDetails/:transactionId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteClientProfileTransactionDetails',
      accessLevel: ['SUPER_ADMIN']
    },
    post: {
      controller: 'ClientProfileController',
      method: 'addTransactionNoteController'
    }
  },
  '/clients/:id/transactionDetails/:transactionId/correspondece/:correspondeceId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteClientProfileTransactionDetails',
      accessLevel: ['SUPER_ADMIN']
    },
    post: {
      controller: 'ClientProfileController',
      method: 'addCorrespondenceNoteController'
    }
  },
  '/clients/:id/transactionDetails/:transactionId/correspondece/:correspondeceId/note/:noteId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteCorrespondingNoteController',
      accessLevel: ['SUPER_ADMIN']
    },
    put: {
      controller: 'ClientProfileController',
      method: 'updateCorrespondenceNoteController',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clients/:id/transactionDetails/:transactionId/correspondeceFile/:correspondeceId': {
    post: {
      controller: 'ClientProfileController',
      method: 'addCorrespondenceFileController',
      upload: true
    }
  },
  '/clients/:id/transactionDetails/:transactionId/note/:noteId': {
    put: {
      controller: 'ClientProfileController',
      method: 'updateTransactionNoteController',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    },
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteTransactionNoteController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clients/:id/transactionDetails/:transactionId/corresponding': {
    post: {
      controller: 'ClientProfileController',
      method: 'addCorrespondingInTransactionController',
      upload: true
    }
  },
  '/clients/:id/transactionDetails/:transactionId/correspondece/:correspondeceId/file/:fileId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteCorrespondingFileController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/referralsClient': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientsDetailsToReferral',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clientTransactionDetails': {
    get: {
      controller: 'ClientProfileController',
      method: 'getTransactionDetails'
    }
  },
  '/clients/:id/transactionDetailAndCorrespondence': {
    get: {
      controller: 'ClientProfileController',
      method: 'getTransactionDetailAndCorrespondenceController'
    }
  },
  '/clientFollowupNote': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientFollowupNote'
    }
  },
  '/clientAssignToMe': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientAssignToMe'
    }
  },
  '/clients/:id/otherAddFile/:otherDetailId': {
    post: {
      controller: 'ClientProfileController',
      method: 'updateClientProfileAddFile',
      upload: true,
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clients/:id/otherDeleteFile/:otherId/file/:fileId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteOtherFileController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clientsWithoutValidation': {
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfileWithoutValidation',
      upload: true,
      calendarApi: true,
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/clients/:id/otherDetails': {
    post: {
      controller: 'ClientProfileController',
      method: 'createClientProfileOtherDetailController',
      upload: true
    }
  },
  '/clients/:id/otherDetails/:otherId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteOtherDetailsController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clients/:id/newOtherDetails/:otherId': {
    post: {
      controller: 'ClientProfileController',
      method: 'addOtherFileController',
      accessLevel: ['SUPER_ADMIN'],
      upload: true
    }
  },
  '/clients/:id/noteOtherDetails/:otherId': {
    post: {
      controller: 'ClientProfileController',
      method: 'addOtherNoteController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clients/:id/noteOtherDetails/:otherId/note/:noteId': {
    delete: {
      controller: 'ClientProfileController',
      method: 'deleteOtherNoteController',
      accessLevel: ['SUPER_ADMIN']
    },
    put: {
      controller: 'ClientProfileController',
      method: 'updateNewOtherNoteController',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clients/:id/completeNote/:completedId': {
    put: {
      controller: 'ClientProfileController',
      method: 'completeFollowUpNoteController'
    }
  },
  '/clients/:id/incompleteNote/:completedId': {
    put: {
      controller: 'ClientProfileController',
      method: 'incompleteFollowUpNoteController'
    }
  },
  '/clients/:id/getOtherDetailsofClient': {
    get: {
      controller: 'ClientProfileController',
      method: 'getClientProfileOtherDetails'
    }
  }
};
