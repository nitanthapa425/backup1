/**
 * Copyright (C) Tuki Logic
 */

/**
 * the User Route
 *
 * @author      Ashim Karki
 * @version     1.0
 */

module.exports = {
  '/user/password': {
    post: {
      controller: 'UserController',
      method: 'changePassword'
    },
    put: {
      controller: 'UserController',
      method: 'updatePassword'
    }
  },
  '/user/password/:id': {
    put: {
      controller: 'UserController',
      method: 'updatePasswordByAdmin',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/user/me': {
    get: {
      controller: 'UserController',
      method: 'userProfile'
    },
    put: {
      controller: 'UserController',
      method: 'updateProfile'
    }
  },
  '/users': {
    get: {
      controller: 'UserController',
      method: 'searchUserProfiles'
    }
  },
  '/user/:id': {
    get: {
      controller: 'UserController',
      method: 'getUserProfileById'
    },
    put: {
      controller: 'UserController',
      method: 'updateUserProfileById',
      accessLevel: ['SUPER_ADMIN']
    },
    delete: {
      controller: 'UserController',
      method: 'deleteUserProfileById',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/revertUser/:id': {
    put: {
      controller: 'UserController',
      method: 'revertDeleteUserProfileById',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/user/:id/emergencyContactPerson': {
    get: {
      controller: 'UserController',
      method: 'getEmergencyContactPerson',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/clientAssignToMe': {
    get: {
      controller: 'UserController',
      method: 'getClientAssignToMe'
    }
  },
  '/uploadProfilePicture': {
    post: {
      controller: 'UserController',
      method: 'uploadProfilePictureController',
      upload: true
    }
  }
};
