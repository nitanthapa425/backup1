const express = require('express');
const { userProfileController } = require('../controllers');
const validate = require('../middlewares/validate');
const {
  expressAuthentication,
  isAuthenticated,
  isAuthorized,
} = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router
  .route('/user')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    userProfileController.getUserProfileList
  );
  router
  .route('/deletedUser')
  .get(
    validate(paramsValidation.pagination),
    expressAuthentication,
    isAuthenticated,
    isAuthorized(['SUPER_ADMIN', 'ADMIN']),
    paginate,
    userProfileController.getDeletedUserProfileList
  );
module.exports = router;
