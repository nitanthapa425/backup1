/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Calendar Route
 *
 * @author
 * @version     1.0
 */

module.exports = {
  // '/user/password': {
  //   post: {
  //     controller: 'UserController',
  //     method: 'changePassword',
  //   },
  // },
  '/calendar': {
    get: {
      controller: 'CalendarController',
      method: 'getEvents',
      calendarApi: true
    }
  },
  '/calendar/create': {
    post: {
      controller: 'CalendarController',
      method: 'createEvent',
      calendarApi: true
    }
  },
  '/calendar/refresh-token': {
    get: {
      controller: 'CalendarController',
      method: 'setToken',
      public: true
    }
    //   put: {
    //     controller: 'UserController',
    //     method: 'updateProfile',
    //   },
  },
  '/calendar/:id': {
    delete: {
      controller: 'CalendarController',
      method: 'deleteCalendar',
      calendarApi: true
    },
    put: {
      controller: 'CalendarController',
      method: 'updateCalendar',
      calendarApi: true
    }
  }
};
