/**
 * Copyright (C) Tuki Logic
 */

/**
 * Defines the API routes
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const express = require('express');
const expressJwt = require('express-jwt');
const _ = require('lodash');
const multer = require('multer');
const errors = require('common-errors');
const { v4: uuidv4 } = require('uuid');
const config = require('config');
const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const SecurityRoutes = require('./SecurityRoutes');
const ClientProfileRoutes = require('./ClientProfileRoutes');
const UtilRoutes = require('./UtilRoutes');
const CategoryRoutes = require('./CategoryRoutes');
const UserRoutes = require('./UserRoutes');
const CalendarRoutes = require('./CalendarRoutes');
const Comment = require('./CommentRoutes');
const helper = require('../common/helper');
const logger = require('../common/logger');
const models = require('../models');
const { MIME_TYPE_MAP } = require('../constants');
const clientProfileRoute = require('./clientProfile.route');
const UserRoute = require('./user.route');
const GlobalSearchRoutes = require('./GlobalSearchRoutes');

/* calendar imports */

const SCOPES = ['https://www.googleapis.com/auth/calendar'];
const TOKEN_PATH = './src/common/token.json'; // store in db
const CREDENTIALS_PATH = './src/common/credentials.json'; // store in db
//

const upload = multer({
  storage: multer.diskStorage({
    destination(req, file, cb) {
      cb(null, './uploads/client-profile-docs');
    },
    filename(req, file, cb) {
      // const ext = MIME_TYPE_MAP[file.mimetype];
      cb(null, `{{${uuidv4()}}}${file.originalname}`);
    }
  })
});

const apiRouter = express.Router();
const routes = _.extend(
  {},
  SecurityRoutes,
  UserRoutes,
  ClientProfileRoutes,
  UtilRoutes,
  CategoryRoutes,
  CalendarRoutes,
  Comment,
  GlobalSearchRoutes
);
// load all routes
_.each(routes, (verbs, url) => {
  _.each(verbs, (def, verb) => {
    const actions = [
      (req, res, next) => {
        req.signature = `${def.controller}#${def.method}`;
        next();
      }
    ];
    const method = require(`../controllers/${def.controller}`)[def.method]; // eslint-disable-line

    if (!method) {
      throw new Error(`${def.method} is undefined, for controller ${def.controller}`);
    }
    if (!def.public) {
      // authentication
      actions.push(
        expressJwt({
          secret: config.JWT_SECRET,
          algorithms: ['HS256']
        })
      );
      actions.push((req, res, next) => {
        if (!req.user) {
          return next(new errors.AuthenticationRequiredError('Authorization failed.'));
        }
        return next();
      });

      // check auth token is there in db or not
      actions.push(async (req, res, next) => {
        try {
          const user = await models.User.findOne({
            _id: req.user.id
          });
          if (!user.accessToken) {
            return next(new errors.AuthenticationRequiredError('token is invalid'));
          }
        } catch (ex) {
          return next(new errors.ValidationError('token is invalid'));
        }
        return next();
      });

      // authorization
      if (def.roles && def.roles.length > 0) {
        actions.push((req, res, next) => {
          if (_.indexOf(def.roles, req.user.role) < 0) {
            next(new errors.NotPermittedError('Invalid role.'));
          } else {
            next();
          }
        });
      }
    }

    if (def.upload) {
      actions.push(upload.array('files'));
    }

    if (def.calendarApi) {
      /* calender middleware: start */
      actions.push((req, res, next) => {
        fs.readFile(CREDENTIALS_PATH, (err, content) => {
          if (err) return console.log('Error loading client secret file:', err);
          // Authorize a client with credentials, then call the Google Calendar API.
          const { client_secret, client_id, redirect_uris } = JSON.parse(content).web;
          const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
          // // Check if we have previously stored a token.
          fs.readFile(TOKEN_PATH, (err, token) => {
            if (err) {
              const authUrl = oAuth2Client.generateAuthUrl({
                access_type: 'offline',
                scope: SCOPES
              });

              const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
              });
              console.log(authUrl);
              rl.question('Enter the code from that page here: ', (code) => {
                rl.close();
                oAuth2Client.getToken(code, (err, token) => {
                  if (err) return console.error('Error retrieving access token', err);
                  oAuth2Client.setCredentials(token);
                  // Store the token to disk for later program executions
                  fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                    if (err) return console.error(err);
                  });
                  req.oAuth2Client = oAuth2Client;
                  next();
                  // callback(oAuth2Client);
                });
              });
            } else {
              oAuth2Client.setCredentials(JSON.parse(token));
              req.oAuth2Client = oAuth2Client;
              next();
            }
          });
        });
      });
      /* calender middleware: end */
    }

    actions.push(method);
    logger.info(`API : ${verb.toLocaleUpperCase()} ${config.API_VERSION}${url}`);
    apiRouter[verb](url, helper.autoWrapExpress(actions));
  });
});

apiRouter.use('/newClientProfile', clientProfileRoute);
apiRouter.use('/newUserProfile', UserRoute);
module.exports = apiRouter;

// module.exports = userRouter;
