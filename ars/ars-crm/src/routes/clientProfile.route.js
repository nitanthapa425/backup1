const express = require('express');
const { clientProfileController } = require('../controllers');
const validate = require('../middlewares/validate');
// const { isAuthenticated, isAuthorized } = require('../middlewares/auth');
const { paginate } = require('../middlewares/pagination');
const paramsValidation = require('../validations/pagination.validation');

const router = express.Router();

// This Route will be able to handle all seller GET request by providing appropriate search filter.
router
  .route('/')
  .get(validate(paramsValidation.pagination), paginate, clientProfileController.getClientProfileList)
  .post(clientProfileController.createClientProfile)
  .put(clientProfileController.insertManyClientProfile);

router
  .route('/:clientProfileId')
  .get(clientProfileController.getClientProfile)
  .put(clientProfileController.updateClientProfile)
  .delete(clientProfileController.deleteClientProfile);

router
  .route('/ClientOtherDetails/:clientProfileId/:otherDetailId')
  .post(clientProfileController.updateClientOtherDetails)
  .delete(clientProfileController.deleteClientOtherDetails);

router
  .route('/updateClientTransactionDetails/:clientProfileId/:transactionId')
  .post(clientProfileController.updateClientTransactionDetails)
  .delete(clientProfileController.deleteClientTransactionDetails);
router
  .route('/ClientTransactionDetails/:clientProfileId/:transactionId/:noteId')
  .put(clientProfileController.updateTransactionDetailsNote)
  .delete(clientProfileController.deleteTransactionDetailsNote);
router
  .route('/ClientOtherDetail/:clientProfileId/:otherDetailId/:fileId')
  .delete(clientProfileController.deleteOtherDetailsFile);
router
  .route('/ClientOtherDetails/:clientProfileId/:otherDetailId/:noteId')
  .put(clientProfileController.updateClientDetailsNote)
  .delete(clientProfileController.deleteOtherDetailsNote);

module.exports = router;
