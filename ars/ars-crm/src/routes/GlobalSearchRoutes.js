/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Global Search Routes
 *
 * @author      Susan Dhakal
 * @version     1.0
 */

module.exports = {
  '/globalSearch': {
    get: {
      controller: 'GlobalSearchController',
      method: 'getGlobalSearchController'
    },
    post: {
      controller: 'GlobalSearchController',
      method: 'createGlobalSearchController',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  }
};
