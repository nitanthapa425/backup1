/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Security Route
 *
 * @author      Ashim Karki
 * @version     1.0
 */

module.exports = {
  '/login': {
    post: { controller: 'securityController', method: 'login', public: true }
  },

  '/signUp': {
    post: {
      controller: 'securityController', method: 'signUp', public: true, role: ['SUPER_ADMIN', 'ADMIN']
    }
  },

  '/confirmEmail': {
    post: { controller: 'securityController', method: 'confirmEmail', public: true }
  },

  '/forgotPassword': {
    post: { controller: 'securityController', method: 'forgotPassword', public: true }
  },

  '/resetPassword': {
    post: { controller: 'securityController', method: 'resetPassword', public: true }
  },

  '/logout': {
    post: { controller: 'securityController', method: 'logout' }
  }
};
