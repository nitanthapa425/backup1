/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Util Routes
 *
 * @author      Ashim Karki
 * @version     1.0
 */

module.exports = {
  '/cities': {
    get: {
      controller: 'UtilController',
      method: 'getCities'
    }
  },
  '/states': {
    get: {
      controller: 'UtilController',
      method: 'getStates'
    }
  }
};
