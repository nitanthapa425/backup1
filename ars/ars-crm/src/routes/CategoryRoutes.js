/**
 * Copyright (C) Tuki Logic
 */

/**
 * the Category Routes
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

module.exports = {
  '/category': {
    get: {
      controller: 'CategoryController',
      method: 'getCategory'
    },
    post: {
      controller: 'CategoryController',
      method: 'createCategory',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    }
  },
  '/category/:id': {
    put: {
      controller: 'CategoryController',
      method: 'updateCategory',
      accessLevel: ['SUPER_ADMIN', 'ADMIN']
    },
    delete: {
      controller: 'CategoryController',
      method: 'deleteCategory',
      accessLevel: ['SUPER_ADMIN']
    }
  },
  '/template': {
    post: {
      controller: 'CategoryController',
      method: 'createTemplate'
    },
    get: {
      controller: 'CategoryController',
      method: 'getTemplate'
    }
  },
  '/sendEmails': {
    post: {
      controller: 'CategoryController',
      method: 'sendEmailsToClients'
    }
  },
  '/imageUpload': {
    post: {
      controller: 'CategoryController',
      method: 'uploadImage',
      upload: true
    }
  }
};
