const expressJwt = require('express-jwt');
const config = require('config');
const httpStatus = require('http-status');
const userService = require('../services/user.service');

const expressAuthentication = expressJwt({
  secret: config.JWT_SECRET,
  algorithms: ['HS256']
});

const isAuthenticated = async (req, res, next) => {
  if (!req.user) return res.status(401).send({ code: httpStatus.UNAUTHORIZED, message: 'Unauthorized' });
  const user = await userService.getUserById(req.user.id);
  if (user.isActive === false) {
    return res
      .status(401)
      .send({
        code: httpStatus.UNAUTHORIZED,
        message: 'Unauthorized. User Inactive',
      });
  }
  req.user = { ...req.user, accessLevel: user.accessLevel };
  return next();
};

const isAuthorized = (options) => (req, res, next) => {
  const { accessLevel } = req.user;
  if (options.includes(accessLevel)) return next();
  return res.status(403).send(`{message: "unauthorized. Required Role ${options} "}`);
};

module.exports = {
  isAuthenticated,
  isAuthorized,
  expressAuthentication,
};
