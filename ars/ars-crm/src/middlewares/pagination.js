/**
 * Validate the search query params
 * @param searchString
 * @returns {any}
 */
const validateSearch = (searchString) => {
  return JSON.parse(`{${searchString}}`);
};

/**
 * Convert plain search object to regex object to implement text search in mongo
 * @param searchObject
 */
const convertSearchToRegex = (searchObject, noCheckRegex = []) => {
  const regexObject = {};
  // eslint-disable-next-line guard-for-in,no-restricted-syntax
  for (const key of Object.keys(searchObject)) {
    // eslint-disable-next-line security/detect-non-literal-regexp
    if (noCheckRegex.includes(key)) {
      regexObject[key] = searchObject[key];
    } else {
      regexObject[key] = new RegExp(searchObject[key], 'i');
    }
  }
  return regexObject;
};

/**
 * Middleware to Convert Pagination and Search Params
 */

const paginate = async (req, res, next) => {
  let { limit, page, search, sortBy, sortOrder, select, noRegex } = req.query;
  limit = limit || 1000; // Default Page Size 1000
  page = page || 1; // Default Page
  search = search || '';
  sortBy = sortBy || '';
  sortOrder = sortOrder || 1;
  select = select || '';
  select = select.replace(/,/g, ' ');
  noRegex = noRegex ? noRegex.split(',') : [];
  try {
    search = convertSearchToRegex(validateSearch(search), noRegex);
  } catch (ignored) {
    return res.status(400).send({ message: 'Search parameter is mis configured', stack: ignored.message });
  }
  res.pagination = { options: { sort: { [sortBy]: parseInt(sortOrder, 10) }, page, limit, select }, search };
  // DELETE SORTING IF NO SORT KEY DEFINED
  if (!sortBy) {
    delete res.pagination.options.sort;
  }
  next();
};

module.exports = {
  paginate
};
