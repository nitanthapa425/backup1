const fs = require('fs');

const util = require('util');
const multer = require('multer');

const { fileConfig } = require('../config/config');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const { restaurantId } = req.params;
    const isProductUpload = req.product;
    const path = `${fileConfig.imageBaseLocation}/${restaurantId}${isProductUpload ? '/productImages' : ''}`;
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    fs.mkdirSync(path, { recursive: true });
    callback(null, path);
  },
  filename: (req, file, callback) => {
    const match = ['image/jpeg'];

    if (match.indexOf(file.mimetype) === -1) {
      const message = `${file.originalname} is invalid. Only accept jpeg.`;
      return callback(message, null);
    }
    const isProductUpload = req.product;
    const filename = `${isProductUpload ? '' : `${Date.now()}-`}${file.originalname}`; // CONVERT FILE NAME TO LOWER CASE
    callback(null, filename);
  },
});

const uploadFiles = multer({ storage }).single('file');
const uploadFilesMiddleware = util.promisify(uploadFiles);
module.exports = uploadFilesMiddleware;
