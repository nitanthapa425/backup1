/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */

const joi = require('joi');
const _ = require('lodash');
const errors = require('common-errors');
const httpStatus = require('http-status');
const models = require('../models');
const helper = require('../common/helper');
const { User } = require('../models');

/**
 * handles the update password
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updatePassword(userId, entity) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId });
  const matched = await helper.validateHash(entity.oldPassword, user.passwordHash);
  const newMatched = await helper.validateHash(entity.newPassword, user.passwordHash);

  if (!matched) {
    throw new errors.HttpStatusError('Your old password is wrong');
  } else if (newMatched) {
    throw new errors.HttpStatusError('You cannot change your old password.');
  } else {
    user.passwordHash = await helper.hashString(entity.newPassword);
    await user.save();
  }
}

updatePassword.schema = {
  userId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      oldPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required()
    })
    .required()
};

/**
 * handles the change password
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function changePassword(userId, entity, superAdminId = null) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId });
  user.passwordHash = await helper.hashString(entity.newPassword);

  if (superAdminId) {
    user.updatedBy = superAdminId;
    user.updatedAt = Date.now();
  }
  await user.save();
}

changePassword.schema = {
  userId: joi.string().required(),
  superAdminId: joi.string().optional().allow(''),
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required()
    })
    .required()
};

/**
 * handles the update profile
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateProfile(userId, entity) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId }, "Sorry, the user doesn't exist!");
  user.fullName = entity.fullName;
  await user.save();
  return user;
}

updateProfile.schema = {
  userId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      fullName: joi
        .string()
        .regex(/^([a-zA-Z, .'-]){3,30}$/i)
        .required()
    })
    .required()
};

/**
 * handles the get profile
 * @param {String} userId the user id
 */
async function userProfile(userId) {
  let user = await helper.ensureEntityExists(models.User, { _id: userId });
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  return user;
}

userProfile.schema = {
  userId: joi.string().required()
};

/**
 * handles the get profile of user
 * @param {String} userId the user id
 */
async function getUserProfileById(userId) {
  let user = await helper.ensureEntityExists(models.User, { _id: userId });
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );
  return user;
}

getUserProfileById.schema = {
  userId: joi.string().required()
};

/**
 * Update user Profiles
 * @param {Object}  update criteria
 * @returns {Object} Updated User
 */
const updateUserProfileById = async (userId, updateId, entity) => {
  let user = await helper.ensureEntityExists(User, { _id: updateId }, `The User Profile ${updateId} does not exist.`);
  // if (user) {
  //   throw new errors.HttpStatusError(httpStatus.CONFLICT, `Email ${entity.email} already exists`);
  // }

  user.updatedBy = userId;

  if (entity.middleName === '') {
    entity.fullName = entity.firstName.concat(' ') + entity.lastName;
  } else {
    entity.fullName = entity.firstName.concat(' ') + entity.middleName.concat(' ') + entity.lastName;
  }

  _.assignIn(user, entity);
  try {
    await user.save();
  } catch (e) {
    if (e.keyValue && e.keyValue.email) {
      throw new Error(`${e.keyValue.email} is already used`);
    }
    throw new Error(e);
  }

  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );

  return user;
};

/**
 * Delete UserById of User
 * @param {Object}  criteria
 * @returns {Object} Search result of users
 */

const deleteUserProfileById = async (userId, deleteId) => {
  let user = await helper.ensureEntityExists(User, { _id: deleteId }, `The User Profile ${deleteId} does not exist.`);

  user.deletedBy = userId;
  user.deletedAt = Date.now();
  user.isActive = false;
  await user.save();
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );

  return user;
};

/**
 * Revert Deleted UserById of User
 * @param {Object}  criteria
 * @returns {Object} Search result of users
 */

const revertDeleteUserProfileById = async (userId, deleteId) => {
  let user = await helper.ensureEntityExists(User, { _id: deleteId }, `The User Profile ${deleteId} does not exist.`);
  user.isActive = true;
  await user.save();
  user = _.omit(
    user.toObject(),
    'passwordHash',
    'id',
    'verificationToken',
    'forgotPasswordToken',
    '__v',
    'accessToken'
  );

  return user;
};

// /**
//  * Search user Profiles
//  * @param {Object} criteria Search criteria
//  * @returns {Object} Search result
//  */
// const searchUserProfiles = async (criteria) => {
//   const filter = { isActive: true };
//   const results = await User.find(filter)
//     .collation({ locale: 'en' })
//     .skip((+criteria.page - 1) * +criteria.perPage)
//     .limit(+criteria.perPage);

//   return {
//     total: await User.countDocuments(filter),
//     results,
//     page: criteria.page,
//     perPage: criteria.perPage
//   };
// };

/**
 * Search User Profiles
 * @param {Object} criteria Search criteria
 * @returns {Object} Search result
 */
const searchUserProfiles = async (criteria) => {
  const filter = { verified: true, $or: [{ isActive: true }, { isActive: { $exists: false } }] };
  // Keyword search
  if (criteria.keyword) {
    filter.$or = [
      { title: { $regex: criteria.keyword, $options: 'i' } },
      { firstName: { $regex: criteria.keyword, $options: 'i' } },
      { lastName: { $regex: criteria.keyword, $options: 'i' } },
      { mobile: { $regex: criteria.keyword, $options: 'i' } },
      { email: { $regex: criteria.keyword, $options: 'i' } },
      { mailingCity: { $regex: criteria.keyword, $options: 'i' } },
      { mailingState: { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.streetAddress': { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.state': { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.country': { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.zip': { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.street': { $regex: criteria.keyword, $options: 'i' } },
      { 'residential.suburb': { $regex: criteria.keyword, $options: 'i' } },
      { 'referrals.referralLeadClose': { $regex: criteria.keyword, $options: 'i' } }
    ];
  }

  // query records
  // for sorting, add second sorting by _id if sortBy is not id, so that result order is determined
  if (criteria.sortBy === 'id' || !criteria.sortBy) {
    criteria.sortBy = '_id';
  }

  const sortStr = `${criteria.sortOrder.toLowerCase() === 'desc' ? '-' : ''}${criteria.sortBy}`;

  // Find records with pagination
  const results = await User.find(filter)
    .collation({ locale: 'en' })
    .populate('createdBy', 'fullName email')
    .populate('updatedBy', 'fullName email')
    .sort(sortStr)
    .skip((+criteria.page - 1) * +criteria.perPage)
    .limit(+criteria.perPage);

  return {
    total: await User.countDocuments(filter),
    results,
    page: criteria.page,
    perPage: criteria.perPage
  };
};

searchUserProfiles.schema = {
  criteria: joi.object().keys({
    keyword: joi.string().trim(),
    page: joi.page(),
    perPage: joi.perPage(),
    sortBy: joi.string().default('id'),
    sortOrder: joi.sortOrder()
  })
};

/**
 * handles the get emergency contact person of user
 * @param {String} userId the user id
 */
async function getEmergencyContactPerson(userId) {
  const userProfile = await helper.ensureEntityExists(User, { _id: userId }, `The User ${userId} does not exist.`);
  if (userId) {
    const findEmergency = await User.find({ _id: userId }, { contactPerson: 1 }).sort({
      emergencyContactPersonFirstName: -1
    });
    return findEmergency;
  }

  // const getAllClient = await ClientProfile.find({}, { clientFollowUpNotes: 1 }).sort({ clientFollowUpDate: -1 });
}

getEmergencyContactPerson.schema = {
  userId: joi.id().required()
};

// TODO
/**
 * handles the get emergency contact person of user
 * @param {String} userId the user id
 */
async function getClientAssignToMe(userId) {
  const userProfile = await helper.ensureEntityExists(User, { _id: userId }, `The User ${userId} does not exist.`);
  // if (userId) {
  //   const findMyTask = await User.find({ _id: userId }, { contactPerson: 1 }).sort({
  //     emergencyContactPersonFirstName: -1
  //   });
  //   return findEmergency;
  // }

  // const getAllClient = await ClientProfile.find({}, { clientFollowUpNotes: 1 }).sort({ clientFollowUpDate: -1 });
}

/* Upload Profile Picture of User  */
const uploadProfilePicture = async (userId, entity) => {
  const user = await helper.ensureEntityExists(User, { _id: userId }, `The User Profile ${userId} does not exist.`);

  user.imageUploadPath = entity.imageUploadPath;

  await user.save();

  return user;
};

module.exports = {
  userProfile,
  changePassword,
  updatePassword,
  updateProfile,
  getUserProfileById,
  updateUserProfileById,
  deleteUserProfileById,
  searchUserProfiles,
  getEmergencyContactPerson,
  getClientAssignToMe,
  revertDeleteUserProfileById,
  uploadProfilePicture
};
