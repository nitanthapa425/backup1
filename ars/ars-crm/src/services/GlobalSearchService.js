const _ = require('lodash');
const { GlobalSearch } = require('../models');

async function createGlobalSerchService(userId, entity) {
  let globalSearch = _.extend(entity);

  globalSearch = new GlobalSearch(globalSearch);

  await globalSearch.save();

  return globalSearch;
}

/**
 * @returns {Object} get  all category
 */

const getGlobalSearchService = async () => {
  const results = await GlobalSearch.find({}).collation({ locale: 'en' });

  return results;
};

module.exports = {
  createGlobalSerchService,
  getGlobalSearchService
};
