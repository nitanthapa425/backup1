/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const _ = require('lodash');
// const config = require('config');
// const httpStatus = require('http-status');
// const errors = require('common-errors');
const { Comment } = require('../models');

/**
 * @param {Object} entity the new Comment
 * @returns {Object} the entity
 */

async function createComment(userId, entity) {
  let comment = await Comment.findOne({ comment: entity.comment });
  comment = _.extend(entity, {
    createdBy: userId
  });
  comment = new Comment(comment);
  await comment.save();

  return comment;
}
createComment.schema = {
  userId: joi.string().required(),
  entity: joi.object().keys({
    comment: joi.string().optional().allow('')
  })
};

/**
 * @returns {Object} get  all Comment
 */

const getComment = async () => {
  const results = await Comment.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * handles the update Comment
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateCommentById(userId, entity) {
  let comment = await Comment.findOne({ comment: entity.comment });
  comment = _.extend(entity, {
    createdBy: userId
  });
  comment = new Comment(comment);
  await comment.save();

  return comment;
}

updateCommentById.schema = {
  userId: joi.string().required(),
  entity: joi.object().keys({
    comment: joi.string().optional().allow('')
  })
};

/**
 * Delete Comment
 * @returns {Object} Search result
 */

const deleteCommentById = async (userId, deleteId) => {
  const comment = await Comment.findOne(Comment, { comment_id: deleteId }, `The Comment  ${deleteId} does not exist.`);

  comment.deletedBy = userId;
  comment.deletedAt = Date.now();
  await comment.save();

  return comment;
};

module.exports = {
  createComment,
  getComment,
  updateCommentById,
  deleteCommentById
};
