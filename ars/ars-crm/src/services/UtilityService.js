/**
 * Copyright (C) Tuki Logic
 */

/**
 * the utility service
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const config = require('config');
const joi = require('joi');
const _ = require('lodash');
const nodemailer = require('nodemailer');
const cron = require('node-cron');
const logger = require('../common/logger');
const { STATES, CITIES } = require('../constants');

/**
 * return the array of cities with or without filter
 * @param {String} filterCity the filter city
 * @returns [String]
 */
async function getCities(filterCity) {
  if (filterCity) {
    return CITIES.filter((city) => city.stateName === filterCity);
  }

  return CITIES;
}

getCities.schema = {
  filterCity: joi.string()
};

/**
 * returns the array of states
 * @returns [String]
 */
async function getStates() {
  return [...STATES];
}

/**
 * transporter config for sending emails
 */
const transporter = nodemailer.createTransport(_.extend(config.email, { logger }), {
  from: `${config.email.auth.user}`
});

/**
 * sends email to the provided email id
 * @param {Object} emailEntity the email entity
 * @returns {Promise}
 */
async function sendEmail(emailEntity) {
  return new Promise((resolve, reject) => {
    transporter.sendMail(emailEntity, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

sendEmail.schema = {
  emailEntity: joi
    .object()
    .keys({
      to: joi.string().required(),
      from: joi.string().optional().allow(''),
      replyTo: joi.string().optional().allow(''),
      subject: joi.string().required(),
      text: joi.string(),
      html: joi.string()
    })
    .required()
};

/**
 * schedule email to the provided email id
 * @param {function} schedule the email entity
 * @returns {Promise}
 */
async function dailyShceduler() {
  return new Promise((resolve, reject) => {
    cron.schedule(
      '0 1 * 1-31* *',
      (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      },
      {
        scheduled: true,
        timezone: 'America/Sao_Paulo'
      }
    );
  });
}

module.exports = {
  getCities,
  getStates,
  sendEmail,
  dailyShceduler
};
