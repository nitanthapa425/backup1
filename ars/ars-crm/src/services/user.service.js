// const _ = require('lodash');
const { User } = require('../models');
// const { paginate } = require('../middlewares/pagination');

const getUserProfileList = async (search, options) => {
  search.$or = [{ isActive: true }, { isActive: { $exists: false } }];
  return User.paginate(search, options);
};
const getDeletedUserProfileList = async (search, options) => {
  search.$or = [{ isActive: false }];

  return User.paginate(search, options);
};

const getUserById = async (userId) =>
  User.findOne({
    _id: userId
  });

module.exports = {
  getUserProfileList,
  getUserById,
  getDeletedUserProfileList
};
