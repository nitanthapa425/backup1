const _ = require('lodash');
const httpStatus = require('http-status');
const errors = require('common-errors');
const { ClientProfile } = require('../models');
const helper = require('../common/helper');

// const { paginate } = require('../middlewares/pagination');

const createClientProfile = async (tag) => ClientProfile.create(tag);

const getClientProfileList = async (search, options) => ClientProfile.paginate(search, options);

// let orArray = [];
// Object.keys(search).forEach(x => {
//   orArray.push({[x]:search[x]})
// })

// const query = Object.keys(search).length ? {$or: orArray} : {}

const getClientProfile = async (tagId) => ClientProfile.findById(tagId);

// const updateClientProfile = async (tagId, tagBody) => ClientProfile.findByIdAndUpdate(tagId, tagBody);

const updateClientProfile = async (tagId, tagBody) => {
  const clientEmail = await helper.getClientByEmailClient(tagBody.email);
  const clientMobile = await helper.getClientByMobileClient(tagBody.mobile);

  const otherEmail = clientEmail.filter((res) => res._id != tagId);
  const otherProfile = clientMobile.filter((res) => res._id != tagId);

  if (otherEmail.length > 0 || (otherProfile.length > 0 && tagBody.mobile !== '')) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Email or Mobile already exists');
  }

  if (tagBody.middleName === '') {
    tagBody.fullName = tagBody.firstName.concat(' ') + tagBody.lastName;
  } else {
    tagBody.fullName = tagBody.firstName.concat(' ') + tagBody.middleName.concat(' ') + tagBody.lastName;
  }

  return ClientProfile.findByIdAndUpdate(tagId, tagBody);
};

const deleteClientProfile = async (tagId) => ClientProfile.remove({ _id: tagId });

const insertManyCategories = async (tagList) => ClientProfile.insertMany(tagList, { ordered: false });

const appendOtherNotes = async (clientProfileId, otherDetailId, notes) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  clientProfile.otherDetails.forEach((x) => {
    if (x._id == otherDetailId) {
      x.notes.push(notes);
    }
  });
  return clientProfile.save();
};

const appendTransactionNotes = async (userId, clientProfileId, transactionId, resultNotes) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Client id  is null');
  // resultNotes.updatedBy = userId;
  resultNotes.createdAt = new Date();
  clientProfile.transactionDetails.forEach((x) => {
    if (x._id == transactionId) {
      x.resultNotes.push(resultNotes);
    }
  });
  return clientProfile.save();
};

// const deleteOtherNotes = async (tagId) => ClientProfile.otherDetails.remove({ _id: tagId });

const deleteClientOtherDetails = async (clientProfileId, otherDetailId) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Client Profile is null');

  const deleteOtherNotes = await ClientProfile.update(
    { _id: clientProfileId },
    { $pull: { otherDetails: { _id: otherDetailId } } },
    { safe: true, multi: true }
  );

  return deleteOtherNotes;
};
// const deleteOtherNotes = async (tagId) => ClientProfile.otherDetails.remove({ _id: tagId });

const deleteTransactionDetails = async (clientProfileId, transactionId) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Client Profile is null');

  const deleteOtherNotes = await ClientProfile.update(
    { _id: clientProfileId },
    { $pull: { transactionDetails: { _id: transactionId } } },
    { safe: true, multi: true }
  );

  return deleteOtherNotes;
};

// delete file from file and correspondence
const deleteOtherDetailsFileById = async (clientProfileId, otherDetailId, fileId) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  const singleClientNote = clientProfile.otherDetails.filter((res) => res._id == otherDetailId);
  await singleClientNote[0].correspondingUploadPaths.splice(fileId, 1);
  return clientProfile.save();
};

// delete note from file and correspondence
const deleteOtherDetailsNoteById = async (clientProfileId, otherDetailId, noteId) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  const singleClientNote = clientProfile.otherDetails.filter((res) => res._id == otherDetailId);
  await singleClientNote[0].notes.splice(noteId, 1);
  return clientProfile.save();
};

// delete note from transaction in Client Profile View
const deleteTransactionDetailsNoteById = async (clientProfileId, transactionId, noteId) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  const singleClientNote = clientProfile.transactionDetails.filter((res) => res._id == transactionId);
  await singleClientNote[0].resultNotes.splice(noteId, 1);
  return clientProfile.save();
};

// Update Other Details note  in Client Profile View
const updateOtherNotes = async (clientProfileId, otherDetailId, noteId, note) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  const singleClientNote = clientProfile.otherDetails.filter((res) => res._id == otherDetailId);
  await singleClientNote[0].notes.splice(noteId, 1, note);
  return clientProfile.save();
};

// Update Transaction Details note  in Client Profile View
const updateTransactionNotes = async (clientProfileId, transactionId, noteId, resultNotes) => {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Other Details is null');
  const singleClientNote = clientProfile.transactionDetails.filter((res) => res._id == transactionId);
  await singleClientNote[0].resultNotes.splice(noteId, 1, resultNotes);
  return clientProfile.save();
};
module.exports = {
  createClientProfile,
  getClientProfileList,
  getClientProfile,
  updateClientProfile,
  deleteClientProfile,
  insertManyCategories,
  appendOtherNotes,
  appendTransactionNotes,
  deleteClientOtherDetails,
  deleteOtherDetailsFileById,
  deleteOtherDetailsNoteById,
  deleteTransactionDetails,
  deleteTransactionDetailsNoteById,
  updateOtherNotes,
  updateTransactionNotes
};
