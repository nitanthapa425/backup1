const { google } = require('googleapis');
const mongoose = require('mongoose');
const errors = require('common-errors');
const { ClientProfile } = require('../models');
const helper = require('../common/helper');

async function listEvents(auth) {
  const calendar = google.calendar({ version: 'v3', auth });
  let events;
  return new Promise((resolve, reject) => {
    calendar.events.list(
      {
        calendarId: 'primary',
        // timeMin: new Date().toISOString(),
        // maxResults: 10,
        singleEvents: true,
        orderBy: 'startTime'
      },
      (err, res) => {
        events = res.data.items;
        if (!events.length) {
          events = {
            data: []
          };
        }
        resolve(events);
      }
    );
  });
}

async function createGoogleEvent(eventData, auth) {
  const calendar = google.calendar({ version: 'v3', auth });

  return new Promise((resolve, reject) => {
    calendar.events.insert(
      {
        auth,
        calendarId: 'primary',
        resource: eventData
      },
      function (err, event) {
        if (err) {
          return;
        }

        resolve({ eventLink: event.htmlLink });
      }
    );
  });
}

async function getEvents(auth) {
  const events = await listEvents(auth);
  return { events: events };
}

async function createEvent(eventData, auth) {
  const events = await createGoogleEvent(eventData, auth);
  return { events: events };
}

const updateCalendarService = async (userId, calendarEventId, eventData, auth) => {
  const calendar = google.calendar({ version: 'v3', auth });

  await calendar.events.update(
    {
      auth,
      calendarId: 'primary',
      eventId: calendarEventId,
      requestBody: eventData
    },
    (err) => err
  );

  const clientProfile = await ClientProfile.find({});

  const clientProfiles = clientProfile
    .map((res) => {
      return {
        ...res,
        clientFollowUpNotes: res.clientFollowUpNotes.filter((resValue) => resValue.eventId === calendarEventId)
      };
    })
    .filter((resValues) => resValues.clientFollowUpNotes.length > 0);

  if (clientProfiles.length !== 0) {
    const singleClientProfile = await helper.ensureEntityExists(
      ClientProfile,
      { _id: mongoose.Types.ObjectId(clientProfiles[0]._doc._id) },
      'The Client Profile does not exist.'
    );

    const noteIndex = Array.from(singleClientProfile.clientFollowUpNotes).findIndex(
      (note) => typeof note.eventId !== 'undefined' && note.eventId === calendarEventId
    );

    if (noteIndex === -1) {
      throw new errors.NotFoundError('The Follow Up Note does not exist.');
    }

    let toUpdateNote = singleClientProfile.clientFollowUpNotes[noteIndex];

    toUpdateNote = {
      followUpNote: eventData.summary,
      clientFollowUpDate: eventData.start.dateTime,
      assignedTo: eventData.attendees.length > 0 ? eventData.attendees[0].email : '',
      noteId: toUpdateNote.noteId,
      createdBy: toUpdateNote.createdBy,
      createdAt: toUpdateNote.createdAt,
      eventId: toUpdateNote.eventId,
      updatedBy: userId,
      updatedAt: new Date()
    };

    singleClientProfile.clientFollowUpNotes[noteIndex] = toUpdateNote;

    await singleClientProfile.save();
  }

  return "Event and it's followup note updated successfully";
};

const deleteCalendarService = async (calendarEventId, auth) => {
  const calendar = google.calendar({ version: 'v3', auth });

  await calendar.events.delete(
    {
      auth,
      calendarId: 'primary',
      eventId: calendarEventId
    },
    (err) => err
  );

  const clientProfile = await ClientProfile.find({});

  const clientProfiles = clientProfile
    .map((res) => {
      return {
        ...res,
        clientFollowUpNotes: res.clientFollowUpNotes.filter((resValue) => resValue.eventId === calendarEventId)
      };
    })
    .filter((resValues) => resValues.clientFollowUpNotes.length > 0);

  if (clientProfiles.length !== 0) {
    const singleClientProfile = await helper.ensureEntityExists(
      ClientProfile,
      { _id: mongoose.Types.ObjectId(clientProfiles[0]._doc._id) },
      'The Client Profile does not exist.'
    );

    const noteIndex = Array.from(singleClientProfile.clientFollowUpNotes).findIndex(
      (note) => typeof note.eventId !== 'undefined' && note.eventId === calendarEventId
    );

    await singleClientProfile.clientFollowUpNotes.splice(noteIndex, 1);

    await singleClientProfile.save();
  }

  return "Event and it's followup note deleted successfully";
};

module.exports = {
  getEvents,
  createEvent,
  deleteCalendarService,
  updateCalendarService
};
