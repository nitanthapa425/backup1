/**
 * Copyright (C) Tuki Logic
 */

/**
 * the security service
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const joi = require('joi');
const errors = require('common-errors');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const util = require('util');
const _ = require('lodash');
const config = require('config');
const models = require('../models');
const helper = require('../common/helper');
const constants = require('../constants');
const UtilityService = require('./UtilityService');

/* ***************************** helpers ***************************** */

/**
 * gets user by email id
 * @param {String} email the email id
 * @returns {Object} the user information
 */
async function getUserByEmail(email) {
  const user = await models.User.findOne({ email });
  return user;
}

/**
 * gets user by user Name
 * @param {String} userName the email id
 * @returns {Object} the user information
 */
async function getUserByUserName(userName) {
  const user = await models.User.findOne({ userName });
  return user;
}

/**
 * gets Deleted Users
 * @param {String} id the user id
 * @returns {Object} the user information
 */
const deletedUsers = async function getDeletedUsers() {
  const user = await models.User.find({ isActive: false });
  return user;
};

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendVerificationEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.emailVerificationContent,
    email,
    `${frontendUrl}/confirmEmail?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/confirmEmail?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Verify Email Address on ARS',
    to: email,
    html: emailContent
  };
  await UtilityService.sendEmail(emailEntity);
}

/**
 * send a verification email to user
 * @param {String} email the email id
 * @param {String} verificationToken the verification token
 * @param {String} hostUrl the host url
 */
async function sendForgotPasswordEmail(email, verificationToken, frontendUrl) {
  const emailContent = util.format(
    config.forgotPasswordContent,
    email,
    `${frontendUrl}/resetPassword?verificationToken=${verificationToken}&email=${email}`,
    `${frontendUrl}/resetPassword?verificationToken=${verificationToken}&email=${email}`
  );
  const emailEntity = {
    subject: 'Forgot Password',
    to: email,
    html: emailContent
  };
  await UtilityService.sendEmail(emailEntity);
}

/**
 * checks user authentication using email and password
 * @param {Object} entity the entity
 * @returns {Object} the user information with token
 */
async function login(entity) {
  let user;
  if (typeof entity.userName === 'undefined' && entity.email !== '') {
    user = await helper.ensureEntityExists(
      models.User,
      { email: entity.email },
      `Sorry, we could not find any user with  ${entity.email} registered with us.`
    );
  } else {
    user = await helper.ensureEntityExists(
      models.User,
      { userName: entity.userName },
      `Sorry, we could not find any user with   ${entity.userName} registered with us.`
    );
  }
  if (user.isActive === true) {
    const matched = await helper.validateHash(entity.password, user.passwordHash);

    if (!matched) {
      throw new errors.NotPermittedError('Wrong email or password.');
    }
    // const ensureActive = await helper.ensureUserActive({ isActive: true });
    // const ensureVerified = await helper.ensureUserVerifiedTrue({ isVerified: true });
    // if (!ensureActive && !ensureVerified) {
    //   ('The user is no longer available');
    // }

    helper.ensureUserVerified(user);

    // generate JWT token
    const token = jwt.sign(
      _.pick(user, ['id', 'userName', 'email', 'firstName', 'lastName', 'role']),
      config.JWT_SECRET,
      {
        expiresIn: config.JWT_EXPIRATION
      }
    );
    user.accessToken = token;
    await user.save();
    user = _.omit(user.toObject(), 'passwordHash', 'verificationToken', 'forgotPasswordToken', '__v');
    return {
      user
    };
  } else {
    throw new errors.NotPermittedError('This account is no longer available.');
  }
}

login.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      userName: joi.string().optional().allow(''),
      password: joi.string().required()
    })
    .required()
};

/**
 * does sign up process
 * @param {Object} entity the request body entity
 * @returns {Object} the sign user information
 */
async function signUp(entity) {
  let user = await getUserByEmail(entity.email);
  const username = await getUserByUserName(entity.userName);
  if (username) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Username ${entity.userName} already exists`);
  }
  if (user) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Email ${entity.email} already exists`);
  }

  const verificationToken = helper.getRandomString(25);
  // const passwordHash = await helper.hashString(entity.password);
  // passwordHash,

  user = _.extend(entity, {
    email: entity.email,
    userName: entity.userName,
    verificationToken,
    firstName: entity.firstName,
    lastName: entity.lastName,
    fullName:
      entity.middleName !== ''
        ? entity.firstName.concat(' ') + entity.middleName.concat(' ') + entity.lastName
        : entity.firstName.concat(' ') + entity.lastName,
    role: entity.role,
    verified: false,
    isActive: true
  });
  user = new models.User(user);
  await user.save();

  // send an email
  try {
    await sendVerificationEmail(user.email, verificationToken, helper.getHostUrl());
  } catch (ex) {
    await user.remove();
    throw ex;
  }
  return user;
}

signUp.schema = {
  entity: joi.object().keys({
    title: joi.string().optional().allow(''),
    firstName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z, .'-]){1,30}$/i)
      .required(),
    middleName: joi
      .string()
      .optional()
      .allow('')
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z, .'-]){1,30}$/i),
    lastName: joi
      .string()
      /** Name containing Alphabets value */
      .regex(/^([a-zA-Z, .'-]){1,30}$/i)
      .required(),
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
    userName: joi
      .string()
      .required()
      .min(3)
      .max(15)
      /** Username containing Alphabets and Numeric value */
      .regex(/^[a-zA-Z0-9_.-]*$/),
    phone: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[0-9]{10,14}$/),
    accessLevel: joi
      .string()
      .optional()
      .allow('')
      .valid([constants.ACCESS_LEVEL.SUPER_ADMIN, constants.ACCESS_LEVEL.ADMIN, constants.ACCESS_LEVEL.USER]),
    residential: {
      street: joi.string().optional().allow(''),
      streetAddress: joi.string().optional().allow(''),
      state: joi.string().optional().allow(''),
      country: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      zip: joi
        .string()
        .optional()
        .allow('')
        .max(10)
        .regex(/^[0-9]+$/),
      suburb: joi.string().optional().allow('')
    },
    rolePosition: joi
      .string()
      .optional()
      .allow('')
      .regex(/^[aA-zZ\s]+$/),
    contactPerson: {
      emergencyContactPersonTitle: joi.string().optional().allow(''),
      emergencyContactPersonFirstName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      emergencyContactPersonMiddleName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      emergencyContactPersonLastName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      emergencyContactPersonMobileNo: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      emergencyContactPersonPhone: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      emergencyContactPersonEmail: joi.string().optional().allow(''),
      emergencyContactPersonRelation: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/)
    },
    emergencyResidential: {
      emergencyStreetAddress: joi.string().optional().allow(''),
      emergencyState: joi.string().optional().allow(''),
      emergencyCountry: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      emergencyZip: joi.string().optional().allow(''),
      emergencyStreet: joi.string().optional().allow(''),
      emergencySuburb: joi.string().optional().allow('')
    },
    maritalStatus: joi.string().optional().allow(''),
    status: joi.string().optional().allow('')
  })
};

/**
 * handles the confirm email
 * @param {Object} entity the request query
 * @returns {String} the success or failure
 */
async function confirmEmail(entity) {
  const user = await getUserByEmail(entity.email);
  if (!user) {
    throw new errors.NotFoundError(`Sorry we cannot find a user with this email ${entity.email}`);
  }
  if (user.verified) {
    throw new errors.HttpStatusError(
      httpStatus.BAD_REQUEST,
      `User with this email ${entity.email} has 
    already been  verified`
    );
  }

  if (user.verificationToken === entity.verificationToken) {
    const passwordHash = await helper.hashString(entity.password);

    user.verified = true;
    user.verificationToken = null;
    user.passwordHash = passwordHash;
    await user.save();
  } else {
    throw new errors.ValidationError("Sorry but the verification code doesn't match");
  }

  return { message: `Congratulations, ${entity.email} has been verified successfully` };
}

confirmEmail.schema = {
  entity: joi.object().keys({
    email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
    verificationToken: joi.string().required(),
    password: joi
      .string()
      /** Password must be minimum of eight characters, with at least */
      /** one uppercase letter, one lowercase letter, one number and one special character */
      .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      .required()
  })
};

/**
 * does forgot password process
 * @param {Object} entity the request body entity
 * @returns {Object} the user information
 */
async function forgotPassword(entity) {
  const user = await helper.ensureEntityExists(
    models.User,
    { email: entity.email },
    `${entity.email} could not be found`
  );
  // generate token
  const verificationToken = jwt.sign(_.pick(user, ['id']), config.JWT_SECRET, {
    expiresIn: 5000
  });
  // update user information in database
  user.forgotPasswordToken = verificationToken;
  await user.save();

  // send an email
  await sendForgotPasswordEmail(user.email, verificationToken, helper.getHostUrl());

  return 'Please check your email for the next step for password reset.';
}

forgotPassword.schema = {
  entity: joi
    .object()
    .keys({
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required()
    })
    .required()
};

/**
 * handles the reset password
 * @param {Object} entity the request body
 * @returns {String} the success or failure
 */
async function resetPassword(entity) {
  const user = await getUserByEmail(entity.email);
  if (!user) {
    throw new errors.NotFoundError(`${entity.email} not found`);
  }
  if (jwt.verify(entity.verificationToken, config.JWT_SECRET)) {
    const passwordHash = await helper.hashString(entity.newPassword);
    user.passwordHash = passwordHash;
    user.forgotPasswordToken = null;
    await user.save();
  } else {
    throw new errors.AuthenticationRequiredError('Sorry but your verification token is not valid.');
  }
  return { message: 'Your password has been reset successfully, please log in to continue!' };
}

resetPassword.schema = {
  entity: joi
    .object()
    .keys({
      newPassword: joi
        .string()
        /** Password must be minimum of eight characters, with at least */
        /** one uppercase letter, one lowercase letter, one number and one special character */
        .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
        .required(),
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
      verificationToken: joi.string().required()
    })
    .required()
};

/**
 * handles logout
 * @param {Object} userId the user id
 */
async function logout(userId) {
  const user = await helper.ensureEntityExists(models.User, { _id: userId });
  if (!user.accessToken) {
    throw new errors.NotPermittedError(`user ${userId} is already logged out`);
  }
  user.accessToken = null;
  await user.save();
}

logout.schema = {
  userId: joi.string().required()
};

module.exports = {
  login,
  signUp,
  logout,
  confirmEmail,
  forgotPassword,
  resetPassword
};
