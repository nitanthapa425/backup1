/**
 * Copyright (C) Tuki Logic
 */

/**
 * the user service
 *
 * @author      Tuki Logic
 * @version     1.0
 */
const joi = require('joi');
const _ = require('lodash');
// const fs = require('fs');
// const path = require('path');
// const util = require('util');
// const config = require('config');
const httpStatus = require('http-status');
const errors = require('common-errors');
const helper = require('../common/helper');
const { Category, EmailTemplate, Template } = require('../models');
const UtilityService = require('./UtilityService');

// Get EmailTemplate by Id
const getEmailTemplate = async (tagId) => EmailTemplate.findById(tagId);

/**
 * @param {Object} entity the new category
 * @returns {Object} the entity
 */

async function createCategory(userId, entity) {
  let category = await Category.findOne({ categoryName: entity.categoryName });
  if (category) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Category name ${entity.categoryName} already exists`);
  }
  category = _.extend(entity, {
    createdBy: userId
  });
  category = new Category(category);
  await category.save();

  return category;
}
createCategory.schema = {
  userId: joi.string().required(),
  entity: joi.object().keys({
    categoryName: joi.string().required(),
    categoryDescription: joi.string().optional().allow(''),
    emailList: joi.array().items(
      joi.object().keys({
        email: joi.string().required()
      })
    )
  })
};

/**
 * @returns {Object} get  all category
 */

const getCategory = async () => {
  const results = await Category.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * handles the update  category
 * @param {String} userId the user id
 * @param {Object} entity the entity
 */
async function updateCategory(userId, categoryId, entity) {
  const category = await Category.findOne({ _id: categoryId });
  const _category = await Category.findOne({ categoryName: entity.categoryName });
  if (_category && _category._id != categoryId) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `Category name ${entity.categoryName} already exists`);
  }
  if (!category) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Category not found. Try Refreshing the page');
  }
  category.updatedBy = userId;
  _.assignIn(category, entity);
  await category.save();

  return category;
}

updateCategory.schema = {
  userId: joi.string().required(),
  categoryId: joi.string().required(),
  entity: joi.object().keys({
    categoryName: joi.string().required(),
    categoryDescription: joi.string().optional().allow(''),
    emailList: joi.array().items(
      joi.object().keys({
        email: joi.string().email({ minDomainAtoms: 2 }).optional().allow()
      })
    )
  })
};

/**
 * Delete Category
 * @returns {Object} Search result
 */
/**
 * delete client profile by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteCategory(deleteId) {
  const category = await helper.ensureEntityExists(
    Category,
    { _id: deleteId },
    `The Client Profile ${deleteId} does not exist.`
  );
  return category.remove();
}

deleteCategory.schema = {
  deleteId: joi.string().required()
};

/**
 * @param {Object} entity the new template
 * @returns {Object} the entity
 */

async function createTemplate(userId, entity) {
  let template = await Template.findOne({ templateName: entity.templateName });
  if (template) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, `template name ${entity.templateName} already exists`);
  }
  template = _.extend(entity, {
    createdBy: userId
  });
  template = new Template(template);
  await template.save();

  return template;
}
createTemplate.schema = {
  userId: joi.string().required(),
  entity: joi.object().keys({
    templateName: joi.string().required(),
    templateBody: joi.string().optional().allow('')
  })
};

/**
 * @returns {Object} get  all template
 */

const getTemplate = async () => {
  const results = await Template.find({}).collation({ locale: 'en' });

  return results;
};

/**
 * @param {Object} entity the send Email to Clients
 * @returns {Object} the entity
 */
async function handleSendEmail(userId, entity, sendEmailToClients) {
  const { category, emailTemplate, emails } = entity;
  const listOfEmail = Category.find({ _id: category });
  const totalEmailList = listOfEmail.concat(emails);
  const { content, subject } = EmailTemplate.find({ _id: emailTemplate });

  sendEmailToClients(userId, {
    subject,
    content,
    to: totalEmailList
  });
}

/**
 * @param {Object} entity To send Email to Clients
 * @returns {Object} the entity
 */

async function sendEmailsToClients(userId, entity) {
  const { to: userEmail } = entity;
  userEmail.forEach(async (emailObject) => {
    const { email: _emailAddress } = emailObject;
    const singleEntity = {
      to: _emailAddress,
      content: entity.content,
      subject: entity.subject
    };
    let emailTemplate = _.extend(singleEntity, {
      createdBy: userId,
      createdAt: Date.now()
    });

    emailTemplate = new EmailTemplate(emailTemplate);
    const email = {
      to: emailTemplate.to,
      from: entity.from !== '' ? `${entity.from} <support@arsgroup.com.au>` : 'ARS Group <support@arsgroup.com.au>',
      replyTo: 'info@arsgroup.com.au',
      subject: emailTemplate.subject,
      html: emailTemplate.content,
      text: emailTemplate.content
    };
    await UtilityService.sendEmail(email);
    await emailTemplate.save();
    return emailTemplate;
  });
  return 'Email has been sent successfully to respective recipients. Thank you.';
}

sendEmailsToClients.schema = {
  userId: joi.string().required(),
  entity: joi.object().keys({
    subject: joi.string().required(),
    content: joi.string().optional().allow(''),
    from: joi.string().optional().allow(''),
    to: joi.array().items(
      joi.object().keys({
        email: joi.string().email({ minDomainAtoms: 2 }).optional().allow()
      })
    )
  })
};

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */
async function uploadImage(userId, templateId, entity) {
  const template = getEmailTemplate(
    EmailTemplate,
    { _id: templateId },
    `The Email Template with ${templateId} does not exist.`
  );

  entity.createBy = userId;
  entity.createdAt = new Date();
  template.push(entity);
  await template.save();

  return entity;
}
module.exports = {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory,
  createTemplate,
  getTemplate,
  sendEmailsToClients,
  handleSendEmail,
  uploadImage
};
