/* eslint-disable no-plusplus */
/* eslint-disable indent */
/**
 * Copyright (C) Tuki Logic
 */

/**
 * the CLient Profile Service
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const { v4: uuidv4 } = require('uuid');
const joi = require('joi');
const _ = require('lodash');
const { google } = require('googleapis');
const httpStatus = require('http-status');
const errors = require('common-errors');
const { getUserProfileById } = require('./UserService');
const helper = require('../common/helper');
const { ClientProfile } = require('../models');
const constants = require('../constants');
const { ClientValidation } = require('../common/validation');
/**
 * creates the client profile
 * @param {String} userId the current user id
 * @param {Object} entity the request body entity
 * @returns {Object} the client profile information
 */
async function createClientProfile(userId, entity, reqBody, auth) {
  const clientEmail = await helper.getClientByEmail(entity.email);
  const clientFirstName = await helper.getClientByFirstName(entity.firstName);
  const clientMiddleName = await helper.getClientByMiddleName(entity.middleName);
  const clientLastName = await helper.getClientByLastName(entity.lastName);
  const clientMobile = await helper.getClientByMobile(entity.mobile);

  if (clientEmail || (clientMobile && entity.mobile !== '')) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Email or Mobile already exists');
  }

  if (clientFirstName && clientMiddleName && clientLastName) {
    throw new errors.HttpStatusError(
      httpStatus.CONFLICT,
      'Client with first name, middle name, last name already exists'
    );
  } else {
    const calendar = google.calendar({ version: 'v3', auth });

    if (reqBody && reqBody.length !== 0) {
      const jsonArray = JSON.parse(reqBody.clientFollowUpNotes);
      if (reqBody.clientFollowUpNotes.length !== 0 && typeof jsonArray[0] !== 'undefined') {
        const date = new Date(jsonArray[0].clientFollowUpDate);
        const getDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');
        const getStartTime = [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
        const getEndTime = [date.getHours() + 1, date.getMinutes(), date.getSeconds()].join(':');

        const eventData = {
          summary: jsonArray[0].followUpNote,
          start: {
            dateTime: getDate.concat('T') + getStartTime.concat('+05:45')
            // timeZone: req.body.start.timeZone
          },
          end: {
            dateTime: getDate.concat('T') + getEndTime.concat('+05:45')
            // timeZone: req.body.end.timeZone
          },
          attendees: [
            {
              email: jsonArray[0].assignedTo
            }
          ]
        };

        await calendar.events
          .insert({
            auth,
            calendarId: 'primary',
            resource: eventData
          })
          .then(async (res1) => {
            if (res1) {
              let profile = _.extend(entity, {
                createdBy: userId
                // createdAt: new Date()
              });

              if (profile.clientFollowUpNotes && profile.clientFollowUpNotes.length > 0) {
                profile.clientFollowUpNotes[0].noteId = uuidv4();
                profile.clientFollowUpNotes[0].createdBy = userId;
                profile.clientFollowUpNotes[0].eventId = res1.data.id;
                profile.clientFollowUpNotes[0].createdAt = new Date();
              }

              if (profile.transactionDetails && profile.transactionDetails.length > 0) {
                profile.transactionDetails[0].createdBy = userId;
                profile.transactionDetails[0].createdAt = new Date();
              }

              if (profile.transactionDetails.length > 0 && profile.transactionDetails[0].resultNotes.length > 0) {
                profile.transactionDetails.map((res) => {
                  if (typeof res.resultNotes !== 'undefined') {
                    res.resultNotes[0].createdBy = userId;
                    res.resultNotes[0].createdAt = new Date();
                  }

                  return res;
                });
              }

              if (profile.otherDetails && profile.otherDetails.length > 0) {
                profile.otherDetails[0].createdBy = userId;
                profile.otherDetails[0].createdAt = new Date();
              }

              if (reqBody.middleName === '') {
                profile.fullName = reqBody.firstName.concat(' ') + reqBody.lastName;
              } else {
                profile.fullName = reqBody.firstName.concat(' ') + reqBody.middleName.concat(' ') + reqBody.lastName;
              }

              profile = new ClientProfile(profile);
              await profile.save();

              return profile;
            }

            return res1;
          });
      } else {
        let profile = _.extend(entity, {
          createdBy: userId
          // createdAt: new Date()
        });

        if (profile.clientFollowUpNotes && profile.clientFollowUpNotes.length > 0) {
          profile.clientFollowUpNotes[0].noteId = uuidv4();
          profile.clientFollowUpNotes[0].createdBy = userId;
          profile.clientFollowUpNotes[0].eventId = '';
          profile.clientFollowUpNotes[0].createdAt = new Date();
        }

        if (profile.transactionDetails && profile.transactionDetails.length > 0) {
          profile.transactionDetails[0].createdBy = userId;
          profile.transactionDetails[0].createdAt = new Date();
        }

        if (profile.transactionDetails.length > 0 && profile.transactionDetails[0].resultNotes.length > 0) {
          profile.transactionDetails.map((res) => {
            if (typeof res.resultNotes !== 'undefined') {
              res.resultNotes[0].createdBy = userId;
              res.resultNotes[0].createdAt = new Date();
            }

            return res;
          });
        }

        if (profile.otherDetails && profile.otherDetails.length > 0) {
          profile.otherDetails[0].createdBy = userId;
          profile.otherDetails[0].createdAt = new Date();
        }

        if (reqBody.middleName === '') {
          profile.fullName = reqBody.firstName.concat(' ') + reqBody.lastName;
        } else {
          profile.fullName = reqBody.firstName.concat(' ') + reqBody.middleName.concat(' ') + reqBody.lastName;
        }

        profile = new ClientProfile(profile);
        await profile.save();

        return profile;
      }
    }
  }

  return entity;
}

createClientProfile.schema = {
  userId: joi.string().required(),
  reqBody: joi.object().optional().allow(''),
  otherDetails: joi.object().optional().allow(''),
  auth: joi.object().optional().allow(''),
  entity: ClientValidation.entity
};

/**
 * creates the client profile
 * @param {String} userId the current user id
 * @param {Object} entity the request body entity
 * @returns {Object} the client profile information
 */
async function createClientProfileWithoutValidation(userId, entity, reqBody, auth) {
  const clientEmail = await helper.getClientByEmail(entity.email);
  const clientMobile = await helper.getClientByMobile(entity.mobile);

  if (clientEmail || (clientMobile && entity.mobile !== '')) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Email or Mobile already exists');
  }

  const calendar = google.calendar({ version: 'v3', auth });

  if (reqBody && reqBody.length !== 0) {
    const jsonArray = JSON.parse(reqBody.clientFollowUpNotes);
    if (reqBody.clientFollowUpNotes.length !== 0 && typeof jsonArray[0] !== 'undefined') {
      const date = new Date(jsonArray[0].clientFollowUpDate);
      const getDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');
      const getStartTime = [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
      const getEndTime = [date.getHours() + 1, date.getMinutes(), date.getSeconds()].join(':');

      const eventData = {
        summary: jsonArray[0].followUpNote,
        start: {
          dateTime: getDate.concat('T') + getStartTime.concat('+05:45')
          // timeZone: req.body.start.timeZone
        },
        end: {
          dateTime: getDate.concat('T') + getEndTime.concat('+05:45')
          // timeZone: req.body.end.timeZone
        },
        attendees: [
          {
            email: jsonArray[0].assignedTo
          }
        ]
      };

      await calendar.events
        .insert({
          auth,
          calendarId: 'primary',
          resource: eventData
        })
        .then(async (res1) => {
          if (res1) {
            let profile = _.extend(entity, {
              createdBy: userId
            });

            if (profile.clientFollowUpNotes && profile.clientFollowUpNotes.length > 0) {
              profile.clientFollowUpNotes[0].noteId = uuidv4();
              profile.clientFollowUpNotes[0].createdBy = userId;
              profile.clientFollowUpNotes[0].eventId = res1.data.id;
              profile.clientFollowUpNotes[0].createdAt = new Date();
            }

            if (profile.transactionDetails && profile.transactionDetails.length > 0) {
              profile.transactionDetails[0].createdBy = userId;
              profile.transactionDetails[0].createdAt = new Date();
            }

            if (profile.transactionDetails.length > 0 && profile.transactionDetails[0].resultNotes.length > 0) {
              profile.transactionDetails.map((res) => {
                if (typeof res.resultNotes !== 'undefined') {
                  res.resultNotes[0].createdBy = userId;
                  res.resultNotes[0].createdAt = new Date();
                }

                return res;
              });
            }

            if (profile.otherDetails && profile.otherDetails.length > 0) {
              profile.otherDetails[0].createdBy = userId;
              profile.otherDetails[0].createdAt = new Date();
            }

            if (reqBody.middleName === '') {
              profile.fullName = reqBody.firstName.concat(' ') + reqBody.lastName;
            } else {
              profile.fullName = reqBody.firstName.concat(' ') + reqBody.middleName.concat(' ') + reqBody.lastName;
            }

            profile = new ClientProfile(profile);
            await profile.save();

            return profile;
          }

          return res1;
        });
    } else {
      let profile = _.extend(entity, {
        createdBy: userId
        // createdAt: new Date()
      });

      if (profile.clientFollowUpNotes && profile.clientFollowUpNotes.length > 0) {
        profile.clientFollowUpNotes[0].noteId = uuidv4();
        profile.clientFollowUpNotes[0].createdBy = userId;
        profile.clientFollowUpNotes[0].eventId = '';
        profile.clientFollowUpNotes[0].createdAt = new Date();
      }

      if (profile.transactionDetails && profile.transactionDetails.length > 0) {
        profile.transactionDetails[0].createdBy = userId;
        profile.transactionDetails[0].createdAt = new Date();
      }

      if (profile.transactionDetails.length > 0 && profile.transactionDetails[0].resultNotes.length > 0) {
        profile.transactionDetails.map((res) => {
          if (typeof res.resultNotes !== 'undefined') {
            res.resultNotes[0].createdBy = userId;
            res.resultNotes[0].createdAt = new Date();
          }

          return res;
        });
      }

      if (profile.otherDetails && profile.otherDetails.length > 0) {
        profile.otherDetails[0].createdBy = userId;
        profile.otherDetails[0].createdAt = new Date();
      }

      if (reqBody.middleName === '') {
        profile.fullName = reqBody.firstName.concat(' ') + reqBody.lastName;
      } else {
        profile.fullName = reqBody.firstName.concat(' ') + reqBody.middleName.concat(' ') + reqBody.lastName;
      }

      profile = new ClientProfile(profile);
      await profile.save();

      return profile;
    }
  }

  // let profile = _.extend(entity, {
  //   createdBy: userId
  // });

  // if (profile.clientFollowUpNotes && profile.clientFollowUpNotes.length > 0) {
  //   profile.clientFollowUpNotes[0].noteId = uuidv4();
  //   profile.clientFollowUpNotes[0].createdBy = userId;
  //   profile.clientFollowUpNotes[0].eventId = '';
  //   profile.clientFollowUpNotes[0].createdAt = new Date();
  // }

  // if (profile.transactionDetails && profile.transactionDetails.length > 0) {
  //   profile.transactionDetails[0].createdBy = userId;
  //   profile.transactionDetails[0].createdAt = new Date();
  // }

  // if (profile.transactionDetails.length > 0 && profile.transactionDetails[0].resultNotes.length > 0) {
  //   profile.transactionDetails.map((res) => {
  //     if (typeof res.resultNotes !== 'undefined') {
  //       res.resultNotes[0].createdBy = userId;
  //       res.resultNotes[0].createdAt = new Date();
  //     }

  //     return res;
  //   });
  // }

  // if (profile.otherDetails && profile.otherDetails.length > 0) {
  //   profile.otherDetails[0].createdBy = userId;
  //   profile.otherDetails[0].createdAt = new Date();
  // }

  // profile = new ClientProfile(profile);

  // await profile.save();

  return entity;
}

createClientProfileWithoutValidation.schema = {
  userId: joi.string().required(),
  otherDetails: joi.object().optional().allow(''),
  reqBody: joi.object().optional().allow(''),
  auth: joi.object().optional().allow(''),
  entity: ClientValidation.entity
};

/**
 * Search Client Profiles
 * @param {Object} criteria Search criteria
 * @returns {Object} Search result
 */
const searchClientProfiles = async (criteria) => {
  const filter = {};
  // Keyword search
  //   let regex = new RegExp(value.searchQuery,'i');
  // const filterd = await Model.find({ $and: [ { $or: [{title: regex },
  // {description: regex}] }, {category: value.category}, {city:value.city} ] })

  if (criteria.keyword) {
    filter.$or = [
      { title: { $regex: criteria.keyword, $options: 'i' } },
      { firstName: { $regex: criteria.keyword, $options: 'i' } },
      { lastName: { $regex: criteria.keyword, $options: 'i' } },
      { mobile: { $regex: criteria.keyword, $options: 'i' } },
      { email: { $regex: criteria.keyword, $options: 'i' } },
      { mailingCity: { $regex: criteria.keyword, $options: 'i' } },
      { mailingState: { $regex: criteria.keyword, $options: 'i' } },
      { 'referrals.name': { $regex: criteria.keyword, $options: 'i' } },
      { 'referrals.referralLeadOpen': { $regex: criteria.keyword, $options: 'i' } },
      { 'referrals.referralLeadClose': { $regex: criteria.keyword, $options: 'i' } },
      {
        'clientFollowUpNotes.assignedTo': {
          $regex: criteria.keyword,
          $options: 'i'
        }
      },
      {
        'transactionDetails.ooInvst': {
          $regex: criteria.keyword,
          $options: 'i'
        }
      }
    ];
  }

  // In filters with Array
  if (criteria.maritalStatus) {
    filter.emailStatus = {};
    filter.emailStatus.$in = criteria.maritalStatus;
  }

  // query records
  // for sorting, add second sorting by _id if sortBy is not id, so that result order is determined
  if (criteria.sortBy === 'id' || !criteria.sortBy) {
    criteria.sortBy = '_id';
  }

  const sortStr = `${criteria.sortOrder.toLowerCase() === 'desc' ? '-' : ''}${criteria.sortBy}`;

  // Find records with pagination
  const results = await ClientProfile.find(filter)
    .collation({ locale: 'en' })
    .populate('createdBy', 'fullName email')
    .populate('updatedBy', 'fullName email')
    .sort(sortStr)
    .skip((+criteria.page - 1) * +criteria.perPage)
    .limit(+criteria.perPage);

  return {
    total: await ClientProfile.countDocuments(filter),
    results,
    page: criteria.page,
    perPage: criteria.perPage
  };
};

searchClientProfiles.schema = {
  criteria: joi.object().keys({
    keyword: joi.string().trim(),
    page: joi.page(),
    perPage: joi.perPage(),
    sortBy: joi.string().default('id'),
    sortOrder: joi.sortOrder()
  })
};

/**
 * Get Client Profile by ID
 * @param {String} clientProfileId Client Profile ID
 * @returns {Object} Client Profiles details
 */
async function getClientProfile(clientProfileId) {
  return helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
}

// getClientProfile.schema = {
//   clientProfileId: joi.id().required()
// };

/**
 * delete client profile by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteClientProfile(clientProfileId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  return clientProfile.remove();
}

deleteClientProfile.schema = {
  clientProfileId: joi.string().required()
};

/**
 * @param {String} userId the user id
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the updated client profile value
 * @returns {Object} the updated entity
 */
async function updateClientProfile(userId, clientProfileId, entity) {
  const clientEmail = await helper.getClientByEmail(entity.email);
  const clientMobile = await helper.getClientByMobile(entity.mobile);

  if (clientEmail || (clientMobile && entity.mobile !== '')) {
    throw new errors.HttpStatusError(httpStatus.CONFLICT, 'Email or Mobile already exists');
  }

  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.updatedBy = userId;
  _.assignIn(clientProfile, entity);

  if (clientProfile.clientFollowUpNotes && clientProfile.clientFollowUpNotes.length > 0) {
    clientProfile.clientFollowUpNotes[0].noteId = uuidv4();
    clientProfile.clientFollowUpNotes[0].createdBy = userId;
    clientProfile.clientFollowUpNotes[0].createdAt = new Date();
  }

  await clientProfile.save();

  return clientProfile;
}

// updateClientProfile.schema = {
//   clientProfileId: joi.string().required(),
//   entity: joi
//     .object()
//     .keys({
//       name: joi.string(),
//       isActive: joi.boolean()
//     })
//     .required()
// };

/**
 * @param {String} clientProfileId the client profile id
 * @returns [Array] the follow up notes
 */
async function getClientProfileFollowUpNotes(clientProfileId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  return clientProfile.clientFollowUpNotes.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
}

getClientProfileFollowUpNotes.schema = {
  clientProfileId: joi.id().required()
};

/**
 * @param {String} clientProfileId the client profile id
 * @param {String} noteId the follow up note id
 * @returns {Object} the updated entity
 */
async function getClientProfileFollowUpNoteById(clientProfileId, noteId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  const followUpNote = Array.from(clientProfile.clientFollowUpNotes).find((note) => note.noteId.toString() === noteId);

  if (!followUpNote) {
    throw new errors.NotFoundError(`The Follow Up Note ${clientProfileId} does not exist.`);
  }

  return followUpNote;
}

getClientProfileFollowUpNoteById.schema = {
  clientProfileId: joi.id().required(),
  noteId: joi.id().required()
};

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */
async function createClientProfileFollowUpNote(userId, clientProfileId, entity, auth) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  const calendar = google.calendar({ version: 'v3', auth });

  const date = new Date(entity.clientFollowUpDate);
  const getDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');
  const getStartTime = [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
  const getEndTime = [date.getHours() + 1, date.getMinutes(), date.getSeconds()].join(':');

  const eventData = {
    summary: entity.followUpNote,
    start: {
      dateTime: getDate.concat('T') + getStartTime.concat('+05:45')
      // timeZone: req.body.start.timeZone
    },
    end: {
      dateTime: getDate.concat('T') + getEndTime.concat('+05:45')
      // timeZone: req.body.end.timeZone
    },
    attendees: [
      {
        email: entity.assignedTo
      }
    ]
  };

  return calendar.events
    .insert({
      auth,
      calendarId: 'primary',
      resource: eventData
    })
    .then(async (res1) => {
      if (res1) {
        entity.noteId = uuidv4();
        entity.createdBy = userId;
        entity.createdAt = new Date();
        entity.eventId = res1.data.id;
        clientProfile.clientFollowUpNotes.push(entity);

        await clientProfile.save();

        return clientProfile;
      }

      return clientProfile;
    });

  // return caldendarSave;
}

createClientProfileFollowUpNote.schema = {
  userId: joi.string().required(),
  clientProfileId: joi.string().required(),
  auth: joi.object().optional().allow(''),
  entity: joi
    .object()
    .keys({
      followUpNote: joi.string().required(),
      clientFollowUpDate: joi.date().required(),
      assignedTo: joi.string().required()
    })
    .required()
};

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {String} noteId the follow up note id
 * @param {Object} entity the updated follow up note
 * @returns {Object} the updated note
 */
async function updateClientProfileFollowUpNoteById(userId, clientProfileId, noteId, entity, auth) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  const noteIndex = Array.from(clientProfile.clientFollowUpNotes).findIndex(
    (note) => note.noteId.toString() === noteId
  );

  if (noteIndex === -1) {
    throw new errors.NotFoundError(`The Follow Up Note ${clientProfileId} does not exist.`);
  }

  let toUpdateNote = clientProfile.clientFollowUpNotes[noteIndex];

  if (toUpdateNote.eventId !== '') {
    const calendar = google.calendar({ version: 'v3', auth });

    const date = new Date(entity.clientFollowUpDate);
    const getDate = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-');
    const getStartTime = [date.getHours(), date.getMinutes(), date.getSeconds()].join(':');
    const getEndTime = [date.getHours() + 1, date.getMinutes(), date.getSeconds()].join(':');

    const eventData = {
      summary: entity.followUpNote,
      start: {
        dateTime: getDate.concat('T') + getStartTime.concat('+05:45')
        // timeZone: req.body.start ? req.body.start.timeZone : ''
      },
      end: {
        dateTime: getDate.concat('T') + getEndTime.concat('+05:45')
        // timeZone: req.body.end ? req.body.end.timeZone : ''
      },
      attendees: [
        {
          email: entity.assignedTo
        }
      ]
    };

    await calendar.events.update({
      auth,
      calendarId: 'primary',
      eventId: toUpdateNote.eventId,
      requestBody: eventData
    });
  }

  toUpdateNote = {
    ...entity,
    noteId: toUpdateNote.noteId,
    createdBy: toUpdateNote.createdBy,
    createdAt: toUpdateNote.createdAt,
    eventId: toUpdateNote.eventId,
    updatedBy: userId,
    updatedAt: new Date()
  };

  clientProfile.clientFollowUpNotes[noteIndex] = toUpdateNote;

  await clientProfile.save();

  return toUpdateNote;
}

updateClientProfileFollowUpNoteById.schema = {
  userId: joi.string().required(),
  clientProfileId: joi.string().required(),
  auth: joi.object().optional().allow(''),
  noteId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      followUpNote: joi.string().required(),
      clientFollowUpDate: joi.date().required(),
      assignedTo: joi.string().required()
    })
    .required()
};
/**
 * @param {String} clientProfileId the client profile id
 * @param {String} noteId the follow up note id
 * @returns {Object} the deleted note
 */
async function deleteClientProfileFollowUpNoteById(clientProfileId, noteId, auth) {
  const calendar = google.calendar({ version: 'v3', auth });

  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  const noteIndex = Array.from(clientProfile.clientFollowUpNotes).findIndex(
    (note) => note.noteId.toString() === noteId
  );

  if (noteIndex === -1) {
    throw new errors.NotFoundError(`The Follow Up Note ${clientProfileId} does not exist.`);
  }

  const deletedNote = clientProfile.clientFollowUpNotes[noteIndex];

  clientProfile.clientFollowUpNotes.splice(noteIndex, 1);

  await clientProfile.save();

  if (deletedNote.eventId && deletedNote.eventId !== '') {
    await calendar.events.delete({
      auth,
      calendarId: 'primary',
      eventId: deletedNote.eventId
    });
  }

  return deletedNote;
}

deleteClientProfileFollowUpNoteById.schema = {
  clientProfileId: joi.string().required(),
  noteId: joi.string().required(),
  auth: joi.object().optional().allow()
};

/**
 * @param {String} clientProfileId the client profile id
 * @returns [Array] the follow up notes
 */
async function getClientProfileOtherDetailsById(clientProfileId, otherId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  const otherdetails = Array.from(clientProfile.otherDetails).find((other) => other._id.toString() === otherId);

  if (!otherdetails) {
    throw new errors.NotFoundError(`The other Details ${otherId} does not exist.`);
  }

  return otherdetails;
}

getClientProfileOtherDetailsById.schema = {
  clientProfileId: joi.id().required(),
  otherId: joi.id().required()
};
// return clientProfile.otherDetails.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
// return clientProfile.correspondingUploadPaths.sort(
//   (a, b) => b.createdAt.getTime() - a.createdAt.getTime(),
//   clientProfile.correspondingNote
// );
// return clientProfile.correspondingNote;
// }

// getClientProfileOtherDetails.schema = {
//   clientProfileId: joi.id().required()
// };

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */
async function createClientProfileOtherDetails(userId, clientProfileId, entity) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  entity.createBy = userId;
  entity.createdAt = new Date();
  clientProfile.otherDetails.push(entity);
  await clientProfile.save();

  return entity;
}

// createClientProfileOtherDetails.schema = {
//   userId: joi.string().required(),
//   clientProfileId: joi.string().required(),
//   entity: joi.object().required()
// };

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */
const updateClientProfileOtherDetails = async (userId, clientProfileId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  entity.createBy = userId;
  entity.createdAt = new Date();
  clientProfile.correspondingUploadPaths.push(entity);
  clientProfile.correspondingNote.push(entity);

  await clientProfile.save();

  return entity;
};

async function updateClientProfileAddFile(userId, clientProfileId, otherDetailId, entity) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.otherDetails.forEach((x) => {
    if (x._id.toString() == otherDetailId.toString()) {
      x.correspondingUploadPaths.push(entity);
    }
  });

  entity.createBy = userId;
  entity.createdAt = new Date();
  //  return clientProfile.otherDetails.push(entity);
  // await clientProfile.save(entity);
  return entity;
}

// updateClientProfileOtherDetails.schema = {
//   userId: joi.string().required(),
//   clientProfileId: joi.string().required(),
//   entity: joi
//     .object()
//     .keys({
//       correspondingUploadPaths: joi.string().optional().allow(''),
//       // correspondingUploadPaths: joi.array().items(joi.string().optional().allow('')),
//       notes: joi.string().optional().allow('')
//     })
//     .required()
// };
/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */
const deleteClientProfileOtherDetails = async (clientProfileId, transactionId, otherId) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  //  clientprofile >> transactionDetails >> correspondingNote >>
  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.remove(otherId);
    }
    return res;
  });

  await clientProfile.save();
  return clientProfile;
};

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {String} noteId the follow up note id
 * @param {Object} entity the updated follow up note
 * @returns {Object} the updated note
 */
async function updateClientProfileOtherNoteById(userId, clientProfileId, otherDetailsId, entity) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  const otherId = await helper.ensureEntityExists(
    ClientProfile.otherDetails,
    { _id: otherDetailsId },
    `The other details ${ClientProfile.otherDetails} does not exist.`
  );
  // const otherId = await ClientProfile.OtherDetails.find({ otherDetailsId });
  if (otherId === -1) {
    throw new errors.NotFoundError(`The others details ${ClientProfile.otherDetails} does not exist.`);
  }

  let toUpdateNote = clientProfile.OtherDetails[otherId];

  toUpdateNote = {
    ...entity,

    createdBy: toUpdateNote.createdBy,
    createdAt: toUpdateNote.createdAt,
    updatedBy: userId,
    updatedAt: new Date()
  };

  clientProfile.OtherDetails[otherId] = toUpdateNote;

  await clientProfile.save();

  return toUpdateNote;
}

updateClientProfileOtherNoteById.schema = {
  userId: joi.string().required(),
  clientProfileId: joi.string().required(),
  noteId: joi.string().required(),
  entity: joi
    .object()
    .keys({
      followUpNote: joi.string().required(),
      clientFollowUpDate: joi.date().required(),
      assignedTo: joi.string().required()
    })
    .required()
};

/**
 * @param {String} userName the user name
 * @param {String} clientProfileId the client profile id
 * @param {Object} entity the new follow up note
 * @returns {Object} the entity
 */

// const createClientProfileTransactionDetails = async (tag) => Transaction.create(tag);
const createClientProfileTransactionDetailsService = async (userId, clientProfileId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  entity.createdBy = userId;
  entity.createdAt = new Date();
  if (typeof entity.resultNotes[0] !== 'undefined') {
    entity.resultNotes[0].createdBy = userId;
    entity.resultNotes[0].createdAt = new Date();
  }

  await clientProfile.transactionDetails.push(entity);

  await clientProfile.save();

  return clientProfile;
};

// createClientProfileTransactionDetails.schema = {
//   userId: joi.string().required(),
//   clientProfileId: joi.string().required(),
//   entity: joi
//     .object()
//     .keys({
//       propertyLotAddress: joi.string().optional().allow(''),
//       upTo: joi.string().optional().allow(''),
//       purchaseStatus: joi.string().optional().allow(''),
//       ooInvst: joi.string().optional().allow(''),
//       // resultNotes: joi.string().optional().allow(''),
//       propertyType: joi.string().optional().allow('')
//     })
//     .required()
// };

/**
 * delete client profile by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteClientProfileTransactionDetails(clientProfileId, transactionId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.transactionDetails.filter((res) => {
    res._id !== transactionId;
    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

deleteClientProfileTransactionDetails.schema = {
  clientProfileId: joi.string().required(),
  transactionId: joi.string().required()
};

const addCorrespondingInTransactionService = async (userId, clientProfileId, transactionId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      entity.createdBy = userId;
      entity.createdAt = new Date();
      // if (typeof entity.otherNotes[0] !== 'undefined') {
      //   entity.otherNotes[0].createdBy = userId;
      //   entity.otherNotes[0].createdAt = new Date();
      // }
      res.correspondingNote.push(entity.correspondingNote[0]);
    }

    return res;
  });

  await clientProfile.save();

  return entity;
};

const addTransactionNoteService = async (userId, clientProfileId, transactionId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      entity.createdBy = userId;
      entity.createdAt = new Date();
      res.resultNotes.push(entity);
    }

    return res;
  });

  await clientProfile.save();

  return clientProfile;
};

async function deleteTransactionNoteService(clientProfileId, transactionId, noteId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.resultNotes.remove(noteId);
    }
    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

async function updateTransactionNoteService(userId, clientProfileId, transactionId, noteId, entity) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      const noteRes = res.resultNotes.filter((resNotes) => resNotes._id == noteId);

      if (noteRes !== []) {
        // const updatedBy = 'Susan';
        noteRes[0].note = entity.note;
      }

      return noteRes;
    }
    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

const validData = (x) => {
  if (!x) return false;
  try {
    return !isNaN(parseInt(x));
  } catch (e) {
    return false;
  }
};

/**
 * @returns {Object} get  all Client types
 */
const getTransactionDetails = async () => {
  const transactions = await ClientProfile.find({});
  // eslint-disable-next-line no-confusing--arrow
  const map = transactions.map((x) =>
    x.transactionDetails
      ? x.transactionDetails
          .map((t) => t.upTo)
          .filter((x) => validData(x))
          .map((x) => parseInt(x))
      : ''
  );

  const _map = {
    250: 0,
    350: 0,
    450: 0,
    550: 0,
    650: 0,
    750: 0,
    850: 0,
    950: 0,
    1000: 0,
    0: 0
  };
  map.forEach((x) => {
    x.forEach((y) => {
      if (y > 1000000) {
        _map['1000']++;
      } else if (y >= 950000) {
        _map['950']++;
      } else if (y >= 850000) {
        _map['850']++;
      } else if (y >= 750000) {
        _map['750']++;
      } else if (y >= 650000) {
        _map['650']++;
      } else if (y >= 550000) {
        _map['550']++;
      } else if (y >= 450000) {
        _map['450']++;
      } else if (y >= 350000) {
        _map['350']++;
      } else if (y >= 250000) {
        _map['250']++;
      } else {
        _map['0']++;
      }
    });
  });
  return _map;
};

/**
 * @returns {Object} get  all Client types
 */

const getTotalClientsType = async (filterDate) => {
  const a = await getTransactionDetails();
  const date = new Date(filterDate);
  const endDate = new Date(date.getFullYear(), new Date(date).getMonth() + 1, 0);
  const checkDate = filterDate ? { entryDate: { $gt: date, $lte: endDate } } : {};
  const _totalClient = ClientProfile.count({
    customerType: 'Clients',
    ...checkDate
  });
  const _totalProspect = ClientProfile.count({
    customerType: 'Prospect',
    ...checkDate
  });
  const _Others = ClientProfile.count({ customerType: 'Others', ...checkDate });
  const _totalNull = ClientProfile.count({ customerType: '', ...checkDate });
  const _activeClient = ClientProfile.count({
    clientStatus: 'Active',
    ...checkDate
  });
  const _inactiveClient = ClientProfile.count({
    clientStatus: 'Inactive',
    ...checkDate
  });
  const _othersClient = ClientProfile.count({
    clientStatus: 'Others',
    ...checkDate
  });
  const _result = ClientProfile.find({}, { customerType: 1 }).collation({
    locale: 'en'
  });
  const _grandTotalClient = ClientProfile.count({ customerType: 'Clients' });
  const _grandTotalProspect = ClientProfile.count({ customerType: 'Prospect' });
  const _grandTotalOthers = ClientProfile.count({
    $or: [{ customerType: 'Others' }, { customerType: '' }]
  });

  const _rawLead = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'raw_lead',
      ...checkDate
    },
    { transactionDetails: 1 }
  );

  const _qualified = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'qualified',
      ...checkDate
    },

    { transactionDetails: 1 }
  );
  const _negotiating = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'negotiating',
      ...checkDate
    },

    { transactionDetails: 1 }
  );
  const _win = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'win',
      ...checkDate
    },

    { transactionDetails: 1 }
  );
  const _lost = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'lost',
      ...checkDate
    },
    { transactionDetails: 1 }
  );

  const _followUp = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'follow_up',
      ...checkDate
    },

    { transactionDetails: 1 }
  );
  const _others = ClientProfile.find(
    {
      'transactionDetails.purchaseStatus': 'Others',
      ...checkDate
    },
    { transactionDetails: 1 }
  );

  const [
    totalClient,
    totalProspect,
    Others,
    totalNull,
    activeClient,
    inactiveClient,
    othersClient,
    result,
    grandTotalClient,
    grandTotalProspect,
    grandTotalOthers,
    rawLead,
    qualified,
    negotiating,
    win,
    lost,
    others,
    followUp
  ] = await Promise.all([
    _totalClient,
    _totalProspect,
    _Others,
    _totalNull,
    _activeClient,
    _inactiveClient,
    _othersClient,
    _result,
    _grandTotalClient,
    _grandTotalProspect,
    _grandTotalOthers,
    _rawLead,
    _qualified,
    _negotiating,
    _win,
    _lost,
    _others,
    _followUp
  ]);

  const totalOthers = Others + totalNull;

  // const noValue = await ClientProfile.find(
  //   {
  //     'transactionDetails.propertyType': ''
  //   },

  //   { transactionDetails: 1 }
  // );
  const results = {
    totalClient,
    totalProspect,
    totalOthers,
    activeClient,
    inactiveClient,
    othersClient,
    totalCustomer: await ClientProfile.countDocuments(result),
    totalRawLead: rawLead.reduce(
      (x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'raw_lead').length,
      0
    ),
    totalQualified: qualified.reduce(
      (x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'qualified').length,
      0
    ),
    totalNegotiating: negotiating.reduce(
      (x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'negotiating').length,
      0
    ),
    totalWin: win.reduce((x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'win').length, 0),
    totalLost: lost.reduce((x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'lost').length, 0),
    totalFollowUp: followUp.reduce(
      (x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'follow_up').length,
      0
    ),
    totalPropertyOthers: others.reduce(
      (x, y) => x + y.transactionDetails.filter((x) => x.purchaseStatus === 'Others').length,
      0
    ),
    totalProperty:
      rawLead.length +
      qualified.length +
      negotiating.length +
      win.length +
      lost.length +
      followUp.length +
      others.length,
    customerTypes: ['Clients', 'Prospect', 'Others'],
    customerTypesValue: [grandTotalClient, grandTotalProspect, grandTotalOthers],
    transationPropertyTypes: ['Raw Lead', 'Qualified', 'Negotiating', 'win', 'lost', 'Follow up in future', 'Others'],
    transationPropertyTypesValue: [
      rawLead.length,
      qualified.length,
      negotiating.length,
      win.length,
      lost.length,
      followUp.length,
      others.length
    ],
    transationDetails: a,
    grandValue: {
      grandTotalClient,
      grandTotalProspect,
      grandTotalOthers,
      grandTotal: grandTotalClient + grandTotalProspect + grandTotalOthers
    }
  };
  return results;
};

// /**
//  * handles search client Status types
//  * @param {Object} req the http request
//  * @param {Object} res the http response
//  */
// const getPropertyDetails = async () => {
//   const monthData = new Date();
//   monthData.setMonth(monthData.getMonth() - 1);
//   ClientProfile.find({ PurchaseDate: { $gte: monthData } });
// };
// // const  = async testRoute() => {
// //   return 'This is for testing'
// // }

/**
 * @returns {Object} the entity
 */
const getClientFollowupNote = async () => {
  const getAllClient = await ClientProfile.find({}, { clientFollowUpNotes: 1 }).sort({ clientFollowUpDate: 1 });

  const getAllWithFilter = getAllClient.filter(
    (res) => res.clientFollowUpNotes.length !== 0 || res.clientFollowUpNotes == []
  );

  const singleArrayFollowUp = getAllWithFilter.map((_res1) => {
    const res1 = JSON.parse(JSON.stringify(_res1));
    res1.clientFollowUpNotes = res1.clientFollowUpNotes
      .map((x) => {
        x.clientId = _res1._id;
        return x;
      })
      .filter((y) => y.clientFollowUpDate);
    return res1.clientFollowUpNotes;
  });

  const flatFollowUp = singleArrayFollowUp
    .flat()
    .sort((x, y) => new Date(x.clientFollowUpDate) - new Date(y.clientFollowUpDate));

  const topFlatFollowUp = flatFollowUp.filter((resValue) => resValue.isNoteCompleted === false);

  return topFlatFollowUp;
  // return getAllClient
};

// Get referred Client Details
async function getClientsDetailsToReferral(userId) {
  const b = await getUserProfileById(userId);
  const referredClient = await ClientProfile.find({
    'referrals.email': b.email
  });
  return referredClient;
}

// return Episode.find({ airedAt: new Date('1987-10-26') })
// Get Assigned To Me Work
async function getClientAssignToMe(userId) {
  const b = await getUserProfileById(userId);
  const referredClient = await ClientProfile.find({
    'clientFollowUpNotes.assignedTo': b.email
  });
  const clientAssignData = await referredClient.map((el) => el.clientFollowUpNotes);

  const clientAssignToData = await clientAssignData.flat();

  const clientAssignSliceValue = clientAssignToData.slice(0, 5);

  const clientAssignIncomplete = clientAssignSliceValue.filter((resValue) => resValue.isNoteCompleted === false);

  return clientAssignIncomplete;
}

// // New Transaction Detail For Client Profile
// async function createNewTransaction(userId, entity) {
//   const resultNotes = [];
//   const correspondingUploadPaths = [];
//   const correspondingNote = [];
//   resultNotes.push(entity.resultNotes);
//   correspondingUploadPaths.push(entity.correspondingUploadPaths);
//   correspondingNote.push({
//     correspondingUploadPaths,
//     createdBy: userId.createdBy,
//     createdAt: userId.createdAt,
//     updatedBy: userId.updatedBy
//   });

//   const transaction = new Transaction({
//     propertyLotAddress: entity.propertyLotAddress,
//     correspondingNote
//   });
//   // const clientProfile = await helper.ensureEntityExists(
//   //   ClientProfile,
//   //   { _id: clientProfileId },
//   //   `The Client Profile ${clientProfileId} does not exist.`
//   // );
//   await transaction.save();
//   return entity;
// transDetail.correspondingNotes = transDetail.correspondingNotes.map(
//         (coNotes) => {
//           if (coNotes._id === "uicorrespondingNOtes") {
//  entity.createdBy = userId;
// entity.createdAt = new Date();
// res.correspondingNote.push(entity);

// coNotes.correspondingUploadPath = methods
// }
// client=> transaction=> correspondece => othernote=> notes

// Add correspondece Note Arjun
const addCorrespondenceNoteService = async (userId, clientProfileId, transactionId, correspondeceId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.map((resCo) => {
        if (resCo._id == correspondeceId) {
          entity.createdBy = userId;
          entity.createdAt = new Date();
          resCo.otherNotes.push(entity);
        }
        return resCo;
      });
    }

    return res;
  });

  await clientProfile.save();

  return clientProfile;
};

// Update corresponding Notes on Transaction
//   const noteRes = res.resultNotes.filter((resNotes) => resNotes._id == noteId);
// Add correspondece Note Arjun
//
// if (noteRes !== []) {
//   // const updatedBy = 'Susan';
//   noteRes[0].note = entity.note;
// }

const updateCorrespondenceNoteService = async (
  userId,
  clientProfileId,
  transactionId,
  correspondeceId,
  noteId,
  entity
) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.map((resCo) => {
        if (resCo._id == correspondeceId) {
          const otherNoteDetail = resCo.otherNotes.filter((resNote) => resNote._id == noteId);
          otherNoteDetail[0].otherNote = entity.otherNote;
        }
        return resCo;
      });
    }

    return res;
  });

  await clientProfile.save();

  return clientProfile;
};

//  Delete Corresponding Note
async function deleteCorrespondingNoteService(clientProfileId, transactionId, correspondeceId, noteId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.map((resCo) => {
        if (resCo._id == correspondeceId) {
          resCo.otherNotes.remove(noteId);
        }
        return resCo;
      });
    }

    return res;
  });
  // clientProfile.transactionDetails.map((res) => {
  //   if (res._id == transactionId) {
  //     res.resultNotes.remove(noteId);
  //   }
  //   return res;
  // });

  await clientProfile.save();

  return clientProfile;
}

// Add File in Corresponding
const addCorrespondenceFileService = async (clientProfileId, transactionId, correspondeceId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.map((resCo) => {
        if (resCo._id == correspondeceId) {
          resCo.correspondingUploadPaths.push(...entity.correspondingUploadPaths);
        }
        return resCo;
      });
    }
    return entity;
  });

  await clientProfile.save();
  return clientProfile;
};

/**
 * @returns {Object} get  all Transaction Detail and Correspondece types
 */
const getTransactionDetailAndCorrespondence = async (clientProfileId) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  return clientProfile.transactionDetails;
  // const transactions = await ClientProfile.find({}, { transactionDetails: 1 });
  // //  const result =  transactions.map((res) => res)
  // return transactions;
};

//  Delete Corresponding File
async function deleteCorrespondingFileService(clientProfileId, transactionId, correspondeceId, fileId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.transactionDetails.map((res) => {
    if (res._id == transactionId) {
      res.correspondingNote.map((resCo) => {
        if (resCo._id == correspondeceId) {
          resCo.correspondingUploadPaths.splice(fileId, 1);
        }
        return resCo;
      });
    }

    return res;
  });
  // clientProfile.transactionDetails.map((res) => {
  //   if (res._id == transactionId) {
  //     res.resultNotes.remove(noteId);
  //   }
  //   return res;
  // });

  await clientProfile.save();

  return clientProfile;
}

// Add New Other Detail
const createClientProfileOtherDetailService = async (userId, clientProfileId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  entity.createdBy = userId;
  entity.createdAt = new Date();
  await clientProfile.otherDetails.push(entity.otherDetails[0]);

  await clientProfile.save();

  return clientProfile;
};

/**
 * delete other corresponding  by id
 * @param {String} clientProfileId the client profile id
 */
async function deleteOtherDetailsService(clientProfileId, otherId) {
  const clientProfile = await getClientProfile(clientProfileId);
  if (!clientProfile) throw new Error('Client Profile is null');

  const deleteNewOther = await ClientProfile.update(
    { _id: clientProfileId },
    { $pull: { otherDetails: { _id: otherId } } },
    { safe: true, multi: true }
  );

  return deleteNewOther;
}

deleteOtherDetailsService.schema = {
  clientProfileId: joi.string().required(),
  otherId: joi.string().required()
};

// Add Files on New Other
// Add File in Corresponding
const addOtherFileService = async (clientProfileId, otherId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.otherDetails.map((res) => {
    if (res._id == otherId) {
      res.correspondingOtherPath.push(...entity.correspondingOtherPath);
    }
    return res;
  });

  await clientProfile.save();
  return clientProfile;
};

// Add correspondece Note Arjun
const addOtherNoteService = async (userId, clientProfileId, otherId, entity) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.otherDetails.map((res) => {
    if (res._id == otherId) {
      entity.createdBy = userId;
      entity.createdAt = new Date();
      res.newOtherNotes.push(entity);
    }

    return res;
  });

  await clientProfile.save();

  return clientProfile;
};

// Delete Notes in New Other
async function deleteOtherNoteService(clientProfileId, otherId, noteId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.otherDetails.map((res) => {
    if (res._id == otherId) {
      res.newOtherNotes.remove(noteId);
    }
    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

// Update Note In new Other Note
async function updateNewOtherNoteService(userId, clientProfileId, otherId, noteId, entity) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );
  clientProfile.otherDetails.map((res) => {
    if (res._id == otherId) {
      const noteRes = res.newOtherNotes.filter((resNotes) => resNotes._id == noteId);
      if (noteRes !== []) {
        noteRes[0].newOtherNote = entity.newOtherNote;
      }

      return noteRes;
    }
    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

//  Delete Other File
async function deleteOtherFileService(clientProfileId, otherId, fileId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.otherDetails.map((res) => {
    if (res._id == otherId) {
      res.correspondingOtherPath.splice(fileId, 1);
    }

    return res;
  });

  await clientProfile.save();

  return clientProfile;
}

/* Completed FollowUp Note is set to True  */
const completeFollowUpNote = async (clientProfileId, completedId) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.clientFollowUpNotes.map((res) => {
    if (res._id == completedId) {
      res.isNoteCompleted = true;
    }
    return res;
  });
  await clientProfile.save();
  return 'Congratulations, this task has been completed successfully.';
};

/* Incompleted FollowUp Note is set to False  */
const incompleteFollowUpNote = async (clientProfileId, completedId) => {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  clientProfile.clientFollowUpNotes.map((res) => {
    if (res._id == completedId) {
      res.isNoteCompleted = false;
    }
    return res;
  });
  await clientProfile.save();
  // return clientProfile;
  return 'Congratulations, this task has been incompleted successfully.';
};

/**
 * @param {String} clientProfileId the client profile id
 * @returns [Array] the follow up notes
 */
async function getClientProfileOtherDetails(clientProfileId) {
  const clientProfile = await helper.ensureEntityExists(
    ClientProfile,
    { _id: clientProfileId },
    `The Client Profile ${clientProfileId} does not exist.`
  );

  return clientProfile.otherDetails;
  // return clientProfile.otherDetails.sort((a, b) => b.createdAt.getTime() - a.createdAt.getTime());
}

getClientProfileOtherDetails.schema = {
  clientProfileId: joi.id().required()
};

module.exports = {
  createClientProfile,
  searchClientProfiles,
  getClientProfile,
  deleteClientProfile,
  updateClientProfile,
  getClientProfileFollowUpNotes,
  getClientProfileFollowUpNoteById,
  createClientProfileFollowUpNote,
  updateClientProfileFollowUpNoteById,
  deleteClientProfileFollowUpNoteById,
  getClientProfileOtherDetailsById,
  createClientProfileOtherDetails,
  updateClientProfileOtherDetails,
  deleteClientProfileOtherDetails,
  updateClientProfileOtherNoteById,
  getTotalClientsType,
  getTransactionDetails,
  createClientProfileTransactionDetailsService,
  deleteClientProfileTransactionDetails,
  getClientFollowupNote,
  getClientsDetailsToReferral,
  getClientAssignToMe,
  updateClientProfileAddFile,
  createClientProfileWithoutValidation,
  addTransactionNoteService,
  deleteTransactionNoteService,
  updateTransactionNoteService,
  addCorrespondingInTransactionService,
  addCorrespondenceNoteService,
  updateCorrespondenceNoteService,
  deleteCorrespondingNoteService,
  addCorrespondenceFileService,
  getTransactionDetailAndCorrespondence,
  deleteCorrespondingFileService,
  createClientProfileOtherDetailService,
  deleteOtherDetailsService,
  addOtherFileService,
  addOtherNoteService,
  deleteOtherNoteService,
  updateNewOtherNoteService,
  deleteOtherFileService,
  completeFollowUpNote,
  incompleteFollowUpNote,
  getClientProfileOtherDetails
};
