/**
 * @license
 * Copyright Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// [START calendar_quickstart]
const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/calendar.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return err;
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), () => {
        if (err) return err;
      });
      callback(oAuth2Client);
    });
  });
}

// Load client secrets from a local file.
/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback, respObj) {
  const { clientSecret, clientId, redirectUris } = credentials.web;
  const oAuth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUris[0]);
  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client, respObj);
  });
}

/**
 * Lists the next 10 events on the user's primary calendar.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listEvents(auth, callback) {
  const calendar = google.calendar({ version: 'v3', auth });
  calendar.events.list(
    {
      calendarId: 'primary',
      timeMin: new Date().toISOString(),
      maxResults: 10,
      singleEvents: true,
      orderBy: 'startTime'
    },
    (err, res) => {
      if (err) return err;
      const events = res.data.items;
      callback(events);
      //   if (events.length) {

      //     events.map((event, i) => {
      //       const start = event.start.dateTime || event.start.date;

      //     });
      //   } else {

      //   }
    }
  );
}

function getEvents(callback) {
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Calendar API.
    authorize(JSON.parse(content), listEvents, callback);
  });
}

// const createEvent = async (callback) => {
//   // Instance of calendar
//   const calendar = google.calendar({ version: 'v3' });

//   // Start date set to next day 3PM
//   const startTime = new Date();
//   startTime.setDate(startTime.getDate() + 1);
//   startTime.setHours(15, 0, 0, 0);

//   // End time set 1 hour after start time
//   const endTime = new Date(startTime.getTime());
//   endTime.setMinutes(startTime.getMinutes() + 60);

//   const newEvent = {
//     calendarId: 'primary',
//     resource: {
//       start: {
//         dateTime: startTime.toISOString()
//       },
//       end: {
//         dateTime: endTime.toISOString()
//       }
//     }
//   };
// [END calendar_quickstart]
module.exports = {
  SCOPES,
  getEvents
  // createEvent
};
