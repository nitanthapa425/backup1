const joi = require('joi');
const constants = require('../constants');

const ClientValidation = {
  entity: joi
    .object()
    .keys({
      title: joi.string().optional().allow(''),
      auth: joi.object().optional().allow(''),
      firstName: joi
        .string()
        .required()
        .regex(/^[aA-zZ\s]+$/),
      middleName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      lastName: joi
        .string()
        .required()
        .regex(/^[aA-zZ\s]+$/),
      phone: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      mobile: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      email: joi.string().lowercase().email({ minDomainAtoms: 2 }).required(),
      country: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      mailingStreetAddress: joi.string().optional().allow(''),
      mailingStreetNo: joi.string().optional().allow(''),
      mailingSuburb: joi.string().optional().allow(''),
      mailingCountry: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      residentialStreetNo: joi.string().optional().allow(''),
      spouseNumber: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      spouseLandline: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      partnerNumber: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      partnerLandline: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      entryDate: joi.string().optional().allow(''),
      mailingCity: joi.string().optional().allow(''),
      // .regex(/^[aA-zZ\s]+$/),
      mailingState: joi.string().optional().allow(''),
      mailingZip: joi
        .string()
        .optional()
        .allow('')
        .max(10)
        .regex(/^[0-9]+$/),
      residentialStreetAddress: joi.string().optional().allow(''),
      residentialSuburb: joi.string().optional().allow(''),
      residentialCountry: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      residentialCity: joi.string().optional().allow(''),
      // .regex(/^[aA-zZ\s]+$/),
      residentialState: joi.string().optional().allow(''),
      residentialZip: joi
        .string()
        .optional()
        .allow('')
        .max(10)
        .regex(/^[0-9]+$/),
      maritalStatus: joi
        .string()
        .optional()
        .allow('')
        .valid(Object.values(constants.MARITAL_STATUS))
        .required()
        .regex(/^[aA-zZ\s]+$/),
      spouseTitle: joi.string().optional().allow(''),
      spouseFirstName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      spouseMiddleName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      spouseLastName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      spouseEmail: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      partnerTitle: joi.string().optional().allow(''),
      partnerFirstName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      partnerMiddleName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      partnerLastName: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      partnerEmail: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      customerType: joi.string().valid(Object.values(constants.CUSTOMER_TYPE)).optional().allow(''),
      correspondingUploadPaths: joi.array().items(joi.string()).optional().allow(''),
      correspondingNote: joi.string().optional().allow(''),
      clientStatus: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[aA-zZ\s]+$/),
      clientFollowUpNumber: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      clientFollowUpPhone: joi
        .string()
        .optional()
        .allow('')
        .regex(/^[0-9]{10,14}$/),
      clientFollowUpEmail: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
      clientFollowUpNotes: joi.array().items(
        joi.object().keys({
          followUpNote: joi.string().optional().allow(''),
          clientFollowUpDate: joi.string().optional().allow(''),
          assignedTo: joi.string().optional().allow('')
        })
      ),

      referrals: joi.object().keys({
        referralCode: joi.string().optional().allow(''),
        name: joi
          .string()
          .optional()
          .allow('')
          .regex(/^[aA-zZ\s]+$/),
        email: joi.string().lowercase().email({ minDomainAtoms: 2 }).optional().allow(''),
        city: joi.string().optional().allow(''),
        // .regex(/^[aA-zZ\s]+$/),
        streetAddress: joi.string().optional().allow(''),
        streetNo: joi.string().optional().allow(''),
        state: joi.string().optional().allow(''),
        zip: joi
          .string()
          .optional()
          .allow('')
          .max(10)
          .regex(/^[0-9]+$/),
        referralLeadOpen: joi.string().optional().allow(''),
        referralLeadClose: joi.string().optional().allow(''),
        phone: joi
          .string()
          .optional()
          .allow('')
          .regex(/^[0-9]{10,14}$/),
        suburb: joi.string().optional().allow(''),
        country: joi
          .string()
          .optional()
          .allow('')
          .regex(/^[aA-zZ\s]+$/)
      }),
      otherDetails: joi.array().items(
        joi.object().keys({
          correspondingOtherPath: joi.array().optional().allow(''),
          newOtherNotes: joi.array().items(
            joi.object().keys({
              newOtherNote: joi.string().optional().allow(''),
              createdAt: joi.date().optional().allow(''),
              createdBy: joi.string().optional().allow(''),
              updatedBy: joi.string().optional().allow('')
            })
          ),
          createdAt: joi.date().optional().allow(''),
          createdBy: joi.string().optional().allow('')
        })
      ),

      transactionDetails: joi.array().items(
        joi.object().keys({
          propertyLotAddress: joi.string().optional().allow(''),
          // priceFrom: joi.string().optional().allow(''),
          upTo: joi.string().optional().allow(''),
          propertyType: joi.string().optional().allow(''),
          purchaseStatus: joi.string().optional().allow(''),
          ooInvst: joi.string().optional().allow(''),
          resultNotes: joi.array().items(
            joi.object().keys({
              note: joi.string().optional().allow(''),
              createdAt: joi.date().optional().allow(''),
              createdBy: joi.string().optional().allow(''),
              updatedBy: joi.string().optional().allow('')
            })
          ),
          createdAt: joi.date().optional().allow(''),
          createdBy: joi.string().optional().allow(''),
          updatedBy: joi.string().optional().allow(''),
          correspondingNote: joi.array().items(
            joi.object().keys({
              correspondingUploadPaths: joi.array().optional().allow(''),
              otherNotes: joi.array().items(
                joi.object().keys({
                  otherNote: joi.string().optional().allow(''),
                  createdAt: joi.date().optional().allow(''),
                  createdBy: joi.string().optional().allow(''),
                  updatedBy: joi.string().optional().allow(''),
                  updatedAt: joi.date().optional().allow('')
                })
              ),
              createdAt: joi.date().optional().allow(''),
              createdBy: joi.string().optional().allow(''),
              updatedBy: joi.string().optional().allow('')
            })
          ) // next line
        })
      ) // main closing
    })
    .required()
};

module.exports = {
  ClientValidation
};
