const { Schema } = require('mongoose');

const GlobalSearchSchema = new Schema({
  label: {
    type: String
  },
  url: {
    type: String
  }
});

module.exports = {
  GlobalSearchSchema
};
