/**
 * Copyright (C) Tuki Logic
 */

const _ = require('lodash');
const { Schema } = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const { CLIENT_STATUS } = require('../constants');

/**
 * the Client New Other schema
 * @author      Ashim Karki
 * @version     1.0
 */

const otherNewSchema = new Schema({
  correspondingOtherPath: [
    {
      type: String
    }
  ],
  newOtherNotes: [
    {
      newOtherNote: {
        type: String
      },
      createdBy: {
        type: String
      },
      createdAt: {
        type: Date
      },
      updatedBy: {
        type: String
      },
      updatedAt: {
        type: Date
      }
    }
  ],
  createdBy: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedBy: {
    type: String
  },
  updatedAt: {
    type: Date
  }
});

/**
 * the Client Referral schema
 * @author      Ashim Karki
 * @version     1.0
 */

const ClientReferralSchema = new Schema({
  referralCode: {
    type: String
  },
  name: {
    type: String
  },
  email: {
    type: String,
    lowercase: true
  },
  phone: {
    type: String
  },
  streetNo: {
    type: String
  },
  streetAddress: {
    type: String
  },
  suburb: {
    type: String
  },
  state: {
    type: String
  },
  zip: {
    type: String
  },
  country: {
    type: String
  },
  referralLeadOpen: {
    type: String
  },
  referralLeadClose: {
    type: String
  }
});

/**
 * the Follow Up Note schema
 * @author      Ashim Karki
 * @version     1.0
 */

const FollowUpNoteSchema = new Schema({
  isNoteCompleted: {
    type: Boolean,
    default: false
  },
  noteId: {
    type: String
  },
  followUpNote: {
    type: String
  },
  clientFollowUpDate: {
    type: Date
  },
  assignedTo: {
    type: String
  },
  eventId: {
    type: String
  },
  createdBy: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedBy: {
    type: String
  },
  updatedAt: {
    type: Date
  }
});

/**
 * The Corresponding  and File Upload Schema
 * @author      Ashim Karki
 * @version     1.0
 */

const correspondingNoteSchema = new Schema({
  correspondingTitle: {
    type: String
  },
  correspondingUploadPaths: [
    {
      type: String
    }
  ],
  otherNotes: [
    {
      otherNote: {
        type: String
      },
      createdBy: {
        type: String
      },
      createdAt: {
        type: Date
      },
      updatedBy: {
        type: String
      },
      updatedAt: {
        type: Date
      }
    }
  ],
  createdBy: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedBy: {
    type: String
  },
  updatedAt: {
    type: Date
  }
});

const TransactionDetailSchema = new Schema({
  propertyLotAddress: {
    type: String
  },
  upTo: {
    type: String
  },
  propertyType: {
    type: String
  },
  purchaseStatus: {
    type: String
  },
  ooInvst: {
    type: String
  },
  resultNotes: [
    {
      note: {
        type: String
      },
      createdAt: {
        type: Date
      },
      createdBy: {
        type: String
      },
      updatedBy: {
        type: String
      }
    }
  ],
  createdAt: {
    type: Date
  },
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  correspondingNote: [correspondingNoteSchema],
  referenceId: {
    type: Schema.Types.ObjectId,
    ref: 'ClientProfile'
  }
});

/**
 * the Client Profile schema
 * @author      Ashim Karki
 * @version     1.0
 */

const ClientProfileSchema = new Schema(
  {
    title: {
      type: String
    },
    fullName: {
      type: String
    },
    firstName: {
      type: String,
      required: true
    },
    middleName: {
      type: String
    },
    lastName: {
      type: String,
      required: true
    },
    phone: {
      type: String
    },
    mobile: {
      type: String,
      unique: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true
    },
    mailingStreetNo: {
      type: String
    },
    mailingStreetAddress: {
      type: String
    },
    mailingSuburb: {
      type: String
    },
    mailingState: {
      type: String
    },
    mailingZip: {
      type: String
    },
    mailingCountry: {
      type: String
    },
    residentialStreetNo: {
      type: String
    },
    residentialStreetAddress: {
      type: String
    },
    residentialSuburb: {
      type: String
    },
    residentialState: {
      type: String
    },
    residentialZip: {
      type: String
    },
    residentialCountry: {
      type: String
    },
    maritalStatus: {
      type: String
    },
    spouseTitle: {
      type: String
    },
    spouseFirstName: {
      type: String
    },
    spouseMiddleName: {
      type: String
    },
    spouseLastName: {
      type: String
    },
    spouseEmail: {
      type: String,
      lowercase: true
    },
    spouseNumber: {
      type: String
    },
    spouseLandline: {
      type: String
    },
    partnerTitle: {
      type: String
    },
    partnerFirstName: {
      type: String
    },
    partnerMiddleName: {
      type: String
    },
    partnerLastName: {
      type: String
    },
    partnerEmail: {
      type: String,
      lowercase: true
    },
    partnerNumber: {
      type: String
    },
    partnerLandline: {
      type: String
    },
    clientEntryDate: {
      type: Date
    },
    clientStatus: {
      type: String,
      enum: _.values(CLIENT_STATUS)
    },
    customerType: {
      type: String
    },
    entryDate: {
      type: Date,
      default: new Date()
    },

    clientFollowUpNumber: {
      type: Number
    },
    clientFollowUpPhone: {
      type: Number
    },
    processStatus: {
      type: String
    },
    clientFollowUpEmail: {
      type: String,
      lowercase: true
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    clientFollowUpNotes: [FollowUpNoteSchema],
    referrals: ClientReferralSchema,
    transactionDetails: [TransactionDetailSchema],
    otherDetails: [otherNewSchema]
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

ClientProfileSchema.plugin(mongoosePaginate);
module.exports = {
  ClientProfileSchema
};
