/**
 * Copyright (C) Tuki Logic
 */

/**
 * the User schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const imageSchema = new Schema({
  name: String,
  desc: String,
  img: {
    data: Buffer,
    contentType: String
  }
});

const EmailTemplateSchema = new Schema({
  subject: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  // To: EmailReceiver,
  to: {
    type: String
  },
  sendBy: {
    type: String
  },
  createBy: {
    type: String
  },
  createdAt: {
    type: Date
  },
  imageUpload: imageSchema
});

module.exports = {
  EmailTemplateSchema
};
