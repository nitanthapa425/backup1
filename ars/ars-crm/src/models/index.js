/**
 * Copyright (C) Tuki Logic
 */

/**
 * the mongoose db schema and init models
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const config = require('config');

const db = require('../datasource').getDb(config.db.url, config.db.poolSize);
const { UserSchema } = require('./user.model');
const { CategorySchema } = require('./Category');
const { EmailTemplateSchema } = require('./EmailTemplate');
const { CommentSchema } = require('./Comment');
// const { ClientProfileSchema } = require('./ClientProfile');
const { ClientProfileSchema } = require('./clientprofile.model');
const { TemplateSchema } = require('./Template');
const { GlobalSearchSchema } = require('./GlobalSearch');

module.exports = {
  db,
  User: db.model('User', UserSchema),
  ClientProfile: db.model('ClientProfile', ClientProfileSchema),
  Category: db.model('Category', CategorySchema),
  EmailTemplate: db.model('EmailTemplate', EmailTemplateSchema),
  Comment: db.model('Comment', CommentSchema),
  Template: db.model('Template', TemplateSchema),
  GlobalSearch: db.model('GlobalSearch', GlobalSearchSchema)
};
