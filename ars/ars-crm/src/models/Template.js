/**
 * Copyright (C) Tuki Logic
 */

/**
 * the User schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const TemplateSchema = new Schema(
  {
    imgUrl: {
      type: String
    },
    templateName: {
      type: String,
      required: true,
      unique: true
    },
    templateBody: {
      type: String
    },
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

module.exports = {
  TemplateSchema
};
