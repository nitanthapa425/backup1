/**
 * the User schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const CommentSchema = new Schema({
  fileId: {
    type: String
  },
  correspondingUploadPaths: [
    {
      type: String
    }
  ],
  createdBy: {
    type: String
  },
  createdAt: {
    type: Date
  },
  updatedBy: {
    type: String
  },
  updatedAt: {
    type: Date
  }
});

module.exports = { CommentSchema };
