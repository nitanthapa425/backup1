/**
 * Copyright (C) Tuki Logic
 */

/**
 * the User schema
 * @author      Arjun Subedi
 * @version     1.0
 */

const { Schema } = require('mongoose');

const EmailSchema = new Schema({
  email: {
    type: String,
    required: true
  }
});

const CategorySchema = new Schema(
  {
    categoryName: {
      type: String,
      required: true
    },
    categoryDescription: {
      type: String
    },
    emailList: [EmailSchema],
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    updatedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;
        return ret;
      }
    }
  }
);

module.exports = {
  CategorySchema
};
