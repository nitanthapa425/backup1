/**
 * Copyright (C) Tuki Logic
 */

/**
 * the User schema
 * @author      Ashim Karki
 * @version     1.0
 */

const { Schema } = require('mongoose');

const residentialAddress = new Schema({
  streetAddress: {
    type: String
  },
  state: {
    type: String
  },
  country: {
    type: String
  },
  zip: {
    type: String
  },
  street: {
    type: String
  },
  suburb: {
    type: String
  }
});

const emergencyResidentialAddress = new Schema({
  emergencyStreetAddress: {
    type: String
  },
  emergencyState: {
    type: String
  },
  emergencyCountry: {
    type: String
  },
  emergencyZip: {
    type: String
  },
  emergencyStreet: {
    type: String
  },
  emergencySuburb: {
    type: String
  }
});

const emergencyContactPerson = new Schema({
  emergencyContactPersonTitle: { type: String },
  emergencyContactPersonFirstName: { type: String },
  emergencyContactPersonMiddleName: { type: String },
  emergencyContactPersonLastName: { type: String },
  emergencyContactPersonMobileNo: { type: String },
  emergencyContactPersonPhone: { type: String },
  emergencyContactPersonEmail: { type: String },
  emergencyContactPersonRelation: { type: String }
});

const UserSchema = new Schema(
  {
    userName: { type: String },
    title: { type: String },
    firstName: { type: String, required: true },
    middleName: { type: String },
    lastName: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    passwordHash: { type: String },
    phone: { type: String },
    rolePosition: { type: String },
    maritalStatus: { type: String },
    status: { type: String },
    accessLevel: { type: String },
    isActive: { type: Boolean, default: true },
    verificationToken: { type: String },
    accessToken: { type: String },
    verified: { type: Boolean },
    forgotPasswordToken: { type: String },
    lastLoginAt: { type: Date },
    residential: residentialAddress,
    contactPerson: emergencyContactPerson,
    emergencyResidential: emergencyResidentialAddress
  },
  {
    timestamps: true,
    toJSON: {
      transform(doc, ret) {
        if (ret._id) {
          ret.id = String(ret._id);
          delete ret._id;
        }
        delete ret.__v;

        // Keep only necessary details for GET requests
        delete ret.passwordHash;
        delete ret.verificationToken;
        delete ret.forgotPasswordToken;
        delete ret.accessToken;
        return ret;
      }
    }
  }
);

module.exports = {
  UserSchema
};
