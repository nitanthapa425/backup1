/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const CategoryService = require('../services/CategoryService');

/**
 * post Category information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createCategory(req, res) {
  res.json(await CategoryService.createCategory(req.user.id, req.body));
}

/**
 * get Category information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getCategory(req, res) {
  res.json(await CategoryService.getCategory());
}

/**
 *  Updates Category
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateCategory(req, res) {
  res.json(await CategoryService.updateCategory(req.user.id, req.params.id, req.body));
}

/**
 *  Deletes Category
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteCategory(req, res) {
  res.json(await CategoryService.deleteCategory(req.params.id));
}

/**
 *  Send  Emails to Clients
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function sendEmailsToClients(req, res) {
  res.json(await CategoryService.sendEmailsToClients(req.user.id, req.body));
}

/**
 *  Send  to Clients
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createTemplate(req, res) {
  res.json(await CategoryService.createTemplate(req.user.id, req.body));
}

/**
 *  Send  Emails to Clients
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getTemplate(req, res) {
  res.json(await CategoryService.getTemplate());
}
/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
 async function uploadImage(req, res) {
  const entity =
    req.files && req.files.length
      ? {
          correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : []
        }
      : [];
  res.json(await CategoryService.uploadImage(req.user.id, req.params.id, entity));
}
module.exports = {
  createCategory,
  getCategory,
  updateCategory,
  deleteCategory,
  createTemplate,
  getTemplate,
  sendEmailsToClients,
  uploadImage

};
