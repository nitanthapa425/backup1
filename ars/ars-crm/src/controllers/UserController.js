/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const UserService = require('../services/UserService');

/**
 * handles the change password
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function changePassword(req, res) {
  res.json(await UserService.changePassword(req.user.id, req.body));
}

/**
 * handles the update password
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updatePassword(req, res) {
  res.json(await UserService.updatePassword(req.user.id, req.body));
}

/**
 * handles the update password
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updatePasswordByAdmin(req, res) {
  res.json(await UserService.changePassword(req.params.id, req.body, req.user.id));
}

/**
 * handles the update profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateProfile(req, res) {
  res.json(await UserService.updateProfile(req.user.id, req.body));
}

/**
 * handles `get` user profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function userProfile(req, res) {
  res.json(await UserService.userProfile(req.user.id));
}
/**
 * handles `search` users profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function searchUserProfiles(req, res) {
  res.json(await UserService.searchUserProfiles(req.query));
}

/**
 * handles `get` user profile By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

async function getUserProfileById(req, res) {
  res.json(await UserService.getUserProfileById(req.params.id));
}
/**
 * handles `update` users profile By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

async function updateUserProfileById(req, res) {
  res.json(await UserService.updateUserProfileById(req.user.id, req.params.id, req.body));
}
/**
 * handles `delete` users profile By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

async function deleteUserProfileById(req, res) {
  res.json(await UserService.deleteUserProfileById(req.user.id, req.params.id));
}

/**
 * handles `revert deleted  ` users profile By Id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */

async function revertDeleteUserProfileById(req, res) {
  res.json(await UserService.revertDeleteUserProfileById(req.user.id, req.params.id));
}

/**
 * get user's emergency contact  person information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getEmergencyContactPerson(req, res) {
  res.json(await UserService.getEmergencyContactPerson(req.params.id));
}

/**
 * get user's client assign to me
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientAssignToMe(req, res) {
  res.json(await UserService.getClientAssignToMe(req.params.id));
}

/**
 * Add Profile Image in User Profile Page
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function uploadProfilePictureController(req, res) {
  const entity = {
    imageUploadPath: req.files && req.files.length > 0 ? req.files[0].path : ''
  };

  res.json(await UserService.uploadProfilePicture(req.user.id, entity));
}
module.exports = {
  changePassword,
  updatePassword,
  updatePasswordByAdmin,
  updateProfile,
  userProfile,
  getUserProfileById,
  updateUserProfileById,
  deleteUserProfileById,
  searchUserProfiles,
  getEmergencyContactPerson,
  getClientAssignToMe,
  revertDeleteUserProfileById,
  uploadProfilePictureController
};
