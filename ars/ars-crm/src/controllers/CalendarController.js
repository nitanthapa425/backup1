const CalendarService = require('../services/CalendarService');

async function getEvents(req, res) {
  // Load client secrets from a local file.
  res.json(await CalendarService.getEvents(req.oAuth2Client));
}

async function createEvent(req, res) {
  // Load client secrets from a local file.
  const eventData = {
    summary: req.body.summary,
    location: req.body.location,
    description: req.body.description,
    start: {
      dateTime: req.body.start.dateTime,
      timeZone: req.body.start.timeZone
    },
    end: {
      dateTime: req.body.end.dateTime,
      timeZone: req.body.end.timeZone
    },
    attendees: req.body.attendees
  };
  res.json(await CalendarService.createEvent(eventData, req.oAuth2Client));
}

async function setToken(req, res) {
  const { code, callback } = req.query;
  res.json({ code, callback });
}

async function deleteCalendar(req, res) {
  res.json(await CalendarService.deleteCalendarService(req.params.id, req.oAuth2Client));
}

async function updateCalendar(req, res) {
  const eventData = {
    summary: req.body.summary,
    location: req.body.location,
    description: req.body.description,
    start: {
      dateTime: req.body.start ? req.body.start.dateTime : '',
      timeZone: req.body.start ? req.body.start.timeZone : ''
    },
    end: {
      dateTime: req.body.end ? req.body.end.dateTime : '',
      timeZone: req.body.end ? req.body.end.timeZone : ''
    },
    attendees: req.body.attendees
  };

  res.json(await CalendarService.updateCalendarService(req.user.id, req.params.id, eventData, req.oAuth2Client));
}

module.exports = {
  getEvents,
  createEvent,
  setToken,
  deleteCalendar,
  updateCalendar
};
