const httpStatus = require('http-status');

const { clientProfileService } = require('../services');
const catchAsync = require('../utils/catchAsync');

/**
 * Create ClientProfile
 */
const createClientProfile = catchAsync(async (req, res) => {
  const _clientProfile = req.body;
  const clientProfile = await clientProfileService.createClientProfile(_clientProfile);
  res.status(httpStatus.CREATED).send(clientProfile);
});

/**
 * Search for ClientProfile on basis of params. Can be used to get any seller by using search filter
 */
const getClientProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await clientProfileService.getClientProfileList(search, options));
});

/**
 * Get Specific ClientProfile by Id
 */
const getClientProfile = catchAsync(async (req, res) => {
  const { clientProfileId } = req.params;
  res.send(await clientProfileService.getClientProfile(clientProfileId));
});

/**
 * Delete ClientProfile
 */
const deleteClientProfile = catchAsync(async (req, res) => {
  const { clientProfileId } = req.params;
  res.send(await clientProfileService.deleteClientProfile(clientProfileId));
});

/**
 * Update ClientProfile
 */
const updateClientProfile = catchAsync(async (req, res) => {
  const { clientProfileId } = req.params;
  const clientProfile = req.body;

  await clientProfileService.updateClientProfile(clientProfileId, clientProfile);

  // Fetch Updated record and send back to Client
  res.status(httpStatus.OK).send(await clientProfileService.getClientProfile(clientProfileId));
});

// update Others detail in client Page
const updateClientOtherDetails = catchAsync(async (req, res) => {
  const { clientProfileId, otherDetailId } = req.params;
  const { notes } = req.body;
  // notes.updatedBy = userId;

  if (notes) {
    const _ = await clientProfileService.appendOtherNotes(clientProfileId, otherDetailId, notes);
    return res.json(_);
  }
  return res.json();
});

// delete Others detail in client Page
const deleteClientOtherDetails = catchAsync(async (req, res) => {
  const { clientProfileId, otherDetailId } = req.params;

  const _ = await clientProfileService.deleteClientOtherDetails(clientProfileId, otherDetailId);

  return res.json(_);
});

// delete Others detail in client Page
const deleteClientTransactionDetails = catchAsync(async (req, res) => {
  const { clientProfileId, transactionId } = req.params;

  const _ = await clientProfileService.deleteTransactionDetails(clientProfileId, transactionId);

  return res.json(_);
});
// update transaction details on Client Profile Page
const updateClientTransactionDetails = catchAsync(async (req, res) => {
  const { userId, clientProfileId, transactionId } = req.params;
  const { resultNotes } = req.body;
  // resultNotes.updatedBy = userId;
  // resultNotes.createdAt = new Date();
  if (resultNotes) {
    const _ = await clientProfileService.appendTransactionNotes(userId, clientProfileId, transactionId, resultNotes);
    return res.json(_);
  }
  return res.json();
});

/**
 * Insert Many Categories once
 */

const insertManyClientProfile = catchAsync(async (req, res) => {
  const clientProfileList = req.body;
  try {
    await clientProfileService.insertManyCategories(clientProfileList);
  } catch (ignored) {
    // Duplicate error occurred
  }
  res.status(httpStatus.CREATED).send('OK');
});

// delete Others detail in client Page
const deleteOtherDetailsFile = catchAsync(async (req, res) => {
  const { clientProfileId, otherDetailId, fileId } = req.params;

  const _ = await clientProfileService.deleteOtherDetailsFileById(clientProfileId, otherDetailId, fileId);

  return res.json(_);
});

// delete Others detail in client Page
const deleteOtherDetailsNote = catchAsync(async (req, res) => {
  const { clientProfileId, otherDetailId, noteId } = req.params;

  const _ = await clientProfileService.deleteOtherDetailsNoteById(clientProfileId, otherDetailId, noteId);

  return res.json(_);
});

// delete Others detail in client Page
const deleteTransactionDetailsNote = catchAsync(async (req, res) => {
  const { clientProfileId, transactionId, noteId } = req.params;

  const _ = await clientProfileService.deleteTransactionDetailsNoteById(clientProfileId, transactionId, noteId);

  return res.json(_);
});
// update other details note on Client Profile Page
const updateClientDetailsNote = catchAsync(async (req, res) => {
  const { clientProfileId, otherDetailId, noteId } = req.params;
  const { notes } = req.body;
  // resultNotes.updatedBy = userId;
  // resultNotes.createdAt = new Date();
  if (notes) {
    const _ = await clientProfileService.updateOtherNotes(clientProfileId, otherDetailId, noteId, notes);

    return res.json(_);
  }
  return res.json();
});

// update transaction details note on Client Profile Page
const updateTransactionDetailsNote = catchAsync(async (req, res) => {
  const { clientProfileId, transactionId, noteId } = req.params;
  const { resultNotes } = req.body;
  // resultNotes.updatedBy = userId;
  // resultNotes.createdAt = new Date();
  if (resultNotes) {
    const _ = await clientProfileService.updateTransactionNotes(clientProfileId, transactionId, noteId, resultNotes);

    return res.json(_);
  }
  return res.json();
});

module.exports = {
  createClientProfile,
  getClientProfileList,
  getClientProfile,
  deleteClientProfile,
  updateClientProfile,
  insertManyClientProfile,
  updateClientOtherDetails,
  updateClientTransactionDetails,
  deleteClientOtherDetails,
  deleteOtherDetailsFile,
  deleteOtherDetailsNote,
  deleteClientTransactionDetails,
  deleteTransactionDetailsNote,
  updateClientDetailsNote,
  updateTransactionDetailsNote
};
