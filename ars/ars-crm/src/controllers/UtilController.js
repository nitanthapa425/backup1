/**
 * Copyright (C) Tuki Logic
 */

/**
 * Util Controller
 *
 * @author      Ashim Karki
 * @version     1.0
 */

const UtilityService = require('../services/UtilityService');

/**
 * handles get cities method
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getCities(req, res) {
  res.json(await UtilityService.getCities(req.query.state));
}

/**
 * handles get states method
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getStates(req, res) {
  res.json(await UtilityService.getStates());
}

module.exports = {
  getCities,
  getStates
};
