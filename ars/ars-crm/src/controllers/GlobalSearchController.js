const GlobalSearchService = require('../services/GlobalSearchService');

/**
 * post global search information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createGlobalSearchController(req, res) {
  res.json(await GlobalSearchService.createGlobalSerchService(req.user.id, req.body));
}

/**
 * get Global Search
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getGlobalSearchController(req, res) {
  res.json(await GlobalSearchService.getGlobalSearchService());
}

module.exports = {
  createGlobalSearchController,
  getGlobalSearchController
};
