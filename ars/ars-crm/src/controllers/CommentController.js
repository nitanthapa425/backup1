/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const CommentService = require('../services/CommentService');

/**
 * post Comment information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createComment(req, res) {
  res.json(await CommentService.createComment(req.user.id, req.body));
}

/**
 * get Comment information
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getComment(req, res) {
  res.json(await CommentService.getComment());
}

/**
 *  Updates Comment
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateCommentById(req, res) {
  res.json(await CommentService.updateCommentById(req.user.id, req.body));
}

/**
 *  Deletes Comment
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteCommentById(req, res) {
  res.json(await CommentService.deleteComment(req.user.id, req.params.id));
}

module.exports = {
  createComment,
  getComment,
  updateCommentById,
  deleteCommentById
};
