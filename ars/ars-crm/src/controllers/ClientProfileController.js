/**
 * Copyright (C) Tuki Logic
 */

/**
 * Client Profile Controller
 *
 * @author      Ashim Karki
 * @version     1.0
 */

// const { v4: uuidv4 } = require('uuid');
const httpStatus = require('http-status');
const ClientProfileService = require('../services/ClientProfileService');
// const { v4: uuidv4 } = require('uuid');
/**
 * handles search client profiles
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function searchClientProfiles(req, res) {
  res.json(await ClientProfileService.searchClientProfiles(req.query));
}

/**
 * handles the create client profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfile(req, res) {
  // const fileId = uuidv4();
  const otherDetail = JSON.parse(req.body.otherDetails);

  const transactionDetail = JSON.parse(req.body.transactionDetails);

  const filtertedTransaction = transactionDetail.filter((res2) => res2.propertyLotAddress !== '');

  const entity = {
    ...req.body,
    referrals: req.body.referrals ? JSON.parse(req.body.referrals) : {},
    transactionDetails:
      req.body.transactionDetails && req.body.transactionDetails.length > 0 ? filtertedTransaction : [],
    clientFollowUpNotes:
      req.body.clientFollowUpNotes && req.body.clientFollowUpNotes.length > 0
        ? JSON.parse(req.body.clientFollowUpNotes)
        : [],
    otherDetails: [
      {
        correspondingOtherPath: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
        newOtherNotes:
          otherDetail[0].newOtherNotes[0].newOtherNote !== ''
            ? [
                {
                  // newOtherNote: otherDetail,
                  newOtherNote: otherDetail[0].newOtherNotes[0].newOtherNote,
                  createdBy: req.user.id,
                  createdAt: new Date()
                }
              ]
            : [],
        createdBy: req.user.id,
        createdAt: new Date()
      }
    ]
    // correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : []
  };

  res.json(await ClientProfileService.createClientProfile(req.user.id, entity, req.body, req.oAuth2Client));
}

/**
 * get client profile by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientProfile(req, res) {
  res.json(await ClientProfileService.getClientProfile(req.params.id));
}

/**
 * delete client profile by id
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteClientProfile(req, res) {
  res.json(await ClientProfileService.deleteClientProfile(req.params.id));
}

/**
 * update client profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateClientProfile(req, res) {
  const entity = {
    ...req.body,
    referrals: req.body.referrals ? JSON.parse(req.body.referrals) : {},
    transactionDetails:
      req.body.transactionDetails && req.body.transactionDetails.length > 0
        ? JSON.parse(req.body.transactionDetails)
        : [],
    clientFollowUpNotes:
      req.body.clientFollowUpNotes && req.body.clientFollowUpNotes.length > 0
        ? JSON.parse(req.body.clientFollowUpNotes)
        : []
  };
  res.json(await ClientProfileService.updateClientProfile(req.user.id, req.params.id, entity));
}

/**
 * get client profile notes
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientProfileFollowUpNotes(req, res) {
  res.json(await ClientProfileService.getClientProfileFollowUpNotes(req.params.id));
}

/**
 * get client profile note by ID
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientProfileFollowUpNoteById(req, res) {
  res.json(await ClientProfileService.getClientProfileFollowUpNoteById(req.params.id, req.params.noteId));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfileFollowUpNote(req, res) {
  res.json(
    await ClientProfileService.createClientProfileFollowUpNote(req.user.id, req.params.id, req.body, req.oAuth2Client)
  );
}

/**
 * update client profile note by ID
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateClientProfileFollowUpNoteById(req, res) {
  res.json(
    await ClientProfileService.updateClientProfileFollowUpNoteById(
      req.user.id,
      req.params.id,
      req.params.noteId,
      req.body,
      req.oAuth2Client
    )
  );
}

/**
 * delete client profile note by ID
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteClientProfileFollowUpNoteById(req, res) {
  res.json(
    await ClientProfileService.deleteClientProfileFollowUpNoteById(req.params.id, req.params.noteId, req.oAuth2Client)
  );
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientProfileOtherDetailsById(req, res) {
  res.json(await ClientProfileService.getClientProfileOtherDetailsById(req.params.id, req.params.otherId));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfileOtherDetails(req, res) {
  // const notes = JSON.parse(req.body.notes).filter((x) => x);
  // const entity =
  //   req.files && req.files.length
  //     ? {
  //         correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
  //         ...(notes.length ? { notes } : {})
  //       }
  //     : [];
  res.json(await ClientProfileService.createClientProfileOtherDetails(req.user.id, req.params.id, req.body));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateClientProfileOtherDetails(req, res) {
  res.json(await ClientProfileService.updateClientProfileOtherDetails(req.user.id, req.params.id, req.body));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteClientProfileOtherDetails(req, res) {
  res.json(
    await ClientProfileService.deleteClientProfileOtherDetails(
      req.params.id,
      req.params.transactionId,
      req.params.otherId
    )
  );
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfileTransactionDetails(req, res) {
  const value = JSON.parse(req.body.correspondingNote);
  const entity = {
    ...req.body,
    propertyLotAddress: JSON.parse(req.body.propertyLotAddress),
    upTo: JSON.parse(req.body.upTo),
    ooInvst: JSON.parse(req.body.ooInvst),
    purchaseStatus: JSON.parse(req.body.purchaseStatus),
    propertyType: JSON.parse(req.body.propertyType),
    resultNotes: JSON.parse(req.body.resultNotes),
    correspondingNote: [
      {
        correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
        //  ...req.body.correspondingNote
        otherNotes: [
          {
            createdBy: req.user.id,
            createdAt: new Date(),
            otherNote: typeof value[0].otherNotes[0] !== 'undefined' ? value[0].otherNotes[0].otherNote : ''
          }
        ],
        correspondingTitle: typeof value[0] !== 'undefined' && value[0] ? value[0].correspondingTitle : ''
      }
    ]
  };

  res.json(await ClientProfileService.createClientProfileTransactionDetailsService(req.user.id, req.params.id, entity));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteClientProfileTransactionDetails(req, res) {
  res.json(await ClientProfileService.deleteClientProfileTransactionDetails(req.params.id, req.params.transactionId));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function addTransactionNoteController(req, res) {
  res.json(
    await ClientProfileService.addTransactionNoteService(req.user.id, req.params.id, req.params.transactionId, req.body)
  );
}
// Delete transaction Note
async function deleteTransactionNoteController(req, res) {
  res.json(
    await ClientProfileService.deleteTransactionNoteService(req.params.id, req.params.transactionId, req.params.noteId)
  );
}

// Update Transaction Note
async function updateTransactionNoteController(req, res) {
  res.json(
    await ClientProfileService.updateTransactionNoteService(
      req.user.id,
      req.params.id,
      req.params.transactionId,
      req.params.noteId,
      req.body
    )
  );
}

async function addCorrespondingInTransactionController(req, res) {
  const value = JSON.parse(req.body.otherNotes);

  const entity = {
    correspondingNote: [
      {
        correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
        //  ...req.body.correspondingNote
        otherNotes: [
          {
            createdBy: req.user.id,
            createdAt: new Date(),
            otherNote: value[0].otherNote
          }
        ],
        correspondingTitle: req.body.correspondingTitle ? req.body.correspondingTitle : ''
      }
    ]
  };

  res.json(
    await ClientProfileService.addCorrespondingInTransactionService(
      req.user.id,
      req.params.id,
      req.params.transactionId,
      entity
    )
  );

  // correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : []

  // res.json(
  //   await ClientProfileService.addCorrespondingInTransactionService(
  //     req.user.id,
  //     req.params.id,
  //     req.params.transactionId,
  //     req.body
  //   )
  // );
}

/**
 * handles search client types
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getTotalClientsType(req, res) {
  res.json(await ClientProfileService.getTotalClientsType(req.query.date));
}

/**
 * handles search client Transaction Details
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getTransactionDetails(req, res) {
  res.json(await ClientProfileService.getTransactionDetails(req.query));
}

/**
 * handles search client Follow Up Notes types
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientFollowupNote(req, res) {
  res.json(await ClientProfileService.getClientFollowupNote(req.query));
}

/**
 * handles search client profiles
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientsDetailsToReferral(req, res) {
  res.json(await ClientProfileService.getClientsDetailsToReferral(req.user.id));
}
/**
 * handles search client profiles
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientAssignToMe(req, res) {
  res.json(await ClientProfileService.getClientAssignToMe(req.user.id));
}

async function updateClientProfileAddFile(req, res) {
  // const notes = JSON.parse(req.body.notes).filter((x) => x);
  const entity =
    req.files && req.files.length
      ? {
          correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : []
        }
      : [];
  res.json(
    await ClientProfileService.updateClientProfileAddFile(req.user.id, req.params.id, req.params.otherDetailId, entity)
  );
}
/**
 * handles the create client profile
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfileWithoutValidation(req, res) {
  const otherDetail = JSON.parse(req.body.otherDetails);

  const entity = {
    ...req.body,
    referrals: req.body.referrals ? JSON.parse(req.body.referrals) : {},
    transactionDetails:
      req.body.transactionDetails && req.body.transactionDetails.length > 0
        ? JSON.parse(req.body.transactionDetails)
        : [],
    clientFollowUpNotes:
      req.body.clientFollowUpNotes && req.body.clientFollowUpNotes.length > 0
        ? JSON.parse(req.body.clientFollowUpNotes)
        : [],
    otherDetails: [
      {
        correspondingOtherPath: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
        newOtherNotes: [
          {
            newOtherNote: otherDetail[0].newOtherNotes[0].newOtherNote,
            createdBy: req.user.id,
            createdAt: new Date()
          }
        ],
        createdBy: req.user.id,
        createdAt: new Date()
      }
    ]
    // correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
  };
  res.json(
    await ClientProfileService.createClientProfileWithoutValidation(req.user.id, entity, req.body, req.oAuth2Client)
  );
}

/**
 * Add Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function addCorrespondenceNoteController(req, res) {
  res.json(
    await ClientProfileService.addCorrespondenceNoteService(
      req.user.id,
      req.params.id,
      req.params.transactionId,
      req.params.correspondeceId,
      req.body
    )
  );
}
/**
 * Update Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateCorrespondenceNoteController(req, res) {
  res.json(
    await ClientProfileService.updateCorrespondenceNoteService(
      req.user.id,
      req.params.id,
      req.params.transactionId,
      req.params.correspondeceId,
      req.params.noteId,
      req.body
    )
  );
}

/**
 * Delete Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteCorrespondingNoteController(req, res) {
  res.json(
    await ClientProfileService.deleteCorrespondingNoteService(
      req.params.id,
      req.params.transactionId,
      req.params.correspondeceId,
      req.params.noteId
    )
  );
}

/**
 * Add File in Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function addCorrespondenceFileController(req, res) {
  const entity = {
    ...req.body,

    correspondingUploadPaths: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : []
  };

  res.json(
    await ClientProfileService.addCorrespondenceFileService(
      req.params.id,
      req.params.transactionId,
      req.params.correspondeceId,
      entity
    )
  );
}
/**
 * Add File in Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getTransactionDetailAndCorrespondenceController(req, res) {
  res.json(await ClientProfileService.getTransactionDetailAndCorrespondence(req.params.id));
}

/**
 * Delete Corresponding File
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteCorrespondingFileController(req, res) {
  res.json(
    await ClientProfileService.deleteCorrespondingFileService(
      req.params.id,
      req.params.transactionId,
      req.params.correspondeceId,
      req.params.fileId
    )
  );
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createClientProfileOtherDetailController(req, res) {
  const value = JSON.parse(req.body.newOtherNotes);
  const entity = {
    // ...req.body,
    // resultNotes: JSON.parse(req.body.newOtherNotes),
    otherDetails: [
      {
        correspondingOtherPath: req.files && req.files.length > 0 ? [...req.files.map((file) => file.path)] : [],
        //  ...req.body.correspondingNote
        newOtherNotes:
          value[0].newOtherNote !== ''
            ? [
                {
                  createdBy: req.user.id,
                  createdAt: new Date(),
                  newOtherNote: value[0].newOtherNote
                }
              ]
            : []
      }
    ]
  };

  res.json(await ClientProfileService.createClientProfileOtherDetailService(req.user.id, req.params.id, entity));
}

/**
 * create client profile note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteOtherDetailsController(req, res) {
  res.json(await ClientProfileService.deleteOtherDetailsService(req.params.id, req.params.otherId));
}

/**
 * Add File in Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function addOtherFileController(req, res) {
  const entity = {
    correspondingOtherPath:
      typeof req.files !== 'undefined' && req.files && req.files.length > 0
        ? [...req.files.map((file) => file.path)]
        : []
  };
  res.json(await ClientProfileService.addOtherFileService(req.params.id, req.params.otherId, entity));
}

/**
 * Add Corresponding Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function addOtherNoteController(req, res) {
  res.json(await ClientProfileService.addOtherNoteService(req.user.id, req.params.id, req.params.otherId, req.body));
}

/**
 * Delete New Other Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteOtherNoteController(req, res) {
  res.json(await ClientProfileService.deleteOtherNoteService(req.params.id, req.params.otherId, req.params.noteId));
}

/**
 * Delete New Other Note
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function updateNewOtherNoteController(req, res) {
  res.json(
    await ClientProfileService.updateNewOtherNoteService(
      req.user.id,
      req.params.id,
      req.params.otherId,
      req.params.noteId,
      req.body
    )
  );
}

/**
 * Delete Other File
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function deleteOtherFileController(req, res) {
  res.json(await ClientProfileService.deleteOtherFileService(req.params.id, req.params.otherId, req.params.fileId));
}

/**
 * Complete FolowUp Note of Client
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function completeFollowUpNoteController(req, res) {
  res.json(await ClientProfileService.completeFollowUpNote(req.params.id, req.params.completedId));
}
/**
 * Incomplete FolowUp Note of Client
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function incompleteFollowUpNoteController(req, res) {
  res.json(await ClientProfileService.incompleteFollowUpNote(req.params.id, req.params.completedId));
}

/**
 * get Client Profile Other Details
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getClientProfileOtherDetails(req, res) {
  res.json(await ClientProfileService.getClientProfileOtherDetails(req.params.id));
}
module.exports = {
  createClientProfile,
  searchClientProfiles,
  getClientProfile,
  deleteClientProfile,
  updateClientProfile,
  getClientProfileFollowUpNotes,
  getClientProfileFollowUpNoteById,
  createClientProfileFollowUpNote,
  updateClientProfileFollowUpNoteById,
  deleteClientProfileFollowUpNoteById,
  getClientProfileOtherDetailsById,
  createClientProfileOtherDetails,
  updateClientProfileOtherDetails,
  deleteClientProfileOtherDetails,
  getTotalClientsType,
  getTransactionDetails,
  createClientProfileTransactionDetails,
  deleteClientProfileTransactionDetails,
  getClientFollowupNote,
  getClientsDetailsToReferral,
  getClientAssignToMe,
  updateClientProfileAddFile,
  createClientProfileWithoutValidation,
  addTransactionNoteController,
  deleteTransactionNoteController,
  updateTransactionNoteController,
  addCorrespondingInTransactionController,
  addCorrespondenceNoteController,
  updateCorrespondenceNoteController,
  deleteCorrespondingNoteController,
  addCorrespondenceFileController,
  getTransactionDetailAndCorrespondenceController,
  deleteCorrespondingFileController,
  createClientProfileOtherDetailController,
  deleteOtherDetailsController,
  addOtherFileController,
  addOtherNoteController,
  deleteOtherNoteController,
  updateNewOtherNoteController,
  deleteOtherFileController,
  completeFollowUpNoteController,
  incompleteFollowUpNoteController,
  getClientProfileOtherDetails
};
