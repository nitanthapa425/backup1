/**
 * Copyright (C) Tuki Logic
 */

/**
 * User Controller
 *
 * @author      Arjun Subedi
 * @version     1.0
 */

const { getEvents } = require('../common/GoogleCalendar');

/**
 * get Event  from google calendar
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function getAllEvents(req, res) {
  getEvents((events) => res.json(events));
}

/**
 * post Event  from google calendar
 * @param {Object} req the http request
 * @param {Object} res the http response
 */
async function createEvents(req, res) {
  getEvents((events) => res.json(events));
}

module.exports = {
  getAllEvents,
  createEvents
};
