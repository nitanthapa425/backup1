// const httpStatus = require('http-status');
const { UserService } = require('../services');
const catchAsync = require('../utils/catchAsync');

/**
 * Search for UserProfile on basis of params. Can be used to get any seller by using search filter
 */
const getUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getUserProfileList(search, options));
});

const getDeletedUserProfileList = catchAsync(async (req, res) => {
  const { options, search } = res.pagination;
  res.send(await UserService.getDeletedUserProfileList(search, options));
});

module.exports = {
  getUserProfileList,
  getDeletedUserProfileList

};
