### Backend API (Rename all the necessary fields to meet your api requirements)

## Install software

- node 14.17.1
- npm 6.x
- mongodb 4.2.5 https://www.mongodb.com/download-center?ct=false#community
- Postman for Verification

### All the config variables can be provided through .env file

## Local deployment

first, you need startup mongodb , modify *db.*url in _config/default.js_ for your db url(you don't need to modify it if you startup mongodb with default config in local).

1. open terminal to project root dir ,Install node dependencies using `npm install`.
2. check code with `npm run lint`.
3. run `npm run test` to run test cases.
4. create static test data run `npm run addData`, **it will drop all tables and create tables.**
5. run server `npm run start`.
6. enable it https://myaccount.google.com/lesssecureapps , and open config/default.js , enter your gmail user and pass at email.auth.

## Email Configuration

- set email configuration in `config/default.js`
- host: the email smtp host
- port: port
- user: email address
- pass: password

## Postman and Swagger

- Please go to `docs` folder, the postman collection and swagger definition is there.

## Creating Path for Files & Correspondence Upload

- Please create a folder `uploads/client-profile-docs` on the main folder which is ars-crm

## Password Authentication

- nitan1@email.com Password@12345
