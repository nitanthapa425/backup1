export const getFileName = (filePath) => {
  if (filePath?.includes('{{') && filePath?.includes('}}')) {
    try {
      let _fileName = filePath?.slice(filePath?.indexOf('}}') + 2);
      return _fileName?.slice(0, _fileName?.length);
    } catch (e) {
      return false;
    }
  }
  return false;
};
