import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

export const downloadPdfFromHtmlElement = async (htmlSelector) => {
  html2canvas(document.querySelector(htmlSelector)).then((canvas) => {
    // document.body.appendChild(canvas);
    const imgData = canvas.toDataURL('image/png');
    const pdf = new jsPDF('p', 'mm', 'a4');
    let width = Math.trunc(pdf.internal.pageSize.getWidth());
    let height = Math.trunc(pdf.internal.pageSize.getHeight()) - 70;
    pdf.addImage(imgData, 'PNG', 0, 0, width, height);
    pdf.save(`Ganesh_dashboard_${new Date(Date.now()).toLocaleDateString()}.pdf`);
  });
};

export const getClassByTime = (_date) => {
  if (!_date) return ' hidden';
  const date1 = new Date(_date);
  const date2 = new Date();
  const diffTime = date1 - date2;
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  if (diffDays < 7) {
    return 'status-urgent';
  } else if (diffDays < 15) {
    return 'status-medium';
  } else {
    return 'status-low';
  }
};
