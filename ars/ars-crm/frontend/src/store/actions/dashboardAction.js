import DashboardService from '../../services/dashboard-service';
import {
  GET_CLIENT_TOTAL_COUNT_FAILED,
  GET_CLIENT_TOTAL_COUNT_PROCESSING,
  GET_CLIENT_TOTAL_COUNT_SUCCESS,
  GET_TOTAL_FOLLOWUP_NOTE_PROCESSING,
  GET_TOTAL_FOLLOWUP_NOTE_SUCCESS,
  GET_FOLLOWUP_NOTE_PROCESSING,
  GET_FOLLOWUP_NOTE_SUCCESS,
  GET_FOLLOWUP_NOTE_FAILED
} from '../types/types';

export const clientListsLengthAction = (date) => (dispatch) => {
  dispatch({
    type: GET_CLIENT_TOTAL_COUNT_PROCESSING
  });

  return DashboardService.showClientListsLength(date)
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: GET_CLIENT_TOTAL_COUNT_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: GET_CLIENT_TOTAL_COUNT_FAILED,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_CLIENT_TOTAL_COUNT_FAILED,
        payload: 'Failed'
      });
    });
};

export const getTotalFollowAction = () => (dispatch) => {
  dispatch({
    type: GET_TOTAL_FOLLOWUP_NOTE_PROCESSING
  });

  return DashboardService.getTotalFollowup()
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: GET_TOTAL_FOLLOWUP_NOTE_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: GET_TOTAL_FOLLOWUP_NOTE_SUCCESS,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_TOTAL_FOLLOWUP_NOTE_SUCCESS,
        payload: 'Failed'
      });
    });
};

//get followupnotes
export const followupNOteListAction = () => (dispatch) => {
  dispatch({
    type: GET_FOLLOWUP_NOTE_PROCESSING
  });

  return DashboardService.showfollowupNOteListAction()
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: GET_FOLLOWUP_NOTE_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: GET_FOLLOWUP_NOTE_SUCCESS,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_FOLLOWUP_NOTE_FAILED,
        payload: 'Failed'
      });
    });
};
