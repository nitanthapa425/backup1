import EventService from '../../services/event-service';
import {
  ADD_EVENT_FAILED,
  ADD_EVENT_PROCESSING,
  ADD_EVENT_SUCCESS,
  DELETE_EVENT_FAILED,
  DELETE_EVENT_PROCESSING,
  DELETE_EVENT_RESET,
  DELETE_EVENT_SUCCESS,
  GET_EVENT_LIST_FAILED,
  GET_EVENT_LIST_PROCESSING,
  GET_EVENT_LIST_SUCCESS,
  GET_SELF_EVENT_LIST_FAILED,
  GET_SELF_EVENT_LIST_PROCESSING,
  GET_SELF_EVENT_LIST_SUCCESS,
  UPDATE_EVENT_PROCESSING,
  UPDATE_EVENT_SUCCESS,
  UPDATE_EVENT_FAILED,
  UPDATE_EVENT_RESET
} from '../types/types';

export const getEventListAction = () => (dispatch) => {
  dispatch({
    type: GET_EVENT_LIST_PROCESSING
  });

  return EventService.getEventListService()
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: GET_EVENT_LIST_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: GET_EVENT_LIST_FAILED,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_EVENT_LIST_FAILED,
        payload: 'Failed'
      });
    })

};

export const getSelfEventListAction = (emailValue) => (dispatch) => {
  dispatch({
    type: GET_SELF_EVENT_LIST_PROCESSING
  });

  return EventService.getEventListService()
    .then(({ status, data }) => {
      if (status === 200) {
        const filterData = data.events.filter(
          (res) => typeof res.attendees !== 'undefined' && res.attendees.length !== 0
        );

        const filterAttendees =
          filterData &&
          filterData
            .map((resValue) => {
              resValue.attendees = resValue.attendees.filter((resAttendees) => resAttendees.email === emailValue);
              return resValue;
            })
            .filter((resFinal) => typeof resFinal.attendees !== 'undefined' && resFinal.attendees.length !== 0);

        // const finalFilteredData =
        //   filterAttendees &&
        //   filterAttendees.filter(
        //     (resFinal) => typeof resFinal.attendees !== 'undefined' && resFinal.attendees.length !== 0
        //   );

        dispatch({
          type: GET_SELF_EVENT_LIST_SUCCESS,
          payload: filterAttendees
        });
      } else {
        dispatch({
          type: GET_SELF_EVENT_LIST_FAILED,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: GET_SELF_EVENT_LIST_FAILED,
        payload: 'Failed'
      });
    });
};

export const addEventAction = (eventDetail, setEmailArray) => (dispatch) => {
  dispatch({
    type: ADD_EVENT_PROCESSING
  });

  return EventService.create(eventDetail)
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: ADD_EVENT_SUCCESS,
          payload: data
        });

        if (setEmailArray === null) {
          dispatch(getSelfEventListAction(eventDetail.attendees[0].email));
        } else {
          dispatch(getEventListAction());
          setEmailArray([]);
        }
      } else {
        dispatch({
          type: ADD_EVENT_FAILED,
          payload: 'Failed'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: ADD_EVENT_FAILED,
        payload: 'Failed'
      });
    });
};


export const deleteEvent = (eventId) => (dispatch) => {
  dispatch({
    type: DELETE_EVENT_PROCESSING
  });
  return EventService.delete(eventId)
    .then(({ status, _ }) => {
      if (status === 200) {
        dispatch({
          type: DELETE_EVENT_SUCCESS,
          payload: 'Event deleted successfully.'
        });
      } else {
        dispatch({
          type: DELETE_EVENT_FAILED,
          payload: 'Error occured while deleting the event.'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: DELETE_EVENT_FAILED,
        payload: 'Error occured while deleting the event.'
      });
    })
    .finally(() => {
      setTimeout(() => {
        dispatch({
          type: DELETE_EVENT_RESET
        });
      }, 4000);
    });
};

export const clearDeleteState = () => (dispatch) => {
  dispatch({
    type: DELETE_EVENT_RESET
  })
}

export const updateEvent = (eventId, newData) => (dispatch) => {
  dispatch({
    type: UPDATE_EVENT_PROCESSING
  });
  return EventService.edit(eventId, newData)
    .then(({ status, _ }) => {
      if (status === 200) {
        dispatch({
          type: UPDATE_EVENT_SUCCESS,
          payload: 'Event updated successfully.'
        });
      } else {
        dispatch({
          type: UPDATE_EVENT_FAILED,
          payload: 'Error occured while updating the event.'
        });
      }
    })
    .catch(() => {
      dispatch({
        type: UPDATE_EVENT_FAILED,
        payload: 'Error occured while updating the event.'
      });
    })
    .finally(() => {
      setTimeout(() => {
        dispatch({
          type: UPDATE_EVENT_RESET
        });
      }, 4000);
    });
};

export const clearUpdateState = () => (dispatch) => {
  dispatch({
    type: UPDATE_EVENT_RESET
  })
}