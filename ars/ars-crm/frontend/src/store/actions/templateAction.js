import TemplateService from './../../services/template-service';
import {
  FETCH_TEMPLATES_PROCESSING,
  FETCH_TEMPLATE_FAILED,
  FETCH_TEMPLATE_SUCCESS
} from '../types/types';


export const getTemplateAction = () => (dispatch) => {
  dispatch({
    type: FETCH_TEMPLATES_PROCESSING
  });

  return TemplateService.getTemplates()
    .then(({ status, data }) => {
      if (status === 200) {
        dispatch({
          type: FETCH_TEMPLATE_SUCCESS,
          payload: data
        });
      } else {
        dispatch({
          type: FETCH_TEMPLATE_FAILED,
        });
      }
    })
    .catch((e) => {
      dispatch({
        type: FETCH_TEMPLATE_FAILED,
      });
    });
};
