import { useParams } from 'react-router-dom';
import Login from '../components/auth/login';

const AuthPage = () => {
  const { mode } = useParams();
  return <div className="auth">{mode === 'login' && <Login />}</div>;
};

export default AuthPage;
