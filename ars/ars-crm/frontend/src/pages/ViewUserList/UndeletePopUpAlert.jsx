import React from 'react';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button } from '@material-ui/core';

const UndeletePopUpAlert = ({ openUndeletePopup, setOpenUndeletePopup, agreeAction }) => {
  return (
    <Dialog
      open={openUndeletePopup}
      onClose={() => setOpenUndeletePopup(false)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to undelete this user?
        </DialogContentText>
      </DialogContent>
      <DialogActions className="alert-btn-holder">
        <Button onClick={agreeAction} color="primary" variant="contained">
          Undelete
        </Button>
        <Button
          onClick={() => setOpenUndeletePopup(false)}
          color="primary"
          variant="outlined"
          className="forClearHover"
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UndeletePopUpAlert;
