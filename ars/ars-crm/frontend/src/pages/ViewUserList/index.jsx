import { ThemeProvider } from '@material-ui/core';
// import Typography from '@material-ui/core/Typography';
import React, { useMemo, useState } from 'react';
import UserService from '../../services/user-service';
import theme from '../../styles/theme';
import TableContainer from './UserTableContainer';
import { SelectColumnFilter, SelectColumnFilterEl } from '../../components/TableContainer/Filter';

const ClientTable = () => {
  const { getCrmUsersWithQuery, getDeletedCrmUsersWithQuery } = UserService;
  const [data, setData] = useState([]);
  const [loading, setLoading] = React.useState(false);
  const [pageCount, setPageCount] = React.useState(0);
  const [totalData, setTotalData] = React.useState(0);
  const [updated] = useState(0);

  const [showDeletedUsers, setShowDeletedUser] = useState(false);

  const columns_ = useMemo(
    () => [
      {
        id: 'fullName',
        Header: 'Full Name',
        accessor: (el) => `${el?.fullName}`
      },
      {
        id: 'email',
        Header: 'Email',
        accessor: 'email',
        disableFilters: false
      },
      {
        id: 'residential.state',
        Header: 'State',
        accessor: (el) => el?.residential?.state || '-'
      },
      {
        id: 'accessLevel',
        Header: 'Access Level',
        Filter: SelectColumnFilter,
        accessor: (el) => el.accessLevel || '-',
        possibleFilters: ['ADMIN', 'SUPER_ADMIN', 'USER']
      },
      {
        id: 'rolePosition',
        Header: 'Role Postion',
        accessor: (el) => el?.rolePosition || '-'
      },
      {
        id: 'verified',
        Header: 'Verified',
        Filter: SelectColumnFilterEl,
        accessor: (el) => {
          if (el?.verified === false) return 'Unverified';
          else if (el?.verified === true) return 'Verified';
          else return '-';
        },
        possibleFilters: [
          { label: 'Unverified', value: false },
          { label: 'Verified', value: true }
        ]
      }
    ],
    []
  );
  // generate param string
  const getQueryString = (sortBy, pageSize, pageIndex, filters) => {
    const noRegexQuery = '&noRegex=accessLevel,verified';
    const requiredDataColumns = '&select=fullName,email,residential,accessLevel,rolePosition,verified';

    let sortString = '';

    if (sortBy.length) {
      sortString = '&sortBy=' + sortBy[0].id;
      sortString += sortBy[0].desc ? '&sortOrder=-1' : '&sortOrder=1';
    }

    let searchString = '';
    if (filters && filters.length) {
      searchString =
        '&search=' +
        filters
          .map((x) => {
            return `"${x.id}":"${x.value}"`;
          })
          .join(',');
    }
    return `?limit=${pageSize}&page=${pageIndex + 1}${sortString}${searchString}${requiredDataColumns}${noRegexQuery}`;
  };

  const getData = React.useCallback(
    ({ sortBy, pageSize, pageIndex, filters }) => {
      setLoading(true);

      // Make timeStamp default sort
      if (sortBy.length === 0) {
        sortBy[0] = { id: '_id', desc: true };
      }
      const query = getQueryString(sortBy, pageSize, pageIndex, filters);
      if (showDeletedUsers) {
        getDeletedCrmUsersWithQuery(query)
          .then((response) => {
            const x = response.data;
            setData(x.docs);
            setPageCount(x.totalPages);
            setTotalData(x.totalDocs);
            setLoading(false);
          })
          .catch((e) => {
            setLoading(false);
          });
      } else {
        getCrmUsersWithQuery(query)
          .then((response) => {
            const x = response.data;
            setData(x.docs);
            setPageCount(x.totalPages);
            setTotalData(x.totalDocs);
            setLoading(false);
          })
          .catch((e) => {
            setLoading(false);
          });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updated, showDeletedUsers]
  );

  const renderRowSubComponent = () => {
    return <span> Other details of this client will be visible here </span>;
  };

  // const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  return (
    <ThemeProvider theme={theme}>
      <div className="table-content-wrapper content-section user-table-wrapper">
        <h4 style={{ marginBottom: '10px' }} className={showDeletedUsers ? 'text-danger' : ''}>
          {showDeletedUsers ? 'Deleted User List' : 'User List'}
        </h4>
        <TableContainer
          columns={columns_}
          data={data}
          defaultPageSize={1}
          fetchData={getData}
          loading={loading}
          pageCount={pageCount}
          totalData={totalData}
          renderRowSubComponent={renderRowSubComponent}
          showDeletedUsers={showDeletedUsers}
          setShowDeletedUser={setShowDeletedUser}
        />
      </div>
    </ThemeProvider>
  );
};

export default ClientTable;
