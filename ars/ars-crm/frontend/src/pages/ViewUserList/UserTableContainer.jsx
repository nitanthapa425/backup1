// import DeleteIcon from '@material-ui/icons/Delete';
// import AddIcon from '@material-ui/icons/Add';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import MaterialTableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import AddIcon from '@material-ui/icons/Add';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
// import './style.css';
// import jsPDF from 'jspdf'
// import 'jspdf-autotable'
// import Pagination from 'react-bootstrap/Pagination';
// import { Form } from 'react-bootstrap';
// import { BsArrowDownShort, BsArrowUpShort } from 'react-icons/all';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import DeleteIcon from '@material-ui/icons/Delete';
import MatPagination from '@material-ui/lab/Pagination';
import React, { Fragment, useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useExpanded, useFilters, usePagination, useRowSelect, useSortBy, useTable } from 'react-table';
import { AuthRole } from '../../components/common/AuthRole';
import useDebounce from '../../components/common/use-debounce';
import Message from '../../components/message/Message';
// import Table from "react-bootstrap/Table";
import { DefaultColumnFilter, Filter } from '../../components/TableContainer/Filter';
// import { useHistory } from 'react-router-dom';
import { resetStatus } from '../../store/actions/clientAction';
import { deleteUserAction, unDeleteUserAction } from '../../store/actions/userAction';
import SelfDeleteAlertDialog from './SelfDeleteAlert';
import UndeletePopUpAlert from './UndeletePopUpAlert';

export const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = React.useRef();
  const resolvedRef = ref || defaultRef;
  React.useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate;
  }, [resolvedRef, indeterminate]);

  return (
    <>
      <input type="checkbox" ref={resolvedRef} {...rest} />
    </>
  );
});

function TableContainer({
  columns,
  data,
  fetchData,
  loading,
  pageCount: controlledPageCount,
  totalData,
  renderRowSubComponent,
  setShowDeletedUser,
  showDeletedUsers
}) {
  // const history = useHistory();
  const dispatch = useDispatch();

  const userState = useSelector((state) => {
    return state.userReducer;
  });
  const authUser = useSelector((state) => {
    return state.authReducer.loginData?.user;
  });

  // const Auth = AuthRole();
  // let userRole = Auth.accessLevel;
  const [openSelfDeletePopup, setSelfDeletePopup] = useState(false);
  const [openUndeletePopup, setOpenUndeletePopup] = useState(false);

  const handleUndeleteUser = () => {
    setOpenUndeletePopup(false);
    // selectedFlatRows.forEach((obj, index) => {
    if (selectedFlatRows[0].original.id) {
      dispatch(unDeleteUserAction(selectedFlatRows[0].original.id));
    }
    // });
  };

  const [popUpopen, setpopUpopen] = useState(false);
  const handleClickOpen = () => {
    const hasLoggedInUser = selectedFlatRows?.filter((row) => row.original.id === authUser?._id).length > 0;
    if (hasLoggedInUser) {
      setSelfDeletePopup(true);
    } else {
      setpopUpopen(true);
    }
  };

  const handleClosePopUp = () => {
    setpopUpopen(false);
  };
  // const popUpAgree = () => {
  //   selectedFlatRows.forEach((obj, index) => {

  //     dispatch(deleteUserAction(obj.original._id));
  //   });
  const popUpAgree = () => {
    selectedFlatRows.forEach((obj, index) => {
      dispatch(deleteUserAction(obj.original.id));
    });

    // setSelectedClient([]);

    // dispatch(deleteClientDetailsAction(selectedClient)).then(()=> {
    // setSnackbarSuccess(true);
    handleClosePopUp();
  };

  // const handleDelete = () => {
  //   selectedFlatRows.forEach((obj, index) => {

  //     dispatch(deleteClientDetailsAction(obj.original._id));
  //   });
  // };

  const popUpDisagree = () => {
    handleClosePopUp();
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    visibleColumns,
    page,
    // canPreviousPage,

    // canNextPage,
    // pageOptions,
    // nextPage,
    // previousPage,
    pageCount,
    gotoPage,
    setPageSize,
    // Get the state from the instance
    state: { pageIndex, pageSize, sortBy, filters },
    selectedFlatRows
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 }, // Pass our hoisted table state
      manualPagination: true, // Tell the usePagination
      manualSortBy: true,
      manualFilters: true,

      autoResetSortBy: false,
      defaultColumn: { Filter: DefaultColumnFilter },
      // hook that we'll handle our own data fetching
      // This means we'll also have to provide our own
      // pageCount.
      pageCount: controlledPageCount
    },

    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect,

    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'selection',

          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),

          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          )
        },
        ...columns
      ]);
    }
  );
  const debounceFilter = useDebounce(filters, 1000);

  const handleChange = (x, value) => {
    gotoPage(value - 1);
  };
  // Listen for changes in pagination and use the state to fetch our new data
  React.useEffect(() => {
    fetchData({ pageIndex, pageSize, sortBy, filters: debounceFilter });
  }, [fetchData, pageIndex, pageSize, sortBy, debounceFilter, userState, showDeletedUsers]);
  // Render the UI for your table

  // const handleViewDetail = () => {
  //   history.push(`/client-profile/update/${selectedFlatRows[0].original._id}`);
  // };
  // const handleUserUpdate = () => {
  //   history.push(`/update-client-profile/${selectedFlatRows[0].original._id}`);
  // };

  const [check] = useState(true);

  useEffect(() => {
    return () => {
      dispatch(resetStatus());
    };
  }, [dispatch]);

  // const csvLink = useRef()

  // const [allData, setAllData] = useState([])

  // useEffect(() => {
  //   setAllData(exportData(headerGroups, page))
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [page])

  // const downloadCSV = async () => {
  //   csvLink.current.link.click()
  // }

  // const downloadPDF = async () => {
  //   await setAllData(exportData(headerGroups, page))
  //   const doc = new jsPDF()
  //   doc.text('Clients Details', 15, 10)

  //   doc.autoTable({
  //     head: [allData[0]],
  //     body: [
  //       ...allData.slice(1)
  //     ],
  //     styles: {
  //       cellPadding: 1,
  //       fontSize: 5
  //     }

  //   })
  //   doc.save(`Ganesh_client_list_${new Date(Date.now()).toLocaleDateString()}.pdf`)
  // }
  const history = useHistory();
  const Auth = AuthRole();
  let userRole = Auth.accessLevel;
  const handleViewDetail = () => {
    history.push(`/user-detail/${selectedFlatRows[0].original.id}`);
  };
  const handleUserUpdate = () => {
    // history.push(`/update-user`);
    history.push(`/update-user/${selectedFlatRows[0].original.id}`);
  };

  return (
    <>
      <Backdrop open={userState.loading || loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message status={check && userState.status} severity={userState.severity} message={userState.message} />

      <div style={{ display: 'none' }}>
        <Dialog
          open={popUpopen}
          onClose={handleClosePopUp}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to delete this record? Please note that, once deleted, this cannot be undone and the
              record will be permanently removed.
            </DialogContentText>
          </DialogContent>
          <DialogActions className="alert-btn-holder">
            <Button onClick={popUpAgree} variant="contained" className="cancel-button">
              Delete
            </Button>
            <Button onClick={popUpDisagree} color="primary" variant="outlined">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>

      {/* <Export currentData={allData} csvLinkRef={csvLink} downloadCSV={downloadCSV} downloadPDF={downloadPDF} /> */}

      {selectedFlatRows?.length > 0 ? (
        <div className="table-top-header">
          <div className="tableHeader-left-col">
            <span className="selected">{selectedFlatRows?.length} selected</span>
          </div>
          {!showDeletedUsers ? (
            <div className="tableHeader-right-col after-checked">
              {selectedFlatRows.length === 1 ? (
                userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' || userRole === 'USER' ? (
                  <Button variant="contained" className="view-btn" onClick={handleViewDetail}>
                    View
                  </Button>
                ) : null
              ) : null}
              {selectedFlatRows.length === 1 ? (
                userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' ? (
                  <Button variant="contained" className="edit-btn" onClick={handleUserUpdate}>
                    Edit
                  </Button>
                ) : null
              ) : null}
              {selectedFlatRows.length >= 1 ? (
                userRole === 'SUPER_ADMIN' ? (
                  <Button
                    variant="contained"
                    startIcon={<DeleteIcon />}
                    className="delete-btn"
                    onClick={handleClickOpen}
                  >
                    Delete
                  </Button>
                ) : null
              ) : null}
            </div>
          ) : (
            <div className="tableHeader-right-col after-checked">
              {selectedFlatRows.length === 1 ? (
                userRole === 'SUPER_ADMIN' ? (
                  <Button
                    variant="contained"
                    // startIcon={<DeleteIcon />}
                    className="delete-btn"
                    onClick={() => {
                      setOpenUndeletePopup(true);
                    }}
                  >
                    UnDelete
                  </Button>
                ) : null
              ) : null}
            </div>
          )}
        </div>
      ) : (
        <div className="table-top-header">
          <div className="tableHeader-left-col">
            <Link to="/crm-user">
              <Button startIcon={<AddIcon />}>Add</Button>
            </Link>
          </div>

          <div className="tableHeader-right-col after-checked">
            <Button
              variant="contained"
              color="primary"
              className={!showDeletedUsers ? 'delete-btn' : 'export-btn'}
              onClick={() => setShowDeletedUser((prev) => !prev)}
            >
              {showDeletedUsers ? 'Show Users' : 'Show Deleted Users'}
            </Button>
          </div>
        </div>
      )}

      <Paper>
        <MaterialTableContainer>
          <Table hover={true} loading={loading} {...getTableProps()} className={loading ? 'atable-loading' : null}>
            <TableHead>
              {headerGroups.map((headerGroup) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <TableCell {...column.getHeaderProps()}>
                      <div
                        {...column.getSortByToggleProps()}
                        style={{ display: 'flex', justifyContent: 'space-between' }}
                      >
                        {column.render('Header')}
                        {/* Add a sort direction indicator */}
                        <span>
                          {column.isSorted ? column.isSortedDesc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon /> : ''}
                        </span>
                      </div>
                      <Filter column={column} />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page.length > 0 ? (
                page.map((row, i) => {
                  prepareRow(row);

                  return (
                    <Fragment key={row.getRowProps().key}>
                      <TableRow>
                        {row.cells.map((cell) => {
                          return <TableCell {...cell.getCellProps()}>{cell.render('Cell')}</TableCell>;
                        })}
                      </TableRow>
                      {row.isExpanded && (
                        <TableRow>
                          <TableCell colSpan={visibleColumns.length}>{renderRowSubComponent(row)}</TableCell>
                        </TableRow>
                      )}
                    </Fragment>
                  );
                })
              ) : loading ? null : (
                <span>No&nbsp;record&nbsp;found</span>
              )}
            </TableBody>
          </Table>
        </MaterialTableContainer>
      </Paper>

      <div className="pagination">
        <div className="total">
          <span>Total {totalData}</span>
        </div>
        <div className="pagination-counter">
          <Form.Group controlId="exampleForm.SelectCustom">
            <span className="pagination-title">Row Per Page</span>
            <Form.Control
              as="select"
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
              custom
            >
              {[...new Set([5, 10, 20, 30, 40, 50, 100, totalData])]
                .filter((v, i) => v <= totalData)
                .map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    {pageSize === totalData ? 'All' : pageSize}
                  </option>
                ))}
            </Form.Control>
          </Form.Group>
          <MatPagination count={pageCount} onChange={handleChange} />
        </div>
      </div>
      <SelfDeleteAlertDialog open={openSelfDeletePopup} setOpen={setSelfDeletePopup} />
      <UndeletePopUpAlert
        openUndeletePopup={openUndeletePopup}
        setOpenUndeletePopup={setOpenUndeletePopup}
        agreeAction={handleUndeleteUser}
      />
    </>
  );
}

export default TableContainer;
