import { useState, useEffect } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem';
// import MoreVertIcon from '@material-ui/icons/MoreVert';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { deleteCategoryAction } from '../../../store/actions/categoryAction';
import { Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import { AuthRole } from '../../../components/common/AuthRole';
import DeleteOutline from '@material-ui/icons/DeleteOutline';

const CategorySelect = ({
  isLoading,
  categoryList,
  openCategory,
  setOpenViewList,
  openViewList,
  selectedCatId,
  setSelectedCatId,
  handleOnChange,
  viewList
}) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [popUpopen, setpopUpopen] = useState(false);
  const handleClickOpenConfirmation = () => {
    handleClose();
    setpopUpopen(true);
  };
  const popUpAgree = () => {
    // dispatch(deleteClientDetailsAction(selectedClient)).then(()=> {
    handleDeleteEdit(selectedCatId);
    handleClosePopUp();
  };

  const popUpDisagree = () => {
    handleClosePopUp();
  };

  const handleClosePopUp = () => {
    setpopUpopen(false);
  };

  // eslint-disable-next-line no-unused-vars
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event, catId) => {
    setSelectedCatId(catId);
    setAnchorEl(event.currentTarget);
  };

  const setnullSelectedCatId = () => {
    setSelectedCatId(null);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const [categoryLoaded, setCategoryLoaded] = useState(true);
  const [categoryState, setCategoryState] = useState([]);
  useEffect(() => {
    if (categoryList && categoryList.length) {
      setCategoryLoaded(true);
      setCategoryState(categoryList);
    }
  }, [categoryList]);

  const handleCategoryEdit = (id) => {
    setnullSelectedCatId();
    history.push(`/edit-category/${id}`);
  };

  const handleDeleteEdit = (id) => {
    setnullSelectedCatId();
    dispatch(deleteCategoryAction(id));
  };
  const Auth = AuthRole();
  let userRole = Auth.accessLevel;

  return (
    <>
      {userRole !== 'USER' ? (
        <>
          <Dialog
            open={popUpopen}
            onClose={handleClosePopUp}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                Are you sure to delete the selected category?
              </DialogContentText>
            </DialogContent>
            <DialogActions className="alert-btn-holder">
              <Button
                onClick={popUpAgree}
                //  variant="contained"
                className="cancel-button"
              >
                Delete
              </Button>
              <Button onClick={popUpDisagree} variant="outlined" color="primary">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

          <div className="select-category" style={{ display: openCategory ? 'block' : 'none' }}>
            <div className="view-btn-holder">
              <Button
                className={`${viewList.length === 0 ? 'Ondisable' : 'NotDisable'}`}
                onClick={(e) => {
                  setOpenViewList(!openViewList);
                }}
                disabled={viewList.length === 0}
              >
                {!openViewList ? 'View List' : 'Hide List'}
              </Button>
            </div>
            <div className="category-heading">
              <h4>Select Category</h4>
            </div>
            {/* {isLoading && 'Loading...'} */}
            {categoryState?.length !== 0 ? (
              <div className="category-list">
                <Grid container spacing={1}>
                  {categoryLoaded &&
                    [...categoryState].reverse().map((category, i) => {
                      return (
                        <Grid key={i} item xs={12}>
                          <div className=" list">
                            <Button
                              aria-controls="simple-menu"
                              aria-haspopup="true"
                              onClick={(e) => {
                                handleClick(e, category.id);
                              }}
                              className="more-icon"
                            >
                              <div className="iconCategory">
                                <EditIcon
                                  className="iconCategory-one"
                                  onClick={() => {
                                    handleCategoryEdit(category.id);
                                  }}
                                />
                                {userRole === 'SUPER_ADMIN' ? (
                                  <DeleteOutline
                                    className="iconCategory-Two"
                                    onClick={() => {
                                      handleClickOpenConfirmation();
                                    }}
                                  />
                                ) : null}
                              </div>
                            </Button>

                            {/* <Menu
                              className="category-menu"
                              anchorEl={anchorEl}
                              keepMounted
                              open={Boolean(anchorEl)}
                              onClose={handleClose}
                            >
                              <MenuItem
                                onClick={() => {
                                  handleCategoryEdit(selectedCatId);
                                }}
                              >
                                Edit
                              </MenuItem>
                              {userRole === 'SUPER_ADMIN' ? (
                                <MenuItem
                                  onClick={() => {
                                    // handleDeleteEdit(selectedCatId)
                                    handleClickOpenConfirmation();
                                  }}
                                >
                                  Delete
                                </MenuItem>
                              ) : null}
                            </Menu> */}
                            <FormControlLabel
                              value={category.emailList}
                              control={
                                <Checkbox
                                  name="checkedB"
                                  color="primary"
                                  onChange={(e) => {
                                    handleOnChange(e, category);
                                  }}
                                />
                              }
                              label={category.categoryName}
                            />
                          </div>
                        </Grid>
                      );
                    })}
                </Grid>
              </div>
            ) : (
              <span>No record Found</span>
            )}
          </div>
        </>
      ) : null}
    </>
  );
};

export default CategorySelect;
