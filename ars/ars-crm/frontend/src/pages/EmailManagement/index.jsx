import React, { useEffect, useState, useRef } from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import {
  EditorState,
  convertToRaw
  // ContentState
} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
// import htmlToDraft from 'html-to-draftjs';

import Button from '@material-ui/core/Button';
import { ThemeProvider } from '@material-ui/styles';

import { bulkEmailSendAction, getCategoryAction } from '../../store/actions/categoryAction';
import { getTemplateAction } from '../../store/actions/templateAction';

import TemplateSelect from './TemplateSelect';
import CategorySelect from './CategorySelect';
import SendEmail from './SendEmail';

import theme from '../../styles/theme';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import { RequiredArray, RequiredField } from '../../components/validations/YupValidations';
import { Backdrop, CircularProgress } from '@material-ui/core';
import Message from '../../components/message/Message';

const EmailManagement = () => {
  const [viewList, setviewList] = useState([]);
  const [selectedCatId, setSelectedCatId] = useState(null);

  const formikRef = useRef();

  const [openViewList, setOpenViewList] = useState(false);
  const [openCategory, setOpenCategory] = useState(true);
  const [openEmail, setOpenEmail] = useState(false);
  const [emailValue, setEmailValue] = useState('');
  const [selectedTemplate, setSelectedTemplate] = useState(null);

  const [myEmailContent, setMyEmailContent] = useState('');

  const selectTemplate = (template) => {
    if (template?.id === selectedTemplate?.id) return setSelectedTemplate(null);
    setSelectedTemplate(template);
  };

  const dispatch = useDispatch();
  const categoryReducer = useSelector((state) => {
    return state.categoryReducer;
  });

  const templatesState = useSelector((state) => {
    return state.templateReducer;
  });

  const { categoryList, deletedCategory } = categoryReducer;
  const initialValues = {
    // from: 'from',
    from: '',
    subject: '',
    content: '',
    to: [],
    headerImg: '',
    footerImg: ''
  };

  useEffect(() => {
    setEmailValue(viewList);
  }, [viewList]);

  const onSubmit = (values, onSubmitProps) => {
    // if (!myEmailContent) return onSubmitProps.setFieldError('content', 'Content is required')
    const emailArray = emailValue.map((v, i) => {
      if (v.email) {
        return v;
      } else {
        return { email: v };
      }
    });

    // console.log(myEmailContent);
    // return console.log(myEmailContent);
    const emailValues = {
      subject: values.subject,
      // content: convertedContent,
      // content: myEmailContent,
      content: values.content,
      to: emailArray,
      from: values.from
    };

    dispatch(
      bulkEmailSendAction(
        emailValues,
        onSubmitProps,
        setEmailValue,
        setConvertedContent,
        setEditorState,
        EditorState,
        setSelectedTemplate,
        setMyEmailContent
      )
    );
  };

  const validationSchema = yup.object({
    to: RequiredArray('At least a recipient'),
    // from: RequiredField('From'),
    subject: RequiredField('Subject'),
    content: RequiredField('Content')
  });

  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  // eslint-disable-next-line no-unused-vars
  const [convertedContent, setConvertedContent] = useState(null);

  useEffect(() => {
    if (selectedTemplate?.templateBody) {
      setMyEmailContent(selectedTemplate?.templateBody);
      // try {
      //   const contentBlock = htmlToDraft(selectedTemplate?.templateBody);
      //   if (contentBlock) {
      //     const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      //     const editorState = EditorState.createWithContent(contentState);
      //     setEditorState(editorState);
      //   }
      // } catch (error) { }
    }
  }, [selectedTemplate]);

  const handleEditorChange = (state) => {
    setEditorState(state);
  };
  const convertContentToHTML = () => {
    const rawContentState = convertToRaw(editorState.getCurrentContent());
    const convertedHtml = draftToHtml(rawContentState);
    setConvertedContent(convertedHtml);
  };

  useEffect(() => {
    convertContentToHTML();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [editorState]);

  const [selectedCategory, setSelectedCatgory] = useState({});

  const handleOnChange = (e, category) => {
    if (e.target.checked) {
      setSelectedCatgory({ ...selectedCategory, [category.categoryName]: category.emailList.map((x) => x.email) });
    } else {
      setSelectedCatgory({ ...selectedCategory, [category.categoryName]: undefined });
    }
  };

  useEffect(() => {
    let _emails = [];
    Object.keys(selectedCategory).forEach((x) => {
      if (selectedCategory[x]) _emails.push(...selectedCategory[x]);
    });
    setviewList([...new Set(_emails)]);
  }, [selectedCategory]);

  useEffect(() => {
    dispatch(getTemplateAction());
  }, [dispatch]);

  useEffect(() => {
    dispatch(getCategoryAction());
  }, [dispatch, deletedCategory]);

  const goBack = () => {
    setOpenCategory(true);
    setOpenEmail(false);
  };

  const onClearClick = () => {
    setEmailValue([]);
    // setConvertedContent('');
    // setEditorState(EditorState.createEmpty());
    setMyEmailContent('');
  };
  const handleCloseViewList = (value) => {
    setOpenViewList(false);
  };
  const authRole = useSelector((state) => state.authReducer);

  useEffect(() => {
    if (formikRef.current && emailValue.length > 0) {
      formikRef.current.setFieldValue('to', emailValue);
    }
  }, [emailValue]);
  useEffect(() => {
    if (categoryReducer.sentEmail) {
      goBack();
      setviewList([]);
      setSelectedTemplate(null);
    }
  }, [categoryReducer.sentEmail]);

  function SimpleDialog(props) {
    // const classes = useStyles();
    const { onClose, open } = props;
    return (
      <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={open} className="category-list-dialog">
        <DialogTitle id="simple-dialog-title">View Lists</DialogTitle>
        <List>
          {viewList.map((email) => (
            <ListItem button onClick={() => onClose()} key={email}>
              <ListItemAvatar>
                <Avatar>
                  <PersonIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={email} />
            </ListItem>
          ))}
        </List>
      </Dialog>
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={categoryReducer.loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message status={categoryReducer.status} severity={categoryReducer.severity} message={categoryReducer.message} />
      {/* <ToastContainer hideProgressBar={true} position="top-center" /> */}
      <Formik
        innerRef={formikRef}
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        enableReinitialize
      >
        {({ resetForm, isSubmitting }) => {
          return (
            <div className="content-section email-management-wrapper">
              <div className="select-category" style={{ display: openCategory ? 'block' : 'none' }}>
                <div className="email-content-wrapper">
                  {/* Select Email Templates */}
                  <TemplateSelect
                    templates={templatesState.templates}
                    selectTemplate={selectTemplate}
                    selectedTemplate={selectedTemplate}
                  />
                  {/* Select Email Category  */}
                  <CategorySelect
                    isLoading={categoryReducer.loading}
                    categoryList={categoryList}
                    openCategory={openCategory}
                    setOpenViewList={setOpenViewList}
                    openViewList={openViewList}
                    selectedCatId={selectedCatId}
                    setSelectedCatId={setSelectedCatId}
                    handleOnChange={handleOnChange}
                    viewList={viewList}
                  />
                  {authRole?.loginData?.user?.accessLevel !== 'USER' ? (
                    <div className="button-holder">
                      <Button
                        // type="submit"
                        variant="contained"
                        color="primary"
                        onClick={(e) => {
                          setOpenCategory(false);
                          setOpenEmail(true);
                        }}
                      >
                        Next
                      </Button>
                    </div>
                  ) : null}
                </div>
                <SimpleDialog open={openViewList} onClose={handleCloseViewList} />
                {/* <div className="view-category-list">
                  {openViewList ? (
                    <div>
                      <br />
                      <h4>View List</h4>
                      {viewList.map((view, i) => {
                        return <div>{view}</div>;
                      })}
                    </div>
                  ) : null}
                </div>
               */}
              </div>

              <SendEmail
                openEmail={openEmail}
                editorState={editorState}
                handleEditorChange={handleEditorChange}
                viewList={viewList}
                emailValue={emailValue}
                setEmailValue={setEmailValue}
                setMyEmailContent={setMyEmailContent}
                myEmailContent={myEmailContent}
                loading={categoryReducer.loading || isSubmitting}
                onClearClick={() => {
                  onClearClick();
                  resetForm();
                }}
                goBack={goBack}
              />
            </div>
          );
        }}
      </Formik>
    </ThemeProvider>
  );
};

export default EmailManagement;
