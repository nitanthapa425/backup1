/* eslint-disable no-unused-vars */
import Typography from '@material-ui/core/Typography';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { Button, TextField, Grid, Checkbox } from '@material-ui/core';
import { Editor } from 'react-draft-wysiwyg';
import { Field, Form } from 'formik';
import AreYouSurePopUp from '../../../components/dropdown/AreYouSure';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MyCustomUploadAdapterPlugin } from '../../../utils/MyUploadAdapter';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

// const uploadImageCallBack = (file) =>
//   new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest();
//     xhr.open('POST', 'https://api.imgur.com/3/image');
//     xhr.setRequestHeader('Authorization', `Client-ID ${process.env.REACT_APP_IMGUR_CLIENT_ID}`);
//     const data = new FormData();
//     data.append('image', file);
//     xhr.send(data);
//     xhr.addEventListener('load', () => {
//       const response = JSON.parse(xhr.responseText);
//       resolve(response);
//     });
//     xhr.addEventListener('error', () => {
//       const error = JSON.parse(xhr.responseText);
//       reject(error);
//     });
//   });

const SendEmail = ({
  openEmail,
  // editorState,
  // handleEditorChange,
  viewList,
  emailValue,
  setEmailValue,
  onClearClick,
  goBack,
  // onAgreeClick,
  setMyEmailContent,
  myEmailContent,
  loading
}) => {
  return (
    <div className="semd-email-form" style={{ display: openEmail ? 'block' : 'none' }}>
      <h2>Send Email </h2>
      <Form>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Grid item xs={12} md={6} lg={4}>
              <Field name="from">
                {(props) => {
                  const { field, meta } = props;
                  return (
                    <div>
                      <TextField
                        type="text"
                        label={
                          <>
                            From
                            {/* <span className="color-red">*</span> */}
                          </>
                        }
                        {...field}
                        error={meta.touched && meta.error ? true : false}
                        helperText={meta.touched && meta.error ? <div>{meta.error}</div> : null}
                      />
                    </div>
                  );
                }}
              </Field>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid item xs={12} lg={8}>
              <Field name="subject">
                {(props) => {
                  const { field, meta } = props;
                  return (
                    <div>
                      <TextField
                        type="text"
                        label={
                          <>
                            Subject <span className="color-red">*</span>
                          </>
                        }
                        error={meta.touched && meta.error ? true : false}
                        helperText={meta.touched && meta.error ? <div>{meta.error}</div> : null}
                        {...field}
                      />
                    </div>
                  );
                }}
              </Field>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid item xs={12} lg={8}>
              <Field name="content">
                {(props) => {
                  const {
                    meta,
                    form: { setFieldValue }
                  } = props;

                  return (
                    <div>
                      <Typography variant="subtitle1" className="textarea-label">
                        {meta.touched && meta.error ? (
                          <div className="color-red">
                            Content <span >*</span>
                          </div>
                        ) : (
                          <div>
                            Content <span className="color-red">*</span>
                          </div>
                        )}
                      </Typography>

                      {/* <Editor
                        editorState={editorState}
                        onEditorStateChange={handleEditorChange}
                        wrapperClassName="wrapper-class"
                        editorClassName="editor-class"
                        toolbarClassName="toolbar-class"
                        placeholder="Type here.."
                        toolbar={{
                          inline: { inDropdown: true },
                          list: { inDropdown: true },
                          textAlign: { inDropdown: true },
                          link: { inDropdown: true },
                          history: { inDropdown: true },
                          image: {
                            uploadCallback: uploadImageCallBack,
                            alt: { present: true, mandatory: true }
                          }
                        }}
                      /> */}

                      <CKEditor
                        editor={ClassicEditor}
                        data={myEmailContent}
                        onReady={(editor) => {
                          // You can store the "editor" and use when it is needed.
                          // console.log('Editor is ready to use!', editor);
                          setMyEmailContent(editor.getData());
                          setFieldValue('content', editor.getData());
                        }}
                        onChange={(event, editor) => {
                          const data = editor.getData();
                          // console.log({ event, editor, data });
                          setFieldValue('content', editor.getData());
                          setMyEmailContent(data);
                        }}
                        config={{ extraPlugins: [MyCustomUploadAdapterPlugin] }}
                      />
                      {meta.touched && meta.error && (
                        <span className="color-red" style={{ fontSize: '0.9rem' }}>
                          {meta.error}
                        </span>
                      )}
                    </div>
                  );
                }}
              </Field>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Grid item xs={12} lg={8}>
              <Field name="to">
                {(props) => {
                  const { form, meta } = props;
                  const { setFieldValue } = form;

                  return (
                    <div>
                      <Autocomplete
                        name="to"
                        multiple
                        freeSolo
                        id="to"
                        options={viewList}
                        value={emailValue || viewList}
                        onChange={(_, value) => {
                          setFieldValue('to', value);
                          setEmailValue(value);
                        }}
                        disableCloseOnSelect
                        className="email-list-autocomplete"
                        getOptionLabel={(option) => {
                          return option;
                        }}
                        renderOption={(option, { selected }) => (
                          <>
                            <Checkbox
                              icon={icon}
                              checkedIcon={checkedIcon}
                              style={{ marginRight: 8 }}
                              checked={selected}
                              colorPrimary
                            />
                            {option}
                          </>
                        )}
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            label={
                              <>
                                Recipient <span className="color-red">*</span>
                              </>
                            }
                            error={meta.touched && meta.error ? true : false}
                            helperText={meta.touched && meta.error ? <div>{meta.error}</div> : null}
                          />
                        )}
                      />
                    </div>
                  );
                }}
              </Field>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <div className="form-row category-submit-button">
              <Button type="submit" variant="contained" color="primary" disabled={loading}>
                Send
              </Button>
              <div className="cancel-button-holder">
                <AreYouSurePopUp
                  variant="outlined"
                  buttonName="Clear"
                  popUpHeading="Are you sure you want to clear everything"
                  popUpDetail="
                  Are you sure you want to clear everything?"
                  formName="myform"
                  buttonType="reset"
                  className="cancel-button"
                  onAgreeClick={onClearClick}
                  yesButtonName="Clear"
                />
              </div>
              <Button startIcon={<ChevronLeftIcon />} className="back-btn " color="primary" onClick={goBack}>
                Back
              </Button>
            </div>
          </Grid>
        </Grid>
      </Form>
    </div>
  );
};

export default SendEmail;
