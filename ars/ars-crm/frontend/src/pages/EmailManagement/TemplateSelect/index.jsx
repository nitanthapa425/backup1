import Grid from '@material-ui/core/Grid';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Template from './Template';

const Templates = ({ templates, selectTemplate, selectedTemplate }) => {
  const authReducer = useSelector((state) => {
    return state.authReducer;
  });
  return (
    <>
      {authReducer?.loginData?.user?.accessLevel !== 'USER' ? (
        <div className="template-wrapper">
          <h2>Select Your Template</h2>
          <Grid container spacing={3}>
            {templates?.length === 0 && <p className="no-record">No Records Found.</p>}
            {templates?.map((template, index) => {
              return (
                <Template
                  key={template.id}
                  onClick={() => selectTemplate(template)}
                  isSelected={template.id === selectedTemplate?.id}
                  // templateImage={sampleTemplateImages[index]}
                  templateImage={template.imgUrl}
                  templateName={template?.templateName}
                  templateBody={template?.templateBody}
                />
              );
            })}
          </Grid>
        </div>
      ) : (
        <div className="content-section dashboard-alert-text">
          <div className="text-holder">
            <div className="icon-holder">
              <ErrorOutlineIcon />
            </div>
            <h4>Sorry! you don't have access to view the Email Management page. </h4>
            <p>
              Kindly visit the <Link to="/client-profile/view">client details</Link> page. If you required the access,
              please contact <a href="mailto:info@arsgroup.com.au">administrator</a>
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default Templates;
