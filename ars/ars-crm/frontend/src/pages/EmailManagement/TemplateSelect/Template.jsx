// import { useEffect, useState } from 'react';
// import { toSvg } from 'html-to-image';
import Grid from '@material-ui/core/Grid';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
// import CircularProgress from '@material-ui/core/CircularProgress';

const Template = ({ onClick, isSelected, templateName, templateBody, templateImage }) => {
  // const [imageUrl, setImageUrl] = useState('');
  // const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   if (templateBody) {
  //     const markup = document.createElement('div');
  //     markup.innerHTML = templateBody;
  //     setLoading(true);
  //     toSvg(markup)
  //       .then((data) => {
  //         setImageUrl(data);
  //       })
  //       .finally(() => setLoading(false));
  //   }
  // }, [templateBody]);

  return (
    <Grid item xs={12} lg={6} xl={4} onClick={onClick}>
      <div className={`template-list-item ${isSelected ? 'active-template' : ''}`}>
        <CheckCircleIcon className={`check-icon ${!isSelected ? 'hide-icon' : 'show-icon'}`} />
        {/* {imageUrl ? <img src={templateImage} alt="" className="template-image" /> : templateName} */}
        {<img src={templateImage ?? ''} alt="" className="template-image" />}
        {/* {loading && <CircularProgress size={20} className="loading" />} */}
      </div>
      <span className={`template-name ${isSelected ? 'active-template-name' : ''}`}>{templateName}</span>
    </Grid>
  );
};

export default Template;
