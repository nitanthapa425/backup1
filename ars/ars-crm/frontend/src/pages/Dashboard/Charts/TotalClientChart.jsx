import { memo } from 'react';
import { Doughnut } from 'react-chartjs-2';

const TotalClientChart = memo(
  ({ data }) => (
    <Doughnut
      data={data}
      options={{
        responsive: false,
        plugins: {
          legend: {
            display: false
          }
        }
      }}
    />
  ),
  (prev, next) => JSON.stringify(prev) === JSON.stringify(next)
);

export default TotalClientChart;
