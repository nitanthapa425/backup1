import { memo } from 'react';
import { Bar } from 'react-chartjs-2';

const PropertyTransactionChart = memo(
  ({ data }) => (
    <Bar
      data={data}
      width={420}
      height={250}
      options={{
        responsive: false,
        plugins: {
          legend: {
            display: false,
            labels: {
              padding: 12,
              boxWidth: 14,
              font: {
                size: 12
              }
            }
          }
        }
      }}
    />
  ),
  (prev, next) => JSON.stringify(prev) === JSON.stringify(next)
);

export default PropertyTransactionChart;
