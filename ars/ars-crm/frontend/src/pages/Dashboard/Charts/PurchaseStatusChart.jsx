import { memo } from 'react'
import { Doughnut } from 'react-chartjs-2';

const PurchaseStatusChart = memo(({ data }) => (
  <Doughnut
    data={data}
    options={{
      responsive: false,
      plugins: {
        legend: {
          position: 'right',
          labels: {
            padding: 8,
            boxWidth: 12,
            font: {
              size: 11
            }
          }
        }
      }
    }}
  />
), (prev, next) => JSON.stringify(prev) === JSON.stringify(next))

export default PurchaseStatusChart