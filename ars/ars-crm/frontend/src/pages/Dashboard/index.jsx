import { useEffect, useState } from 'react';
import { Button, Backdrop, Grid, ThemeProvider, CircularProgress } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import DatePicker from 'react-datepicker';

import { resetLoginState } from '../../store/actions/authAction';
import { clientListsLengthAction, followupNOteListAction } from '../../store/actions/dashboardAction';
import { getEventListAction } from '../../store/actions/eventAction';

import { TotalClientChart, PurchaseStatusChart, PropertyTransactionChart } from './Charts';

import { downloadPdfFromHtmlElement, getClassByTime } from '../../utils';

import theme from '../../../src/styles/theme';
import 'react-datepicker/dist/react-datepicker.css';

const DashboardPage = () => {
  const dispatch = useDispatch();

  const [clientCustomerType, setClientCustomerType] = useState([]);
  const [clientCustomerTypeValue, setClientCustomerTypeValue] = useState([]);
  const [transationPropertyTypes, setTransationPropertyTypes] = useState([]);
  const [transationPropertyTypesValue, setTransationPropertyTypesValue] = useState([]);
  const [selectedDate, setSelectedDate] = useState(null);
  const [transationData, setTransactionData] = useState(null);
  const [allEventList, setAllEventList] = useState([]);

  const authReducer = useSelector((state) => {
    return state.authReducer;
  });
  const clientListsState = useSelector((state) => {
    return state.dashboardReducer;
  });
  const getFollowupNotes = useSelector((state) => {
    return state.dashboardReducer?.getFollowupNotes;
  });
  const eventState = useSelector((state) => {
    return state.eventReducer?.getEventList?.events;
  });

  useEffect(() => {
    if (eventState) {
      const filteredEventState = eventState.filter((event) => {
        const myDate = new Date();
        myDate.setDate(myDate.getDate() - 7);
        const startDate = new Date(event.start.dateTime);
        if (startDate >= myDate) return true;
        else return false;
      });
      setAllEventList(filteredEventState);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(eventState)]);

  useEffect(() => {
    if (authReducer?.loginData?.user?.accessLevel !== 'USER') {
      dispatch(clientListsLengthAction());
      dispatch(followupNOteListAction());
      dispatch(getEventListAction());
    }
  }, [dispatch, authReducer?.loginData?.user?.accessLevel]);

  useEffect(() => {
    if (selectedDate instanceof Date) {
      var month = selectedDate.getMonth() + 1; //months from 1-12
      var year = selectedDate.getFullYear();
      const dateYM = `${year}/${month}/1`;
      dispatch(clientListsLengthAction(dateYM));
    }
  }, [dispatch, selectedDate]);

  useEffect(() => {
    if (
      typeof clientListsState.clientListsAction.customerTypes !== 'undefined' &&
      typeof clientListsState.clientListsAction.customerTypesValue !== 'undefined'
    ) {
      setClientCustomerType(clientListsState.clientListsAction.customerTypes);
      setClientCustomerTypeValue(clientListsState.clientListsAction.customerTypesValue);
    }
  }, [clientListsState.clientListsAction.customerTypes, clientListsState.clientListsAction.customerTypesValue]);

  const totalClientChartData = {
    labels: clientCustomerType,
    options: [{ position: 'bottom' }],
    datasets: [
      {
        label: 'Total Clients',
        data: clientCustomerTypeValue,
        backgroundColor: ['#00ABCC', '#BED643', 'rgba(255, 125, 36, 0.8)'],
        borderWidth: 1
      }
    ]
  };

  useEffect(() => {
    if (
      typeof clientListsState.clientListsAction.transationPropertyTypes !== 'undefined' &&
      typeof clientListsState.clientListsAction.transationPropertyTypesValue !== 'undefined'
    ) {
      setTransationPropertyTypes(clientListsState.clientListsAction.transationPropertyTypes);
      setTransationPropertyTypesValue(clientListsState.clientListsAction.transationPropertyTypesValue);
    }
  }, [
    clientListsState.clientListsAction.transationPropertyTypes,
    clientListsState.clientListsAction.transationPropertyTypesValue
  ]);

  useEffect(() => {
    if (clientListsState.clientListsAction.transationDetails) {
      const tDetails = clientListsState.clientListsAction.transationDetails;
      const _d = Object.keys(tDetails).map((x) => x * 1000);
      const datasets = [
        {
          label: 'Transaction',
          data: Object.keys(tDetails).map((x) => tDetails[x]),
          fill: false,
          backgroundColor: '#00ABCC',
          borderColor: '#00ABCC',
          barThickness: 45,
          maxBarThickness: 45
        }
      ];
      setTransactionData({
        labels: _d,
        datasets
      });
    }
  }, [clientListsState.clientListsAction.transationDetails]);

  const purchaseStatusChartData = {
    labels: transationPropertyTypes,
    options: [{ position: 'bottom' }],
    datasets: [
      {
        label: 'Total Clients',
        data: transationPropertyTypesValue,
        backgroundColor: ['#00ABCC', '#BED643', '#ff7d24cc', '#FFC734', '#402795', '#F93030', '#2bff00'],
        borderWidth: 1
      }
    ]
  };

  useEffect(() => {
    return () => {
      dispatch(resetLoginState());
    };
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={clientListsState.loading || clientListsState.loadingClientsCount}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {authReducer?.loginData?.user?.accessLevel !== 'USER' ? (
        <div className="dashboard-section ">
          <div className="dashboard-top-bar content-section">
            <div className="top-left">
              <h2>Dashboard </h2>
            </div>
            <div className="top-right">
              <div className="calender-box">
                <DatePicker
                  dateFormat="MMMM yyyy"
                  showMonthYearPicker
                  selected={selectedDate}
                  onChange={(date) => setSelectedDate(date)}
                  placeholderText="Please Select Date"
                />
              </div>
              <Button
                type="button"
                variant="contained"
                color="primary"
                onClick={() => downloadPdfFromHtmlElement('#dashboard-sections')}
              >
                Export
              </Button>
            </div>
          </div>
          <div className="dashboard-content content-section" id="dashboard-sections">
            <div className="grid-holder">
              <Grid container spacing={2}>
                <Grid item xs={12} lg={4}>
                  <div className="dashboard-box bg-primary h-100">
                    <div className="box-heading">
                      <span className="title">Total Clients</span>
                      <span className="number">
                        {clientListsState && clientListsState.clientListsAction?.grandValue?.grandTotal}
                      </span>
                    </div>
                    <div className="chart-holder">
                      <TotalClientChart data={totalClientChartData} />
                    </div>
                    <div className="detail-holder total-client-holder">
                      <div className="box-detail">
                        <span className="title client">Clients</span>
                        <span className="number">
                          {clientListsState && clientListsState.clientListsAction?.grandValue?.grandTotalClient}
                        </span>
                      </div>
                      <div className="box-detail">
                        <span className="title prospect">Prospect</span>
                        <span className="number">
                          {clientListsState && clientListsState.clientListsAction?.grandValue?.grandTotalProspect}
                        </span>
                      </div>
                      <div className="box-detail">
                        <span className="title others">Others</span>
                        <span className="number">
                          {clientListsState && clientListsState.clientListsAction?.grandValue?.grandTotalOthers}
                        </span>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} lg={8}>
                  <Grid container spacing={1}>
                    <Grid item xs={12} sm={6}>
                      <div className="dashboard-box">
                        <div className="box-heading">
                          <span className="title">New Clients</span>
                          <span className="number">
                            {clientListsState &&
                              clientListsState.clientListsAction.totalClient +
                                clientListsState.clientListsAction.totalProspect +
                                clientListsState.clientListsAction.totalOthers}
                          </span>
                        </div>
                        <div className="detail-holder">
                          <div className="box-detail">
                            <span className="title">Clients</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.totalClient}
                            </span>
                          </div>
                          <div className="box-detail">
                            <span className="title">Prospect</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.totalProspect}
                            </span>
                          </div>
                          <div className="box-detail">
                            <span className="title">Others</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.totalOthers}
                            </span>
                          </div>
                        </div>
                      </div>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <div className="dashboard-box">
                        <div className="box-heading">
                          <span className="title">Clients status</span>
                          <span className="number">
                            {clientListsState &&
                              clientListsState.clientListsAction.activeClient +
                                clientListsState.clientListsAction.inactiveClient +
                                clientListsState.clientListsAction.othersClient}
                          </span>
                        </div>
                        <div className="detail-holder">
                          <div className="box-detail">
                            <span className="title">Active</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.activeClient}
                            </span>
                          </div>
                          <div className="box-detail">
                            <span className="title">Inactive</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.inactiveClient}
                            </span>
                          </div>
                          <div className="box-detail">
                            <span className="title">Others</span>
                            <span className="number">
                              {clientListsState && clientListsState.clientListsAction.othersClient}
                            </span>
                          </div>
                        </div>
                      </div>
                    </Grid>
                    <Grid item xs={12}>
                      <div className="dashboard-box two-col-box">
                        <div className="left-col">
                          <div className="box-heading">
                            <span className="title">Purchase status</span>
                            <span className="number">
                              {clientListsState &&
                                clientListsState.clientListsAction.totalRawLead +
                                  clientListsState.clientListsAction.totalQualified +
                                  clientListsState.clientListsAction.totalNegotiating +
                                  clientListsState.clientListsAction.totalWin +
                                  clientListsState.clientListsAction.totalLost +
                                  clientListsState.clientListsAction.totalPropertyOthers +
                                  clientListsState.clientListsAction.totalFollowUp}
                            </span>
                          </div>
                          <div className="detail-holder">
                            <div className="box-detail">
                              <span className="title"> Raw Lead</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalRawLead}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Qualified</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalQualified}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Negotiating</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalNegotiating}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Win</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalWin}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Lost</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalLost}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Others</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalPropertyOthers}
                              </span>
                            </div>
                            <div className="box-detail">
                              <span className="title">Follow up in future</span>
                              <span className="number">
                                {clientListsState && clientListsState.clientListsAction.totalFollowUp}
                              </span>
                            </div>
                          </div>
                        </div>
                        <div className="right-col">
                          <div className="chart-holder">
                            <PurchaseStatusChart data={purchaseStatusChartData} />
                          </div>
                        </div>
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
            <div className="grid-holder">
              <Grid container spacing={3}>
                <Grid item xs={12} lg={6}>
                  <div className="dashboard-box h-100">
                    <div className="box-heading">
                      <span className="title">Assigned To Me</span>
                    </div>
                    <div className="listbox">
                      <div className="listbox-head">
                        <div className="left-col">
                          <span className="listbox-title">Task List</span>
                        </div>
                        <div className="right-col">
                          <span className="listbox-title">ETA</span>
                        </div>
                      </div>
                      <div className="listbox-body">
                        <ul className="list-holder">
                          {getFollowupNotes &&
                            getFollowupNotes.length &&
                            getFollowupNotes
                              .filter(
                                (x) =>
                                  x.assignedTo === authReducer?.loginData?.user?.email ||
                                  x.assignedTo ===
                                    authReducer?.loginData?.user?.firstName +
                                      ' ' +
                                      authReducer?.loginData?.user?.lastName
                              )
                              .map((x, idx) => (
                                <li key={idx} className={'list ' + getClassByTime(x.clientFollowUpDate)}>
                                  <div className="list-text">
                                    <Link to={`client-profile/update/${x.clientId}`}>{x.followUpNote}</Link>
                                  </div>
                                  <div className="badge">
                                    <time dateTime="08-15">
                                      {new Date(x.clientFollowUpDate)
                                        .toLocaleDateString('en-GB', {
                                          day: 'numeric',
                                          month: 'short'
                                        })
                                        .split(' ')
                                        .join(' ')}
                                    </time>
                                  </div>
                                </li>
                              ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} lg={6}>
                  <div className="dashboard-box h-100">
                    <div className="box-heading">
                      <span className="title">Follow up</span>
                    </div>
                    <div className="listbox">
                      <div className="listbox-head">
                        <div className="left-col">
                          <span className="listbox-title">Follow Up</span>
                        </div>
                        <div className="right-col">
                          <span className="listbox-title">ETA</span>
                        </div>
                      </div>
                      <div className="listbox-body">
                        <ul className="list-holder">
                          {getFollowupNotes &&
                            getFollowupNotes.length &&
                            getFollowupNotes.map((x, idx) => (
                              <li key={idx} className={'list ' + getClassByTime(x.clientFollowUpDate)}>
                                <div className="list-text">
                                  <Link style={{ marginBottom: '5px' }} to={`client-profile/update/${x.clientId}`}>
                                    {x.followUpNote}
                                  </Link>{' '}
                                  <br />
                                  <span className="assignedToIndashboardtxt">Assigned To: </span>
                                  <span className="assignedToIndashboard">{x.assignedTo}</span>
                                </div>
                                <div className="badge">
                                  <time dateTime="08-15">
                                    {new Date(x.clientFollowUpDate)
                                      .toLocaleDateString('en-GB', {
                                        day: 'numeric',
                                        month: 'short'
                                      })
                                      .split(' ')
                                      .join(' ')}
                                  </time>
                                </div>
                              </li>
                            ))}
                        </ul>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} lg={6}>
                  <div className="dashboard-box h-100">
                    <div className="box-heading">
                      <span className="title">Event</span>
                    </div>
                    <div className="listbox">
                      <div className="listbox-head">
                        <div className="left-col">
                          <span className="listbox-title">Event List</span>
                        </div>
                        <div className="right-col">
                          <span className="listbox-title">ETA</span>
                        </div>
                      </div>
                      <div className="listbox-body">
                        <ul className="list-holder" style={{ maxHeight: 240 }}>
                          {
                            // eventState &&
                            // eventState.length &&
                            // eventState.map((x, idx) => (
                            allEventList?.map((x, idx) => (
                              <li key={idx} className={'list ' + getClassByTime(x.start.dateTime)}>
                                <div className="list-text">
                                  {x?.summary || ''}
                                  {/* <Link to={`client-profile/update/${x.clientId}`}>{x.followUpNote}</Link> */}
                                </div>
                                <div className="badge">
                                  <time dateTime="08-15">
                                    {new Date(x.start.dateTime)
                                      .toLocaleDateString('en-GB', {
                                        day: 'numeric',
                                        month: 'short'
                                      })
                                      .split(' ')
                                      .join(' ')}
                                  </time>
                                </div>
                              </li>
                            ))
                          }
                        </ul>
                      </div>
                    </div>
                  </div>
                </Grid>
                <Grid item xs={12} lg={6}>
                  <div className="dashboard-box h-100 bg-primary">
                    <div className="box-heading">
                      <span className="title">Property Transaction Range</span>
                    </div>
                    <div className="graph-holder">
                      <PropertyTransactionChart data={transationData} />
                    </div>
                    <div className="denote">
                      <span className="denote-text">Amount of transaction</span>
                    </div>
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      ) : (
        <div className="content-section dashboard-alert-text">
          <div className="text-holder">
            <div className="icon-holder">
              <ErrorOutlineIcon />
            </div>
            <h4>Sorry! you don't have access to view the dashboard page. </h4>
            <p>
              Kindly visit the <Link to="/client-profile/view">client details</Link> page. If you required the access,
              please contact <a href="mailto:info@arsgroup.com.au">administrator</a>
            </p>
          </div>
        </div>
      )}
    </ThemeProvider>
  );
};

export default DashboardPage;
