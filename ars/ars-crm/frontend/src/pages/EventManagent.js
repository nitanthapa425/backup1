import React, { useEffect, useState } from 'react';
import { ThemeProvider } from '@material-ui/core';
import theme from '../../src/styles/theme';
import Scheduler, { Resource } from 'devextreme-react/scheduler';
import CustomStore from 'devextreme/data/custom_store';
import 'whatwg-fetch';
import { useDispatch, useSelector } from 'react-redux';
import { Backdrop, CircularProgress } from '@material-ui/core';
import { getCrmUsers } from '../store/actions/userAction';
import { addEventAction, getEventListAction, deleteEvent, clearDeleteState, updateEvent } from '../store/actions/eventAction';
import Message from '../components/message/Message';

const EventManagement = ({ auth }) => {
  const dispatch = useDispatch();
  const [emailArray, setEmailArray] = useState([]);
  const [userList, setUserList] = useState([]);

  const eventState = useSelector((state) => {
    return state.eventReducer;
  });

  const userState = useSelector((state) => {
    return state.userReducer;
  });

  useEffect(() => {
    dispatch(getEventListAction());
    dispatch(getCrmUsers());

    return () => {
      dispatch(clearDeleteState());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (
      typeof userState !== 'undefined' &&
      typeof userState.userList.results !== 'undefined' &&
      userState.userList !== []
    ) {
      const tempUserList = userState.userList.results.map((res) => ({
        id: res.email,
        text: res.email,
        color: res.email === auth?.email ? '#FF7D24' : '#18B2D0'
      }));
      const uniqueUserList = tempUserList.filter(
        (user, idx, self) => self.map((itm) => itm.id).indexOf(user.id) === idx
      );
      setUserList(uniqueUserList);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(userState.userList), auth?.email]);

  const dataSource = new CustomStore({
    load: () => {
      const events = eventState?.getEventList?.events?.map((e) => ({
        ...e,
        startDate: e.start.dateTime,
        endDate: e.end.dateTime,
        attendees: e.attendees?.map((attendee) => attendee.email)
      }));
      return events;
    },
    insert: (values) => {
      if (typeof values.attendees !== 'undefined') {
        values.attendees.forEach((element) => {
          const filterData = userList.filter((res) => res.id === element);
          emailArray.push({
            email: filterData[0].text
          });
        });
      }

      const eventValue = {
        summary: values.summary,
        description: values.description,
        start: {
          dateTime: values.startDate
        },
        end: {
          dateTime: values.endDate
        },
        attendees: emailArray
      };
      dispatch(addEventAction(eventValue, setEmailArray));
    },
    remove: (event) => {
      dispatch(deleteEvent(event?.id));
    },
    update: (_, newValues) => {
      if (newValues.attendees) {
        newValues.attendees = newValues.attendees.map(email => ({ email }))
      }
      newValues.start = {
        dateTime: newValues.startDate
      }
      newValues.end = {
        dateTime: newValues.endDate
      }
      dispatch(updateEvent(newValues?.id, newValues));
    },
  });

  useEffect(() => {
    if (eventState.deleteSuccessMsg || eventState.updateSuccessMsg) {
      dispatch(getEventListAction());
    }
  }, [dispatch, eventState.deleteSuccessMsg, eventState.updateSuccessMsg]);

  const views = ['day', 'workWeek', 'month'];
  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={eventState.loading || eventState.updatingEvent}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message
        status={eventState.status}
        severity={eventState.severity}
        message={
          eventState.deleteSuccessMsg || eventState.deleteErrorMsg ||
          eventState.updateSuccessMsg || eventState.updateErrorMsg
        }
      />

      <div className="content-section events-wrapper">
        <Scheduler
          dataSource={dataSource}
          views={views}
          defaultCurrentView="month"
          height={500}
          editing={auth.accessLevel === 'SUPER_ADMIN' || auth.accessLevel === 'ADMIN' ? true : false}
          showAllDayPanel={true}
          textExpr="summary"
          className="my-schedular"
        >
          <Resource
            label="Attendees"
            fieldExpr="attendees"
            dataSource={userList}
            allowMultiple={true}
            useColorAsDefault={true}
          />
        </Scheduler>
      </div>
    </ThemeProvider>
  );
};

export default EventManagement;
