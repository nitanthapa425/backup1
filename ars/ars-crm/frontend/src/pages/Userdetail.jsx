import { Backdrop, CircularProgress, ThemeProvider } from '@material-ui/core';
import theme from '../../src/styles/theme';
import Grid from '@material-ui/core/Grid';
import AvatarImageCropper from 'react-avatar-image-cropper';
import PersonIcon from '@material-ui/icons/Person';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import Button from '@material-ui/core/Button';
import AvatarImage from '../../src/assets/images/user.JPG';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getCrmUser, resetUploadStatus, uploadUserProfilePicture } from '../store/actions/userAction';
import Message from '../components/message/Message';

// import CameraIcon from '../assets/camera.svg';

const UserDetail = () => {
  const dispatch = useDispatch();

  const [showUpload, setShowUpload] = useState(false);

  const userReducer = useSelector((state) => state.userReducer);
  const { user } = userReducer;
  const { id } = useParams();

  useEffect(() => {
    dispatch(getCrmUser(id));
    return () => dispatch(resetUploadStatus());
  }, [dispatch, id]);

  const history = useHistory();

  const editProfile = () => {
    history.push(`/update-user/${user._id}`);
  };

  const uploadImage = (fileBlob) => {
    // create new file from BLOB
    const imgFile = new File([fileBlob], `pic_${Date.now()}.png`, {
      lastModified: new Date().getTime(),
      type: 'image/png'
    });
    const formData = new FormData();
    formData.append('files', imgFile);
    dispatch(uploadUserProfilePicture(formData));
    setShowUpload(false);
  };

  const IMAGE_URL = user.imageUploadPath
    ? `${process.env.REACT_APP_FILE_ACCESS_URL}/{{${user.imageUploadPath.split('\\{{')[1]}`
    : AvatarImage;

  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={userReducer.loading || userReducer.uploadingPicture}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message
        status={userReducer.status}
        severity={userReducer.severity}
        message={userReducer.uploadError || userReducer.uploadSuccess}
      />
      <div className="client-detail-wrapper content-section">
        <div className="top-bar">
          <div className="name-status">
            <h3>
              {user.firstName} {user.middleName} {user.lastName}
            </h3>
          </div>
          <div className="edit-btn">
            <Button variant="contained" color="primary" onClick={editProfile}>
              Edit Profile
            </Button>
          </div>
        </div>
        <Grid container spacing={3}>
          <Grid item xs={12} lg={4}>
            <div className="client-personal-detail">
              <div className="top-col">
                {/* <div className="image-holder">
                  <Button className="upload">
                    <img src={CameraIcon} alt="Upload" />
                  </Button>
                  <img src={AvatarImage} alt="" />
                </div> */}
                <div
                  className="avatar-holder"
                  style={{
                    backgroundImage: `url(${IMAGE_URL})`
                  }}
                  onClick={() => setShowUpload(true)}
                >
                  <AvatarImageCropper
                    apply={uploadImage}
                    isBack={true}
                    className={`avatarImageCropper ${showUpload ? 'active' : ''}`}
                    cancel={(val) => {
                      setShowUpload(false);
                    }}
                  />
                </div>

                <h4 className="username" style={{ textTransform: 'lowercase' }}>
                  {user.userName}
                </h4>
              </div>
              <div className="bottom-col">
                {user.phone ? (
                  <div className="contact-info">
                    <div className="icon-holder">
                      <PhoneIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Mobile no:</strong>
                        {user.phone}
                      </span>
                    </div>
                  </div>
                ) : null}
                {user.residential?.street ||
                user.residential?.state ||
                user.residential?.suburb ||
                user.residential?.state ? (
                  <div className="contact-info">
                    <div className="icon-holder">
                      <LocationOnIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Mailing address:</strong>
                        <address>
                          {user.residential.street ? user.residential.street + ',' : null}
                          {user.residential.streetAddress ? user.residential.streetAddress + ',' : null}
                          {user.residential.suburb ? user.residential.suburb + ',' : null}
                          {user.residential.state ? user.residential.state : null}
                        </address>
                      </span>
                    </div>
                  </div>
                ) : null}
                <div className="contact-info">
                  <div className="icon-holder email-icon">
                    <EmailIcon />
                  </div>
                  <div className="info-holder">
                    <span>
                      <strong>Email: </strong> {user.email}
                    </span>
                  </div>
                </div>
                {user.maritalStatus ? (
                  <div className="contact-info">
                    <div className="icon-holder email-icon">
                      <PersonIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Marital Status: </strong> {user.maritalStatus}
                      </span>
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </Grid>
          <Grid item xs={12} lg={8}>
            <div className="user-other-details">
              {user.rolePosition ? (
                <div className="details">
                  <span style={{ textTransform: 'capitalize' }}>
                    <strong>Role/position: </strong>
                    {user?.rolePosition?.split('_').join(' ').toLowerCase()}
                  </span>
                </div>
              ) : null}
              <div className="details">
                <span style={{ textTransform: 'capitalize' }}>
                  <strong>Access Level:</strong> {user?.accessLevel?.split('_').join(' ').toLowerCase()}
                </span>
              </div>
            </div>
            <div className="emergency-contact">
              <h4>Emergency Contact Person:</h4>
              {user.contactPerson?.emergencyContactPersonFirstName ||
              user.contactPerson?.emergencyContactPersonMiddleName ||
              user.contactPerson?.emergencyContactPersonLastName ||
              user.contactPerson?.emergencyContactPersonMobileNo ||
              user.emergencyResidential?.emergencyStreet ||
              user.emergencyResidential?.emergencyStreetAddress ||
              user.emergencyResidential?.emergencySuburb ||
              user.emergencyResidential?.emergencyState ||
              user.contactPerson?.emergencyContactPersonEmail ||
              user.contactPerson?.emergencyContactPersonRelation ? (
                <>
                  <div className="contact-info">
                    <div className="icon-holder user-icon">
                      <PersonIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Name: </strong>{' '}
                        {user.contactPerson?.emergencyContactPersonFirstName ||
                        user.contactPerson?.emergencyContactPersonMiddleName ||
                        user.contactPerson?.emergencyContactPersonLastName ? (
                          <>
                            {user.contactPerson.emergencyContactPersonFirstName
                              ? user.contactPerson.emergencyContactPersonFirstName
                              : null}{' '}
                            {user.contactPerson.emergencyContactPersonMiddleName
                              ? user.contactPerson.emergencyContactPersonMiddleName
                              : null}{' '}
                            {user.contactPerson.emergencyContactPersonLastName
                              ? user.contactPerson.emergencyContactPersonLastName
                              : null}
                          </>
                        ) : (
                          'No record found'
                        )}
                      </span>
                    </div>
                  </div>

                  <div className="contact-info">
                    <div className="icon-holder">
                      <PhoneIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Contact no:</strong>{' '}
                        {user.contactPerson?.emergencyContactPersonMobileNo
                          ? user.contactPerson?.emergencyContactPersonMobileNo
                          : 'No record found'}
                      </span>
                    </div>
                  </div>

                  <div className="contact-info">
                    <div className="icon-holder">
                      <LocationOnIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Mailing address:</strong>
                        <address>
                          {' '}
                          {user.emergencyResidential?.emergencyStreet ||
                          user.emergencyResidential?.emergencyStreetAddress ||
                          user.emergencyResidential?.emergencySuburb ||
                          user.emergencyResidential?.emergencyState ? (
                            <>
                              {user.emergencyResidential.emergencyStreet
                                ? user.emergencyResidential.emergencyStreet + ','
                                : null}
                              {user.emergencyResidential.emergencyStreetAddress
                                ? user.emergencyResidential.emergencyStreetAddress + ','
                                : null}
                              {user.emergencyResidential.emergencySuburb
                                ? user.emergencyResidential.emergencySuburb + ','
                                : null}
                              {user.emergencyResidential.emergencyState
                                ? user.emergencyResidential.emergencyState
                                : null}
                            </>
                          ) : (
                            'No record found'
                          )}
                        </address>
                      </span>
                    </div>
                  </div>

                  <div className="contact-info">
                    <div className="icon-holder email-icon">
                      <EmailIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Email: </strong>
                        {user.contactPerson?.emergencyContactPersonEmail
                          ? user.contactPerson?.emergencyContactPersonEmail
                          : 'No record found'}
                      </span>
                    </div>
                  </div>

                  <div className="contact-info">
                    <div className="icon-holder email-icon">
                      <PersonIcon />
                    </div>
                    <div className="info-holder">
                      <span>
                        <strong>Relation to user: </strong>
                        {user.contactPerson?.emergencyContactPersonRelation
                          ? user.contactPerson.emergencyContactPersonRelation
                          : 'No record found'}
                      </span>
                    </div>
                  </div>
                </>
              ) : (
                <div>No record found</div>
              )}
            </div>
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
};
export default UserDetail;
