import ThemeProvider from '@material-ui/styles/ThemeProvider';
import React, { useEffect, useState } from 'react';
import theme from '../../src/styles/theme';
import {
  Button,
  TextField,
  Grid,
  TextareaAutosize,
  Checkbox
  // InputLabel,
  // Backdrop,
  // CircularProgress
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { useDispatch, useSelector } from 'react-redux';
import { clientListsAction } from '../store/actions/clientAction';
import * as Yup from 'yup';
import {
  Field,
  Form,
  Formik
  // useFormik, useFormikContext
} from 'formik';
import { createCategoryAction, resetCategoryState } from '../store/actions/categoryAction';
import { RequiredArray, RequiredField } from '../components/validations/YupValidations';
// import { useHistory } from 'react-router-dom';
import Message from '../components/message/Message';
import { capitilized } from '../components/common/CapitalizedWord';
import AreYouSurePopUp from '../components/dropdown/AreYouSure';

// import Message from '../message/Message';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const CreateCategory = () => {
  // const history = useHistory();
  const dispatch = useDispatch();
  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });
  const { userdata } = clientLists;
  // const { results } = userdata;
  const categoryReducer = useSelector((state) => {
    return state.categoryReducer;
  });
  const { errorMessage } = categoryReducer;
  let [states, setStates] = useState([]);
  let [checkValue, setCheckValue] = useState([]);

  useEffect(() => {
    if (userdata && userdata.results) {
      const _states = [];
      userdata.results.forEach((result, i) => {
        // eslint-disable-next-line no-unused-expressions
        result.email ? _states.push({ email: result.email }) : null;
      });
      setStates(_states);
    }
  }, [userdata]);

  useEffect(() => {
    dispatch(clientListsAction());
  }, [dispatch]);

  const initialValues = {
    categoryname: '',
    description: '',
    emailList: []
  };

  const onSubmit = (values, onSubmitProps, history) => {
    const categorydata = {
      categoryName: capitilized(values.categoryname),
      categoryDescription: values.description,
      emailList: values.emailList
    };
    dispatch(createCategoryAction(categorydata, onSubmitProps, history, setCheckValue));
    onSubmitProps.setSubmitting(false);
  };

  const validationSchema = Yup.object({
    categoryname: RequiredField('Category Name'),
    emailList: RequiredArray('At least a client email')
  });

  useEffect(() => {
    return () => {
      dispatch(resetCategoryState());
    };
  }, [dispatch]);
  return (
    <ThemeProvider theme={theme}>
      <Message status={categoryReducer.status} severity={categoryReducer.severity} message={categoryReducer.message} />
      <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
        {(formik) => {
          const { values, isSubmitting } = formik;
          return (
            <div className="content-section email-management-wrapper">
              <h2>Create Category </h2>
              <Form className="add-category-form">
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <Grid item xs={12} md={6} lg={4}>
                      <Field name="categoryname">
                        {(props) => {
                          const {
                            field,
                            // form,
                            meta
                          } = props;
                          return (
                            <div>
                              <TextField
                                type="text"
                                label={
                                  <>
                                    Category <span className="color-red">*</span>
                                  </>
                                }
                                {...field}
                                error={
                                  meta.touched && meta.error
                                    ? true
                                    : errorMessage ===
                                      `Category name ${capitilized(values.categoryname)} already exists`
                                    ? true
                                    : false
                                }
                                helperText={
                                  meta.touched && meta.error ? (
                                    <div>{meta.error}</div>
                                  ) : errorMessage ===
                                    `Category name ${capitilized(values.categoryname)} already exists` ? (
                                    <div>{errorMessage}</div>
                                  ) : null
                                }
                              />
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Grid item xs={12} lg={8}>
                      <Field name="description">
                        {(props) => {
                          const { field, meta } = props;
                          return (
                            <div>
                              <span variant="subtitle1" className="textarea-label">
                                Description
                              </span>
                              <TextareaAutosize
                                rowsMin={4}
                                rowsMax={4}
                                {...field}
                                error={meta.touched && meta.error ? true : false}
                                helperText={meta.touched && meta.error ? <div>{meta.error}</div> : null}
                              />
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <Grid item xs={12} lg={8}>
                      <Field name="emailList">
                        {(props) => {
                          const { form, meta } = props;
                          const { setFieldValue } = form;

                          return (
                            <div>
                              <Autocomplete
                                // {...field}

                                name="emailList"
                                onChange={(_, value) => {
                                  setFieldValue('emailList', value);
                                  setCheckValue(value);
                                }}
                                multiple
                                id="checkboxes-tags"
                                options={states}
                                disableCloseOnSelect
                                className="email-list-autocomplete"
                                getOptionLabel={(option) => option.email}
                                // name="emailList"
                                value={checkValue}
                                renderOption={(option, { selected }) => (
                                  <React.Fragment>
                                    <Checkbox
                                      icon={icon}
                                      checkedIcon={checkedIcon}
                                      style={{ marginRight: 8 }}
                                      checked={selected}
                                      colorPrimary
                                    />
                                    {option.email}
                                  </React.Fragment>
                                )}
                                renderInput={(params) => (
                                  <TextField
                                    {...params}
                                    // {...field}

                                    // label="Add Client"
                                    label={
                                      <>
                                        Add Client <span style={{ color: 'red' }}>*</span>
                                      </>
                                    }
                                    error={meta.touched && meta.error ? true : false}
                                    helperText={meta.touched && meta.error ? <div>{meta.error}</div> : null}
                                  />
                                )}
                              />
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                  <Grid item xs={12}>
                    <div className="form-row category-submit-button">
                      <Button type="submit" variant="contained" color="primary" disabled={isSubmitting}>
                        Create
                      </Button>

                      <div className="cancel-button-holder">
                        <AreYouSurePopUp
                          variant="outlined"
                          buttonName="Clear"
                          popUpHeading="Are you sure you want to clear everything"
                          popUpDetail="
                          Are you sure you want to clear everything?"
                          formName="myform"
                          buttonType="reset"
                          className="cancel-button"
                          onAgreeClick={() => {
                            formik.handleReset();
                            setCheckValue([]);
                          }}
                        />
                      </div>
                    </div>
                  </Grid>
                </Grid>
              </Form>
            </div>
          );
        }}
      </Formik>
    </ThemeProvider>
  );
};

export default CreateCategory;
