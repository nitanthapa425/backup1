import React, { useEffect } from 'react';
import { ThemeProvider } from '@material-ui/core';
import theme from '../../src/styles/theme';
import Scheduler from 'devextreme-react/scheduler';
import CustomStore from 'devextreme/data/custom_store';
import 'whatwg-fetch';
import { useDispatch, useSelector } from 'react-redux';
import { Backdrop, CircularProgress } from '@material-ui/core';
import {
  addEventAction,
  getSelfEventListAction,
  deleteEvent,
  clearDeleteState,
  updateEvent,
  clearUpdateState
} from '../store/actions/eventAction';
import Message from '../components/message/Message';

const SelfEventManagement = ({ auth }) => {
  const dispatch = useDispatch();

  const views = ['day', 'workWeek', 'month'];

  useEffect(() => {
    if (auth.email) {
      dispatch(getSelfEventListAction(auth.email));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth.email]);

  const eventState = useSelector((state) => {
    return state.eventReducer;
  });

  const dataSource = new CustomStore({
    load: () => {
      const events = eventState?.getSelfEventList?.map((e) => ({
        ...e,
        startDate: e.start.dateTime,
        endDate: e.end.dateTime,
        attendees: e.attendees?.map((attendee) => attendee.email)
      }));
      return events;
    },
    insert: (values) => {
      const eventValue = {
        summary: values.summary,
        description: values.description,
        start: {
          dateTime: values.startDate
        },
        end: {
          dateTime: values.endDate
        },
        attendees: [
          {
            email: auth.email
          }
        ]
      };
      dispatch(addEventAction(eventValue, null));
    },
    remove: (event) => {
      dispatch(deleteEvent(event?.id));
    },
    update: (_, newValues) => {
      if (newValues.attendees) {
        newValues.attendees = newValues.attendees.map((email) => ({ email }));
      }
      newValues.start = {
        dateTime: newValues.startDate
      };
      newValues.end = {
        dateTime: newValues.endDate
      };

      dispatch(updateEvent(newValues?.id, newValues));
    }
  });

  useEffect(() => {
    if (auth.email || eventState.deleteSuccessMsg || eventState.updateSuccessMsg) {
      dispatch(getSelfEventListAction(auth.email));
    }
  }, [dispatch, eventState.deleteSuccessMsg, auth.email, eventState.updateSuccessMsg]);

  useEffect(() => {
    return () => {
      dispatch(clearDeleteState());
      dispatch(clearUpdateState());
    };
  }, [dispatch]);
  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={eventState.loading || eventState.updatingEvent}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message
        status={eventState.status}
        severity={eventState.severity}
        message={
          eventState.deleteSuccessMsg ||
          eventState.deleteErrorMsg ||
          eventState.updateSuccessMsg ||
          eventState.updateErrorMsg
        }
      />
      <div className="content-section events-wrapper selfEvent">
        <Scheduler
          dataSource={dataSource}
          views={views}
          defaultCurrentView="month"
          height={500}
          editing={true}
          showAllDayPanel={true}
          // startDayHour={7}
          textExpr="summary"
          className="my-schedular"
          // startDateExpr="start.dateTime"
          // endDateExpr="end.dateTime"
          // timeZone="Asia/Kathmandu"
        ></Scheduler>
      </div>
      /
    </ThemeProvider>
  );
};

export default SelfEventManagement;
