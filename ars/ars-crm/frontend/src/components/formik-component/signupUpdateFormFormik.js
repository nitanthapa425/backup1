import { AlphabetsValidation, AlphabetsWithoutRequiredValidation, EmailValidation, NumberWithoutRequiredValidation, PhoneValidation, RequiredValidation, UserNameValidation } from "../validations/YupValidations";
import * as yup from 'yup';
import { changeToSmallCase } from "../common/CleanInputData";
import { capitilized } from '../common/CapitalizedWord'
export const signupUpdateInitialValues=(user )=>{
  return(
    {
      userName: user?.userName ? user.userName : '',
      title: user?.title ? user.title : '',
      firstName: user?.firstName ? user.firstName : '',
      middleName: user?.middleName ? user.middleName : '',
      lastName: user?.lastName ? user.lastName : '',
      email: user?.email ? user.email : '',
      phoneNumber: user?.phone ? user.phone : '',
      rolePosition: user?.rolePosition ? user.rolePosition : '',
      userRole: user?.accessLevel ? user.accessLevel : '',
      residentialStreet: user?.residential?.street ? user.residential.street : '',
      residentialState: user?.residential?.state ? user.residential.state : '',
      residentialCountry: user?.residential?.country ? user.residential.country : '',
      residentialZip: user?.residential?.zip ? user.residential.zip : '',
      residentialStreetAddress: user?.residential?.streetAddress ? user.residential.streetAddress : '',
      residentialSuburb: user?.residential?.suburb ? user.residential.suburb : '',
      emergencyContactPersonTitle: user?.contactPerson?.emergencyContactPersonTitle
        ? user.contactPerson.emergencyContactPersonTitle
        : '',
      emergencyContactPersonFirstName: user?.contactPerson?.emergencyContactPersonFirstName
        ? user.contactPerson.emergencyContactPersonFirstName
        : '',
      emergencyContactPersonMiddleName: user?.contactPerson?.emergencyContactPersonMiddleName
        ? user.contactPerson.emergencyContactPersonMiddleName
        : '',
      emergencyContactPersonLastName: user?.contactPerson?.emergencyContactPersonLastName
        ? user.contactPerson.emergencyContactPersonLastName
        : '',
      emergencyResidentialStreet: user?.emergencyResidential?.emergencyStreet
        ? user.emergencyResidential.emergencyStreet
        : '',
      emergencyResidentialStreetAddress: user?.emergencyResidential?.emergencyStreetAddress
        ? user.emergencyResidential.emergencyStreetAddress
        : '',
      emergencyResidentialSuburb: user?.emergencyResidential?.emergencySuburb
        ? user.emergencyResidential.emergencySuburb
        : '',
      emergencyResidentialState: user?.emergencyResidential?.emergencyState
        ? user.emergencyResidential.emergencyState
        : '',
      emergencyResidentialCountry: user?.emergencyResidential?.emergencyCountry
        ? user.emergencyResidential.emergencyCountry
        : '',
      emergencyResidentialZip: user?.emergencyResidential?.emergencyZip ? user.emergencyResidential.emergencyZip : '',
      emergencyContactPersonMobileNo: user?.contactPerson?.emergencyContactPersonMobileNo
        ? user.contactPerson.emergencyContactPersonMobileNo
        : '',
      emergencyContactPersonPhone: user?.contactPerson?.emergencyContactPersonPhone
        ? user.contactPerson.emergencyContactPersonPhone
        : '',
      emergencyContactPersonEmail: user?.contactPerson?.emergencyContactPersonEmail
        ? user.contactPerson.emergencyContactPersonEmail
        : '',
      emergencyContactPersonRelation: user?.contactPerson?.emergencyContactPersonRelation
        ? user.contactPerson.emergencyContactPersonRelation
        : '',
      maritalStatus: user?.maritalStatus ? user.maritalStatus : ''
    }

  )
}



export const signupUpdateValidationSchema = yup.object({
    userName: UserNameValidation().lowercase().required('Username is required'),
    rolePosition: AlphabetsWithoutRequiredValidation(),
    userRole: RequiredValidation('Access Level'),
    title: yup.string(),
    firstName: AlphabetsValidation('First name'),
    middleName: AlphabetsWithoutRequiredValidation(),
    lastName: AlphabetsValidation('Last name'),
    residentialStreet: yup.string(),
    residentialZip: NumberWithoutRequiredValidation('Residential postal code'),
    residentialStreetAddress: yup.string(),
    residentialSuburb: yup.string(),
    residentialState: yup.string(),
    residentialCountry: AlphabetsWithoutRequiredValidation(),
    email: EmailValidation().required('Email is required'),
    phoneNumber: PhoneValidation(),
    emergencyContactPersonTitle: yup.string(),
    emergencyContactPersonFirstName: AlphabetsWithoutRequiredValidation('First name'),
    emergencyContactPersonMiddleName: AlphabetsWithoutRequiredValidation(),
    emergencyContactPersonLastName: AlphabetsWithoutRequiredValidation('Last name'),
    emergencyResidentialStreet: yup.string(),
    emergencyResidentialZip: NumberWithoutRequiredValidation('Emergency Contact Residential postal code'),
    emergencyResidentialStreetAddress: yup.string(),
    emergencyResidentialSuburb: yup.string(),
    maritalStatus: yup.string(),
    emergencyResidentialCountry: AlphabetsWithoutRequiredValidation(),
    emergencyContactPersonMobileNo: PhoneValidation(),
    emergencyContactPersonPhone: PhoneValidation(),
    emergencyContactPersonEmail: EmailValidation(),
    emergencyContactPersonRelation: AlphabetsWithoutRequiredValidation()
  });


  export const signupUpdateOnSubmit =(updateAction,userRoleid,setPristine,getCrmUser,history)=>(values, { resetForm }) => {
    const updateValues = {
      userName: changeToSmallCase(values.userName),
      title: capitilized(values.title),
      firstName: capitilized(values.firstName),
      middleName: capitilized(values.middleName),
      lastName: capitilized(values.lastName),
      email: changeToSmallCase(values.email),
      phone: values.phoneNumber,
      rolePosition: values.rolePosition,
      accessLevel: values.userRole,
      maritalStatus: values.maritalStatus,
      residential: {
        street: values.residentialStreet,
        streetAddress: values.residentialStreetAddress,
        state: values.residentialState,
        country: values.residentialCountry,
        zip: values.residentialZip,
        suburb: values.residentialSuburb
      },
      contactPerson: {
        emergencyContactPersonTitle: capitilized(values.emergencyContactPersonTitle),
        emergencyContactPersonFirstName: capitilized(values.emergencyContactPersonFirstName),
        emergencyContactPersonMiddleName: capitilized(values.emergencyContactPersonMiddleName),
        emergencyContactPersonLastName: capitilized(values.emergencyContactPersonLastName),
        emergencyContactPersonMobileNo: values.emergencyContactPersonMobileNo,
        emergencyContactPersonPhone: values.emergencyContactPersonPhone,
        emergencyContactPersonEmail: values.emergencyContactPersonEmail,
        emergencyContactPersonRelation: values.emergencyContactPersonRelation
      },
      emergencyResidential: {
        emergencyStreet: values.emergencyResidentialStreet,
        emergencyStreetAddress: values.emergencyResidentialStreetAddress,
        emergencyState: values.emergencyResidentialState,
        emergencyCountry: values.emergencyResidentialCountry,
        emergencyZip: values.emergencyResidentialZip,
        emergencySuburb: values.emergencyResidentialSuburb
      }
    };
    updateAction(updateValues, userRoleid, resetForm, setPristine,history).then(() => {
      getCrmUser(userRoleid);
    });
  }
  
  // userRole._id