import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { CSVLink } from 'react-csv';
import Button from '@material-ui/core/Button';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const Export = ({ currentData, csvLinkRef, downloadCSV, downloadPDF }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div style={{ display: 'inline-block' }}>
      <CSVLink
        filename={`Ganesh_client_list_${new Date(Date.now()).toLocaleDateString()}.csv`}
        ref={csvLinkRef}
        target="_blank"
        data={currentData}
        // data={data}
        // headers={headers}
      />
      <div className="export-btn-holder">
        <Button
          endIcon={<ExpandMoreIcon />}
          aria-controls="simple-menu"
          variant="contained"
          color="primary"
          aria-haspopup="true"
          onClick={handleClick}
          className="export-btn"
        >
          Export
        </Button>
      </div>

      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{ width: '12rem', marginTop: '3.5rem' }}
      >
        <MenuItem
          onClick={() => {
            downloadCSV();
            handleClose();
          }}
        >
          Export to CSV
        </MenuItem>
        <MenuItem
          onClick={() => {
            downloadPDF();
            handleClose();
          }}
        >
          Export to PDF
        </MenuItem>
      </Menu>
    </div>
  );
};

export default Export;
