export const exportData = (headerGroups, page) => {
  let allData = [];

  if (page) {
    // const headers = headerGroups[0].headers.slice(2).map((header) => header.Header);
    const headers = [
      'Client Name',
      'Email',
      'State',
      'Mobile',
      'Lead Assigned To',
      'Follow Up Note',
      "Referrer's Name",
      "Referrer's Email",
      "Referrer's Phone",
      "Referrer's State",
      "Referrer's Postal Code",
      'Lead Open',
      'Lead Close',
      'Property Lot Address',
      'Price Range',
      'Property Type',
      'Purchase Status'
    ];
    const data = page.map((item) => {
      const row = item.original;
      row.transaction = row.transactionDetails[0] || {};
      row.clientFollowUpNote = row.clientFollowUpNotes[0] || {};
      const clientName = row.fullName || '-';
      const email = row.email || '-';
      const mailingState = row.mailingState || '-';
      const mobile = row.mobile || '-';
      const leadAssignedTo = row?.clientFollowUpNote?.assignedTo || '-';
      const followUpNote = row?.clientFollowUpNote?.followUpNote || '-';
      const referralName = row?.referrals?.name || '-';
      const referralEmail = row?.referrals?.email || '-';
      const referralPhone = row?.referrals?.phone || '-';
      const referralState = row?.referrals?.state || '-';
      const referralZip = row?.referrals?.zip || '-';
      const leadOpen = row?.referrals?.referralLeadOpen || '-';
      const leadClose = row?.referrals?.referralLeadClose || '-';
      const PropertyLotAddress = row?.transaction?.propertyLotAddress || '-';
      const priceRange = row?.transaction?.upTo || '-';
      const propertyType = row?.transaction?.propertyType || '-';
      const purchaseStatus = row?.transaction?.purchaseStatus || '-';
      return [
        clientName,
        email,
        mailingState,
        mobile,
        leadAssignedTo,
        followUpNote,
        referralName,
        referralEmail,
        referralPhone,
        referralState,
        referralZip,
        leadOpen,
        leadClose,
        PropertyLotAddress,
        priceRange,
        propertyType,
        purchaseStatus
      ];
    });
    // const data = page.map((item) => {
    //   return item.cells.slice(2).map((cell) => cell.value);
    // });

    // allData = [headers, ...data];
    allData = [headers, ...data];
  }

  return allData || [];
};

export const prepareJsonData = (page) => {
  let headers = [];
  let data = [];

  if (page) {
    headers = [
      { label: 'Client Name', key: 'fullName' },
      { label: 'Email', key: 'email' },
      { label: 'State', key: 'mailingState' },
      { label: 'Mobile', key: 'mobile' },
      { label: 'Lead Assigned To', key: 'clientFollowUpNote.assignedTo' },
      { label: 'Follow Up Note', key: 'clientFollowUpNote.followUpNote' },
      { label: "Referrer's Name", key: 'referrals.name' },
      { label: "Referrer's Email", key: 'referrals.email' },
      { label: "Referrer's Phone", key: 'referrals.phone' },
      { label: "Referrer's State", key: 'referrals.state' },
      { label: "Referrer's Postal Code", key: 'referrals.zip' },
      { label: 'Lead Open', key: 'referrals.referralLeadOpen' },
      { label: 'Lead Close', key: 'referrals.referralLeadClose' },
      { label: 'Property Lot Address', key: 'transaction.propertyLotAddress' },
      { label: 'Price Range', key: 'transaction.upTo' },
      { label: 'Property Type', key: 'transaction.propertyType' },
      { label: 'Purchase Status', key: 'transaction.purchaseStatus' }
    ];

    data = page.map((item) => {
      let row = item.original;
      row.transaction = row.transactionDetails[0] || {};
      row.clientFollowUpNote = row.clientFollowUpNotes[0] || {};
      return row;
    });
  }

  return { headers, data };
};
