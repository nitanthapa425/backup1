import React, { Fragment, useState, useEffect, useRef } from 'react';
import { useTable, usePagination, useSortBy, useFilters, useExpanded, useRowSelect } from 'react-table';
// import { ThemeProvider } from '@material-ui/core/styles';
// import Table from "react-bootstrap/Table";
import { DefaultColumnFilter, Filter } from './Filter';
import useDebounce from '../common/use-debounce';
import './style.css';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
// import Pagination from 'react-bootstrap/Pagination';
import { Form } from 'react-bootstrap';
// import { BsArrowDownShort, BsArrowUpShort } from 'react-icons/all';

import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { Link, useHistory } from 'react-router-dom';
import { 
  // clientListsAction, searchUsers,
  deleteClientDetailsAction,
  resetStatus
} from '../../store/actions/clientAction';
import { useDispatch, useSelector } from 'react-redux';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import MaterialTableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import MatPagination from '@material-ui/lab/Pagination';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { Dialog, DialogContentText, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import Message from '../message/Message';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { AuthRole } from '../common/AuthRole';
// import Menu from '@material-ui/core/Menu';
// import MenuItem from '@material-ui/core/MenuItem';
// import FilterListIcon from '@material-ui/icons/FilterList';

// import Pagination from '@material-ui/core/TablePagination'
import './TableContainer.scss';
import { exportData } from './clientsData';
import Export from './Export';

export const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = React.useRef();
  const resolvedRef = ref || defaultRef;
  React.useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate;
  }, [resolvedRef, indeterminate]);

  return (
    <>
      <input type="checkbox" ref={resolvedRef} {...rest} />
    </>
  );
});

function TableContainer({
  columns,
  data,
  fetchData,
  loading,
  pageCount: controlledPageCount,
  totalData,
  renderRowSubComponent
}) {
  const history = useHistory();
  const dispatch = useDispatch();
  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });
  const Auth = AuthRole();
  let userRole = Auth.accessLevel;

  const [popUpopen, setpopUpopen] = React.useState(false);
  const handleClickOpen = () => {
    setpopUpopen(true);
  };
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  const handleClosePopUp = () => {
    setpopUpopen(false);
  };
  const popUpAgree = () => {
    selectedFlatRows.forEach((obj, index) => {
      dispatch(deleteClientDetailsAction(obj.original.id));
    });

    // setSelectedClient([]);

    // dispatch(deleteClientDetailsAction(selectedClient)).then(()=> {
    // setSnackbarSuccess(true);
    handleClosePopUp();
  };

  // const handleDelete = () => {
  //   selectedFlatRows.forEach((obj, index) => {
  //
  //     dispatch(deleteClientDetailsAction(obj.original._id));
  //   });
  // };

  const popUpDisagree = () => {
    handleClosePopUp();
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    visibleColumns,
    page,
    // canPreviousPage,

    // canNextPage,
    // pageOptions,
    // nextPage,
    // previousPage,
    pageCount,
    gotoPage,
    setPageSize,
    // Get the state from the instance
    state: { pageIndex, pageSize, sortBy, filters },
    selectedFlatRows
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 }, // Pass our hoisted table state
      manualPagination: true, // Tell the usePagination
      manualSortBy: true,
      manualFilters: true,

      autoResetSortBy: false,
      defaultColumn: { Filter: DefaultColumnFilter },
      // hook that we'll handle our own data fetching
      // This means we'll also have to provide our own
      // pageCount.
      pageCount: controlledPageCount
    },

    useFilters,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect,

    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        {
          id: 'selection',

          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
            </div>
          ),

          Cell: ({ row }) => (
            <div>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
            </div>
          )
        },
        ...columns
      ]);
    }
  );
  const debounceFilter = useDebounce(filters, 1000);

  const handleChange = (x, value) => {
    gotoPage(value - 1);
  };
  // Listen for changes in pagination and use the state to fetch our new data
  React.useEffect(() => {
    fetchData({ pageIndex, pageSize, sortBy, filters: debounceFilter });
  }, [fetchData, pageIndex, pageSize, sortBy, debounceFilter, clientLists]);
  // Render the UI for your table

  const handleUpdate = () => {
    history.push(`/client-profile/update/${selectedFlatRows[0].original.id}`);
  };
  const handleClientUpdate = () => {
    history.push(`/update-client-profile/${selectedFlatRows[0].original.id}`);
  };
  const [check] = useState(true);

  useEffect(() => {
    return () => {
      dispatch(resetStatus());
    };
  }, [dispatch]);

  const csvLink = useRef();

  const [allData, setAllData] = useState([]);

  useEffect(() => {
    setAllData(exportData(headerGroups, page));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  const downloadCSV = async () => {
    csvLink.current.link.click();
  };

  const downloadPDF = async () => {
    await setAllData(exportData(headerGroups, page));
    const doc = new jsPDF('p', 'mm', [612, 792]);
    doc.text('Clients Details', 15, 10);

    doc.autoTable({
      head: [allData[0]],
      body: [...allData.slice(1)],
      styles: {
        cellPadding: 1,
        fontSize: 10
      }
    });
    doc.save(`Ganesh_client_list_${new Date(Date.now()).toLocaleDateString()}.pdf`);
  };

  // const [anchorEl, setAnchorEl] = React.useState(null);

  // const handleClick = (event) => {
  //   setAnchorEl(event.currentTarget);
  // };

  // const handleClose = () => {
  //   setAnchorEl(null);
  // };

  return (
    <>
      <Backdrop open={clientLists.loading || loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message status={check && clientLists.status} severity={clientLists.severity} message={clientLists.message} />

      <div style={{ display: 'none' }}>
        <Dialog
          open={popUpopen}
          onClose={handleClosePopUp}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to delete this record? Please note that, once deleted, this cannot be undone and the
              record will be permanently removed.
            </DialogContentText>
          </DialogContent>
          <DialogActions className="alert-btn-holder">
            <Button onClick={popUpAgree} variant="contained" className="cancel-button">
              Delete
            </Button>
            <Button onClick={popUpDisagree} variant="outlined" color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {/* <h4 style={{ marginBottom: '10px' }}>Client List</h4> */}

      {selectedFlatRows?.length > 0 ? (
        <div className="table-top-header">
          <div className="tableHeader-left-col">
            <span className="selected">{selectedFlatRows?.length} selected</span>
          </div>
          <div className="tableHeader-right-col after-checked">
            <Export currentData={allData} csvLinkRef={csvLink} downloadCSV={downloadCSV} downloadPDF={downloadPDF} />
            {selectedFlatRows.length === 1 ? (
              userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' || userRole === 'USER' ? (
                <Button variant="contained" className="view-btn" onClick={handleUpdate}>
                  View
                </Button>
              ) : null
            ) : null}
            {selectedFlatRows.length === 1 ? (
              userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' ? (
                <Button variant="contained" className="edit-btn" onClick={handleClientUpdate}>
                  Edit
                </Button>
              ) : null
            ) : null}
            {selectedFlatRows.length >= 1 ? (
              userRole === 'SUPER_ADMIN' ? (
                <Button variant="contained" startIcon={<DeleteIcon />} className="delete-btn" onClick={handleClickOpen}>
                  Delete
                </Button>
              ) : null
            ) : null}
          </div>
        </div>
      ) : (
        <div className="table-top-header">
          <div className="tableHeader-left-col">
            <Link to="/client-profile/create">
              <Button startIcon={<AddIcon />}>Add</Button>
            </Link>
            {/* <form className="client-table-search">
              <div className="form-control">
                <TextField id="table-search" type="search" placeholder="Search" />
                <span class="icon-search"></span>
              </div>
            </form> */}
            {/* {hello world} */}
          </div>
          <div className="tableHeader-right-col">
            <Export currentData={allData} csvLinkRef={csvLink} downloadCSV={downloadCSV} downloadPDF={downloadPDF} />
          </div>
        </div>
      )}

      <Paper>
        <MaterialTableContainer>
          <Table hover={true} loading={loading} {...getTableProps()} className={loading ? 'atable-loading' : null}>
            <TableHead>
              {headerGroups.map((headerGroup) => (
                <TableRow {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <TableCell {...column.getHeaderProps()}>
                      <div
                        {...column.getSortByToggleProps()}
                        style={{ display: 'flex', justifyContent: 'space-between' }}
                      >
                        {column.render('Header')}
                        {/* Add a sort direction indicator */}
                        <span>
                          {column.isSorted ? column.isSortedDesc ? <ArrowDownwardIcon /> : <ArrowUpwardIcon /> : ''}
                        </span>
                      </div>
                      <Filter column={column} />
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody {...getTableBodyProps()}>
              {page?.length > 0 ? (
                page.map((row, i) => {
                  prepareRow(row);
                  const check = row.original;
                  const arrayLegth = check.transactionDetails.length - 1;
                  // console.log(check);
                  return (
                    <Fragment key={row.getRowProps().key}>
                      <TableRow>
                        {row.cells.map((cell) => {
                          return <TableCell {...cell.getCellProps()}>{cell.render('Cell')}</TableCell>;
                        })}
                      </TableRow>
                      {row.isExpanded && (
                        <TableRow>
                          <TableCell colSpan={visibleColumns.length}>
                            <div className="dropDownOtherDetails">
                              <div className="dropDownOtherDetails__Transaction">
                                <h2 className="dropDownOtherDetails__Transaction-heading">Latest Transaction</h2>
                                <Link
                                  to={`/client-profile/update/${check.id || check._id}`}
                                  className="transcation-button"
                                >
                                  See more
                                </Link>
                                <div className="dropDownOtherDetails__Transaction--top">
                                  <div className="dropDownOtherDetails__Transaction--top-1">
                                    Property Lot Address <br />
                                    {check.transactionDetails[arrayLegth]?.propertyLotAddress === '' ||
                                    check.transactionDetails[arrayLegth]?.propertyLotAddress === undefined
                                      ? 'No record found'
                                      : `${check.transactionDetails[arrayLegth]?.propertyLotAddress}`}
                                  </div>
                                  <div className="dropDownOtherDetails__Transaction--top-2">
                                    Price Range <br />
                                    {check.transactionDetails[arrayLegth]?.upTo === '' ||
                                    check.transactionDetails[arrayLegth]?.upTo === undefined ? (
                                      'No record found'
                                    ) : (
                                      <>${formatNumber(`${check.transactionDetails[arrayLegth]?.upTo}`)}</>
                                    )}
                                  </div>
                                  <div className="dropDownOtherDetails__Transaction--top-3">
                                    Property Type <br />
                                    {check.transactionDetails[arrayLegth]?.propertyType === '' ||
                                    check.transactionDetails[arrayLegth]?.propertyType === undefined
                                      ? 'No record found'
                                      : `${check.transactionDetails[arrayLegth]?.propertyType
                                          .replaceAll('"', '')
                                          .replaceAll('_', ' ')}`}
                                  </div>
                                  <div className="dropDownOtherDetails__Transaction--top-4">
                                    Purchase Status <br />
                                    {check.transactionDetails[arrayLegth]?.purchaseStatus === '' ||
                                    check.transactionDetails[arrayLegth]?.purchaseStatus === undefined
                                      ? 'No record found'
                                      : `${check.transactionDetails[arrayLegth].purchaseStatus
                                          .replaceAll('"', '')
                                          .replaceAll('_', ' ')}`}
                                  </div>
                                  <div className="dropDownOtherDetails__Transaction--top-5">
                                    Results Notes <br />
                                    {check.transactionDetails[arrayLegth]?.resultNotes[0]?.note === '' ||
                                    check.transactionDetails[arrayLegth]?.resultNotes[0]?.note === undefined
                                      ? 'No record found'
                                      : `${check.transactionDetails[arrayLegth]?.resultNotes[0]?.note}`}
                                  </div>
                                </div>
                              </div>
                              <div className="dropDownOtherDetails__referral">
                                <h2>Referrals</h2>
                                <div className="dropDownOtherDetails__referral--top">
                                  <div className="dropDownOtherDetails__referral--top-2">
                                    Name <br />
                                    {check.referrals.name === '' || check.referrals.name === undefined
                                      ? 'No record found'
                                      : `${check.referrals.name}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-2">
                                    Referralcode <br />
                                    {check.referrals.referralCode === '' || check.referrals.referralCode === undefined
                                      ? 'No record found'
                                      : `${check.referrals.referralCode}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-3">
                                    Email <br />
                                    {check.referrals.email === '' || check.referrals.email === undefined
                                      ? 'No record found'
                                      : `${check.referrals.email}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-4">
                                    Phone <br />
                                    {check.referrals.phone === '' || check.referrals.phone === undefined
                                      ? 'No record found'
                                      : `${check.referrals.phone}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-5">
                                    State <br />
                                    {check.referrals.state === '' || check.referrals.state === undefined
                                      ? 'No record found'
                                      : `${check.referrals.state}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-5">
                                    Street Address <br />
                                    {check.referrals.streetAddress === '' || check.referrals.streetAddress === undefined
                                      ? 'No record found'
                                      : `${check.referrals.streetAddress}`}
                                  </div>
                                  <div className="dropDownOtherDetails__referral--top-5">
                                    Zip <br />
                                    {check.referrals.zip === '' || check.referrals.zip === undefined
                                      ? 'No record found'
                                      : `${check.referrals.zip}`}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </TableCell>
                        </TableRow>
                      )}
                    </Fragment>
                  );
                })
              ) : loading ? null : (
                <span>No&nbsp;record&nbsp;found</span>
              )}
            </TableBody>
          </Table>
        </MaterialTableContainer>
      </Paper>

      <div className="pagination">
        <div className="total">
          <span>Total {totalData}</span>
        </div>
        <div className="pagination-counter">
          <Form.Group controlId="exampleForm.SelectCustom">
            <span className="pagination-title">Row Per Page</span>
            <Form.Control
              as="select"
              value={pageSize}
              onChange={(e) => {
                setPageSize(Number(e.target.value));
              }}
              custom
            >
              {[...new Set([5, 10, 20, 30, 40, 50, 100, totalData])]
                .filter((v, i) => v <= totalData)
                .map((pageSize) => (
                  <option key={pageSize} value={pageSize}>
                    {pageSize === totalData ? 'All' : pageSize}
                  </option>
                ))}
            </Form.Control>
          </Form.Group>
          <MatPagination count={pageCount} onChange={handleChange} />
        </div>
      </div>
    </>
  );
}

export default TableContainer;
