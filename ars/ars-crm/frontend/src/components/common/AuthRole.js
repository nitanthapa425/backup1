import { useSelector } from 'react-redux';

export const AuthRole = () => {
  return useSelector((state) => {
    return state.authReducer.loginData.user;
  });
};
