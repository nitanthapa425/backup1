import React, { useEffect, useState } from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { Button, TextField, Box, CircularProgress } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import Message from '../message/Message';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import { useFormik } from 'formik';
// import { useForm, Controller } from 'react-hook-form';

import theme from '../../styles/theme';
import logoWhite from '../../assets/images/logo-white.svg';
import bgPattern from '../../assets/images/pattern.svg';
import logo from '../../assets/images/logo.svg';
import { loginAction, resetLoginState } from '../../store/actions/authAction';
import { checkEmailValidity } from '../validations/YupValidations';
import { loginPagevalidationSchema } from '../formik-component/loginPageFormFormik';
import { changeToSmallCase } from '../common/CleanInputData';

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loginData = useSelector((state) => state.authReducer);

  // const { handleSubmit, control, reset } = useForm({
  //   defaultValues: {
  //     email: '',
  //     password: ''
  //   }
  // });

  const [showPassword, setShowPassword] = useState(false);

  const [snackbarMessage, setSnackbarMessage] = useState('');

  const login = async (value) => {
    const isEmail = await checkEmailValidity(value.email);
    const email = isEmail ? value.email : undefined;
    const userName = !isEmail ? value.email : undefined;

    dispatch(loginAction(email, value.password, userName, setSnackbarMessage, history, formik.setSubmitting));
  };

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: loginPagevalidationSchema,
    onSubmit: (values) => {
      login(values);
    }
  });
  // const cancelLoginSubmit = (e) => {
  //   reset();
  // };

  const handleClickShowPassword = () => {
    setShowPassword((prev) => !prev);
  };

  useEffect(() => {
    return () => {
      dispatch(resetLoginState());
    };
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <Message status={loginData.status} severity={loginData.severity} message={snackbarMessage} />
      <Box className="login-wrapper">
        <div className="welcome-screen">
          <div className="login-container">
            <div className="logo-holder">
              <a href="https://www.arsgroup.com.au/">
                <img src={logoWhite} alt="ARS mate property group"></img>
              </a>
            </div>
            <h1>
              Welcome to <br />
              <div className="ganesh-logo">
                <span className="icon-ganesh-logo"></span> <span className="ganesh-text">GANESH</span>
              </div>
            </h1>
            <p className="login-note">
              -A CRM Application of <a href="https://www.arsgroup.com.au/">ARS Mates Property Group</a>
            </p>
            <span className="login-subtitle">Please login to continue to your account</span>
            <p className="login-note">
              If you hit the wrong URL you can visit
              <br />
              our website for more details. <a href="https://www.arsgroup.com.au/">https://www.arsgroup.com.au/</a>
            </p>
          </div>
          <div className="bg-pattern">
            <img src={bgPattern} alt="" />
          </div>
        </div>
        <div className="login-screen">
          <div className="logo-holder-sm">
            <a href="https://www.arsgroup.com.au/">
              <img src={logo} alt="ARS mate property group"></img>
            </a>
          </div>
          {/* <div className="login-container">
            <form onSubmit={handleSubmit(onSubmit)} className="login-form">
              <h1>Login</h1>
              <div className="form-group">
                <div>
                  <Controller
                    name="email"
                    control={control}
                    render={({ field: { onChange, value }, fieldState: { error } }) => (
                      <TextField
                        label="Email"
                        className="text-field-custom"
                        placeholder="Please enter your username"
                        // type="email"
                        value={value}
                        onChange={onChange}
                        error={!!error}
                        helperText={error ? error.message : null}
                      />
                    )}
                    rules={{ required: 'Email or userName is required' }}
                  />
                </div>
              </div>
              <div className="form-group">
                <div>
                  <Controller
                    name="password"
                    control={control}
                    render={({ field: { onChange, value }, fieldState: { error } }) => (
                      <TextField
                        label="Password"
                        className="text-field-custom"
                        placeholder="Type your password"
                        type={values.showPassword ? 'text' : 'password'}
                        value={value}
                        onChange={onChange}
                        error={!!error}
                        helperText={error ? error.message : null}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword}>
                                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          )
                        }}
                      />
                    )}
                    rules={{ required: 'Password is required' }}
                  />
                </div>
              </div>
              <div className="button-holder">
                <Button type="submit" variant="contained" color="primary">
                  Login
                </Button>
                <Button variant="outlined" color="primary" className="cancel-button" onClick={cancelLoginSubmit}>
                  Cancel
                </Button>
                <span className="forget-password-text">
                  <Link to="/forgotpassword">Did you forget your password?</Link>
                </span>
              </div>
            </form>
          </div> */}

          <div className="form-group">
            <form onSubmit={formik.handleSubmit} className="login-form">
              <h1>Login</h1>
              <div className="form-group">
                <div>
                  <TextField
                    id="email"
                    name="email"
                    label="Email or Username"
                    className="text-field-custom"
                    value={changeToSmallCase(formik.values.email)}
                    onChange={formik.handleChange}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                  />
                </div>
              </div>
              <div className="form-group">
                <div>
                  <TextField
                    id="password"
                    name="password"
                    label="Password"
                    type={showPassword ? 'text' : 'password'}
                    className="text-field-custom"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton aria-label="toggle password visibility" onClick={handleClickShowPassword}>
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      )
                    }}
                  />
                </div>
              </div>

              <div className="button-holder">
                <Button type="submit" variant="contained" color="primary">
                  {formik.isSubmitting ? (
                    <CircularProgress color="secondary" size={28} style={{ margin: '0 0.4rem' }} />
                  ) : (
                    'Login'
                  )}
                </Button>
                <Button
                  variant="outlined"
                  color="primary"
                  // className="cancel-button"
                  onClick={formik.handleReset}
                >
                  Cancel
                </Button>
                <span className="forget-password-text">
                  <Link to="/forgotpassword">Did you forgot your password?</Link>
                </span>
              </div>
            </form>
          </div>

          <div className="login-footer">
            <ul>
              <li>
                <a href="mailto:info@arsgroup.com.au">Support</a>
              </li>
              <li>&copy; Copyright 2021</li>
            </ul>
          </div>
        </div>
      </Box>
    </ThemeProvider>
  );
};

export default Login;
