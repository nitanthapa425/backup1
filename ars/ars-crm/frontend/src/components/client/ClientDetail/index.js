/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useRef, useState } from 'react';
import * as yup from 'yup';
import {
  Backdrop,
  CircularProgress,
  ThemeProvider,
  RadioGroup,
  FormControlLabel,
  Radio,
  Select,
  MenuItem,
  InputLabel,
  Paper
} from '@material-ui/core';
import theme from '../../../styles/theme';
import { useHistory } from 'react-router-dom';
import { TextField } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import PersonIcon from '@material-ui/icons/Person';
// import FileCopyIcon from '@material-ui/icons/FileCopy';
import PhoneIcon from '@material-ui/icons/Phone';
import AddIcon from '@material-ui/icons/Add';
import EmailIcon from '@material-ui/icons/Email';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { useParams } from 'react-router-dom';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
// import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useDispatch, useSelector } from 'react-redux';
import {
  addFilesAction,
  addFilesActionOther,
  addFilesAndCorrespondence,
  addOtherDetails,
  addTransactionDetails,
  clientDetailsAction,
  deleteCorrespondigNotes,
  deleteCorrespondigNotesOther,
  deleteCorrespondingNote,
  deleteFiles,
  deleteFilesAndCorrespondence,
  deleteFilesOther,
  deleteTransactionsDetails,
  deleteTransactionsNotes,
  editTransactionsNotes,
  updateClientFollowUpNotesAction,
  updateCorrespondigNotes,
  updateCorrespondigNotesOther,
  updateFilesNotes,
  updateFilesNotesOthers,
  updateTransactionsNotes,
  othersdetailsfileandcorrespondence,
  fileandcorrespondenceothersdetails,
  getFollowupNotes
} from '../../../store/actions/clientAction';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { getCrmUsers } from '../../../store/actions/userAction';
import Dialog from '@material-ui/core/Dialog';
import { IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Field, Form, Formik } from 'formik';
import Dropzone from 'react-dropzone';
import ClearIcon from '@material-ui/icons/Clear';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import NumberFormat from 'react-number-format';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import EditIcon from '@material-ui/icons/Edit';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { AiFillFilePdf } from 'react-icons/ai';
import Message from '../../message/Message';
import AreYouSurePopUp from '../../dropdown/AreYouSure';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import FileViewer from '.././FileViewer';
import FollowUpNOte from './FollowUpNOte';
import ReferralTab from './ReferralTab';
import MaritalStatusTab from './MaritalStatusTab';
import GeneralDetail from './GeneralDetail';
import OtherDetailsTab from './OtherDetailsTab';
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

const ClientDetail = ({ auth }) => {
  const [userRole, setUserRole] = useState({});
  // eslint-disable-next-line no-unused-vars
  const [toastColor, setToastColor] = useState('');

  useEffect(() => {
    if (userRole !== {}) {
      setUserRole(auth.accessLevel);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth]);

  const history = useHistory();
  const [value, setValue] = React.useState(0);

  const handleSwitch = (event, newValue) => {
    setValue(newValue);
  };

  const params = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCrmUsers());
  }, [dispatch]);

  const userState = useSelector((state) => {
    return state.userReducer;
  });

  const { userList } = userState;
  const { results } = userList;

  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });

  const {
    clientDetails,
    correspondingfilesAndNotes,
    loading,
    othersdetailsfilesandcorrespondence,
    filesandcorrespondenceothersdetails
  } = clientLists;

  const [transactionIdForFileNote, setTransactionIdForFileNote] = useState('');
  useEffect(() => {
    dispatch(othersdetailsfileandcorrespondence(params.id));
  }, [
    correspondingfilesAndNotes,
    clientLists.deletedcorrespondingfilesAndNotes,
    clientLists.deletedfiles,
    clientLists.filesNotes,
    clientLists.updatedcorrespondencenotes,
    clientLists.deleteddcorrespondencenotes,
    clientLists.transactionsDetails,
    clientLists.deletetransactionsDetails,
    clientLists.updatedtransactionsdetails,
    clientLists.transactionsNotes,
    clientLists.deletedtransactionsNotes,
    clientLists.deleteCorrespondingNote
  ]);
  useEffect(() => {
    dispatch(fileandcorrespondenceothersdetails(params.id));
  }, [
    correspondingfilesAndNotes,
    clientLists.deletedcorrespondingfilesAndNotes,
    clientLists.deletedfiles,
    clientLists.filesNotes,
    clientLists.updatedcorrespondencenotes,
    clientLists.deleteddcorrespondencenotes
  ]);

  useEffect(() => {
    dispatch(clientDetailsAction(params.id));
  }, []);

  useEffect(() => {
    dispatch(getFollowupNotes(params.id));
  }, []);

  useEffect(() => {
    if (clientLists.deletedfollowupnotesuccessmsg) {
      dispatch(getFollowupNotes(params.id));
    }
  }, [clientLists.deletedfollowupnotesuccessmsg]);
  useEffect(() => {
    if (clientLists.followUpTaskCompletedsuccessmsg) {
      dispatch(getFollowupNotes(params.id));
    }
  }, [clientLists.followUpTaskCompletedsuccessmsg]);
  useEffect(() => {
    if (clientLists.updatedfollowupnotesuccessmsg) {
      dispatch(getFollowupNotes(params.id));
    }
  }, [clientLists.updatedfollowupnotesuccessmsg]);

  useEffect(() => {
    if (clientLists.addFollowupSuccessMsg) {
      dispatch(getFollowupNotes(params.id));
    }
  }, [clientLists.addFollowupSuccessMsg]);

  const [expanded, setExpanded] = React.useState(`panel[0]`);
  useEffect(() => {
    setExpanded(`panel[0]`);
    setExpandedFile(`panel[0]`);
  }, [clientDetails]);
  const handleChangetransaction = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const [expandedFile, setExpandedFile] = React.useState(`panel[0]`);

  const handleChangeFile = (panel) => (event, isExpanded) => {
    setExpandedFile(isExpanded ? panel : false);
  };

  const [fileDeleteSection, setFileDeleteSection] = useState('');

  const getDate = (objectId) => {
    if (!objectId) {
      return '';
    }
    const _date = new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
    return formatDate(_date) + ' ' + _date.toLocaleTimeString();
  };

  const formatDate = (date) => {
    if (!date || date === 'undefined') {
      return 'NA';
    }
    const _date = new Date(date);
    return `${_date.getDate()}/${_date.getMonth() + 1}/${_date.getFullYear()}`;
  };

  const handleClientUpdate = (id) => {
    history.push(`/update-client-profile/${id}`);
  };

  const clientDetailAutoComplet = [];
  const clientDataPull = () => {
    if (typeof userState.userList !== 'undefined' && typeof userState.userList.results !== 'undefined') {
      for (let index = 0; index < userState.userList.results.length; index++) {
        clientDetailAutoComplet.push(userState.userList.results[index]);
      }
    }
  };

  clientDataPull();

  const statusState = () => {
    if (clientDetails?.clientStatus === 'Active') {
      return <span className="status">{clientDetails?.clientStatus}</span>;
    } else {
      if (clientDetails?.clientStatus === 'Inactive') {
        return <span className=" status status-inactive">{clientDetails?.clientStatus}</span>;
      } else {
        return <span className=" status status-others">{clientDetails?.clientStatus}</span>;
      }
    }
  };

  const [open, setOpen] = React.useState(false);
  const [openOtherDetails, setOpenOtherDetails] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);
  const [openFileUploadDialog, setOpenFileUploadDialog] = React.useState(false);
  const [openFileUploadDialogOther, setOpenFileUploadDialogOther] = React.useState(false);
  const [openFileViewer, setOpenFileViewer] = useState(false);
  const [currentlyOpenedFile, setCurrentlyOpenFile] = useState(null);

  const handleOpenFileViewer = (fileName) => {
    setCurrentlyOpenFile(fileName);
    setOpenFileViewer(true);
  };
  const handleCloseFileViewer = () => {
    setCurrentlyOpenFile('');
    setOpenFileViewer(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickOpenOtherDetails = () => {
    setOpenOtherDetails(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseOtherDetails = () => {
    setOpenOtherDetails(false);
  };
  const handleClickView = () => {
    setOpen1(true);
  };
  const handleShut = () => {
    setOpen1(false);
  };

  let handleonDrop = (acceptedFiles, rejectedFiles, setFieldValue, values) => {
    if (acceptedFiles.length > 0) {
      setToastColor('toast-success');

      const prevFiles = values?.files ? [...values.files] : [];
      setFieldValue('files', [...prevFiles, ...acceptedFiles]);
      toast('File successfully uploaded', {
        position: 'top-center',
        autoClose: 2000,
        className: 'toast-success',
        progressClassName: 'toastProgress'
      });
    }
    if (rejectedFiles.length > 0) {
      setToastColor('toast-error');
      rejectedFiles.forEach((file) => {
        if (file.file.size > 1024 * 1024 * 20) {
          toast('File size must be less than 20MB', {
            position: 'top-center',
            autoClose: false,
            className: 'toast-error'
          });
        } else {
          toast('Only .docx and .pdf files are accepted', {
            position: 'top-center',
            autoClose: false,
            className: 'toast-error'
          });
        }
      });
    }
  };

  let handleonDropOther = (acceptedFiles, rejectedFiles, setFieldValue, values) => {
    if (acceptedFiles.length > 0) {
      setFieldValue('files', [...values.files, ...acceptedFiles]);
      toast('File successfully uploaded', {
        position: 'top-center',
        autoClose: 2000,
        className: 'toast-success',
        progressClassName: 'toastProgress'
      });
    }
    if (rejectedFiles.length > 0) {
      rejectedFiles.forEach((file) => {
        if (file.file.size > 1024 * 1024 * 10) {
          toast('File size must be less than 20MB', {
            position: 'top-center',
            autoClose: false,
            className: 'toast-error'
          });
        } else {
          toast('Only .docx and .pdf files are accepted', {
            position: 'top-center',
            autoClose: false,
            className: 'toast-error'
          });
        }
      });
    }
  };

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  const getFileName = (filePath) => {
    if (filePath?.includes('{{') && filePath?.includes('}}')) {
      try {
        let _fileName = filePath?.slice(filePath?.indexOf('}}') + 2);
        return _fileName?.slice(0, _fileName?.length);
      } catch (e) {
        return false;
      }
    }
    return false;
  };

  let handleCloseFiles = (i, setFieldValue, values) => {
    let changedFiles = values?.correspondingNotes[0]?.correspongingUploadPath?.filter((file, index) => index !== i);
    setFieldValue('correspondingNotes[0].correspongingUploadPath', changedFiles);
  };

  const initialValues = {
    files: [],
    correspondingNote: '',
    correspondingTitle: ''
  };

  const initialValuesOtherDetails = {
    files: [],
    newOtherNotes: [
      {
        newOtherNote: ''
      }
    ]
  };
  const onSubmitOtherDetails = (values, onSubmitProps) => {
    const formData = new FormData();

    formData.append('newOtherNotes', JSON.stringify(values.newOtherNotes));
    values.files.forEach((file) => {
      formData.append('files', file);
    });
    // if (params.id && transactionId) {
    //   dispatch(addFilesAndCorrespondence(params.id, transactionId, formData, handleClose));
    // }
    dispatch(addOtherDetails(params.id, formData, handleCloseOtherDetails));
    handleCloseOtherDetails();
  };

  const onSubmit = (values, onSubmitProps) => {
    const formData = new FormData();

    formData.append('otherNotes', JSON.stringify([{ otherNote: values.correspondingNote }]));
    formData.append('correspondingTitle', values.correspondingTitle);
    values.files.forEach((file) => {
      formData.append('files', file);
    });
    if (params.id && transactionId) {
      dispatch(addFilesAndCorrespondence(params.id, transactionId, formData, handleClose));
      handleClose();
    }
  };

  const onFileUpload = (values) => {
    const formData = new FormData();
    values.files.forEach((file) => {
      formData.append('files', file);
    });
    if (params.id && transactionId && currentCorrespondingId) {
      dispatch(
        addFilesAction(params.id, transactionId, currentCorrespondingId, formData, () => setOpenFileUploadDialog(false))
      );
    }
  };

  const onFileUploadOther = (values) => {
    const formData = new FormData();
    values.files.forEach((file) => {
      formData.append('files', file);
    });

    dispatch(
      addFilesActionOther(params.id, currentCorrespondingId, formData, () => setOpenFileUploadDialogOther(false))
    );
  };

  const transactionInitialValues = {
    propertyLotAddress: '',
    upTo: '',
    propertyType: '',
    purchaseStatus: '',
    ooInvst: '',
    resultNotes: [],
    files: [],
    // correspondingTitle:'',
    correspondingNotes: [
      {
        correspondingTitle: '',
        correspongingUploadPath: [],
        // otherNotes: [{ otherNote: '' }]
        otherNotes: [
          {
            otherNote: ''
          }
        ]
      }
    ]
  };

  const transactionOnSubmit = (values, onSubmitProps) => {
    const formData = new FormData();
    formData.append('propertyLotAddress', JSON.stringify(values.propertyLotAddress));
    formData.append('upTo', JSON.stringify(values.upTo));
    formData.append('ooInvst', JSON.stringify(values.ooInvst));
    formData.append('resultNotes', JSON.stringify(values.resultNotes));
    formData.append('purchaseStatus', JSON.stringify(values.purchaseStatus));
    formData.append('correspondingNote', JSON.stringify(values.correspondingNotes));
    formData.append('propertyType', JSON.stringify(values.propertyType));
    values?.files?.forEach((file) => {
      formData.append('files', file);
    });

    dispatch(addTransactionDetails(formData, params.id));
  };

  const fileInitialValues = {
    filesNotes: ''
  };
  const fileInitialValuesOther = {
    newOtherNote: ''
  };
  const fileNotesOnSubmit = (values, onSubmitProps) => {
    const data = {
      otherNote: values.filesNotes
    };
    dispatch(updateFilesNotes(data, params.id, transactionIdForFileNote, values._id, history));
  };

  const fileNotesOnSubmitOther = (values, onSubmitProps) => {
    const data = {
      newOtherNote: values.newOtherNote
    };
    dispatch(updateFilesNotesOthers(data, params.id, values._id, history));
  };

  const fileNotesValidationSchema = yup.object({});

  const transactionsNotesInitialValues = {
    transactionsNotes: ''
  };
  const transactionsNotesOnSubmit = (values, onSubmitProps) => {
    const data = {
      note: values.transactionsNotes
    };
    dispatch(updateTransactionsNotes(data, params.id, values.transactionId));
  };

  const transactionsNotesValidationSchema = yup.object({});

  const [fileDeleteOpen, setFileDeleteOpen] = React.useState(false);
  const [fileCorrespondingDelete, setFileCorrespondenceDelete] = useState('');
  const handleClickFileDeleteOpen = (othersId) => {
    setFileCorrespondenceDelete(othersId);
    setFileDeleteOpen(true);
    // dispatch(deleteFilesAndCorrespondence(params.id, othersId));
  };

  const handleFileDeleteClose = () => {
    setFileDeleteOpen(false);
  };

  const [transactionId, setTransactionId] = React.useState('');
  const [currentCorrespondingId, setCurrentCorrespondingId] = React.useState('');
  const [transactionDeleteOpen, settransactionDeleteOpen] = React.useState(false);
  const handleClicktransactionDeleteOpen = (transactionId) => {
    setTransactionId(transactionId);
    settransactionDeleteOpen(true);
    // dispatch(deleteFilesAndCorrespondence(params.id, othersId));
  };

  const handletransactionDeleteClose = () => {
    settransactionDeleteOpen(false);
  };

  const [openFollowUpdate, setOpenFollowUpdate] = React.useState(false);

  const postedByUser = (id) => {
    const selecteddata = results?.find((user, i) => {
      return user.id === `${id}`;
    });
    const name = selecteddata?.firstName + ' ' + selecteddata?.lastName;
    return name || 'NA';
  };

  const [openCorrespondenceUpdate, setOpenCorrespondenceUpdate] = useState(false);
  const [openCorrespondenceUpdateOther, setOpenCorrespondenceUpdateOther] = useState(false);
  const [editCorrespondenceNote, setEditCorrespondenceNote] = useState('');
  const [editCorrespondenceNoteInitial, setEditCorrespondenceNoteInitial] = useState('');
  const [editFileNoteDetails, setEditFileNoteDetails] = useState('');
  const hendelOpenCorrespondenceUpdate = (tranId, valueId, i) => {
    setEditFileNoteDetails({ tranId: tranId, valueId: valueId, index: i });
    setOpenCorrespondenceUpdate(true);
  };
  const hendelOpenCorrespondenceUpdateOther = (valueId, i) => {
    setEditFileNoteDetails({ valueId: valueId, index: i });
    setOpenCorrespondenceUpdateOther(true);
  };
  const hendelCloseCorrespondenceUpdate = () => {
    setOpenCorrespondenceUpdate(false);
  };
  const hendelCloseCorrespondenceUpdateOther = () => {
    setOpenCorrespondenceUpdateOther(false);
  };
  const [openTransactionNoteUpdate, setOpenTransactionNoteUpdate] = useState(false);
  const [updateTransactionNoteId, setUpdateTransactionNoteId] = useState({});

  const [editTransactionNoteValue, setEditTransactionNoteValue] = useState('');
  const [editTransactionNoteValueInitial, setEditTransactionNoteValueInitial] = useState('');
  const hendelOpenTransactionNoteUpdate = (valueId, i) => {
    setUpdateTransactionNoteId({ valueId: valueId, index: i });
    setOpenTransactionNoteUpdate(true);
  };
  const hendelCloseTransactionNoteUpdate = () => {
    setOpenTransactionNoteUpdate(false);
  };

  const [openDeleteFiles, setOpenDeleteFiles] = useState(false);
  const [openTaskCompleted, setOpenTaskCompleted] = useState(false);
  const [openDeleteFilesOther, setOpenDeleteFilesOther] = useState(false);
  const [deleteFileId, setDeleteFileId] = useState();
  const [deleteFileIndex, setDeleteFileIndex] = useState();
  const [deleteFileTransactions, setDeleteFileTransactions] = useState();
  const handleClickOpenDeleteFiles = (tranactionId, othersId, index) => {
    setDeleteFileId(othersId);
    setDeleteFileIndex(index);
    setDeleteFileTransactions(tranactionId);
    setOpenDeleteFiles(true);

    // dispatch(deleteFiles(params.id,othersId,index))
  };
  const handleClickOpenDeleteFilesOther = (othersId, index) => {
    setDeleteFileId(othersId);
    setDeleteFileIndex(index);
    setOpenDeleteFilesOther(true);

    // dispatch(deleteFiles(params.id,othersId,index))
  };

  const handleCloseDeleteFiles = () => {
    setOpenDeleteFiles(false);
  };
  const handleCloseDeleteFilesOther = () => {
    setOpenDeleteFilesOther(false);
  };

  const [openDeleteCorrespondenceNote, setOpenDeleteCorrespondenceNote] = useState(false);
  const [openDeleteCorrespondenceNoteOther, setOpenDeleteCorrespondenceNoteOther] = useState(false);
  const [deletefilenote, setDeletefilenote] = useState('');
  const handleClickOpenDeleteCorrespondenceNote = (tranId, othersId, index) => {
    setDeletefilenote({ tranId: tranId, othersId: othersId, index: index });
    setOpenDeleteCorrespondenceNote(true);

    // dispatch(deleteCorrespondigNotes(params.id, othersId, index));
  };
  const handleClickOpenDeleteCorrespondenceNoteOther = (othersId, index) => {
    setDeletefilenote({ othersId: othersId, index: index });
    setOpenDeleteCorrespondenceNoteOther(true);

    // dispatch(deleteCorrespondigNotes(params.id, othersId, index));
  };

  const handleClickCloseDeleteCorrespondenceNote = () => {
    setOpenDeleteCorrespondenceNote(false);
  };
  const handleClickCloseDeleteCorrespondenceNoteOther = () => {
    setOpenDeleteCorrespondenceNoteOther(false);
  };

  const [openDeleteTransactionNote, setOpenDeleteTransactionNote] = useState(false);
  const [deleteTransactionNodeId, setDeleteTransactionNodeId] = useState({});
  const handleClickOpenDeleteTransactionNote = (valueId, i) => {
    setDeleteTransactionNodeId({ valueId: valueId, index: i });
    setOpenDeleteTransactionNote(true);

    // dispatch(deleteCorrespondigNotes(params.id, othersId, index));
  };

  const handleClickCloseDeleteTransactionNote = () => {
    setOpenDeleteTransactionNote(false);
  };

  const redirecttoClietEdit = () => {
    history.push(`/update-client-profile/${params.id}`);
  };

  const [openDeleteCorrespondingFiles, setOpenDeleteCorrespondingFiles] = useState(false);
  return (
    <ThemeProvider theme={theme}>
      <Backdrop open={clientLists.loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      <Message status={clientLists.status} severity={clientLists.severity} message={clientLists.message} />
      <ToastContainer />
      <Dialog
        open={openDeleteFiles}
        onClose={handleCloseDeleteFiles}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please note that,
            once deleted, this cannot be undone and the record will be permanently removed.
          </DialogContentText>
        </DialogContent>
        <DialogActions className="alert-btn-holder">
          <Button
            onClick={() => {
              handleCloseDeleteFiles();
              dispatch(deleteFiles(params.id, deleteFileTransactions, deleteFileId, deleteFileIndex));
            }}
            variant="contained"
            className="delete-button"
          >
            Delete
          </Button>
          <Button
            onClick={handleCloseDeleteFiles}
            color="primary"
            variant="outlined"
            // className="cancel-button"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openDeleteFilesOther}
        onClose={handleCloseDeleteFilesOther}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please note that,
            once deleted, this cannot be undone and the record will be permanently removed.
          </DialogContentText>
        </DialogContent>
        <DialogActions className="alert-btn-holder">
          <Button
            onClick={() => {
              handleCloseDeleteFilesOther();
              dispatch(deleteFilesOther(params.id, deleteFileId, deleteFileIndex));
            }}
            variant="contained"
            className="delete-button"
          >
            Delete
          </Button>
          <Button
            onClick={handleCloseDeleteFilesOther}
            color="primary"
            //  variant="outlined"
            variant="outlined"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      {/* DELETE CORRESPONDING FILES AND NOTES */}
      <Dialog
        open={openDeleteCorrespondingFiles}
        onClose={() => setOpenDeleteCorrespondingFiles(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please note that,
            once deleted, this cannot be undone and the record will be permanently removed.
          </DialogContentText>
        </DialogContent>
        <DialogActions className="alert-btn-holder">
          <Button
            onClick={() => {
              dispatch(deleteCorrespondingNote(params.id, transactionId, currentCorrespondingId));
              setOpenDeleteCorrespondingFiles(false);
            }}
            variant="contained"
            className="delete-button"
          >
            Delete
          </Button>
          <Button
            onClick={() => setOpenDeleteCorrespondingFiles(false)}
            color="primary"
            variant="outlined"
            // className="cancel-button"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      <div className="client-detail-wrapper content-section">
        <div className="top-bar">
          <div className="name-status">
            <h3>
              {clientDetails?.firstName} {clientDetails?.middleName} {clientDetails?.lastName}
            </h3>
            {statusState()}
            {clientDetails?.entryDate ? (
              <span className="entryDate">
                <strong>Entry Date :</strong>
                {formatDate(clientDetails?.entryDate)}
              </span>
            ) : null}
          </div>
          <div className="edit-btn">
            {userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' ? (
              <Button variant="contained" color="primary" onClick={() => handleClientUpdate(clientDetails?.id)}>
                Edit Profile
              </Button>
            ) : null}
          </div>
        </div>
        <Grid container spacing={3}>
          <Grid item xs={12} xl={4}>
            <GeneralDetail clientDetails={clientDetails}></GeneralDetail>
          </Grid>

          <Grid item xs={12} xl={8}>
            <FollowUpNOte
              paramsId={params.id}
              userState={userState}
              clientLists={clientLists}
              userRole={userRole}
              formatDate={formatDate}
              postedByUser={postedByUser}
              getDate={getDate}
            ></FollowUpNOte>
          </Grid>
          <Grid item xs={12}>
            <div className="other-details">
              <div>
                <AppBar position="static" color="default">
                  <Tabs
                    value={value}
                    onChange={handleSwitch}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons="auto"
                    aria-label="scrollable auto tabs example"
                  >
                    <Tab label="Other Details" {...a11yProps(0)} />
                    <Tab label="Transaction Details" {...a11yProps(1)} />
                    <Tab label="Referral" {...a11yProps(2)} />
                    <Tab label="Marital Status" {...a11yProps(3)} />
                  </Tabs>
                </AppBar>
                <TabPanel value={value} index={0}>
                  <OtherDetailsTab
                    filesandcorrespondenceothersdetails={filesandcorrespondenceothersdetails}
                    expandedFile={expandedFile}
                    handleChangeFile={handleChangeFile}
                    handleClickOpenOtherDetails={handleClickOpenOtherDetails}
                    userRole={userRole}
                    redirecttoClietEdit={redirecttoClietEdit}
                    setFileDeleteSection={setFileDeleteSection}
                    handleClickFileDeleteOpen={handleClickFileDeleteOpen}
                    setOpenFileUploadDialogOther={setOpenFileUploadDialogOther}
                    setCurrentCorrespondingId={setCurrentCorrespondingId}
                    getFileName={getFileName}
                    handleOpenFileViewer={handleOpenFileViewer}
                    handleClickOpenDeleteFilesOther={handleClickOpenDeleteFilesOther}
                    history={history}
                    paramsId={params.id}
                    setEditCorrespondenceNote={setEditCorrespondenceNote}
                    setEditCorrespondenceNoteInitial={setEditCorrespondenceNoteInitial}
                    hendelOpenCorrespondenceUpdateOther={hendelOpenCorrespondenceUpdateOther}
                    handleClickOpenDeleteCorrespondenceNoteOther={handleClickOpenDeleteCorrespondenceNoteOther}
                    formatDate={formatDate}
                    results={results}
                    openDeleteCorrespondenceNote={openDeleteCorrespondenceNote}
                    handleClickCloseDeleteCorrespondenceNote={handleClickCloseDeleteCorrespondenceNote}
                  ></OtherDetailsTab>
                </TabPanel>

                <TabPanel value={value} index={1}>
                  <div className="tab-panel-details  detail-content-accordion">
                    <div className="details-heading file-uploads-heading ">
                      <h4>Transaction Details : </h4>
                      <div className="add-button">
                        <Button
                          type="submit"
                          variant="contained"
                          className="btn-secondary"
                          startIcon={<AddIcon />}
                          onClick={handleClickView}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                    <div>
                      {typeof othersdetailsfilesandcorrespondence !== 'undefined' ? (
                        <>
                          {othersdetailsfilesandcorrespondence?.length === 0 ? (
                            <div>No record found</div>
                          ) : (
                            [...othersdetailsfilesandcorrespondence]?.reverse?.()?.map?.((value, i) => {
                              return (
                                <Accordion
                                  expanded={expanded === `panel[${i}]`}
                                  // expanded={true}
                                  onChange={handleChangetransaction(`panel[${i}]`)}
                                  className="acc-holder"
                                >
                                  <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1bh-content"
                                    id="panel1bh-header"
                                  >
                                    <div className="edit-delete-wraper">
                                      <h4>
                                        {othersdetailsfilesandcorrespondence?.length - i} -{' '}
                                        {value.propertyLotAddress ? (
                                          <>{value.propertyLotAddress.replaceAll('"', '')}</>
                                        ) : (
                                          'No record found'
                                        )}{' '}
                                      </h4>
                                      <div>
                                        <EditIcon
                                          onClick={() => {
                                            redirecttoClietEdit();
                                          }}
                                          className="edit-icons"
                                        />
                                        {userRole === 'SUPER_ADMIN' ? (
                                          <DeleteOutline
                                            onClick={() => {
                                              handleClicktransactionDeleteOpen(value._id);
                                            }}
                                            className="margindelete fileCorrespondingDelete"
                                          />
                                        ) : null}
                                      </div>
                                    </div>
                                  </AccordionSummary>
                                  <AccordionDetails>
                                    <div className="details-list">
                                      {value.propertyLotAddress ||
                                      value.upTo ||
                                      value.propertyType ||
                                      value.purchaseStatus ||
                                      value.ooInvst ? (
                                        <div>
                                          {value.propertyLotAddress ? (
                                            <div className="list">
                                              <span className="title">Property Lot Address:&nbsp;</span>

                                              {value.propertyLotAddress.replaceAll('"', '')}
                                            </div>
                                          ) : null}

                                          {value.upTo ? (
                                            <div className="list">
                                              <span className="title">
                                                Purchase Price Range <span className="title"> Up To :&nbsp;</span>{' '}
                                              </span>
                                              ${formatNumber(value.upTo.replaceAll('"', ''))}
                                            </div>
                                          ) : null}

                                          {value.propertyType ? (
                                            <div className="list">
                                              <span className="title">Property Type :&nbsp;</span>

                                              {value.propertyType.replaceAll('"', '').replaceAll('_', ' ')}
                                            </div>
                                          ) : null}

                                          {value.purchaseStatus ? (
                                            <div className="list">
                                              <span className="title">Purchase Status :&nbsp;</span>
                                              {value.purchaseStatus.replaceAll('"', '').replaceAll('_', ' ')}
                                            </div>
                                          ) : null}

                                          {value?.ooInvst ? (
                                            <div className="list">
                                              <span className="title">OO/INVST :&nbsp;</span>
                                              {value.ooInvst.replaceAll('"', '').replaceAll('_', ' ')}
                                            </div>
                                          ) : null}
                                        </div>
                                      ) : (
                                        'No record found'
                                      )}
                                      <div className="list">
                                        <div className="file-notes">
                                          <Formik
                                            initialValues={transactionsNotesInitialValues}
                                            onSubmit={transactionsNotesOnSubmit}
                                            validationSchema={transactionsNotesValidationSchema}
                                          >
                                            {(formik) => {
                                              const { values, isSubmitting, form, setFieldValue } = formik;

                                              return (
                                                <Form>
                                                  <div className="details-heading">
                                                    <span className="title">Result Note : </span>
                                                    <div className="form-control">
                                                      <Field name="transactionsNotes">
                                                        {(props) => {
                                                          const { field, meta, form } = props;

                                                          return (
                                                            <TextareaAutosize
                                                              aria-label="notes"
                                                              // minRows={2}
                                                              placeholder="Add a note"
                                                              {...field}
                                                              onChange={(e) => {
                                                                setFieldValue(
                                                                  'transactionsNotes',
                                                                  e.target.value.trimLeft()
                                                                );
                                                              }}
                                                            />
                                                          );
                                                        }}
                                                      </Field>
                                                      <Button
                                                        type="submit"
                                                        variant="contained"
                                                        className="btn-secondary"
                                                        disabled={values.transactionsNotes.trim() ? false : true}
                                                        onClick={() => {
                                                          setFieldValue('transactionId', value._id);
                                                        }}
                                                      >
                                                        Post
                                                      </Button>
                                                    </div>
                                                  </div>
                                                </Form>
                                              );
                                            }}
                                          </Formik>
                                          <div className="note-list-holder">
                                            <div className="note-lists">
                                              {value?.resultNotes?.map((v, i) => {
                                                return (
                                                  <div
                                                    key={i}
                                                    className="lists"
                                                    style={{ display: v?.note ? 'block' : 'none' }}
                                                  >
                                                    <div className="rightIcon">
                                                      <div className="icon-holder">
                                                        <EditIcon
                                                          onClick={() => {
                                                            setEditTransactionNoteValue(v);
                                                            setEditTransactionNoteValueInitial(v);

                                                            hendelOpenTransactionNoteUpdate(value._id, v._id);
                                                          }}
                                                          className="edit-icon"
                                                        />
                                                      </div>

                                                      {userRole === 'SUPER_ADMIN' ? (
                                                        <div className="icon-holder">
                                                          <DeleteOutline
                                                            onClick={() => {
                                                              handleClickOpenDeleteTransactionNote(value._id, v._id);
                                                            }}
                                                          />
                                                        </div>
                                                      ) : null}
                                                    </div>
                                                    <p>{v?.note}</p>
                                                    <Dialog
                                                      open={openTransactionNoteUpdate}
                                                      onClose={hendelCloseTransactionNoteUpdate}
                                                      aria-labelledby="alert-dialog-title"
                                                      aria-describedby="alert-dialog-description"
                                                    >
                                                      <DialogTitle id="responsive-dialog-title">
                                                        {'Update transaction Note'}
                                                        <IconButton onClick={hendelCloseTransactionNoteUpdate}>
                                                          <ClearIcon></ClearIcon>
                                                        </IconButton>
                                                      </DialogTitle>
                                                      <DialogContent>
                                                        <TextField
                                                          label="Follow up Note"
                                                          value={editTransactionNoteValue?.note}
                                                          fullWidth
                                                          type="text"
                                                          onChange={(e) => {
                                                            setEditTransactionNoteValue({
                                                              note: e.target.value.trimLeft()
                                                            });
                                                          }}
                                                        />
                                                      </DialogContent>
                                                      <DialogActions>
                                                        <Button
                                                          onClick={() => {
                                                            dispatch(
                                                              editTransactionsNotes(
                                                                editTransactionNoteValue,
                                                                params.id,
                                                                updateTransactionNoteId.valueId,
                                                                updateTransactionNoteId.index
                                                              )
                                                            );
                                                            hendelCloseTransactionNoteUpdate();
                                                          }}
                                                          color="primary"
                                                          variant="contained"
                                                          className="forUpdates"
                                                          autoFocus
                                                          // disabled={editTransactionNoteValue?.note?.trim() ? false : true}
                                                        >
                                                          Update
                                                        </Button>
                                                        <Button
                                                          autoFocus
                                                          onClick={() => {
                                                            setEditTransactionNoteValue(
                                                              editTransactionNoteValueInitial
                                                            );
                                                          }}
                                                          // color="primary"
                                                          variant="outlined"
                                                          className="forClearHover"
                                                        >
                                                          Clear
                                                        </Button>
                                                      </DialogActions>
                                                    </Dialog>
                                                    <div className="list-bottom">
                                                      <span className="posted-date">
                                                        <strong>Posted on : {formatDate(v.createdAt)}</strong>
                                                      </span>
                                                      <span className="assigned-to">
                                                        <strong>Posted by :&nbsp;{postedByUser(v.createdBy)}</strong>
                                                      </span>
                                                    </div>

                                                    <Dialog
                                                      open={openDeleteTransactionNote}
                                                      onClose={handleClickCloseDeleteTransactionNote}
                                                      aria-labelledby="alert-dialog-title"
                                                      aria-describedby="alert-dialog-description"
                                                    >
                                                      <DialogTitle id="responsive-dialog-title">
                                                        {'Are you sure want to Delete?'}
                                                      </DialogTitle>
                                                      <DialogContent>
                                                        <DialogContentText id="alert-dialog-description">
                                                          <span className="text-danger">Alert!!!</span> Are you sure you
                                                          want to delete this record? Please note that, once deleted,
                                                          this cannot be undone and the record will be permanently
                                                          removed.
                                                        </DialogContentText>
                                                      </DialogContent>
                                                      <DialogActions className="alert-btn-holder">
                                                        <Button
                                                          onClick={() => {
                                                            handleClickCloseDeleteTransactionNote();
                                                            dispatch(
                                                              deleteTransactionsNotes(
                                                                params.id,
                                                                deleteTransactionNodeId.valueId,
                                                                deleteTransactionNodeId.index
                                                              )
                                                            );
                                                          }}
                                                          variant="contained"
                                                          className="delete-button"
                                                        >
                                                          Delete
                                                        </Button>
                                                        <Button
                                                          variant="outlined"
                                                          onClick={handleClickCloseDeleteTransactionNote}
                                                          color="primary"
                                                          // className="cancel-button"
                                                        >
                                                          Cancel
                                                        </Button>
                                                      </DialogActions>
                                                    </Dialog>
                                                  </div>
                                                );
                                              })}
                                            </div>
                                          </div>
                                        </div>
                                      </div>

                                      <div className="tab-panel-details tab-panel-details-1">
                                        <div className="file-uploads-heading details-heading">
                                          <h4>Files & Correspondence Upload : </h4>
                                          <div className="add-button">
                                            <Button
                                              type="submit"
                                              variant="contained"
                                              className="btn-secondary"
                                              startIcon={<AddIcon />}
                                              onClick={() => {
                                                handleClickOpen();
                                                setTransactionId(value._id);
                                              }}
                                            >
                                              Add
                                            </Button>
                                          </div>
                                        </div>
                                        {/* typeof clientDetails !== "undefined" && typeof clientDetails?.otherDetails !== "undefined"  */}
                                        {typeof value?.correspondingNote !== 'undefined' &&
                                        value?.correspondingNote.length > 0
                                          ? [...value?.correspondingNote].reverse().map((x, i) => {
                                              return (
                                                <div className="detail-content-accordion">
                                                  <Accordion
                                                    expanded={expandedFile === `panel[${i}]`}
                                                    onChange={handleChangeFile(`panel[${i}]`)}
                                                    className="acc-holder"
                                                  >
                                                    <AccordionSummary
                                                      expandIcon={<ExpandMoreIcon />}
                                                      aria-controls="panel1bh-content"
                                                      id="panel1bh-header"
                                                      className="detail-content-accordion-title"
                                                    >
                                                      <div className="edit-delete-wraper">
                                                        <h4>
                                                          {[...value?.correspondingNote].length - i} -
                                                          {x?.correspondingTitle}
                                                        </h4>

                                                        <EditIcon
                                                          onClick={() => {
                                                            redirecttoClietEdit();
                                                          }}
                                                          className="edit-icons"
                                                        />
                                                        {userRole === 'SUPER_ADMIN' ? (
                                                          <DeleteOutline
                                                            onClick={() => {
                                                              // handleClickFileDeleteOpen(x._id);
                                                              setFileDeleteSection('transactions');
                                                              setOpenDeleteCorrespondingFiles(true);
                                                              setTransactionId(value._id);
                                                              setCurrentCorrespondingId(x._id);
                                                            }}
                                                            className="margindelete fileCorrespondingDelete"
                                                          />
                                                        ) : null}
                                                      </div>
                                                    </AccordionSummary>

                                                    <AccordionDetails>
                                                      <div className="file-uploads">
                                                        <span className="title">
                                                          Files :{' '}
                                                          <span className="add-button">
                                                            <Button
                                                              type="submit"
                                                              variant="contained"
                                                              className="btn-secondary-xs"
                                                              startIcon={<AddIcon />}
                                                              onClick={() => {
                                                                setOpenFileUploadDialog(true);
                                                                setCurrentCorrespondingId(x._id);
                                                                setTransactionId(value._id);
                                                              }}
                                                            >
                                                              Add files
                                                            </Button>
                                                          </span>
                                                        </span>
                                                        <div className="file-uploads-container">
                                                          {x && typeof x.correspondingUploadPaths !== 'undefined'
                                                            ? [...x.correspondingUploadPaths]
                                                                .reverse()
                                                                .map((transactionUploadPath, i) => {
                                                                  return (
                                                                    <div className="file-uploads-container-1" key={i}>
                                                                      <div className="file-uploads-one">
                                                                        <AiFillFilePdf className="file-uploads-one-icon" />
                                                                        <span className="file-uploads-container-1-name">
                                                                          {getFileName(transactionUploadPath) ||
                                                                            `File ${i + 1}`}
                                                                        </span>
                                                                      </div>
                                                                      <div className="file-uploads-one file-uploads-two">
                                                                        <button
                                                                          className="file-view-btn"
                                                                          onClick={() => {
                                                                            const fileName =
                                                                              // transactionUploadPath[i].split('\\{{')[1];
                                                                              transactionUploadPath.split('\\{{')[1];
                                                                            handleOpenFileViewer(`{{${fileName}`);
                                                                          }}
                                                                        >
                                                                          View
                                                                        </button>
                                                                        <button className="file-uploads-one-btn-1">
                                                                          <a
                                                                            href={'/' + transactionUploadPath}
                                                                            download={
                                                                              getFileName(transactionUploadPath) ||
                                                                              undefined
                                                                            }
                                                                            rel="noreferrer"
                                                                            target="_blank"
                                                                          >
                                                                            Download
                                                                          </a>
                                                                        </button>
                                                                        {userRole === 'SUPER_ADMIN' ? (
                                                                          <button
                                                                            className="file-uploads-one-btn-2"
                                                                            onClick={() => {
                                                                              handleClickOpenDeleteFiles(
                                                                                value._id,
                                                                                x._id,
                                                                                x?.correspondingUploadPaths?.length -
                                                                                  1 -
                                                                                  i
                                                                              );
                                                                            }}
                                                                          >
                                                                            Delete
                                                                          </button>
                                                                        ) : null}
                                                                      </div>
                                                                      {/* {x.correspondingUploadPaths[i] ? (
                                                <>
                                                  <span className="filename">
                                                    <a
                                                      href={'/' + x.correspondingUploadPaths[i]}
                                                      download={getFileName(x.correspondingUploadPaths[i]) || undefined}
                                                      rel="noreferrer"
                                                      target="_blank"
                                                    >
                                                      {getFileName(x.correspondingUploadPaths[i]) || `File ${i + 1}`}
                                                    </a>
                                                  </span>
                                                </>
                                              ) : null} */}
                                                                    </div>
                                                                  );
                                                                })
                                                            : null}
                                                        </div>
                                                      </div>

                                                      <div className="file-notes">
                                                        <Formik
                                                          initialValues={fileInitialValues}
                                                          onSubmit={fileNotesOnSubmit}
                                                          validationSchema={fileNotesValidationSchema}
                                                        >
                                                          {(formik) => {
                                                            const { values, isSubmitting } = formik;
                                                            return (
                                                              <Form>
                                                                <div className="details-heading">
                                                                  <span className="title">Correspondence Note : </span>
                                                                  <div className="form-control">
                                                                    <Field name="filesNotes">
                                                                      {(props) => {
                                                                        const { field, meta, form } = props;
                                                                        const { setFieldValue } = form;

                                                                        return (
                                                                          <TextareaAutosize
                                                                            aria-label="notes"
                                                                            // minRows={2}
                                                                            placeholder="Add a note"
                                                                            {...field}
                                                                            onChange={(e) => {
                                                                              setFieldValue(
                                                                                'filesNotes',
                                                                                e?.target?.value?.trimLeft()
                                                                              );
                                                                              setFieldValue('_id', x?._id);
                                                                            }}
                                                                          />
                                                                        );
                                                                      }}
                                                                    </Field>

                                                                    <Button
                                                                      type="submit"
                                                                      variant="contained"
                                                                      className="btn-secondary"
                                                                      disabled={
                                                                        values?.filesNotes?.trim() ? false : true
                                                                      }
                                                                      onClick={() => {
                                                                        setTransactionIdForFileNote(value._id);
                                                                      }}
                                                                    >
                                                                      Post
                                                                    </Button>
                                                                  </div>
                                                                </div>
                                                              </Form>
                                                            );
                                                          }}
                                                        </Formik>
                                                        <div className="note-list-holder">
                                                          <div className="note-lists">
                                                            {true ? (
                                                              <>
                                                                {x?.otherNotes.map((othernotevalue, i) => {
                                                                  return othernotevalue?.otherNote ? (
                                                                    <div key={i} className="lists">
                                                                      <p>{x.otherNotes[i].otherNote}</p>
                                                                      <div className="rightIcon">
                                                                        <div className="icon-holder">
                                                                          <EditIcon
                                                                            onClick={() => {
                                                                              setEditCorrespondenceNote(
                                                                                othernotevalue.otherNote
                                                                              );
                                                                              setEditCorrespondenceNoteInitial(
                                                                                othernotevalue.otherNote
                                                                              );
                                                                              hendelOpenCorrespondenceUpdate(
                                                                                value._id,
                                                                                x._id,
                                                                                othernotevalue._id
                                                                              );
                                                                            }}
                                                                            className="edit-icon"
                                                                          />
                                                                        </div>

                                                                        {userRole === 'SUPER_ADMIN' ? (
                                                                          <div className="icon-holder">
                                                                            <DeleteOutline
                                                                              onClick={() => {
                                                                                handleClickOpenDeleteCorrespondenceNote(
                                                                                  value._id,
                                                                                  x._id,
                                                                                  othernotevalue._id
                                                                                );
                                                                              }}
                                                                            />
                                                                          </div>
                                                                        ) : null}
                                                                      </div>
                                                                      <div className="list-bottom">
                                                                        <span className="posted-date">
                                                                          <strong>
                                                                            {/* Posted on : {new Date(x.createdAt).toLocaleDateString()} */}
                                                                            <strong>
                                                                              Posted on :&nbsp;
                                                                              {formatDate(othernotevalue.createdAt)}
                                                                            </strong>
                                                                          </strong>
                                                                        </span>
                                                                        <span className="assigned-to">
                                                                          <strong>
                                                                            Posted by :{' '}
                                                                            {postedByUser(othernotevalue.createdBy)}{' '}
                                                                          </strong>
                                                                        </span>
                                                                      </div>

                                                                      <Dialog
                                                                        open={openDeleteCorrespondenceNote}
                                                                        onClose={
                                                                          handleClickCloseDeleteCorrespondenceNote
                                                                        }
                                                                        aria-labelledby="alert-dialog-title"
                                                                        aria-describedby="alert-dialog-description"
                                                                      >
                                                                        <DialogTitle id="responsive-dialog-title">
                                                                          {'Are you sure want to Delete?'}
                                                                        </DialogTitle>
                                                                        <DialogContent>
                                                                          <DialogContentText id="alert-dialog-description">
                                                                            <span className="text-danger">
                                                                              Alert!!!
                                                                            </span>{' '}
                                                                            Are you sure you want to delete this record?
                                                                            Please note that, once deleted, this cannot
                                                                            be undone and the record will be permanently
                                                                            removed.
                                                                          </DialogContentText>
                                                                        </DialogContent>
                                                                        <DialogActions className="alert-btn-holder">
                                                                          <Button
                                                                            onClick={() => {
                                                                              dispatch(
                                                                                deleteCorrespondigNotes(
                                                                                  params.id,
                                                                                  deletefilenote.tranId,
                                                                                  deletefilenote.othersId,
                                                                                  deletefilenote.index
                                                                                )
                                                                              );
                                                                              handleClickCloseDeleteCorrespondenceNote();
                                                                            }}
                                                                            variant="contained"
                                                                            className="delete-button"
                                                                          >
                                                                            Delete
                                                                          </Button>
                                                                          <Button
                                                                            variant="outlined"
                                                                            onClick={
                                                                              handleClickCloseDeleteCorrespondenceNote
                                                                            }
                                                                            color="primary"
                                                                            // className="cancel-button"
                                                                          >
                                                                            Cancel
                                                                          </Button>
                                                                        </DialogActions>
                                                                      </Dialog>
                                                                    </div>
                                                                  ) : null;
                                                                })}
                                                              </>
                                                            ) : null}
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </AccordionDetails>
                                                  </Accordion>
                                                </div>
                                              );
                                            })
                                          : clientLists.loading
                                          ? null
                                          : 'No record found'}
                                      </div>
                                    </div>
                                  </AccordionDetails>
                                </Accordion>
                              );
                            })
                          )}
                        </>
                      ) : null}
                    </div>
                  </div>
                </TabPanel>

                <Dialog
                  open={fileDeleteOpen}
                  onClose={handleFileDeleteClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please
                      note that, once deleted, this cannot be undone and the record will be permanently removed.
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions className="alert-btn-holder">
                    <Button
                      onClick={() => {
                        handleFileDeleteClose();

                        if (fileDeleteSection === 'transactions') {
                          dispatch(deleteFilesAndCorrespondence(params.id, fileCorrespondingDelete));
                        }
                        if (fileDeleteSection === 'otherDetails') {
                          dispatch(deleteFilesAndCorrespondence(params.id, fileCorrespondingDelete));
                        }
                      }}
                      variant="contained"
                      className="delete-button"
                    >
                      Delete
                    </Button>
                    <Button
                      onClick={handleFileDeleteClose}
                      color="primary"
                      variant="outlined"
                      // className="cancel-button"
                    >
                      Cancel
                    </Button>
                  </DialogActions>
                </Dialog>

                <Dialog
                  open={openCorrespondenceUpdate}
                  onClose={hendelCloseCorrespondenceUpdate}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="responsive-dialog-title">
                    {'Update Correspondence Note'}
                    <IconButton onClick={hendelCloseCorrespondenceUpdate}>
                      <ClearIcon></ClearIcon>
                    </IconButton>
                  </DialogTitle>
                  <DialogContent>
                    <TextField
                      label="Follow up Note"
                      // value={value}
                      fullWidth
                      type="text"
                      value={editCorrespondenceNote}
                      onChange={(e) => {
                        setEditCorrespondenceNote(e.target.value.trimLeft());
                      }}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={() => {
                        dispatch(
                          updateCorrespondigNotes(
                            {
                              otherNote: editCorrespondenceNote
                            },
                            params.id,
                            editFileNoteDetails.tranId,
                            editFileNoteDetails.valueId,
                            editFileNoteDetails.index
                          )
                        );
                        // editFileNoteDetails

                        hendelCloseCorrespondenceUpdate();
                      }}
                      color="primary"
                      variant="contained"
                      className="forUpdates"
                      autoFocus
                      disabled={editCorrespondenceNote.trim() ? false : true}
                    >
                      Update
                    </Button>
                    <Button
                      autoFocus
                      // color="primary"
                      // variant="outlined"
                      variant="outlined"
                      className="forClearHover"
                      // className="cancel-button"
                      onClick={() => {
                        setEditCorrespondenceNote(editCorrespondenceNoteInitial);
                      }}
                    >
                      Clear
                    </Button>
                  </DialogActions>
                </Dialog>

                <Dialog
                  open={openCorrespondenceUpdateOther}
                  onClose={hendelCloseCorrespondenceUpdateOther}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="responsive-dialog-title">
                    {'Update Correspondence Note'}
                    <IconButton onClick={hendelCloseCorrespondenceUpdateOther}>
                      <ClearIcon></ClearIcon>
                    </IconButton>
                  </DialogTitle>
                  <DialogContent>
                    <TextField
                      label="Follow up Note"
                      // value={value}
                      fullWidth
                      type="text"
                      value={editCorrespondenceNote}
                      onChange={(e) => {
                        setEditCorrespondenceNote(e.target.value.trimLeft());
                      }}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={() => {
                        dispatch(
                          updateCorrespondigNotesOther(
                            {
                              newOtherNote: editCorrespondenceNote
                            },
                            params.id,
                            editFileNoteDetails.valueId,
                            editFileNoteDetails.index
                          )
                        );
                        // editFileNoteDetails

                        hendelCloseCorrespondenceUpdateOther();
                      }}
                      color="primary"
                      variant="contained"
                      className="forUpdates"
                      autoFocus
                      disabled={editCorrespondenceNote.trim() ? false : true}
                    >
                      Update
                    </Button>
                    <Button
                      autoFocus
                      // color="primary"
                      variant="outlined"
                      className="forClearHover"
                      onClick={() => {
                        setEditCorrespondenceNote(editCorrespondenceNoteInitial);
                      }}
                    >
                      Clear
                    </Button>
                  </DialogActions>
                </Dialog>

                <TabPanel value={value} index={2}>
                  <ReferralTab clientDetails={clientDetails}></ReferralTab>
                </TabPanel>

                <Dialog
                  open={transactionDeleteOpen}
                  onClose={handletransactionDeleteClose}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please
                      note that, once deleted, this cannot be undone and the record will be permanently removed.
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions className="alert-btn-holder">
                    <Button
                      onClick={() => {
                        handletransactionDeleteClose();
                        dispatch(deleteTransactionsDetails(params.id, transactionId));
                      }}
                      variant="contained"
                      className="delete-button"
                    >
                      Delete
                    </Button>
                    <Button
                      onClick={handletransactionDeleteClose}
                      color="primary"
                      variant="outlined"
                      // className="cancel-button"
                    >
                      Cancel
                    </Button>
                  </DialogActions>
                </Dialog>

                <Dialog
                  open={openDeleteCorrespondenceNoteOther}
                  onClose={handleClickCloseDeleteCorrespondenceNoteOther}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="responsive-dialog-title">{'Are you sure want to Delete?'}</DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please
                      note that, once deleted, this cannot be undone and the record will be permanently removed.
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions className="alert-btn-holder">
                    <Button
                      onClick={() => {
                        dispatch(
                          deleteCorrespondigNotesOther(params.id, deletefilenote.othersId, deletefilenote.index)
                        );
                        handleClickCloseDeleteCorrespondenceNoteOther();
                      }}
                      variant="contained"
                      className="delete-button"
                    >
                      Delete
                    </Button>
                    <Button
                      variant="outlined"
                      onClick={handleClickCloseDeleteCorrespondenceNoteOther}
                      color="primary"
                      // className="cancel-button"
                    >
                      Cancel
                    </Button>
                  </DialogActions>
                </Dialog>

                <TabPanel value={value} index={3}>
                  <MaritalStatusTab clientDetails={clientDetails}></MaritalStatusTab>
                </TabPanel>
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="Detail-page-dialog-files"
        className="detail-page-dialog"
      >
        <h4>Other Details : </h4>
        <Formik initialValues={initialValues} onSubmit={onSubmit} enableReinitialize>
          {({ values, setFieldValue }) => (
            <Form className="detail-page-dialog-form">
              <Box width="100%">
                <Field name="files">
                  {(props) => {
                    const { field, form } = props;
                    const { setFieldValue, values } = form;
                    return (
                      <Dropzone
                        accept=".pdf,.docx,.txt,.jpeg,.jpg,.png"
                        minSize={0}
                        maxSize={1024 * 1024 * 20}
                        {...field}
                        onDrop={(acceptedFiles, rejectedFiles) => {
                          handleonDrop(acceptedFiles, rejectedFiles, setFieldValue, values);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <section>
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <Box>
                                <span className="textarea-label">Files & Correspondence Upload</span>
                              </Box>

                              <Box
                                display="flex"
                                flexDirection="column"
                                justifyContent="center"
                                alignItems="center"
                                border="1px solid #DCDADA;"
                                padding="45px"
                                margin="0px 0 20px"
                              >
                                <Box textAlign="center" className="fileupload-content">
                                  <img
                                    src="https://ganesh.arsgroup.com.au/static/upload.png"
                                    width="101px"
                                    height="73.49px"
                                    alt="upload icons"
                                  />
                                  <Typography className="upload-subtitle">Click to Upload</Typography>
                                  <Typography>Drag & Drop File Here</Typography>
                                </Box>
                              </Box>
                            </div>
                            <Box textAlign="left">
                              <Paper style={{ paddingLeft: '0.5rem' }}>
                                {values.files.map((file, i) => {
                                  return (
                                    <Box key={i}>
                                      <Typography display="inline">{`File Name: ${file.name}, File Size: ${(
                                        file.size / 1024
                                      ).toFixed(3)}KB`}</Typography>
                                      <IconButton
                                        onClick={(e) => {
                                          e.preventDefault();
                                          const selectedFiles = [...values.files];
                                          selectedFiles.splice(i, 1);
                                          setFieldValue('files', selectedFiles);
                                          // if there are no files clear note too
                                          if (selectedFiles?.length === 0) {
                                            setFieldValue('correspondingNote', '');
                                            setFieldValue('correspondingTitle', '');
                                          }
                                        }}
                                      >
                                        <ClearIcon></ClearIcon>
                                      </IconButton>
                                    </Box>
                                  );
                                })}
                              </Paper>
                            </Box>
                          </section>
                        )}
                      </Dropzone>
                    );
                  }}
                </Field>
                <Field name="correspondingTitle">
                  {(props) => {
                    const { field } = props;
                    return <TextField label={<>Corresponding Title</>} type="text" {...field} />;
                  }}
                </Field>

                <span className={`textarea-label `}>Correspondence Note :&nbsp;</span>

                <Field name="correspondingNote">
                  {(props) => {
                    const { field } = props;
                    return (
                      <div>
                        <TextareaAutosize rowsMin={4} rowsMax={4} className="text-field-custom" {...field} />
                      </div>
                    );
                  }}
                </Field>
              </Box>
              <div className="form-buttons">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={loading || (values?.correspondingNote?.trim() === '' && values?.files[0] === undefined)}
                >
                  {loading ? <CircularProgress color="secondary" size={28} style={{ margin: '0 1rem' }} /> : 'Submit'}
                </Button>
                <Button type="reset" variant="outlined" className="forClearHover onlyForBig">
                  Clear
                </Button>
              </div>
            </Form>
          )}
        </Formik>

        <IconButton className="cross-button" aria-label="close" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </Dialog>

      <Dialog
        open={openOtherDetails}
        onClose={handleCloseOtherDetails}
        aria-labelledby="Detail-page-dialog-files"
        className="detail-page-dialog"
      >
        <h4>Other Details : </h4>
        <Formik initialValues={initialValuesOtherDetails} onSubmit={onSubmitOtherDetails} enableReinitialize>
          {({ values }) => (
            <Form className="detail-page-dialog-form">
              <Box width="100%">
                <Field name="files">
                  {(props) => {
                    const { field, form } = props;
                    const { setFieldValue, values } = form;
                    return (
                      <Dropzone
                        accept=".pdf,.docx,.txt,.jpeg,.jpg,.png"
                        minSize={0}
                        maxSize={1024 * 1024 * 20}
                        {...field}
                        onDrop={(acceptedFiles, rejectedFiles) => {
                          handleonDrop(acceptedFiles, rejectedFiles, setFieldValue, values);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <section>
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <Box>
                                <Typography variant="subtitle1" className="textarea-label">
                                  Files & Correspondence Upload
                                </Typography>
                              </Box>

                              <Box
                                display="flex"
                                flexDirection="column"
                                justifyContent="center"
                                alignItems="center"
                                border="1px solid #DCDADA;"
                                padding="45px"
                                margin="10px 0 30px"
                              >
                                <Box textAlign="center" className="fileupload-content">
                                  <img
                                    src="https://ganesh.arsgroup.com.au/static/upload.png"
                                    width="101px"
                                    height="73.49px"
                                    alt="upload icons"
                                  />
                                  <Typography className="upload-subtitle">Click to Upload</Typography>
                                  <Typography>Drag & Drop File Here</Typography>
                                </Box>
                              </Box>
                            </div>
                            <Box textAlign="left">
                              <Paper style={{ paddingLeft: '0.5rem' }}>
                                {values.files.map((file, i) => {
                                  return (
                                    <Box key={i}>
                                      <Typography display="inline">{`File Name: ${file.name}, File Size: ${(
                                        file.size / 1024
                                      ).toFixed(3)}KB`}</Typography>
                                      <IconButton
                                        onClick={(e) => {
                                          e.preventDefault();
                                          const selectedFiles = [...values.files];
                                          selectedFiles.splice(i, 1);
                                          setFieldValue('files', selectedFiles);
                                          // if there are no files clear note too
                                          if (selectedFiles?.length === 0)
                                            setFieldValue('newOtherNotes[0].newOtherNote', '');
                                        }}
                                      >
                                        <ClearIcon></ClearIcon>
                                      </IconButton>
                                    </Box>
                                  );
                                })}
                              </Paper>
                            </Box>
                          </section>
                        )}
                      </Dropzone>
                    );
                  }}
                </Field>

                <Typography variant="subtitle1" className={`textarea-label p-0 mb-sm `}>
                  Correspondence Note :
                </Typography>
                <Field name="newOtherNotes[0].newOtherNote">
                  {(props) => {
                    const { field } = props;
                    return (
                      <div>
                        <TextareaAutosize rowsMin={4} rowsMax={4} className="text-field-custom" {...field} />
                      </div>
                    );
                  }}
                </Field>
              </Box>
              <div className="form-buttons">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={
                    loading ||
                    (values?.newOtherNotes?.[0]?.newOtherNote?.trim() === '' && values?.files[0] === undefined)
                  }
                >
                  {loading ? <CircularProgress color="secondary" size={28} style={{ margin: '0 1rem' }} /> : 'Submit'}
                </Button>
                <Button
                  type="reset"
                  variant="outlined"
                  //  color="primary"
                  className="forClearHover onlyForBig"
                  // className="cancel-button"
                >
                  Clear
                </Button>
              </div>
            </Form>
          )}
        </Formik>

        <IconButton className="cross-button" aria-label="close" onClick={handleCloseOtherDetails}>
          <CloseIcon />
        </IconButton>
      </Dialog>

      {/* FILE UPLOAD DIALOG */}
      <Dialog
        open={openFileUploadDialogOther}
        onClose={() => setOpenFileUploadDialogOther(false)}
        aria-labelledby="Detail-page-dialog-files"
        className="detail-page-dialog"
      >
        <h4>Files Upload : </h4>
        <Formik initialValues={{ files: [] }} onSubmit={onFileUploadOther} enableReinitialize>
          {({ values }) => (
            <Form className="detail-page-dialog-form">
              <Box width="100%">
                <Field name="files">
                  {(props) => {
                    const { field, form } = props;
                    const { setFieldValue, values } = form;
                    return (
                      <Dropzone
                        accept=".pdf,.docx,.txt,.jpeg,.jpg,.png"
                        minSize={0}
                        maxSize={1024 * 1024 * 20}
                        {...field}
                        onDrop={(acceptedFiles, rejectedFiles) => {
                          handleonDropOther(acceptedFiles, rejectedFiles, setFieldValue, values);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <section>
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <Box>
                                <Typography variant="subtitle1" className="textarea-label">
                                  Files & Correspondence Upload
                                </Typography>
                              </Box>

                              <Box
                                display="flex"
                                flexDirection="column"
                                justifyContent="center"
                                alignItems="center"
                                border="1px solid #DCDADA;"
                                padding="45px"
                                margin="10px 0 30px"
                              >
                                <Box textAlign="center" className="fileupload-content">
                                  <img
                                    src="https://ganesh.arsgroup.com.au/static/upload.png"
                                    width="101px"
                                    height="73.49px"
                                    alt="upload icons"
                                  />
                                  <Typography className="upload-subtitle">Click to Upload</Typography>
                                  <Typography>Drag & Drop File Here</Typography>
                                </Box>
                              </Box>
                            </div>
                            <Box textAlign="left">
                              <Paper style={{ paddingLeft: '0.5rem' }}>
                                {values.files.map((file, i) => {
                                  return (
                                    <Box key={i}>
                                      <Typography display="inline">{`File Name: ${file.name}, File Size: ${(
                                        file.size / 1024
                                      ).toFixed(3)}KB`}</Typography>
                                      <IconButton
                                        onClick={(e) => {
                                          e.preventDefault();
                                          const selectedFiles = [...values.files];
                                          selectedFiles.splice(i, 1);
                                          setFieldValue('files', selectedFiles);
                                        }}
                                      >
                                        <ClearIcon></ClearIcon>
                                      </IconButton>
                                    </Box>
                                  );
                                })}
                              </Paper>
                            </Box>
                          </section>
                        )}
                      </Dropzone>
                    );
                  }}
                </Field>
              </Box>

              <div className="form-buttons">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={loading || values?.files?.length === 0}
                >
                  {loading ? <CircularProgress color="secondary" size={28} style={{ margin: '0 1rem' }} /> : 'Submit'}
                </Button>
                <Button
                  type="reset"
                  variant="outlined"
                  // color="primary"
                  className="forClearHover onlyForBig"
                >
                  Clear
                </Button>
              </div>
            </Form>
          )}
        </Formik>

        <IconButton className="cross-button" aria-label="close" onClick={() => setOpenFileUploadDialogOther(false)}>
          <CloseIcon />
        </IconButton>
      </Dialog>

      <Dialog
        open={openFileUploadDialog}
        onClose={() => setOpenFileUploadDialog(false)}
        aria-labelledby="Detail-page-dialog-files"
        className="detail-page-dialog"
      >
        <h4>Files Upload : </h4>
        <Formik initialValues={{ files: [] }} onSubmit={onFileUpload} enableReinitialize>
          {({ values }) => (
            <Form className="detail-page-dialog-form">
              <Box width="100%">
                <Field name="files">
                  {(props) => {
                    const { field, form } = props;
                    const { setFieldValue, values } = form;
                    return (
                      <Dropzone
                        accept=".pdf,.docx,.txt,.jpeg,.jpg,.png"
                        minSize={0}
                        maxSize={1024 * 1024 * 20}
                        {...field}
                        onDrop={(acceptedFiles, rejectedFiles) => {
                          handleonDrop(acceptedFiles, rejectedFiles, setFieldValue, values);
                        }}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <section>
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <Box>
                                <Typography variant="subtitle1" className="textarea-label">
                                  Files & Correspondence Upload
                                </Typography>
                              </Box>

                              <Box
                                display="flex"
                                flexDirection="column"
                                justifyContent="center"
                                alignItems="center"
                                border="1px solid #DCDADA;"
                                padding="45px"
                                margin="10px 0 30px"
                              >
                                <Box textAlign="center" className="fileupload-content">
                                  <img
                                    src="https://ganesh.arsgroup.com.au/static/upload.png"
                                    width="101px"
                                    height="73.49px"
                                    alt="upload icons"
                                  />
                                  <Typography className="upload-subtitle">Click to Upload</Typography>
                                  <Typography>Drag & Drop File Here</Typography>
                                </Box>
                              </Box>
                            </div>
                            <Box textAlign="left">
                              <Paper style={{ paddingLeft: '0.5rem' }}>
                                {values.files.map((file, i) => {
                                  return (
                                    <Box key={i}>
                                      <Typography display="inline">{`File Name: ${file.name}, File Size: ${(
                                        file.size / 1024
                                      ).toFixed(3)}KB`}</Typography>
                                      <IconButton
                                        onClick={(e) => {
                                          e.preventDefault();
                                          const selectedFiles = [...values.files];
                                          selectedFiles.splice(i, 1);
                                          setFieldValue('files', selectedFiles);
                                        }}
                                      >
                                        <ClearIcon></ClearIcon>
                                      </IconButton>
                                    </Box>
                                  );
                                })}
                              </Paper>
                            </Box>
                          </section>
                        )}
                      </Dropzone>
                    );
                  }}
                </Field>
              </Box>

              <div className="form-buttons">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={loading || values?.files?.length === 0}
                >
                  {loading ? <CircularProgress color="secondary" size={28} style={{ margin: '0 1rem' }} /> : 'Submit'}
                </Button>
                <Button
                  type="reset"
                  variant="outlined"
                  //  color="primary"
                  className="forClearHover onlyForBig"
                >
                  Clear
                </Button>
              </div>
            </Form>
          )}
        </Formik>

        <IconButton className="cross-button" aria-label="close" onClick={() => setOpenFileUploadDialog(false)}>
          <CloseIcon />
        </IconButton>
      </Dialog>

      <Dialog
        open={open1}
        onClose={handleShut}
        aria-labelledby="Detail-page-dialog-transaction"
        className="detail-page-dialog"
      >
        <h4>Transaction Details : </h4>
        <Formik initialValues={transactionInitialValues} onSubmit={transactionOnSubmit} enableReinitialize>
          {(formik) => {
            const { values, isSubmitting, handleChange, setFieldValue } = formik;
            return (
              <Form className="detail-page-dialog-form">
                <div className="form-row">
                  <Grid container spacing={3}>
                    <Grid item xs={12} lg={7}>
                      <Field name="propertyLotAddress">
                        {(props) => {
                          const { field } = props;
                          return (
                            <TextField
                              label={
                                <>
                                  Property Lot Address <span style={{ color: 'red' }}>*</span>
                                </>
                              }
                              type="text"
                              {...field}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                    <Grid item xs={12} lg={5}>
                      <Field name="upTo">
                        {(props) => {
                          const { field, meta } = props;
                          return (
                            <NumberFormat
                              label="Property Price Range"
                              thousandSeparator={true}
                              prefix={'$'}
                              customInput={TextField}
                              {...field}
                              onChange={(e) => {
                                const evt = { ...e };
                                if (e.target.value) {
                                  evt.target.value = String(Number(e.target.value.split('$')[1].replaceAll(/,/gi, '')));
                                }
                                handleChange(evt);
                              }}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row">
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <Field name="propertyType">
                        {(props) => {
                          const { field } = props;
                          return (
                            <div>
                              <span className="subtitle-label p-0">Property Type</span>
                              <RadioGroup aria-label="type" {...field}>
                                <Grid container spacing={1}>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="land_and_house"
                                      control={<Radio color="primary" />}
                                      label="House & Land"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="town_home"
                                      control={<Radio color="primary" />}
                                      label="Town House"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="units"
                                      control={<Radio color="primary" />}
                                      label="Units/ Apartment"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="all_kinds"
                                      control={<Radio color="primary" />}
                                      label="All Kinds"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="Others"
                                      control={<Radio color="primary" />}
                                      label="Others"
                                    />
                                  </Grid>
                                </Grid>
                              </RadioGroup>
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row">
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <Field name="purchaseStatus">
                        {(props) => {
                          const { field } = props;
                          return (
                            <div>
                              <span className="subtitle-label">Purchase Status</span>
                              <RadioGroup aria-label="status" name="status" {...field}>
                                <Grid container spacing={1}>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="raw_lead"
                                      control={<Radio color="primary" />}
                                      label="Raw Lead"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="qualified"
                                      control={<Radio color="primary" />}
                                      label="Qualified"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="negotiating"
                                      control={<Radio color="primary" />}
                                      label="Negotiating"
                                    />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel value="win" control={<Radio color="primary" />} label="Win" />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel value="lost" control={<Radio color="primary" />} label="Lost" />
                                  </Grid>
                                  <Grid item xs={12} md={6} lg={4}>
                                    <FormControlLabel
                                      value="follow_up"
                                      control={<Radio color="primary" />}
                                      label="Follow up in future"
                                    />
                                  </Grid>
                                  <Grid item xs={6} lg={1}>
                                    <FormControlLabel
                                      value="Others"
                                      control={<Radio color="primary" />}
                                      label="Others"
                                    />
                                  </Grid>
                                </Grid>
                              </RadioGroup>
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row mt-sm">
                  <Grid container spacing={1}>
                    <Grid item xs={12} md={8}>
                      <Field name="ooInvst">
                        {(props) => {
                          const { field } = props;
                          return (
                            <div>
                              <InputLabel>OO/INVST</InputLabel>
                              <Select {...field}>
                                <MenuItem value="" disabled>
                                  Please Select
                                </MenuItem>
                                <MenuItem value="OO">OO - Owner Occupier</MenuItem>
                                <MenuItem value="INVST">INVST - Investment Property</MenuItem>
                                <MenuItem value="OTHERS">Others</MenuItem>
                              </Select>
                            </div>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row mb-xl">
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <Field name="resultNotes[0].note">
                        {(props) => {
                          const { field } = props;
                          return (
                            <>
                              <div variant="subtitle1" className="textarea-label">
                                Result Notes :&nbsp;
                              </div>
                              <TextareaAutosize rowsMin={4} rowsMax={4} {...field} className="text-field-custom" />
                            </>
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row mt-sm">
                  <Field name="files">
                    {(props) => {
                      const { field, form } = props;
                      const { setFieldValue, values } = form;
                      return (
                        <Dropzone
                          accept=".pdf,.docx,.txt,.jpeg,.jpg,.png"
                          minSize={0}
                          maxSize={1024 * 1024 * 20}
                          {...field}
                          // onDrop={handleonDrop}
                          onDrop={(acceptedFiles, rejectedFiles) => {
                            handleonDrop(acceptedFiles, rejectedFiles, setFieldValue, values);
                          }}
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                <Box>
                                  <span className="textarea-label ">Files & Correspondence Upload</span>
                                </Box>

                                <Box
                                  display="flex"
                                  flexDirection="column"
                                  justifyContent="center"
                                  alignItems="center"
                                  border="1px solid #DCDADA;"
                                  padding="45px"
                                  margin="0 0 20px"
                                >
                                  <Box textAlign="center" className="fileupload-content">
                                    <img
                                      src="https://ganesh.arsgroup.com.au/static/upload.png"
                                      width="101px"
                                      height="73.49px"
                                      alt="upload icons"
                                    />
                                    <Typography className="upload-subtitle">Click to Upload</Typography>
                                    <Typography>Drag & Drop File Here</Typography>
                                  </Box>
                                </Box>
                              </div>
                              <Box textAlign="left">
                                <Paper style={{ paddingLeft: '0.5rem' }}>
                                  {values?.files?.map((file, i) => {
                                    return (
                                      <Box key={i}>
                                        <Typography display="inline">{`File Name: ${file.name}, File Size: ${(
                                          file.size / 1024
                                        ).toFixed(3)}KB`}</Typography>
                                        <IconButton
                                          onClick={(e) => {
                                            e.preventDefault();
                                            const selectedFiles = [...values.files];
                                            selectedFiles.splice(i, 1);
                                            setFieldValue('files', selectedFiles);
                                            if (selectedFiles?.length === 0) {
                                              setFieldValue('correspondingNotes[0].otherNotes[0].otherNote', '');
                                              setFieldValue('correspondingNotes[0].correspondingTitle', '');
                                            }
                                          }}
                                        >
                                          <ClearIcon></ClearIcon>
                                        </IconButton>
                                      </Box>
                                    );
                                  })}
                                </Paper>
                              </Box>
                            </section>
                          )}
                        </Dropzone>
                      );
                    }}
                  </Field>
                </div>
                <div className="form-row ">
                  <Grid container spacing={3}>
                    <Grid item xs={12}>
                      <Field name="correspondingNotes[0].correspondingTitle">
                        {(props) => {
                          const { field } = props;
                          return (
                            <TextField
                              label={
                                <>
                                  Corresponding Title
                                  {/* <span style={{ color: 'red' }}>*</span> */}
                                </>
                              }
                              type="text"
                              {...field}
                            />
                          );
                        }}
                      </Field>
                    </Grid>
                  </Grid>
                </div>
                <div className="form-row">
                  <span className={`textarea-label p-0 mb-sm `}>Correspondence Note : </span>

                  <Field name="correspondingNotes[0].otherNotes[0].otherNote">
                    {(props) => {
                      const { field } = props;
                      return (
                        <div>
                          <TextareaAutosize
                            rowsMin={4}
                            rowsMax={4}
                            className="text-field-custom"
                            {...field}

                            // disabled={true}
                            // onChange={()=>{
                            //   setDirty();
                            // }}
                          />
                        </div>
                      );
                    }}
                  </Field>
                </div>

                <div className="form-buttons">
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      handleShut();
                    }}
                    disabled={values.propertyLotAddress ? false : true}
                  >
                    Submit
                  </Button>

                  <Button type="reset" variant="outlined" className="onlyForBig forClearHover">
                    Clear
                  </Button>
                </div>
              </Form>
            );
          }}
        </Formik>

        <IconButton className="cross-button" aria-label="close" onClick={handleShut}>
          <CloseIcon />
        </IconButton>
      </Dialog>

      {/* File Viewer */}
      <FileViewer
        openFileViewer={openFileViewer}
        handleCloseFileViewer={handleCloseFileViewer}
        currentlyOpenedFile={currentlyOpenedFile}
      />
    </ThemeProvider>
  );
};

export default ClientDetail;
