import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextareaAutosize
} from '@material-ui/core';
import React from 'react';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import { AiFillFilePdf } from 'react-icons/ai';
import { Field, Form, Formik } from 'formik';
import { deleteCorrespondigNotes, updateFilesNotesOthers } from '../../../store/actions/clientAction';
import { useDispatch } from 'react-redux';

const OtherDetailsTab = (
    {   
        handleClickOpenOtherDetails,
        filesandcorrespondenceothersdetails,
        expandedFile,
        handleChangeFile,
        userRole,
        redirecttoClietEdit,
        setFileDeleteSection,
        handleClickFileDeleteOpen,
        setOpenFileUploadDialogOther,
        setCurrentCorrespondingId,
        getFileName,
        handleOpenFileViewer,
        handleClickOpenDeleteFilesOther,
        history,
        paramsId,
        setEditCorrespondenceNote,
        setEditCorrespondenceNoteInitial,
        hendelOpenCorrespondenceUpdateOther,
        handleClickOpenDeleteCorrespondenceNoteOther,
        formatDate,
        results,
        openDeleteCorrespondenceNote,
        handleClickCloseDeleteCorrespondenceNote,
    }
) => {

    const dispatch = useDispatch()

    const fileInitialValuesOther = {
        newOtherNote: ''
      };
      const fileNotesOnSubmitOther = (values, onSubmitProps) => {
        const data = {
          newOtherNote: values.newOtherNote
        };
        dispatch(updateFilesNotesOthers(data,paramsId, values._id, history));
      };
  return (
    <div className="tab-panel-details">
      <div className="file-uploads-heading details-heading">
        <h4>Files & Correspondence Upload:</h4>
        <div className="add-button">
          <Button
            type="submit"
            variant="contained"
            className="btn-secondary"
            startIcon={<AddIcon />}
            onClick={handleClickOpenOtherDetails}
          >
            Add
          </Button>
        </div>
      </div>
      {filesandcorrespondenceothersdetails?.length > 0
        ? [...filesandcorrespondenceothersdetails]?.reverse()?.map((x, i) => {
            return (
              <div className="detail-content-accordion">
                <Accordion
                  expanded={expandedFile === `panel[${i}]`}
                  onChange={handleChangeFile(`panel[${i}]`)}
                  className="acc-holder"
                >
                  <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                  >
                    <div className="edit-delete-wraper">
                      <h4>{i + 1}. Files &amp; Correspondence</h4>
                      <div>
                        <div className="edit-delete">
                          <EditIcon
                            onClick={() => {
                              redirecttoClietEdit();
                            }}
                            className="edit-icons"
                          />
                          {userRole === 'SUPER_ADMIN' ? (
                            <DeleteOutline
                              onClick={() => {
                                setFileDeleteSection('otherDetails');
                                handleClickFileDeleteOpen(x._id);
                              }}
                              className="margindelete fileCorrespondingDelete"
                            />
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </AccordionSummary>
                  <AccordionDetails>
                    <div className="file-uploads">
                      <span className="title">
                        Files :{' '}
                        <span className="add-button">
                          <Button
                            type="submit"
                            variant="contained"
                            className="btn-secondary-xs"
                            startIcon={<AddIcon />}
                            onClick={() => {
                              setOpenFileUploadDialogOther(true);
                              setCurrentCorrespondingId(x._id);
                              // setTransactionId(value._id);
                            }}
                          >
                            Add files
                          </Button>
                        </span>
                      </span>
                      <div className="file-uploads-container">
                        {typeof x?.correspondingOtherPath !== 'undefined'
                          ? [...x?.correspondingOtherPath]?.reverse()?.map((value, i) => {
                              return (
                                <div className="file-uploads-container-1">
                                  <div style={{ width: '29%' }} className="file-uploads-one">
                                    <AiFillFilePdf className="file-uploads-one-icon" />
                                    <span className="file-uploads-container-1-name">
                                      {getFileName(value) || `File ${i + 1}`}
                                    </span>
                                  </div>
                                  <div className="file-uploads-one">
                                    <button
                                      className="file-view-btn"
                                      onClick={() => {
                                        const fileName = value.split('\\{{')[1];
                                        handleOpenFileViewer(`{{${fileName}`);
                                      }}
                                    >
                                      View
                                    </button>
                                    <button className="file-uploads-one-btn-1">
                                      <a
                                        href={'/' + value}
                                        download={getFileName(value) || undefined}
                                        rel="noreferrer"
                                        target="_blank"
                                      >
                                        Download
                                      </a>
                                    </button>
                                    {userRole === 'SUPER_ADMIN' ? (
                                      <button
                                        className="file-uploads-one-btn-2"
                                        onClick={() => {
                                          handleClickOpenDeleteFilesOther(
                                            x._id,
                                            x.correspondingOtherPath.length - 1 - i
                                          );
                                        }}
                                      >
                                        Delete
                                      </button>
                                    ) : null}
                                  </div>
                                  {/* {x.correspondingUploadPaths[i] ? (
                                    <>
                                      <span className="filename">
                                        <a
                                          href={'/' + x.correspondingUploadPaths[i]}
                                          download={getFileName(x.correspondingUploadPaths[i]) || undefined}
                                          rel="noreferrer"
                                          target="_blank"
                                        >
                                          {getFileName(x.correspondingUploadPaths[i]) || `File ${i + 1}`}
                                        </a>
                                      </span>
                                    </>
                                  ) : null} */}
                                </div>
                              );
                            })
                          : null}
                      </div>
                    </div>

                    <div className="file-notes">
                      <Formik initialValues={fileInitialValuesOther} onSubmit={fileNotesOnSubmitOther}>
                        {(formik) => {
                          const { values, isSubmitting } = formik;
                          return (
                            <Form>
                              <div className="details-heading">
                                <span className="title">Correspondence Note:</span>
                                <div className="form-control">
                                  <Field name="newOtherNote">
                                    {(props) => {
                                      const { field, meta, form } = props;
                                      const { setFieldValue } = form;

                                      return (
                                        <TextareaAutosize
                                          aria-label="notes"
                                          // minRows={2}
                                          placeholder="Add a note"
                                          {...field}
                                          onChange={(e) => {
                                            setFieldValue('newOtherNote', e.target.value.trimLeft());
                                            setFieldValue('_id', x._id);
                                          }}
                                        />
                                      );
                                    }}
                                  </Field>

                                  <Button
                                    type="submit"
                                    variant="contained"
                                    className="btn-secondary"
                                    disabled={values?.newOtherNote?.trim?.() ? false : true}
                                  >
                                    Post
                                  </Button>
                                </div>
                              </div>
                            </Form>
                          );
                        }}
                      </Formik>
                      <div className="note-lists">
                        {true ? (
                          <>
                            {x?.newOtherNotes?.map((value, i) => {
                              return (
                                value.newOtherNote && (
                                  <div className="lists">
                                    <div className="rightIcon">
                                      <div className="icon-holder">
                                        <EditIcon
                                          onClick={() => {
                                            // setEditCorrespondenceNote(othernotevalue.otherNote);
                                            // setEditCorrespondenceNoteInitial(othernotevalue.otherNote);
                                            setEditCorrespondenceNote(value.newOtherNote);
                                            setEditCorrespondenceNoteInitial(value.newOtherNote);
                                            hendelOpenCorrespondenceUpdateOther(
                                              x._id,
                                              value._id
                                              // othernotevalue._id
                                            );
                                          }}
                                          className="edit-icon"
                                        />
                                      </div>

                                      {userRole === 'SUPER_ADMIN' ? (
                                        <div className="icon-holder">
                                          <DeleteOutline
                                            onClick={() => {
                                              handleClickOpenDeleteCorrespondenceNoteOther(x._id, value._id);
                                            }}
                                          />
                                        </div>
                                      ) : null}
                                    </div>

                                    <p>{value.newOtherNote}</p>
                                    <div className="list-bottom">
                                      <span className="posted-date">
                                        <strong>Posted on: {formatDate(value.createdAt)}</strong>
                                      </span>
                                      <span className="assigned-to">
                                        <strong>
                                          Posted by:
                                          {results?.find((v, i) => {
                                            return v.id === value.createdBy;
                                          })?.firstName + ' ' || ' NA'}
                                          {results?.find((v, i) => {
                                            return v.id === value.createdBy;
                                          })?.lastName || ' NA'}
                                        </strong>
                                      </span>
                                    </div>

                                    <Dialog
                                      open={openDeleteCorrespondenceNote}
                                      onClose={handleClickCloseDeleteCorrespondenceNote}
                                      aria-labelledby="alert-dialog-title"
                                      aria-describedby="alert-dialog-description"
                                    >
                                      <DialogTitle id="responsive-dialog-title">
                                        {'Are you sure want to Delete?'}
                                      </DialogTitle>
                                      <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                          <span className="text-danger">Alert!!!</span> Are you sure you want to delete
                                          this record? Please note that, once deleted, this cannot be undone and the
                                          record will be permanently removed.
                                        </DialogContentText>
                                      </DialogContent>
                                      <DialogActions className="alert-btn-holder">
                                        <Button
                                          onClick={() => {
                                            dispatch(deleteCorrespondigNotes(paramsId, x._id, i));
                                            handleClickCloseDeleteCorrespondenceNote();
                                          }}
                                          variant="contained"
                                          className="delete-button"
                                        >
                                          Delete
                                        </Button>
                                        <Button
                                          variant="outlined"
                                          onClick={handleClickCloseDeleteCorrespondenceNote}
                                          color="primary"
                                          // className="cancel-button"
                                        >
                                          Cancel
                                        </Button>
                                      </DialogActions>
                                    </Dialog>
                                  </div>
                                )
                              );
                            })}
                          </>
                        ) : null}
                      </div>
                    </div>
                  </AccordionDetails>
                </Accordion>
              </div>
            );
          })
        : 'No record found'}
    </div>
  );
};

export default OtherDetailsTab;

{/* <OtherDetailsTab
filesandcorrespondenceothersdetails={filesandcorrespondenceothersdetails}
expandedFile={expandedFile}
handleChangeFile={handleChangeFile}
handleClickOpenOtherDetails={handleClickOpenOtherDetails}
userRole={userRole}
redirecttoClietEdit={redirecttoClietEdit}
setFileDeleteSection={setFileDeleteSection}
handleClickFileDeleteOpen={handleClickFileDeleteOpen}
setOpenFileUploadDialogOther={setOpenFileUploadDialogOther}
setCurrentCorrespondingId={setCurrentCorrespondingId}
getFileName={getFileName}
handleOpenFileViewer={handleOpenFileViewer}
handleClickOpenDeleteFilesOther={handleClickOpenDeleteFilesOther}
history={history}
paramsId={params.id}
setEditCorrespondenceNote={setEditCorrespondenceNote}
setEditCorrespondenceNoteInitial={setEditCorrespondenceNoteInitial}
hendelOpenCorrespondenceUpdateOther={hendelOpenCorrespondenceUpdateOther}
handleClickOpenDeleteCorrespondenceNoteOther={handleClickOpenDeleteCorrespondenceNoteOther}
formatDate={formatDate}
results={results}
openDeleteCorrespondenceNote={openDeleteCorrespondenceNote}
handleClickCloseDeleteCorrespondenceNote={handleClickCloseDeleteCorrespondenceNote}

></OtherDetailsTab>  */}
