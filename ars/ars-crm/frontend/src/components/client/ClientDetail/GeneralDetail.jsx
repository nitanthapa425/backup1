import React from 'react'
import PhoneIcon from '@material-ui/icons/Phone';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import EmailIcon from '@material-ui/icons/Email';
import PersonIcon from '@material-ui/icons/Person';

const GeneralDetail = ({clientDetails}) => {
    return (
        <div className="client-personal-detail">
        <div className="top-col">
          {/* <div className="image-holder">
            <img src={img10} alt=""></img>
          </div> */}
          <h4>
            {clientDetails?.firstName} {clientDetails?.middleName} {clientDetails?.lastName}
          </h4>
        </div>
        <div className="bottom-col">
          {clientDetails?.mobile || clientDetails?.phone ? (
            <div className="contact-info">
              <div className="icon-holder">
                <PhoneIcon />
              </div>
              <div className="info-holder">
                {clientDetails?.mobile ? (
                  <span>
                    <strong>Mobile no : </strong> {clientDetails?.mobile}
                  </span>
                ) : null}

                {clientDetails?.phone ? (
                  <span>
                    <strong>Landline no : </strong> {clientDetails?.phone}
                  </span>
                ) : null}
              </div>
            </div>
          ) : null}

          {clientDetails?.mailingStreetNo ||
          clientDetails?.mailingStreetAddress ||
          clientDetails?.mailingSuburb ||
          clientDetails?.residentialStreetNo ||
          clientDetails?.residentialAddress ||
          clientDetails?.residentialSubUrb ? (
            <div className="contact-info">
              <div className="icon-holder">
                <LocationOnIcon />
              </div>
              <div className="info-holder">
                {clientDetails?.mailingStreetNo ||
                clientDetails?.mailingStreetAddress ||
                clientDetails?.mailingSuburb ? (
                  <span>
                    <strong>Mailing address : </strong>{' '}
                    <address>
                      {clientDetails?.mailingStreetNo ? clientDetails?.mailingStreetNo + ', ' : null}
                      {clientDetails?.mailingStreetAddress ? clientDetails?.mailingStreetAddress + ', ' : null}
                      {clientDetails?.mailingSuburb ? clientDetails?.mailingSuburb + ', ' : null}
                      {clientDetails?.mailingState ? clientDetails?.mailingState : null}
                    </address>
                  </span>
                ) : null}

                {clientDetails?.residentialStreetNo ||
                clientDetails?.residentialAddress ||
                clientDetails?.residentialSuburb ? (
                  <span>
                    <strong>Residential Address : </strong>
                    <address>
                      {clientDetails?.residentialStreetNo ? clientDetails?.residentialStreetNo + ', ' : null}
                      {clientDetails?.residentialStreetAddress
                        ? clientDetails?.residentialStreetAddress + ', '
                        : null}
                      {clientDetails?.residentialSuburb ? clientDetails?.residentialSuburb + ', ' : null}
                      {clientDetails?.residentialState ? clientDetails?.residentialState : null}
                    </address>
                  </span>
                ) : null}
              </div>
            </div>
          ) : null}

          {clientDetails?.email && (
            <div className="contact-info">
              <div className="icon-holder email-icon">
                <EmailIcon />
              </div>
              <div className="info-holder">
                <span>
                  <strong>Email : </strong>
                  {clientDetails?.email}
                </span>
              </div>
            </div>
          )}

          {clientDetails?.customerType && (
            <div className="contact-info">
              <div className="icon-holder user-icon">
                <PersonIcon />
              </div>
              <div className="info-holder">
                {clientDetails?.customerType ? (
                  <span>
                    <strong>Customer type : </strong> {clientDetails?.customerType}
                  </span>
                ) : null}
              </div>
            </div>
          )}
        </div>
      </div>

    )
}

export default GeneralDetail
