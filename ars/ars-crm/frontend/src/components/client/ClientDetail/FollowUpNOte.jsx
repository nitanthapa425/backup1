/* eslint-disable react-hooks/exhaustive-deps */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextareaAutosize,
  TextField
} from '@material-ui/core';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Autocomplete from '@material-ui/lab/Autocomplete';
import React, { useEffect, useState } from 'react';
import AreYouSurePopUp from '../../dropdown/AreYouSure';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditIcon from '@material-ui/icons/Edit';
import DatePicker from 'react-datepicker';
import { IconButton } from '@material-ui/core';
import {
  deletefollowupnote,
  followUpTaskCompleted,
  getFollowupNotes,
  updateClientFollowUpNotesAction,
  updatefollowupnote
} from '../../../store/actions/clientAction';
import { useDispatch } from 'react-redux';
import CloseIcon from '@material-ui/icons/Close';

const FollowUpNOte = ({ paramsId, userState, clientLists, userRole, formatDate, postedByUser, getDate }) => {
  const [assignTo, setAssignTo] = useState(null);
  const [followUpDate, setFollowUpDate] = useState('');
  const [updatedata, setupdatedata] = useState('');
  const [openTaskCompleted, setOpenTaskCompleted] = useState(false);
  const [taskId, setTaskId] = React.useState({});
  const [followUpUpdate, setFollowUpUpdate] = useState('');
  const [followUpUpdateInitialInitial, setFollowUpUpdateInitial] = useState('');
  const [assignedToUpdateInitial, setAssignedToUpdateInitial] = useState(null);
  const [assignedToUpdate, setAssignedToUpdate] = useState(null);
  const [followUpDateUpdate, setfollowUpDateUpdate] = useState('');
  const [followUpDateUpdateInitial, setfollowUpDateUpdateInitial] = useState('');
  const [openFollowUpdate, setOpenFollowUpdate] = React.useState(false);
  const [editnoteId, seteditnoteId] = useState('');
  const [deletenoteId, setdeletenoteId] = useState('');

  const [openFollowDel, setOpenFollowDel] = React.useState(false);
  const handleClickOpenFollowDel = (noteId) => {
    setdeletenoteId(noteId);
    setOpenFollowDel(true);
    // dispatch(deletefollowupnote(paramsId, noteId));
  };

  const handleCloseFollowDel = () => {
    setOpenFollowDel(false);
  };

  const handleFollowUpdate = (othersId) => {
    handleCloseFollowUpdate();
    const data = {
      followUpNote: followUpUpdate,
      assignedTo: assignedToUpdate.email,
      clientFollowUpDate: followUpDateUpdate
    };
    dispatch(updatefollowupnote(paramsId, othersId, data));
    setFollowUpUpdate('');
    setAssignedToUpdate(null);
    setfollowUpDateUpdate('');
  };
  const resetUpdateFollowUpForm = () => {
    setFollowUpUpdate(followUpUpdateInitialInitial);
    setAssignedToUpdate(assignedToUpdateInitial);
    setfollowUpDateUpdate(followUpDateUpdateInitial);
  };
  const handleCloseFollowUpdate = () => {
    setFollowUpUpdate('');
    setAssignedToUpdate(null);
    setfollowUpDateUpdate('');
    setOpenFollowUpdate(false);
  };
  const handleCloseTaskCompleted = () => {
    setOpenTaskCompleted(false);
  };
  useEffect(() => {
    dispatch(getFollowupNotes(paramsId));
  }, []);
  useEffect(() => {
    if (clientLists.deletedfollowupnotesuccessmsg) {
      dispatch(getFollowupNotes(paramsId));
    }
  }, [clientLists.deletedfollowupnotesuccessmsg]);
  useEffect(() => {
    if (clientLists.followUpTaskCompletedsuccessmsg) {
      dispatch(getFollowupNotes(paramsId));
    }
  }, [clientLists.followUpTaskCompletedsuccessmsg]);
  useEffect(() => {
    if (clientLists.updatedfollowupnotesuccessmsg) {
      dispatch(getFollowupNotes(paramsId));
    }
  }, [clientLists.updatedfollowupnotesuccessmsg]);

  useEffect(() => {
    if (clientLists.addFollowupSuccessMsg) {
      dispatch(getFollowupNotes(paramsId));
    }
  }, [clientLists.addFollowupSuccessMsg]);

  const handleClickOpenFollowUpdate = (followUpDate, followUpNote, assignedTo, noteId) => {
    setFollowUpUpdate(followUpNote);
    setFollowUpUpdateInitial(followUpNote);
    const selecteddata = userState.userList.results.find((user, i) => {
      return user.email === assignedTo;
    });
    setAssignedToUpdate(selecteddata);
    setAssignedToUpdateInitial(selecteddata);
    // setfollowUpDateUpdate(followUpDate.split('T')[0]);
    setfollowUpDateUpdate(followUpDate);
    setfollowUpDateUpdateInitial(followUpDate.split('T')[0]);
    seteditnoteId(noteId);
    setOpenFollowUpdate(true);
  };

  const dispatch = useDispatch();
  const emptyField = () => {
    setAssignTo(null);
    setupdatedata('');
    setFollowUpDate('');
  };

  const handleUpdateSubmit = async (e) => {
    e.preventDefault();

    if (updatedata) {
      dispatch(
        updateClientFollowUpNotesAction(
          {
            clientFollowUpDate: followUpDate,
            assignedTo: assignTo.email,
            followUpNote: updatedata
          },
          paramsId
        )
      );

      emptyField();
    }
  };

  const clientDetailAutoComplet = [];
  const clientDataPull = () => {
    if (typeof userState.userList !== 'undefined' && typeof userState.userList.results !== 'undefined') {
      for (let index = 0; index < userState.userList.results.length; index++) {
        clientDetailAutoComplet.push(userState.userList.results[index]);
      }
    }
  };

  clientDataPull();

  return (
    <div>
      <Dialog
        open={openFollowDel}
        onClose={handleCloseFollowDel}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">{'Are you sure want to delete?'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span className="text-danger">Alert!!!</span> Are you sure you want to delete this record? Please note that,
            once deleted, this cannot be undone and the record will be permanently removed.
          </DialogContentText>
        </DialogContent>
        <DialogActions className="alert-btn-holder">
          <Button
            onClick={() => {
              dispatch(deletefollowupnote(paramsId, deletenoteId));
              handleCloseFollowDel();
            }}
            variant="contained"
            autoFocus
            className="delete-button"
          >
            Delete
          </Button>
          <Button
            autoFocus
            onClick={handleCloseFollowDel}
            variant="outlined"
            color="primary"
            // className="cancel-button"
          >
            Cancel
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openFollowUpdate}
        onClose={handleCloseFollowUpdate}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title" className="MuiDialogTitle-root-big">
          {'Update Follow Up Note'}

          <IconButton
            className="cross-button MuiIconButton-root-big"
            aria-label="close"
            onClick={handleCloseFollowUpdate}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>

        <DialogContent className="MuiDialog-paperWidthSm-big">
          {/* <TextField
                style={{ marginBottom: '20px' }}
                label="Follow up date"
                fullWidth
                // defaultValue={value.clientFollowUpDate.split('T').shift()}
                onChange={(e) => setfollowUpDateUpdate(e.target.value)}
                type="date"
                value={followUpDateUpdate}
              /> */}
          <div className="follow-up-feild-create" style={{ marginBottom: '20px' }}>
            <label className="MuiInputLabel-root">Date</label>
            <DatePicker
              showTimeInput
              dateFormat="dd/MM/yyyy h:mm aa"
              selected={followUpDateUpdate ? new Date(followUpDateUpdate) : null}
              onChange={(date) => {
                setfollowUpDateUpdate(date);
              }}
              placeholderText="Please Select Date"
            />
          </div>
          <Autocomplete
            style={{ marginBottom: '20px' }}
            id="assignedTo"
            className="text-field-custom assigned-to"
            options={userState.userList.results || []}
            getOptionLabel={({ firstName, lastName, email }) => {
              return `${firstName || ''} ${lastName} (${email || ''})`;
            }}
            // defaultValue={function(){

            // }}
            value={assignedToUpdate}
            onChange={(_, newValue) => {
              setAssignedToUpdate(newValue);
            }}
            renderInput={(params) => <TextField fullWidth label="Assigned To" {...params} placeholder="Assigned To" />}
          />
          <TextField
            label="Follow up Note"
            onChange={(e) => {
              setFollowUpUpdate(e.target.value);
            }}
            fullWidth
            type="text"
            value={followUpUpdate}
          />
        </DialogContent>
        <DialogActions className="alert-btn-holder">
          <Button
            onClick={() => {
              handleFollowUpdate(editnoteId);
            }}
            variant="contained"
            color="primary"
          >
            Update
          </Button>
          <div className="cancel-button-holder">
            <AreYouSurePopUp
              variant="outlined"
              buttonName="Clear"
              popUpHeading="Are you sure you want to clear everything"
              popUpDetail="
                                  Are you sure you want to clear everything?"
              formName="myform"
              buttonType="reset"
              className="cancel-button"
              onAgreeClick={resetUpdateFollowUpForm}
              yesButtonName="Clear"
            />
          </div>
        </DialogActions>
      </Dialog>

      <div className="client-note-detail">
        <h3>Follow up notes regarding the client</h3>
        <form className="client-note-form" onSubmit={handleUpdateSubmit}>
          <div className="form-holder">
            <div className="note-date">
              {/* <TextField
                type="date"
                className="calender-feild"
                value={followUpDate}
                onChange={(e) => {
                  console.log(e.target.value);
                  setFollowUpDate(e.target.value);
                }}
              /> */}
              <div className="follow-up-feild">
                <DatePicker
                  showTimeInput
                  dateFormat="dd/MM/yyyy h:mm aa"
                  selected={followUpDate || null}
                  onChange={(date) => {
                    // console.log("Date: ", date.toUTCString());
                    setFollowUpDate(date);
                  }}
                  placeholderText="Please Select Date"
                />
              </div>
            </div>
            <div className="form-control">
              <TextareaAutosize
                aria-label="notes"
                // minRows={2}
                placeholder="Add a note"
                value={updatedata}
                onChange={(e) => {
                  setupdatedata(e.target.value);
                }}
              />
              <Autocomplete
                id="assignedTo"
                className="text-field-custom assigned-to"
                options={clientDetailAutoComplet || []}
                getOptionLabel={({ firstName, lastName, email }) => {
                  return `${firstName || ''} ${lastName} (${email || ''})`;
                }}
                value={assignTo}
                onChange={(_, newValue) => {
                  setAssignTo(newValue);
                }}
                renderInput={(params) => <TextField {...params} placeholder="Assigned To" />}
              />
            </div>
          </div>
          <div className="form-buttons">
            <div className="form-row">
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disabled={followUpDate && updatedata.trim() && assignTo ? false : true}
              >
                Post
              </Button>
              {/* show clear btn only if any of the value exist */}
              {(assignTo || followUpDate || updatedata) && (
                <div className="cancel-button-holder">
                  <AreYouSurePopUp
                    variant="outlined"
                    buttonName="Clear"
                    yesButtonName="Clear"
                    popUpHeading="Are you sure you want to clear everything"
                    popUpDetail="
                  Are you sure you want to clear everything?"
                    formName="myform"
                    buttonType="reset"
                    className="cancel-button"
                    onAgreeClick={emptyField}
                  />
                </div>
              )}
            </div>
          </div>
        </form>
        <div className="note-list-holder">
          <Dialog
            open={openTaskCompleted}
            onClose={handleCloseTaskCompleted}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="responsive-dialog-title">
              {taskId?.isNoteCompleted
                ? `Are you sure you want to mark this task as incomplete?`
                : `Are you sure you want to mark this task as complete?`}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                <span className="text-danger">Alert!!!</span> If you {taskId?.isNoteCompleted ? ' unmark ' : ' mark '}
                then the task {taskId?.isNoteCompleted ? 'will' : "won't"} show up for the assigned user.Are you sure
                want to {taskId?.isNoteCompleted ? ' incomplete ' : ' complete '}
                this task ?
              </DialogContentText>
            </DialogContent>
            <DialogActions className="alert-btn-holder">
              <Button
                onClick={() => {
                  handleCloseTaskCompleted();
                  dispatch(followUpTaskCompleted(paramsId, taskId));
                }}
                variant="contained"
                color="primary"
              >
                Agree
              </Button>
              <Button onClick={handleCloseTaskCompleted} variant="outlined" className="forClearHover">
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

          {/* {clientDetails?.clientFollowUpNotes?.length !== 0 ? (
            <div className="note-lists">
              {clientDetails?.clientFollowUpNotes &&
                clientDetails?.clientFollowUpNotes */}
          {clientLists?.getFollowupNotes?.length !== 0 ? (
            <div className="note-lists">
              {[...clientLists?.getFollowupNotes]
                ?.reverse()
                .filter((x) => x.followUpNote)
                .map((value, i) => {
                  return (
                    <div className={`lists ${value.isNoteCompleted ? 'blur' : ''}`}>
                      <div className="rightIcon">
                        <div className="icon-holder">
                          <EditIcon
                            onClick={() => {
                              handleClickOpenFollowUpdate(
                                value.clientFollowUpDate,
                                value.followUpNote.trimLeft(),
                                value.assignedTo,
                                value.noteId
                              );
                            }}
                            className="edit-icon"
                          />
                        </div>
                        {userRole === 'SUPER_ADMIN' ? (
                          <div className="icon-holder">
                            <DeleteOutline
                              onClick={() => {
                                handleClickOpenFollowDel(value.noteId);
                              }}
                            />
                          </div>
                        ) : null}
                      </div>
                      <span className="follow-up-date">
                        {value.isNoteCompleted ? (
                          <CheckCircleIcon
                            onClick={() => {
                              setOpenTaskCompleted(true);
                              setTaskId({ noteId: value._id, isNoteCompleted: value.isNoteCompleted });
                            }}
                            className="color-check-circle"
                          ></CheckCircleIcon>
                        ) : (
                          <CheckCircleOutlineIcon
                            onClick={() => {
                              setOpenTaskCompleted(true);
                              setTaskId({ noteId: value._id, isNoteCompleted: value.isNoteCompleted });
                            }}
                          ></CheckCircleOutlineIcon>
                        )}
                        <strong>Follow up date : {formatDate(value.clientFollowUpDate)}</strong>
                      </span>
                      <p>{value.followUpNote}</p>
                      <div className="list-bottom">
                        <span className="posted-date">
                          {value?.updatedBy ? (
                            <strong>Updated By: {postedByUser(value.updatedBy)} </strong>
                          ) : (
                            <strong>Created By: {postedByUser(value.createdBy)} </strong>
                          )}
                          <strong>
                            {' '}
                            <span className="color-orange"> On</span> {getDate(value._id)}{' '}
                          </strong>
                        </span>
                        <span className="assigned-to">
                          <strong>Assigned to: </strong> {value.assignedTo || 'NA'}
                        </span>
                      </div>
                    </div>
                  );
                })}
            </div>
          ) : (
            <span>No record Found</span>
          )}
        </div>
      </div>
    </div>
  );
};

export default FollowUpNOte;
