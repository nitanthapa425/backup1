import React from 'react';

const ReferralTab = ({ clientDetails }) => {
  return (
    <div className="tab-panel-details">
      <div className="details-heading">
        <h4>Referral Details : </h4>
      </div>

      {clientDetails?.referrals?.referralCode ||
      clientDetails?.referrals?.name ||
      clientDetails?.referrals?.email ||
      clientDetails?.referrals?.phone ||
      clientDetails?.referrals?.streetAddress ||
      clientDetails?.referrals?.referralLeadOpen ||
      clientDetails?.referrals?.referralLeadClose ? (
        <div className="details-list">
          <div className="list">
            {clientDetails?.referrals?.referralCode ? (
              <>
                <span className="title">Referral id :&nbsp;</span>
                {clientDetails?.referrals?.referralCode}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.name ? (
              <>
                <span className="title">Name :&nbsp; </span>
                {clientDetails?.referrals?.name}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.email ? (
              <>
                <span className="title">Email :&nbsp; </span>
                {clientDetails?.referrals?.email}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.phone ? (
              <>
                <span className="title">Mobile :&nbsp; </span>
                {clientDetails?.referrals?.phone}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.streetAddress ? (
              <>
                <span className="title">Address :&nbsp; </span>
                {clientDetails?.referrals?.streetAddress}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.referralLeadOpen ? (
              <>
                <span className="title">Lead Open :&nbsp; </span>
                {clientDetails?.referrals?.referralLeadOpen}
              </>
            ) : null}
          </div>
          <div className="list">
            {clientDetails?.referrals?.referralLeadClose ? (
              <>
                <span className="title">Lead Close :&nbsp; </span>
                {clientDetails?.referrals?.referralLeadClose}
              </>
            ) : null}
          </div>
        </div>
      ) : (
        <div>No record found</div>
      )}
    </div>
  );
};

export default ReferralTab;
