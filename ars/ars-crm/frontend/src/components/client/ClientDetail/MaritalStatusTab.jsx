import React from 'react';
const MaritalStatusTab = ({ clientDetails }) => {
  return (
    <div className="tab-panel-details">
      <div>
        <h4 className="inline-block-property">Marital Status : </h4>
        <span>
          &nbsp;
          {clientDetails?.maritalStatus ? (
            <h4 className="inline-block-property">{clientDetails?.maritalStatus}</h4>
          ) : (
            'No record found'
          )}
        </span>
      </div>

      {clientDetails?.maritalStatus && clientDetails?.maritalStatus === 'Married' && (
        <div className="details-list">
          {clientDetails?.spouseTitle ||
          clientDetails?.spouseFirstName ||
          clientDetails?.spouseMiddleName ||
          clientDetails?.spouseLastName ||
          clientDetails?.spouseEmail ||
          clientDetails?.spouseNumber ? (
            <>
              {clientDetails?.spouseTitle ? (
                <div className="list">
                  <span className="title">Spouse's Title&nbsp; </span>:&nbsp;
                  {clientDetails?.spouseTitle}
                </div>
              ) : null}

              {clientDetails?.spouseFirstName ? (
                <div className="list">
                  <span className="title">Spouse's First Name&nbsp;</span>:&nbsp;
                  {clientDetails?.spouseFirstName}
                </div>
              ) : null}

              {clientDetails?.spouseMiddleName ? (
                <div className="list">
                  <span className="title">Spouse's Middle Name&nbsp; </span>: {clientDetails?.spouseMiddleName}
                </div>
              ) : null}
              {clientDetails?.spouseLastName ? (
                <div className="list">
                  <span className="title">Spouse's Last Name&nbsp; </span>: {clientDetails?.spouseLastName}
                </div>
              ) : null}
              {clientDetails?.spouseEmail ? (
                <div className="list">
                  <span className="title">Spouse's Email&nbsp;</span>: {clientDetails?.spouseEmail}
                </div>
              ) : null}

              {clientDetails?.spouseNumber ? (
                <div className="list">
                  <span className="title">Spouse's Phone Number&nbsp;</span>: {clientDetails?.spouseNumber}
                </div>
              ) : null}
            </>
          ) : (
            <div>No record found</div>
          )}
        </div>
      )}
      {clientDetails?.maritalStatus && clientDetails?.maritalStatus === 'Partner' && (
        <div className="details-list">
          {clientDetails?.partnerTitle ||
          clientDetails?.partnerFirstName ||
          clientDetails?.partnerMiddleName ||
          clientDetails?.partnerLastName ||
          clientDetails?.partnerEmail ||
          clientDetails?.partnerNumber ? (
            <>
              {clientDetails?.partnerTitle ? (
                <div className="list">
                  <span className="title">Partner's Title&nbsp;</span>:&nbsp;
                  {clientDetails?.partnerTitle}
                </div>
              ) : null}

              {clientDetails?.partnerFirstName ? (
                <div className="list">
                  <span className="title">Partner's First Name&nbsp;</span>:&nbsp;
                  {clientDetails?.partnerFirstName}
                </div>
              ) : null}

              {clientDetails?.partnerMiddleName ? (
                <div className="list">
                  <span className="title">Partner's Middle Name&nbsp; </span>: {clientDetails?.partnerMiddleName}
                </div>
              ) : null}

              {clientDetails?.partnerLastName ? (
                <div className="list">
                  <span className="title">Partner's Last Name &nbsp;</span>: {clientDetails?.partnerLastName}
                </div>
              ) : null}

              {clientDetails?.partnerEmail ? (
                <div className="list">
                  <span className="title">Partner's Email&nbsp;</span>: {clientDetails?.partnerEmail}
                </div>
              ) : null}

              {clientDetails?.partnerNumber ? (
                <div className="list">
                  <span className="title">Partner's Phone Number&nbsp;</span>: {clientDetails?.partnerNumber}
                </div>
              ) : null}
            </>
          ) : (
            <div>No record found</div>
          )}
        </div>
      )}
    </div>
  );
};

export default MaritalStatusTab;
