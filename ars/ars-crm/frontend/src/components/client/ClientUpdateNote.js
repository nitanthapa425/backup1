import { Box, Paper, Typography, TextField, InputAdornment, Button } from '@material-ui/core';
import React, { useState } from 'react';
import PostAddIcon from '@material-ui/icons/PostAdd';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { updateClientDetailsAction } from '../../store/actions/clientAction';

const ClientUpdateNote = () => {
  const params = useParams();
  const dispatch = useDispatch();
  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });
  const { userdata } = clientLists;
  const { results } = userdata;

  const updatedClientDetail = results.filter((data, i) => {
    return data.id === params.id;
  });
  const [updatedata, setupdatedata] = useState({});

  const handleUpdateSubmit = async () => {
    dispatch(
      updateClientDetailsAction(
        { clientFollowUpNotes: [updatedata, ...updatedClientDetail[0].clientFollowUpNotes] },
        params.id
      )
    );
  };

  return (
    <Box width="70%">
      <Box margin={3}>
        <TextField
          placeholder="Add Notes"
          multiline={true}
          fullWidth={true}
          InputProps={{
            startAdornment: (
              <InputAdornment component={Button} onClick={handleUpdateSubmit} position="end">
                <PostAddIcon />
              </InputAdornment>
            )
          }}
          onChange={(e) => {
            setupdatedata({ followUpNote: e.target.value });
          }}
        />
      </Box>
      {updatedClientDetail[0].clientFollowUpNotes.map((v) => {
        return (
          <Paper component={Box} margin={3}>
            <Typography variant="headline" component="p">
              Note: {v.followUpNote}
            </Typography>
            <Typography variant="headline" component="p">
              Follow-up date:{' '}
              {updatedClientDetail[0].clientFollowUpDate && updatedClientDetail[0].clientFollowUpDate.slice(0, 10)}
            </Typography>
            <Typography variant="headline" component="p">
              Added at: {updatedClientDetail[0].createdAt.slice(0, 10)}
            </Typography>

            <Typography variant="headline" component="p">
              Updated by: {updatedClientDetail[0].createdBy.fullName}
            </Typography>
          </Paper>
        );
      })}
    </Box>
  );
};

export default ClientUpdateNote;
