import React, { useEffect } from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { clientListsAction } from '../../store/actions/clientAction';

const ClientProfileView = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });
  const { userdata } = clientLists;
  const { results } = userdata;

  const handleUpdate = (id) => {
    history.push(`/update/${id}`);
  };

  const handleDelete = (id) => {
    history.push(`/update/${id}`);
  };

  useEffect(() => {
    dispatch(clientListsAction());
  }, [dispatch]);

  return (
    <>
      <div className="container-fluid">
        <form method="GET">
          <div className="mx-3">
            <h3 className="text-xs-center m-y-3 text-center text-muted" id="speakers" style={{ padding: '0 40px' }}>
              Customer List
            </h3>
            <main id="site-main">
              <div
                class="container"
                style={{ maxWidth: 1170, marginLeft: 'auto', marginRight: 'auto', padding: '1em' }}
              >
                <div class="box-nav d-flex justify-between" style={{ padding: '20px 0' }}>
                  <a href="/client-profile/create" class="border-shadow">
                    <span class="text-gradient">
                      Create New User <i class="fas fa-user"></i>
                    </span>
                  </a>
                </div>

                <form>
                  <table class="table table-responsive table-responsive-md">
                    <thead class="thead-dark">
                      <tr class="table-responsive-md">
                        <th style={{ padding: '0 20px' }}>S.No.</th>
                        <th style={{ padding: '0 20px', float: 'left' }}> Name</th>
                        <th style={{ padding: '0 20px' }}>Marital Status</th>
                        <th style={{ padding: '0 20px' }}>Mailing Address</th>
                        <th style={{ padding: '0 20px' }}>Customer Type</th>
                        <th style={{ padding: '0 20px', float: 'left' }}>Purchase Status</th>
                      </tr>
                    </thead>
                    <tbody class="tbody">
                      {results
                        ? results.map((user, i) => {
                            return (
                              <tr id="customerProfile" key={user.id}>
                                <td style={{ padding: '0 20px' }}>{i + 1}</td>
                                <td style={{ padding: '0 20px' }}>{user.firstName + ' ' + user.lastName}</td>
                                <td style={{ padding: '0 20px' }}>{user.maritalStatus}</td>
                                <td style={{ padding: '0 20px' }}>{user.mailingCity}</td>
                                <td style={{ padding: '0 20px' }}>{user.customerType}</td>

                                <td style={{ padding: '0 20px' }}>
                                  <span class="md-col-3">
                                    {user.transactionDetails.map((i) => i.purchaseStatus + ' ')}
                                  </span>
                                </td>
                                <td>
                                  <button
                                    onClick={(e) => {
                                      e.preventDefault();
                                      handleUpdate(user.id);
                                    }}
                                  >
                                    <EditIcon></EditIcon>
                                  </button>
                                  &nbsp;
                                  <button
                                    onClick={() => {
                                      handleDelete(user.id);
                                    }}
                                  >
                                    <DeleteIcon></DeleteIcon>
                                  </button>
                                </td>
                              </tr>
                            );
                          })
                        : ''}
                    </tbody>
                  </table>
                </form>
              </div>
            </main>
          </div>
        </form>
      </div>
    </>
  );
};

export default ClientProfileView;
