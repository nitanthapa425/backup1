import React, { useState, useMemo } from 'react';
import { ThemeProvider } from '@material-ui/core';
import theme from '../../styles/theme';
import ClientProfileService from '../../services/client-profile-service';
import TableContainer from '../TableContainer/tablecontainer';
import { BsArrowDownShort, BsArrowUpShort } from 'react-icons/all';
import { SelectColumnFilter } from '../TableContainer/Filter';
import { useSelector } from 'react-redux';
import { USER } from '../../constants/roles';
import { ADMIN, SUPER_ADMIN } from './../../constants/roles';

const ClientTable = () => {
  const { paginatedClientQuery } = ClientProfileService;
  const [data, setData] = useState([]);
  const [loading, setLoading] = React.useState(false);
  const [pageCount, setPageCount] = React.useState(0);
  const [totalData, setTotalData] = React.useState(0);
  const [updated] = useState(0);

  const authReducer = useSelector((state) => {
    return state.authReducer;
  });

  // generate param string
  const getQueryString = (sortBy, pageSize, pageIndex, filters) => {
    const requiredDataColumns =
      '&select=fullName,email,mailingState,mobile,transactionDetails,referrals,clientFollowUpNotes';

    let sortString = '';
    if (sortBy.length) {
      sortString = '&sortBy=' + sortBy[0].id;
      sortString += sortBy[0].desc ? '&sortOrder=-1' : '&sortOrder=1';
    }

    let searchString = '';
    if (filters && filters.length) {
      searchString = '&search=' + filters.map((x) => `"${x.id}":"${x.value}"`).join(',');
    }
    return `?limit=${pageSize}&page=${pageIndex + 1}${sortString}${searchString}${requiredDataColumns}`;
  };

  const columns = useMemo(
    () => [
      {
        Header: () => null,
        id: 'expander', // 'id' is required
        Cell: ({ row }) => (
          <span {...row.getToggleRowExpandedProps()} title={'Show Details'}>
            {row.isExpanded ? <BsArrowUpShort /> : <BsArrowDownShort />}
          </span>
        )
      },
      {
        id: 'fullName',
        Header: 'Client Name',
        accessor: (el) => el?.fullName
      },
      {
        Header: 'Email',
        accessor: 'email',
        disableFilters: false
      },
      {
        id: 'mailingState',
        Header: 'State',
        accessor: (el) => el.mailingState || '-'
      },
      {
        id: 'mobile',
        Header: 'Mobile',
        accessor: (el) => el.mobile || '-'
      },
      {
        id: 'transactionDetails.ooInvst',
        Header: 'OO/INVST',
        accessor: (el) =>
          el?.transactionDetails
            .map((x) => x.ooInvst)
            .filter((x) => x)
            .join(', ') || '-',
        Filter: SelectColumnFilter,
        filter: 'equals',
        possibleFilters: ['OTHERS', 'INVST', 'OO']
      },
      {
        id: 'referrals.name',
        Header: 'Referral Source',
        accessor: (el) => el?.referrals?.name || '-'
      },
      {
        id: 'referrals.referralLeadOpen',
        Header: 'Lead Open',
        accessor: (el) => el?.referrals?.referralLeadOpen || '-'
      },
      {
        id: 'referrals.referralLeadClose',
        Header: 'Lead Close',
        accessor: (el) => el?.referrals?.referralLeadClose || '-'
      },
      {
        //does not have search
        id: 'clientFollowUpNotes.0.assignedTo',
        Header: 'Lead Assigned To',
        accessor: (el) => el?.clientFollowUpNotes[0]?.assignedTo || '-',
        disableFilters: true
      },
      {
        id: 'clientFollowUpNotes.0.followUpNote',
        //does not have search
        Header: 'Follow Up Note',
        accessor: (el) => el?.clientFollowUpNotes[0]?.followUpNote || '-',
        disableFilters: true
      }
    ],
    []
  );

  const getData = React.useCallback(
    ({ sortBy, pageSize, pageIndex, filters }) => {
      setLoading(true);

      // Make timeStamp default sort
      if (sortBy.length === 0) {
        sortBy[0] = { id: '_id', desc: true };
      }
      const query = getQueryString(sortBy, pageSize, pageIndex, filters);
      paginatedClientQuery(query)
        .then((response) => {
          const x = response.data;
          const role = authReducer.loginData?.user?.accessLevel;
          const loggedInUserEmail = authReducer.loginData?.user?.email?.toLowerCase();

          if ([ADMIN, SUPER_ADMIN].includes(role)) {
            setData(x.docs);
            setTotalData(x.totalDocs);
          } else if (role === USER) {
            const userReferredData = x.docs?.filter(
              (user) => user?.referrals?.email.toLowerCase() === loggedInUserEmail
            );
            setData(userReferredData);
            setTotalData(userReferredData.length);
          }
          setPageCount(x.totalPages);
          setLoading(false);
        })
        .catch((e) => {
          setLoading(false);
        });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updated]
  );

  // const searchGlobal = () => { };

  const renderRowSubComponent = () => {
    return (
      <span>
        Refferal code Id: 11
        <br />
      </span>
    );
  };
  // const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  return (
    <ThemeProvider theme={theme}>
      <div className="table-content-wrapper content-section">
        <TableContainer
          columns={columns}
          data={data}
          defaultPageSize={1}
          fetchData={getData}
          loading={loading}
          pageCount={pageCount}
          totalData={totalData}
          renderRowSubComponent={renderRowSubComponent}
        />
      </div>
    </ThemeProvider>
  );
};

export default ClientTable;
