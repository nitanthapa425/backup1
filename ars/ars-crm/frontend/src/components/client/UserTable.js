import { ThemeProvider } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import FilterListIcon from '@material-ui/icons/FilterList';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { clientListsAction, deleteClientDetailsAction, searchUsers } from '../../store/actions/clientAction';
// import { getCrmUsers } from '../../store/actions/userAction';

import theme from '../../styles/theme';
import DeleteIcon from '@material-ui/icons/Delete';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { useDebounce } from 'use-debounce';
import { AuthRole } from '../common/AuthRole';
export function createData(
  sn,
  name,
  status,
  number,
  email,
  location,
  ooInvst,
  referral,
  leadOpen,
  leadClose,
  leadAssigned,
  followUpNote
) {
  return {
    sn,
    name,
    status,
    number,
    email,
    location,
    ooInvst,
    referral,
    leadOpen,
    leadClose,
    leadAssigned,
    followUpNote
  };
}

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  // { id: 'sn', numeric: true, disablePadding: true, label: 'S/N', className: 'thead-sn' },
  { id: 'name', numeric: false, disablePadding: false, label: 'Client Name', className: 'customer-name' },
  // { id: 'status', numeric: false, disablePadding: false, label: 'Status', className: 'thead-status' },
  { id: 'number', numeric: true, disablePadding: false, label: 'Mobile Number' },
  { id: 'email', numeric: true, disablePadding: false, label: 'Email' },
  { id: 'location', numeric: true, disablePadding: false, label: 'State' },
  { id: 'ooInvst', numeric: false, disablePadding: false, label: 'OO / INVST' },
  { id: 'referral', numeric: true, disablePadding: false, label: 'Referral Source' },
  { id: 'leadOpen', numeric: false, disablePadding: false, label: 'Lead Open' },
  { id: 'leadClose', numeric: false, disablePadding: false, label: 'Lead Close' },
  { id: 'leadAssigned', numeric: false, disablePadding: false, label: 'Lead Assigned to' },
  { id: 'followUpNote', numeric: false, disablePadding: false, label: 'Follow Up Note' }
];

function EnhancedTableHead(props) {
  const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align="left"
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            className={headCell.className}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles((theme) => ({
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: '#f0fbfe'
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  title: {
    flex: '1 1 100%',
    margin: '0 30px 0 20px'
  }
}));

const EnhancedTableToolbar = (props, clientData) => {
  const dispatch = useDispatch();
  const [searchString, setSearchString] = useState('');
  const [debouncedSearchString] = useDebounce(searchString, 1000);
  const Auth = AuthRole();

  useEffect(() => {
    dispatch(searchUsers(debouncedSearchString));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [debouncedSearchString]);

  // const [search, setSearch] = useState('');
  const onChange = (event) => {
    setSearchString(event.target.value);
  };

  // const searchLists = useSelector((state) => {
  //   return state.clientReducer;
  // });

  const history = useHistory();
  const classes = useToolbarStyles();
  const { numSelected, selectedClient, setSelectedClient } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);

  let userRole = Auth.accessLevel;

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleUpdate = () => {
    history.push(`/client-profile/update/${selectedClient}`);
  };
  const handleClientUpdate = () => {
    history.push(`/update-client-profile/${selectedClient}`);
  };
  const [snackbarSuccess, setSnackbarSuccess] = useState(false);
  // const handleDelete = () => {
  //   const remove = window.confirm('Are you sure you want to delete?');
  //   if (remove) {
  //     selectedClient.forEach((id, index) => {
  //       dispatch(deleteClientDetailsAction(id));
  //     });

  //     setSelectedClient([]);

  //     // dispatch(deleteClientDetailsAction(selectedClient)).then(()=> {
  //     setSnackbarSuccess(true);
  //     // });
  //   }
  // };

  const [popUpopen, setpopUpopen] = React.useState(false);

  const handleClickOpen = () => {
    setpopUpopen(true);
  };

  const handleClosePopUp = () => {
    setpopUpopen(false);
  };
  const popUpAgree = () => {
    selectedClient.forEach((id, index) => {
      dispatch(deleteClientDetailsAction(id));
    });

    setSelectedClient([]);

    // dispatch(deleteClientDetailsAction(selectedClient)).then(()=> {
    setSnackbarSuccess(true);
    handleClosePopUp();
  };

  const popUpDisagree = () => {
    handleClosePopUp();
  };
  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      {selectedClient.length > 0 ? (
        <div className="tableHeader-left-col">
          <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
            {selectedClient.length} selected
          </Typography>
        </div>
      ) : (
        <div className="tableHeader-left-col">
          <Link to="/client-profile/create">
            <Button startIcon={<AddIcon />}>Add</Button>
          </Link>
          <form className="client-table-search">
            <div className="form-control">
              <TextField id="table-search" type="search" placeholder="Search" onChange={onChange} />
              <span class="icon-search"></span>
            </div>
          </form>
        </div>
      )}

      {selectedClient.length > 0 ? (
        <div className="tableHeader-right-col after-checked">
          {selectedClient.length <= 1 ? (
            <>
              {userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' || userRole === 'USER' ? (
                <Button variant="contained" className="view-btn" onClick={handleUpdate}>
                  View
                </Button>
              ) : null}

              {userRole === 'SUPER_ADMIN' || userRole === 'ADMIN' ? (
                <Button variant="contained" startIcon={<AddIcon />} className="edit-btn" onClick={handleClientUpdate}>
                  Edit
                </Button>
              ) : null}
            </>
          ) : null}
          {userRole === 'SUPER_ADMIN' ? (
            <Button variant="contained" startIcon={<DeleteIcon />} className="delete-btn" onClick={handleClickOpen}>
              Delete
            </Button>
          ) : null}
        </div>
      ) : (
        <div className="tableHeader-right-col">
          <Button aria-controls="filter menu" aria-haspopup="true" onClick={handleClick}>
            <FilterListIcon /> <span className="filter-text">Filter</span>
            <span className="icon-arrow-down"></span>
          </Button>
        </div>
      )}
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        open={snackbarSuccess}
        autoHideDuration={15000}
        onClose={() => setSnackbarSuccess(false)}
      >
        <Alert severity="success">Client deleted successfully</Alert>
      </Snackbar>
      <div style={{ display: 'none' }}>
        <Dialog
          open={popUpopen}
          onClose={handleClosePopUp}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{'Are you sure?'}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Are you sure you want to delete this record? Please note that, once deleted, this cannot be undone and the
              record will be permanently removed.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={popUpDisagree} variant="outlined" color="primary">
              Cancel
            </Button>
            <Button
              onClick={popUpAgree}
              //  variant="contained"
              className="cancel-button"
              autoFocus
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  },
  table: {
    minWidth: 750
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1
  }
}));

const UserTable = () => {
  // const checkedRow = useRef(null);

  const [selectedClient, setSelectedClient] = useState([]);

  const dispatch = useDispatch();
  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });
  const { userdata } = clientLists;
  const { results } = userdata;
  const { clientSearchData } = clientLists;

  //const rows = results || [];
  const rows = Array.isArray(clientSearchData.results) ? clientSearchData.results : results ? results : [];

  // const [userData, setUserData] = useState([]);

  useEffect(() => {
    dispatch(clientListsAction());
  }, [dispatch]);

  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('sn');
  const [selected] = React.useState([]);
  // const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  //const [rowsPerPage, setRowsPerPage] = React.useState(500);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  // const handleSelectAllClick = (event) => {
  //   if (event.target.checked) {
  //     const newSelecteds = rows.map((n) => n.sn);
  //     setSelected(newSelecteds);
  //     return;
  //   }
  //   setSelected([]);
  // };

  // const handleClick = (event, sn) => {
  //   const selectedIndex = selected.indexOf(sn);
  //   let newSelected = [];

  //   if (selectedIndex === -1) {
  //     newSelected = newSelected.concat(selected, sn);
  //   } else if (selectedIndex === 0) {
  //     newSelected = newSelected.concat(selected.slice(1));
  //   } else if (selectedIndex === selected.length - 1) {
  //     newSelected = newSelected.concat(selected.slice(0, -1));
  //   } else if (selectedIndex > 0) {
  //     newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
  //   }

  //   setSelected(newSelected);
  // };

  //const handleChangePage = (event, newPage) => {
  //setPage(newPage);
  //};

  //const handleChangeRowsPerPage = (event) => {
  //setRowsPerPage(parseInt(event.target.value, 10));
  //setPage(0);
  //};

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  // const isSelected = (sn) => selected.indexOf(sn) !== -1;

  //const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
  return (
    <ThemeProvider theme={theme}>
      <div className="table-content-wrapper content-section">
        <div className={classes.root}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            selectedClient={selectedClient}
            setSelectedClient={setSelectedClient}
          />
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={dense ? 'small' : 'medium'}
              aria-label="enhanced table"
            >
              <EnhancedTableHead
                classes={classes}
                numSelected={selected.length + 1}
                order={order}
                orderBy={orderBy}
                // onSelectAllClick={handleSelectAllClick}
                onRequestSort={handleRequestSort}
                rowCount={rows.length}
              />
              <TableBody>
                {stableSort(rows, getComparator(order, orderBy))
                  //.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    // const isItemSelected = isSelected(row.id);
                    const labelId = `enhanced-table-checkbox-${index}`;

                    return (
                      <TableRow
                      // onClick={() => {
                      //   let checked = document.querySelector(`#a${row.id}`).checked;
                      //   if (checked) {
                      //     setSelectedClient(
                      //       selectedClient.filter((value, index) => {
                      //         return value !== row.id;
                      //       })
                      //     );
                      //   } else {
                      //     setSelectedClient([...new Set([...selectedClient, row.id])]);
                      //   }
                      // }}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            id={`a${row.id}`}
                            checked={selectedClient.includes(row.id)}
                            inputProps={{ 'aria-labelledby': labelId }}
                            onChange={(e) => {
                              if (e.target.checked) {
                                setSelectedClient([...new Set([...selectedClient, row.id])]);
                              } else {
                                setSelectedClient(
                                  selectedClient.filter((value, index) => {
                                    return value !== row.id;
                                  })
                                );
                              }
                            }}
                          />
                        </TableCell>

                        <TableCell className="customer-name">{row.firstName + ' ' + row.lastName}</TableCell>
                        {/* <TableCell className="status">{row.status}</TableCell> */}
                        <TableCell>{row.mobile}</TableCell>
                        <TableCell>{row.email}</TableCell>
                        <TableCell>{row.mailingState ? row.mailingState : ''}</TableCell>
                        <TableCell>
                          {row.transactionDetails.length > 0
                            ? row.transactionDetails[row.transactionDetails.length - 1].ooInvst
                            : '-'}
                        </TableCell>
                        <TableCell>{row.referrals ? row.referrals.name : '-'}</TableCell>
                        <TableCell>{row.referrals ? row.referrals.referralLeadOpen : '-'}</TableCell>
                        <TableCell>{row.referrals ? row.referrals.referralLeadClose : '-'}</TableCell>
                        <TableCell>
                          {row.clientFollowUpNotes.length > 0 ? row.clientFollowUpNotes[0].assignedTo : '-'}
                        </TableCell>
                        <TableCell>
                          {row.clientFollowUpNotes.length > 0 ? row.clientFollowUpNotes[0].followUpNote : '-'}
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {/*emptyRows > 0 && (
                  <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )*/}
              </TableBody>
            </Table>
          </TableContainer>
          {/* <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component="div"
                  count={rows.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                /> */}
          <FormControlLabel control={<Switch checked={dense} onChange={handleChangeDense} />} label="Dense padding" />
        </div>
      </div>
    </ThemeProvider>
  );
};

export default UserTable;
