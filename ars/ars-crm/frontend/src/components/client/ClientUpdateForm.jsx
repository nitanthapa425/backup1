import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { clientDetailsAction } from "../../store/actions/clientAction";

import {
  clientUpdateInitialValues,
  clientUpdatevalidationSchema
} from "../formik-component/clientUpdateFormFormik";
import ClientProfileFormTemplate from "./ClientProfileFormTemplate";

// COMPONENT
const ClientUpdateForm = () => {

  const dispatch = useDispatch();

  const params = useParams();

  const clientLists = useSelector((state) => {
    return state.clientReducer;
  });


  const { clientDetails } = clientLists;

  const [maritalStatusValue, setMaritalStatusalue] = useState('');
  const [customerTypeValue, setCustomerTypeValue] = useState('');
  // const [clientStatusValue, setClientStatusValue] = useState('');
  const [referalCodeValue, setReferalCodeValue] = useState('');
  const [entryDateValue, setEntryDateValue] = useState('');

  useEffect(() => {
    if (clientDetails !== {}) {
      setMaritalStatusalue(clientDetails.maritalStatus);
      setCustomerTypeValue(clientDetails.customerType);
      // setClientStatusValue(clientDetails.clientStatus);
      setReferalCodeValue(clientDetails.referrals && clientDetails.referrals.referralCode);
      setEntryDateValue(clientDetails.entryDate);
    }
  }, [clientDetails]);


  useEffect(() => {
    dispatch(clientDetailsAction(params.id));
  }, [dispatch, params]);

  return <ClientProfileFormTemplate
    formType="update"
    initialValues={clientUpdateInitialValues(
      clientDetails,
      maritalStatusValue,
      customerTypeValue,
      entryDateValue,
      referalCodeValue
    )}
    validationSchema={clientUpdatevalidationSchema}

  />
}

export default ClientUpdateForm
