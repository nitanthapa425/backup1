import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Iframe from 'react-iframe';

import { getFileName } from './../../utils/getFileName';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const FileViewer = ({ openFileViewer, handleCloseFileViewer, currentlyOpenedFile }) => {
  return (
    <Dialog
      open={openFileViewer}
      onClose={handleCloseFileViewer}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth="lg"
    >
      <DialogTitle id="alert-dialog-title">
        File: {getFileName(currentlyOpenedFile)}{' '}
        <IconButton
          className="cross-button"
          aria-label="close"
          onClick={handleCloseFileViewer}
          style={{ float: 'right' }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      {currentlyOpenedFile && (
        <DialogContent>
          <Iframe
            url={`${process.env.REACT_APP_FILE_ACCESS_URL}/${currentlyOpenedFile}`}
            width="900px"
            height="563px"
            // id="myId"
            // className="myClassname"
            // display="initial"
            // position="relative"
          />
        </DialogContent>
      )}
    </Dialog>
  );
};
export default FileViewer;
