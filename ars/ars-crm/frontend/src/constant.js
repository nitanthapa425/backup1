import axios from 'axios';
export const BASE_URL = 'http://localhost:8080/api/v1';
// const BASE_URL = '/api/v1';

export const Axios = axios.create({
  baseURL: BASE_URL,
  timeout: 3000000,
  headers: {
    Accept: 'application/json',
    'content-type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem('user_accessToken')}`
  }
});

export const AxiosWithoutHeader = axios.create({
  baseURL: BASE_URL,
  timeout: 3000000,
  headers: {
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,OPTIONS,PATCH,DELETE,POST,PUT',
    'Access-Control-Allow-Headers':
      'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version'
  }
});

export const AxiosWithFormData = axios.create({
  baseURL: BASE_URL,
  timeout: 3000000,
  headers: {
    Accept: '/*',
    'content-type': undefined,
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET,OPTIONS,PATCH,DELETE,POST,PUT',
    'Access-Control-Allow-Headers':
      'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version',
    Authorization: `Bearer ${localStorage.getItem('user_accessToken')}`
  }
});
