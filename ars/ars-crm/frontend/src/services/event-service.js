import http from './httpHelper';

export default class EventService {
  static getEventListService() {
    return http().get('/calendar');
  }

  static create(eventDetail) {
    return http().post('/calendar/create', eventDetail);
  }
  static delete(eventId) {
    return http().delete(`/calendar/${eventId}`);
  }
  static edit(eventId, newData) {
    return http().put(`/calendar/${eventId}`, newData);
  }
}
