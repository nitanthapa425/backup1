import http from './httpHelper';

export default class TemplateService {
  static getTemplates() {
    return http().get('/template');
  }

}
