import http from './httpHelper';

export default class DashboardService {
  static showClientListsLength(date) {
    return http().get(`/customerDetails${date ? '?date='+ date : ''}`);
  }

  static getTotalFollowup() {
    return http().get('/clientFollowupNote');
  }
  static showfollowupNOteListAction() {
    return http().get(`/clientFollowupNote`);
  }
}
