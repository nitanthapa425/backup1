/**
 * Copyright (C) Tuki Logic
 */

/**
 * The application entry point
 *
 * @author      Ashim Karki
 * @version     1.0
 */

require('dotenv').config();
require('./src/bootstrap');

const config = require('config');
const express = require('express');

const cors = require('cors');
// const httpStatus = require('http-status');
const helmet = require('helmet');
// const autoReap = require('multer-autoreap');
const path = require('path');
const compress = require('compression');
const methodOverride = require('method-override');

const logger = require('./src/common/logger');
const errorMiddleware = require('./src/common/ErrorMiddleware');

const app = express();
// eslint-disable-next-line import/order
const http = require('http').Server(app);
const routes = require('./src/routes');

app.set('port', config.PORT);

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// parse body params and attache them to req.body
app.use(express.json());

// app.use(autoReap);

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// secure apps by setting various HTTP headers
app.use(helmet.hidePoweredBy());

// gzip compression
app.use(compress());

app.use(config.API_VERSION, routes);

app.use(errorMiddleware());
// app.use('/uploads', express.static('./uploads/client-profile-docs'));
app.use('/uploads', express.static(path.join(__dirname, '/uploads/client-profile-docs')));
// http://localhost:3100/api/v1/uploads/client-profile-docs/files-0b4e9c66-b8d5-4df7-95e2-b71d8543c002.pdf/

app.use(express.static(path.join(__dirname, '/frontend/build')));

app.use('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/frontend/build/index.html'));
});

if (!module.parent) {
  http.listen(app.get('port'), () => {
    logger.info(`Express server listening on port ${app.get('port')}`);
  });
} else {
  module.exports = app;
}
