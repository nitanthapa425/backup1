aggg
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| age   | int          | NO   | PRI | NULL    |       |
| name  | varchar(100) | YES  | UNI | NULL    |       |
+-------+--------------+------+-----+---------+-------+

ALTER TABLE aggg DROP primary key;                        note don not write drop constraint primary key;
note if you drop primary key then
automatically auto_increment is also drop
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| age   | int          | NO   |     | NULL    |       |
| name  | varchar(100) | YES  | UNI | NULL    |       |
+-------+--------------+------+-----+---------+-------+