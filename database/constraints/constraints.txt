constraint are also called restriction in english
constraint are other than column name and datatype
not null,,default,auto_increment,check(column1>30) ie check(condition),
pirmary key, unique key foreign key

not null means the column will not contain null data
default "value" it means if column is not inserted then its default value is value
check(colum1>30) it means the column has only data greater than 30

key
primary key,unique key and foreign key
note always it is better to write the key at last ie after declaring all column
ie 
CREATE TABLE table(c1 int,c2 varchar(100),c3 decimal(6,2),unique key(c2,c3) primary key(c1);

key ie either unique or primary or foreign key

unique key column have distinct record and can store null
a table has many unique key column;

primary key column have distinct record and can will not store null data
a table has only one primary key column

foreign key can have duplicate record as well as null 
but it can only store the record of parent primary key column
a table can have many foreign key column


example of all constraing
CREATE TABLE employee(
eid int auto_increment not null,
ename varchar(100) default "unknown", 
age int check(age>25);
primary key(eid)

);

CREATE TABLE department(
did int auto_increment ,
dname varchar(100) 
eeid int;
primary key(did);
foreign key(eeid) references employee(eid)
);


 
