mysql> select*from empdetails;
+----+--------+--------+
| id | name   | salary |
+----+--------+--------+
|  1 | nitan  |    100 |
|  2 | utshab |    200 |
|  3 | roshan |    300 |
|  4 | kamal  |    400 |
|  5 | dilly  |    500 |
|  6 | madhav |    600 |
|  7 | ram    |    300 |
+----+--------+--------+
select *from empdetails
 where salary=(select min(salary) from (select distinct salary from empdetails order by salary desc limit 4) as t1);

or do
select*from empdetails where salary=(select*from (select distinct salary from empdetails order by salary desc limit 4) as t1 order by salary limit 1);

+----+--------+--------+
| id | name   | salary |
+----+--------+--------+
|  3 | roshan |    300 |
|  7 | ram    |    300 |
+----+--------+--------+

point discover
1)select*from select*from empdetails;...........wrong
because to use select as a data or a table we have to put ()
select*from (select*from empdetails);...........wrong

2) again above statement is wrong
if you derive a table we most give the name
select*from (select*from empdetails) as t1;.................right

simply 
if a derive table is use to derive another table then its name must be define

if a record is use then derive table name is not necessar


