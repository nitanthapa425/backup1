SUBSTRING
getting the selected substring
you might be thinking why to use substring function if we have like
here like is use in where clause ie like is use to check

but substring is a function which is at star of select

syntax
SUBSTRING(column starting slicing , end slicing)
note unlike python we start from 1 not 0index
and end slicing in not 1 step a head
and there is no step size like python name[0:5:2]

SUBSTRING(column,1,3)
here unlike python 1 is S 
HERE 1 to 3 character is print

SELECT SUBSTRING("string",1,4)
stri is print

SELECT SUBSTRING("string",1,1)
s is print


SELECT SUBSTRING("string",3,3)
3 is print

@ write the program that print the the table 4 letter of each name

dstudent
+------------+--------+------+
| iddstudent | name   | roll |
+------------+--------+------+
|          1 |  nitan |    1 |
|          2 | utshab |    2 |
|          3 | roshan |    3 |
|          4 | nitan  |    4 |
+------------+--------+------+
